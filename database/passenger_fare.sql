-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2015 at 05:35 PM
-- Server version: 5.5.41
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oceanjet_temp`
--

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE IF NOT EXISTS `conditions` (
`id_conditions` int(11) unsigned NOT NULL,
  `conditions` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id_conditions`, `conditions`) VALUES
(1, 'Age Bracket'),
(2, 'Selling Source'),
(3, 'Require Advance Booking'),
(4, 'Booking Period'),
(5, 'Travel Period'),
(6, 'Travel Days'),
(7, 'Maximum Leg Interval');

-- --------------------------------------------------------

--
-- Table structure for table `leg`
--

CREATE TABLE IF NOT EXISTS `leg` (
`id_leg` int(11) unsigned NOT NULL,
  `passenger_fare_id` int(11) unsigned NOT NULL,
  `voyage_id` int(11) unsigned NOT NULL,
  `amount` decimal(13,2) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `leg`
--

INSERT INTO `leg` (`id_leg`, `passenger_fare_id`, `voyage_id`, `amount`) VALUES
(1, 92, 1, '555.00'),
(2, 92, 2, '666.00');

-- --------------------------------------------------------

--
-- Table structure for table `passenger_fare`
--

CREATE TABLE IF NOT EXISTS `passenger_fare` (
`id_passenger_fare` int(11) unsigned NOT NULL,
  `passenger_fare` varchar(50) NOT NULL,
  `trip_type_id` int(11) unsigned NOT NULL,
  `rule_set_id` int(11) unsigned NOT NULL,
  `seat_limit` int(5) unsigned NOT NULL,
  `regular_fare` tinyint(1) unsigned NOT NULL,
  `points_earned` int(5) unsigned NOT NULL,
  `points_redemption` int(5) unsigned NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passenger_fare`
--

INSERT INTO `passenger_fare` (`id_passenger_fare`, `passenger_fare`, `trip_type_id`, `rule_set_id`, `seat_limit`, `regular_fare`, `points_earned`, `points_redemption`, `enabled`, `date_added`, `date_update`) VALUES
(86, 'RA1', 123, 0, 0, 0, 0, 0, 1, '2015-12-04 15:42:41', '2015-12-04 15:42:41'),
(87, '44-88', 1, 86, 55, 0, 44, 0, 1, '2015-12-10 15:53:56', '2015-12-10 15:53:56'),
(88, '67-8776', 2, 87, 2333, 1, 55, 33, 1, '2015-12-10 15:54:20', '2015-12-10 15:54:20'),
(89, 'Test Fare Code', 1, 86, 44, 1, 33, 45, 1, '2015-12-10 16:07:03', '2015-12-10 16:07:03'),
(90, '44-887', 1, 86, 22, 0, 33, 566, 1, '2015-12-11 12:28:12', '2015-12-11 12:28:12'),
(91, 'IOD-88DD', 1, 86, 44, 1, 0, 0, 1, '2015-12-11 13:55:58', '2015-12-11 13:55:58'),
(92, 'PF01', 2, 87, 44, 1, 0, 0, 1, '2015-12-11 15:18:50', '2015-12-11 15:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `passenger_fare_conditions`
--

CREATE TABLE IF NOT EXISTS `passenger_fare_conditions` (
`id_passenger_fare_conditions` int(11) unsigned NOT NULL,
  `passenger_fare_id` int(11) unsigned NOT NULL,
  `conditions_id` tinyint(2) unsigned NOT NULL,
  `age_bracket_from` tinyint(2) unsigned NOT NULL,
  `age_bracket_to` tinyint(3) unsigned NOT NULL,
  `outlet_id` int(11) unsigned NOT NULL,
  `is_advance_booking` tinyint(2) unsigned NOT NULL,
  `booking_period_from` datetime NOT NULL,
  `booking_period_to` datetime NOT NULL,
  `travel_period_from` datetime NOT NULL,
  `travel_period_to` datetime NOT NULL,
  `travel_days` varchar(10) NOT NULL,
  `max_leg_interval` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passenger_fare_conditions`
--

INSERT INTO `passenger_fare_conditions` (`id_passenger_fare_conditions`, `passenger_fare_id`, `conditions_id`, `age_bracket_from`, `age_bracket_to`, `outlet_id`, `is_advance_booking`, `booking_period_from`, `booking_period_to`, `travel_period_from`, `travel_period_to`, `travel_days`, `max_leg_interval`) VALUES
(11, 86, 2, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0),
(12, 86, 3, 0, 0, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0),
(13, 90, 1, 55, 255, 10, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '6', 4),
(14, 90, 2, 55, 255, 10, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '6', 4),
(15, 90, 5, 55, 255, 10, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '6', 4),
(16, 90, 6, 55, 255, 10, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '6', 4),
(17, 90, 7, 55, 255, 10, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '6', 4),
(18, 92, 1, 55, 255, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0),
(19, 92, 2, 55, 255, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
 ADD PRIMARY KEY (`id_conditions`);

--
-- Indexes for table `leg`
--
ALTER TABLE `leg`
 ADD PRIMARY KEY (`id_leg`);

--
-- Indexes for table `passenger_fare`
--
ALTER TABLE `passenger_fare`
 ADD PRIMARY KEY (`id_passenger_fare`);

--
-- Indexes for table `passenger_fare_conditions`
--
ALTER TABLE `passenger_fare_conditions`
 ADD PRIMARY KEY (`id_passenger_fare_conditions`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
MODIFY `id_conditions` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `leg`
--
ALTER TABLE `leg`
MODIFY `id_leg` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `passenger_fare`
--
ALTER TABLE `passenger_fare`
MODIFY `id_passenger_fare` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `passenger_fare_conditions`
--
ALTER TABLE `passenger_fare_conditions`
MODIFY `id_passenger_fare_conditions` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
