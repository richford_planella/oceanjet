/*
MySQL Backup
Source Host:           192.168.0.41
Source Server Version: 5.5.41
Source Database:       oceanjet1_db
Date:                  2015/12/04 18:19:43
*/

SET FOREIGN_KEY_CHECKS=0;
use oceanjet1_db;
#----------------------------
# Table structure for accommodation
#----------------------------
CREATE TABLE `accommodation` (
  `id_accommodation` int(11) NOT NULL AUTO_INCREMENT,
  `accommodation_code` varchar(16) NOT NULL,
  `accommodation` varchar(128) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_accommodation`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table accommodation
#----------------------------


insert  into accommodation values 
(1, 'BC', 'Business Class', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(2, 'pu', 'tsa', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(3, 'cooode', 'naaame', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(4, 'AwC', 'Aw Class', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
#----------------------------
# Table structure for administrator_access
#----------------------------
CREATE TABLE `administrator_access` (
  `user_profile_id` int(10) unsigned NOT NULL,
  `page_id` int(11) unsigned NOT NULL,
  `access` tinyint(1) NOT NULL DEFAULT '0',
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `add` tinyint(1) NOT NULL DEFAULT '0',
  `edit` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  `approve` tinyint(4) NOT NULL DEFAULT '0',
  `verify` tinyint(4) NOT NULL DEFAULT '0',
  `print` tinyint(4) NOT NULL DEFAULT '0',
  `request` tinyint(4) NOT NULL DEFAULT '0',
  `close` tinyint(4) NOT NULL DEFAULT '0',
  `received` tinyint(4) NOT NULL DEFAULT '0',
  `allocate` tinyint(4) NOT NULL DEFAULT '0',
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  `reactivate` tinyint(1) NOT NULL DEFAULT '0',
  `export` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_profile_id`,`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#----------------------------
# Records for table administrator_access
#----------------------------


insert  into administrator_access values 
(2, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(1, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(1, 4, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 55, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 13, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 42, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 5, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 56, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 51, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 52, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(10, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(10, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(1, 53, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(4, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(2, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(1, 41, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 14, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(1, 54, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 141, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(1, 142, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1), 
(10, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 141, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(11, 143, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), 
(1, 143, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1);
#----------------------------
# Table structure for administrator_log
#----------------------------
CREATE TABLE `administrator_log` (
  `id_administrator_log` int(11) NOT NULL AUTO_INCREMENT,
  `administrator_id` int(11) NOT NULL,
  `administrator_profile_id` int(11) NOT NULL,
  `classname` varchar(128) NOT NULL,
  `action` varchar(128) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id_administrator_log`)
) ENGINE=InnoDB AUTO_INCREMENT=371 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table administrator_log
#----------------------------


insert  into administrator_log values 
(1, 1, 1, 'user', 'add', 2, 'Successfully added User Account!', '2015-11-27 18:10:08'), 
(2, 33, 2, 'logout', 'index', 0, '', '2015-11-27 18:10:39'), 
(3, 1, 1, 'client', 'add', 1, 'Successfully added client!', '2015-11-27 18:13:49'), 
(4, 1, 1, 'user', 'add', 3, 'Successfully added User Account!', '2015-11-27 18:16:27'), 
(5, 2, 4, 'logout', 'index', 0, '', '2015-11-27 18:16:43'), 
(6, 3, 4, 'logout', 'index', 0, '', '2015-11-27 18:17:37'), 
(7, 1, 1, 'user', 'add', 4, 'Successfully added User Account!', '2015-11-27 18:18:35'), 
(8, 4, 4, 'logout', 'index', 0, '', '2015-11-27 18:19:46'), 
(9, 1, 1, 'user', 'add', 5, 'Successfully added User Account!', '2015-11-27 18:22:06'), 
(10, 1, 1, 'client', 'add', 2, 'Successfully added client!', '2015-11-27 18:24:23'), 
(11, 1, 1, 'client', 'add', 3, 'Successfully added client!', '2015-11-27 18:24:58'), 
(12, 1, 1, 'client', 'add', 4, 'Successfully added client!', '2015-11-27 18:25:51'), 
(13, 1, 1, 'user', 'add', 6, 'Successfully added User Account!', '2015-11-27 18:26:29'), 
(14, 5, 4, 'logout', 'index', 0, '', '2015-11-27 18:26:56'), 
(15, 6, 4, 'logout', 'index', 0, '', '2015-11-27 18:27:40'), 
(16, 1, 1, 'client', 'add', 5, 'Successfully added client!', '2015-11-27 18:28:43'), 
(17, 1, 1, 'user', 'add', 7, 'Successfully added User Account!', '2015-11-27 18:38:05'), 
(18, 7, 3, 'logout', 'index', 0, '', '2015-11-27 18:39:07'), 
(19, 1, 1, 'user', 'add', 8, 'Successfully added User Account!', '2015-11-27 18:40:32'), 
(20, 8, 3, 'logout', 'index', 0, '', '2015-11-27 18:42:58'), 
(21, 1, 1, 'user', 'add', 9, 'Successfully added User Account!', '2015-11-27 18:44:54'), 
(22, 9, 3, 'logout', 'index', 0, '', '2015-11-27 18:46:22'), 
(23, 1, 1, 'user', 'add', 10, 'Successfully added User Account!', '2015-11-27 18:48:48'), 
(24, 10, 3, 'logout', 'index', 0, '', '2015-11-27 18:50:27'), 
(25, 1, 1, 'user', 'add', 11, 'Successfully added User Account!', '2015-11-27 18:52:02'), 
(26, 11, 3, 'logout', 'index', 0, '', '2015-11-27 18:54:08'), 
(27, 1, 1, 'user', 'add', 12, 'Successfully added User Account!', '2015-11-27 18:55:16'), 
(28, 12, 3, 'logout', 'index', 0, '', '2015-11-27 18:56:21'), 
(29, 1, 1, 'user', 'add', 13, 'Successfully added User Account!', '2015-11-27 18:57:39'), 
(30, 13, 3, 'logout', 'index', 0, '', '2015-11-27 18:58:44'), 
(31, 1, 1, 'project', 'add', 1, 'Successfully added project!', '2015-11-27 18:59:58'), 
(32, 1, 1, 'project', 'add', 2, 'Successfully added project!', '2015-11-27 19:00:09'), 
(33, 1, 1, 'project', 'add', 3, 'Successfully added project!', '2015-11-27 19:00:19'), 
(34, 1, 1, 'project', 'add', 4, 'Successfully added project!', '2015-11-27 19:00:40'), 
(35, 1, 1, 'project', 'add', 5, 'Successfully added project!', '2015-11-27 19:02:35'), 
(36, 1, 1, 'project', 'add', 6, 'Successfully added project!', '2015-11-27 19:02:51'), 
(37, 1, 1, 'project', 'add', 7, 'Successfully added project!', '2015-11-27 19:03:11'), 
(38, 1, 1, 'user', 'add', 14, 'Successfully added User Account!', '2015-11-27 19:07:15'), 
(39, 14, 2, 'logout', 'index', 0, '', '2015-11-27 19:09:43'), 
(40, 1, 1, 'user', 'add', 15, 'Successfully added User Account!', '2015-11-27 19:10:51'), 
(41, 15, 2, 'logout', 'index', 0, '', '2015-11-27 19:11:58'), 
(42, 1, 1, 'user', 'add', 16, 'Successfully added User Account!', '2015-11-27 19:13:29'), 
(43, 16, 2, 'logout', 'index', 0, '', '2015-11-27 19:14:42'), 
(44, 1, 1, 'user', 'add', 17, 'Successfully added User Account!', '2015-11-27 19:15:39'), 
(45, 17, 2, 'logout', 'index', 0, '', '2015-11-27 19:16:48'), 
(46, 1, 1, 'user', 'add', 18, 'Successfully added User Account!', '2015-11-27 19:17:41'), 
(47, 18, 2, 'logout', 'index', 0, '', '2015-11-27 19:23:42'), 
(48, 1, 1, 'appearance', 'index', 0, 'Change Site Title', '2015-11-27 19:31:54'), 
(49, 1, 1, 'appearance', 'index', 0, 'Change Site Title', '2015-11-27 19:32:15'), 
(50, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:46:02'), 
(51, 14, 2, 'logout', 'index', 0, '', '2015-11-27 19:46:13'), 
(52, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:47:23'), 
(53, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:50:38'), 
(54, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:50:57'), 
(55, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:51:21'), 
(56, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:53:18'), 
(57, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:54:37'), 
(58, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:56:31'), 
(59, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:58:18'), 
(60, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:59:56'), 
(61, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:00:26'), 
(62, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:00:32'), 
(63, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:01:29'), 
(64, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:01:48'), 
(65, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:02:45'), 
(66, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:04:00'), 
(67, 2, 4, 'logout', 'index', 0, '', '2015-11-27 20:07:23'), 
(68, 1, 1, 'user', 'add', 19, 'Successfully added User Account!', '2015-11-27 20:22:38'), 
(69, 2, 4, 'logout', 'index', 0, '', '2015-11-27 20:23:10'), 
(70, 1, 1, 'employee_data', 'index', 20, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(71, 1, 1, 'employee_data', 'index', 21, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(72, 1, 1, 'employee_data', 'index', 22, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(73, 1, 1, 'employee_data', 'index', 23, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(74, 1, 1, 'employee_data', 'index', 24, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(75, 1, 1, 'employee_data', 'index', 25, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(76, 1, 1, 'employee_data', 'index', 26, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(77, 1, 1, 'employee_data', 'index', 27, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(78, 1, 1, 'employee_data', 'index', 28, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(79, 1, 1, 'employee_data', 'index', 29, 'Successfully added user account.', '2015-11-27 20:33:18'), 
(80, 1, 1, 'employee_data', 'index', 30, 'Successfully added user account.', '2015-11-27 20:33:19'), 
(81, 1, 1, 'employee_data', 'index', 31, 'Successfully added user account.', '2015-11-27 20:33:19'), 
(82, 1, 1, 'employee_data', 'index', 32, 'Successfully added user account.', '2015-11-27 20:33:19'), 
(83, 1, 1, 'employee_data', 'index', 33, 'Successfully added user account.', '2015-11-27 20:33:19'), 
(84, 1, 1, 'employee_data', 'index', 34, 'Successfully added user account.', '2015-11-27 20:33:19'), 
(85, 1, 1, 'employee_data', 'index', 35, 'Successfully added user account.', '2015-11-27 20:33:19'), 
(86, 1, 1, 'employee_data', 'index', 36, 'Successfully added user account.', '2015-11-27 20:33:19'), 
(87, 1, 1, 'employee_data', 'index', 19, 'User successfully updated.', '2015-11-27 20:33:19'), 
(88, 1, 1, 'employee_data', 'index', 37, 'Successfully added user account.', '2015-11-27 20:33:19'), 
(89, 1, 1, 'employee_data', 'index', 20, 'User successfully updated.', '2015-11-27 20:36:05'), 
(90, 1, 1, 'employee_data', 'index', 21, 'User successfully updated.', '2015-11-27 20:36:05'), 
(91, 1, 1, 'employee_data', 'index', 22, 'User successfully updated.', '2015-11-27 20:36:05'), 
(92, 1, 1, 'employee_data', 'index', 23, 'User successfully updated.', '2015-11-27 20:36:05'), 
(93, 1, 1, 'employee_data', 'index', 24, 'User successfully updated.', '2015-11-27 20:36:05'), 
(94, 1, 1, 'employee_data', 'index', 25, 'User successfully updated.', '2015-11-27 20:36:05'), 
(95, 1, 1, 'employee_data', 'index', 26, 'User successfully updated.', '2015-11-27 20:36:05'), 
(96, 1, 1, 'employee_data', 'index', 27, 'User successfully updated.', '2015-11-27 20:36:05'), 
(97, 1, 1, 'employee_data', 'index', 28, 'User successfully updated.', '2015-11-27 20:36:05'), 
(98, 1, 1, 'employee_data', 'index', 29, 'User successfully updated.', '2015-11-27 20:36:05'), 
(99, 1, 1, 'employee_data', 'index', 30, 'User successfully updated.', '2015-11-27 20:36:05'), 
(100, 1, 1, 'employee_data', 'index', 31, 'User successfully updated.', '2015-11-27 20:36:05'), 
(101, 1, 1, 'employee_data', 'index', 32, 'User successfully updated.', '2015-11-27 20:36:05'), 
(102, 1, 1, 'employee_data', 'index', 33, 'User successfully updated.', '2015-11-27 20:36:05'), 
(103, 1, 1, 'employee_data', 'index', 34, 'User successfully updated.', '2015-11-27 20:36:05'), 
(104, 1, 1, 'employee_data', 'index', 35, 'User successfully updated.', '2015-11-27 20:36:05'), 
(105, 1, 1, 'employee_data', 'index', 36, 'User successfully updated.', '2015-11-27 20:36:05'), 
(106, 1, 1, 'employee_data', 'index', 19, 'User successfully updated.', '2015-11-27 20:36:05'), 
(107, 1, 1, 'employee_data', 'index', 37, 'User successfully updated.', '2015-11-27 20:36:05'), 
(108, 1, 1, 'employee_data', 'index', 20, 'User successfully updated.', '2015-11-27 20:37:21'), 
(109, 1, 1, 'employee_data', 'index', 21, 'User successfully updated.', '2015-11-27 20:37:21'), 
(110, 1, 1, 'employee_data', 'index', 22, 'User successfully updated.', '2015-11-27 20:37:21'), 
(111, 1, 1, 'employee_data', 'index', 23, 'User successfully updated.', '2015-11-27 20:37:21'), 
(112, 1, 1, 'employee_data', 'index', 24, 'User successfully updated.', '2015-11-27 20:37:21'), 
(113, 1, 1, 'employee_data', 'index', 25, 'User successfully updated.', '2015-11-27 20:37:21'), 
(114, 1, 1, 'employee_data', 'index', 26, 'User successfully updated.', '2015-11-27 20:37:21'), 
(115, 1, 1, 'employee_data', 'index', 27, 'User successfully updated.', '2015-11-27 20:37:21'), 
(116, 1, 1, 'employee_data', 'index', 28, 'User successfully updated.', '2015-11-27 20:37:21'), 
(117, 1, 1, 'employee_data', 'index', 29, 'User successfully updated.', '2015-11-27 20:37:21'), 
(118, 1, 1, 'employee_data', 'index', 30, 'User successfully updated.', '2015-11-27 20:37:21'), 
(119, 1, 1, 'employee_data', 'index', 31, 'User successfully updated.', '2015-11-27 20:37:21'), 
(120, 1, 1, 'employee_data', 'index', 32, 'User successfully updated.', '2015-11-27 20:37:21'), 
(121, 1, 1, 'employee_data', 'index', 33, 'User successfully updated.', '2015-11-27 20:37:21'), 
(122, 1, 1, 'employee_data', 'index', 34, 'User successfully updated.', '2015-11-27 20:37:21'), 
(123, 1, 1, 'employee_data', 'index', 35, 'User successfully updated.', '2015-11-27 20:37:21'), 
(124, 1, 1, 'employee_data', 'index', 36, 'User successfully updated.', '2015-11-27 20:37:21'), 
(125, 1, 1, 'employee_data', 'index', 19, 'User successfully updated.', '2015-11-27 20:37:21'), 
(126, 1, 1, 'employee_data', 'index', 37, 'User successfully updated.', '2015-11-27 20:37:21'), 
(127, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-29 10:15:37'), 
(128, 7, 3, 'logout', 'index', 0, '', '2015-11-29 21:07:33'), 
(129, 14, 2, 'logout', 'index', 0, '', '2015-11-29 22:47:50'), 
(130, 16, 2, 'logout', 'index', 0, '', '2015-11-30 17:12:44'), 
(131, 17, 2, 'logout', 'index', 0, '', '2015-11-30 17:23:15'), 
(132, 18, 2, 'logout', 'index', 0, '', '2015-11-30 17:24:42'), 
(133, 1, 1, 'user', 'add', 38, 'Successfully added User Account!', '2015-11-30 17:44:19'), 
(134, 38, 3, 'logout', 'index', 0, '', '2015-11-30 17:45:47'), 
(135, 1, 1, 'user', 'add', 39, 'Successfully added User Account!', '2015-11-30 17:48:02'), 
(136, 39, 3, 'logout', 'index', 0, '', '2015-11-30 17:49:49'), 
(137, 1, 1, 'user', 'add', 40, 'Successfully added User Account!', '2015-11-30 17:52:35'), 
(138, 1, 1, 'project', 'add', 8, 'Successfully added project!', '2015-11-30 17:54:52'), 
(139, 1, 1, 'project', 'add', 9, 'Successfully added project!', '2015-11-30 17:55:07'), 
(140, 1, 1, 'project', 'add', 10, 'Successfully added project!', '2015-11-30 17:55:20'), 
(141, 1, 1, 'client_group', 'add', 1, 'Successfully added employee client!', '2015-11-30 17:56:28'), 
(142, 1, 1, 'user', 'add', 41, 'Successfully added User Account!', '2015-11-30 17:58:13'), 
(143, 40, 3, 'logout', 'index', 0, '', '2015-11-30 17:58:33'), 
(144, 41, 2, 'logout', 'index', 0, '', '2015-11-30 17:59:49'), 
(145, 1, 1, 'user', 'add', 42, 'Successfully added User Account!', '2015-11-30 18:01:07'), 
(146, 1, 1, 'user', 'edit', 42, 'Successfully updated User Account!', '2015-11-30 18:03:39'), 
(147, 42, 2, 'logout', 'index', 0, '', '2015-11-30 18:03:58'), 
(148, 1, 1, 'user', 'add', 43, 'Successfully added User Account!', '2015-11-30 18:04:56'), 
(149, 1, 1, 'user', 'edit', 1, 'Successfully updated User Account!', '2015-11-30 18:05:49'), 
(150, 43, 2, 'logout', 'index', 0, '', '2015-11-30 18:07:03'), 
(151, 1, 1, 'user', 'add', 44, 'Successfully added User Account!', '2015-11-30 18:08:56'), 
(152, 44, 2, 'logout', 'index', 0, '', '2015-11-30 18:10:25'), 
(153, 1, 1, 'user', 'add', 45, 'Successfully added User Account!', '2015-11-30 18:11:05'), 
(154, 45, 2, 'logout', 'index', 0, '', '2015-11-30 18:12:23'), 
(155, 45, 2, 'logout', 'index', 0, '', '2015-11-30 18:12:54'), 
(156, 1, 1, 'user', 'add', 46, 'Successfully added User Account!', '2015-11-30 18:15:27'), 
(157, 46, 5, 'employee_list', 'add', 1, 'Successfully added employee rate.', '2015-11-30 18:18:32'), 
(158, 46, 5, 'employee_list', 'add', 2, 'Successfully added employee rate.', '2015-11-30 18:19:16'), 
(159, 46, 5, 'employee_list', 'add', 3, 'Successfully added employee rate.', '2015-11-30 18:19:51'), 
(160, 46, 5, 'employee_list', 'add', 4, 'Successfully added employee rate.', '2015-11-30 18:20:37'), 
(161, 46, 5, 'employee_list', 'add', 5, 'Successfully added employee rate.', '2015-11-30 18:21:30'), 
(162, 46, 5, 'logout', 'index', 0, '', '2015-11-30 18:22:39'), 
(163, 41, 2, 'timesheet', 'edit', 9, 'Timesheet Submitted', '2015-11-30 18:29:57'), 
(164, 41, 2, 'logout', 'index', 0, '', '2015-11-30 18:31:29'), 
(165, 42, 2, 'timesheet', 'edit', 12, 'Timesheet Submitted', '2015-11-30 18:36:41'), 
(166, 42, 2, 'logout', 'index', 0, '', '2015-11-30 18:36:56'), 
(167, 43, 2, 'timesheet', 'edit', 15, 'Timesheet Submitted', '2015-11-30 18:48:58'), 
(168, 43, 2, 'logout', 'index', 0, '', '2015-11-30 18:49:08'), 
(169, 44, 2, 'timesheet', 'edit', 16, 'Timesheet Submitted', '2015-11-30 18:57:40'), 
(170, 44, 2, 'logout', 'index', 0, '', '2015-11-30 18:57:56'), 
(171, 45, 2, 'timesheet', 'edit', 17, 'Timesheet Submitted', '2015-11-30 19:01:55'), 
(172, 45, 2, 'logout', 'index', 0, '', '2015-11-30 19:02:38'), 
(173, 38, 3, 'for_pre_approval', 'view', 9, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:08:49'), 
(174, 38, 3, 'for_pre_approval', 'view', 12, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:09:07'), 
(175, 1, 1, 'client', 'edit', 2, 'Client successfully updated!', '2015-11-30 19:10:36'), 
(176, 1, 1, 'client', 'edit', 2, 'Client successfully updated!', '2015-11-30 19:14:07'), 
(177, 38, 3, 'for_pre_approval', 'view', 15, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:15:03'), 
(178, 38, 3, 'for_pre_approval', 'view', 16, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:15:16'), 
(179, 38, 3, 'for_pre_approval', 'view', 17, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:16:10'), 
(180, 3, 4, 'for_approval', 'view', 9, 'Time Report succcessfully change to Approved', '2015-11-30 19:17:53'), 
(181, 3, 4, 'for_approval', 'index', 12, 'Time Report succcessfully change to Approved', '2015-11-30 19:18:22'), 
(182, 3, 4, 'for_approval', 'index', 15, 'Time Report succcessfully change to Approved', '2015-11-30 19:18:22'), 
(183, 3, 4, 'for_approval', 'index', 16, 'Time Report succcessfully change to Approved', '2015-11-30 19:18:22'), 
(184, 3, 4, 'for_approval', 'index', 17, 'Time Report succcessfully change to Approved', '2015-11-30 19:18:22'), 
(185, 1, 1, 'user', 'add', 47, 'Successfully added User Account!', '2015-11-30 19:33:44'), 
(186, 38, 3, 'logout', 'index', 0, '', '2015-11-30 19:34:27'), 
(187, 47, 3, 'logout', 'index', 0, '', '2015-11-30 19:35:40'), 
(188, 1, 1, 'user', 'add', 48, 'Successfully added User Account!', '2015-11-30 19:37:51'), 
(189, 48, 3, 'logout', 'index', 0, '', '2015-11-30 19:39:21'), 
(190, 1, 1, 'user', 'add', 49, 'Successfully added User Account!', '2015-11-30 19:41:14'), 
(191, 49, 3, 'logout', 'index', 0, '', '2015-11-30 19:42:35'), 
(192, 1, 1, 'project', 'add', 11, 'Successfully added project!', '2015-11-30 19:43:11'), 
(193, 1, 1, 'project', 'add', 12, 'Successfully added project!', '2015-11-30 19:43:32'), 
(194, 1, 1, 'project', 'add', 13, 'Successfully added project!', '2015-11-30 19:44:08'), 
(195, 1, 1, 'user', 'add', 50, 'Successfully added User Account!', '2015-11-30 20:00:42'), 
(196, 50, 2, 'logout', 'index', 0, '', '2015-11-30 20:02:31'), 
(197, 1, 1, 'user', 'add', 51, 'Successfully added User Account!', '2015-11-30 20:04:14'), 
(198, 51, 2, 'logout', 'index', 0, '', '2015-11-30 20:05:33'), 
(199, 1, 1, 'client_group', 'add', 2, 'Successfully added employee client!', '2015-11-30 20:07:22'), 
(200, 1, 1, 'user', 'add', 52, 'Successfully added User Account!', '2015-11-30 20:09:29'), 
(201, 51, 2, 'logout', 'index', 0, '', '2015-11-30 20:10:08'), 
(202, 51, 2, 'logout', 'index', 0, '', '2015-11-30 20:11:05'), 
(203, 52, 2, 'logout', 'index', 0, '', '2015-11-30 20:11:18'), 
(204, 1, 1, 'user', 'add', 53, 'Successfully added User Account!', '2015-11-30 20:12:25'), 
(205, 53, 2, 'logout', 'index', 0, '', '2015-11-30 20:13:58'), 
(206, 1, 1, 'user', 'add', 54, 'Successfully added User Account!', '2015-11-30 20:14:47'), 
(207, 54, 2, 'logout', 'index', 0, '', '2015-11-30 20:16:17'), 
(208, 46, 5, 'employee_list', 'add', 6, 'Successfully added employee rate.', '2015-11-30 20:22:39'), 
(209, 46, 5, 'employee_list', 'add', 7, 'Successfully added employee rate.', '2015-11-30 20:24:07'), 
(210, 46, 5, 'employee_list', 'add', 8, 'Successfully added employee rate.', '2015-11-30 20:25:20'), 
(211, 50, 2, 'timesheet', 'edit', 18, 'Timesheet Submitted', '2015-11-30 20:35:04'), 
(212, 50, 2, 'logout', 'index', 0, '', '2015-11-30 20:35:14'), 
(213, 51, 2, 'timesheet', 'edit', 19, 'Timesheet Submitted', '2015-11-30 20:49:48'), 
(214, 51, 2, 'logout', 'index', 0, '', '2015-11-30 20:50:10'), 
(215, 52, 2, 'timesheet', 'edit', 20, 'Timesheet Submitted', '2015-11-30 20:59:05'), 
(216, 52, 2, 'logout', 'index', 0, '', '2015-11-30 20:59:15'), 
(217, 53, 2, 'timesheet', 'edit', 21, 'Timesheet Submitted', '2015-11-30 21:23:09'), 
(218, 0, 0, 'logout', 'index', 0, '', '2015-11-30 21:23:53'), 
(219, 53, 2, 'logout', 'index', 0, '', '2015-11-30 21:24:51'), 
(220, 48, 3, 'for_pre_approval', 'view', 18, 'Time Report succcessfully change to Pre-approved', '2015-11-30 21:25:34'), 
(221, 48, 3, 'for_pre_approval', 'view', 19, 'Time Report succcessfully change to Pre-approved', '2015-11-30 21:25:44'), 
(222, 48, 3, 'for_pre_approval', 'view', 20, 'Time Report succcessfully change to Pre-approved', '2015-11-30 21:25:58'), 
(223, 48, 3, 'for_pre_approval', 'view', 21, 'Time Report succcessfully change to Pre-approved', '2015-11-30 21:26:04'), 
(224, 4, 4, 'for_approval', 'index', 18, 'Time Report succcessfully change to Approved', '2015-11-30 21:26:45'), 
(225, 4, 4, 'for_approval', 'index', 19, 'Time Report succcessfully change to Approved', '2015-11-30 21:26:45'), 
(226, 4, 4, 'for_approval', 'view', 20, 'Time Report succcessfully change to Approved', '2015-11-30 21:27:47'), 
(227, 4, 4, 'for_approval', 'view', 21, 'Time Report succcessfully change to Approved', '2015-11-30 21:28:03'), 
(228, 1, 1, 'user', 'add', 55, 'Successfully added User Account!', '2015-11-30 21:42:36'), 
(229, 48, 3, 'logout', 'index', 0, '', '2015-11-30 21:42:58'), 
(230, 55, 3, 'logout', 'index', 0, '', '2015-11-30 21:47:43'), 
(231, 1, 1, 'user', 'add', 56, 'Successfully added User Account!', '2015-11-30 21:49:10'), 
(232, 56, 3, 'logout', 'index', 0, '', '2015-11-30 21:50:37'), 
(233, 1, 1, 'user', 'add', 57, 'Successfully added User Account!', '2015-11-30 21:55:26'), 
(234, 57, 3, 'logout', 'index', 0, '', '2015-11-30 21:57:41'), 
(235, 1, 1, 'project', 'add', 14, 'Successfully added project!', '2015-11-30 21:58:54'), 
(236, 1, 1, 'project', 'add', 15, 'Successfully added project!', '2015-11-30 21:59:26'), 
(237, 1, 1, 'project', 'add', 16, 'Successfully added project!', '2015-11-30 22:00:24'), 
(238, 1, 1, 'client_group', 'add', 3, 'Successfully added employee client!', '2015-11-30 22:01:41'), 
(239, 1, 1, 'user', 'add', 58, 'Successfully added User Account!', '2015-11-30 22:05:03'), 
(240, 1, 1, 'user', 'add', 59, 'Successfully added User Account!', '2015-11-30 22:12:41'), 
(241, 58, 5, 'logout', 'index', 0, '', '2015-11-30 22:13:55'), 
(242, 59, 6, 'logout', 'index', 0, '', '2015-11-30 22:15:48'), 
(243, 1, 1, 'user', 'add', 60, 'Successfully added User Account!', '2015-11-30 22:18:04'), 
(244, 59, 6, 'logout', 'index', 0, '', '2015-11-30 22:18:40'), 
(245, 60, 2, 'logout', 'index', 0, '', '2015-11-30 22:19:43'), 
(246, 1, 1, 'user', 'add', 61, 'Successfully added User Account!', '2015-11-30 22:20:41'), 
(247, 61, 2, 'logout', 'index', 0, '', '2015-11-30 22:21:53'), 
(248, 1, 1, 'user', 'add', 62, 'Successfully added User Account!', '2015-11-30 22:23:36'), 
(249, 62, 2, 'logout', 'index', 0, '', '2015-11-30 22:24:55'), 
(250, 1, 1, 'user', 'add', 63, 'Successfully added User Account!', '2015-11-30 22:26:26'), 
(251, 63, 2, 'logout', 'index', 0, '', '2015-11-30 22:27:42'), 
(252, 1, 1, 'user', 'add', 64, 'Successfully added User Account!', '2015-11-30 22:28:24'), 
(253, 64, 2, 'logout', 'index', 0, '', '2015-11-30 22:29:34'), 
(254, 46, 5, 'employee_list', 'add', 9, 'Successfully added employee rate.', '2015-11-30 22:30:29'), 
(255, 46, 5, 'employee_list', 'add', 10, 'Successfully added employee rate.', '2015-11-30 22:31:40'), 
(256, 46, 5, 'employee_list', 'add', 11, 'Successfully added employee rate.', '2015-11-30 22:33:04'), 
(257, 60, 2, 'timesheet', 'edit', 22, 'Timesheet Submitted', '2015-11-30 22:41:43'), 
(258, 60, 2, 'logout', 'index', 0, '', '2015-11-30 22:42:04'), 
(259, 61, 2, 'timesheet', 'edit', 23, 'Timesheet Submitted', '2015-11-30 22:51:27'), 
(260, 61, 2, 'logout', 'index', 0, '', '2015-11-30 22:51:37'), 
(261, 62, 2, 'timesheet', 'edit', 24, 'Timesheet Submitted', '2015-11-30 22:59:41'), 
(262, 62, 2, 'logout', 'index', 0, '', '2015-11-30 22:59:51'), 
(263, 63, 2, 'timesheet', 'edit', 25, 'Timesheet Submitted', '2015-11-30 23:08:41'), 
(264, 63, 2, 'logout', 'index', 0, '', '2015-11-30 23:08:57'), 
(265, 4, 4, 'logout', 'index', 0, '', '2015-11-30 23:09:39'), 
(266, 56, 3, 'for_pre_approval', 'view', 22, 'Time Report succcessfully change to Pre-approved', '2015-11-30 23:10:08'), 
(267, 56, 3, 'for_pre_approval', 'view', 23, 'Time Report succcessfully change to Pre-approved', '2015-11-30 23:10:14'), 
(268, 56, 3, 'for_pre_approval', 'view', 24, 'Time Report succcessfully change to Pre-approved', '2015-11-30 23:10:29'), 
(269, 56, 3, 'for_pre_approval', 'view', 25, 'Time Report succcessfully change to Pre-approved', '2015-11-30 23:10:35'), 
(270, 5, 4, 'for_approval', 'view', 22, 'Time Report succcessfully change to Approved', '2015-11-30 23:11:12'), 
(271, 5, 4, 'for_approval', 'view', 23, 'Time Report succcessfully change to Approved', '2015-11-30 23:11:28'), 
(272, 5, 4, 'for_approval', 'view', 24, 'Time Report succcessfully change to Approved', '2015-11-30 23:11:48'), 
(273, 5, 4, 'for_approval', 'index', 25, 'Time Report succcessfully change to Approved', '2015-11-30 23:11:52'), 
(274, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-30 23:15:18'), 
(275, 1, 1, 'logout', 'index', 0, '', '2015-12-01 13:36:34'), 
(276, 3, 4, 'logout', 'index', 0, '', '2015-12-01 14:10:16'), 
(277, 46, 5, 'logout', 'index', 0, '', '2015-12-01 14:34:16'), 
(278, 1, 1, 'logout', 'index', 0, '', '2015-12-01 14:55:02'), 
(279, 1, 1, 'logout', 'index', 0, '', '2015-12-01 15:05:06'), 
(280, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:05:44'), 
(281, 14, 2, 'logout', 'index', 0, '', '2015-12-01 15:05:49'), 
(282, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 15:06:33'), 
(283, 7, 3, 'logout', 'index', 0, '', '2015-12-01 15:08:14'), 
(284, 14, 2, 'logout', 'index', 0, '', '2015-12-01 15:08:50'), 
(285, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 15:09:20'), 
(286, 7, 3, 'logout', 'index', 0, '', '2015-12-01 15:09:24'), 
(287, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:10:07'), 
(288, 14, 2, 'logout', 'index', 0, '', '2015-12-01 15:10:10'), 
(289, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 15:10:57'), 
(290, 7, 3, 'logout', 'index', 0, '', '2015-12-01 15:11:02'), 
(291, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:19:11'), 
(292, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 15:43:55'), 
(293, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:44:21'), 
(294, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 15:44:40'), 
(295, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 15:48:01'), 
(296, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:48:10'), 
(297, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 15:53:27'), 
(298, 7, 3, 'logout', 'index', 0, '', '2015-12-01 16:12:29'), 
(299, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 16:14:07'), 
(300, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 16:16:10'), 
(301, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 16:16:27'), 
(302, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 16:16:49'), 
(303, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 16:16:59'), 
(304, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 16:17:07'), 
(305, 7, 3, 'logout', 'index', 0, '', '2015-12-01 16:21:42'), 
(306, 2, 4, 'logout', 'index', 0, '', '2015-12-01 16:50:52'), 
(307, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 16:51:19'), 
(308, 7, 3, 'logout', 'index', 0, '', '2015-12-01 16:51:24'), 
(309, 1, 1, 'client', 'edit', 1, 'Client successfully updated!', '2015-12-01 16:51:44'), 
(310, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 16:51:52'), 
(311, 1, 1, 'logout', 'index', 0, '', '2015-12-01 16:52:02'), 
(312, 2, 4, 'for_approval', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 17:11:46'), 
(313, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 17:12:35'), 
(314, 2, 4, 'for_approval', 'view', 2, 'Time Report succcessfully change to Approved', '2015-12-01 17:12:48'), 
(315, 2, 4, 'logout', 'index', 0, '', '2015-12-01 17:13:12'), 
(316, 1, 1, 'client', 'edit', 1, 'Client successfully updated!', '2015-12-01 17:13:33'), 
(317, 1, 1, 'logout', 'index', 0, '', '2015-12-01 17:13:40'), 
(318, 14, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-12-01 17:14:04'), 
(319, 7, 3, 'for_pre_approval', 'view', 1, 'Time Report succcessfully change to Pre-approved', '2015-12-01 17:14:22'), 
(320, 7, 3, 'pre_approved', 'view', 1, 'Time Report succcessfully change to For re-work', '2015-12-01 17:14:38'), 
(321, 14, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-12-01 17:14:43'), 
(322, 7, 3, 'for_pre_approval', 'view', 1, 'Time Report succcessfully change to Pre-approved', '2015-12-01 17:14:51'), 
(323, 7, 3, 'logout', 'index', 0, '', '2015-12-01 17:14:55'), 
(324, 2, 4, 'for_approval', 'view', 1, 'Time Report succcessfully change to Approved', '2015-12-01 17:15:22'), 
(325, 0, 0, 'logout', 'index', 0, '', '2015-12-02 09:28:30'), 
(326, 1, 1, 'logout', 'index', 0, '', '2015-12-02 11:12:10'), 
(327, 1, 1, 'logout', 'index', 0, '', '2015-12-02 11:18:50'), 
(328, 1, 1, 'logout', 'index', 0, '', '2015-12-02 12:21:10'), 
(329, 0, 0, 'login', 'index', 0, '', '2015-12-02 12:21:29'), 
(330, 1, 1, 'appearance', 'index', 0, 'Change Site Title', '2015-12-02 12:28:46'), 
(331, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-12-02 12:31:49'), 
(332, 1, 1, 'user_profile', 'add', 2, 'Successfully added User Role!', '2015-12-02 12:42:59'), 
(333, 1, 1, 'user_profile', 'add', 3, 'Successfully added User Role!', '2015-12-02 12:43:13'), 
(334, 1, 1, 'user_profile', 'add', 4, 'Successfully added User Role!', '2015-12-02 12:43:30'), 
(335, 1, 1, 'user_profile', 'add', 5, 'Successfully added User Role!', '2015-12-02 12:44:07'), 
(336, 1, 1, 'user_profile', 'add', 6, 'Successfully added User Role!', '2015-12-02 12:44:26'), 
(337, 1, 1, 'user_profile', 'add', 7, 'Successfully added User Role!', '2015-12-02 12:44:43'), 
(338, 1, 1, 'user_profile', 'add', 8, 'Successfully added User Role!', '2015-12-02 12:44:59'), 
(339, 1, 1, 'user_profile', 'add', 9, 'Successfully added User Role!', '2015-12-02 12:45:13'), 
(340, 1, 1, 'user_profile', 'add', 10, 'Successfully added User Role!', '2015-12-02 12:47:43'), 
(341, 1, 1, 'user_profile', 'add', 11, 'Successfully added User Role!', '2015-12-02 12:47:53'), 
(342, 1, 1, 'logout', 'index', 0, '', '2015-12-02 14:57:15'), 
(343, 0, 0, 'login', 'index', 0, '', '2015-12-02 15:29:22'), 
(344, 1, 1, 'logout', 'index', 0, '', '2015-12-02 16:44:53'), 
(345, 0, 0, 'login', 'index', 0, '', '2015-12-02 16:45:01'), 
(346, 1, 1, 'logout', 'index', 0, '', '2015-12-02 17:46:23'), 
(347, 0, 0, 'login', 'index', 0, '', '2015-12-02 17:46:30'), 
(348, 14, 2, 'logout', 'index', 0, '', '2015-12-02 18:17:25'), 
(349, 0, 0, 'login', 'index', 0, '', '2015-12-03 09:37:07'), 
(350, 1, 1, 'logout', 'index', 0, '', '2015-12-03 11:40:24'), 
(351, 0, 0, 'login', 'index', 0, '', '2015-12-03 11:40:30'), 
(352, 1, 1, 'outlet', 'edit', 1, 'Successfully updated outlet', '2015-12-03 11:42:37'), 
(353, 1, 1, 'outlet', 'edit', 1, 'Successfully updated outlet', '2015-12-03 11:42:48'), 
(354, 1, 1, 'logout', 'index', 0, '', '2015-12-03 12:24:06'), 
(355, 0, 0, 'login', 'index', 0, '', '2015-12-03 12:24:12'), 
(356, 1, 1, 'user', 'add', 2, 'Successfully added User Account!', '2015-12-03 12:51:31'), 
(357, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-12-03 12:55:54'), 
(358, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-12-03 12:56:02'), 
(359, 1, 1, 'user', 'add', 3, 'Successfully added User Account!', '2015-12-03 12:59:36'), 
(360, 1, 1, 'user', 'edit', 3, 'Successfully updated User Account!', '2015-12-03 15:11:40'), 
(361, 1, 1, 'user', 'edit', 3, 'Successfully updated User Account!', '2015-12-03 15:11:51'), 
(362, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-12-03 15:46:19'), 
(363, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-12-03 15:46:26'), 
(364, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-12-03 15:50:27'), 
(365, 1, 1, 'logout', 'index', 0, '', '2015-12-04 10:28:40'), 
(366, 0, 0, 'login', 'index', 0, '', '2015-12-04 10:28:51'), 
(367, 1, 1, 'logout', 'index', 0, '', '2015-12-04 10:31:55'), 
(368, 0, 0, 'login', 'index', 0, '', '2015-12-04 10:32:25'), 
(369, 1, 1, 'logout', 'index', 0, '', '2015-12-04 10:32:49'), 
(370, 0, 0, 'login', 'index', 0, '', '2015-12-04 13:49:08');
#----------------------------
# Table structure for announcement
#----------------------------
CREATE TABLE `announcement` (
  `id_announcement` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_code` varchar(50) NOT NULL,
  `announcement` varchar(255) NOT NULL,
  `announcement_content` longtext NOT NULL,
  `announcement_visibility` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_announcement`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
#----------------------------
# No records for table announcement
#----------------------------

#----------------------------
# Table structure for announcement_visibility
#----------------------------
CREATE TABLE `announcement_visibility` (
  `id_announcement_visibility` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned NOT NULL,
  `user_role` int(11) NOT NULL,
  PRIMARY KEY (`id_announcement_visibility`),
  KEY `announcement_id` (`announcement_id`),
  KEY `user_role` (`user_role`),
  CONSTRAINT `fk_announcement_id` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id_announcement`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
#----------------------------
# No records for table announcement_visibility
#----------------------------

#----------------------------
# Table structure for civil_status
#----------------------------
CREATE TABLE `civil_status` (
  `id_civil_status` int(11) NOT NULL AUTO_INCREMENT,
  `civil_status_iso_code` varchar(3) NOT NULL,
  `civil_status_code` varchar(15) NOT NULL,
  `civil_status_status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_civil_status`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table civil_status
#----------------------------


insert  into civil_status values 
(1, 'S', 'Single', 1), 
(2, 'M', 'Married', 1), 
(3, 'D', 'Divorced', 1), 
(4, 'W', 'Widow/er', 1);
#----------------------------
# Table structure for commission
#----------------------------
CREATE TABLE `commission` (
  `id_commission` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `commission_code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `commission` varchar(255) CHARACTER SET utf8 NOT NULL,
  `commission_percentage` decimal(13,2) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_commission`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table commission
#----------------------------


insert  into commission values 
(1, '1234', 'Internal', '0.00', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(2, '2345', 'Ocean Disc', '23.00', 0, '0000-00-00 00:00:00', '2015-11-26 10:37:55'), 
(3, '123-567', 'Internal D', '0.00', 1, '0000-00-00 00:00:00', '2015-11-26 10:38:07'), 
(4, '123-567', 'External', '7.00', 1, '0000-00-00 00:00:00', '2015-11-26 10:31:31'), 
(5, '23333', 'External', '0.00', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(6, '1234545', 'External', '0.00', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(7, '1234545', 'External', '0.00', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(8, '1234545@!', 'External', '0.00', 1, '2015-11-25 16:56:15', '2015-11-25 16:56:53'), 
(9, '23-090', 'Mine Commi', '50.00', 1, '2015-11-26 10:18:58', '2015-11-26 10:18:58');
#----------------------------
# Table structure for configuration
#----------------------------
CREATE TABLE `configuration` (
  `id_configuration` int(11) NOT NULL AUTO_INCREMENT,
  `configuration` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `config_type` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id_configuration`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table configuration
#----------------------------


insert  into configuration values 
(1, 'EMAIL_PROTOCOL', 'mail', 1), 
(2, 'SMTP_DOMAIN', '', 1), 
(3, 'SMTP_HOST', '', 1), 
(4, 'SMTP_USER', '', 1), 
(5, 'SMTP_PASS', '', 1), 
(6, 'SMTP_PORT', '25', 1), 
(7, 'EMAIL_TYPE', 'html', 1), 
(8, 'DEBUG_MODE', '0', 1), 
(9, 'THEME', 'default', 1), 
(10, 'SITE_TITLE', 'Ocean Jet', 1), 
(11, 'CACHE_MODE', '0', 1), 
(12, 'CACHE_TIMEOUT', '2', 1), 
(13, 'DEFAULT_LANGUAGE', '1', 1), 
(14, 'ADMIN_PAGE_LOGO', '1', 1), 
(15, 'ADMIN_ITEMS', '100', 1), 
(16, 'SMTP_CC', '', 1), 
(17, 'COMP_LOGO', '', 1), 
(18, 'COMP_ADDR1', '', 1), 
(19, 'COMP_ADDR2', '', 1), 
(20, 'COMP_EMAIL', '', 1), 
(21, 'COMP_HEADER', '', 1), 
(22, 'COMP_FOOTER', '', 1), 
(23, 'COMP_HEADER', '', 1), 
(24, 'DEFAULT_COUNTRY', '', 1), 
(25, 'DEFAULT_TIME_ZONE', 'Asia/Manila', 1), 
(26, 'SMTP_BCC', '', 1), 
(27, 'UPLOAD_DIR', 'e:\\titanium', 1), 
(28, 'NUM_PASSWORD_CHANGE', '4', 1), 
(29, 'REQ_PASSWORD_CHANGE', '30', 1), 
(30, 'PASSWORD_REQ_UPPERCASE', '0', 1), 
(31, 'PASSWORD_REQ_NUMBER', '0', 1), 
(32, 'PASSWORD_REQ_SPL_CHAR', '1', 1), 
(33, 'LOGIN_ATTEMPT', '5', 1), 
(34, 'ENABLE_LOCKED_ACCT', '10', 1), 
(35, 'RE_AUTHENTICATE_ACCT', '60', 1), 
(36, 'INACTIVE_ACCT', '10', 1), 
(37, 'MIN_PASSWORD_CHAR', '5', 1), 
(38, 'MAX_PASSWORD_CHAR', '7', 1), 
(39, 'PASSWORD_REQ_LOWERCASE', '0', 1);
#----------------------------
# Table structure for discounts
#----------------------------
CREATE TABLE `discounts` (
  `id_discount` int(11) NOT NULL AUTO_INCREMENT,
  `discount_code` varchar(100) NOT NULL,
  `discount_name` varchar(100) NOT NULL,
  `discount_amount` decimal(12,2) NOT NULL,
  `discount_percentage` decimal(12,2) NOT NULL,
  `age_bracket` int(11) NOT NULL,
  `age_from` int(11) NOT NULL,
  `age_to` int(11) NOT NULL,
  `vat_exempt` int(11) NOT NULL,
  `enabled` int(11) NOT NULL,
  PRIMARY KEY (`id_discount`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#----------------------------
# No records for table discounts
#----------------------------

#----------------------------
# Table structure for eticket
#----------------------------
CREATE TABLE `eticket` (
  `id_eticket` int(11) NOT NULL AUTO_INCREMENT,
  `eticket_details` varchar(128) NOT NULL,
  `eticket_terms` varchar(3000) NOT NULL,
  `eticket_reminders` varchar(3000) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_eticket`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table eticket
#----------------------------


insert  into eticket values 
(1, 'Details', 'Terms', 'Reminders', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
#----------------------------
# Table structure for fare
#----------------------------
CREATE TABLE `fare` (
  `id_fare` int(11) NOT NULL AUTO_INCREMENT,
  `fare_code` varchar(24) NOT NULL,
  `trip_type` int(11) NOT NULL,
  PRIMARY KEY (`id_fare`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table fare
#----------------------------


insert  into fare values 
(1, 'F1', 1);
#----------------------------
# Table structure for month
#----------------------------
CREATE TABLE `month` (
  `id_month` int(11) NOT NULL,
  `month` varchar(128) NOT NULL,
  PRIMARY KEY (`id_month`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#----------------------------
# Records for table month
#----------------------------


insert  into `month` values 
(1, 'January'), 
(2, 'February'), 
(3, 'March'), 
(4, 'April'), 
(5, 'May'), 
(6, 'June'), 
(7, 'July'), 
(8, 'August'), 
(9, 'September'), 
(10, 'October'), 
(11, 'November'), 
(12, 'December');
#----------------------------
# Table structure for outlet
#----------------------------
CREATE TABLE `outlet` (
  `id_outlet` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `outlet_code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `outlet_type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `outlet` varchar(255) CHARACTER SET utf8 NOT NULL,
  `outlet_address` longtext CHARACTER SET utf8 NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_outlet`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table outlet
#----------------------------


insert  into outlet values 
(1, '1234', 'Internal', 'SM Megamall', '#123 Manila Street', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(2, '2345', 'External', 'Ayala Mall', '#123 Manila Street', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(3, '123-567', 'Internal', 'Test 1', '#123 Manila Street', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(4, '123-567', 'External', 'Test 1', '#123 Manila Street', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(5, '23333', 'Internal', 'Test 2', '#123 Manila Street', 0, '0000-00-00 00:00:00', '2015-11-26 11:07:17'), 
(6, '1234545', 'External', 'Test 3', '#123 Manila Street', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(7, '1234545', 'External', 'Test 4', '#123 Manila Street', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'), 
(8, '1234545@!', 'External', 'Test 6', '#123 Manila Street1', 0, '2015-11-25 16:56:15', '2015-11-26 11:07:40'), 
(9, 'Test g', 'Internal', 'Test 8', '#123 Manila Street', 1, '2015-11-26 11:02:51', '2015-11-26 11:06:41');
#----------------------------
# Table structure for payment_account
#----------------------------
CREATE TABLE `payment_account` (
  `id_payment_account` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `payment_account` varchar(50) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `bank_account_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_payment_account`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
#----------------------------
# Records for table payment_account
#----------------------------


insert  into payment_account values 
(1, '1234', 'Maybank', '0.00', '0', 1, '0000-00-00 00:00:00', '2015-11-26 13:54:22'), 
(2, '2345', 'Ocean Discount', '23.00', '0', 0, '2015-11-26 10:45:34', '0000-00-00 00:00:00'), 
(3, '123-567', 'Internal D', '0.00', '1', 0, '2015-11-26 10:38:07', '0000-00-00 00:00:00'), 
(8, '1234545@!', 'External', '0.00', '1', 127, '2015-11-25 16:56:53', '0000-00-00 00:00:00'), 
(9, '23-090', 'BDO', '392-BDO-44', '340-IOD-89437', 1, '2015-11-26 10:45:45', '2015-11-26 13:51:22'), 
(10, '456', 'Bank of the Philippine Islands', '392.00', '340-IOD-89437', 0, '2015-11-26 13:46:49', '2015-11-26 13:46:49'), 
(11, '8439-2832704', 'Security Bank', '392-Security-44', '8472-438-234', 0, '2015-11-26 13:48:09', '2015-11-26 13:48:09');
#----------------------------
# Table structure for port
#----------------------------
CREATE TABLE `port` (
  `id_port` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `port_code` varchar(50) NOT NULL,
  `port` varchar(255) NOT NULL,
  `terminal_fee` decimal(13,2) unsigned NOT NULL,
  `port_charge` decimal(13,2) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_port`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#----------------------------
# No records for table port
#----------------------------

#----------------------------
# Table structure for trip_type
#----------------------------
CREATE TABLE `trip_type` (
  `id_trip_type` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(24) NOT NULL,
  PRIMARY KEY (`id_trip_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table trip_type
#----------------------------


insert  into trip_type values 
(1, 'One way'), 
(2, 'Round Trip'), 
(3, 'Multi-leg');
#----------------------------
# Table structure for user
#----------------------------
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_profile_id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `secure_token` varchar(128) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_failed_login` datetime DEFAULT NULL,
  `num_failed_login` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `is_new` tinyint(1) NOT NULL,
  `password_generated` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table user
#----------------------------


insert  into `user` values 
(1, 1, 'admin', 'ed1fd2fc25c6646d9c55bf4aa283477cca8a6f4e9b8af58a39529f1775210690', 'dba7f4dee86fd3c2b906065e85bd2a', 'YZBQ1JB8', '2015-12-04 13:49:08', '2015-12-04 13:49:08', 0, 1, 0, 0, 0, '2015-11-29'), 
(2, 2, 'administrator', '2fc042a2575272bb40d7471ece818a2705fb38a39888666d50fd5982246de790', '4d511b6ac3713cb842ca3cec7c01de', 'TIY2DLZ5', '2015-12-03 12:51:31', '1970-01-01 00:00:00', 0, 1, 0, 0, 1, '2015-12-03'), 
(3, 4, 'sales', '1a6021abd28a47b5647394b7a495a9a06db1409947a6824cd2010b98330e2d71', 'ca19f8f24b11ca7c6d161ee4308703', '5L3YSE6D', '2015-12-03 12:59:35', '1970-01-01 00:00:00', 0, 1, 0, 0, 1, '2015-12-03');
#----------------------------
# Table structure for user_account
#----------------------------
CREATE TABLE `user_account` (
  `id_user_account` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email_address` varchar(128) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `middlename` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) NOT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user_account`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table user_account
#----------------------------


insert  into user_account values 
(1, 1, 'superadmin@yondu.com', 'Super Admin', 'Super Admin', 'Super Admin', 0), 
(2, 2, 'admin@yondu.com', 'admin', 'admin', 'admin', 0), 
(3, 3, 'sales@yondu.com', 'sales', 'sales', 'sales', 1);
#----------------------------
# Table structure for user_login
#----------------------------
CREATE TABLE `user_login` (
  `user_id` int(11) NOT NULL,
  `token` varchar(128) NOT NULL,
  `salt` varchar(64) NOT NULL,
  `date_added` date NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#----------------------------
# No records for table user_login
#----------------------------

#----------------------------
# Table structure for user_profile
#----------------------------
CREATE TABLE `user_profile` (
  `id_user_profile` int(11) NOT NULL AUTO_INCREMENT,
  `user_profile` varchar(128) NOT NULL,
  `user_description` varchar(256) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_user_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table user_profile
#----------------------------


insert  into user_profile values 
(1, 'Super Administrator', 'Super Administrator', 1), 
(2, 'Admin', 'Admin', 1), 
(3, 'Operation Manager', 'Operation Manager', 1), 
(4, 'Sales Agent', 'Sales Agent', 1), 
(5, 'Third Party Sales Agent', 'Third Party Sales Agent', 1), 
(6, 'Check - In Clerk', 'Check - In Clerk', 1), 
(7, 'Baggage Clerk', 'Baggage Clerk', 1), 
(8, 'Baggage Cashier', 'Baggage Cashier', 1), 
(9, 'Reservation Officer', 'Reservation Officer', 1), 
(10, 'Port Supervisors', 'Port Supervisors', 1), 
(11, 'Debtor', 'Debtor', 1);
#----------------------------
# Table structure for vessel
#----------------------------
CREATE TABLE `vessel` (
  `id_vessel` int(11) NOT NULL AUTO_INCREMENT,
  `vessel_code` varchar(36) NOT NULL,
  `vessel` varchar(128) NOT NULL,
  `total_seats` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_vessel`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table vessel
#----------------------------


insert  into vessel values 
(1, 'V1', 'Vessel 1', 120, 1), 
(2, 'V2', 'Vessel 2', 200, 1);
#----------------------------
# Table structure for voyage
#----------------------------
CREATE TABLE `voyage` (
  `id_voyage` int(11) NOT NULL AUTO_INCREMENT,
  `voyage` varchar(24) NOT NULL,
  `vessel_id` int(11) NOT NULL,
  `ETD` datetime NOT NULL,
  `ETA` datetime NOT NULL,
  `status` int(1) NOT NULL,
  `accommodation_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `origin` int(11) NOT NULL,
  `destination` int(11) NOT NULL,
  `fare_id` int(11) NOT NULL,
  PRIMARY KEY (`id_voyage`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table voyage
#----------------------------


insert  into voyage values 
(1, '00001', 1, '2015-12-02 17:01:29', '2015-12-03 17:01:32', 1, 5, 0, 1, 2, 0), 
(2, '00002', 1, '2015-12-02 11:00:01', '2015-12-03 12:00:01', 0, 5, 1, 2, 1, 1), 
(3, '00003', 2, '2015-12-02 11:00:00', '2015-12-03 12:00:00', 0, 5, 1, 1, 2, 1);
#----------------------------
# Table structure for rule_set
#----------------------------
CREATE TABLE `rule_set` (
  `id_rule_set` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rule_code` varchar(50) NOT NULL,
  `rule_set` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_rule_set`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
#----------------------------
# No records for table rule_set
#----------------------------

#----------------------------
# Table structure for rule_type
#----------------------------
CREATE TABLE `rule_type` (
  `id_rule_type` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rule_set_id` int(11) NOT NULL,
  `rule_type_name_id` int(11) NOT NULL,
  `rule_type_amount` decimal(13,2) NOT NULL,
  `rule_type_percentage` decimal(13,2) NOT NULL,
  `rule_type_weight` decimal(13,2) NOT NULL,
  PRIMARY KEY (`id_rule_type`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
#----------------------------
# No records for table rule_type
#----------------------------

#----------------------------
# Table structure for rule_type_name
#----------------------------
CREATE TABLE `rule_type_name` (
  `id_rule_type_name` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rule_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_rule_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
#----------------------------
# Records for table rule_type_name
#----------------------------


insert  into rule_type_name values 
(38, 'Re-schedule Tickets Not Yet Checked In - Before Departure'), 
(39, 'Re-schedule Tickets Not Yet Checked In - After Departure'), 
(40, 'Re-schedule Tickets Already Checked In - Before Departure'), 
(41, 'Re-schedule Tickets Already Checked In - After Departure'), 
(42, 'Re-schedule Ticket for Cancelled Voyage'), 
(43, 'Refund Tickets Not Yet Checked In - Before Departure'), 
(44, 'Refund Tickets Not Yet Checked In - After Departure'), 
(45, 'Refund Tickets Already Checked In - Before Departure'), 
(46, 'Refund Tickets Already Checked In - After Departure'), 
(47, 'Refund Ticket for Cancelled Voyage'), 
(48, 'Upgrade Ticket from Open Air to Tourist Class'), 
(49, 'Upgrade Ticket from Tourist Class to Business Class'), 
(50, 'Upgrade Ticket from Open Air to Business Class'), 
(51, 'Free Baggage Allowance');
#----------------------------
# Table structure for debtor
#----------------------------
CREATE TABLE `debtor` (
  `id_debtor` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_code` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `commission_id` int(11) NOT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `enabled` int(11) NOT NULL,
  PRIMARY KEY (`id_debtor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
#----------------------------
# No records for table debtor
#----------------------------

#----------------------------
# Table structure for debtor_wallet
#----------------------------
CREATE TABLE `debtor_wallet` (
  `id_debtor_wallet` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_id` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `payment_account_id` int(11) NOT NULL,
  `deposit_date` datetime NOT NULL,
  `reference` text NOT NULL,
  PRIMARY KEY (`id_debtor_wallet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#----------------------------
# No records for table debtor_wallet
#----------------------------

#----------------------------
# Table structure for default_fare
#----------------------------
CREATE TABLE `default_fare` (
  `voyage_id` int(11) NOT NULL,
  `accommodation_id` int(11) NOT NULL,
  `fare_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#----------------------------
# Records for table default_fare
#----------------------------


insert  into default_fare values 
(1, 1, 2), 
(1, 2, 1), 
(2, 1, 2);
#----------------------------
# Table structure for passenger
#----------------------------
CREATE TABLE `passenger` (
  `id_passenger` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(64) NOT NULL,
  `middlename` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) NOT NULL,
  `birthdate` date NOT NULL,
  `age` varchar(64) DEFAULT NULL,
  `gender` varchar(32) NOT NULL,
  `contactno` varchar(32) DEFAULT NULL,
  `id_no` varchar(64) DEFAULT NULL,
  `nationality` varchar(64) DEFAULT NULL,
  `with_infant` tinyint(1) DEFAULT NULL,
  `infant_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_passenger`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#----------------------------
# No records for table passenger
#----------------------------

#----------------------------
# Table structure for reset_password
#----------------------------
CREATE TABLE `reset_password` (
  `id_reset_password` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `date_added` varchar(64) NOT NULL,
  `exp_date` varchar(64) NOT NULL,
  PRIMARY KEY (`id_reset_password`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table reset_password
#----------------------------


insert  into reset_password values 
(1, 2, '2fc042a2575272bb40d7471ece818a2705fb38a39888666d50fd5982246de790', '1449118260', '1449204660'), 
(2, 3, '1a6021abd28a47b5647394b7a495a9a06db1409947a6824cd2010b98330e2d71', '1449118740', '1449205140');
#----------------------------
# Table structure for ticket
#----------------------------
CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL AUTO_INCREMENT,
  `trip_type_id` int(11) NOT NULL,
  `leg_id` int(11) NOT NULL,
  `departure_date` datetime NOT NULL,
  `voyage_id` int(11) NOT NULL,
  `vessel_id` int(11) NOT NULL,
  `accommodation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_ticket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#----------------------------
# No records for table ticket
#----------------------------

#----------------------------
# Table structure for user_password
#----------------------------
CREATE TABLE `user_password` (
  `id_user_password` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(32) NOT NULL,
  PRIMARY KEY (`id_user_password`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
#----------------------------
# Records for table user_password
#----------------------------


insert  into user_password values 
(1, 1, 'ba9400f68feb8bddfed7f8dbb58871085069056876b812427a6e77abf8d85891', '6Z6DNBJD'), 
(2, 1, 'ed1fd2fc25c6646d9c55bf4aa283477cca8a6f4e9b8af58a39529f1775210690', 'YZBQ1JB8'), 
(3, 2, '2fc042a2575272bb40d7471ece818a2705fb38a39888666d50fd5982246de790', 'TIY2DLZ5'), 
(4, 3, '1a6021abd28a47b5647394b7a495a9a06db1409947a6824cd2010b98330e2d71', '5L3YSE6D');
#----------------------------
# Table structure for user_type
#----------------------------
CREATE TABLE `user_type` (
  `id_user_type` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(128) NOT NULL,
  PRIMARY KEY (`id_user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#----------------------------
# No records for table user_type
#----------------------------


