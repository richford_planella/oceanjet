-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2015 at 03:21 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oceanjet_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator_access`
--

CREATE TABLE IF NOT EXISTS `administrator_access` (
  `user_profile_id` int(10) unsigned NOT NULL,
  `page_id` int(11) unsigned NOT NULL,
  `access` tinyint(1) NOT NULL DEFAULT '0',
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `add` tinyint(1) NOT NULL DEFAULT '0',
  `edit` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  `approve` tinyint(4) NOT NULL DEFAULT '0',
  `verify` tinyint(4) NOT NULL DEFAULT '0',
  `print` tinyint(4) NOT NULL DEFAULT '0',
  `request` tinyint(4) NOT NULL DEFAULT '0',
  `close` tinyint(4) NOT NULL DEFAULT '0',
  `received` tinyint(4) NOT NULL DEFAULT '0',
  `allocate` tinyint(4) NOT NULL DEFAULT '0',
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  `reactivate` tinyint(1) NOT NULL DEFAULT '0',
  `export` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator_access`
--

INSERT INTO `administrator_access` (`user_profile_id`, `page_id`, `access`, `view`, `add`, `edit`, `delete`, `approve`, `verify`, `print`, `request`, `close`, `received`, `allocate`, `activate`, `reactivate`, `export`) VALUES
(1, 2, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 52, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 54, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 131, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 132, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 9, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 10, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 41, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 53, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 5, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 43, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 7, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 3, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 51, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 6, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 8, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 13, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 59, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 42, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 58, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 4, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 11, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 55, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(1, 12, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(2, 132, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 131, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 3, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1),
(2, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 6, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(3, 7, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(3, 8, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(3, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 10, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(3, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1),
(3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 131, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 132, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 132, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 131, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1),
(4, 10, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(4, 9, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(4, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 132, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 131, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(5, 13, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 12, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(5, 11, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(5, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 43, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(5, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1),
(5, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(1, 133, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 132, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 131, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 13, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 58, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 59, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 55, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 54, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 53, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 52, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 51, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 5, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 42, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 41, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 4, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 2, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
(6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `administrator_log`
--

CREATE TABLE IF NOT EXISTS `administrator_log` (
`id_administrator_log` int(11) NOT NULL,
  `administrator_id` int(11) NOT NULL,
  `administrator_profile_id` int(11) NOT NULL,
  `classname` varchar(128) NOT NULL,
  `action` varchar(128) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=361 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator_log`
--

INSERT INTO `administrator_log` (`id_administrator_log`, `administrator_id`, `administrator_profile_id`, `classname`, `action`, `reference_id`, `description`, `date_added`) VALUES
(1, 1, 1, 'user', 'add', 2, 'Successfully added user account.', '2015-10-23 17:07:49'),
(2, 2, 6, 'user', 'add', 3, 'Successfully added user account.', '2015-10-23 20:19:41'),
(3, 2, 6, 'user', 'add', 4, 'Successfully added user account.', '2015-10-23 20:23:34'),
(4, 2, 6, 'user', 'add', 5, 'Successfully added user account.', '2015-10-23 20:25:09'),
(5, 2, 6, 'user', 'add', 6, 'Successfully added user account.', '2015-10-23 20:26:29'),
(6, 2, 6, 'user', 'add', 7, 'Successfully added user account.', '2015-10-23 21:13:47'),
(7, 2, 6, 'user', 'add', 8, 'Successfully added user account.', '2015-10-25 16:47:13'),
(8, 2, 6, 'client', 'add', 1, 'Successfully added client.', '2015-10-25 20:27:47'),
(9, 2, 6, 'client', 'edit', 1, 'Client successfully updated.', '2015-10-25 20:40:10'),
(10, 2, 6, 'user', 'add', 9, 'Successfully added user account.', '2015-10-25 20:46:29'),
(11, 2, 6, 'project', 'add', 1, 'Successfully added project.', '2015-10-25 20:55:43'),
(12, 2, 6, 'project', 'edit', 1, 'Project successfully updated.', '2015-10-25 20:56:12'),
(13, 2, 6, 'project', 'edit', 1, 'Project successfully updated.', '2015-10-25 20:56:39'),
(14, 2, 6, 'project', 'add', 2, 'Successfully added project.', '2015-10-25 20:59:59'),
(15, 2, 6, 'project', 'add', 3, 'Successfully added project.', '2015-10-25 21:00:14'),
(16, 2, 6, 'user', 'add', 10, 'Successfully added user account.', '2015-10-25 21:02:53'),
(17, 2, 6, 'client_group', 'add', 1, 'Successfully created employee group.', '2015-10-25 21:11:09'),
(18, 2, 6, 'client_group', 'add', 2, 'Successfully created employee group.', '2015-10-25 21:13:49'),
(19, 2, 6, 'user', 'add', 11, 'Successfully added user account.', '2015-10-25 23:06:20'),
(20, 2, 6, 'user', 'edit', 4, 'User successfully updated.', '2015-10-25 23:15:35'),
(21, 2, 6, 'user', 'edit', 4, 'User successfully updated.', '2015-10-25 23:16:53'),
(22, 2, 6, 'user', 'edit', 5, 'User successfully updated.', '2015-10-25 23:22:50'),
(23, 2, 6, 'user', 'edit', 7, 'User successfully updated.', '2015-10-25 23:25:52'),
(24, 11, 5, 'employee_list', 'add', 1, 'Successfully added employee rate.', '2015-10-25 23:26:47'),
(25, 11, 5, 'employee_list', 'add', 2, 'Successfully added employee rate.', '2015-10-25 23:27:30'),
(26, 11, 5, 'employee_list', 'add', 3, 'Successfully added employee rate.', '2015-10-25 23:27:57'),
(27, 11, 5, 'employee_list', 'add', 4, 'Successfully added employee rate.', '2015-10-25 23:28:49'),
(28, 11, 5, 'employee_list', 'add', 5, 'Successfully added employee rate.', '2015-10-25 23:29:05'),
(29, 11, 5, 'employee_list', 'add', 6, 'Successfully added employee rate.', '2015-10-25 23:29:31'),
(30, 11, 5, 'employee_list', 'add', 7, 'Successfully added employee rate.', '2015-10-25 23:30:06'),
(31, 11, 5, 'employee_list', 'add', 8, 'Successfully added employee rate.', '2015-10-25 23:30:23'),
(32, 11, 5, 'employee_list', 'add', 9, 'Successfully added employee rate.', '2015-10-25 23:30:52'),
(33, 2, 6, 'user', 'add', 12, 'Successfully added user account.', '2015-10-26 09:25:02'),
(34, 2, 6, 'client', 'add', 2, 'Successfully added client.', '2015-10-26 09:36:11'),
(35, 2, 6, 'client', 'add', 3, 'Successfully added client.', '2015-10-26 09:51:12'),
(36, 2, 6, 'client', 'add', 4, 'Successfully added client.', '2015-10-26 09:53:03'),
(37, 2, 6, 'client', 'add', 5, 'Successfully added client.', '2015-10-26 09:53:38'),
(38, 2, 6, 'client', 'add', 6, 'Successfully added client.', '2015-10-26 09:54:08'),
(39, 2, 6, 'client', 'add', 7, 'Successfully added client.', '2015-10-26 09:55:08'),
(40, 2, 6, 'client', 'add', 8, 'Successfully added client.', '2015-10-26 09:56:45'),
(41, 2, 6, 'client', 'add', 9, 'Successfully added client.', '2015-10-26 09:57:27'),
(42, 2, 6, 'client', 'add', 10, 'Successfully added client.', '2015-10-26 09:57:49'),
(43, 2, 6, 'user', 'add', 13, 'Successfully added user account.', '2015-10-26 10:15:54'),
(44, 2, 6, 'user', 'add', 14, 'Successfully added user account.', '2015-10-26 10:17:52'),
(45, 2, 6, 'user', 'add', 15, 'Successfully added user account.', '2015-10-26 10:21:24'),
(46, 2, 6, 'user', 'add', 16, 'Successfully added user account.', '2015-10-26 10:24:50'),
(47, 11, 5, 'employee_list', 'add', 10, 'Successfully added employee rate.', '2015-10-26 10:30:56'),
(48, 4, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-10-26 14:04:48'),
(49, 4, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-10-26 14:13:12'),
(50, 4, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-10-26 14:17:32'),
(51, 2, 6, 'client', 'edit', 1, 'Client successfully updated.', '2015-10-26 14:18:14'),
(52, 4, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-10-26 14:18:36'),
(53, 4, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-10-26 14:20:03'),
(54, 2, 6, 'client', 'edit', 1, 'Client successfully updated.', '2015-10-26 14:21:19'),
(55, 4, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-10-26 14:21:53'),
(56, 2, 6, 'user_profile', 'edit', 3, 'User profile successfully updated.', '2015-10-26 14:25:16'),
(57, 2, 6, 'user_profile', 'edit', 3, 'User profile successfully updated.', '2015-10-26 14:25:35'),
(58, 9, 3, 'for_pre_approval', 'view', 1, 'Time Report succcessfully change to Pre-approved', '2015-10-26 14:27:50'),
(59, 2, 6, 'user_profile', 'edit', 4, 'User profile successfully updated.', '2015-10-26 14:30:54'),
(60, 3, 4, 'for_approval', 'view', 1, 'Time Report succcessfully change to Approved', '2015-10-26 14:31:53'),
(61, 2, 6, 'user_profile', 'edit', 5, 'User profile successfully updated.', '2015-10-26 14:43:24'),
(62, 12, 6, 'user_profile', 'edit', 6, 'User profile successfully updated.', '2015-10-26 16:51:51'),
(63, 12, 6, 'user', 'add', 17, 'Successfully added user account.', '2015-10-26 17:01:54'),
(64, 12, 6, 'user', 'add', 18, 'Successfully added user account.', '2015-10-26 17:20:56'),
(65, 12, 6, 'user', 'add', 19, 'Successfully added user account.', '2015-10-26 17:23:58'),
(66, 14, 5, 'employee_list', 'add', 11, 'Successfully added employee rate.', '2015-10-26 17:25:09'),
(67, 12, 6, 'user_profile', 'edit', 6, 'User profile successfully updated.', '2015-10-26 17:25:58'),
(68, 5, 2, 'timesheet', 'edit', 4, 'Timesheet Submitted', '2015-10-26 17:44:33'),
(69, 9, 3, 'for_pre_approval', 'view', 4, 'Time Report succcessfully change to Pre-approved', '2015-10-26 17:51:12'),
(70, 3, 4, 'for_approval', 'view', 4, 'Time Report succcessfully change to Approved', '2015-10-26 17:56:31'),
(71, 9, 3, 'logout', 'index', 0, '', '2015-10-26 19:35:02'),
(72, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-26 19:36:07'),
(73, 5, 2, 'logout', 'index', 0, '', '2015-10-26 19:36:11'),
(74, 9, 3, 'logout', 'index', 0, '', '2015-10-26 19:49:55'),
(75, 1, 1, 'client', 'edit', 1, 'Client successfully updated.', '2015-10-26 19:50:33'),
(76, 1, 1, 'logout', 'index', 0, '', '2015-10-26 19:50:35'),
(77, 3, 4, 'logout', 'index', 0, '', '2015-10-26 19:55:57'),
(78, 1, 1, 'client', 'edit', 1, 'Client successfully updated.', '2015-10-26 19:56:11'),
(79, 1, 1, 'logout', 'index', 0, '', '2015-10-26 19:56:14'),
(80, 9, 3, 'for_pre_approval', 'view', 3, 'Time Report succcessfully change to Pre-approved', '2015-10-26 19:56:35'),
(81, 9, 3, 'logout', 'index', 0, '', '2015-10-26 19:56:38'),
(82, 3, 4, 'for_approval', 'view', 3, 'Time Report succcessfully change to Approved', '2015-10-26 19:56:57'),
(83, 3, 4, 'logout', 'index', 0, '', '2015-10-26 20:02:47'),
(84, 1, 1, 'user_profile', 'edit', 1, 'User profile successfully updated.', '2015-10-26 20:13:03'),
(85, 1, 1, 'logout', 'index', 0, '', '2015-10-26 20:39:10'),
(86, 4, 2, 'logout', 'index', 0, '', '2015-10-26 20:41:29'),
(87, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-26 22:15:54'),
(88, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-26 22:17:43'),
(89, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-26 22:19:06'),
(90, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-26 22:21:19'),
(91, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-26 22:21:50'),
(92, 5, 2, 'logout', 'index', 0, '', '2015-10-26 23:09:55'),
(93, 2, 6, 'user', 'edit', 5, 'User successfully updated.', '2015-10-26 23:35:29'),
(94, 2, 6, 'user', 'edit', 10, 'User successfully updated.', '2015-10-26 23:36:59'),
(95, 2, 6, 'project', 'add', 4, 'Successfully added project.', '2015-10-26 23:37:52'),
(96, 2, 6, 'client_group', 'add', 3, 'Successfully created employee group.', '2015-10-26 23:39:42'),
(97, 2, 6, 'user', 'edit', 7, 'User successfully updated.', '2015-10-26 23:42:17'),
(98, 2, 6, 'user', 'add', 20, 'Successfully added user account.', '2015-10-26 23:53:04'),
(99, 2, 6, 'user', 'edit', 20, 'User successfully updated.', '2015-10-26 23:53:53'),
(100, 11, 5, 'logout', 'index', 0, '', '2015-10-26 23:54:23'),
(101, 20, 2, 'logout', 'index', 0, '', '2015-10-26 23:55:36'),
(102, 2, 6, 'user', 'add', 21, 'Successfully added user account.', '2015-10-26 23:57:15'),
(103, 21, 2, 'logout', 'index', 0, '', '2015-10-26 23:58:38'),
(104, 2, 6, 'user', 'add', 22, 'Successfully added user account.', '2015-10-27 00:00:29'),
(105, 22, 2, 'logout', 'index', 0, '', '2015-10-27 00:03:38'),
(106, 2, 6, 'user', 'add', 23, 'Successfully added user account.', '2015-10-27 00:05:19'),
(107, 23, 2, 'logout', 'index', 0, '', '2015-10-27 00:08:20'),
(108, 2, 6, 'user', 'add', 24, 'Successfully added user account.', '2015-10-27 00:09:54'),
(109, 2, 6, 'logout', 'index', 0, '', '2015-10-27 00:10:04'),
(110, 24, 2, 'logout', 'index', 0, '', '2015-10-27 00:11:17'),
(111, 2, 6, 'logout', 'index', 0, '', '2015-10-27 00:12:37'),
(112, 2, 6, 'user', 'add', 25, 'Successfully added user account.', '2015-10-27 00:14:39'),
(113, 2, 6, 'logout', 'index', 0, '', '2015-10-27 00:15:01'),
(114, 25, 2, 'logout', 'index', 0, '', '2015-10-27 00:15:56'),
(115, 2, 6, 'logout', 'index', 0, '', '2015-10-27 00:16:02'),
(116, 11, 5, 'employee_list', 'add', 12, 'Successfully added employee rate.', '2015-10-27 00:18:16'),
(117, 11, 5, 'employee_list', 'add', 13, 'Successfully added employee rate.', '2015-10-27 00:18:35'),
(118, 11, 5, 'employee_list', 'add', 14, 'Successfully added employee rate.', '2015-10-27 00:19:30'),
(119, 11, 5, 'employee_list', 'add', 15, 'Successfully added employee rate.', '2015-10-27 00:20:40'),
(120, 11, 5, 'employee_list', 'add', 16, 'Successfully added employee rate.', '2015-10-27 00:21:27'),
(121, 11, 5, 'logout', 'index', 0, '', '2015-10-27 00:31:58'),
(122, 22, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-10-27 04:32:34'),
(123, 11, 5, 'logout', 'index', 0, '', '2015-10-27 04:34:35'),
(124, 22, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-10-27 04:36:14'),
(125, 9, 3, 'for_pre_approval', 'view', 1, 'Time Report succcessfully change to Pre-approved', '2015-10-27 04:39:19'),
(126, 9, 3, 'logout', 'index', 0, '', '2015-10-27 04:39:30'),
(127, 3, 4, 'for_approval', 'view', 1, 'Time Report succcessfully change to Approved', '2015-10-27 04:40:20'),
(128, 3, 4, 'logout', 'index', 0, '', '2015-10-27 04:40:31'),
(129, 11, 5, 'employee_list', 'add', 17, 'Successfully added employee rate.', '2015-10-27 04:44:19'),
(130, 2, 6, 'user', 'edit', 20, 'User successfully updated.', '2015-10-27 04:51:28'),
(131, 5, 2, 'logout', 'index', 0, '', '2015-10-27 08:59:24'),
(132, 12, 6, 'logout', 'index', 0, '', '2015-10-27 09:06:28'),
(133, 5, 2, 'logout', 'index', 0, '', '2015-10-27 09:07:39'),
(134, 12, 6, 'logout', 'index', 0, '', '2015-10-27 09:10:28'),
(135, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-27 09:11:40'),
(136, 9, 3, 'for_pre_approval', 'view', 3, 'Time Report succcessfully change to Pre-approved', '2015-10-27 09:14:24'),
(137, 9, 3, 'pre_approved', 'view', 3, 'Time Report succcessfully change to For re-work', '2015-10-27 09:21:43'),
(138, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-27 09:32:26'),
(139, 5, 2, 'logout', 'index', 0, '', '2015-10-27 09:38:55'),
(140, 9, 3, 'for_pre_approval', 'view', 3, 'Time Report succcessfully change to Pre-approved', '2015-10-27 09:42:45'),
(141, 9, 3, 'logout', 'index', 0, '', '2015-10-27 09:42:50'),
(142, 0, 0, 'logout', 'index', 0, '', '2015-10-27 09:48:15'),
(143, 9, 3, 'pre_approved', 'view', 3, 'Time Report succcessfully change to For re-work', '2015-10-27 09:53:33'),
(144, 3, 4, 'logout', 'index', 0, '', '2015-10-27 09:56:23'),
(145, 12, 6, 'logout', 'index', 0, '', '2015-10-27 09:58:04'),
(146, 3, 4, 'logout', 'index', 0, '', '2015-10-27 09:58:21'),
(147, 5, 2, 'timesheet', 'edit', 3, 'Timesheet Submitted', '2015-10-27 10:05:18'),
(148, 5, 2, 'logout', 'index', 0, '', '2015-10-27 10:05:22'),
(149, 9, 3, 'for_pre_approval', 'view', 3, 'Time Report succcessfully change to Pre-approved', '2015-10-27 10:05:52'),
(150, 9, 3, 'logout', 'index', 0, '', '2015-10-27 10:06:04'),
(151, 9, 3, 'logout', 'index', 0, '', '2015-10-27 10:48:28'),
(152, 3, 4, 'logout', 'index', 0, '', '2015-10-27 10:49:53'),
(153, 5, 2, 'logout', 'index', 0, '', '2015-10-27 10:50:32'),
(154, 5, 2, 'logout', 'index', 0, '', '2015-10-27 11:42:22'),
(155, 11, 5, 'logout', 'index', 0, '', '2015-10-27 15:38:26'),
(156, 7, 2, 'logout', 'index', 0, '', '2015-10-27 15:40:59'),
(157, 2, 6, 'user', 'edit', 16, 'User successfully updated.', '2015-10-27 15:53:17'),
(158, 16, 2, 'timesheet', 'edit', 8, 'Timesheet Submitted', '2015-10-27 15:59:28'),
(159, 2, 6, 'logout', 'index', 0, '', '2015-10-27 16:00:18'),
(160, 9, 3, 'for_pre_approval', 'view', 8, 'Time Report succcessfully change to For re-work', '2015-10-27 16:01:13'),
(161, 16, 2, 'timesheet', 'edit', 8, 'Timesheet Submitted', '2015-10-27 16:01:37'),
(162, 9, 3, 'for_pre_approval', 'view', 8, 'Time Report succcessfully change to Pre-approved', '2015-10-27 16:01:57'),
(163, 9, 3, 'logout', 'index', 0, '', '2015-10-27 16:03:03'),
(164, 3, 4, 'for_approval', 'view', 8, 'Time Report succcessfully change to Approved', '2015-10-27 16:03:44'),
(165, 3, 4, 'logout', 'index', 0, '', '2015-10-27 16:04:05'),
(166, 2, 6, 'client', 'edit', 1, 'Client successfully updated.', '2015-10-27 16:04:34'),
(167, 2, 6, 'user', 'edit', 16, 'User successfully updated.', '2015-10-27 16:08:07'),
(168, 2, 6, 'user', 'edit', 16, 'User successfully updated.', '2015-10-27 16:09:15'),
(169, 16, 2, 'timesheet', 'edit', 10, 'Timesheet Submitted', '2015-10-27 16:10:22'),
(170, 2, 6, 'logout', 'index', 0, '', '2015-10-27 16:10:33'),
(171, 9, 3, 'logout', 'index', 0, '', '2015-10-27 16:10:53'),
(172, 3, 4, 'for_approval', 'view', 10, 'Time Report succcessfully change to Approved', '2015-10-27 16:11:27'),
(173, 3, 4, 'logout', 'index', 0, '', '2015-10-27 16:11:37'),
(174, 16, 2, 'logout', 'index', 0, '', '2015-10-27 16:13:41'),
(175, 1, 1, 'logout', 'index', 0, '', '2015-10-27 16:18:48'),
(176, 2, 6, 'client', 'add', 11, 'Successfully added client.', '2015-10-27 16:19:55'),
(177, 0, 0, 'logout', 'index', 0, '', '2015-10-27 16:23:44'),
(178, 2, 6, 'logout', 'index', 0, '', '2015-10-27 17:05:20'),
(179, 2, 6, 'user', 'add', 26, 'Successfully added user account.', '2015-10-27 17:08:21'),
(180, 2, 6, 'user', 'add', 27, 'Successfully added user account.', '2015-10-27 17:11:58'),
(181, 11, 5, 'logout', 'index', 0, '', '2015-10-27 17:16:20'),
(182, 11, 5, 'logout', 'index', 0, '', '2015-10-27 17:21:04'),
(183, 2, 6, 'logout', 'index', 0, '', '2015-10-27 17:27:07'),
(184, 7, 2, 'logout', 'index', 0, '', '2015-10-28 09:27:11'),
(185, 4, 2, 'logout', 'index', 0, '', '2015-10-28 09:47:12'),
(186, 2, 6, 'user', 'edit', 22, 'User successfully updated.', '2015-10-28 10:05:06'),
(187, 2, 6, 'logout', 'index', 0, '', '2015-10-28 10:06:45'),
(188, 2, 6, 'logout', 'index', 0, '', '2015-10-28 10:07:13'),
(189, 20, 2, 'timesheet', 'edit', 17, 'Timesheet Submitted', '2015-10-28 10:15:56'),
(190, 20, 2, 'logout', 'index', 0, '', '2015-10-28 10:16:03'),
(191, 7, 2, 'logout', 'index', 0, '', '2015-10-28 10:16:23'),
(192, 22, 2, 'logout', 'index', 0, '', '2015-10-28 10:23:32'),
(193, 11, 5, 'employee_list', 'add', 18, 'Successfully added employee rate.', '2015-10-28 10:28:53'),
(194, 2, 6, 'user', 'edit', 20, 'User successfully updated.', '2015-10-28 10:33:54'),
(195, 2, 6, 'user', 'edit', 21, 'User successfully updated.', '2015-10-28 10:35:09'),
(196, 20, 2, 'timesheet', 'edit', 18, 'Timesheet Submitted', '2015-10-28 10:42:53'),
(197, 20, 2, 'logout', 'index', 0, '', '2015-10-28 10:43:00'),
(198, 21, 2, 'timesheet', 'edit', 19, 'Timesheet Submitted', '2015-10-28 10:48:42'),
(199, 21, 2, 'logout', 'index', 0, '', '2015-10-28 10:48:47'),
(200, 2, 6, 'client', 'edit', 2, 'Client successfully updated.', '2015-10-28 10:49:56'),
(201, 2, 6, 'user', 'add', 28, 'Successfully added user account.', '2015-10-28 10:52:16'),
(202, 28, 4, 'logout', 'index', 0, '', '2015-10-28 10:54:17'),
(203, 8, 4, 'for_approval', 'view', 18, 'Time Report succcessfully change to Approved', '2015-10-28 10:54:56'),
(204, 8, 4, 'for_approval', 'view', 19, 'Time Report succcessfully change to Approved', '2015-10-28 10:55:05'),
(205, 3, 4, 'logout', 'index', 0, '', '2015-10-28 11:30:54'),
(206, 11, 5, 'logout', 'index', 0, '', '2015-10-28 12:57:50'),
(207, 1, 1, 'client', 'add', 12, 'Successfully added client.', '2015-10-28 12:58:50'),
(208, 1, 1, 'upload_csv', 'edit', 0, 'Upload Directory updated.', '2015-10-28 13:17:06'),
(209, 1, 1, 'user', 'add', 29, 'Successfully added user account.', '2015-10-28 17:03:28'),
(210, 1, 1, 'logout', 'index', 0, '', '2015-10-28 17:04:17'),
(211, 29, 2, 'logout', 'index', 0, '', '2015-10-28 18:11:21'),
(212, 1, 1, 'logout', 'index', 0, '', '2015-10-28 18:34:39'),
(213, 0, 0, 'logout', 'index', 0, '', '2015-10-29 09:07:17'),
(214, 1, 1, 'logout', 'index', 0, '', '2015-10-29 09:59:55'),
(215, 29, 2, 'logout', 'index', 0, '', '2015-10-29 12:08:10'),
(216, 1, 1, 'logout', 'index', 0, '', '2015-10-29 15:37:15'),
(217, 1, 1, 'logout', 'index', 0, '', '2015-10-29 15:55:40'),
(218, 29, 2, 'logout', 'index', 0, '', '2015-10-29 16:47:29'),
(219, 0, 0, 'logout', 'index', 0, '', '2015-10-30 09:13:32'),
(220, 1, 1, 'logout', 'index', 0, '', '2015-10-30 10:12:59'),
(221, 2, 6, 'logout', 'index', 0, '', '2015-10-30 10:32:37'),
(222, 11, 5, 'logout', 'index', 0, '', '2015-10-30 10:37:00'),
(223, 1, 1, 'rates', 'toggle', 2, 'Successfully added hour type.', '2015-10-30 13:19:08'),
(224, 1, 1, 'logout', 'index', 0, '', '2015-10-30 13:53:17'),
(225, 5, 2, 'logout', 'index', 0, '', '2015-10-30 13:59:56'),
(226, 1, 1, 'logout', 'index', 0, '', '2015-10-30 15:24:52'),
(227, 11, 5, 'logout', 'index', 0, '', '2015-10-30 15:25:14'),
(228, 5, 2, 'logout', 'index', 0, '', '2015-10-30 15:36:25'),
(229, 1, 1, 'rates', 'toggle', 2, 'Successfully added hour type.', '2015-10-30 16:44:11'),
(230, 1, 1, 'rates', 'toggle', 2, 'Successfully added hour type.', '2015-10-30 16:44:13'),
(231, 1, 1, 'rates', 'toggle', 2, 'Successfully added hour type.', '2015-10-30 16:44:15'),
(232, 1, 1, 'logout', 'index', 0, '', '2015-10-30 17:55:21'),
(233, 1, 1, 'logout', 'index', 0, '', '2015-11-02 10:53:04'),
(234, 5, 2, 'logout', 'index', 0, '', '2015-11-02 16:51:46'),
(235, 1, 1, 'logout', 'index', 0, '', '2015-11-03 10:01:46'),
(236, 1, 1, 'logout', 'index', 0, '', '2015-11-03 10:22:07'),
(237, 1, 1, 'logout', 'index', 0, '', '2015-11-03 10:29:39'),
(238, 1, 1, 'logout', 'index', 0, '', '2015-11-03 10:51:56'),
(239, 1, 1, 'logout', 'index', 0, '', '2015-11-03 10:53:04'),
(240, 1, 1, 'logout', 'index', 0, '', '2015-11-03 11:04:59'),
(241, 1, 1, 'logout', 'index', 0, '', '2015-11-03 11:20:24'),
(242, 1, 1, 'logout', 'index', 0, '', '2015-11-03 11:20:52'),
(243, 1, 1, 'logout', 'index', 0, '', '2015-11-03 11:22:32'),
(244, 1, 1, 'logout', 'index', 0, '', '2015-11-03 11:46:15'),
(245, 5, 2, 'logout', 'index', 0, '', '2015-11-03 12:00:39'),
(246, 1, 1, 'logout', 'index', 0, '', '2015-11-03 13:57:18'),
(247, 1, 1, 'logout', 'index', 0, '', '2015-11-03 14:50:21'),
(248, 1, 1, 'logout', 'index', 0, '', '2015-11-03 15:15:43'),
(249, 5, 2, 'logout', 'index', 0, '', '2015-11-03 15:47:44'),
(250, 1, 1, 'logout', 'index', 0, '', '2015-11-03 15:54:09'),
(251, 1, 1, 'logout', 'index', 0, '', '2015-11-03 16:13:43'),
(252, 1, 1, 'logout', 'index', 0, '', '2015-11-03 16:24:03'),
(253, 1, 1, 'logout', 'index', 0, '', '2015-11-03 16:31:47'),
(254, 1, 1, 'logout', 'index', 0, '', '2015-11-03 16:32:13'),
(255, 1, 1, 'logout', 'index', 0, '', '2015-11-03 16:33:43'),
(256, 5, 2, 'logout', 'index', 0, '', '2015-11-03 16:38:31'),
(257, 1, 1, 'logout', 'index', 0, '', '2015-11-03 16:44:29'),
(258, 5, 2, 'logout', 'index', 0, '', '2015-11-03 16:51:40'),
(259, 1, 1, 'logout', 'index', 0, '', '2015-11-03 17:53:34'),
(260, 1, 1, 'logout', 'index', 0, '', '2015-11-04 10:43:59'),
(261, 1, 1, 'logout', 'index', 0, '', '2015-11-04 14:10:31'),
(262, 3, 4, 'logout', 'index', 0, '', '2015-11-04 15:03:26'),
(263, 3, 4, 'for_approval', 'view', 3, 'Time Report succcessfully change to Approved', '2015-11-04 15:54:40'),
(264, 3, 4, 'logout', 'index', 0, '', '2015-11-04 15:57:17'),
(265, 1, 1, 'logout', 'index', 0, '', '2015-11-04 16:46:18'),
(266, 9, 3, 'logout', 'index', 0, '', '2015-11-04 16:59:07'),
(267, 3, 4, 'for_approval', 'view', 3, 'Time Report succcessfully change to Approved', '2015-11-04 16:59:56'),
(268, 3, 4, 'for_approval', 'view', 17, 'Time Report succcessfully change to Approved', '2015-11-04 17:10:55'),
(269, 3, 4, 'logout', 'index', 0, '', '2015-11-04 17:18:50'),
(270, 1, 1, 'logout', 'index', 0, '', '2015-11-04 17:33:34'),
(271, 1, 1, 'logout', 'index', 0, '', '2015-11-04 17:34:42'),
(272, 1, 1, 'logout', 'index', 0, '', '2015-11-04 17:59:17'),
(273, 1, 1, 'logout', 'index', 0, '', '2015-11-05 10:20:00'),
(274, 1, 1, 'logout', 'index', 0, '', '2015-11-05 11:16:20'),
(275, 20, 2, 'logout', 'index', 0, '', '2015-11-05 11:29:44'),
(276, 1, 1, 'logout', 'index', 0, '', '2015-11-05 11:45:14'),
(277, 1, 1, 'logout', 'index', 0, '', '2015-11-05 13:56:01'),
(278, 9, 3, 'logout', 'index', 0, '', '2015-11-05 14:00:08'),
(279, 1, 1, 'client', 'edit', 1, 'Client successfully updated.', '2015-11-05 14:00:31'),
(280, 1, 1, 'logout', 'index', 0, '', '2015-11-05 14:00:42'),
(281, 9, 3, 'logout', 'index', 0, '', '2015-11-05 14:02:20'),
(282, 21, 2, 'timesheet', 'edit', 23, 'Timesheet Submitted', '2015-11-05 14:46:10'),
(283, 21, 2, 'timesheet', 'edit', 23, 'Timesheet Submitted', '2015-11-05 14:47:07'),
(284, 21, 2, 'logout', 'index', 0, '', '2015-11-05 14:47:18'),
(285, 9, 3, 'logout', 'index', 0, '', '2015-11-05 15:23:38'),
(286, 3, 4, 'logout', 'index', 0, '', '2015-11-05 15:23:50'),
(287, 9, 3, 'for_pre_approval', 'view', 23, 'Time Report succcessfully change to Pre-approved', '2015-11-05 15:24:16'),
(288, 9, 3, 'logout', 'index', 0, '', '2015-11-05 15:24:21'),
(289, 3, 4, 'logout', 'index', 0, '', '2015-11-05 16:46:38'),
(290, 9, 3, 'pre_approved', 'view', 23, 'Time Report succcessfully change to For re-work', '2015-11-05 16:47:12'),
(291, 9, 3, 'logout', 'index', 0, '', '2015-11-05 16:47:16'),
(292, 5, 2, 'logout', 'index', 0, '', '2015-11-05 16:47:49'),
(293, 21, 2, 'logout', 'index', 0, '', '2015-11-05 16:48:32'),
(294, 1, 1, 'client', 'edit', 1, 'Client successfully updated.', '2015-11-05 16:48:57'),
(295, 1, 1, 'logout', 'index', 0, '', '2015-11-05 16:49:00'),
(296, 21, 2, 'timesheet', 'edit', 23, 'Timesheet Submitted', '2015-11-05 16:49:18'),
(297, 21, 2, 'logout', 'index', 0, '', '2015-11-05 16:49:27'),
(298, 3, 4, 'logout', 'index', 0, '', '2015-11-05 16:53:13'),
(299, 1, 1, 'client', 'edit', 1, 'Client successfully updated.', '2015-11-05 16:53:28'),
(300, 1, 1, 'logout', 'index', 0, '', '2015-11-05 16:53:32'),
(301, 3, 4, 'logout', 'index', 0, '', '2015-11-05 16:53:47'),
(302, 9, 3, 'for_pre_approval', 'view', 23, 'Time Report succcessfully change to Pre-approved', '2015-11-05 16:54:04'),
(303, 9, 3, 'logout', 'index', 0, '', '2015-11-05 16:54:10'),
(304, 3, 4, 'for_approval', 'index', 23, 'Time Report succcessfully change to Approved', '2015-11-05 17:55:01'),
(305, 1, 1, 'logout', 'index', 0, '', '2015-11-06 10:58:52'),
(306, 21, 2, 'logout', 'index', 0, '', '2015-11-06 11:13:49'),
(307, 22, 2, 'logout', 'index', 0, '', '2015-11-06 12:04:48'),
(308, 1, 1, 'logout', 'index', 0, '', '2015-11-06 13:55:20'),
(309, 22, 2, 'logout', 'index', 0, '', '2015-11-06 13:57:07'),
(310, 1, 1, 'logout', 'index', 0, '', '2015-11-06 13:57:41'),
(311, 22, 2, 'logout', 'index', 0, '', '2015-11-06 14:41:00'),
(312, 1, 1, 'logout', 'index', 0, '', '2015-11-10 10:49:41'),
(313, 1, 1, 'logout', 'index', 0, '', '2015-11-10 13:35:39'),
(314, 1, 1, 'user_profile', 'edit', 1, 'User profile successfully updated.', '2015-11-11 09:25:32'),
(315, 1, 1, 'user_profile', 'edit', 6, 'User profile successfully updated.', '2015-11-11 09:28:32'),
(316, 1, 1, 'user_profile', 'edit', 1, 'User profile successfully updated.', '2015-11-11 10:14:43'),
(317, 1, 1, 'user_profile', 'edit', 1, 'User profile successfully updated.', '2015-11-11 10:14:59'),
(318, 1, 1, 'user_profile', 'edit', 1, 'User profile successfully updated.', '2015-11-11 10:15:09'),
(319, 1, 1, 'user_profile', 'edit', 1, 'User profile successfully updated.', '2015-11-11 10:15:15'),
(320, 1, 1, 'user_profile', 'edit', 1, 'User profile successfully updated.', '2015-11-11 10:15:23'),
(321, 1, 1, 'user_profile', 'edit', 1, 'User profile successfully updated.', '2015-11-11 10:15:34'),
(322, 1, 1, 'employee_data', 'index', 29, 'User successfully updated.', '2015-11-12 18:33:23'),
(323, 1, 1, 'employee_data', 'index', 29, 'User successfully updated.', '2015-11-12 18:40:42'),
(324, 1, 1, 'employee_data', 'index', 30, 'Successfully added user account.', '2015-11-12 18:40:47'),
(325, 1, 1, 'employee_data', 'index', 31, 'Successfully added user account.', '2015-11-12 18:40:47'),
(326, 1, 1, 'employee_data', 'index', 17, 'User successfully updated.', '2015-11-12 18:40:47'),
(327, 1, 1, 'employee_data', 'index', 32, 'Successfully added user account.', '2015-11-12 18:40:47'),
(328, 1, 1, 'employee_data', 'index', 33, 'Successfully added user account.', '2015-11-12 18:40:48'),
(329, 1, 1, 'employee_data', 'index', 34, 'Successfully added user account.', '2015-11-12 18:40:48'),
(330, 1, 1, 'employee_data', 'index', 26, 'User successfully updated.', '2015-11-12 18:40:48'),
(331, 1, 1, 'employee_data', 'index', 35, 'Successfully added user account.', '2015-11-12 18:40:48'),
(332, 1, 1, 'employee_data', 'index', 36, 'Successfully added user account.', '2015-11-12 18:40:48'),
(333, 1, 1, 'employee_data', 'index', 37, 'Successfully added user account.', '2015-11-12 18:40:48'),
(334, 1, 1, 'employee_data', 'index', 38, 'Successfully added user account.', '2015-11-12 18:40:48'),
(335, 1, 1, 'employee_data', 'index', 39, 'Successfully added user account.', '2015-11-12 18:40:49'),
(336, 1, 1, 'employee_data', 'index', 40, 'Successfully added user account.', '2015-11-12 18:40:49'),
(337, 1, 1, 'employee_data', 'index', 12, 'User successfully updated.', '2015-11-12 18:40:49'),
(338, 1, 1, 'employee_data', 'index', 41, 'Successfully added user account.', '2015-11-12 18:40:49'),
(339, 1, 1, 'employee_data', 'index', 42, 'Successfully added user account.', '2015-11-12 18:40:49'),
(340, 1, 1, 'employee_data', 'index', 43, 'Successfully added user account.', '2015-11-12 18:40:49'),
(341, 1, 1, 'employee_data', 'index', 44, 'Successfully added user account.', '2015-11-12 18:40:49'),
(342, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-11-13 10:55:57'),
(343, 1, 1, 'logout', 'index', 0, '', '2015-11-16 17:01:22'),
(344, 21, 2, 'logout', 'index', 0, '', '2015-11-16 17:01:51'),
(345, 7, 2, 'logout', 'index', 0, '', '2015-11-16 17:19:45'),
(346, 0, 0, 'logout', 'index', 0, '', '2015-11-17 09:57:45'),
(347, 1, 1, 'logout', 'index', 0, '', '2015-11-17 10:49:00'),
(348, 1, 1, 'logout', 'index', 0, '', '2015-11-17 11:10:42'),
(349, 1, 1, 'logout', 'index', 0, '', '2015-11-17 12:00:51'),
(350, 1, 1, 'logout', 'index', 0, '', '2015-11-24 14:31:23'),
(351, 0, 0, 'login', 'index', 0, '', '2015-11-24 16:20:40'),
(352, 0, 0, 'login', 'index', 0, '', '2015-11-24 16:50:54'),
(353, 1, 1, 'logout', 'index', 0, '', '2015-11-24 17:12:30'),
(354, 0, 0, 'login', 'index', 0, '', '2015-11-24 17:12:37'),
(355, 1, 1, 'logout', 'index', 0, '', '2015-11-24 17:18:15'),
(356, 0, 0, 'login', 'index', 0, '', '2015-11-24 17:18:24'),
(357, 1, 1, 'user', 'edit', 1, 'Successfully updated User Account!', '2015-11-24 17:34:53'),
(358, 1, 1, 'user', 'edit', 1, 'Successfully updated User Account!', '2015-11-24 17:34:57'),
(359, 0, 0, 'login', 'index', 0, '', '2015-11-25 09:25:14'),
(360, 1, 1, 'logout', 'index', 0, '', '2015-11-25 10:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `administrator_page`
--

CREATE TABLE IF NOT EXISTS `administrator_page` (
`id_administrator_page` int(11) NOT NULL,
  `parent_administrator_page_id` int(11) NOT NULL,
  `classname` varchar(128) NOT NULL,
  `page` varchar(128) NOT NULL,
  `img` varchar(256) NOT NULL,
  `display` tinyint(1) NOT NULL,
  `display_subpage` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `civil_status`
--

CREATE TABLE IF NOT EXISTS `civil_status` (
`id_civil_status` int(11) NOT NULL,
  `civil_status_iso_code` varchar(3) NOT NULL,
  `civil_status_code` varchar(15) NOT NULL,
  `civil_status_status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `civil_status`
--

INSERT INTO `civil_status` (`id_civil_status`, `civil_status_iso_code`, `civil_status_code`, `civil_status_status`) VALUES
(1, 'S', 'Single', 1),
(2, 'M', 'Married', 1),
(3, 'D', 'Divorced', 1),
(4, 'W', 'Widow/er', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('715588ee1bb7974e7e9614fe4398bfffae76de34', '::1', 1448344977, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383334343937373b);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
`id_configuration` int(11) NOT NULL,
  `configuration` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `config_type` tinyint(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`id_configuration`, `configuration`, `value`, `config_type`) VALUES
(1, 'EMAIL_PROTOCOL', 'mail', 1),
(2, 'SMTP_DOMAIN', '', 1),
(3, 'SMTP_HOST', '', 1),
(4, 'SMTP_USER', '', 1),
(5, 'SMTP_PASS', '', 1),
(6, 'SMTP_PORT', '25', 1),
(7, 'EMAIL_TYPE', 'html', 1),
(8, 'DEBUG_MODE', '0', 1),
(9, 'THEME', 'default', 1),
(10, 'SITE_TITLE', 'Mother', 1),
(11, 'CACHE_MODE', '0', 1),
(12, 'CACHE_TIMEOUT', '2', 1),
(13, 'DEFAULT_LANGUAGE', '1', 1),
(14, 'ADMIN_PAGE_LOGO', '1', 1),
(15, 'ADMIN_ITEMS', '100', 1),
(16, 'SMTP_CC', '', 1),
(17, 'COMP_LOGO', '', 1),
(18, 'COMP_ADDR1', '', 1),
(19, 'COMP_ADDR2', '', 1),
(20, 'COMP_EMAIL', '', 1),
(21, 'COMP_HEADER', '', 1),
(22, 'COMP_FOOTER', '', 1),
(23, 'COMP_HEADER', '', 1),
(24, 'DEFAULT_COUNTRY', '', 1),
(25, 'DEFAULT_TIME_ZONE', 'Asia/Manila', 1),
(26, 'SMTP_BCC', '', 1),
(27, 'UPLOAD_DIR', 'e:\\titanium', 1);

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE IF NOT EXISTS `month` (
  `id_month` int(11) NOT NULL,
  `month` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`id_month`, `month`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id_user` int(11) NOT NULL,
  `user_profile_id` int(11) NOT NULL,
  `employee_num` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `secure_token` varchar(128) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_failed_login` datetime DEFAULT NULL,
  `num_failed_login` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `is_new` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `user_profile_id`, `employee_num`, `password`, `secure_token`, `salt`, `last_login`, `last_failed_login`, `num_failed_login`, `enabled`, `is_deleted`, `is_locked`, `is_new`) VALUES
(1, 1, '00-00001', 'e22d65491774d75f3bd6125b03d68d22', '1f58cb4e7e21fb4fff3494f6655885', 'DUKSJODD', '2015-11-25 09:25:14', '2015-11-16 00:00:00', 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
`id_user_account` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `employee_num` varchar(128) NOT NULL,
  `email_address` varchar(128) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `middlename` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) NOT NULL,
  `nickname` varchar(32) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `birthplace` varchar(128) DEFAULT NULL,
  `gender_code` varchar(32) NOT NULL,
  `nationality` varchar(64) DEFAULT NULL,
  `civil_status_code` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `zipcode` varchar(16) DEFAULT NULL,
  `contactno` varchar(32) DEFAULT NULL,
  `position` varchar(64) DEFAULT NULL,
  `department` varchar(128) DEFAULT NULL,
  `section` varchar(128) DEFAULT NULL,
  `date_hired` date DEFAULT NULL,
  `pay_frequency` varchar(64) DEFAULT NULL,
  `tin_tax_status` varchar(64) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`id_user_account`, `user_id`, `employee_num`, `email_address`, `firstname`, `middlename`, `lastname`, `nickname`, `birthdate`, `birthplace`, `gender_code`, `nationality`, `civil_status_code`, `address`, `zipcode`, `contactno`, `position`, `department`, `section`, `date_hired`, `pay_frequency`, `tin_tax_status`, `client_id`) VALUES
(1, 1, '00-00001', 'superadmin@yondu.com', 'Super Admin', 'Super Admin', 'Super Admin', 'Admin', '2015-06-08', 'Manila', 'Male', 'Filipino', 'Single', 'dadsadasd', '', '0912312831', 'Super Admin', 'SDG', 'Technology Department', '2015-08-03', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `user_id` int(11) NOT NULL,
  `token` varchar(128) NOT NULL,
  `salt` varchar(64) NOT NULL,
  `date_added` date NOT NULL,
  `ip_address` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
`id_user_profile` int(11) NOT NULL,
  `user_profile` varchar(128) NOT NULL,
  `user_description` varchar(256) NOT NULL,
  `enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id_user_profile`, `user_profile`, `user_description`, `enabled`) VALUES
(1, 'Super Administrator', 'Super Administrator', 1),
(2, 'Employee', 'Yondu Employee', 1),
(3, 'Client IS', 'Client Approver', 1),
(4, 'Yondu IS', 'Yondu Approver', 1),
(5, 'YFU', 'Yondu Finance User', 1),
(6, 'Admin', 'Administrator', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator_access`
--
ALTER TABLE `administrator_access`
 ADD PRIMARY KEY (`user_profile_id`,`page_id`);

--
-- Indexes for table `administrator_log`
--
ALTER TABLE `administrator_log`
 ADD PRIMARY KEY (`id_administrator_log`);

--
-- Indexes for table `administrator_page`
--
ALTER TABLE `administrator_page`
 ADD PRIMARY KEY (`id_administrator_page`);

--
-- Indexes for table `civil_status`
--
ALTER TABLE `civil_status`
 ADD PRIMARY KEY (`id_civil_status`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`id`), ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
 ADD PRIMARY KEY (`id_configuration`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
 ADD PRIMARY KEY (`id_month`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
 ADD PRIMARY KEY (`id_user_account`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
 ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
 ADD PRIMARY KEY (`id_user_profile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator_log`
--
ALTER TABLE `administrator_log`
MODIFY `id_administrator_log` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=361;
--
-- AUTO_INCREMENT for table `administrator_page`
--
ALTER TABLE `administrator_page`
MODIFY `id_administrator_page` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `civil_status`
--
ALTER TABLE `civil_status`
MODIFY `id_civil_status` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
MODIFY `id_configuration` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
MODIFY `id_user_account` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
MODIFY `id_user_profile` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
