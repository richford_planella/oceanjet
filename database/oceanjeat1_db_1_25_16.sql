-- --------------------------------------------------------
-- Host:                         192.168.0.41
-- Server version:               5.5.41 - MySQL Community Server (GPL) by Remi
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5031
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oceanjet1_db.accommodation
CREATE TABLE IF NOT EXISTS `accommodation` (
  `id_accommodation` int(11) NOT NULL AUTO_INCREMENT,
  `accommodation_code` varchar(16) NOT NULL,
  `accommodation` varchar(128) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `palette` varchar(8) NOT NULL,
  PRIMARY KEY (`id_accommodation`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.accommodation: ~3 rows (approximately)
/*!40000 ALTER TABLE `accommodation` DISABLE KEYS */;
INSERT INTO `accommodation` (`id_accommodation`, `accommodation_code`, `accommodation`, `enabled`, `date_added`, `date_update`, `palette`) VALUES
	(1, 'TC', 'Tourist Class', 1, '2016-01-20 11:10:43', '2016-01-20 12:44:57', '#ffff48'),
	(2, 'BC', 'Business Class', 1, '2016-01-20 11:11:09', '2016-01-20 12:45:21', '#3eff9e'),
	(3, 'OA', 'Open Air', 1, '2016-01-20 11:11:22', '2016-01-20 12:45:58', '#fe6650');
/*!40000 ALTER TABLE `accommodation` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.administrator_access
CREATE TABLE IF NOT EXISTS `administrator_access` (
  `user_profile_id` int(10) unsigned NOT NULL,
  `page_id` int(11) unsigned NOT NULL,
  `access` tinyint(1) NOT NULL DEFAULT '0',
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `add` tinyint(1) NOT NULL DEFAULT '0',
  `edit` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  `approve` tinyint(4) NOT NULL DEFAULT '0',
  `verify` tinyint(4) NOT NULL DEFAULT '0',
  `print` tinyint(4) NOT NULL DEFAULT '0',
  `request` tinyint(4) NOT NULL DEFAULT '0',
  `close` tinyint(4) NOT NULL DEFAULT '0',
  `received` tinyint(4) NOT NULL DEFAULT '0',
  `allocate` tinyint(4) NOT NULL DEFAULT '0',
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  `reactivate` tinyint(1) NOT NULL DEFAULT '0',
  `export` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_profile_id`,`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.administrator_access: 64 rows
/*!40000 ALTER TABLE `administrator_access` DISABLE KEYS */;
INSERT INTO `administrator_access` (`user_profile_id`, `page_id`, `access`, `view`, `add`, `edit`, `delete`, `approve`, `verify`, `print`, `request`, `close`, `received`, `allocate`, `activate`, `reactivate`, `export`) VALUES
	(1, 113, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 112, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 111, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 106, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 105, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 104, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 103, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 102, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 101, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 10, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 912, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 911, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 910, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 99, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 98, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 97, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 96, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 95, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 94, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 93, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 92, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 91, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 8, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 78, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 77, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 76, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 75, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 74, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 73, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 72, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 71, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 64, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 63, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 62, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 61, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 5, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 44, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 43, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 42, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 41, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 313, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 312, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 311, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 310, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 39, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 38, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 37, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 36, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 35, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 34, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 33, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 32, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 31, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
	(1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 23, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 22, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
	(1, 21, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1),
	(1, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `administrator_access` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.administrator_log
CREATE TABLE IF NOT EXISTS `administrator_log` (
  `id_administrator_log` int(11) NOT NULL AUTO_INCREMENT,
  `administrator_id` int(11) NOT NULL,
  `administrator_profile_id` int(11) NOT NULL,
  `classname` varchar(128) NOT NULL,
  `action` varchar(128) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id_administrator_log`)
) ENGINE=InnoDB AUTO_INCREMENT=667 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.administrator_log: ~551 rows (approximately)
/*!40000 ALTER TABLE `administrator_log` DISABLE KEYS */;
INSERT INTO `administrator_log` (`id_administrator_log`, `administrator_id`, `administrator_profile_id`, `classname`, `action`, `reference_id`, `description`, `date_added`) VALUES
	(1, 1, 1, 'user', 'add', 2, 'Successfully added User Account!', '2015-11-27 18:10:08'),
	(2, 33, 2, 'logout', 'index', 0, '', '2015-11-27 18:10:39'),
	(3, 1, 1, 'client', 'add', 1, 'Successfully added client!', '2015-11-27 18:13:49'),
	(4, 1, 1, 'user', 'add', 3, 'Successfully added User Account!', '2015-11-27 18:16:27'),
	(5, 2, 4, 'logout', 'index', 0, '', '2015-11-27 18:16:43'),
	(6, 3, 4, 'logout', 'index', 0, '', '2015-11-27 18:17:37'),
	(7, 1, 1, 'user', 'add', 4, 'Successfully added User Account!', '2015-11-27 18:18:35'),
	(8, 4, 4, 'logout', 'index', 0, '', '2015-11-27 18:19:46'),
	(9, 1, 1, 'user', 'add', 5, 'Successfully added User Account!', '2015-11-27 18:22:06'),
	(10, 1, 1, 'client', 'add', 2, 'Successfully added client!', '2015-11-27 18:24:23'),
	(11, 1, 1, 'client', 'add', 3, 'Successfully added client!', '2015-11-27 18:24:58'),
	(12, 1, 1, 'client', 'add', 4, 'Successfully added client!', '2015-11-27 18:25:51'),
	(13, 1, 1, 'user', 'add', 6, 'Successfully added User Account!', '2015-11-27 18:26:29'),
	(14, 5, 4, 'logout', 'index', 0, '', '2015-11-27 18:26:56'),
	(15, 6, 4, 'logout', 'index', 0, '', '2015-11-27 18:27:40'),
	(16, 1, 1, 'client', 'add', 5, 'Successfully added client!', '2015-11-27 18:28:43'),
	(17, 1, 1, 'user', 'add', 7, 'Successfully added User Account!', '2015-11-27 18:38:05'),
	(18, 7, 3, 'logout', 'index', 0, '', '2015-11-27 18:39:07'),
	(19, 1, 1, 'user', 'add', 8, 'Successfully added User Account!', '2015-11-27 18:40:32'),
	(20, 8, 3, 'logout', 'index', 0, '', '2015-11-27 18:42:58'),
	(21, 1, 1, 'user', 'add', 9, 'Successfully added User Account!', '2015-11-27 18:44:54'),
	(22, 9, 3, 'logout', 'index', 0, '', '2015-11-27 18:46:22'),
	(23, 1, 1, 'user', 'add', 10, 'Successfully added User Account!', '2015-11-27 18:48:48'),
	(24, 10, 3, 'logout', 'index', 0, '', '2015-11-27 18:50:27'),
	(25, 1, 1, 'user', 'add', 11, 'Successfully added User Account!', '2015-11-27 18:52:02'),
	(26, 11, 3, 'logout', 'index', 0, '', '2015-11-27 18:54:08'),
	(27, 1, 1, 'user', 'add', 12, 'Successfully added User Account!', '2015-11-27 18:55:16'),
	(28, 12, 3, 'logout', 'index', 0, '', '2015-11-27 18:56:21'),
	(29, 1, 1, 'user', 'add', 13, 'Successfully added User Account!', '2015-11-27 18:57:39'),
	(30, 13, 3, 'logout', 'index', 0, '', '2015-11-27 18:58:44'),
	(31, 1, 1, 'project', 'add', 1, 'Successfully added project!', '2015-11-27 18:59:58'),
	(32, 1, 1, 'project', 'add', 2, 'Successfully added project!', '2015-11-27 19:00:09'),
	(33, 1, 1, 'project', 'add', 3, 'Successfully added project!', '2015-11-27 19:00:19'),
	(34, 1, 1, 'project', 'add', 4, 'Successfully added project!', '2015-11-27 19:00:40'),
	(35, 1, 1, 'project', 'add', 5, 'Successfully added project!', '2015-11-27 19:02:35'),
	(36, 1, 1, 'project', 'add', 6, 'Successfully added project!', '2015-11-27 19:02:51'),
	(37, 1, 1, 'project', 'add', 7, 'Successfully added project!', '2015-11-27 19:03:11'),
	(38, 1, 1, 'user', 'add', 14, 'Successfully added User Account!', '2015-11-27 19:07:15'),
	(39, 14, 2, 'logout', 'index', 0, '', '2015-11-27 19:09:43'),
	(40, 1, 1, 'user', 'add', 15, 'Successfully added User Account!', '2015-11-27 19:10:51'),
	(41, 15, 2, 'logout', 'index', 0, '', '2015-11-27 19:11:58'),
	(42, 1, 1, 'user', 'add', 16, 'Successfully added User Account!', '2015-11-27 19:13:29'),
	(43, 16, 2, 'logout', 'index', 0, '', '2015-11-27 19:14:42'),
	(44, 1, 1, 'user', 'add', 17, 'Successfully added User Account!', '2015-11-27 19:15:39'),
	(45, 17, 2, 'logout', 'index', 0, '', '2015-11-27 19:16:48'),
	(46, 1, 1, 'user', 'add', 18, 'Successfully added User Account!', '2015-11-27 19:17:41'),
	(47, 18, 2, 'logout', 'index', 0, '', '2015-11-27 19:23:42'),
	(48, 1, 1, 'appearance', 'index', 0, 'Change Site Title', '2015-11-27 19:31:54'),
	(49, 1, 1, 'appearance', 'index', 0, 'Change Site Title', '2015-11-27 19:32:15'),
	(50, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:46:02'),
	(51, 14, 2, 'logout', 'index', 0, '', '2015-11-27 19:46:13'),
	(52, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:47:23'),
	(53, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:50:38'),
	(54, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:50:57'),
	(55, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:51:21'),
	(56, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:53:18'),
	(57, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:54:37'),
	(58, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:56:31'),
	(59, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:58:18'),
	(60, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 19:59:56'),
	(61, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:00:26'),
	(62, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:00:32'),
	(63, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:01:29'),
	(64, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:01:48'),
	(65, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:02:45'),
	(66, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-27 20:04:00'),
	(67, 2, 4, 'logout', 'index', 0, '', '2015-11-27 20:07:23'),
	(68, 1, 1, 'user', 'add', 19, 'Successfully added User Account!', '2015-11-27 20:22:38'),
	(69, 2, 4, 'logout', 'index', 0, '', '2015-11-27 20:23:10'),
	(70, 1, 1, 'employee_data', 'index', 20, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(71, 1, 1, 'employee_data', 'index', 21, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(72, 1, 1, 'employee_data', 'index', 22, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(73, 1, 1, 'employee_data', 'index', 23, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(74, 1, 1, 'employee_data', 'index', 24, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(75, 1, 1, 'employee_data', 'index', 25, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(76, 1, 1, 'employee_data', 'index', 26, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(77, 1, 1, 'employee_data', 'index', 27, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(78, 1, 1, 'employee_data', 'index', 28, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(79, 1, 1, 'employee_data', 'index', 29, 'Successfully added user account.', '2015-11-27 20:33:18'),
	(80, 1, 1, 'employee_data', 'index', 30, 'Successfully added user account.', '2015-11-27 20:33:19'),
	(81, 1, 1, 'employee_data', 'index', 31, 'Successfully added user account.', '2015-11-27 20:33:19'),
	(82, 1, 1, 'employee_data', 'index', 32, 'Successfully added user account.', '2015-11-27 20:33:19'),
	(83, 1, 1, 'employee_data', 'index', 33, 'Successfully added user account.', '2015-11-27 20:33:19'),
	(84, 1, 1, 'employee_data', 'index', 34, 'Successfully added user account.', '2015-11-27 20:33:19'),
	(85, 1, 1, 'employee_data', 'index', 35, 'Successfully added user account.', '2015-11-27 20:33:19'),
	(86, 1, 1, 'employee_data', 'index', 36, 'Successfully added user account.', '2015-11-27 20:33:19'),
	(87, 1, 1, 'employee_data', 'index', 19, 'User successfully updated.', '2015-11-27 20:33:19'),
	(88, 1, 1, 'employee_data', 'index', 37, 'Successfully added user account.', '2015-11-27 20:33:19'),
	(89, 1, 1, 'employee_data', 'index', 20, 'User successfully updated.', '2015-11-27 20:36:05'),
	(90, 1, 1, 'employee_data', 'index', 21, 'User successfully updated.', '2015-11-27 20:36:05'),
	(91, 1, 1, 'employee_data', 'index', 22, 'User successfully updated.', '2015-11-27 20:36:05'),
	(92, 1, 1, 'employee_data', 'index', 23, 'User successfully updated.', '2015-11-27 20:36:05'),
	(93, 1, 1, 'employee_data', 'index', 24, 'User successfully updated.', '2015-11-27 20:36:05'),
	(94, 1, 1, 'employee_data', 'index', 25, 'User successfully updated.', '2015-11-27 20:36:05'),
	(95, 1, 1, 'employee_data', 'index', 26, 'User successfully updated.', '2015-11-27 20:36:05'),
	(96, 1, 1, 'employee_data', 'index', 27, 'User successfully updated.', '2015-11-27 20:36:05'),
	(97, 1, 1, 'employee_data', 'index', 28, 'User successfully updated.', '2015-11-27 20:36:05'),
	(98, 1, 1, 'employee_data', 'index', 29, 'User successfully updated.', '2015-11-27 20:36:05'),
	(99, 1, 1, 'employee_data', 'index', 30, 'User successfully updated.', '2015-11-27 20:36:05'),
	(100, 1, 1, 'employee_data', 'index', 31, 'User successfully updated.', '2015-11-27 20:36:05'),
	(101, 1, 1, 'employee_data', 'index', 32, 'User successfully updated.', '2015-11-27 20:36:05'),
	(102, 1, 1, 'employee_data', 'index', 33, 'User successfully updated.', '2015-11-27 20:36:05'),
	(103, 1, 1, 'employee_data', 'index', 34, 'User successfully updated.', '2015-11-27 20:36:05'),
	(104, 1, 1, 'employee_data', 'index', 35, 'User successfully updated.', '2015-11-27 20:36:05'),
	(105, 1, 1, 'employee_data', 'index', 36, 'User successfully updated.', '2015-11-27 20:36:05'),
	(106, 1, 1, 'employee_data', 'index', 19, 'User successfully updated.', '2015-11-27 20:36:05'),
	(107, 1, 1, 'employee_data', 'index', 37, 'User successfully updated.', '2015-11-27 20:36:05'),
	(108, 1, 1, 'employee_data', 'index', 20, 'User successfully updated.', '2015-11-27 20:37:21'),
	(109, 1, 1, 'employee_data', 'index', 21, 'User successfully updated.', '2015-11-27 20:37:21'),
	(110, 1, 1, 'employee_data', 'index', 22, 'User successfully updated.', '2015-11-27 20:37:21'),
	(111, 1, 1, 'employee_data', 'index', 23, 'User successfully updated.', '2015-11-27 20:37:21'),
	(112, 1, 1, 'employee_data', 'index', 24, 'User successfully updated.', '2015-11-27 20:37:21'),
	(113, 1, 1, 'employee_data', 'index', 25, 'User successfully updated.', '2015-11-27 20:37:21'),
	(114, 1, 1, 'employee_data', 'index', 26, 'User successfully updated.', '2015-11-27 20:37:21'),
	(115, 1, 1, 'employee_data', 'index', 27, 'User successfully updated.', '2015-11-27 20:37:21'),
	(116, 1, 1, 'employee_data', 'index', 28, 'User successfully updated.', '2015-11-27 20:37:21'),
	(117, 1, 1, 'employee_data', 'index', 29, 'User successfully updated.', '2015-11-27 20:37:21'),
	(118, 1, 1, 'employee_data', 'index', 30, 'User successfully updated.', '2015-11-27 20:37:21'),
	(119, 1, 1, 'employee_data', 'index', 31, 'User successfully updated.', '2015-11-27 20:37:21'),
	(120, 1, 1, 'employee_data', 'index', 32, 'User successfully updated.', '2015-11-27 20:37:21'),
	(121, 1, 1, 'employee_data', 'index', 33, 'User successfully updated.', '2015-11-27 20:37:21'),
	(122, 1, 1, 'employee_data', 'index', 34, 'User successfully updated.', '2015-11-27 20:37:21'),
	(123, 1, 1, 'employee_data', 'index', 35, 'User successfully updated.', '2015-11-27 20:37:21'),
	(124, 1, 1, 'employee_data', 'index', 36, 'User successfully updated.', '2015-11-27 20:37:21'),
	(125, 1, 1, 'employee_data', 'index', 19, 'User successfully updated.', '2015-11-27 20:37:21'),
	(126, 1, 1, 'employee_data', 'index', 37, 'User successfully updated.', '2015-11-27 20:37:21'),
	(127, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-29 10:15:37'),
	(128, 7, 3, 'logout', 'index', 0, '', '2015-11-29 21:07:33'),
	(129, 14, 2, 'logout', 'index', 0, '', '2015-11-29 22:47:50'),
	(130, 16, 2, 'logout', 'index', 0, '', '2015-11-30 17:12:44'),
	(131, 17, 2, 'logout', 'index', 0, '', '2015-11-30 17:23:15'),
	(132, 18, 2, 'logout', 'index', 0, '', '2015-11-30 17:24:42'),
	(133, 1, 1, 'user', 'add', 38, 'Successfully added User Account!', '2015-11-30 17:44:19'),
	(134, 38, 3, 'logout', 'index', 0, '', '2015-11-30 17:45:47'),
	(135, 1, 1, 'user', 'add', 39, 'Successfully added User Account!', '2015-11-30 17:48:02'),
	(136, 39, 3, 'logout', 'index', 0, '', '2015-11-30 17:49:49'),
	(137, 1, 1, 'user', 'add', 40, 'Successfully added User Account!', '2015-11-30 17:52:35'),
	(138, 1, 1, 'project', 'add', 8, 'Successfully added project!', '2015-11-30 17:54:52'),
	(139, 1, 1, 'project', 'add', 9, 'Successfully added project!', '2015-11-30 17:55:07'),
	(140, 1, 1, 'project', 'add', 10, 'Successfully added project!', '2015-11-30 17:55:20'),
	(141, 1, 1, 'client_group', 'add', 1, 'Successfully added employee client!', '2015-11-30 17:56:28'),
	(142, 1, 1, 'user', 'add', 41, 'Successfully added User Account!', '2015-11-30 17:58:13'),
	(143, 40, 3, 'logout', 'index', 0, '', '2015-11-30 17:58:33'),
	(144, 41, 2, 'logout', 'index', 0, '', '2015-11-30 17:59:49'),
	(145, 1, 1, 'user', 'add', 42, 'Successfully added User Account!', '2015-11-30 18:01:07'),
	(146, 1, 1, 'user', 'edit', 42, 'Successfully updated User Account!', '2015-11-30 18:03:39'),
	(147, 42, 2, 'logout', 'index', 0, '', '2015-11-30 18:03:58'),
	(148, 1, 1, 'user', 'add', 43, 'Successfully added User Account!', '2015-11-30 18:04:56'),
	(149, 1, 1, 'user', 'edit', 1, 'Successfully updated User Account!', '2015-11-30 18:05:49'),
	(150, 43, 2, 'logout', 'index', 0, '', '2015-11-30 18:07:03'),
	(151, 1, 1, 'user', 'add', 44, 'Successfully added User Account!', '2015-11-30 18:08:56'),
	(152, 44, 2, 'logout', 'index', 0, '', '2015-11-30 18:10:25'),
	(153, 1, 1, 'user', 'add', 45, 'Successfully added User Account!', '2015-11-30 18:11:05'),
	(154, 45, 2, 'logout', 'index', 0, '', '2015-11-30 18:12:23'),
	(155, 45, 2, 'logout', 'index', 0, '', '2015-11-30 18:12:54'),
	(156, 1, 1, 'user', 'add', 46, 'Successfully added User Account!', '2015-11-30 18:15:27'),
	(157, 46, 5, 'employee_list', 'add', 1, 'Successfully added employee rate.', '2015-11-30 18:18:32'),
	(158, 46, 5, 'employee_list', 'add', 2, 'Successfully added employee rate.', '2015-11-30 18:19:16'),
	(159, 46, 5, 'employee_list', 'add', 3, 'Successfully added employee rate.', '2015-11-30 18:19:51'),
	(160, 46, 5, 'employee_list', 'add', 4, 'Successfully added employee rate.', '2015-11-30 18:20:37'),
	(161, 46, 5, 'employee_list', 'add', 5, 'Successfully added employee rate.', '2015-11-30 18:21:30'),
	(162, 46, 5, 'logout', 'index', 0, '', '2015-11-30 18:22:39'),
	(163, 41, 2, 'timesheet', 'edit', 9, 'Timesheet Submitted', '2015-11-30 18:29:57'),
	(164, 41, 2, 'logout', 'index', 0, '', '2015-11-30 18:31:29'),
	(165, 42, 2, 'timesheet', 'edit', 12, 'Timesheet Submitted', '2015-11-30 18:36:41'),
	(166, 42, 2, 'logout', 'index', 0, '', '2015-11-30 18:36:56'),
	(167, 43, 2, 'timesheet', 'edit', 15, 'Timesheet Submitted', '2015-11-30 18:48:58'),
	(168, 43, 2, 'logout', 'index', 0, '', '2015-11-30 18:49:08'),
	(169, 44, 2, 'timesheet', 'edit', 16, 'Timesheet Submitted', '2015-11-30 18:57:40'),
	(170, 44, 2, 'logout', 'index', 0, '', '2015-11-30 18:57:56'),
	(171, 45, 2, 'timesheet', 'edit', 17, 'Timesheet Submitted', '2015-11-30 19:01:55'),
	(172, 45, 2, 'logout', 'index', 0, '', '2015-11-30 19:02:38'),
	(173, 38, 3, 'for_pre_approval', 'view', 9, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:08:49'),
	(174, 38, 3, 'for_pre_approval', 'view', 12, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:09:07'),
	(175, 1, 1, 'client', 'edit', 2, 'Client successfully updated!', '2015-11-30 19:10:36'),
	(176, 1, 1, 'client', 'edit', 2, 'Client successfully updated!', '2015-11-30 19:14:07'),
	(177, 38, 3, 'for_pre_approval', 'view', 15, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:15:03'),
	(178, 38, 3, 'for_pre_approval', 'view', 16, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:15:16'),
	(179, 38, 3, 'for_pre_approval', 'view', 17, 'Time Report succcessfully change to Pre-approved', '2015-11-30 19:16:10'),
	(180, 3, 4, 'for_approval', 'view', 9, 'Time Report succcessfully change to Approved', '2015-11-30 19:17:53'),
	(181, 3, 4, 'for_approval', 'index', 12, 'Time Report succcessfully change to Approved', '2015-11-30 19:18:22'),
	(182, 3, 4, 'for_approval', 'index', 15, 'Time Report succcessfully change to Approved', '2015-11-30 19:18:22'),
	(183, 3, 4, 'for_approval', 'index', 16, 'Time Report succcessfully change to Approved', '2015-11-30 19:18:22'),
	(184, 3, 4, 'for_approval', 'index', 17, 'Time Report succcessfully change to Approved', '2015-11-30 19:18:22'),
	(185, 1, 1, 'user', 'add', 47, 'Successfully added User Account!', '2015-11-30 19:33:44'),
	(186, 38, 3, 'logout', 'index', 0, '', '2015-11-30 19:34:27'),
	(187, 47, 3, 'logout', 'index', 0, '', '2015-11-30 19:35:40'),
	(188, 1, 1, 'user', 'add', 48, 'Successfully added User Account!', '2015-11-30 19:37:51'),
	(189, 48, 3, 'logout', 'index', 0, '', '2015-11-30 19:39:21'),
	(190, 1, 1, 'user', 'add', 49, 'Successfully added User Account!', '2015-11-30 19:41:14'),
	(191, 49, 3, 'logout', 'index', 0, '', '2015-11-30 19:42:35'),
	(192, 1, 1, 'project', 'add', 11, 'Successfully added project!', '2015-11-30 19:43:11'),
	(193, 1, 1, 'project', 'add', 12, 'Successfully added project!', '2015-11-30 19:43:32'),
	(194, 1, 1, 'project', 'add', 13, 'Successfully added project!', '2015-11-30 19:44:08'),
	(195, 1, 1, 'user', 'add', 50, 'Successfully added User Account!', '2015-11-30 20:00:42'),
	(196, 50, 2, 'logout', 'index', 0, '', '2015-11-30 20:02:31'),
	(197, 1, 1, 'user', 'add', 51, 'Successfully added User Account!', '2015-11-30 20:04:14'),
	(198, 51, 2, 'logout', 'index', 0, '', '2015-11-30 20:05:33'),
	(199, 1, 1, 'client_group', 'add', 2, 'Successfully added employee client!', '2015-11-30 20:07:22'),
	(200, 1, 1, 'user', 'add', 52, 'Successfully added User Account!', '2015-11-30 20:09:29'),
	(201, 51, 2, 'logout', 'index', 0, '', '2015-11-30 20:10:08'),
	(202, 51, 2, 'logout', 'index', 0, '', '2015-11-30 20:11:05'),
	(203, 52, 2, 'logout', 'index', 0, '', '2015-11-30 20:11:18'),
	(204, 1, 1, 'user', 'add', 53, 'Successfully added User Account!', '2015-11-30 20:12:25'),
	(205, 53, 2, 'logout', 'index', 0, '', '2015-11-30 20:13:58'),
	(206, 1, 1, 'user', 'add', 54, 'Successfully added User Account!', '2015-11-30 20:14:47'),
	(207, 54, 2, 'logout', 'index', 0, '', '2015-11-30 20:16:17'),
	(208, 46, 5, 'employee_list', 'add', 6, 'Successfully added employee rate.', '2015-11-30 20:22:39'),
	(209, 46, 5, 'employee_list', 'add', 7, 'Successfully added employee rate.', '2015-11-30 20:24:07'),
	(210, 46, 5, 'employee_list', 'add', 8, 'Successfully added employee rate.', '2015-11-30 20:25:20'),
	(211, 50, 2, 'timesheet', 'edit', 18, 'Timesheet Submitted', '2015-11-30 20:35:04'),
	(212, 50, 2, 'logout', 'index', 0, '', '2015-11-30 20:35:14'),
	(213, 51, 2, 'timesheet', 'edit', 19, 'Timesheet Submitted', '2015-11-30 20:49:48'),
	(214, 51, 2, 'logout', 'index', 0, '', '2015-11-30 20:50:10'),
	(215, 52, 2, 'timesheet', 'edit', 20, 'Timesheet Submitted', '2015-11-30 20:59:05'),
	(216, 52, 2, 'logout', 'index', 0, '', '2015-11-30 20:59:15'),
	(217, 53, 2, 'timesheet', 'edit', 21, 'Timesheet Submitted', '2015-11-30 21:23:09'),
	(218, 0, 0, 'logout', 'index', 0, '', '2015-11-30 21:23:53'),
	(219, 53, 2, 'logout', 'index', 0, '', '2015-11-30 21:24:51'),
	(220, 48, 3, 'for_pre_approval', 'view', 18, 'Time Report succcessfully change to Pre-approved', '2015-11-30 21:25:34'),
	(221, 48, 3, 'for_pre_approval', 'view', 19, 'Time Report succcessfully change to Pre-approved', '2015-11-30 21:25:44'),
	(222, 48, 3, 'for_pre_approval', 'view', 20, 'Time Report succcessfully change to Pre-approved', '2015-11-30 21:25:58'),
	(223, 48, 3, 'for_pre_approval', 'view', 21, 'Time Report succcessfully change to Pre-approved', '2015-11-30 21:26:04'),
	(224, 4, 4, 'for_approval', 'index', 18, 'Time Report succcessfully change to Approved', '2015-11-30 21:26:45'),
	(225, 4, 4, 'for_approval', 'index', 19, 'Time Report succcessfully change to Approved', '2015-11-30 21:26:45'),
	(226, 4, 4, 'for_approval', 'view', 20, 'Time Report succcessfully change to Approved', '2015-11-30 21:27:47'),
	(227, 4, 4, 'for_approval', 'view', 21, 'Time Report succcessfully change to Approved', '2015-11-30 21:28:03'),
	(228, 1, 1, 'user', 'add', 55, 'Successfully added User Account!', '2015-11-30 21:42:36'),
	(229, 48, 3, 'logout', 'index', 0, '', '2015-11-30 21:42:58'),
	(230, 55, 3, 'logout', 'index', 0, '', '2015-11-30 21:47:43'),
	(231, 1, 1, 'user', 'add', 56, 'Successfully added User Account!', '2015-11-30 21:49:10'),
	(232, 56, 3, 'logout', 'index', 0, '', '2015-11-30 21:50:37'),
	(233, 1, 1, 'user', 'add', 57, 'Successfully added User Account!', '2015-11-30 21:55:26'),
	(234, 57, 3, 'logout', 'index', 0, '', '2015-11-30 21:57:41'),
	(235, 1, 1, 'project', 'add', 14, 'Successfully added project!', '2015-11-30 21:58:54'),
	(236, 1, 1, 'project', 'add', 15, 'Successfully added project!', '2015-11-30 21:59:26'),
	(237, 1, 1, 'project', 'add', 16, 'Successfully added project!', '2015-11-30 22:00:24'),
	(238, 1, 1, 'client_group', 'add', 3, 'Successfully added employee client!', '2015-11-30 22:01:41'),
	(239, 1, 1, 'user', 'add', 58, 'Successfully added User Account!', '2015-11-30 22:05:03'),
	(240, 1, 1, 'user', 'add', 59, 'Successfully added User Account!', '2015-11-30 22:12:41'),
	(241, 58, 5, 'logout', 'index', 0, '', '2015-11-30 22:13:55'),
	(242, 59, 6, 'logout', 'index', 0, '', '2015-11-30 22:15:48'),
	(243, 1, 1, 'user', 'add', 60, 'Successfully added User Account!', '2015-11-30 22:18:04'),
	(244, 59, 6, 'logout', 'index', 0, '', '2015-11-30 22:18:40'),
	(245, 60, 2, 'logout', 'index', 0, '', '2015-11-30 22:19:43'),
	(246, 1, 1, 'user', 'add', 61, 'Successfully added User Account!', '2015-11-30 22:20:41'),
	(247, 61, 2, 'logout', 'index', 0, '', '2015-11-30 22:21:53'),
	(248, 1, 1, 'user', 'add', 62, 'Successfully added User Account!', '2015-11-30 22:23:36'),
	(249, 62, 2, 'logout', 'index', 0, '', '2015-11-30 22:24:55'),
	(250, 1, 1, 'user', 'add', 63, 'Successfully added User Account!', '2015-11-30 22:26:26'),
	(251, 63, 2, 'logout', 'index', 0, '', '2015-11-30 22:27:42'),
	(252, 1, 1, 'user', 'add', 64, 'Successfully added User Account!', '2015-11-30 22:28:24'),
	(253, 64, 2, 'logout', 'index', 0, '', '2015-11-30 22:29:34'),
	(254, 46, 5, 'employee_list', 'add', 9, 'Successfully added employee rate.', '2015-11-30 22:30:29'),
	(255, 46, 5, 'employee_list', 'add', 10, 'Successfully added employee rate.', '2015-11-30 22:31:40'),
	(256, 46, 5, 'employee_list', 'add', 11, 'Successfully added employee rate.', '2015-11-30 22:33:04'),
	(257, 60, 2, 'timesheet', 'edit', 22, 'Timesheet Submitted', '2015-11-30 22:41:43'),
	(258, 60, 2, 'logout', 'index', 0, '', '2015-11-30 22:42:04'),
	(259, 61, 2, 'timesheet', 'edit', 23, 'Timesheet Submitted', '2015-11-30 22:51:27'),
	(260, 61, 2, 'logout', 'index', 0, '', '2015-11-30 22:51:37'),
	(261, 62, 2, 'timesheet', 'edit', 24, 'Timesheet Submitted', '2015-11-30 22:59:41'),
	(262, 62, 2, 'logout', 'index', 0, '', '2015-11-30 22:59:51'),
	(263, 63, 2, 'timesheet', 'edit', 25, 'Timesheet Submitted', '2015-11-30 23:08:41'),
	(264, 63, 2, 'logout', 'index', 0, '', '2015-11-30 23:08:57'),
	(265, 4, 4, 'logout', 'index', 0, '', '2015-11-30 23:09:39'),
	(266, 56, 3, 'for_pre_approval', 'view', 22, 'Time Report succcessfully change to Pre-approved', '2015-11-30 23:10:08'),
	(267, 56, 3, 'for_pre_approval', 'view', 23, 'Time Report succcessfully change to Pre-approved', '2015-11-30 23:10:14'),
	(268, 56, 3, 'for_pre_approval', 'view', 24, 'Time Report succcessfully change to Pre-approved', '2015-11-30 23:10:29'),
	(269, 56, 3, 'for_pre_approval', 'view', 25, 'Time Report succcessfully change to Pre-approved', '2015-11-30 23:10:35'),
	(270, 5, 4, 'for_approval', 'view', 22, 'Time Report succcessfully change to Approved', '2015-11-30 23:11:12'),
	(271, 5, 4, 'for_approval', 'view', 23, 'Time Report succcessfully change to Approved', '2015-11-30 23:11:28'),
	(272, 5, 4, 'for_approval', 'view', 24, 'Time Report succcessfully change to Approved', '2015-11-30 23:11:48'),
	(273, 5, 4, 'for_approval', 'index', 25, 'Time Report succcessfully change to Approved', '2015-11-30 23:11:52'),
	(274, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-11-30 23:15:18'),
	(275, 1, 1, 'logout', 'index', 0, '', '2015-12-01 13:36:34'),
	(276, 3, 4, 'logout', 'index', 0, '', '2015-12-01 14:10:16'),
	(277, 46, 5, 'logout', 'index', 0, '', '2015-12-01 14:34:16'),
	(278, 1, 1, 'logout', 'index', 0, '', '2015-12-01 14:55:02'),
	(279, 1, 1, 'logout', 'index', 0, '', '2015-12-01 15:05:06'),
	(280, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:05:44'),
	(281, 14, 2, 'logout', 'index', 0, '', '2015-12-01 15:05:49'),
	(282, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 15:06:33'),
	(283, 7, 3, 'logout', 'index', 0, '', '2015-12-01 15:08:14'),
	(284, 14, 2, 'logout', 'index', 0, '', '2015-12-01 15:08:50'),
	(285, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 15:09:20'),
	(286, 7, 3, 'logout', 'index', 0, '', '2015-12-01 15:09:24'),
	(287, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:10:07'),
	(288, 14, 2, 'logout', 'index', 0, '', '2015-12-01 15:10:10'),
	(289, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 15:10:57'),
	(290, 7, 3, 'logout', 'index', 0, '', '2015-12-01 15:11:02'),
	(291, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:19:11'),
	(292, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 15:43:55'),
	(293, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:44:21'),
	(294, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 15:44:40'),
	(295, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 15:48:01'),
	(296, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 15:48:10'),
	(297, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 15:53:27'),
	(298, 7, 3, 'logout', 'index', 0, '', '2015-12-01 16:12:29'),
	(299, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 16:14:07'),
	(300, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 16:16:10'),
	(301, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 16:16:27'),
	(302, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 16:16:49'),
	(303, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 16:16:59'),
	(304, 7, 3, 'for_pre_approval', 'view', 2, 'Time Report succcessfully change to Pre-approved', '2015-12-01 16:17:07'),
	(305, 7, 3, 'logout', 'index', 0, '', '2015-12-01 16:21:42'),
	(306, 2, 4, 'logout', 'index', 0, '', '2015-12-01 16:50:52'),
	(307, 7, 3, 'pre_approved', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 16:51:19'),
	(308, 7, 3, 'logout', 'index', 0, '', '2015-12-01 16:51:24'),
	(309, 1, 1, 'client', 'edit', 1, 'Client successfully updated!', '2015-12-01 16:51:44'),
	(310, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 16:51:52'),
	(311, 1, 1, 'logout', 'index', 0, '', '2015-12-01 16:52:02'),
	(312, 2, 4, 'for_approval', 'view', 2, 'Time Report succcessfully change to For re-work', '2015-12-01 17:11:46'),
	(313, 14, 2, 'timesheet', 'edit', 2, 'Timesheet Submitted', '2015-12-01 17:12:35'),
	(314, 2, 4, 'for_approval', 'view', 2, 'Time Report succcessfully change to Approved', '2015-12-01 17:12:48'),
	(315, 2, 4, 'logout', 'index', 0, '', '2015-12-01 17:13:12'),
	(316, 1, 1, 'client', 'edit', 1, 'Client successfully updated!', '2015-12-01 17:13:33'),
	(317, 1, 1, 'logout', 'index', 0, '', '2015-12-01 17:13:40'),
	(318, 14, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-12-01 17:14:04'),
	(319, 7, 3, 'for_pre_approval', 'view', 1, 'Time Report succcessfully change to Pre-approved', '2015-12-01 17:14:22'),
	(320, 7, 3, 'pre_approved', 'view', 1, 'Time Report succcessfully change to For re-work', '2015-12-01 17:14:38'),
	(321, 14, 2, 'timesheet', 'edit', 1, 'Timesheet Submitted', '2015-12-01 17:14:43'),
	(322, 7, 3, 'for_pre_approval', 'view', 1, 'Time Report succcessfully change to Pre-approved', '2015-12-01 17:14:51'),
	(323, 7, 3, 'logout', 'index', 0, '', '2015-12-01 17:14:55'),
	(324, 2, 4, 'for_approval', 'view', 1, 'Time Report succcessfully change to Approved', '2015-12-01 17:15:22'),
	(325, 0, 0, 'logout', 'index', 0, '', '2015-12-02 09:28:30'),
	(326, 1, 1, 'logout', 'index', 0, '', '2015-12-02 11:12:10'),
	(327, 1, 1, 'logout', 'index', 0, '', '2015-12-02 11:18:50'),
	(328, 1, 1, 'logout', 'index', 0, '', '2015-12-02 12:21:10'),
	(329, 0, 0, 'login', 'index', 0, '', '2015-12-02 12:21:29'),
	(330, 1, 1, 'appearance', 'index', 0, 'Change Site Title', '2015-12-02 12:28:46'),
	(331, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-12-02 12:31:49'),
	(332, 1, 1, 'user_profile', 'add', 2, 'Successfully added User Role!', '2015-12-02 12:42:59'),
	(333, 1, 1, 'user_profile', 'add', 3, 'Successfully added User Role!', '2015-12-02 12:43:13'),
	(334, 1, 1, 'user_profile', 'add', 4, 'Successfully added User Role!', '2015-12-02 12:43:30'),
	(335, 1, 1, 'user_profile', 'add', 5, 'Successfully added User Role!', '2015-12-02 12:44:07'),
	(336, 1, 1, 'user_profile', 'add', 6, 'Successfully added User Role!', '2015-12-02 12:44:26'),
	(337, 1, 1, 'user_profile', 'add', 7, 'Successfully added User Role!', '2015-12-02 12:44:43'),
	(338, 1, 1, 'user_profile', 'add', 8, 'Successfully added User Role!', '2015-12-02 12:44:59'),
	(339, 1, 1, 'user_profile', 'add', 9, 'Successfully added User Role!', '2015-12-02 12:45:13'),
	(340, 1, 1, 'user_profile', 'add', 10, 'Successfully added User Role!', '2015-12-02 12:47:43'),
	(341, 1, 1, 'user_profile', 'add', 11, 'Successfully added User Role!', '2015-12-02 12:47:53'),
	(342, 1, 1, 'logout', 'index', 0, '', '2015-12-02 14:57:15'),
	(343, 0, 0, 'login', 'index', 0, '', '2015-12-02 15:29:22'),
	(344, 1, 1, 'logout', 'index', 0, '', '2015-12-02 16:44:53'),
	(345, 0, 0, 'login', 'index', 0, '', '2015-12-02 16:45:01'),
	(346, 1, 1, 'logout', 'index', 0, '', '2015-12-02 17:46:23'),
	(347, 0, 0, 'login', 'index', 0, '', '2015-12-02 17:46:30'),
	(348, 14, 2, 'logout', 'index', 0, '', '2015-12-02 18:17:25'),
	(349, 0, 0, 'login', 'index', 0, '', '2015-12-03 09:37:07'),
	(350, 1, 1, 'logout', 'index', 0, '', '2015-12-03 11:40:24'),
	(351, 0, 0, 'login', 'index', 0, '', '2015-12-03 11:40:30'),
	(352, 1, 1, 'outlet', 'edit', 1, 'Successfully updated outlet', '2015-12-03 11:42:37'),
	(353, 1, 1, 'outlet', 'edit', 1, 'Successfully updated outlet', '2015-12-03 11:42:48'),
	(354, 1, 1, 'logout', 'index', 0, '', '2015-12-03 12:24:06'),
	(355, 0, 0, 'login', 'index', 0, '', '2015-12-03 12:24:12'),
	(356, 1, 1, 'user', 'add', 2, 'Successfully added User Account!', '2015-12-03 12:51:31'),
	(357, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-12-03 12:55:54'),
	(358, 1, 1, 'account_control', 'index', 0, 'Successfully updated system configuration!', '2015-12-03 12:56:02'),
	(359, 1, 1, 'user', 'add', 3, 'Successfully added User Account!', '2015-12-03 12:59:36'),
	(360, 1, 1, 'user', 'edit', 3, 'Successfully updated User Account!', '2015-12-03 15:11:40'),
	(361, 1, 1, 'user', 'edit', 3, 'Successfully updated User Account!', '2015-12-03 15:11:51'),
	(362, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-12-03 15:46:19'),
	(363, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-12-03 15:46:26'),
	(364, 1, 1, 'user_profile', 'edit', 1, 'Successfully updated User Role!', '2015-12-03 15:50:27'),
	(365, 1, 1, 'logout', 'index', 0, '', '2015-12-04 10:28:40'),
	(366, 0, 0, 'login', 'index', 0, '', '2015-12-04 10:28:51'),
	(367, 1, 1, 'logout', 'index', 0, '', '2015-12-04 10:31:55'),
	(368, 0, 0, 'login', 'index', 0, '', '2015-12-04 10:32:25'),
	(369, 1, 1, 'logout', 'index', 0, '', '2015-12-04 10:32:49'),
	(370, 0, 0, 'login', 'index', 0, '', '2015-12-04 13:49:08'),
	(371, 1, 1, 'logout', 'index', 0, '', '2016-01-20 11:02:54'),
	(372, 1, 1, 'port', 'add', 1, 'Successfully added Port', '2016-01-20 11:07:24'),
	(373, 1, 1, 'port', 'add', 2, 'Successfully added Port', '2016-01-20 11:07:40'),
	(374, 1, 1, 'port', 'add', 3, 'Successfully added Port', '2016-01-20 11:08:50'),
	(375, 1, 1, 'port', 'add', 4, 'Successfully added Port', '2016-01-20 11:09:04'),
	(376, 1, 1, 'vessel', 'add', 1, 'Successfully added vessel.', '2016-01-20 11:10:41'),
	(377, 1, 1, 'accommodation', 'add', 1, 'Successfully added Accommodation', '2016-01-20 11:10:43'),
	(378, 1, 1, 'accommodation', 'add', 2, 'Successfully added Accommodation', '2016-01-20 11:11:09'),
	(379, 1, 1, 'accommodation', 'add', 3, 'Successfully added Accommodation', '2016-01-20 11:11:22'),
	(380, 1, 1, 'vessel', 'add', 2, 'Successfully added vessel.', '2016-01-20 11:11:31'),
	(381, 1, 1, 'vessel', 'add', 3, 'Successfully added vessel.', '2016-01-20 11:11:50'),
	(382, 1, 1, 'vessel', 'add', 4, 'Successfully added vessel.', '2016-01-20 11:12:07'),
	(383, 1, 1, 'vessel', 'add', 5, 'Successfully added vessel.', '2016-01-20 11:12:29'),
	(384, 1, 1, 'evoucher', 'add', 1, 'Successfully added e-voucher.', '2016-01-20 11:13:28'),
	(385, 1, 1, 'ticket_series', 'add', 1, 'Successfully added Ticket series', '2016-01-20 11:13:35'),
	(386, 1, 1, 'evoucher', 'add', 2, 'Successfully added e-voucher.', '2016-01-20 11:13:48'),
	(387, 1, 1, 'user', 'edit', 3, 'Successfully updated User Account!', '2016-01-20 11:15:19'),
	(388, 1, 1, 'rule_set', 'add', 1, 'Successfully added Rule Set', '2016-01-20 11:15:42'),
	(389, 1, 1, 'rule_set', 'add', 2, 'Successfully added Rule Set', '2016-01-20 11:16:47'),
	(390, 1, 1, 'rule_set', 'add', 3, 'Successfully added Rule Set', '2016-01-20 11:17:36'),
	(391, 1, 1, 'rule_set', 'add', 4, 'Successfully added Rule Set', '2016-01-20 11:17:59'),
	(392, 1, 1, 'voyage', 'add', 1, 'Successfully added Voyage', '2016-01-20 11:19:49'),
	(393, 1, 1, 'voyage', 'add', 2, 'Successfully added Voyage', '2016-01-20 11:20:21'),
	(394, 1, 1, 'voyage', 'add', 3, 'Successfully added Voyage', '2016-01-20 11:22:20'),
	(395, 1, 1, 'voyage', 'add', 4, 'Successfully added Voyage', '2016-01-20 11:24:05'),
	(396, 1, 1, 'voyage_management', 'add', 2, 'Successfully added Voyage Management', '2016-01-20 11:24:33'),
	(397, 1, 1, 'voyage_management', 'add', 3, 'Successfully added Voyage Management', '2016-01-20 11:24:50'),
	(398, 1, 1, 'voyage_management', 'add', 4, 'Successfully added Voyage Management', '2016-01-20 11:25:06'),
	(399, 1, 1, 'voyage_management', 'add', 5, 'Successfully added Voyage Management', '2016-01-20 11:25:20'),
	(400, 1, 1, 'voyage', 'edit', 1, 'Successfully updated voyage', '2016-01-20 11:33:31'),
	(401, 1, 1, 'voyage', 'edit', 3, 'Successfully updated voyage', '2016-01-20 11:36:43'),
	(402, 1, 1, 'voyage', 'edit', 4, 'Successfully updated voyage', '2016-01-20 11:43:16'),
	(403, 1, 1, 'vessel', 'edit', 1, 'Vessel successfully updated.', '2016-01-20 11:43:52'),
	(404, 1, 1, 'user', 'add', 4, 'Successfully added User Account!', '2016-01-20 11:45:40'),
	(405, 1, 1, 'voyage', 'add', 5, 'Successfully added Voyage', '2016-01-20 11:47:22'),
	(406, 1, 1, 'voyage', 'edit', 3, 'Successfully updated voyage', '2016-01-20 11:47:37'),
	(407, 1, 1, 'voyage', 'edit', 2, 'Successfully updated voyage', '2016-01-20 11:49:30'),
	(408, 1, 1, 'accommodation', 'edit', 1, 'Successfully updated accommodation', '2016-01-20 11:50:04'),
	(409, 1, 1, 'voyage', 'edit', 5, 'Successfully updated voyage', '2016-01-20 11:50:40'),
	(410, 1, 1, 'vessel', 'edit', 1, 'Vessel successfully updated.', '2016-01-20 11:55:23'),
	(411, 1, 1, 'vessel', 'edit', 2, 'Vessel successfully updated.', '2016-01-20 11:55:52'),
	(412, 1, 1, 'vessel', 'edit', 3, 'Vessel successfully updated.', '2016-01-20 11:56:15'),
	(413, 1, 1, 'vessel', 'edit', 4, 'Vessel successfully updated.', '2016-01-20 11:56:26'),
	(414, 1, 1, 'vessel', 'edit', 5, 'Vessel successfully updated.', '2016-01-20 11:56:36'),
	(415, 1, 1, 'logout', 'index', 0, '', '2016-01-20 12:13:10'),
	(416, 0, 0, 'login', 'index', 0, '', '2016-01-20 12:30:23'),
	(417, 1, 1, 'accommodation', 'edit', 2, 'Successfully updated accommodation', '2016-01-20 12:37:06'),
	(418, 1, 1, 'accommodation', 'edit', 3, 'Successfully updated accommodation', '2016-01-20 12:40:11'),
	(419, 1, 1, 'accommodation', 'edit', 3, 'Successfully updated accommodation', '2016-01-20 12:40:26'),
	(420, 1, 1, 'accommodation', 'edit', 3, 'Successfully updated accommodation', '2016-01-20 12:44:37'),
	(421, 1, 1, 'accommodation', 'edit', 1, 'Successfully updated accommodation', '2016-01-20 12:44:57'),
	(422, 1, 1, 'accommodation', 'edit', 2, 'Successfully updated accommodation', '2016-01-20 12:45:21'),
	(423, 1, 1, 'accommodation', 'edit', 3, 'Successfully updated accommodation', '2016-01-20 12:45:58'),
	(424, 1, 1, 'voyage', 'add', 1, 'Successfully added Voyage', '2016-01-20 13:24:15'),
	(425, 1, 1, 'voyage', 'add', 2, 'Successfully added Voyage', '2016-01-20 13:27:44'),
	(426, 1, 1, 'voyage', 'add', 3, 'Successfully added Voyage', '2016-01-20 13:28:33'),
	(427, 1, 1, 'voyage', 'add', 4, 'Successfully added Voyage', '2016-01-20 13:33:33'),
	(428, 1, 1, 'voyage', 'add', 5, 'Successfully added Voyage', '2016-01-20 13:40:33'),
	(429, 1, 1, 'voyage', 'add', 6, 'Successfully added Voyage', '2016-01-20 13:41:34'),
	(430, 1, 1, 'voyage', 'add', 7, 'Successfully added Voyage', '2016-01-20 13:42:51'),
	(431, 1, 1, 'voyage', 'add', 8, 'Successfully added Voyage', '2016-01-20 13:45:54'),
	(432, 1, 1, 'voyage', 'add', 9, 'Successfully added Voyage', '2016-01-20 13:46:48'),
	(433, 1, 1, 'voyage', 'add', 10, 'Successfully added Voyage', '2016-01-20 13:52:55'),
	(434, 1, 1, 'voyage', 'add', 11, 'Successfully added Voyage', '2016-01-20 13:55:16'),
	(435, 1, 1, 'voyage', 'add', 12, 'Successfully added Voyage', '2016-01-20 13:56:04'),
	(436, 1, 1, 'voyage', 'add', 13, 'Successfully added Voyage', '2016-01-20 13:57:56'),
	(437, 1, 1, 'voyage', 'add', 14, 'Successfully added Voyage', '2016-01-20 13:58:55'),
	(438, 1, 1, 'voyage', 'add', 15, 'Successfully added Voyage', '2016-01-20 14:02:59'),
	(439, 1, 1, 'voyage', 'add', 16, 'Successfully added Voyage', '2016-01-20 14:05:01'),
	(440, 1, 1, 'voyage', 'add', 17, 'Successfully added Voyage', '2016-01-20 14:07:35'),
	(441, 1, 1, 'voyage', 'add', 18, 'Successfully added Voyage', '2016-01-20 14:11:24'),
	(442, 1, 1, 'rule_set', 'add', 5, 'Successfully added Rule Set', '2016-01-20 14:28:37'),
	(443, 1, 1, 'passenger_fare', 'add', 1, 'Successfully added Passenger Fare', '2016-01-20 14:39:09'),
	(444, 1, 1, 'passenger_fare', 'add', 2, 'Successfully added Passenger Fare', '2016-01-20 14:54:23'),
	(445, 1, 1, 'passenger_fare', 'edit', 2, 'Successfully updated Passenger Fare', '2016-01-20 14:55:10'),
	(446, 1, 1, 'passenger_fare', 'edit', 2, 'Successfully updated Passenger Fare', '2016-01-20 14:56:00'),
	(447, 1, 1, 'passenger_fare', 'add', 3, 'Successfully added Passenger Fare', '2016-01-20 14:57:27'),
	(448, 1, 1, 'passenger_fare', 'edit', 3, 'Successfully updated Passenger Fare', '2016-01-20 14:57:42'),
	(449, 1, 1, 'passenger_fare', 'edit', 3, 'Successfully updated Passenger Fare', '2016-01-20 15:01:19'),
	(450, 1, 1, 'voyage_management', 'add', 1, 'Successfully added Voyage Management', '2016-01-20 15:06:34'),
	(451, 1, 1, 'voyage_management', 'add', 5, 'Successfully added Subvoyage Management', '2016-01-20 15:06:34'),
	(452, 1, 1, 'voyage_management', 'add', 6, 'Successfully added Subvoyage Management', '2016-01-20 15:06:35'),
	(453, 1, 1, 'passenger_fare', 'add', 4, 'Successfully added Passenger Fare', '2016-01-20 15:06:56'),
	(454, 1, 1, 'voyage_management', 'edit', 5, 'Successfully updated Subvoyage Management', '2016-01-20 15:07:11'),
	(455, 1, 1, 'voyage_management', 'edit', 6, 'Successfully updated Subvoyage Management', '2016-01-20 15:07:11'),
	(456, 1, 1, 'voyage_management', 'add', 2, 'Successfully added Voyage Management', '2016-01-20 15:07:41'),
	(457, 1, 1, 'voyage_management', 'add', 7, 'Successfully added Subvoyage Management', '2016-01-20 15:07:41'),
	(458, 1, 1, 'voyage_management', 'add', 8, 'Successfully added Subvoyage Management', '2016-01-20 15:07:41'),
	(459, 1, 1, 'voyage_management', 'add', 3, 'Successfully added Voyage Management', '2016-01-20 15:07:56'),
	(460, 1, 1, 'voyage_management', 'add', 9, 'Successfully added Subvoyage Management', '2016-01-20 15:07:56'),
	(461, 1, 1, 'voyage_management', 'add', 10, 'Successfully added Subvoyage Management', '2016-01-20 15:07:56'),
	(462, 1, 1, 'voyage_management', 'add', 4, 'Successfully added Voyage Management', '2016-01-20 15:08:14'),
	(463, 1, 1, 'voyage_management', 'add', 11, 'Successfully added Subvoyage Management', '2016-01-20 15:08:14'),
	(464, 1, 1, 'voyage_management', 'add', 5, 'Successfully added Voyage Management', '2016-01-20 15:08:25'),
	(465, 1, 1, 'voyage_management', 'add', 12, 'Successfully added Subvoyage Management', '2016-01-20 15:08:25'),
	(466, 1, 1, 'voyage_management', 'add', 6, 'Successfully added Voyage Management', '2016-01-20 15:08:38'),
	(467, 1, 1, 'voyage_management', 'add', 13, 'Successfully added Subvoyage Management', '2016-01-20 15:08:39'),
	(468, 1, 1, 'voyage_management', 'add', 7, 'Successfully added Voyage Management', '2016-01-20 15:08:55'),
	(469, 1, 1, 'voyage_management', 'add', 14, 'Successfully added Subvoyage Management', '2016-01-20 15:08:55'),
	(470, 1, 1, 'voyage_management', 'add', 8, 'Successfully added Voyage Management', '2016-01-20 15:09:36'),
	(471, 1, 1, 'voyage_management', 'add', 15, 'Successfully added Subvoyage Management', '2016-01-20 15:09:37'),
	(472, 1, 1, 'voyage_management', 'add', 9, 'Successfully added Voyage Management', '2016-01-20 15:10:00'),
	(473, 1, 1, 'voyage_management', 'add', 16, 'Successfully added Subvoyage Management', '2016-01-20 15:10:00'),
	(474, 1, 1, 'voyage_management', 'add', 10, 'Successfully added Voyage Management', '2016-01-20 15:10:39'),
	(475, 1, 1, 'voyage_management', 'add', 17, 'Successfully added Subvoyage Management', '2016-01-20 15:10:39'),
	(476, 1, 1, 'voyage_management', 'add', 11, 'Successfully added Voyage Management', '2016-01-20 15:10:59'),
	(477, 1, 1, 'voyage_management', 'add', 18, 'Successfully added Subvoyage Management', '2016-01-20 15:11:00'),
	(478, 1, 1, 'voyage_management', 'add', 12, 'Successfully added Voyage Management', '2016-01-20 15:11:13'),
	(479, 1, 1, 'voyage_management', 'add', 19, 'Successfully added Subvoyage Management', '2016-01-20 15:11:13'),
	(480, 1, 1, 'voyage_management', 'add', 13, 'Successfully added Voyage Management', '2016-01-20 15:11:43'),
	(481, 1, 1, 'voyage_management', 'add', 20, 'Successfully added Subvoyage Management', '2016-01-20 15:11:43'),
	(482, 1, 1, 'voyage_management', 'add', 14, 'Successfully added Voyage Management', '2016-01-20 15:12:02'),
	(483, 1, 1, 'voyage_management', 'add', 21, 'Successfully added Subvoyage Management', '2016-01-20 15:12:02'),
	(484, 1, 1, 'voyage_management', 'add', 15, 'Successfully added Voyage Management', '2016-01-20 15:13:56'),
	(485, 1, 1, 'voyage_management', 'add', 22, 'Successfully added Subvoyage Management', '2016-01-20 15:13:56'),
	(486, 1, 1, 'voyage_management', 'add', 16, 'Successfully added Voyage Management', '2016-01-20 15:21:04'),
	(487, 1, 1, 'voyage_management', 'add', 23, 'Successfully added Subvoyage Management', '2016-01-20 15:21:05'),
	(488, 1, 1, 'voyage_management', 'add', 24, 'Successfully added Subvoyage Management', '2016-01-20 15:21:05'),
	(489, 1, 1, 'voyage_management', 'add', 25, 'Successfully added Subvoyage Management', '2016-01-20 15:21:05'),
	(490, 1, 1, 'voyage_management', 'add', 17, 'Successfully added Voyage Management', '2016-01-20 15:23:06'),
	(491, 1, 1, 'voyage_management', 'add', 26, 'Successfully added Subvoyage Management', '2016-01-20 15:23:06'),
	(492, 1, 1, 'voyage_management', 'add', 27, 'Successfully added Subvoyage Management', '2016-01-20 15:23:07'),
	(493, 1, 1, 'voyage_management', 'add', 28, 'Successfully added Subvoyage Management', '2016-01-20 15:23:07'),
	(494, 1, 1, 'passenger_fare', 'add', 5, 'Successfully added Passenger Fare', '2016-01-20 15:23:07'),
	(495, 1, 1, 'passenger_fare', 'edit', 5, 'Successfully updated Passenger Fare', '2016-01-20 15:23:18'),
	(496, 1, 1, 'voyage_management', 'add', 18, 'Successfully added Voyage Management', '2016-01-20 15:23:29'),
	(497, 1, 1, 'voyage_management', 'add', 29, 'Successfully added Subvoyage Management', '2016-01-20 15:23:29'),
	(498, 1, 1, 'voyage_management', 'add', 30, 'Successfully added Subvoyage Management', '2016-01-20 15:23:29'),
	(499, 1, 1, 'voyage_management', 'add', 31, 'Successfully added Subvoyage Management', '2016-01-20 15:23:29'),
	(500, 1, 1, 'passenger_fare', 'edit', 4, 'Successfully updated Passenger Fare', '2016-01-20 15:23:29'),
	(501, 1, 1, 'passenger_fare', 'add', 6, 'Successfully added Passenger Fare', '2016-01-20 15:27:31'),
	(502, 1, 1, 'voyage_management', 'add', 19, 'Successfully added Voyage Management', '2016-01-20 15:28:09'),
	(503, 1, 1, 'voyage_management', 'add', 32, 'Successfully added Subvoyage Management', '2016-01-20 15:28:09'),
	(504, 1, 1, 'voyage_management', 'add', 20, 'Successfully added Voyage Management', '2016-01-20 15:28:21'),
	(505, 1, 1, 'voyage_management', 'add', 33, 'Successfully added Subvoyage Management', '2016-01-20 15:28:21'),
	(506, 1, 1, 'voyage_management', 'add', 21, 'Successfully added Voyage Management', '2016-01-20 15:28:35'),
	(507, 1, 1, 'voyage_management', 'add', 34, 'Successfully added Subvoyage Management', '2016-01-20 15:28:35'),
	(508, 1, 1, 'voyage_management', 'add', 22, 'Successfully added Voyage Management', '2016-01-20 15:28:52'),
	(509, 1, 1, 'voyage_management', 'add', 35, 'Successfully added Subvoyage Management', '2016-01-20 15:28:52'),
	(510, 1, 1, 'voyage_management', 'add', 23, 'Successfully added Voyage Management', '2016-01-20 15:29:02'),
	(511, 1, 1, 'voyage_management', 'add', 36, 'Successfully added Subvoyage Management', '2016-01-20 15:29:02'),
	(512, 1, 1, 'voyage_management', 'add', 24, 'Successfully added Voyage Management', '2016-01-20 15:29:33'),
	(513, 1, 1, 'voyage_management', 'add', 37, 'Successfully added Subvoyage Management', '2016-01-20 15:29:33'),
	(514, 1, 1, 'voyage_management', 'add', 25, 'Successfully added Voyage Management', '2016-01-20 15:29:43'),
	(515, 1, 1, 'voyage_management', 'add', 38, 'Successfully added Subvoyage Management', '2016-01-20 15:29:43'),
	(516, 1, 1, 'voyage_management', 'add', 26, 'Successfully added Voyage Management', '2016-01-20 15:29:54'),
	(517, 1, 1, 'voyage_management', 'add', 39, 'Successfully added Subvoyage Management', '2016-01-20 15:29:54'),
	(518, 1, 1, 'voyage_management', 'add', 27, 'Successfully added Voyage Management', '2016-01-20 15:30:03'),
	(519, 1, 1, 'voyage_management', 'add', 40, 'Successfully added Subvoyage Management', '2016-01-20 15:30:03'),
	(520, 1, 1, 'voyage_management', 'add', 28, 'Successfully added Voyage Management', '2016-01-20 15:30:24'),
	(521, 1, 1, 'voyage_management', 'add', 41, 'Successfully added Subvoyage Management', '2016-01-20 15:30:24'),
	(522, 1, 1, 'passenger_fare', 'add', 7, 'Successfully added Passenger Fare', '2016-01-20 15:30:39'),
	(523, 1, 1, 'voyage_management', 'add', 29, 'Successfully added Voyage Management', '2016-01-20 15:30:53'),
	(524, 1, 1, 'voyage_management', 'add', 42, 'Successfully added Subvoyage Management', '2016-01-20 15:30:53'),
	(525, 1, 1, 'voyage_management', 'add', 43, 'Successfully added Subvoyage Management', '2016-01-20 15:30:53'),
	(526, 1, 1, 'voyage_management', 'add', 30, 'Successfully added Voyage Management', '2016-01-20 15:31:09'),
	(527, 1, 1, 'voyage_management', 'add', 44, 'Successfully added Subvoyage Management', '2016-01-20 15:31:09'),
	(528, 1, 1, 'voyage_management', 'add', 45, 'Successfully added Subvoyage Management', '2016-01-20 15:31:09'),
	(529, 1, 1, 'voyage_management', 'add', 31, 'Successfully added Voyage Management', '2016-01-20 15:31:29'),
	(530, 1, 1, 'voyage_management', 'add', 46, 'Successfully added Subvoyage Management', '2016-01-20 15:31:29'),
	(531, 1, 1, 'voyage_management', 'add', 47, 'Successfully added Subvoyage Management', '2016-01-20 15:31:29'),
	(532, 1, 1, 'voyage_management', 'add', 32, 'Successfully added Voyage Management', '2016-01-20 15:31:54'),
	(533, 1, 1, 'voyage_management', 'add', 48, 'Successfully added Subvoyage Management', '2016-01-20 15:31:54'),
	(534, 1, 1, 'voyage_management', 'add', 49, 'Successfully added Subvoyage Management', '2016-01-20 15:31:55'),
	(535, 1, 1, 'voyage_management', 'add', 33, 'Successfully added Voyage Management', '2016-01-20 15:32:11'),
	(536, 1, 1, 'voyage_management', 'add', 50, 'Successfully added Subvoyage Management', '2016-01-20 15:32:11'),
	(537, 1, 1, 'voyage_management', 'add', 51, 'Successfully added Subvoyage Management', '2016-01-20 15:32:11'),
	(538, 1, 1, 'voyage_management', 'add', 34, 'Successfully added Voyage Management', '2016-01-20 15:32:24'),
	(539, 1, 1, 'voyage_management', 'add', 52, 'Successfully added Subvoyage Management', '2016-01-20 15:32:24'),
	(540, 1, 1, 'voyage_management', 'add', 35, 'Successfully added Voyage Management', '2016-01-20 15:32:37'),
	(541, 1, 1, 'voyage_management', 'add', 53, 'Successfully added Subvoyage Management', '2016-01-20 15:32:37'),
	(542, 1, 1, 'voyage_management', 'add', 36, 'Successfully added Voyage Management', '2016-01-20 15:32:50'),
	(543, 1, 1, 'voyage_management', 'add', 54, 'Successfully added Subvoyage Management', '2016-01-20 15:32:50'),
	(544, 1, 1, 'passenger_fare', 'edit', 7, 'Successfully updated Passenger Fare', '2016-01-20 15:33:10'),
	(545, 1, 1, 'voyage_management', 'add', 37, 'Successfully added Voyage Management', '2016-01-20 15:33:15'),
	(546, 1, 1, 'voyage_management', 'add', 55, 'Successfully added Subvoyage Management', '2016-01-20 15:33:15'),
	(547, 1, 1, 'passenger_fare', 'edit', 7, 'Successfully updated Passenger Fare', '2016-01-20 15:33:18'),
	(548, 1, 1, 'passenger_fare', 'edit', 7, 'Successfully updated Passenger Fare', '2016-01-20 15:33:25'),
	(549, 1, 1, 'voyage_management', 'add', 38, 'Successfully added Voyage Management', '2016-01-20 15:33:30'),
	(550, 1, 1, 'voyage_management', 'add', 56, 'Successfully added Subvoyage Management', '2016-01-20 15:33:30'),
	(551, 1, 1, 'voyage_management', 'add', 39, 'Successfully added Voyage Management', '2016-01-20 15:33:43'),
	(552, 1, 1, 'voyage_management', 'add', 57, 'Successfully added Subvoyage Management', '2016-01-20 15:33:43'),
	(553, 1, 1, 'voyage_management', 'add', 40, 'Successfully added Voyage Management', '2016-01-20 15:34:11'),
	(554, 1, 1, 'voyage_management', 'add', 58, 'Successfully added Subvoyage Management', '2016-01-20 15:34:11'),
	(555, 1, 1, 'voyage_management', 'add', 59, 'Successfully added Subvoyage Management', '2016-01-20 15:34:11'),
	(556, 1, 1, 'voyage_management', 'add', 60, 'Successfully added Subvoyage Management', '2016-01-20 15:34:11'),
	(557, 1, 1, 'voyage_management', 'add', 41, 'Successfully added Voyage Management', '2016-01-20 15:35:05'),
	(558, 1, 1, 'voyage_management', 'add', 61, 'Successfully added Subvoyage Management', '2016-01-20 15:35:05'),
	(559, 1, 1, 'voyage_management', 'add', 62, 'Successfully added Subvoyage Management', '2016-01-20 15:35:05'),
	(560, 1, 1, 'voyage_management', 'add', 63, 'Successfully added Subvoyage Management', '2016-01-20 15:35:05'),
	(561, 1, 1, 'voyage_management', 'add', 42, 'Successfully added Voyage Management', '2016-01-20 15:35:28'),
	(562, 1, 1, 'voyage_management', 'add', 64, 'Successfully added Subvoyage Management', '2016-01-20 15:35:29'),
	(563, 1, 1, 'voyage_management', 'add', 65, 'Successfully added Subvoyage Management', '2016-01-20 15:35:29'),
	(564, 1, 1, 'voyage_management', 'add', 66, 'Successfully added Subvoyage Management', '2016-01-20 15:35:29'),
	(565, 1, 1, 'voyage_management', 'add', 43, 'Successfully added Voyage Management', '2016-01-20 15:36:39'),
	(566, 1, 1, 'voyage_management', 'add', 67, 'Successfully added Subvoyage Management', '2016-01-20 15:36:39'),
	(567, 1, 1, 'voyage_management', 'add', 68, 'Successfully added Subvoyage Management', '2016-01-20 15:36:39'),
	(568, 1, 1, 'voyage_management', 'add', 44, 'Successfully added Voyage Management', '2016-01-20 15:37:00'),
	(569, 1, 1, 'voyage_management', 'add', 69, 'Successfully added Subvoyage Management', '2016-01-20 15:37:00'),
	(570, 1, 1, 'voyage_management', 'add', 70, 'Successfully added Subvoyage Management', '2016-01-20 15:37:01'),
	(571, 1, 1, 'voyage_management', 'add', 45, 'Successfully added Voyage Management', '2016-01-20 15:37:26'),
	(572, 1, 1, 'voyage_management', 'add', 71, 'Successfully added Subvoyage Management', '2016-01-20 15:37:26'),
	(573, 1, 1, 'voyage_management', 'add', 72, 'Successfully added Subvoyage Management', '2016-01-20 15:37:26'),
	(574, 1, 1, 'voyage_management', 'add', 46, 'Successfully added Voyage Management', '2016-01-20 15:37:50'),
	(575, 1, 1, 'voyage_management', 'add', 73, 'Successfully added Subvoyage Management', '2016-01-20 15:37:50'),
	(576, 1, 1, 'voyage_management', 'add', 74, 'Successfully added Subvoyage Management', '2016-01-20 15:37:50'),
	(577, 1, 1, 'voyage_management', 'add', 47, 'Successfully added Voyage Management', '2016-01-20 15:38:08'),
	(578, 1, 1, 'voyage_management', 'add', 75, 'Successfully added Subvoyage Management', '2016-01-20 15:38:08'),
	(579, 1, 1, 'voyage_management', 'add', 48, 'Successfully added Voyage Management', '2016-01-20 15:38:24'),
	(580, 1, 1, 'voyage_management', 'add', 76, 'Successfully added Subvoyage Management', '2016-01-20 15:38:24'),
	(581, 1, 1, 'voyage_management', 'add', 49, 'Successfully added Voyage Management', '2016-01-20 15:38:35'),
	(582, 1, 1, 'voyage_management', 'add', 77, 'Successfully added Subvoyage Management', '2016-01-20 15:38:35'),
	(583, 1, 1, 'voyage_management', 'add', 50, 'Successfully added Voyage Management', '2016-01-20 15:38:45'),
	(584, 1, 1, 'voyage_management', 'add', 78, 'Successfully added Subvoyage Management', '2016-01-20 15:38:45'),
	(585, 1, 1, 'voyage_management', 'add', 51, 'Successfully added Voyage Management', '2016-01-20 15:40:50'),
	(586, 1, 1, 'voyage_management', 'add', 79, 'Successfully added Subvoyage Management', '2016-01-20 15:40:50'),
	(587, 1, 1, 'voyage_management', 'add', 52, 'Successfully added Voyage Management', '2016-01-20 15:41:12'),
	(588, 1, 1, 'voyage_management', 'add', 80, 'Successfully added Subvoyage Management', '2016-01-20 15:41:12'),
	(589, 1, 1, 'voyage_management', 'add', 53, 'Successfully added Voyage Management', '2016-01-20 15:41:23'),
	(590, 1, 1, 'voyage_management', 'add', 81, 'Successfully added Subvoyage Management', '2016-01-20 15:41:23'),
	(591, 1, 1, 'voyage_management', 'add', 54, 'Successfully added Voyage Management', '2016-01-20 15:41:42'),
	(592, 1, 1, 'voyage_management', 'add', 82, 'Successfully added Subvoyage Management', '2016-01-20 15:41:42'),
	(593, 1, 1, 'voyage_management', 'add', 55, 'Successfully added Voyage Management', '2016-01-20 15:41:52'),
	(594, 1, 1, 'voyage_management', 'add', 83, 'Successfully added Subvoyage Management', '2016-01-20 15:41:52'),
	(595, 1, 1, 'voyage_management', 'add', 56, 'Successfully added Voyage Management', '2016-01-20 15:42:02'),
	(596, 1, 1, 'voyage_management', 'add', 84, 'Successfully added Subvoyage Management', '2016-01-20 15:42:02'),
	(597, 1, 1, 'voyage_management', 'add', 57, 'Successfully added Voyage Management', '2016-01-20 15:42:14'),
	(598, 1, 1, 'voyage_management', 'add', 85, 'Successfully added Subvoyage Management', '2016-01-20 15:42:14'),
	(599, 1, 1, 'voyage_management', 'add', 58, 'Successfully added Voyage Management', '2016-01-20 15:42:26'),
	(600, 1, 1, 'voyage_management', 'add', 86, 'Successfully added Subvoyage Management', '2016-01-20 15:42:26'),
	(601, 1, 1, 'voyage_management', 'add', 59, 'Successfully added Voyage Management', '2016-01-20 15:42:37'),
	(602, 1, 1, 'voyage_management', 'add', 87, 'Successfully added Subvoyage Management', '2016-01-20 15:42:37'),
	(603, 1, 1, 'voyage_management', 'add', 60, 'Successfully added Voyage Management', '2016-01-20 15:42:56'),
	(604, 1, 1, 'voyage_management', 'add', 88, 'Successfully added Subvoyage Management', '2016-01-20 15:42:56'),
	(605, 1, 1, 'voyage_management', 'add', 89, 'Successfully added Subvoyage Management', '2016-01-20 15:42:57'),
	(606, 1, 1, 'voyage_management', 'add', 61, 'Successfully added Voyage Management', '2016-01-20 15:43:17'),
	(607, 1, 1, 'voyage_management', 'add', 90, 'Successfully added Subvoyage Management', '2016-01-20 15:43:17'),
	(608, 1, 1, 'voyage_management', 'add', 91, 'Successfully added Subvoyage Management', '2016-01-20 15:43:18'),
	(609, 1, 1, 'voyage_management', 'add', 62, 'Successfully added Voyage Management', '2016-01-20 15:43:37'),
	(610, 1, 1, 'voyage_management', 'add', 92, 'Successfully added Subvoyage Management', '2016-01-20 15:43:37'),
	(611, 1, 1, 'voyage_management', 'add', 93, 'Successfully added Subvoyage Management', '2016-01-20 15:43:37'),
	(612, 1, 1, 'voyage_management', 'add', 63, 'Successfully added Voyage Management', '2016-01-20 15:43:55'),
	(613, 1, 1, 'voyage_management', 'add', 94, 'Successfully added Subvoyage Management', '2016-01-20 15:43:55'),
	(614, 1, 1, 'voyage_management', 'add', 95, 'Successfully added Subvoyage Management', '2016-01-20 15:43:55'),
	(615, 1, 1, 'voyage_management', 'add', 64, 'Successfully added Voyage Management', '2016-01-20 15:44:12'),
	(616, 1, 1, 'voyage_management', 'add', 96, 'Successfully added Subvoyage Management', '2016-01-20 15:44:12'),
	(617, 1, 1, 'voyage_management', 'add', 97, 'Successfully added Subvoyage Management', '2016-01-20 15:44:12'),
	(618, 1, 1, 'voyage_management', 'add', 65, 'Successfully added Voyage Management', '2016-01-20 15:44:34'),
	(619, 1, 1, 'voyage_management', 'add', 98, 'Successfully added Subvoyage Management', '2016-01-20 15:44:34'),
	(620, 1, 1, 'voyage_management', 'add', 99, 'Successfully added Subvoyage Management', '2016-01-20 15:44:34'),
	(621, 1, 1, 'voyage_management', 'add', 66, 'Successfully added Voyage Management', '2016-01-20 15:44:51'),
	(622, 1, 1, 'voyage_management', 'add', 100, 'Successfully added Subvoyage Management', '2016-01-20 15:44:51'),
	(623, 1, 1, 'voyage_management', 'add', 67, 'Successfully added Voyage Management', '2016-01-20 15:45:00'),
	(624, 1, 1, 'voyage_management', 'add', 101, 'Successfully added Subvoyage Management', '2016-01-20 15:45:00'),
	(625, 1, 1, 'voyage_management', 'add', 68, 'Successfully added Voyage Management', '2016-01-20 15:45:12'),
	(626, 1, 1, 'voyage_management', 'add', 102, 'Successfully added Subvoyage Management', '2016-01-20 15:45:12'),
	(627, 1, 1, 'voyage_management', 'add', 69, 'Successfully added Voyage Management', '2016-01-20 15:45:22'),
	(628, 1, 1, 'voyage_management', 'add', 103, 'Successfully added Subvoyage Management', '2016-01-20 15:45:22'),
	(629, 1, 1, 'voyage_management', 'add', 70, 'Successfully added Voyage Management', '2016-01-20 15:45:32'),
	(630, 1, 1, 'voyage_management', 'add', 104, 'Successfully added Subvoyage Management', '2016-01-20 15:45:32'),
	(631, 1, 1, 'voyage_management', 'add', 71, 'Successfully added Voyage Management', '2016-01-20 15:48:00'),
	(632, 1, 1, 'voyage_management', 'add', 105, 'Successfully added Subvoyage Management', '2016-01-20 15:48:00'),
	(633, 1, 1, 'voyage_management', 'add', 72, 'Successfully added Voyage Management', '2016-01-20 15:48:11'),
	(634, 1, 1, 'voyage_management', 'add', 106, 'Successfully added Subvoyage Management', '2016-01-20 15:48:11'),
	(635, 1, 1, 'voyage_management', 'add', 73, 'Successfully added Voyage Management', '2016-01-20 15:48:23'),
	(636, 1, 1, 'voyage_management', 'add', 107, 'Successfully added Subvoyage Management', '2016-01-20 15:48:23'),
	(637, 1, 1, 'voyage_management', 'add', 74, 'Successfully added Voyage Management', '2016-01-20 15:48:47'),
	(638, 1, 1, 'voyage_management', 'add', 108, 'Successfully added Subvoyage Management', '2016-01-20 15:48:48'),
	(639, 1, 1, 'voyage_management', 'add', 75, 'Successfully added Voyage Management', '2016-01-20 15:48:58'),
	(640, 1, 1, 'voyage_management', 'add', 109, 'Successfully added Subvoyage Management', '2016-01-20 15:48:58'),
	(641, 1, 1, 'voyage_management', 'add', 76, 'Successfully added Voyage Management', '2016-01-20 15:49:18'),
	(642, 1, 1, 'voyage_management', 'add', 110, 'Successfully added Subvoyage Management', '2016-01-20 15:49:18'),
	(643, 1, 1, 'voyage_management', 'add', 77, 'Successfully added Voyage Management', '2016-01-20 15:49:27'),
	(644, 1, 1, 'voyage_management', 'add', 111, 'Successfully added Subvoyage Management', '2016-01-20 15:49:27'),
	(645, 1, 1, 'voyage_management', 'add', 78, 'Successfully added Voyage Management', '2016-01-20 15:49:36'),
	(646, 1, 1, 'voyage_management', 'add', 112, 'Successfully added Subvoyage Management', '2016-01-20 15:49:36'),
	(647, 1, 1, 'voyage_management', 'add', 79, 'Successfully added Voyage Management', '2016-01-20 15:49:44'),
	(648, 1, 1, 'voyage_management', 'add', 113, 'Successfully added Subvoyage Management', '2016-01-20 15:49:44'),
	(649, 1, 1, 'voyage_management', 'add', 80, 'Successfully added Voyage Management', '2016-01-20 15:49:56'),
	(650, 1, 1, 'voyage_management', 'add', 114, 'Successfully added Subvoyage Management', '2016-01-20 15:49:56'),
	(651, 1, 1, 'voyage_management', 'add', 81, 'Successfully added Voyage Management', '2016-01-20 15:50:07'),
	(652, 1, 1, 'voyage_management', 'add', 115, 'Successfully added Subvoyage Management', '2016-01-20 15:50:07'),
	(653, 1, 1, 'voyage_management', 'add', 82, 'Successfully added Voyage Management', '2016-01-20 15:50:22'),
	(654, 1, 1, 'voyage_management', 'add', 116, 'Successfully added Subvoyage Management', '2016-01-20 15:50:22'),
	(655, 1, 1, 'voyage_management', 'add', 117, 'Successfully added Subvoyage Management', '2016-01-20 15:50:22'),
	(656, 1, 1, 'voyage_management', 'add', 83, 'Successfully added Voyage Management', '2016-01-20 15:50:37'),
	(657, 1, 1, 'voyage_management', 'add', 118, 'Successfully added Subvoyage Management', '2016-01-20 15:50:38'),
	(658, 1, 1, 'voyage_management', 'add', 119, 'Successfully added Subvoyage Management', '2016-01-20 15:50:38'),
	(659, 0, 0, 'login', 'index', 0, '', '2016-01-20 18:00:38'),
	(660, 0, 0, 'login', 'index', 0, '', '2016-01-21 10:14:02'),
	(661, 0, 0, 'login', 'index', 0, '', '2016-01-21 13:50:20'),
	(662, 0, 0, 'login', 'index', 0, '', '2016-01-22 09:46:42'),
	(663, 0, 0, 'login', 'index', 0, '', '2016-01-22 14:16:14'),
	(664, 0, 0, 'login', 'index', 0, '', '2016-01-22 14:48:11'),
	(665, 0, 0, 'login', 'index', 0, '', '2016-01-25 10:06:14'),
	(666, 0, 0, 'login', 'index', 0, '', '2016-01-25 10:35:07');
/*!40000 ALTER TABLE `administrator_log` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.announcement
CREATE TABLE IF NOT EXISTS `announcement` (
  `id_announcement` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_code` varchar(50) NOT NULL,
  `announcement` varchar(255) NOT NULL,
  `announcement_content` longtext NOT NULL,
  `announcement_visibility` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_announcement`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.announcement: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.announcement_visibility
CREATE TABLE IF NOT EXISTS `announcement_visibility` (
  `id_announcement_visibility` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` int(11) unsigned NOT NULL,
  `user_role` int(11) NOT NULL,
  PRIMARY KEY (`id_announcement_visibility`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.announcement_visibility: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement_visibility` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement_visibility` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.booking
CREATE TABLE IF NOT EXISTS `booking` (
  `id_booking` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ticket_series_info_id` int(11) NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `passenger_fare_id` int(11) NOT NULL,
  `booking_source_id` int(11) NOT NULL,
  `booking_status_id` int(11) NOT NULL,
  `discount_id` int(11) unsigned NOT NULL,
  `points` decimal(13,2) NOT NULL,
  `jetter_no` varchar(32) DEFAULT NULL,
  `payment` varchar(32) NOT NULL,
  `fare_price` decimal(13,2) NOT NULL,
  `return_fare_price` decimal(13,2) NOT NULL,
  `total_discount` decimal(13,2) NOT NULL,
  `terminal_fee` decimal(13,2) NOT NULL,
  `port_charge` decimal(13,2) NOT NULL,
  `total_amount` decimal(13,2) NOT NULL,
  `reference_id` int(11) unsigned NOT NULL,
  `passenger_valid_id_number` varchar(64) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_booking`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.booking: ~1 rows (approximately)
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` (`id_booking`, `user_id`, `ticket_series_info_id`, `passenger_id`, `passenger_fare_id`, `booking_source_id`, `booking_status_id`, `discount_id`, `points`, `jetter_no`, `payment`, `fare_price`, `return_fare_price`, `total_discount`, `terminal_fee`, `port_charge`, `total_amount`, `reference_id`, `passenger_valid_id_number`, `date_added`, `date_update`) VALUES
	(1, 3, 339, 1, 1, 1, 8, 1, 1.00, '1', '1', 1.00, 1.00, 1.00, 0.00, 0.00, 0.00, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.booking_source
CREATE TABLE IF NOT EXISTS `booking_source` (
  `id_booking_source` int(11) NOT NULL AUTO_INCREMENT,
  `booking_source` varchar(64) NOT NULL,
  PRIMARY KEY (`id_booking_source`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.booking_source: ~4 rows (approximately)
/*!40000 ALTER TABLE `booking_source` DISABLE KEYS */;
INSERT INTO `booking_source` (`id_booking_source`, `booking_source`) VALUES
	(1, 'Online'),
	(2, 'Reservation'),
	(3, 'E-ticket'),
	(4, 'Pre-Printed');
/*!40000 ALTER TABLE `booking_source` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.booking_status
CREATE TABLE IF NOT EXISTS `booking_status` (
  `id_booking_status` int(11) NOT NULL AUTO_INCREMENT,
  `booking_status` varchar(64) NOT NULL,
  PRIMARY KEY (`id_booking_status`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.booking_status: ~8 rows (approximately)
/*!40000 ALTER TABLE `booking_status` DISABLE KEYS */;
INSERT INTO `booking_status` (`id_booking_status`, `booking_status`) VALUES
	(1, 'Issued'),
	(2, 'Void'),
	(3, 'Pending for Refund Request'),
	(4, 'Approved Refund'),
	(5, 'Disapproved Refund'),
	(6, 'Reschedule'),
	(7, 'Upgrade'),
	(8, 'Check-in');
/*!40000 ALTER TABLE `booking_status` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.cargo_description
CREATE TABLE IF NOT EXISTS `cargo_description` (
  `loose_cargo_checkin_id` int(11) NOT NULL,
  `particular` varchar(128) NOT NULL,
  `loose_cargo_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL,
  `amount` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.cargo_description: ~8 rows (approximately)
/*!40000 ALTER TABLE `cargo_description` DISABLE KEYS */;
INSERT INTO `cargo_description` (`loose_cargo_checkin_id`, `particular`, `loose_cargo_id`, `quantity`, `amount`) VALUES
	(1, 'Cargo01', 1, 0, 9.99),
	(1, 'Cargo02', 2, 0, 39.98),
	(2, 'Book', 1, 0, 9.99),
	(2, 'Notebook', 2, 0, 39.98),
	(3, 'Sabon', 1, 0, 9.99),
	(3, 'Sabon din', 2, 0, 19.99),
	(4, 'Sako ng bigas', 1, 0, 99.90),
	(5, 'Ball', 1, 0, 199.80);
/*!40000 ALTER TABLE `cargo_description` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.checkin_type
CREATE TABLE IF NOT EXISTS `checkin_type` (
  `id_checkin_type` int(11) NOT NULL AUTO_INCREMENT,
  `checkin_type` varchar(16) NOT NULL,
  PRIMARY KEY (`id_checkin_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.checkin_type: ~4 rows (approximately)
/*!40000 ALTER TABLE `checkin_type` DISABLE KEYS */;
INSERT INTO `checkin_type` (`id_checkin_type`, `checkin_type`) VALUES
	(1, 'Passenger'),
	(2, 'Baggage'),
	(3, 'Loose Cargo'),
	(4, 'Rolling Cargo');
/*!40000 ALTER TABLE `checkin_type` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.civil_status
CREATE TABLE IF NOT EXISTS `civil_status` (
  `id_civil_status` int(11) NOT NULL AUTO_INCREMENT,
  `civil_status_iso_code` varchar(3) NOT NULL,
  `civil_status_code` varchar(15) NOT NULL,
  `civil_status_status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_civil_status`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.civil_status: ~4 rows (approximately)
/*!40000 ALTER TABLE `civil_status` DISABLE KEYS */;
INSERT INTO `civil_status` (`id_civil_status`, `civil_status_iso_code`, `civil_status_code`, `civil_status_status`) VALUES
	(1, 'S', 'Single', 1),
	(2, 'M', 'Married', 1),
	(3, 'D', 'Divorced', 1),
	(4, 'W', 'Widow/er', 1);
/*!40000 ALTER TABLE `civil_status` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.commission
CREATE TABLE IF NOT EXISTS `commission` (
  `id_commission` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `commission_code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `commission` varchar(255) CHARACTER SET utf8 NOT NULL,
  `commission_percentage` decimal(13,2) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_commission`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.commission: ~0 rows (approximately)
/*!40000 ALTER TABLE `commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `commission` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.conditions
CREATE TABLE IF NOT EXISTS `conditions` (
  `id_conditions` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conditions` varchar(255) NOT NULL,
  PRIMARY KEY (`id_conditions`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.conditions: ~7 rows (approximately)
/*!40000 ALTER TABLE `conditions` DISABLE KEYS */;
INSERT INTO `conditions` (`id_conditions`, `conditions`) VALUES
	(1, 'Age Bracket'),
	(2, 'Selling Source'),
	(3, 'Require Advance Booking'),
	(4, 'Booking Period'),
	(5, 'Travel Period'),
	(6, 'Travel Days'),
	(7, 'Maximum Leg Interval');
/*!40000 ALTER TABLE `conditions` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.configuration
CREATE TABLE IF NOT EXISTS `configuration` (
  `id_configuration` int(11) NOT NULL AUTO_INCREMENT,
  `configuration` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `config_type` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id_configuration`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.configuration: ~39 rows (approximately)
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` (`id_configuration`, `configuration`, `value`, `config_type`) VALUES
	(1, 'EMAIL_PROTOCOL', 'mail', 1),
	(2, 'SMTP_DOMAIN', '', 1),
	(3, 'SMTP_HOST', '', 1),
	(4, 'SMTP_USER', '', 1),
	(5, 'SMTP_PASS', '', 1),
	(6, 'SMTP_PORT', '25', 1),
	(7, 'EMAIL_TYPE', 'html', 1),
	(8, 'DEBUG_MODE', '0', 1),
	(9, 'THEME', 'default', 1),
	(10, 'SITE_TITLE', 'Ocean Jet', 1),
	(11, 'CACHE_MODE', '0', 1),
	(12, 'CACHE_TIMEOUT', '2', 1),
	(13, 'DEFAULT_LANGUAGE', '1', 1),
	(14, 'ADMIN_PAGE_LOGO', '1', 1),
	(15, 'ADMIN_ITEMS', '100', 1),
	(16, 'SMTP_CC', '', 1),
	(17, 'COMP_LOGO', '', 1),
	(18, 'COMP_ADDR1', '', 1),
	(19, 'COMP_ADDR2', '', 1),
	(20, 'COMP_EMAIL', '', 1),
	(21, 'COMP_HEADER', '', 1),
	(22, 'COMP_FOOTER', '', 1),
	(23, 'COMP_HEADER', '', 1),
	(24, 'DEFAULT_COUNTRY', '', 1),
	(25, 'DEFAULT_TIME_ZONE', 'Asia/Manila', 1),
	(26, 'SMTP_BCC', '', 1),
	(27, 'UPLOAD_DIR', 'e:\\titanium', 1),
	(28, 'NUM_PASSWORD_CHANGE', '4', 1),
	(29, 'REQ_PASSWORD_CHANGE', '90', 1),
	(30, 'PASSWORD_REQ_UPPERCASE', '0', 1),
	(31, 'PASSWORD_REQ_NUMBER', '0', 1),
	(32, 'PASSWORD_REQ_SPL_CHAR', '1', 1),
	(33, 'LOGIN_ATTEMPT', '5', 1),
	(34, 'ENABLE_LOCKED_ACCT', '10', 1),
	(35, 'RE_AUTHENTICATE_ACCT', '60', 1),
	(36, 'INACTIVE_ACCT', '90', 1),
	(37, 'MIN_PASSWORD_CHAR', '5', 1),
	(38, 'MAX_PASSWORD_CHAR', '7', 1),
	(39, 'PASSWORD_REQ_LOWERCASE', '0', 1);
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.debtor
CREATE TABLE IF NOT EXISTS `debtor` (
  `id_debtor` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_code` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `commission_id` int(11) NOT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `enabled` int(11) NOT NULL,
  PRIMARY KEY (`id_debtor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.debtor: ~0 rows (approximately)
/*!40000 ALTER TABLE `debtor` DISABLE KEYS */;
/*!40000 ALTER TABLE `debtor` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.debtor_outlet
CREATE TABLE IF NOT EXISTS `debtor_outlet` (
  `id_debtor_outlet` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_id` int(11) DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_debtor_outlet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.debtor_outlet: ~0 rows (approximately)
/*!40000 ALTER TABLE `debtor_outlet` DISABLE KEYS */;
/*!40000 ALTER TABLE `debtor_outlet` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.debtor_wallet
CREATE TABLE IF NOT EXISTS `debtor_wallet` (
  `id_debtor_wallet` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_id` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `payment_account_id` int(11) NOT NULL,
  `deposit_date` datetime NOT NULL,
  `reference` text NOT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_debtor_wallet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.debtor_wallet: ~0 rows (approximately)
/*!40000 ALTER TABLE `debtor_wallet` DISABLE KEYS */;
/*!40000 ALTER TABLE `debtor_wallet` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.discounts
CREATE TABLE IF NOT EXISTS `discounts` (
  `id_discount` int(11) NOT NULL AUTO_INCREMENT,
  `discount_code` varchar(100) NOT NULL,
  `discount` varchar(100) NOT NULL,
  `discount_type` varchar(20) NOT NULL,
  `discount_amount` decimal(13,2) NOT NULL,
  `age_from` int(11) NOT NULL,
  `age_to` int(11) NOT NULL,
  `vat_exempt` tinyint(2) NOT NULL,
  `require_id` tinyint(2) NOT NULL,
  `enabled` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_discount`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.discounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `discounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `discounts` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.eticket
CREATE TABLE IF NOT EXISTS `eticket` (
  `id_eticket` int(11) NOT NULL AUTO_INCREMENT,
  `eticket_details` varchar(128) NOT NULL,
  `eticket_terms` varchar(3000) NOT NULL,
  `eticket_reminders` varchar(3000) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_eticket`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.eticket: ~1 rows (approximately)
/*!40000 ALTER TABLE `eticket` DISABLE KEYS */;
INSERT INTO `eticket` (`id_eticket`, `eticket_details`, `eticket_terms`, `eticket_reminders`, `date_added`, `date_update`) VALUES
	(1, 'Details', 'Terms', 'Reminders', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `eticket` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.evoucher
CREATE TABLE IF NOT EXISTS `evoucher` (
  `id_evoucher` int(11) NOT NULL AUTO_INCREMENT,
  `evoucher` varchar(128) NOT NULL,
  `origin_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `accommodation_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `alloted_evoucher` int(10) unsigned NOT NULL,
  `generated_evoucher` int(10) unsigned NOT NULL,
  `cancelled_evoucher` int(10) unsigned NOT NULL,
  `evoucher_type` tinyint(1) NOT NULL,
  `evoucher_amount` decimal(13,2) NOT NULL,
  `status` varchar(8) NOT NULL,
  PRIMARY KEY (`id_evoucher`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.evoucher: ~2 rows (approximately)
/*!40000 ALTER TABLE `evoucher` DISABLE KEYS */;
INSERT INTO `evoucher` (`id_evoucher`, `evoucher`, `origin_id`, `destination_id`, `accommodation_id`, `quantity`, `alloted_evoucher`, `generated_evoucher`, `cancelled_evoucher`, `evoucher_type`, `evoucher_amount`, `status`) VALUES
	(1, 'Voucher 001', 1, 3, 1, 100, 0, 0, 0, 1, 100.00, 'New'),
	(2, 'Voucher 002', 1, 2, 3, 200, 0, 0, 0, 2, 50.00, 'New');
/*!40000 ALTER TABLE `evoucher` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.evoucher_series
CREATE TABLE IF NOT EXISTS `evoucher_series` (
  `id_evoucher_series` int(11) NOT NULL AUTO_INCREMENT,
  `evoucher_id` int(11) NOT NULL,
  `evoucher_code` varchar(32) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `estatus` varchar(16) NOT NULL,
  `date_generated` date DEFAULT NULL,
  `date_used` date DEFAULT NULL,
  PRIMARY KEY (`id_evoucher_series`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.evoucher_series: ~0 rows (approximately)
/*!40000 ALTER TABLE `evoucher_series` DISABLE KEYS */;
/*!40000 ALTER TABLE `evoucher_series` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.excess_baggage
CREATE TABLE IF NOT EXISTS `excess_baggage` (
  `id_excess_baggage` int(11) NOT NULL AUTO_INCREMENT,
  `excess_baggage` varchar(36) NOT NULL,
  `description` text NOT NULL,
  `voyage_id` int(11) NOT NULL,
  `amount` decimal(6,2) NOT NULL,
  `unit_of_measurement` varchar(8) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_excess_baggage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.excess_baggage: ~0 rows (approximately)
/*!40000 ALTER TABLE `excess_baggage` DISABLE KEYS */;
/*!40000 ALTER TABLE `excess_baggage` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.gender
CREATE TABLE IF NOT EXISTS `gender` (
  `id_gender` int(11) NOT NULL AUTO_INCREMENT,
  `gender_iso_code` varchar(3) NOT NULL,
  `gender_code` varchar(15) NOT NULL,
  `gender_status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_gender`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.gender: ~2 rows (approximately)
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` (`id_gender`, `gender_iso_code`, `gender_code`, `gender_status`) VALUES
	(1, 'M', 'Male', 1),
	(2, 'F', 'Female', 1);
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.loose_cargo
CREATE TABLE IF NOT EXISTS `loose_cargo` (
  `id_loose_cargo` int(11) NOT NULL AUTO_INCREMENT,
  `loose_cargo` varchar(36) NOT NULL,
  `description` text NOT NULL,
  `voyage_id` int(11) NOT NULL,
  `amount` decimal(6,2) NOT NULL,
  `unit_of_measurement` varchar(8) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_loose_cargo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.loose_cargo: ~0 rows (approximately)
/*!40000 ALTER TABLE `loose_cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `loose_cargo` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.loose_cargo_checkin
CREATE TABLE IF NOT EXISTS `loose_cargo_checkin` (
  `id_loose_cargo_checkin` int(11) NOT NULL AUTO_INCREMENT,
  `departure_date` date NOT NULL,
  `voyage_id` int(11) NOT NULL,
  `reference_no` varchar(24) NOT NULL,
  `sender_first_name` varchar(64) NOT NULL,
  `sender_last_name` varchar(32) NOT NULL,
  `sender_middle_initial` varchar(3) NOT NULL,
  `sender_contact_no` varchar(16) NOT NULL,
  `recipient_first_name` varchar(64) NOT NULL,
  `recipient_last_name` varchar(32) NOT NULL,
  `recipient_middle_initial` varchar(3) NOT NULL,
  `recipient_contact_no` varchar(16) NOT NULL,
  PRIMARY KEY (`id_loose_cargo_checkin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.loose_cargo_checkin: ~0 rows (approximately)
/*!40000 ALTER TABLE `loose_cargo_checkin` DISABLE KEYS */;
/*!40000 ALTER TABLE `loose_cargo_checkin` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.month
CREATE TABLE IF NOT EXISTS `month` (
  `id_month` int(11) NOT NULL,
  `month` varchar(128) NOT NULL,
  PRIMARY KEY (`id_month`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.month: 12 rows
/*!40000 ALTER TABLE `month` DISABLE KEYS */;
INSERT INTO `month` (`id_month`, `month`) VALUES
	(1, 'January'),
	(2, 'February'),
	(3, 'March'),
	(4, 'April'),
	(5, 'May'),
	(6, 'June'),
	(7, 'July'),
	(8, 'August'),
	(9, 'September'),
	(10, 'October'),
	(11, 'November'),
	(12, 'December');
/*!40000 ALTER TABLE `month` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.outlet
CREATE TABLE IF NOT EXISTS `outlet` (
  `id_outlet` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `port_id` int(11) unsigned NOT NULL,
  `outlet_code` varchar(50) CHARACTER SET utf8 NOT NULL,
  `outlet_type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `outlet` varchar(255) CHARACTER SET utf8 NOT NULL,
  `outlet_address` longtext CHARACTER SET utf8 NOT NULL,
  `outlet_commission` decimal(13,2) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_outlet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.outlet: ~0 rows (approximately)
/*!40000 ALTER TABLE `outlet` DISABLE KEYS */;
/*!40000 ALTER TABLE `outlet` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.passenger
CREATE TABLE IF NOT EXISTS `passenger` (
  `id_passenger` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(64) NOT NULL,
  `middlename` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) NOT NULL,
  `birthdate` date NOT NULL,
  `age` varchar(64) DEFAULT NULL,
  `gender` varchar(32) NOT NULL,
  `contactno` varchar(32) DEFAULT NULL,
  `id_no` varchar(64) DEFAULT NULL,
  `nationality` varchar(64) DEFAULT NULL,
  `with_infant` tinyint(1) DEFAULT NULL,
  `infant_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_passenger`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.passenger: ~1 rows (approximately)
/*!40000 ALTER TABLE `passenger` DISABLE KEYS */;
INSERT INTO `passenger` (`id_passenger`, `firstname`, `middlename`, `lastname`, `birthdate`, `age`, `gender`, `contactno`, `id_no`, `nationality`, `with_infant`, `infant_name`) VALUES
	(1, 'Gian', 'Asuncion', 'Zapatero', '0000-00-00', NULL, '', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `passenger` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.passenger_baggage_checkin
CREATE TABLE IF NOT EXISTS `passenger_baggage_checkin` (
  `id_passenger_baggage_checkin` int(11) NOT NULL AUTO_INCREMENT,
  `voyage_management_id` int(11) DEFAULT NULL,
  `reference_no` varchar(255) DEFAULT NULL,
  `ticket_series_info_id` int(11) DEFAULT NULL,
  `passenger_id` int(11) DEFAULT NULL,
  `excess_baggage_id` int(11) DEFAULT NULL,
  `no_of_bags` int(11) DEFAULT NULL,
  `quantity` decimal(10,2) DEFAULT NULL,
  `rule_type_id` int(11) DEFAULT NULL,
  `attended_baggage` decimal(10,2) DEFAULT NULL,
  `paid_status` int(11) DEFAULT NULL COMMENT '0: not paid| 1: paid',
  `cash` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id_passenger_baggage_checkin`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.passenger_baggage_checkin: ~0 rows (approximately)
/*!40000 ALTER TABLE `passenger_baggage_checkin` DISABLE KEYS */;
/*!40000 ALTER TABLE `passenger_baggage_checkin` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.passenger_checkin
CREATE TABLE IF NOT EXISTS `passenger_checkin` (
  `id_passenger_checkin` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `vessel_seat_id` int(11) NOT NULL,
  `date_checkedin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `voyage_management_id` int(11) NOT NULL,
  PRIMARY KEY (`id_passenger_checkin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.passenger_checkin: ~0 rows (approximately)
/*!40000 ALTER TABLE `passenger_checkin` DISABLE KEYS */;
/*!40000 ALTER TABLE `passenger_checkin` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.passenger_fare
CREATE TABLE IF NOT EXISTS `passenger_fare` (
  `id_passenger_fare` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `passenger_fare` varchar(50) NOT NULL,
  `rule_set_id` int(11) unsigned NOT NULL,
  `accommodation_id` int(11) unsigned NOT NULL,
  `voyage_id` int(11) unsigned NOT NULL,
  `seat_limit` int(5) unsigned NOT NULL,
  `regular_fare` tinyint(1) unsigned NOT NULL,
  `default_fare` tinyint(1) unsigned NOT NULL,
  `points_earned` int(5) unsigned NOT NULL,
  `points_redemption` int(5) unsigned NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_passenger_fare`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.passenger_fare: ~1 rows (approximately)
/*!40000 ALTER TABLE `passenger_fare` DISABLE KEYS */;
INSERT INTO `passenger_fare` (`id_passenger_fare`, `passenger_fare`, `rule_set_id`, `accommodation_id`, `voyage_id`, `seat_limit`, `regular_fare`, `default_fare`, `points_earned`, `points_redemption`, `enabled`, `date_added`, `date_update`) VALUES
	(8, 'PF014', 1, 1, 1, 44, 1, 0, 55, 0, 1, '2016-01-20 15:43:28', '2016-01-20 15:43:28');
/*!40000 ALTER TABLE `passenger_fare` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.passenger_fare_conditions
CREATE TABLE IF NOT EXISTS `passenger_fare_conditions` (
  `id_passenger_fare_conditions` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `passenger_fare_id` int(11) unsigned NOT NULL,
  `conditions_id` tinyint(2) unsigned NOT NULL,
  `age_bracket_from` tinyint(2) unsigned NOT NULL,
  `age_bracket_to` tinyint(3) unsigned NOT NULL,
  `outlet_id` int(11) unsigned NOT NULL,
  `is_advance_booking` tinyint(2) unsigned NOT NULL,
  `booking_period_from` datetime NOT NULL,
  `booking_period_to` datetime NOT NULL,
  `travel_period_from` datetime NOT NULL,
  `travel_period_to` datetime NOT NULL,
  `travel_days` varchar(10) NOT NULL,
  `max_leg_interval` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_passenger_fare_conditions`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.passenger_fare_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `passenger_fare_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `passenger_fare_conditions` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.payment_account
CREATE TABLE IF NOT EXISTS `payment_account` (
  `id_payment_account` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `payment_account` varchar(50) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `bank_account_name` varchar(255) NOT NULL,
  `bank_account_no` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_payment_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.payment_account: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_account` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.port
CREATE TABLE IF NOT EXISTS `port` (
  `id_port` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `port_code` varchar(50) NOT NULL,
  `port` varchar(255) NOT NULL,
  `terminal_fee` decimal(13,2) unsigned NOT NULL,
  `port_charge` decimal(13,2) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_port`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.port: ~4 rows (approximately)
/*!40000 ALTER TABLE `port` DISABLE KEYS */;
INSERT INTO `port` (`id_port`, `port_code`, `port`, `terminal_fee`, `port_charge`, `enabled`, `date_added`, `date_update`) VALUES
	(1, 'CEB', 'Cebu', 100.00, 50.00, 1, '2016-01-20 11:07:24', '2016-01-20 11:07:24'),
	(2, 'TAG', 'Tagbilaran', 200.00, 50.00, 1, '2016-01-20 11:07:40', '2016-01-20 11:07:40'),
	(3, 'DUM', 'Dumaguete', 150.00, 50.00, 1, '2016-01-20 11:08:50', '2016-01-20 11:08:50'),
	(4, 'SIQ', 'Siquijor', 200.00, 50.00, 1, '2016-01-20 11:09:04', '2016-01-20 11:09:04');
/*!40000 ALTER TABLE `port` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.refund_tickets
CREATE TABLE IF NOT EXISTS `refund_tickets` (
  `id_refund_tickets` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `refund_amount` decimal(13,2) DEFAULT NULL,
  `date_refunded` datetime DEFAULT NULL,
  PRIMARY KEY (`id_refund_tickets`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.refund_tickets: ~0 rows (approximately)
/*!40000 ALTER TABLE `refund_tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `refund_tickets` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.reservation
CREATE TABLE IF NOT EXISTS `reservation` (
  `id_reservation` int(11) NOT NULL AUTO_INCREMENT,
  `reservation` varchar(128) NOT NULL,
  `contact_firstname` varchar(128) NOT NULL,
  `contact_lastname` varchar(128) NOT NULL,
  `contact_number` varchar(64) NOT NULL,
  `pax` int(11) NOT NULL,
  `outlet_id` int(11) NOT NULL,
  `expiration_date` date NOT NULL,
  `reservation_code` varchar(64) NOT NULL,
  `passenger_fare_id` int(11) NOT NULL,
  `discount_id` int(11) unsigned NOT NULL,
  `payment` varchar(64) NOT NULL,
  `fare_price` decimal(13,2) NOT NULL,
  `return_fare_price` decimal(13,2) NOT NULL,
  `total_discount` decimal(13,2) NOT NULL,
  `terminal_fee` decimal(13,2) NOT NULL,
  `port_charge` decimal(13,2) NOT NULL,
  `total_amount` decimal(13,2) NOT NULL,
  `passenger_valid_id_number` varchar(64) DEFAULT NULL,
  `reservation_status_id` int(11) NOT NULL,
  PRIMARY KEY (`id_reservation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.reservation: ~0 rows (approximately)
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.reservation_booking
CREATE TABLE IF NOT EXISTS `reservation_booking` (
  `id_reservation_booking` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ticket_series_info_id` int(11) NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `passenger_fare_id` int(11) NOT NULL,
  `booking_source_id` int(11) NOT NULL,
  `booking_status_id` int(11) NOT NULL,
  `discount_id` int(11) unsigned NOT NULL,
  `points` decimal(13,2) NOT NULL,
  `jetter_no` varchar(32) DEFAULT NULL,
  `payment` varchar(32) NOT NULL,
  `fare_price` decimal(13,2) NOT NULL,
  `return_fare_price` decimal(13,2) NOT NULL,
  `total_discount` decimal(13,2) NOT NULL,
  `terminal_fee` decimal(13,2) NOT NULL,
  `port_charge` decimal(13,2) NOT NULL,
  `total_amount` decimal(13,2) NOT NULL,
  `reference_id` int(11) unsigned NOT NULL,
  `is_return` int(11) unsigned NOT NULL,
  `passenger_valid_id_number` varchar(64) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_reservation_booking`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.reservation_booking: ~0 rows (approximately)
/*!40000 ALTER TABLE `reservation_booking` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_booking` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.reservation_passenger
CREATE TABLE IF NOT EXISTS `reservation_passenger` (
  `id_reservation_passenger` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(64) NOT NULL,
  `middlename` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) NOT NULL,
  `birthdate` date NOT NULL,
  `age` varchar(64) DEFAULT NULL,
  `gender` varchar(32) NOT NULL,
  `contactno` varchar(32) DEFAULT NULL,
  `id_no` varchar(64) DEFAULT NULL,
  `nationality` varchar(64) DEFAULT NULL,
  `with_infant` tinyint(1) DEFAULT NULL,
  `infant_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_reservation_passenger`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table oceanjet1_db.reservation_passenger: ~0 rows (approximately)
/*!40000 ALTER TABLE `reservation_passenger` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_passenger` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.reservation_status
CREATE TABLE IF NOT EXISTS `reservation_status` (
  `id_reservation_status` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_status` varchar(32) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_reservation_status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.reservation_status: ~5 rows (approximately)
/*!40000 ALTER TABLE `reservation_status` DISABLE KEYS */;
INSERT INTO `reservation_status` (`id_reservation_status`, `reservation_status`, `enabled`) VALUES
	(1, 'New', 1),
	(2, 'Reserved', 1),
	(3, 'Issued Ticket', 1),
	(4, 'Cancelled', 1),
	(5, 'Expired', 1);
/*!40000 ALTER TABLE `reservation_status` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.reset_password
CREATE TABLE IF NOT EXISTS `reset_password` (
  `id_reset_password` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `date_added` varchar(64) NOT NULL,
  `exp_date` varchar(64) NOT NULL,
  PRIMARY KEY (`id_reset_password`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.reset_password: ~3 rows (approximately)
/*!40000 ALTER TABLE `reset_password` DISABLE KEYS */;
INSERT INTO `reset_password` (`id_reset_password`, `user_id`, `value`, `date_added`, `exp_date`) VALUES
	(1, 2, '2fc042a2575272bb40d7471ece818a2705fb38a39888666d50fd5982246de790', '1449118260', '1449204660'),
	(2, 3, '1a6021abd28a47b5647394b7a495a9a06db1409947a6824cd2010b98330e2d71', '1449118740', '1449205140'),
	(3, 4, '238e8fd12353d9d40de7b906de13c08844228a3d0b1d339167c32b6939908400', '1453261500', '1453347900');
/*!40000 ALTER TABLE `reset_password` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.rolling_cargo
CREATE TABLE IF NOT EXISTS `rolling_cargo` (
  `id_rolling_cargo` int(11) NOT NULL AUTO_INCREMENT,
  `rolling_cargo_code` varchar(32) NOT NULL,
  `rolling_cargo` varchar(128) NOT NULL,
  `voyage_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `unit_measurement` varchar(16) NOT NULL,
  `rule_set_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_rolling_cargo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.rolling_cargo: ~0 rows (approximately)
/*!40000 ALTER TABLE `rolling_cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `rolling_cargo` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.rolling_cargo_checkin
CREATE TABLE IF NOT EXISTS `rolling_cargo_checkin` (
  `id_rolling_cargo_checkin` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `voyage_management_id` int(11) unsigned NOT NULL,
  `rolling_cargo_id` int(11) unsigned NOT NULL,
  `rolling_cargo_description` text,
  `with_ticket` tinyint(1) unsigned DEFAULT NULL,
  `ticket_no` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `reference_no` varchar(255) NOT NULL,
  `sender_first_name` varchar(64) NOT NULL,
  `sender_middle_name` varchar(64) NOT NULL,
  `sender_last_name` varchar(64) NOT NULL,
  `sender_contact_no` varchar(45) NOT NULL,
  `driver_first_name` varchar(64) NOT NULL,
  `driver_middle_name` varchar(64) NOT NULL,
  `driver_last_name` varchar(64) NOT NULL,
  `driver_contact_no` varchar(45) NOT NULL,
  `enabled` tinyint(1) unsigned DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_rolling_cargo_checkin`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.rolling_cargo_checkin: ~0 rows (approximately)
/*!40000 ALTER TABLE `rolling_cargo_checkin` DISABLE KEYS */;
/*!40000 ALTER TABLE `rolling_cargo_checkin` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.rule_set
CREATE TABLE IF NOT EXISTS `rule_set` (
  `id_rule_set` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rule_code` varchar(50) NOT NULL,
  `rule_set` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id_rule_set`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.rule_set: ~5 rows (approximately)
/*!40000 ALTER TABLE `rule_set` DISABLE KEYS */;
INSERT INTO `rule_set` (`id_rule_set`, `rule_code`, `rule_set`, `enabled`, `date_added`, `date_update`) VALUES
	(1, 'RA1', 'Re-Schedule Tickets', 1, '2016-01-20 11:15:41', '2016-01-20 11:15:41'),
	(2, 'RA2', 'Refund Tickets', 1, '2016-01-20 11:16:47', '2016-01-20 11:16:47'),
	(3, 'RA3', 'Upgrade Tickets', 1, '2016-01-20 11:17:36', '2016-01-20 11:17:36'),
	(4, 'RA4', 'Free Baggage Only', 1, '2016-01-20 11:17:59', '2016-01-20 11:17:59'),
	(5, 'RA5', 'All Rules', 1, '2016-01-20 14:28:36', '2016-01-20 14:28:36');
/*!40000 ALTER TABLE `rule_set` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.rule_type
CREATE TABLE IF NOT EXISTS `rule_type` (
  `id_rule_type` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rule_set_id` int(11) NOT NULL,
  `rule_type_name_id` int(11) NOT NULL,
  `rule_amount_type` varchar(10) NOT NULL,
  `rule_type_amount` decimal(13,2) NOT NULL,
  PRIMARY KEY (`id_rule_type`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.rule_type: ~31 rows (approximately)
/*!40000 ALTER TABLE `rule_type` DISABLE KEYS */;
INSERT INTO `rule_type` (`id_rule_type`, `rule_set_id`, `rule_type_name_id`, `rule_amount_type`, `rule_type_amount`) VALUES
	(1, 1, 38, 'amount', 500.00),
	(2, 1, 39, 'amount', 400.00),
	(3, 1, 40, 'amount', 300.00),
	(4, 1, 41, 'percentage', 5.00),
	(5, 1, 42, 'percentage', 10.00),
	(6, 1, 51, 'weight', 100.00),
	(7, 2, 43, 'amount', 2000.00),
	(8, 2, 44, 'amount', 1500.00),
	(9, 2, 45, 'amount', 1000.00),
	(10, 2, 46, 'percentage', 10.00),
	(11, 2, 47, 'percentage', 20.00),
	(12, 2, 51, 'weight', 100.00),
	(13, 3, 48, 'percentage', 5.00),
	(14, 3, 49, 'percentage', 10.00),
	(15, 3, 50, 'percentage', 20.00),
	(16, 3, 51, 'weight', 100.00),
	(17, 4, 51, 'weight', 400.00),
	(18, 5, 38, 'amount', 500.00),
	(19, 5, 39, 'amount', 500.00),
	(20, 5, 40, 'percentage', 2.10),
	(21, 5, 41, 'percentage', 2.30),
	(22, 5, 42, 'percentage', 3.00),
	(23, 5, 43, 'amount', 1500.00),
	(24, 5, 44, 'amount', 1000.00),
	(25, 5, 45, 'amount', 1800.00),
	(26, 5, 46, 'percentage', 60.00),
	(27, 5, 47, 'percentage', 70.00),
	(28, 5, 48, 'amount', 550.00),
	(29, 5, 49, 'amount', 450.00),
	(30, 5, 50, 'percentage', 700.00),
	(31, 5, 51, 'weight', 2000.00);
/*!40000 ALTER TABLE `rule_type` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.rule_type_name
CREATE TABLE IF NOT EXISTS `rule_type_name` (
  `id_rule_type_name` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rule_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_rule_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.rule_type_name: ~14 rows (approximately)
/*!40000 ALTER TABLE `rule_type_name` DISABLE KEYS */;
INSERT INTO `rule_type_name` (`id_rule_type_name`, `rule_type`) VALUES
	(38, 'Re-schedule Tickets Not Yet Checked In - Before Departure'),
	(39, 'Re-schedule Tickets Not Yet Checked In - After Departure'),
	(40, 'Re-schedule Tickets Already Checked In - Before Departure'),
	(41, 'Re-schedule Tickets Already Checked In - After Departure'),
	(42, 'Re-schedule Ticket for Cancelled Voyage'),
	(43, 'Refund Tickets Not Yet Checked In - Before Departure'),
	(44, 'Refund Tickets Not Yet Checked In - After Departure'),
	(45, 'Refund Tickets Already Checked In - Before Departure'),
	(46, 'Refund Tickets Already Checked In - After Departure'),
	(47, 'Refund Ticket for Cancelled Voyage'),
	(48, 'Upgrade Ticket from Open Air to Tourist Class'),
	(49, 'Upgrade Ticket from Tourist Class to Business Class'),
	(50, 'Upgrade Ticket from Open Air to Business Class'),
	(51, 'Free Baggage Allowance');
/*!40000 ALTER TABLE `rule_type_name` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.subvoyage
CREATE TABLE IF NOT EXISTS `subvoyage` (
  `id_subvoyage` int(11) NOT NULL AUTO_INCREMENT,
  `voyage_id` int(11) NOT NULL,
  `vessel_id` int(11) NOT NULL,
  `ETD` time NOT NULL,
  `ETA` time NOT NULL,
  `origin` int(11) NOT NULL,
  `destination` int(11) NOT NULL,
  PRIMARY KEY (`id_subvoyage`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.subvoyage: ~27 rows (approximately)
/*!40000 ALTER TABLE `subvoyage` DISABLE KEYS */;
INSERT INTO `subvoyage` (`id_subvoyage`, `voyage_id`, `vessel_id`, `ETD`, `ETA`, `origin`, `destination`) VALUES
	(1, 1, 1, '06:00:00', '08:00:00', 1, 2),
	(2, 1, 3, '09:00:00', '11:00:00', 2, 3),
	(3, 2, 1, '12:00:00', '14:00:00', 1, 2),
	(4, 3, 1, '18:00:00', '20:00:00', 1, 2),
	(5, 4, 4, '06:00:00', '08:00:00', 3, 4),
	(6, 5, 3, '06:00:00', '08:00:00', 1, 2),
	(7, 5, 3, '09:00:00', '11:00:00', 2, 3),
	(8, 5, 4, '12:00:00', '16:00:00', 3, 4),
	(9, 6, 4, '09:00:00', '11:00:00', 4, 3),
	(10, 7, 1, '09:00:00', '11:00:00', 2, 1),
	(11, 8, 2, '09:00:00', '11:00:00', 1, 2),
	(12, 8, 5, '13:00:00', '15:00:00', 2, 3),
	(13, 9, 5, '06:00:00', '08:00:00', 2, 3),
	(14, 10, 4, '09:00:00', '11:00:00', 4, 3),
	(15, 10, 3, '12:00:00', '14:00:00', 3, 2),
	(16, 10, 1, '15:00:00', '17:00:00', 2, 1),
	(17, 11, 5, '09:00:00', '11:00:00', 3, 2),
	(18, 11, 2, '12:00:00', '14:00:00', 2, 1),
	(19, 12, 2, '03:00:00', '05:00:00', 1, 2),
	(20, 13, 5, '09:00:00', '11:00:00', 3, 2),
	(21, 14, 5, '18:00:00', '20:00:00', 3, 2),
	(22, 15, 4, '15:00:00', '17:00:00', 4, 3),
	(23, 15, 5, '18:00:00', '20:00:00', 3, 2),
	(24, 16, 3, '15:00:00', '17:00:00', 2, 3),
	(25, 17, 4, '06:00:00', '08:00:00', 3, 4),
	(26, 18, 2, '15:00:00', '17:00:00', 1, 2),
	(27, 18, 2, '18:00:00', '20:00:00', 2, 3);
/*!40000 ALTER TABLE `subvoyage` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.subvoyage_fare
CREATE TABLE IF NOT EXISTS `subvoyage_fare` (
  `id_subvoyage_fare` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `passenger_fare_id` int(11) unsigned NOT NULL,
  `subvoyage_id` int(11) unsigned NOT NULL,
  `amount` decimal(13,2) unsigned NOT NULL,
  PRIMARY KEY (`id_subvoyage_fare`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Dumping data for table oceanjet1_db.subvoyage_fare: ~14 rows (approximately)
/*!40000 ALTER TABLE `subvoyage_fare` DISABLE KEYS */;
INSERT INTO `subvoyage_fare` (`id_subvoyage_fare`, `passenger_fare_id`, `subvoyage_id`, `amount`) VALUES
	(1, 1, 1, 2800.00),
	(2, 1, 2, 3000.00),
	(7, 2, 1, 0.00),
	(8, 2, 2, 0.00),
	(13, 3, 1, 2800.00),
	(14, 3, 2, 3000.00),
	(19, 5, 1, 3000.00),
	(20, 5, 2, 3200.00),
	(21, 4, 1, 2800.00),
	(22, 4, 2, 3000.00),
	(23, 6, 1, 2800.00),
	(24, 6, 2, 3000.00),
	(31, 7, 1, 2800.00),
	(32, 7, 2, 3000.00);
/*!40000 ALTER TABLE `subvoyage_fare` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.subvoyage_management
CREATE TABLE IF NOT EXISTS `subvoyage_management` (
  `id_subvoyage_management` int(11) NOT NULL AUTO_INCREMENT,
  `voyage_management_id` int(11) NOT NULL,
  `subvoyage_id` int(11) NOT NULL,
  `departure_date` date NOT NULL,
  `vessel_id` int(11) NOT NULL,
  `voyage_management_status_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `actual_departure_time` time NOT NULL,
  PRIMARY KEY (`id_subvoyage_management`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.subvoyage_management: ~115 rows (approximately)
/*!40000 ALTER TABLE `subvoyage_management` DISABLE KEYS */;
INSERT INTO `subvoyage_management` (`id_subvoyage_management`, `voyage_management_id`, `subvoyage_id`, `departure_date`, `vessel_id`, `voyage_management_status_id`, `date_added`, `date_update`, `actual_departure_time`) VALUES
	(5, 1, 1, '2016-01-23', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(6, 1, 2, '2016-01-23', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(7, 2, 1, '2016-01-26', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(8, 2, 2, '2016-01-26', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(9, 3, 1, '2016-01-29', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(10, 3, 2, '2016-01-29', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(11, 4, 3, '2016-01-22', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(12, 5, 3, '2016-01-25', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(13, 6, 3, '2016-01-27', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(14, 7, 3, '2016-01-30', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(15, 8, 4, '2016-01-21', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(16, 9, 4, '2016-01-23', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(17, 10, 4, '2016-01-25', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(18, 11, 5, '2016-01-22', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(19, 12, 5, '2016-01-25', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(20, 13, 5, '2016-01-27', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(21, 14, 5, '2016-01-29', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(22, 15, 5, '2016-01-31', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(23, 16, 6, '2016-01-22', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(24, 16, 7, '2016-01-22', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(25, 16, 8, '2016-01-22', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(26, 17, 6, '2016-01-27', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(27, 17, 7, '2016-01-27', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(28, 17, 8, '2016-01-27', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(29, 18, 6, '2016-01-31', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(30, 18, 7, '2016-01-31', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(31, 18, 8, '2016-01-31', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(32, 19, 9, '2016-01-22', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(33, 20, 9, '2016-01-25', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(34, 21, 9, '2016-01-27', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(35, 22, 9, '2016-01-29', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(36, 23, 9, '2016-01-31', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(37, 24, 10, '2016-01-22', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(38, 25, 10, '2016-01-25', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(39, 26, 10, '2016-01-27', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(40, 27, 10, '2016-01-29', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(41, 28, 10, '2016-01-31', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(42, 29, 11, '2016-01-22', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(43, 29, 12, '2016-01-22', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(44, 30, 11, '2016-01-25', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(45, 30, 12, '2016-01-25', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(46, 31, 11, '2016-01-27', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(47, 31, 12, '2016-01-27', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(48, 32, 11, '2016-01-29', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(49, 32, 12, '2016-01-29', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(50, 33, 11, '2016-01-31', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(51, 33, 12, '2016-01-31', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(52, 34, 13, '2016-01-22', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(53, 35, 13, '2016-01-25', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(54, 36, 13, '2016-01-26', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(55, 37, 13, '2016-01-28', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(56, 38, 13, '2016-01-30', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(57, 39, 13, '2016-01-31', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(58, 40, 14, '2016-01-22', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(59, 40, 15, '2016-01-22', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(60, 40, 16, '2016-01-22', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(61, 41, 14, '2016-01-27', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(62, 41, 15, '2016-01-27', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(63, 41, 16, '2016-01-27', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(64, 42, 14, '2016-01-31', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(65, 42, 15, '2016-01-31', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(66, 42, 16, '2016-01-31', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(67, 43, 17, '2016-01-22', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(68, 43, 18, '2016-01-22', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(69, 44, 17, '2016-01-25', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(70, 44, 18, '2016-01-25', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(71, 45, 17, '2016-01-27', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(72, 45, 18, '2016-01-27', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(73, 46, 17, '2016-01-31', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(74, 46, 18, '2016-01-31', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(75, 47, 19, '2016-01-22', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(76, 48, 19, '2016-01-26', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(77, 49, 19, '2016-01-28', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(78, 50, 19, '2016-01-29', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(79, 51, 20, '2016-01-23', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(80, 52, 20, '2016-01-23', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(81, 53, 20, '2016-01-27', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(82, 54, 21, '2016-01-23', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(83, 55, 21, '2016-01-25', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(84, 56, 21, '2016-01-27', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(85, 57, 21, '2016-01-29', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(86, 58, 21, '2016-01-30', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(87, 59, 21, '2016-01-31', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(88, 60, 22, '2016-01-21', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(89, 60, 23, '2016-01-21', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(90, 61, 22, '2016-01-23', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(91, 61, 23, '2016-01-23', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(92, 62, 22, '2016-01-24', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(93, 62, 23, '2016-01-24', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(94, 63, 22, '2016-01-26', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(95, 63, 23, '2016-01-26', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(96, 64, 22, '2016-01-28', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(97, 64, 23, '2016-01-28', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(98, 65, 22, '2016-01-30', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(99, 65, 23, '2016-01-30', 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(100, 66, 24, '2016-01-21', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(101, 67, 24, '2016-01-22', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(102, 68, 24, '2016-01-23', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(103, 69, 24, '2016-01-26', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(104, 70, 24, '2016-01-27', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(105, 71, 24, '2016-01-28', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(106, 72, 24, '2016-01-29', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(107, 73, 24, '2016-01-30', 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(108, 74, 25, '2016-01-23', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(109, 75, 25, '2016-01-24', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(110, 76, 25, '2016-01-26', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(111, 77, 25, '2016-01-27', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(112, 78, 25, '2016-01-28', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(113, 79, 25, '2016-01-29', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(114, 80, 25, '2016-01-30', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(115, 81, 25, '2016-01-31', 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(116, 82, 26, '2016-01-27', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(117, 82, 27, '2016-01-27', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(118, 83, 26, '2016-01-30', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00'),
	(119, 83, 27, '2016-01-30', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00');
/*!40000 ALTER TABLE `subvoyage_management` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.ticket_series
CREATE TABLE IF NOT EXISTS `ticket_series` (
  `id_ticket_series` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_starts_from` varchar(255) DEFAULT NULL,
  `ticket_ends_to` varchar(255) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `no_tickets` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id_ticket_series`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.ticket_series: ~1 rows (approximately)
/*!40000 ALTER TABLE `ticket_series` DISABLE KEYS */;
INSERT INTO `ticket_series` (`id_ticket_series`, `ticket_starts_from`, `ticket_ends_to`, `enabled`, `no_tickets`, `date_added`, `date_updated`) VALUES
	(1, '100000000001', '100000001000', 1, 1000, '2016-01-20 11:13:35', NULL);
/*!40000 ALTER TABLE `ticket_series` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.ticket_series_info
CREATE TABLE IF NOT EXISTS `ticket_series_info` (
  `id_ticket_series_info` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_series_id` int(11) DEFAULT NULL,
  `ticket_no` varchar(255) DEFAULT NULL,
  `issued` int(11) DEFAULT '0' COMMENT '1: issued, 0: not issued',
  `user_id` int(11) DEFAULT '0',
  `enabled` int(1) DEFAULT '1',
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id_ticket_series_info`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.ticket_series_info: ~1,043 rows (approximately)
/*!40000 ALTER TABLE `ticket_series_info` DISABLE KEYS */;
INSERT INTO `ticket_series_info` (`id_ticket_series_info`, `ticket_series_id`, `ticket_no`, `issued`, `user_id`, `enabled`, `date_added`, `date_updated`) VALUES
	(1, 1, '100', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(2, 1, '100000000002', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(3, 1, '100000000003', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(4, 1, '100000000004', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(5, 1, '100000000005', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(6, 1, '100000000006', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(7, 1, '100000000007', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(8, 1, '100000000008', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(9, 1, '100000000009', 0, 3, 1, '2016-01-20 11:13:35', NULL),
	(10, 1, '100000000010', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(11, 1, '100000000011', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(12, 1, '100000000012', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(13, 1, '100000000013', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(14, 1, '100000000014', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(15, 1, '100000000015', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(16, 1, '100000000016', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(17, 1, '100000000017', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(18, 1, '100000000018', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(19, 1, '100000000019', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(20, 1, '100000000020', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(21, 1, '100000000021', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(22, 1, '100000000022', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(23, 1, '100000000023', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(24, 1, '100000000024', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(25, 1, '100000000025', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(26, 1, '100000000026', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(27, 1, '100000000027', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(28, 1, '100000000028', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(29, 1, '100000000029', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(30, 1, '100000000030', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(31, 1, '100000000031', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(32, 1, '100000000032', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(33, 1, '100000000033', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(34, 1, '100000000034', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(35, 1, '100000000035', 0, 3, 1, '2016-01-20 11:13:36', NULL),
	(36, 1, '100000000036', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(37, 1, '100000000037', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(38, 1, '100000000038', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(39, 1, '100000000039', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(40, 1, '100000000040', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(41, 1, '100000000041', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(42, 1, '100000000042', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(43, 1, '100000000043', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(44, 1, '100000000044', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(45, 1, '100000000045', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(46, 1, '100000000046', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(47, 1, '100000000047', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(48, 1, '100000000048', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(49, 1, '100000000049', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(50, 1, '100000000050', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(51, 1, '100000000051', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(52, 1, '100000000052', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(53, 1, '100000000053', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(54, 1, '100000000054', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(55, 1, '100000000055', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(56, 1, '100000000056', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(57, 1, '100000000057', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(58, 1, '100000000058', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(59, 1, '100000000059', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(60, 1, '100000000060', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(61, 1, '100000000061', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(62, 1, '100000000062', 0, 3, 1, '2016-01-20 11:13:37', NULL),
	(63, 1, '100000000063', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(64, 1, '100000000064', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(65, 1, '100000000065', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(66, 1, '100000000066', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(67, 1, '100000000067', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(68, 1, '100000000068', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(69, 1, '100000000069', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(70, 1, '100000000070', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(71, 1, '100000000071', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(72, 1, '100000000072', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(73, 1, '100000000073', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(74, 1, '100000000074', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(75, 1, '100000000075', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(76, 1, '100000000076', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(77, 1, '100000000077', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(78, 1, '100000000078', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(79, 1, '100000000079', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(80, 1, '100000000080', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(81, 1, '100000000081', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(82, 1, '100000000082', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(83, 1, '100000000083', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(84, 1, '100000000084', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(85, 1, '100000000085', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(86, 1, '100000000086', 0, 3, 1, '2016-01-20 11:13:38', NULL),
	(87, 1, '100000000087', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(88, 1, '100000000088', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(89, 1, '100000000089', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(90, 1, '100000000090', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(91, 1, '100000000091', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(92, 1, '100000000092', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(93, 1, '100000000093', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(94, 1, '100000000094', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(95, 1, '100000000095', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(96, 1, '100000000096', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(97, 1, '100000000097', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(98, 1, '100000000098', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(99, 1, '100000000099', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(100, 1, '100000000100', 0, 3, 1, '2016-01-20 11:13:39', NULL),
	(101, 1, '100000000101', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(102, 1, '100000000102', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(103, 1, '100000000103', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(104, 1, '100000000104', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(105, 1, '100000000105', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(106, 1, '100000000106', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(107, 1, '100000000107', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(108, 1, '100000000108', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(109, 1, '100000000109', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(110, 1, '100000000110', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(111, 1, '100000000111', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(112, 1, '100000000112', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(113, 1, '100000000113', 0, 4, 1, '2016-01-20 11:13:39', NULL),
	(114, 1, '100000000114', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(115, 1, '100000000115', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(116, 1, '100000000116', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(117, 1, '100000000117', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(118, 1, '100000000118', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(119, 1, '100000000119', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(120, 1, '100000000120', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(121, 1, '100000000121', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(122, 1, '100000000122', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(123, 1, '100000000123', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(124, 1, '100000000124', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(125, 1, '100000000125', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(126, 1, '100000000126', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(127, 1, '100000000127', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(128, 1, '100000000128', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(129, 1, '100000000129', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(130, 1, '100000000130', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(131, 1, '100000000131', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(132, 1, '100000000132', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(133, 1, '100000000133', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(134, 1, '100000000134', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(135, 1, '100000000135', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(136, 1, '100000000136', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(137, 1, '100000000137', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(138, 1, '100000000138', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(139, 1, '100000000139', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(140, 1, '100000000140', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(141, 1, '100000000141', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(142, 1, '100000000142', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(143, 1, '100000000143', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(144, 1, '100000000144', 0, 4, 1, '2016-01-20 11:13:40', NULL),
	(145, 1, '100000000145', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(146, 1, '100000000146', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(147, 1, '100000000147', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(148, 1, '100000000148', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(149, 1, '100000000149', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(150, 1, '100000000150', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(151, 1, '100000000151', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(152, 1, '100000000152', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(153, 1, '100000000153', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(154, 1, '100000000154', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(155, 1, '100000000155', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(156, 1, '100000000156', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(157, 1, '100000000157', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(158, 1, '100000000158', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(159, 1, '100000000159', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(160, 1, '100000000160', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(161, 1, '100000000161', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(162, 1, '100000000162', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(163, 1, '100000000163', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(164, 1, '100000000164', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(165, 1, '100000000165', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(166, 1, '100000000166', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(167, 1, '100000000167', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(168, 1, '100000000168', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(169, 1, '100000000169', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(170, 1, '100000000170', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(171, 1, '100000000171', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(172, 1, '100000000172', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(173, 1, '100000000173', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(174, 1, '100000000174', 0, 4, 1, '2016-01-20 11:13:41', NULL),
	(175, 1, '100000000175', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(176, 1, '100000000176', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(177, 1, '100000000177', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(178, 1, '100000000178', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(179, 1, '100000000179', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(180, 1, '100000000180', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(181, 1, '100000000181', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(182, 1, '100000000182', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(183, 1, '100000000183', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(184, 1, '100000000184', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(185, 1, '100000000185', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(186, 1, '100000000186', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(187, 1, '100000000187', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(188, 1, '100000000188', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(189, 1, '100000000189', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(190, 1, '100000000190', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(191, 1, '100000000191', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(192, 1, '100000000192', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(193, 1, '100000000193', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(194, 1, '100000000194', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(195, 1, '100000000195', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(196, 1, '100000000196', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(197, 1, '100000000197', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(198, 1, '100000000198', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(199, 1, '100000000199', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(200, 1, '100000000200', 0, 4, 1, '2016-01-20 11:13:42', NULL),
	(201, 1, '100000000201', 0, 0, 1, '2016-01-20 11:13:42', NULL),
	(202, 1, '100000000202', 0, 0, 1, '2016-01-20 11:13:42', NULL),
	(203, 1, '100000000203', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(204, 1, '100000000204', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(205, 1, '100000000205', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(206, 1, '100000000206', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(207, 1, '100000000207', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(208, 1, '100000000208', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(209, 1, '100000000209', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(210, 1, '100000000210', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(211, 1, '100000000211', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(212, 1, '100000000212', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(213, 1, '100000000213', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(214, 1, '100000000214', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(215, 1, '100000000215', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(216, 1, '100000000216', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(217, 1, '100000000217', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(218, 1, '100000000218', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(219, 1, '100000000219', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(220, 1, '100000000220', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(221, 1, '100000000221', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(222, 1, '100000000222', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(223, 1, '100000000223', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(224, 1, '100000000224', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(225, 1, '100000000225', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(226, 1, '100000000226', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(227, 1, '100000000227', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(228, 1, '100000000228', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(229, 1, '100000000229', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(230, 1, '100000000230', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(231, 1, '100000000231', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(232, 1, '100000000232', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(233, 1, '100000000233', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(234, 1, '100000000234', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(235, 1, '100000000235', 0, 0, 1, '2016-01-20 11:13:43', NULL),
	(236, 1, '100000000236', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(237, 1, '100000000237', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(238, 1, '100000000238', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(239, 1, '100000000239', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(240, 1, '100000000240', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(241, 1, '100000000241', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(242, 1, '100000000242', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(243, 1, '100000000243', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(244, 1, '100000000244', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(245, 1, '100000000245', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(246, 1, '100000000246', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(247, 1, '100000000247', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(248, 1, '100000000248', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(249, 1, '100000000249', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(250, 1, '100000000250', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(251, 1, '100000000251', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(252, 1, '100000000252', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(253, 1, '100000000253', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(254, 1, '100000000254', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(255, 1, '100000000255', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(256, 1, '100000000256', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(257, 1, '100000000257', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(258, 1, '100000000258', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(259, 1, '100000000259', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(260, 1, '100000000260', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(261, 1, '100000000261', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(262, 1, '100000000262', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(263, 1, '100000000263', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(264, 1, '100000000264', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(265, 1, '100000000265', 0, 0, 1, '2016-01-20 11:13:44', NULL),
	(266, 1, '100000000266', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(267, 1, '100000000267', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(268, 1, '100000000268', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(269, 1, '100000000269', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(270, 1, '100000000270', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(271, 1, '100000000271', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(272, 1, '100000000272', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(273, 1, '100000000273', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(274, 1, '100000000274', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(275, 1, '100000000275', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(276, 1, '100000000276', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(277, 1, '100000000277', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(278, 1, '100000000278', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(279, 1, '100000000279', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(280, 1, '100000000280', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(281, 1, '100000000281', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(282, 1, '100000000282', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(283, 1, '100000000283', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(284, 1, '100000000284', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(285, 1, '100000000285', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(286, 1, '100000000286', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(287, 1, '100000000287', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(288, 1, '100000000288', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(289, 1, '100000000289', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(290, 1, '100000000290', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(291, 1, '100000000291', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(292, 1, '100000000292', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(293, 1, '100000000293', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(294, 1, '100000000294', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(295, 1, '100000000295', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(296, 1, '100000000296', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(297, 1, '100000000297', 0, 0, 1, '2016-01-20 11:13:45', NULL),
	(298, 1, '100000000298', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(299, 1, '100000000299', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(300, 1, '100000000300', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(301, 1, '100000000301', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(302, 1, '100000000302', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(303, 1, '100000000303', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(304, 1, '100000000304', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(305, 1, '100000000305', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(306, 1, '100000000306', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(307, 1, '100000000307', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(308, 1, '100000000308', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(309, 1, '100000000309', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(310, 1, '100000000310', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(311, 1, '100000000311', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(312, 1, '100000000312', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(313, 1, '100000000313', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(314, 1, '100000000314', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(315, 1, '100000000315', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(316, 1, '100000000316', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(317, 1, '100000000317', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(318, 1, '100000000318', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(319, 1, '100000000319', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(320, 1, '100000000320', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(321, 1, '100000000321', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(322, 1, '100000000322', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(323, 1, '100000000323', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(324, 1, '100000000324', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(325, 1, '100000000325', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(326, 1, '100000000326', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(327, 1, '100000000327', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(328, 1, '100000000328', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(329, 1, '100000000329', 0, 0, 1, '2016-01-20 11:13:46', NULL),
	(330, 1, '100000000330', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(331, 1, '100000000331', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(332, 1, '100000000332', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(333, 1, '100000000333', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(334, 1, '100000000334', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(335, 1, '100000000335', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(336, 1, '100000000336', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(337, 1, '100000000337', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(338, 1, '100000000338', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(339, 1, '100000000339', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(340, 1, '100000000340', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(341, 1, '100000000341', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(342, 1, '100000000342', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(343, 1, '100000000343', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(344, 1, '100000000344', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(345, 1, '100000000345', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(346, 1, '100000000346', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(347, 1, '100000000347', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(348, 1, '100000000348', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(349, 1, '100000000349', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(350, 1, '100000000350', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(351, 1, '100000000351', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(352, 1, '100000000352', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(353, 1, '100000000353', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(354, 1, '100000000354', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(355, 1, '100000000355', 0, 0, 1, '2016-01-20 11:13:47', NULL),
	(356, 1, '100000000356', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(357, 1, '100000000357', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(358, 1, '100000000358', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(359, 1, '100000000359', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(360, 1, '100000000360', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(361, 1, '100000000361', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(362, 1, '100000000362', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(363, 1, '100000000363', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(364, 1, '100000000364', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(365, 1, '100000000365', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(366, 1, '100000000366', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(367, 1, '100000000367', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(368, 1, '100000000368', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(369, 1, '100000000369', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(370, 1, '100000000370', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(371, 1, '100000000371', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(372, 1, '100000000372', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(373, 1, '100000000373', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(374, 1, '100000000374', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(375, 1, '100000000375', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(376, 1, '100000000376', 0, 0, 1, '2016-01-20 11:13:48', NULL),
	(377, 1, '100000000377', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(378, 1, '100000000378', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(379, 1, '100000000379', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(380, 1, '100000000380', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(381, 1, '100000000381', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(382, 1, '100000000382', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(383, 1, '100000000383', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(384, 1, '100000000384', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(385, 1, '100000000385', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(386, 1, '100000000386', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(387, 1, '100000000387', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(388, 1, '100000000388', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(389, 1, '100000000389', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(390, 1, '100000000390', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(391, 1, '100000000391', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(392, 1, '100000000392', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(393, 1, '100000000393', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(394, 1, '100000000394', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(395, 1, '100000000395', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(396, 1, '100000000396', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(397, 1, '100000000397', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(398, 1, '100000000398', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(399, 1, '100000000399', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(400, 1, '100000000400', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(401, 1, '100000000401', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(402, 1, '100000000402', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(403, 1, '100000000403', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(404, 1, '100000000404', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(405, 1, '100000000405', 0, 0, 1, '2016-01-20 11:13:49', NULL),
	(406, 1, '100000000406', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(407, 1, '100000000407', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(408, 1, '100000000408', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(409, 1, '100000000409', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(410, 1, '100000000410', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(411, 1, '100000000411', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(412, 1, '100000000412', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(413, 1, '100000000413', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(414, 1, '100000000414', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(415, 1, '100000000415', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(416, 1, '100000000416', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(417, 1, '100000000417', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(418, 1, '100000000418', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(419, 1, '100000000419', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(420, 1, '100000000420', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(421, 1, '100000000421', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(422, 1, '100000000422', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(423, 1, '100000000423', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(424, 1, '100000000424', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(425, 1, '100000000425', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(426, 1, '100000000426', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(427, 1, '100000000427', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(428, 1, '100000000428', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(429, 1, '100000000429', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(430, 1, '100000000430', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(431, 1, '100000000431', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(432, 1, '100000000432', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(433, 1, '100000000433', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(434, 1, '100000000434', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(435, 1, '100000000435', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(436, 1, '100000000436', 0, 0, 1, '2016-01-20 11:13:50', NULL),
	(437, 1, '100000000437', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(438, 1, '100000000438', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(439, 1, '100000000439', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(440, 1, '100000000440', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(441, 1, '100000000441', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(442, 1, '100000000442', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(443, 1, '100000000443', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(444, 1, '100000000444', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(445, 1, '100000000445', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(446, 1, '100000000446', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(447, 1, '100000000447', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(448, 1, '100000000448', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(449, 1, '100000000449', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(450, 1, '100000000450', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(451, 1, '100000000451', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(452, 1, '100000000452', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(453, 1, '100000000453', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(454, 1, '100000000454', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(455, 1, '100000000455', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(456, 1, '100000000456', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(457, 1, '100000000457', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(458, 1, '100000000458', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(459, 1, '100000000459', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(460, 1, '100000000460', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(461, 1, '100000000461', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(462, 1, '100000000462', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(463, 1, '100000000463', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(464, 1, '100000000464', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(465, 1, '100000000465', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(466, 1, '100000000466', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(467, 1, '100000000467', 0, 0, 1, '2016-01-20 11:13:51', NULL),
	(468, 1, '100000000468', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(469, 1, '100000000469', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(470, 1, '100000000470', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(471, 1, '100000000471', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(472, 1, '100000000472', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(473, 1, '100000000473', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(474, 1, '100000000474', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(475, 1, '100000000475', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(476, 1, '100000000476', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(477, 1, '100000000477', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(478, 1, '100000000478', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(479, 1, '100000000479', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(480, 1, '100000000480', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(481, 1, '100000000481', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(482, 1, '100000000482', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(483, 1, '100000000483', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(484, 1, '100000000484', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(485, 1, '100000000485', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(486, 1, '100000000486', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(487, 1, '100000000487', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(488, 1, '100000000488', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(489, 1, '100000000489', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(490, 1, '100000000490', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(491, 1, '100000000491', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(492, 1, '100000000492', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(493, 1, '100000000493', 0, 0, 1, '2016-01-20 11:13:52', NULL),
	(494, 1, '100000000494', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(495, 1, '100000000495', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(496, 1, '100000000496', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(497, 1, '100000000497', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(498, 1, '100000000498', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(499, 1, '100000000499', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(500, 1, '100000000500', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(501, 1, '100000000501', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(502, 1, '100000000502', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(503, 1, '100000000503', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(504, 1, '100000000504', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(505, 1, '100000000505', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(506, 1, '100000000506', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(507, 1, '100000000507', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(508, 1, '100000000508', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(509, 1, '100000000509', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(510, 1, '100000000510', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(511, 1, '100000000511', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(512, 1, '100000000512', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(513, 1, '100000000513', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(514, 1, '100000000514', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(515, 1, '100000000515', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(516, 1, '100000000516', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(517, 1, '100000000517', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(518, 1, '100000000518', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(519, 1, '100000000519', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(520, 1, '100000000520', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(521, 1, '100000000521', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(522, 1, '100000000522', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(523, 1, '100000000523', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(524, 1, '100000000524', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(525, 1, '100000000525', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(526, 1, '100000000526', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(527, 1, '100000000527', 0, 0, 1, '2016-01-20 11:13:53', NULL),
	(528, 1, '100000000528', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(529, 1, '100000000529', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(530, 1, '100000000530', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(531, 1, '100000000531', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(532, 1, '100000000532', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(533, 1, '100000000533', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(534, 1, '100000000534', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(535, 1, '100000000535', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(536, 1, '100000000536', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(537, 1, '100000000537', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(538, 1, '100000000538', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(539, 1, '100000000539', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(540, 1, '100000000540', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(541, 1, '100000000541', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(542, 1, '100000000542', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(543, 1, '100000000543', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(544, 1, '100000000544', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(545, 1, '100000000545', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(546, 1, '100000000546', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(547, 1, '100000000547', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(548, 1, '100000000548', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(549, 1, '100000000549', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(550, 1, '100000000550', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(551, 1, '100000000551', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(552, 1, '100000000552', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(553, 1, '100000000553', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(554, 1, '100000000554', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(555, 1, '100000000555', 0, 0, 1, '2016-01-20 11:13:54', NULL),
	(556, 1, '100000000556', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(557, 1, '100000000557', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(558, 1, '100000000558', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(559, 1, '100000000559', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(560, 1, '100000000560', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(561, 1, '100000000561', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(562, 1, '100000000562', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(563, 1, '100000000563', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(564, 1, '100000000564', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(565, 1, '100000000565', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(566, 1, '100000000566', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(567, 1, '100000000567', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(568, 1, '100000000568', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(569, 1, '100000000569', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(570, 1, '100000000570', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(571, 1, '100000000571', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(572, 1, '100000000572', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(573, 1, '100000000573', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(574, 1, '100000000574', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(575, 1, '100000000575', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(576, 1, '100000000576', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(577, 1, '100000000577', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(578, 1, '100000000578', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(579, 1, '100000000579', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(580, 1, '100000000580', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(581, 1, '100000000581', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(582, 1, '100000000582', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(583, 1, '100000000583', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(584, 1, '100000000584', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(585, 1, '100000000585', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(586, 1, '100000000586', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(587, 1, '100000000587', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(588, 1, '100000000588', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(589, 1, '100000000589', 0, 0, 1, '2016-01-20 11:13:55', NULL),
	(590, 1, '100000000590', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(591, 1, '100000000591', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(592, 1, '100000000592', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(593, 1, '100000000593', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(594, 1, '100000000594', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(595, 1, '100000000595', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(596, 1, '100000000596', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(597, 1, '100000000597', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(598, 1, '100000000598', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(599, 1, '100000000599', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(600, 1, '100000000600', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(601, 1, '100000000601', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(602, 1, '100000000602', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(603, 1, '100000000603', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(604, 1, '100000000604', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(605, 1, '100000000605', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(606, 1, '100000000606', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(607, 1, '100000000607', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(608, 1, '100000000608', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(609, 1, '100000000609', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(610, 1, '100000000610', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(611, 1, '100000000611', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(612, 1, '100000000612', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(613, 1, '100000000613', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(614, 1, '100000000614', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(615, 1, '100000000615', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(616, 1, '100000000616', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(617, 1, '100000000617', 0, 0, 1, '2016-01-20 11:13:56', NULL),
	(618, 1, '100000000618', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(619, 1, '100000000619', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(620, 1, '100000000620', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(621, 1, '100000000621', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(622, 1, '100000000622', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(623, 1, '100000000623', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(624, 1, '100000000624', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(625, 1, '100000000625', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(626, 1, '100000000626', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(627, 1, '100000000627', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(628, 1, '100000000628', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(629, 1, '100000000629', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(630, 1, '100000000630', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(631, 1, '100000000631', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(632, 1, '100000000632', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(633, 1, '100000000633', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(634, 1, '100000000634', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(635, 1, '100000000635', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(636, 1, '100000000636', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(637, 1, '100000000637', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(638, 1, '100000000638', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(639, 1, '100000000639', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(640, 1, '100000000640', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(641, 1, '100000000641', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(642, 1, '100000000642', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(643, 1, '100000000643', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(644, 1, '100000000644', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(645, 1, '100000000645', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(646, 1, '100000000646', 0, 0, 1, '2016-01-20 11:13:57', NULL),
	(647, 1, '100000000647', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(648, 1, '100000000648', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(649, 1, '100000000649', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(650, 1, '100000000650', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(651, 1, '100000000651', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(652, 1, '100000000652', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(653, 1, '100000000653', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(654, 1, '100000000654', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(655, 1, '100000000655', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(656, 1, '100000000656', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(657, 1, '100000000657', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(658, 1, '100000000658', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(659, 1, '100000000659', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(660, 1, '100000000660', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(661, 1, '100000000661', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(662, 1, '100000000662', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(663, 1, '100000000663', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(664, 1, '100000000664', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(665, 1, '100000000665', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(666, 1, '100000000666', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(667, 1, '100000000667', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(668, 1, '100000000668', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(669, 1, '100000000669', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(670, 1, '100000000670', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(671, 1, '100000000671', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(672, 1, '100000000672', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(673, 1, '100000000673', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(674, 1, '100000000674', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(675, 1, '100000000675', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(676, 1, '100000000676', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(677, 1, '100000000677', 0, 0, 1, '2016-01-20 11:13:58', NULL),
	(678, 1, '100000000678', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(679, 1, '100000000679', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(680, 1, '100000000680', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(681, 1, '100000000681', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(682, 1, '100000000682', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(683, 1, '100000000683', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(684, 1, '100000000684', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(685, 1, '100000000685', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(686, 1, '100000000686', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(687, 1, '100000000687', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(688, 1, '100000000688', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(689, 1, '100000000689', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(690, 1, '100000000690', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(691, 1, '100000000691', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(692, 1, '100000000692', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(693, 1, '100000000693', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(694, 1, '100000000694', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(695, 1, '100000000695', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(696, 1, '100000000696', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(697, 1, '100000000697', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(698, 1, '100000000698', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(699, 1, '100000000699', 0, 0, 1, '2016-01-20 11:13:59', NULL),
	(700, 1, '100000000700', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(701, 1, '100000000701', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(702, 1, '100000000702', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(703, 1, '100000000703', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(704, 1, '100000000704', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(705, 1, '100000000705', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(706, 1, '100000000706', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(707, 1, '100000000707', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(708, 1, '100000000708', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(709, 1, '100000000709', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(710, 1, '100000000710', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(711, 1, '100000000711', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(712, 1, '100000000712', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(713, 1, '100000000713', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(714, 1, '100000000714', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(715, 1, '100000000715', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(716, 1, '100000000716', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(717, 1, '100000000717', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(718, 1, '100000000718', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(719, 1, '100000000719', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(720, 1, '100000000720', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(721, 1, '100000000721', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(722, 1, '100000000722', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(723, 1, '100000000723', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(724, 1, '100000000724', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(725, 1, '100000000725', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(726, 1, '100000000726', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(727, 1, '100000000727', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(728, 1, '100000000728', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(729, 1, '100000000729', 0, 0, 1, '2016-01-20 11:14:00', NULL),
	(730, 1, '100000000730', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(731, 1, '100000000731', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(732, 1, '100000000732', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(733, 1, '100000000733', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(734, 1, '100000000734', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(735, 1, '100000000735', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(736, 1, '100000000736', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(737, 1, '100000000737', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(738, 1, '100000000738', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(739, 1, '100000000739', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(740, 1, '100000000740', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(741, 1, '100000000741', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(742, 1, '100000000742', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(743, 1, '100000000743', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(744, 1, '100000000744', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(745, 1, '100000000745', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(746, 1, '100000000746', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(747, 1, '100000000747', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(748, 1, '100000000748', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(749, 1, '100000000749', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(750, 1, '100000000750', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(751, 1, '100000000751', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(752, 1, '100000000752', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(753, 1, '100000000753', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(754, 1, '100000000754', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(755, 1, '100000000755', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(756, 1, '100000000756', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(757, 1, '100000000757', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(758, 1, '100000000758', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(759, 1, '100000000759', 0, 0, 1, '2016-01-20 11:14:01', NULL),
	(760, 1, '100000000760', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(761, 1, '100000000761', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(762, 1, '100000000762', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(763, 1, '100000000763', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(764, 1, '100000000764', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(765, 1, '100000000765', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(766, 1, '100000000766', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(767, 1, '100000000767', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(768, 1, '100000000768', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(769, 1, '100000000769', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(770, 1, '100000000770', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(771, 1, '100000000771', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(772, 1, '100000000772', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(773, 1, '100000000773', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(774, 1, '100000000774', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(775, 1, '100000000775', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(776, 1, '100000000776', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(777, 1, '100000000777', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(778, 1, '100000000778', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(779, 1, '100000000779', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(780, 1, '100000000780', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(781, 1, '100000000781', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(782, 1, '100000000782', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(783, 1, '100000000783', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(784, 1, '100000000784', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(785, 1, '100000000785', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(786, 1, '100000000786', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(787, 1, '100000000787', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(788, 1, '100000000788', 0, 0, 1, '2016-01-20 11:14:02', NULL),
	(789, 1, '100000000789', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(790, 1, '100000000790', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(791, 1, '100000000791', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(792, 1, '100000000792', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(793, 1, '100000000793', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(794, 1, '100000000794', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(795, 1, '100000000795', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(796, 1, '100000000796', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(797, 1, '100000000797', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(798, 1, '100000000798', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(799, 1, '100000000799', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(800, 1, '100000000800', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(801, 1, '100000000801', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(802, 1, '100000000802', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(803, 1, '100000000803', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(804, 1, '100000000804', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(805, 1, '100000000805', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(806, 1, '100000000806', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(807, 1, '100000000807', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(808, 1, '100000000808', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(809, 1, '100000000809', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(810, 1, '100000000810', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(811, 1, '100000000811', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(812, 1, '100000000812', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(813, 1, '100000000813', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(814, 1, '100000000814', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(815, 1, '100000000815', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(816, 1, '100000000816', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(817, 1, '100000000817', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(818, 1, '100000000818', 0, 0, 1, '2016-01-20 11:14:03', NULL),
	(819, 1, '100000000819', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(820, 1, '100000000820', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(821, 1, '100000000821', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(822, 1, '100000000822', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(823, 1, '100000000823', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(824, 1, '100000000824', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(825, 1, '100000000825', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(826, 1, '100000000826', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(827, 1, '100000000827', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(828, 1, '100000000828', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(829, 1, '100000000829', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(830, 1, '100000000830', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(831, 1, '100000000831', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(832, 1, '100000000832', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(833, 1, '100000000833', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(834, 1, '100000000834', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(835, 1, '100000000835', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(836, 1, '100000000836', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(837, 1, '100000000837', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(838, 1, '100000000838', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(839, 1, '100000000839', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(840, 1, '100000000840', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(841, 1, '100000000841', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(842, 1, '100000000842', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(843, 1, '100000000843', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(844, 1, '100000000844', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(845, 1, '100000000845', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(846, 1, '100000000846', 0, 0, 1, '2016-01-20 11:14:04', NULL),
	(847, 1, '100000000847', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(848, 1, '100000000848', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(849, 1, '100000000849', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(850, 1, '100000000850', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(851, 1, '100000000851', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(852, 1, '100000000852', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(853, 1, '100000000853', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(854, 1, '100000000854', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(855, 1, '100000000855', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(856, 1, '100000000856', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(857, 1, '100000000857', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(858, 1, '100000000858', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(859, 1, '100000000859', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(860, 1, '100000000860', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(861, 1, '100000000861', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(862, 1, '100000000862', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(863, 1, '100000000863', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(864, 1, '100000000864', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(865, 1, '100000000865', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(866, 1, '100000000866', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(867, 1, '100000000867', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(868, 1, '100000000868', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(869, 1, '100000000869', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(870, 1, '100000000870', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(871, 1, '100000000871', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(872, 1, '100000000872', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(873, 1, '100000000873', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(874, 1, '100000000874', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(875, 1, '100000000875', 0, 0, 1, '2016-01-20 11:14:05', NULL),
	(876, 1, '100000000876', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(877, 1, '100000000877', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(878, 1, '100000000878', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(879, 1, '100000000879', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(880, 1, '100000000880', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(881, 1, '100000000881', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(882, 1, '100000000882', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(883, 1, '100000000883', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(884, 1, '100000000884', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(885, 1, '100000000885', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(886, 1, '100000000886', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(887, 1, '100000000887', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(888, 1, '100000000888', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(889, 1, '100000000889', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(890, 1, '100000000890', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(891, 1, '100000000891', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(892, 1, '100000000892', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(893, 1, '100000000893', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(894, 1, '100000000894', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(895, 1, '100000000895', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(896, 1, '100000000896', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(897, 1, '100000000897', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(898, 1, '100000000898', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(899, 1, '100000000899', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(900, 1, '100000000900', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(901, 1, '100000000901', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(902, 1, '100000000902', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(903, 1, '100000000903', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(904, 1, '100000000904', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(905, 1, '100000000905', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(906, 1, '100000000906', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(907, 1, '100000000907', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(908, 1, '100000000908', 0, 0, 1, '2016-01-20 11:14:06', NULL),
	(909, 1, '100000000909', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(910, 1, '100000000910', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(911, 1, '100000000911', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(912, 1, '100000000912', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(913, 1, '100000000913', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(914, 1, '100000000914', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(915, 1, '100000000915', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(916, 1, '100000000916', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(917, 1, '100000000917', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(918, 1, '100000000918', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(919, 1, '100000000919', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(920, 1, '100000000920', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(921, 1, '100000000921', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(922, 1, '100000000922', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(923, 1, '100000000923', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(924, 1, '100000000924', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(925, 1, '100000000925', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(926, 1, '100000000926', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(927, 1, '100000000927', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(928, 1, '100000000928', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(929, 1, '100000000929', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(930, 1, '100000000930', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(931, 1, '100000000931', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(932, 1, '100000000932', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(933, 1, '100000000933', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(934, 1, '100000000934', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(935, 1, '100000000935', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(936, 1, '100000000936', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(937, 1, '100000000937', 0, 0, 1, '2016-01-20 11:14:07', NULL),
	(938, 1, '100000000938', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(939, 1, '100000000939', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(940, 1, '100000000940', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(941, 1, '100000000941', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(942, 1, '100000000942', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(943, 1, '100000000943', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(944, 1, '100000000944', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(945, 1, '100000000945', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(946, 1, '100000000946', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(947, 1, '100000000947', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(948, 1, '100000000948', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(949, 1, '100000000949', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(950, 1, '100000000950', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(951, 1, '100000000951', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(952, 1, '100000000952', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(953, 1, '100000000953', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(954, 1, '100000000954', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(955, 1, '100000000955', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(956, 1, '100000000956', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(957, 1, '100000000957', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(958, 1, '100000000958', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(959, 1, '100000000959', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(960, 1, '100000000960', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(961, 1, '100000000961', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(962, 1, '100000000962', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(963, 1, '100000000963', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(964, 1, '100000000964', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(965, 1, '100000000965', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(966, 1, '100000000966', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(967, 1, '100000000967', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(968, 1, '100000000968', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(969, 1, '100000000969', 0, 0, 1, '2016-01-20 11:14:08', NULL),
	(970, 1, '100000000970', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(971, 1, '100000000971', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(972, 1, '100000000972', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(973, 1, '100000000973', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(974, 1, '100000000974', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(975, 1, '100000000975', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(976, 1, '100000000976', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(977, 1, '100000000977', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(978, 1, '100000000978', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(979, 1, '100000000979', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(980, 1, '100000000980', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(981, 1, '100000000981', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(982, 1, '100000000982', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(983, 1, '100000000983', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(984, 1, '100000000984', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(985, 1, '100000000985', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(986, 1, '100000000986', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(987, 1, '100000000987', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(988, 1, '100000000988', 0, 0, 1, '2016-01-20 11:14:09', NULL),
	(989, 1, '100000000989', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(990, 1, '100000000990', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(991, 1, '100000000991', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(992, 1, '100000000992', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(993, 1, '100000000993', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(994, 1, '100000000994', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(995, 1, '100000000995', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(996, 1, '100000000996', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(997, 1, '100000000997', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(998, 1, '100000000998', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(999, 1, '100000000999', 0, 0, 1, '2016-01-20 11:14:10', NULL),
	(1000, 1, '100000001000', 0, 0, 1, '2016-01-20 11:14:10', NULL);
/*!40000 ALTER TABLE `ticket_series_info` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.ticket_status
CREATE TABLE IF NOT EXISTS `ticket_status` (
  `id_ticket_status` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_ticket_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.ticket_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `ticket_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_status` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.transaction
CREATE TABLE IF NOT EXISTS `transaction` (
  `id_transaction` int(11) NOT NULL AUTO_INCREMENT,
  `total_amount` decimal(9,2) NOT NULL,
  `amount_tendered` decimal(9,2) NOT NULL,
  `checkin_type_id` int(1) NOT NULL,
  `checkin_id` int(11) NOT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.transaction: ~0 rows (approximately)
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.trip_type
CREATE TABLE IF NOT EXISTS `trip_type` (
  `id_trip_type` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(24) NOT NULL,
  PRIMARY KEY (`id_trip_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.trip_type: ~3 rows (approximately)
/*!40000 ALTER TABLE `trip_type` DISABLE KEYS */;
INSERT INTO `trip_type` (`id_trip_type`, `type`) VALUES
	(1, 'One way'),
	(2, 'Round Trip'),
	(3, 'Multi-leg');
/*!40000 ALTER TABLE `trip_type` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.user
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_profile_id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `secure_token` varchar(128) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_failed_login` datetime DEFAULT NULL,
  `num_failed_login` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `is_new` tinyint(1) NOT NULL,
  `password_generated` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.user: ~4 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id_user`, `user_profile_id`, `username`, `password`, `secure_token`, `salt`, `last_login`, `last_failed_login`, `num_failed_login`, `enabled`, `is_deleted`, `is_locked`, `is_new`, `password_generated`) VALUES
	(1, 1, 'admin', 'ed1fd2fc25c6646d9c55bf4aa283477cca8a6f4e9b8af58a39529f1775210690', 'dba7f4dee86fd3c2b906065e85bd2a', 'YZBQ1JB8', '2016-01-25 10:35:07', '2016-01-25 10:35:07', 0, 1, 0, 0, 0, '2016-01-15'),
	(2, 2, 'administrator', '2fc042a2575272bb40d7471ece818a2705fb38a39888666d50fd5982246de790', '4d511b6ac3713cb842ca3cec7c01de', 'TIY2DLZ5', '2015-12-03 12:51:31', '2016-01-04 09:17:24', 1, 1, 0, 0, 1, '2015-12-03'),
	(3, 4, 'sales', 'ed1fd2fc25c6646d9c55bf4aa283477cca8a6f4e9b8af58a39529f1775210690', 'ca19f8f24b11ca7c6d161ee4308703', 'YZBQ1JB8', '2015-12-03 12:59:35', '1970-01-01 00:00:00', 0, 1, 0, 0, 1, '2015-12-03'),
	(4, 4, 'sales2', '238e8fd12353d9d40de7b906de13c08844228a3d0b1d339167c32b6939908400', '784040ed8ced09ba8389381957994a', 'BK0O5OSJ', '2016-01-20 11:45:38', '1970-01-01 00:00:00', 0, 1, 0, 0, 1, '2016-01-20');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.user_account
CREATE TABLE IF NOT EXISTS `user_account` (
  `id_user_account` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email_address` varchar(128) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `middlename` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) NOT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user_account`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.user_account: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` (`id_user_account`, `user_id`, `email_address`, `firstname`, `middlename`, `lastname`, `outlet_id`) VALUES
	(1, 1, 'superadmin@yondu.com', 'Super Admin', 'Super Admin', 'Super Admin', 0),
	(2, 2, 'admin@yondu.com', 'admin', 'admin', 'admin', 0),
	(3, 3, 'sales_tagabenta@yondu.com', 'Angeline', 'Meran', 'Quinto', 0),
	(4, 4, 'sales2_tagabenta@yondu.com', 'Lovi', 'L.', 'Poe', 0);
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.user_login
CREATE TABLE IF NOT EXISTS `user_login` (
  `user_id` int(11) NOT NULL,
  `token` varchar(128) NOT NULL,
  `salt` varchar(64) NOT NULL,
  `date_added` date NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.user_login: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_login` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.user_password
CREATE TABLE IF NOT EXISTS `user_password` (
  `id_user_password` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(32) NOT NULL,
  PRIMARY KEY (`id_user_password`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.user_password: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_password` DISABLE KEYS */;
INSERT INTO `user_password` (`id_user_password`, `user_id`, `password`, `salt`) VALUES
	(1, 1, 'ba9400f68feb8bddfed7f8dbb58871085069056876b812427a6e77abf8d85891', '6Z6DNBJD'),
	(2, 1, 'ed1fd2fc25c6646d9c55bf4aa283477cca8a6f4e9b8af58a39529f1775210690', 'YZBQ1JB8'),
	(3, 2, '2fc042a2575272bb40d7471ece818a2705fb38a39888666d50fd5982246de790', 'TIY2DLZ5'),
	(4, 3, 'ed1fd2fc25c6646d9c55bf4aa283477cca8a6f4e9b8af58a39529f1775210690', 'YZBQ1JB8'),
	(5, 4, '238e8fd12353d9d40de7b906de13c08844228a3d0b1d339167c32b6939908400', 'BK0O5OSJ');
/*!40000 ALTER TABLE `user_password` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.user_profile
CREATE TABLE IF NOT EXISTS `user_profile` (
  `id_user_profile` int(11) NOT NULL AUTO_INCREMENT,
  `user_profile` varchar(128) NOT NULL,
  `user_description` varchar(256) NOT NULL,
  `is_outlet` tinyint(1) NOT NULL,
  `has_mobile_access` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_user_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.user_profile: ~11 rows (approximately)
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` (`id_user_profile`, `user_profile`, `user_description`, `is_outlet`, `has_mobile_access`, `enabled`) VALUES
	(1, 'Super Administrator', 'Super Administrator', 0, 0, 1),
	(2, 'Admin', 'Admin', 0, 0, 1),
	(3, 'Operation Manager', 'Operation Manager', 0, 0, 1),
	(4, 'Sales Agent', 'Sales Agent', 0, 0, 1),
	(5, 'Third Party Sales Agent', 'Third Party Sales Agent', 0, 0, 1),
	(6, 'Check - In Clerk', 'Check - In Clerk', 0, 0, 1),
	(7, 'Baggage Clerk', 'Baggage Clerk', 0, 0, 1),
	(8, 'Baggage Cashier', 'Baggage Cashier', 0, 0, 1),
	(9, 'Reservation Officer', 'Reservation Officer', 0, 0, 1),
	(10, 'Port Supervisors', 'Port Supervisors', 0, 0, 1),
	(11, 'Debtor', 'Debtor', 0, 0, 1);
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.user_type
CREATE TABLE IF NOT EXISTS `user_type` (
  `id_user_type` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(128) NOT NULL,
  PRIMARY KEY (`id_user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.user_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_type` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.vessel
CREATE TABLE IF NOT EXISTS `vessel` (
  `id_vessel` int(11) NOT NULL AUTO_INCREMENT,
  `vessel_code` varchar(36) NOT NULL,
  `vessel` varchar(128) NOT NULL,
  `total_row` int(11) NOT NULL,
  `total_column` int(11) NOT NULL,
  `total_seats` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_vessel`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.vessel: ~5 rows (approximately)
/*!40000 ALTER TABLE `vessel` DISABLE KEYS */;
INSERT INTO `vessel` (`id_vessel`, `vessel_code`, `vessel`, `total_row`, `total_column`, `total_seats`, `enabled`) VALUES
	(1, 'V1', 'Vessel 1', 5, 12, 42, 1),
	(2, 'V2', 'Vessel 2', 5, 12, 42, 1),
	(3, 'V3', 'Vessel 3', 5, 12, 42, 1),
	(4, 'V4', 'Vessel 4', 5, 12, 42, 1),
	(5, 'V5', 'Vessel 5', 5, 12, 42, 1);
/*!40000 ALTER TABLE `vessel` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.vessel_seats
CREATE TABLE IF NOT EXISTS `vessel_seats` (
  `id_vessel_seats` int(11) NOT NULL AUTO_INCREMENT,
  `vessel_id` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `column` int(11) NOT NULL,
  `vessel_seats` varchar(128) NOT NULL,
  `accommodation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_vessel_seats`)
) ENGINE=InnoDB AUTO_INCREMENT=601 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.vessel_seats: ~300 rows (approximately)
/*!40000 ALTER TABLE `vessel_seats` DISABLE KEYS */;
INSERT INTO `vessel_seats` (`id_vessel_seats`, `vessel_id`, `row`, `column`, `vessel_seats`, `accommodation_id`) VALUES
	(301, 1, 1, 1, 'BC-1A', 2),
	(302, 1, 1, 2, 'N/A', 0),
	(303, 1, 1, 3, 'BC-1B', 2),
	(304, 1, 1, 4, 'BC-1C', 2),
	(305, 1, 1, 5, 'N/A', 0),
	(306, 1, 1, 6, 'N/A', 0),
	(307, 1, 1, 7, 'BC-1D', 2),
	(308, 1, 1, 8, 'BC-1E', 2),
	(309, 1, 1, 9, 'BC-1F', 2),
	(310, 1, 1, 10, 'BC-1G', 2),
	(311, 1, 1, 11, 'N/A', 0),
	(312, 1, 1, 12, 'N/A', 0),
	(313, 1, 2, 1, 'BC-2A', 2),
	(314, 1, 2, 2, 'N/A', 0),
	(315, 1, 2, 3, 'N/A', 0),
	(316, 1, 2, 4, 'BC-2B', 2),
	(317, 1, 2, 5, 'BC-2C', 2),
	(318, 1, 2, 6, 'BC-2D', 2),
	(319, 1, 2, 7, 'BC-2E', 2),
	(320, 1, 2, 8, 'BC-2F', 2),
	(321, 1, 2, 9, 'N/A', 0),
	(322, 1, 2, 10, 'N/A', 0),
	(323, 1, 2, 11, 'BC-2G', 2),
	(324, 1, 2, 12, 'BC-2H', 2),
	(325, 1, 3, 1, 'BC-3A', 2),
	(326, 1, 3, 2, 'BC-3B', 2),
	(327, 1, 3, 3, 'BC-3C', 2),
	(328, 1, 3, 4, 'N/A', 0),
	(329, 1, 3, 5, 'N/A', 0),
	(330, 1, 3, 6, 'BC-3D', 2),
	(331, 1, 3, 7, 'N/A', 0),
	(332, 1, 3, 8, 'N/A', 0),
	(333, 1, 3, 9, 'N/A', 0),
	(334, 1, 3, 10, 'BC-3E', 2),
	(335, 1, 3, 11, 'BC-3F', 2),
	(336, 1, 3, 12, 'BC-3G', 2),
	(337, 1, 4, 1, 'TC-1A', 1),
	(338, 1, 4, 2, 'TC-1B', 1),
	(339, 1, 4, 3, 'TC-1C', 1),
	(340, 1, 4, 4, 'TC-1D', 1),
	(341, 1, 4, 5, 'N/A', 0),
	(342, 1, 4, 6, 'N/A', 0),
	(343, 1, 4, 7, 'N/A', 0),
	(344, 1, 4, 8, 'TC-1E', 1),
	(345, 1, 4, 9, 'TC-1F', 1),
	(346, 1, 4, 10, 'TC-1G', 1),
	(347, 1, 4, 11, 'TC-1H', 1),
	(348, 1, 4, 12, 'N/A', 0),
	(349, 1, 5, 1, 'OA-1A', 3),
	(350, 1, 5, 2, 'OA-1B', 3),
	(351, 1, 5, 3, 'OA-1C', 3),
	(352, 1, 5, 4, 'OA-1D', 3),
	(353, 1, 5, 5, 'OA-1E', 3),
	(354, 1, 5, 6, 'OA-1F', 3),
	(355, 1, 5, 7, 'OA-1G', 3),
	(356, 1, 5, 8, 'OA-1H', 3),
	(357, 1, 5, 9, 'OA-1I', 3),
	(358, 1, 5, 10, 'OA-1J', 3),
	(359, 1, 5, 11, 'OA-1K', 3),
	(360, 1, 5, 12, 'OA-1L', 3),
	(361, 2, 1, 1, 'BC-1A', 2),
	(362, 2, 1, 2, 'N/A', 0),
	(363, 2, 1, 3, 'BC-1B', 2),
	(364, 2, 1, 4, 'BC-1C', 2),
	(365, 2, 1, 5, 'N/A', 0),
	(366, 2, 1, 6, 'N/A', 0),
	(367, 2, 1, 7, 'BC-1D', 2),
	(368, 2, 1, 8, 'BC-1E', 2),
	(369, 2, 1, 9, 'BC-1F', 2),
	(370, 2, 1, 10, 'BC-1G', 2),
	(371, 2, 1, 11, 'N/A', 0),
	(372, 2, 1, 12, 'N/A', 0),
	(373, 2, 2, 1, 'BC-2A', 2),
	(374, 2, 2, 2, 'N/A', 0),
	(375, 2, 2, 3, 'N/A', 0),
	(376, 2, 2, 4, 'BC-2B', 2),
	(377, 2, 2, 5, 'BC-2C', 2),
	(378, 2, 2, 6, 'BC-2D', 2),
	(379, 2, 2, 7, 'BC-2E', 2),
	(380, 2, 2, 8, 'BC-2F', 2),
	(381, 2, 2, 9, 'N/A', 0),
	(382, 2, 2, 10, 'N/A', 0),
	(383, 2, 2, 11, 'BC-2G', 2),
	(384, 2, 2, 12, 'BC-2H', 2),
	(385, 2, 3, 1, 'BC-3A', 2),
	(386, 2, 3, 2, 'BC-3B', 2),
	(387, 2, 3, 3, 'BC-3C', 2),
	(388, 2, 3, 4, 'N/A', 0),
	(389, 2, 3, 5, 'N/A', 0),
	(390, 2, 3, 6, 'BC-3D', 2),
	(391, 2, 3, 7, 'N/A', 0),
	(392, 2, 3, 8, 'N/A', 0),
	(393, 2, 3, 9, 'N/A', 0),
	(394, 2, 3, 10, 'BC-3E', 2),
	(395, 2, 3, 11, 'BC-3F', 2),
	(396, 2, 3, 12, 'BC-3G', 2),
	(397, 2, 4, 1, 'TC-1A', 1),
	(398, 2, 4, 2, 'TC-1B', 1),
	(399, 2, 4, 3, 'TC-1C', 1),
	(400, 2, 4, 4, 'TC-1D', 1),
	(401, 2, 4, 5, 'N/A', 0),
	(402, 2, 4, 6, 'N/A', 0),
	(403, 2, 4, 7, 'N/A', 0),
	(404, 2, 4, 8, 'TC-1E', 1),
	(405, 2, 4, 9, 'TC-1F', 1),
	(406, 2, 4, 10, 'TC-1G', 1),
	(407, 2, 4, 11, 'TC-1H', 1),
	(408, 2, 4, 12, 'N/A', 0),
	(409, 2, 5, 1, 'OA-1A', 3),
	(410, 2, 5, 2, 'OA-1B', 3),
	(411, 2, 5, 3, 'OA-1C', 3),
	(412, 2, 5, 4, 'OA-1D', 3),
	(413, 2, 5, 5, 'OA-1E', 3),
	(414, 2, 5, 6, 'OA-1F', 3),
	(415, 2, 5, 7, 'OA-1G', 3),
	(416, 2, 5, 8, 'OA-1H', 3),
	(417, 2, 5, 9, 'OA-1I', 3),
	(418, 2, 5, 10, 'OA-1J', 3),
	(419, 2, 5, 11, 'OA-1K', 3),
	(420, 2, 5, 12, 'OA-1L', 3),
	(421, 3, 1, 1, 'BC-1A', 2),
	(422, 3, 1, 2, 'N/A', 0),
	(423, 3, 1, 3, 'BC-1B', 2),
	(424, 3, 1, 4, 'BC-1C', 2),
	(425, 3, 1, 5, 'N/A', 0),
	(426, 3, 1, 6, 'N/A', 0),
	(427, 3, 1, 7, 'BC-1D', 2),
	(428, 3, 1, 8, 'BC-1E', 2),
	(429, 3, 1, 9, 'BC-1F', 2),
	(430, 3, 1, 10, 'BC-1G', 2),
	(431, 3, 1, 11, 'N/A', 0),
	(432, 3, 1, 12, 'N/A', 0),
	(433, 3, 2, 1, 'BC-2A', 2),
	(434, 3, 2, 2, 'N/A', 0),
	(435, 3, 2, 3, 'N/A', 0),
	(436, 3, 2, 4, 'BC-2B', 2),
	(437, 3, 2, 5, 'BC-2C', 2),
	(438, 3, 2, 6, 'BC-2D', 2),
	(439, 3, 2, 7, 'BC-2E', 2),
	(440, 3, 2, 8, 'BC-2F', 2),
	(441, 3, 2, 9, 'N/A', 0),
	(442, 3, 2, 10, 'N/A', 0),
	(443, 3, 2, 11, 'BC-2G', 2),
	(444, 3, 2, 12, 'BC-2H', 2),
	(445, 3, 3, 1, 'BC-3A', 2),
	(446, 3, 3, 2, 'BC-3B', 2),
	(447, 3, 3, 3, 'BC-3C', 2),
	(448, 3, 3, 4, 'N/A', 0),
	(449, 3, 3, 5, 'N/A', 0),
	(450, 3, 3, 6, 'BC-3D', 2),
	(451, 3, 3, 7, 'N/A', 0),
	(452, 3, 3, 8, 'N/A', 0),
	(453, 3, 3, 9, 'N/A', 0),
	(454, 3, 3, 10, 'BC-3E', 2),
	(455, 3, 3, 11, 'BC-3F', 2),
	(456, 3, 3, 12, 'BC-3G', 2),
	(457, 3, 4, 1, 'TC-1A', 1),
	(458, 3, 4, 2, 'TC-1B', 1),
	(459, 3, 4, 3, 'TC-1C', 1),
	(460, 3, 4, 4, 'TC-1D', 1),
	(461, 3, 4, 5, 'N/A', 0),
	(462, 3, 4, 6, 'N/A', 0),
	(463, 3, 4, 7, 'N/A', 0),
	(464, 3, 4, 8, 'TC-1E', 1),
	(465, 3, 4, 9, 'TC-1F', 1),
	(466, 3, 4, 10, 'TC-1G', 1),
	(467, 3, 4, 11, 'TC-1H', 1),
	(468, 3, 4, 12, 'N/A', 0),
	(469, 3, 5, 1, 'OA-1A', 3),
	(470, 3, 5, 2, 'OA-1B', 3),
	(471, 3, 5, 3, 'OA-1C', 3),
	(472, 3, 5, 4, 'OA-1D', 3),
	(473, 3, 5, 5, 'OA-1E', 3),
	(474, 3, 5, 6, 'OA-1F', 3),
	(475, 3, 5, 7, 'OA-1G', 3),
	(476, 3, 5, 8, 'OA-1H', 3),
	(477, 3, 5, 9, 'OA-1I', 3),
	(478, 3, 5, 10, 'OA-1J', 3),
	(479, 3, 5, 11, 'OA-1K', 3),
	(480, 3, 5, 12, 'OA-1L', 3),
	(481, 4, 1, 1, 'BC-1A', 2),
	(482, 4, 1, 2, 'N/A', 0),
	(483, 4, 1, 3, 'BC-1B', 2),
	(484, 4, 1, 4, 'BC-1C', 2),
	(485, 4, 1, 5, 'N/A', 0),
	(486, 4, 1, 6, 'N/A', 0),
	(487, 4, 1, 7, 'BC-1D', 2),
	(488, 4, 1, 8, 'BC-1E', 2),
	(489, 4, 1, 9, 'BC-1F', 2),
	(490, 4, 1, 10, 'BC-1G', 2),
	(491, 4, 1, 11, 'N/A', 0),
	(492, 4, 1, 12, 'N/A', 0),
	(493, 4, 2, 1, 'BC-2A', 2),
	(494, 4, 2, 2, 'N/A', 0),
	(495, 4, 2, 3, 'N/A', 0),
	(496, 4, 2, 4, 'BC-2B', 2),
	(497, 4, 2, 5, 'BC-2C', 2),
	(498, 4, 2, 6, 'BC-2D', 2),
	(499, 4, 2, 7, 'BC-2E', 2),
	(500, 4, 2, 8, 'BC-2F', 2),
	(501, 4, 2, 9, 'N/A', 0),
	(502, 4, 2, 10, 'N/A', 0),
	(503, 4, 2, 11, 'BC-2G', 2),
	(504, 4, 2, 12, 'BC-2H', 2),
	(505, 4, 3, 1, 'BC-3A', 2),
	(506, 4, 3, 2, 'BC-3B', 2),
	(507, 4, 3, 3, 'BC-3C', 2),
	(508, 4, 3, 4, 'N/A', 0),
	(509, 4, 3, 5, 'N/A', 0),
	(510, 4, 3, 6, 'BC-3D', 2),
	(511, 4, 3, 7, 'N/A', 0),
	(512, 4, 3, 8, 'N/A', 0),
	(513, 4, 3, 9, 'N/A', 0),
	(514, 4, 3, 10, 'BC-3E', 2),
	(515, 4, 3, 11, 'BC-3F', 2),
	(516, 4, 3, 12, 'BC-3G', 2),
	(517, 4, 4, 1, 'TC-1A', 1),
	(518, 4, 4, 2, 'TC-1B', 1),
	(519, 4, 4, 3, 'TC-1C', 1),
	(520, 4, 4, 4, 'TC-1D', 1),
	(521, 4, 4, 5, 'N/A', 0),
	(522, 4, 4, 6, 'N/A', 0),
	(523, 4, 4, 7, 'N/A', 0),
	(524, 4, 4, 8, 'TC-1E', 1),
	(525, 4, 4, 9, 'TC-1F', 1),
	(526, 4, 4, 10, 'TC-1G', 1),
	(527, 4, 4, 11, 'TC-1H', 1),
	(528, 4, 4, 12, 'N/A', 0),
	(529, 4, 5, 1, 'OA-1A', 3),
	(530, 4, 5, 2, 'OA-1B', 3),
	(531, 4, 5, 3, 'OA-1C', 3),
	(532, 4, 5, 4, 'OA-1D', 3),
	(533, 4, 5, 5, 'OA-1E', 3),
	(534, 4, 5, 6, 'OA-1F', 3),
	(535, 4, 5, 7, 'OA-1G', 3),
	(536, 4, 5, 8, 'OA-1H', 3),
	(537, 4, 5, 9, 'OA-1I', 3),
	(538, 4, 5, 10, 'OA-1J', 3),
	(539, 4, 5, 11, 'OA-1K', 3),
	(540, 4, 5, 12, 'OA-1L', 3),
	(541, 5, 1, 1, 'BC-1A', 2),
	(542, 5, 1, 2, 'N/A', 0),
	(543, 5, 1, 3, 'BC-1B', 2),
	(544, 5, 1, 4, 'BC-1C', 2),
	(545, 5, 1, 5, 'N/A', 0),
	(546, 5, 1, 6, 'N/A', 0),
	(547, 5, 1, 7, 'BC-1D', 2),
	(548, 5, 1, 8, 'BC-1E', 2),
	(549, 5, 1, 9, 'BC-1F', 2),
	(550, 5, 1, 10, 'BC-1G', 2),
	(551, 5, 1, 11, 'N/A', 0),
	(552, 5, 1, 12, 'N/A', 0),
	(553, 5, 2, 1, 'BC-2A', 2),
	(554, 5, 2, 2, 'N/A', 0),
	(555, 5, 2, 3, 'N/A', 0),
	(556, 5, 2, 4, 'BC-2B', 2),
	(557, 5, 2, 5, 'BC-2C', 2),
	(558, 5, 2, 6, 'BC-2D', 2),
	(559, 5, 2, 7, 'BC-2E', 2),
	(560, 5, 2, 8, 'BC-2F', 2),
	(561, 5, 2, 9, 'N/A', 0),
	(562, 5, 2, 10, 'N/A', 0),
	(563, 5, 2, 11, 'BC-2G', 2),
	(564, 5, 2, 12, 'BC-2H', 2),
	(565, 5, 3, 1, 'BC-3A', 2),
	(566, 5, 3, 2, 'BC-3B', 2),
	(567, 5, 3, 3, 'BC-3C', 2),
	(568, 5, 3, 4, 'N/A', 0),
	(569, 5, 3, 5, 'N/A', 0),
	(570, 5, 3, 6, 'BC-3D', 2),
	(571, 5, 3, 7, 'N/A', 0),
	(572, 5, 3, 8, 'N/A', 0),
	(573, 5, 3, 9, 'N/A', 0),
	(574, 5, 3, 10, 'BC-3E', 2),
	(575, 5, 3, 11, 'BC-3F', 2),
	(576, 5, 3, 12, 'BC-3G', 2),
	(577, 5, 4, 1, 'TC-1A', 1),
	(578, 5, 4, 2, 'TC-1B', 1),
	(579, 5, 4, 3, 'TC-1C', 1),
	(580, 5, 4, 4, 'TC-1D', 1),
	(581, 5, 4, 5, 'N/A', 0),
	(582, 5, 4, 6, 'N/A', 0),
	(583, 5, 4, 7, 'N/A', 0),
	(584, 5, 4, 8, 'TC-1E', 1),
	(585, 5, 4, 9, 'TC-1F', 1),
	(586, 5, 4, 10, 'TC-1G', 1),
	(587, 5, 4, 11, 'TC-1H', 1),
	(588, 5, 4, 12, 'N/A', 0),
	(589, 5, 5, 1, 'OA-1A', 3),
	(590, 5, 5, 2, 'OA-1B', 3),
	(591, 5, 5, 3, 'OA-1C', 3),
	(592, 5, 5, 4, 'OA-1D', 3),
	(593, 5, 5, 5, 'OA-1E', 3),
	(594, 5, 5, 6, 'OA-1F', 3),
	(595, 5, 5, 7, 'OA-1G', 3),
	(596, 5, 5, 8, 'OA-1H', 3),
	(597, 5, 5, 9, 'OA-1I', 3),
	(598, 5, 5, 10, 'OA-1J', 3),
	(599, 5, 5, 11, 'OA-1K', 3),
	(600, 5, 5, 12, 'OA-1L', 3);
/*!40000 ALTER TABLE `vessel_seats` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.void_tickets
CREATE TABLE IF NOT EXISTS `void_tickets` (
  `id_void_tickets` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `void_reason` text,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`id_void_tickets`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.void_tickets: ~0 rows (approximately)
/*!40000 ALTER TABLE `void_tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `void_tickets` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.voyage
CREATE TABLE IF NOT EXISTS `voyage` (
  `id_voyage` int(11) NOT NULL AUTO_INCREMENT,
  `voyage` varchar(24) NOT NULL,
  `origin` int(11) NOT NULL,
  `destination` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `trip_type_id` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_voyage`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.voyage: ~18 rows (approximately)
/*!40000 ALTER TABLE `voyage` DISABLE KEYS */;
INSERT INTO `voyage` (`id_voyage`, `voyage`, `origin`, `destination`, `status`, `enabled`, `trip_type_id`) VALUES
	(1, 'V-CEB-DUM-01', 1, 3, 0, 1, 3),
	(2, 'V-CEB-TAG-01', 1, 2, 0, 1, 1),
	(3, 'V-CEB-TAG-02', 1, 2, 0, 1, 1),
	(4, 'V-DUM-SIQ-01', 3, 4, 0, 1, 1),
	(5, 'V-CEB-SIQ-01', 1, 4, 0, 1, 3),
	(6, 'V-SIQ-DUM-01', 4, 3, 0, 1, 1),
	(7, 'V-TAG-CEB-01', 2, 1, 0, 1, 1),
	(8, 'V-CEB-DUM-02', 1, 3, 0, 1, 3),
	(9, 'V-TAG-DUM-01', 2, 3, 0, 1, 1),
	(10, 'V-SIQ-CEB-01', 4, 1, 0, 1, 3),
	(11, 'V-DUM-CEB-01', 3, 1, 0, 1, 3),
	(12, 'V-CEB-TAG-03', 1, 2, 0, 1, 1),
	(13, 'V-DUM-TAG-02', 3, 2, 0, 1, 1),
	(14, 'V-DUM-TAG-03', 3, 2, 0, 1, 1),
	(15, 'V-SIQ-TAG-01', 4, 2, 0, 1, 3),
	(16, 'V-TAG-DUM-02', 2, 3, 0, 1, 1),
	(17, 'V-DUM-SIQ-02', 3, 4, 0, 1, 1),
	(18, 'V-CEB-DUM-03', 1, 3, 0, 1, 3);
/*!40000 ALTER TABLE `voyage` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.voyage_management
CREATE TABLE IF NOT EXISTS `voyage_management` (
  `id_voyage_management` int(11) NOT NULL AUTO_INCREMENT,
  `voyage_id` int(11) NOT NULL,
  PRIMARY KEY (`id_voyage_management`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.voyage_management: ~83 rows (approximately)
/*!40000 ALTER TABLE `voyage_management` DISABLE KEYS */;
INSERT INTO `voyage_management` (`id_voyage_management`, `voyage_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 2),
	(5, 2),
	(6, 2),
	(7, 2),
	(8, 3),
	(9, 3),
	(10, 3),
	(11, 4),
	(12, 4),
	(13, 4),
	(14, 4),
	(15, 4),
	(16, 5),
	(17, 5),
	(18, 5),
	(19, 6),
	(20, 6),
	(21, 6),
	(22, 6),
	(23, 6),
	(24, 7),
	(25, 7),
	(26, 7),
	(27, 7),
	(28, 7),
	(29, 8),
	(30, 8),
	(31, 8),
	(32, 8),
	(33, 8),
	(34, 9),
	(35, 9),
	(36, 9),
	(37, 9),
	(38, 9),
	(39, 9),
	(40, 10),
	(41, 10),
	(42, 10),
	(43, 11),
	(44, 11),
	(45, 11),
	(46, 11),
	(47, 12),
	(48, 12),
	(49, 12),
	(50, 12),
	(51, 13),
	(52, 13),
	(53, 13),
	(54, 14),
	(55, 14),
	(56, 14),
	(57, 14),
	(58, 14),
	(59, 14),
	(60, 15),
	(61, 15),
	(62, 15),
	(63, 15),
	(64, 15),
	(65, 15),
	(66, 16),
	(67, 16),
	(68, 16),
	(69, 16),
	(70, 16),
	(71, 16),
	(72, 16),
	(73, 16),
	(74, 17),
	(75, 17),
	(76, 17),
	(77, 17),
	(78, 17),
	(79, 17),
	(80, 17),
	(81, 17),
	(82, 18),
	(83, 18);
/*!40000 ALTER TABLE `voyage_management` ENABLE KEYS */;


-- Dumping structure for table oceanjet1_db.voyage_management_status
CREATE TABLE IF NOT EXISTS `voyage_management_status` (
  `id_voyage_management_status` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(36) NOT NULL,
  PRIMARY KEY (`id_voyage_management_status`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table oceanjet1_db.voyage_management_status: ~8 rows (approximately)
/*!40000 ALTER TABLE `voyage_management_status` DISABLE KEYS */;
INSERT INTO `voyage_management_status` (`id_voyage_management_status`, `description`) VALUES
	(1, 'On Dock'),
	(2, 'Departed on time'),
	(3, 'Departed but delayed'),
	(4, 'Undeparted'),
	(5, 'Blocked'),
	(6, 'Cancelled'),
	(7, 'Unblocked'),
	(8, 'Arrived at port of destination');
/*!40000 ALTER TABLE `voyage_management_status` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
