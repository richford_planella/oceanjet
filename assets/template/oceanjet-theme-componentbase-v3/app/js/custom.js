$(document).ready(function () {
  //dataTables
  var table = $('#example').DataTable();
  $('#btnSearch').click(function () {
    table.search("search").draw();
  });


  //Search btn Fade in Fade out action 
  $('.btn-search').click(function () {
    $('div.noinfo').fadeOut(250, function () {
      $('.hide').removeClass('hide');
      $('div.info').fadeIn(250);
    })
  });

  // clicked after successful void
  $('.btn-oj-yes').click(function () {
    $('div.info').fadeOut(250, function () {
      $('div.info').addClass('hide');
      $('div.noinfo').fadeIn(250);
    })
  });


  //  page open 
  $('.btn-open').click(function () {
    url = $('select.open-page').val();
    window.open(url, "_self");
  });

  //  //  Modal Active button
  //  $(".dropdown-menu.status li a").click(function(){
  //    var selText = $(this).text();
  //    $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
  //  });

  //  Bootstrap Select
  $('.selectpicker').selectpicker();

  //  Check all checkbox
  $('#checkAll').click(function () {
    var checkedStatus = this.checked;
    $('.user-visibility .checkbox-inline').find('input:checkbox').each(function () {
      $(this).prop('checked', checkedStatus);
    });
  });
  
  //Table Check box all
  $('#generated-all').click(function (e) {
    $(this).closest('table.e-voucher-table').find('td input:checkbox').prop('checked', this.checked);
  });
  
  
  $("#generated-all").on("change", function(e){
    if($("#generated-all").attr("checked")){
      $("#batch-cancel").button("enable");
    } else {
      $("#batch-cancel").button("disable");
    }

  });
  
  $("#generated-all, table.e-voucher-table td input:checkbox, #table-checked-all").click(function () {
      $("#batch-cancel").prop("disabled", !this.checked);
  });


  //  clear button upload
  // Referneces
  var clearBn = $("#a-clear");
  var showclear = $("#a-upload-file");

  // Setup the clear functionality
  clearBn.on("click", function () {
    var control = $("#a-upload-file");
    control.replaceWith(control.val('').clone(true));
    $(this).hide();
  });

  showclear.on("change", function () {
    clearBn.show();
  });

  $("#age-bracket").change(function () {
    $(".age-range-wrap").toggleClass("show");
  });



  //  Image Profile Preview
  var SITE = SITE || {};

  SITE.fileInputs = function () {
    var $this = $(this),
      $fakeFile = $this.siblings('.file-holder');
  };


  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#img-uploaded').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(".uploader").change(function () {
    readURL(this);
  });



  //custom datepicker
  $('.custom-datepicker, .oj-date-picker, .date-to, .date-from, .datetimepicker-age').datetimepicker({
    format: 'YYYY-MM-DD'
  });
  
  $('.oj-date-value').datetimepicker({
//    locale: 'nl',
    format: 'YYYY-MM-DD'
  });
  
  //  Age Fromtat
  $('.datetimepicker-age').datetimepicker().on('dp.change', function(e) {
    setAge(e.date);
  });
  
//  Date Time Picker From and To
  var dateTo = $('.date-from-to');
  var datePair = [
    {
      from: "date-from",
      to: "date-to"
    }
  ]
  if(dateTo.length){
    dateTo.find('.date-from').datetimepicker();
    dateTo.find('.date-to').datetimepicker({
      useCurrent: false //Important! See issue #1075
    });
    dateTo.find('.date-from').on("dp.change", function (e) {
        $(this).parents(".date-from-to").find(".date-to").data("DateTimePicker").minDate(e.date)
    });
    dateTo.find('.date-to').on("dp.change", function (e) {
       $(this).parents(".date-from-to").find(".date-from").data("DateTimePicker").maxDate(e.date)
    });
  }
  


  //Table Header filter
  $('#show-select-forms').change(function () {
    $('.forms-hide').hide();
    $('#' + $(this).val()).css('display', 'inline-block');
  });


//Checkbox List Toggle
  if (!$(".checkbox-fields__check").is(":checked")) {
    $(".checkbox-fields .form-control, .checkbox-fields .checkbox-fields__radio").prop("disabled",true);
    $(".checkbox-fields .form-control, .checkbox-fields .checkbox-fields__radio").selectpicker('refresh');
  }
  
  $(".checkbox-fields__check").click(function () {
    if ($(this).is(":checked")) {
      $(this).parents('.checkbox-fields').find('.form-control, .checkbox-fields__radio').prop("disabled", false);
    } else {
      $(this).parents('.checkbox-fields').find('.form-control, .checkbox-fields__radio').prop("disabled", true);
    }
    $(this).parents('.checkbox-fields').find('.form-control, .checkbox-fields__radio').selectpicker('refresh');
  });

  

  
//  //Table Checkbox toggle all

    //Checkbox toggle all
//  $(".condition-check").click(function () {
//    $(".oj-check").prop('checked', $(this).prop('checked'));
//  });
//  

//  Batch button 
//  $('.oj-check').click(function () {
//    if ($('.oj-check').is(':checked')) {
//      console.log('1')
//      $('.batch-btn').removeAttr('disabled')
//    } else {
//      console.log('b')
//      $('.batch-btn').attr('disabled', true)
//    }
//  })

//Form Check box
var update_check = function () {
  var fparent = $(this).parents('.f-checked');
  var bselect = fparent.find('.f-checked--fields');
  if (fparent.length) {
    if ($(this).is(":checked")) {
      fparent.find('.f-checked--fields').prop('disabled', false);
    } else {
      fparent.find('.f-checked--fields').prop('disabled', 'disabled');
    }
    if (bselect.length){
      bselect.selectpicker('refresh');
    }
  }

};

$(update_check);
$(".f-checked--check").change(update_check);

  
  datepickerDiff()

});



//Time Picker with output difference
var datepickerDiff = function () {

  //Timepicker    
  $('.oj-time-picker').datetimepicker({
    format: 'LT',
    //    debug: true
  });


  $('.oj-time-picker').on('dp.change', function (e) {
    var pdate = $(this).parents('.oj-leg');
    var fromTime = moment(pdate.find('.oj-etd').val(), 'h:mm a');
    var toTime = moment(pdate.find('.oj-eta').val(), 'h:mm a');
    var timeDiff = toTime.diff(fromTime)
    pdate.find('.oj-travel-time').val(moment.utc(timeDiff).format('H:mm'));

  })
  
  
  //Select Option User Matrix
  $('#user-matrix__select').change(function(){
    $('.user-matrix__modules').hide();
    $('#' + $(this).val()).show();
  });
}

//oj-leg Add Leg for timepicker
var legCounter = function () {
  $(".oj-leg").each(function (index) {
    $(this).find(".oj-leg__title").html("Leg " + (index + 1))
  })
}

//DataTables Checkboxs All
//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table) {
  var $table = table.table().container();
  var $chkbox_all = $('tbody input[type="checkbox"]', $table);
  var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
  var chkbox_select_all = $('thead input[type="checkbox"]', $table).get(0);

  // If none of the checkboxes are checked
  if ($chkbox_checked.length === 0) {
    chkbox_select_all.checked = false;
    if ('indeterminate' in chkbox_select_all) {
      chkbox_select_all.indeterminate = false;
    }

    // If all of the checkboxes are checked
  } else if ($chkbox_checked.length === $chkbox_all.length) {
    chkbox_select_all.checked = true;
    if ('indeterminate' in chkbox_select_all) {
      chkbox_select_all.indeterminate = false;
    }

    // If some of the checkboxes are checked
  } else {
    chkbox_select_all.checked = true;
    if ('indeterminate' in chkbox_select_all) {
      chkbox_select_all.indeterminate = true;
    }
  }
}

var dataTableCheckAll = function (tableInput, table) {
  // Handle row selection event
  tableInput.on('select.dt deselect.dt', function (e, api, type, items) {
    if (e.type === 'select') {
      $('tr.selected input[type="checkbox"]', api.table().container()).prop('checked', true);
      $('.batch-btn').removeAttr('disabled');
    } else {
      $('tr:not(.selected) input[type="checkbox"]', api.table().container()).prop('checked', false);
      $('.batch-btn').attr('disabled', true);
    }

    // Update state of "Select all" control
    updateDataTableSelectAllCtrl(table);
  });

  // Handle click on "Select all" control
  tableInput.find('thead').on('click', 'input[type="checkbox"]', function (e) {
    if (this.checked) {
      table.rows({
        page: 'current'
      }).select();
    } else {
      table.rows({
        page: 'current'
      }).deselect();
    }

    e.stopPropagation();
  });

  // Handle click on heading containing "Select all" control
  $('thead', table.table().container()).on('click', 'th:first-child', function (e) {
    $('input[type="checkbox"]', this).trigger('click');
  });

  // Handle table draw event
  tableInput.on('draw.dt', function () {
    // Update state of "Select all" control
    updateDataTableSelectAllCtrl(table);
  });

}

//Age Format

function setAge(d){
  var age = moment().diff(d, 'years', true);
  $('#age').val(age.toFixed(0));
}



