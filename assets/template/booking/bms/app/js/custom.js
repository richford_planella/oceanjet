$(document).ready(function(){
	
	// FOR CUSTOM SCROLLER
	/*$(".malihu").mCustomScrollbar({
	    theme : "dark"
	});*/
	var finput = $('.bms-file-input');
	if( finput.length ){
		finput.on('change', function(){
			var val = $(this).val(), text;
			
			if( val.length ){
				var trimmed = val.substr(val.lastIndexOf('\\') + 1, val.length);
				text = trimmed;
			}else{
				text = "Choose file";
			}
			$(this).next('label').html(text);
		});
	}

	// FOR Content Loading
	// bmsApp.init();
});

var bmsApp = (function(){
	// Load Page mark-ups //
	// Can be deleted someday . lol //
	var loadPages = function(){
		var step1, step2, step3, step4, step5, html = '';
		var bmsCallback = function(data){
			html += data;
		};

		$('.bms-pagestep').each(function(){
			var stepnum = $(this).attr('data-step');
			$(this).load('templates/bms-step'+stepnum+'.html', bmsCallback);
		});

		bindFormLinks();
	};

	var tabListener = function(){
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		  // e.target // newly activated tab
		  console.log('yeah', e.target, 'yow', e.relatedTarget);
		  // e.relatedTarget // previous active tab
		});
	};

	// Display Page Method //
	var renderPage = function(mode){
		// console.log('render page yes', mode);

		if( mode === 'next'){
			$('#fn-link-continue').tab('show');
		}else if( mode === 'prev'){
			$('#fn-link-back').tab('show');
		}else{
			// Error
		}

		$('.bms-pagestep.tab-pane').promise().done(function(){
			bindFormLinks();
			var yeah1 = $('.bms-pagestep.in.active').attr('id');
			// console.log('yeah!', yeah1);
		});
	};

	// Initial State of the page
	var init = function(){
		// magic codes here
		console.log('It\'s ALIIVE!');
		loadPages();
		tabListener();
	};

	var bindFormLinks = function(){
		$('.fn-link').unbind('click');
		var tab = $('.tab-pane.active'),
			backBtn = $('#fn-link-back'),
			nextBtn = $('#fn-link-continue'),
			backTarget = '#' + $('.bms-pagestep.in.active').prev('.bms-pagestep').attr('id'),
			nextTarget = '#' + $('.bms-pagestep.in.active').next('.bms-pagestep').attr('id'),
			backIsActive = tab.not('div.tab-pane:first-child').length,
			nextIsActive = tab.not('div.tab-pane:last-child').length,
			isClicked = function(mode){
				// Open pre-loader
				// Validate form
				// Display Next / Previous Page
				renderPage(mode);
				// Display Links
			};

		// console.log('bindings yes');

		if( backIsActive ){
			backBtn.show();
			backBtn.attr('href',backTarget);
			backBtn.click(function(){ isClicked('prev'); });
		}else{
			backBtn.hide();
		}

		if( nextIsActive ){
			nextBtn.show();
			nextBtn.attr('href',nextTarget);
			nextBtn.click(function(){ isClicked('next'); });
		}else{
			nextBtn.hide();
		}

		// console.log(backTarget, nextTarget);
	};

	return {
		init : init
	}
}());



/* SUPER CUSTOM APP
var bmsApp = (function(){

	var componentMap = {
		steps : {
			el : '.bms-page-header',
			fl : 'templates/bms-steps.html'
		},
		step1 : {
			el : '.bms-main-content',
			fl : 'templates/bms-step1.html'
		},
		step2 : {
			el : '.bms-main-content',
			fl : 'templates/bms-step2.html'
		},
		step3 : {
			el : '.bms-main-content',
			fl : 'templates/bms-step3.html'
		},
		step4 : {
			el : '.bms-main-content',
			fl : 'templates/bms-step4.html'
		},
		step5 : {
			el : '.bms-main-content',
			fl : 'templates/bms-step5.html'
		},
		profile : {
			el : '.bms-main-content',
			fl : 'templates/bms-profile.html'
		}
	};

	var links = {
		'#voyage-details' : componentMap.step1,
		'#passenger-details' : componentMap.step2,
		'#voyage-summary' : componentMap.step3,
		'#payment' : componentMap.step4,
		'#confirmation' : componentMap.step5
	};

	var componentLoader = function(component){
		var el = component.el; 
		$(el).html('Loading please wait...');
		$(el).load(component.fl, function(){
			// console.log('yeah' , component);
			if(el !== '.bms-page-header') bindings();
		});
	};

	var bindings = function(){
		$('.fn-link').click(function(){
			var link = $(this).attr('href');
			console.log(link, links[link]);
			componentLoader(links[link]);
		});
	};

	// Initial State of the page
	var init = function(){
		componentLoader(componentMap.steps);
		componentLoader(componentMap.step1);
	};

	return {
		init : init
	}

}());

*/