$(function(){

	// Initialize outlet
	outlet();

	$(document).on("change","#outlet_type",function() {
		outlet();
	});

	// Hide and Show Commission and Port Fields
	function outlet()
	{
		if($("#outlet_type").val() == "Internal"){
			$("#commission").hide();
			$("#outlet_commission").val('0.00');
			$("#outlet_commission").removeAttr("max");
			$("#port").show();
		}
		else if($("#outlet_type").val() == "External") {
			$('#port_id').val('').selectpicker('refresh');
			$("#outlet_commission").attr("max", "100");
			$("#commission").show();
			$("#port").hide();
		}
	}

});

// Module Name
var module = "Outlet";

// Modal dialog
$(document).on('click', '.activate', function(event){
	modalDialog($(this), module, "Are you sure you want to re-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.deactivate', function(event){
	modalDialog($(this), module, "Are you sure you want to de-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.remove', function(event){
	modalDialog($(this), module, "Are you sure you want to delete " + module.toLowerCase() + " ?"); event.preventDefault();
});

// Activate, Deactivate, Remove Modal
function modalDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'No',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}, 
		{
			label: 'Yes',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				// ajax post/jQuery post code
				window.location.replace(link);
			}
		}]
	});
        
    return false;
}