$(document).on("change", "#origin, #destination", function(){
	checkPort();
	if($("#origin").val() && $("#destination").val())
		voyager('voyage');
	$(".trip_details").val('');
});

// Fix for the modal
$(".roro-list").on("click", function(){
	// Add Body
	$('body').addClass('margin-dialog'); 
});

// Check if the ticket number reaches its maxlength
// Execute the query
$('#ticket_number').keyup(function(){
	// Get Ticket details
	voyager('dtTicket');
});

// Check-in RORO
$("#roro-checkin").on("click", function(){
	if($("#booking_roro_id").val()){
		if($("#subvoyage_management_status_id").val() == 9 || $("#subvoyage_management_status_id").val() == 10)
			voyager('checkin');
		else
			modalDismissDialog($(this), "Rolling Cargo Check-in", "Change voyage status to open for check-in. Please try again.");
	}
	else
	{
		modalDismissDialog($(this), "Rolling Cargo Check-in", "Input valid ticket number. Please try again.");
	}	
});

// Change the status of the voyage
$(".svm-update-status").on("click", function(){
	// Initialize
	var svmstatus;
	var statusmsg;
	var svm_id = $("#subvoyage_management_status_id").val();
	
	// Register/remember clicked voyaged status
	$("#subvoyage_update_status_id").val($(this).val());

	// Change the status of subvoyage management
	// Check if voyage/subvoyage management is empty
	if($("#voyage").val()){

			if(svm_id == 2 && $(this).val() == 9){
				statusmsg = "You need to undepart the voyage";
			}
			else if(svm_id == 1 && $(this).val() == 4){
				statusmsg = "You cannot undepart the voyage";
			}
			else if(svm_id == 2 && $(this).val() == 5 || svm_id == 2 && $(this).val() == 6){
				statusmsg = "You cannot block or cancel. Undepart the voyage.";
			}
			else if($(this).val() == 3 || $(this).val() == 6){
				cancelDialog($(this), "Rolling Cargo Check-in", $(this).val());
			}
			else {

				// Change the status of the subvoyage
				$("#subvoyage_management_status_id").val($(this).val());

				// Change backend status
				voyager('changeVoyageStatus');

				// Get the status message
				var mstat = statMsg($(this).val());

			   	// Change the voyage status
				$("#subvoyage_management_status").val(mstat.svmstatus);
			}

			// Don't call dismiss dialog for delayed and cancelled voyages
			if($(this).val() != 3)
				if($(this).val() != 6)
					modalDismissDialog($(this), "Rolling Cargo Check-in", mstat.statusmsg);
	}
	else{
		modalDismissDialog($(this), "Rolling Cargo Check-in", "Please select voyage schedule.");
	}

});

// Call clear voyage details
$("#voyage").on("change", function(){
	clearVoyageDetails();

	// Get the checked in RORO
	voyager('checkedInROROList');
});

// Function for returning status messages
function statMsg(valData){
	// Show the modal
	switch (valData){
	    case '9':
	        // Open for check-in
	        svmstatus = "Open for Check-in";
	        statusmsg = 'Voyage is now open for check-in.';
	        break;
	    case '2':
	        // Departed on time
	        svmstatus = "Departed on time";
	        statusmsg = 'Voyage departed on time.';
	        break;
	    case '3':
	        // Departed but delayed
	        svmstatus = "Departed but delayed";
	        statusmsg = 'Voyage departed but delayed.';
	        break;
	    case '4':
	        // Undepart
	        svmstatus = "Undeparted";
	        statusmsg = 'Voyage has been undeparted.';
	        break;
	    case '6':
	        // Canceled
	        svmstatus = "Cancelled";
	        statusmsg = 'Voyage has been cancelled.';
	        break;
	}

	// Initialize object
	var obj = {svmstatus:svmstatus, statusmsg:statusmsg, statuscode:valData}
	// Return data
	return obj;
}


// Dismiss Modal
function modalDismissDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'Dismiss',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}]
	});
        
    return false;
}

// Dismiss Modal
function cancelDialog(obj, title, valData)
{            
	// Get anchor	
    var link = $(obj).attr('href');
	var message = returnModalMessage(valData);
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: message, // modal body
		buttons: [{
                label: 'Cancel',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                   dialog.close(); // close modal
                }
            }, {
                label: 'Save',
                cssClass: 'btn oj-button btnChangeStatus',
                action: function(dialog) {
					voyager('changeVoyageStatus');
					dialog.close();
                }
            }]
	});
        
    return false;
}

// Return the message in Modal
function returnModalMessage(cstatus){
	// Initialize
	var message = "";
	if(cstatus == 3){
		message +=  "<label class=\"control-label\">No. of minutes late.</label>";
        message += "<div class=\"btn-group bootstrap-select form-control\">";
        message += "<input class=\"form-control\" id=\"delayed_time\" type=\"text\" placeholder=\"Input number of minutes delayed\" onkeypress=\"numeric_validation(event)\" required>";
        message += "</div>";
        message += "<span class=\"input-notes-bottom\" id=\"modal-error\"></span>";
	}
	else if(cstatus == 6) {
		// Initialize
		message +=  "<label class=\"control-label\">Reason for cancelling.</label>";
		message += "<div class=\"btn-group bootstrap-select form-control\">";
		message += "<input class=\"form-control\" id=\"reason-for-cancelling\" type=\"text\" placeholder=\"Input reason for cancelling\" required>";
		message += "</div>";
		message += "<span class=\"input-notes-bottom\" id=\"modal-error\"></span>";
	}

	return message;
}


// Clear voyage trip details
function clearVoyageDetails(){
	if($("#voyage").val())
		voyager('dtvoyage');
	else
		$(".trip_details").val('');
}

// Check if origin and destination are the same
function checkPort(){
	var origin = $("#origin").val();
	var destination = $("#destination").val();
	if(origin == destination && (origin != 0 && destination != 0)){
		$("#destination").prop('selectedIndex',0).selectpicker('refresh');
		modalDismissDialog($(this), "Rolling Cargo Check-in", "Origin and Destination port should not be the same");
	}
}

// Main function
function voyager(module){

	var svm = false;
	var ticket = false;	
	var valData = false;	
	var cstatus = false;	
	var roro_id = false;
	var origin = $("#origin").val();
	var voyage = $("#voyage").val();
	var destination = $("#destination").val();

	// Get the ticket number
	// Get the Booking and Subvoyage Management
	if(module == 'dtTicket'){
		var ticket = $("#ticket_number").val();
	}
	else if(module == 'checkin'){
		var svm = voyage;
		var roro_id = $("#booking_roro_id").val();
	}
	else if(module == 'changeVoyageStatus'){
		var svm = voyage;
		var cstatus = $("#subvoyage_update_status_id").val();
		if(cstatus == 3)
			var valData = $("#delayed_time").val();
		else if(cstatus == 6)
			var valData = $("#reason-for-cancelling").val();
	}

	$.ajax({
		type : 'post',
		async: true,
		url : adminURL+'rolling_cargo_checkin/ajax',
		data: 'module='+module+'&origin='+origin+'&destination='+destination+'&voyage='+voyage+"&ticket_number="+
		ticket+"&subvoyage_management_id="+svm+"&booking_roro_id="+roro_id+"&cstatus="+cstatus+"&valdata="+valData,
		success : function(data){

			if(module == 'voyage'){
				$("#voyage").html('');
				$("#voyage").html(data).selectpicker('refresh');
			}
			else if(module == 'dtvoyage'){
				var obj  = JSON.parse(data);
				$("#etd, #etd1").val(obj.ETD);	
				$("#eta, #eta1").val(obj.ETA);	
				$("#vessel, #vessel1").val(obj.vessel_code);	
				$("#voyage_code, #voyage_code1").val(obj.voyage);	
				$("#origin_port, #origin_port1").val(obj.port_origin);	
				$("#subvoyage_management_status").val(obj.description);
				$("#departure_date, #departure_date1").val(obj.departure_date);	
				$("#destination_port, #destination_port1").val(obj.port_destination);
				$("#subvoyage_management_status_id").val(obj.subvoyage_management_status_id);
			}
			else if(module == 'dtTicket'){
				var obj  = JSON.parse(data);
				if(!obj.success)
					$(".ticket:not(:first)").val();

				$("#roro_rate").val(obj.amount);
				$("#total_amount").val(obj.total_amount);
				$("#reference_number").val(obj.reference_no);
				$("#booking_roro_id").val(obj.id_booking_roro);
				$("#sender_first_name").val(obj.sender_first_name);
				$("#sender_middle_name").val(obj.sender_middle_name);
				$("#sender_last_name").val(obj.sender_last_name);
				$("#sender_contact_no").val(obj.sender_contact_no);
				$("#sender_birthdate").val(obj.sender_birthdate);
				$("#driver_first_name").val(obj.driver_first_name);
				$("#driver_middle_name").val(obj.driver_middle_name);
				$("#driver_last_name").val(obj.driver_last_name);
				$("#driver_contact_no").val(obj.driver_contact_no);
				$("#driver_birthdate").val(obj.driver_birthdate);
				$("#rolling_cargo_description").val(obj.rolling_cargo_description);
			}
			else if(module == 'checkin'){
				var obj  = JSON.parse(data);
				// Clear after checkin
				if(obj.success)
					$(".ticket").val('');
			}
			else if(module == 'changeVoyageStatus'){
				var obj = JSON.parse(data);
				var mstat = statMsg(obj.subvoyage.subvoyage_management_status_id);
				// Subvoyage Management Status
				$("#subvoyage_management_status").val(mstat.svmstatus);
				$("#subvoyage_management_status_id").val(mstat.statuscode);
			}
			else if(module == 'checkedInROROList'){
				$("#trip-details-table").html(data);
			}
		},
		error : function(data){},
		complete: function(){ requestSent = false; }
	});
}