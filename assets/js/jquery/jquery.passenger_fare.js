/*
*--------------------------------------------------------------------------
* Passenger Fare
*--------------------------------------------------------------------------
* Handles passenger fare frontend javascripts
* @author Ronald Meran
* @updates: April 1, 2016
*/

// Module Name
var module = "Passenger Fare";

// Modal dialog
$(document).on('click', '.activate', function(event){
	modalDialog($(this), module, "Are you sure you want to re-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.deactivate', function(event){
	modalDialog($(this), module, "Are you sure you want to de-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.remove', function(event){
	modalDialog($(this), module, "Are you sure you want to delete " + module.toLowerCase() + " ?"); event.preventDefault();
});

// Activate, Deactivate, Remove Modal
function modalDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'No',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}, 
		{
			label: 'Yes',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				// ajax post/jQuery post code
				window.location.replace(link);
			}
		}]
	});
        
    return false;
}

// Dismiss Modal
function modalDismissDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'Dismiss',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}]
	});
        
    return false;
}

$(function(){
	
	// Call async
	function async(module, valData){
	
		// Voyage
		var voyage = $("#voyage").val();
		var subvoyage = $("#subvoyage").val();
		var valData = valData;
		
		// Get the last subvoyage id
		if(module == 'addLegs' || module == 'dtAddLegs')
			var lastClass = $(".oj-leg-fare:last(:visible)").find('.subvoyage_ids').val();
		else if(module == 'dtRemoveLegs')
			var lastClass = splitMe(valData);
		else 
			var lastClass = false;

		// Validate lastClass
		if(!lastClass)
			lastClass = false;

		$.ajax({
			type : 'post',
			async: true,
			url : adminURL+'passenger_fare/ajax',
			data: 'module='+module+'&voyage_id='+voyage+'&crud=add&subvoyage_id='+subvoyage+'&last_subvoyage_id='+lastClass,
			success : function(data){
				
				if(module == 'voyage'){
					$("#subvoyage_details").html('');
					$("#subvoyage").html(data).selectpicker('refresh');													
					$("#subvoyage_return_details .oj-leg-fares").remove();
				}
				else if(module == 'subvoyage'){
					$("#subvoyage_details").html('');
					$("#subvoyage_details").html(data);
					$("#subvoyage_return_details .oj-leg-fares").remove();
				}
				else if(module == 'dtSubvoyage'){
					obj  = JSON.parse(data);
					$(".leg_count").val(obj.leg_count);
					$(".leg_order").val(obj.leg_order);
					
					if(valData){
						if(obj.key){
							$(".origin_id").val(obj.subvoyages[obj.key].origin_id);
							$(".destination_id").val(obj.subvoyages[obj.key].destination_id);
						}
					}					
				}
				else if(module == 'addLegs'){
					$("#subvoyage_details").append(data);
					legCounter();

					// Enable/Disable return fare
					disableReturnFare();
				}
				else if(module == 'dtAddLegs'){
					obj  = JSON.parse(data);
					$(".origin_id").val(obj.origin_id);
					if(obj.destination_id)
						$(".destination_id").val(obj.destination_id);	
				}
				else if(module == 'removeLegs'){
					valData.parents(".oj-leg-fare").remove();
					legCounter();

					// Enable/Disable return fare
					disableReturnFare();								
				}
				else if(module == 'dtRemoveLegs'){
					obj  = JSON.parse(data);
					if(obj.destination_id)
						$(".destination_id").val(obj.destination_id);							
				}
				else if(module == 'returnVoyages'){
					console.log(data)
					$("#subvoyage_return_details").append(data);
				}
			},
			error : function(data){},
			complete: function(){ requestSent = false; }
		});
	}
	 
	// On change of List of Voyages
	$("#voyage").on("change", function() {
		async('voyage', false);
	}); // success
	
	// On change of List of Legs under the voyage
	$("#subvoyage").on("change", function() {
		// Print and append the Subvoyage
		async('subvoyage', false);
		// Get only the data of the subvoyage
		async('dtSubvoyage', true);
		// Get only the add data details
		async('dtAddLegs', true);	
		legCounter();

		// Uncheck always the return fare every change of subvoyage
		$(".return_fare").prop('checked', false);
	}); // success
	
	// Get only the data of the subvoyage
	if($('.leg_count').is(":hidden"))
		async('dtSubvoyage', false);
	
	// Request if completed
	var requestSent = false;

	// Success
	$("#add-leg-btn").on("click", function() {
	
		if(!requestSent){
			requestSent = true;
			// Add Legs
			async('addLegs', false);
			// Get only the data of the subvoyage
			async('dtSubvoyage', false);
			// Get only the add data details
			async('dtAddLegs', true);
		}

		// Disable Remove buttons
		disableRemoveOnAdd();
			
	}); // success
	
	// Disable remove button on add
	function disableRemoveOnAdd(){
		// Count class
		var btnClass = $(".oj-leg__btn").length;
		var subCnt = $(".leg_count").val();
		var leg_order = parseInt($(".leg_order").val()) + btnClass;			
							
		if(leg_order <= subCnt)
			$(".oj-leg__btn").not('last').attr("disabled", true);
	}

	// Call on load
	disableReturnFare();

	// Disable return fare if leg count is more than 1
	function disableReturnFare(){
		var btnClass = $(".oj-leg__btn").length;
		if(btnClass > 1)
			$(".return_fare").prop('disabled', true).prop('checked', false);
		else
			$(".return_fare").prop('disabled', false);

		if(btnClass > 1)
			$("#subvoyage_return_details .oj-leg-fares").remove();
	}

	// Return fare actions
	$(".return_fare").on('click', function(){
		async('returnVoyages', false);
	});

	// Return unique id
	function splitMe(ids){
		if(ids){
			var id = ids.split("-");
			return id[1];
		}

		return false;
	}
	
	// Remove Legs
	$(document).on("click",".oj-leg__btn", function() {
		// Disable last class
		var _lastClass = $(".oj-leg-fare:last").prev().find('.oj-leg__btn').attr('id');
		var lastClass = $(".oj-leg-fare:last").prev().find('.oj-leg__btn').attr('disabled', false);

		// Remove Legs
		async('removeLegs', $(this));

		// Data binding in origin and destination
		if(_lastClass)
			async('dtRemoveLegs', _lastClass);
		
		// Trigger Subvoyage change
		if($(".oj-leg__btn").length == 1)
			$("#subvoyage").prop('selectedIndex',0).selectpicker('refresh');
		
	}); // success
	
	disableRemove();
	
	// Check subvoyages
	function disableRemove(){
		$(".oj-leg-fare:not(:last)").find('.oj-leg__btn').attr('disabled', true);
	}

	// Leg Counter
	var legCounter = function () {
	  $(".oj-leg").each(function (index) {
		$(this).find(".oj-leg__title").html("Leg No. " + (index + 1))
	  })
	}
	
	// Check leg counter
	legCounter();
	
	// Validate check box
	$(".chk_conditions").each(function(key, value){
		var is_check = $(this).is(":checked");
		
		if(is_check){
			switch(key){
				case 0:
						$("#age_bracket_from").attr('disabled', false).removeClass('disabled'); 
						$("#age_bracket_to").attr('disabled', false).removeClass('disabled');
					break;
				case 1:
						$("#outlet_id").attr('disabled', false).removeClass('disabled').selectpicker('refresh'); 					
					break;
				case 2:
						$("#min_advance_booking").attr('disabled', false).removeClass('disabled'); 					
						$("#max_advance_booking").attr('disabled', false).removeClass('disabled'); 					
					break;
				case 3:
						$("#booking_period_from").attr('disabled', false).removeClass('disabled'); 					
						$("#booking_period_to").attr('disabled', false).removeClass('disabled'); 					
					break;
				case 4:
						$("#travel_period_from").attr('disabled', false).removeClass('disabled'); 					
						$("#travel_period_to").attr('disabled', false).removeClass('disabled'); 					
					break;
				case 5:
						$("#travel_days").attr('disabled', false).removeClass('disabled').selectpicker('refresh'); 					
					break;
				case 6:
						$("#max_leg_interval").attr('disabled', false).removeClass('disabled'); 					
					break;
			}
		}
		else{

			switch(key){
				case 0:
						$("#age_bracket_from").attr('disabled', true).addClass('disabled'); 
						$("#age_bracket_to").attr('disabled', true).addClass('disabled');
					break;
				case 1:
						$("#outlet_id").attr('disabled', true).addClass('disabled').selectpicker('refresh'); 					
					break;
				case 2:
						$("#min_advance_booking").attr('disabled', true).addClass('disabled'); 					
						$("#max_advance_booking").attr('disabled', true).addClass('disabled'); 					
					break;
				case 3:
						$("#booking_period_from").attr('disabled', true).addClass('disabled'); 					
						$("#booking_period_to").attr('disabled', true).addClass('disabled'); 					
					break;
				case 4:
						$("#travel_period_from").attr('disabled', true).addClass('disabled'); 					
						$("#travel_period_to").attr('disabled', true).addClass('disabled'); 					
					break;
				case 5:
						$("#travel_days").attr('disabled', true).addClass('disabled').selectpicker('refresh'); 					
					break;
				case 6:
						$("#max_leg_interval").attr('disabled', true).addClass('disabled'); 					
					break;
			}
		}
			
	});
		
});

// Age Bracket validation
$(document).on("keyup", "#age_bracket_from", function(){
	var age_from = $(this).val();			
	$("#age_bracket_to").val(age_from);
});

$(document).on("change", "#age_bracket_to", function(){
	var module = "Age Bracket"; 
	var age_from = $("#age_bracket_from").val();			
	if($(this).val() < age_from){
		$(this).val('');
		modalDismissDialog($(this), module, "Age To should not be less than Age From"); event.preventDefault();
	}
});

// Minimum Advance Booking validation
$(document).on("keyup", "#min_advance_booking", function(){
	var min = $(this).val();			
	$("#max_advance_booking").val(min);
});


// Validation for conditions
$(document).on('submit', '.oj-form', function(event){
	
	// Module Name
	var module = "Passenger Fare";

	if(jQuery('.oj-form input[type=checkbox]:checked').length < 1){
		modalDismissDialog($(this), module, "Please select at least one condition");
		return false;
	}
});
		
$(document).on("change", "#max_advance_booking", function(){
	var module = "Advance Booking"; 
	var min = $("#min_advance_booking").val();			
	if($(this).val() < min){
		$(this).val('');
		modalDismissDialog($(this), module, "Maximum Advance Booking should not be less than Minimum Advance Booking"); event.preventDefault();
	}
});

// Booking Date Limit
$("#booking_period_from, #booking_period_to, #travel_period_from, #travel_period_to").datetimepicker({
	format: "YYYY-MM-DD",
});

$("#booking_period_from").on("dp.change", function(e) {
	$("#booking_period_to").data("DateTimePicker").minDate(e.date);
});

$("#travel_period_from").on("dp.change", function(e) {
	$("#travel_period_to").data("DateTimePicker").minDate(e.date);
});
