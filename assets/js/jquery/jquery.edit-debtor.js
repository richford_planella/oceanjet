$(function(){
    var selectVal='';
    var selectText='';
    var selectRel="";
    var arOutlet =[];
    var test = $("#test").val();
 // get existing values
  $('*[id*=test]').each(function(){
     arOutlet.push($(this).val());
  });

    $("#outlet_id").change(function(){
        
        selectVal = $(this).val();
        selectText = $("#outlet_id option:selected").text();
        selectRel = $("#outlet_id option:selected").attr("rel");

    });
    // Add Outlet
    $(document).on("click",".add-outlet",function(){
       
        if (selectVal !== "" ) {
           if ($.inArray(selectVal,arOutlet) !='-1') {
                $('body').addClass('margin-dialog');   
                 BootstrapDialog.show({
                     title: '<h3>Debtor Management</h3>', // modal title
                     message: '<h4>Outlet Already in use</h4>', // modal body
                     buttons: [{
                     label: 'Dismiss',
                     cssClass: 'btn oj-button',
                    action: function(dialog) {
                        dialog.close(); // close modal
                    }
                }]
               });
           } else {
                var index = $('#outlet_id').get(0).selectedIndex;
                $('li[data-original-index='+index+']:contains("'+selectText+'")').removeClass('selected').addClass('disabled');
                $("#outlet_id").each(function(){
                    var addtbl = $('.table #outlet_tbody');
                    addtbl.append("<tr><td>"+selectText+"<input id='test' type='hidden' name='outlet_id[]' value='"+selectVal+"'/></td><td>"+selectRel+"</td><td><a alt='"+index+"' href='' rel='"+selectVal+"' class='remove-outlet'>Delete</a></td></tr>");
                });
               arOutlet.push(selectVal); 
           }
           
        } else {
            $('body').addClass('margin-dialog'); 
              BootstrapDialog.show({
                     title: '<h3>Debtor Management</h3>', // modal title
                     message: '<h4>Please Select an Outlet</h4>', // modal body
                     buttons: [{
                     label: 'Dismiss',
                     cssClass: 'btn oj-button',
                    action: function(dialog) {
                        dialog.close(); // close modal
                    }
                }]
               });
    
           
        }
         return false;
    });
    // remove outlet
    $(document).on("click",".remove-outlet",function(event){
        
        var outletid = $(this).attr("rel");
    
        var parent_tr = $(this).closest("tr");
        
        if (parent_tr.parent("tbody").children("tr").length >= 1) {
           
            var index1 = $('#outlet_id').get(0).selectedIndex;
            var index = $(this).attr("alt");
            console.log(index1);
            // $('li[data-original-index='+index1+']:contains("'+selectText+'")').removeClass('disabled');
            $('li[data-original-index='+index+']').removeClass('disabled');
            arOutlet.splice($.inArray(outletid,arOutlet),1);
          
            parent_tr.remove();
         }

       return false;
      
    });   
    
    $(document).on("click","#update-debtor-outlet",function(){
        // $(this).closest('form').outerHTML();
        // return false;
    });
});