/*
|--------------------------------------------------------------------------
| Ticket Management JQuery/Ajax
|--------------------------------------------------------------------------
|
| Ticket Management
|
| @author Richford Planella
| @updates Ronald Meran March 23, 2016
*/

function requestPath() {
	var url = window.location.href;
	if (url.substr(url.lastIndexOf('/') + 1) == "issue_passenger_ticket")
		return url;
	else if (url.substr(url.lastIndexOf('/') + 1) == "issue_rolling_cargo_ticket")
		return url;
	else
		return url.substring(0,url.lastIndexOf("/"));
}

function appendRoundTripCheckbox(voyage_id) {
	var row = '<div class="col-xs-1"><label class="control-label">Round Trip</label><div class="checkbox-wrapper"><label class="checkbox-inline"><span><input type="checkbox" id="round-trip-checkbox" value="'+ voyage_id +'" /></span><span>Yes</span></label></div></div>';
	
	return row;
}

function appendRoundtrip() {
	var row = '<div class="col-xs-3">';
		row += '<label class="control-label">Origin</label>';
		row += '<select class="selectpicker form-control ticket-orig-dest" id="origin" name="origin" title="Choose">'
		row += '</select>';
		row += '</div>';
		
		row += '<div class="col-xs-3">';
		row += '<label class="control-label">Destination</label>';
		row += '<select class="selectpicker form-control ticket-orig-dest" id="destination" name="destination" title="Choose">';
		row += '</select>';
		row += '</div>';
		
		row += '<div class="col-xs-5">';
		row += '<label class="control-label">Return Voyage</label>';
		row += '<div class="select-holder">';
		row += '<select class="selectpicker form-control select-voyage" id="voyage" name="voyage" title="Choose">';
		row += '</select>';
		row += '</div>';
			// <span class="input-notes-bottom"><?=form_error('voyage')?></span>
		row += '</div>';
		
	return row;
}

function appendAmount(value) {
	var row = '<input type="text" id="voyage_amount" name="voyage_amount" class="form-control price" value="'+value+'" disabled />';
	
	return row;
}


$(document).ready(function() {
	// Function for async calls
	// @updates: Ronald Meran
	function async(valData){
		$.post(requestPath()+'/ajax', valData, function(callback) {
			console.log(callback)
			if(valData.module == 'getvoyage'){
				if(valData.main_voyage){
					$("#voyage").html('');
					$("#voyage").html(callback).selectpicker('refresh');	
				}
				else{
					$("#return_voyage").html('');
					$("#return_voyage").html(callback).selectpicker('refresh');	
				}
			}
			else if(valData.module == 'passenger_fare'){
				obj  = JSON.parse(callback);
				if(valData.mode == 'main_passenger_fare')
					$("#main_points_earning").val(obj.points_earning)
				else
					$("#return_points_earning").val(obj.points_earning)
				
				$.each(obj.subvoyage_fare, function(index, value){
					$("#subvoyage_fare").append("<li><span>Leg "+ (index+1) +":</span><span>"+value.amount+"</span></li>");
				});
			}
			else if(valData.module == 'count_subvoyages'){
				obj  = JSON.parse(callback);
				if(obj.subvoyage_count > 1)
					$("#round_trip").attr('disabled', true);
				else
					$("#round_trip").attr('disabled', false);
			}

		});
	}
	
	// Get the origin and destination
	// @updates: Ronald Meran
	function evalData(evData){
		var origin = $('#origin').val();
		var destination = $('#destination').val();
		var return_date = $('#return_date').val();
		var departure_date = $('#departure_date').val();
		var obj = {origin:origin, destination:destination, departure_date:departure_date, return_date:return_date}
		return obj;
	}

	// Get the voyages on change of departure date
	// @updates: Ronald Meran
	$(".voyage_date").on('dp.change', function(e) {

		// Disable past dates
		$(this).data("DateTimePicker").minDate(new Date());

		var validate_voyage = $(this).attr('id') == 'departure_date' ? true : false;
		var objData = evalData();
		var objOrigin = validate_voyage ? objData.origin : objData.destination;
		var objDestination = validate_voyage ? objData.destination : objData.origin;
		var valData = {	
						module:'getvoyage', 
						origin:objOrigin, 
						destination:objDestination, 
						departure_date:$(this).val(),
						main_voyage:validate_voyage
					  };
		// Post data
		if(origin == "" || destination == "")
			console.log('empty');
		else
			async(valData)

	});

	// Call onload
	// @updates: Ronald Meran
	is_check();
	disableRoundtrip();

	// Validate roundtrip checkbox if checked
	// @updates: Ronald Meran
	$('#round_trip').on('click', function(e) {
		is_check();
	});

	// Enable/Disable textboxes if round trip is checked
	// @updates: Ronald Meran
	function is_check(){
		// Evaluate checkbox if checked
		var checked = $("#round_trip").is(':checked') == true ? false : true;
		
		// Refresh select picker
		$("#return_date").prop('disabled', checked);
		$("#return_voyage").prop('disabled', checked);
		$("#return_voyage").selectpicker('refresh');
	}

	// Enable/Disable textboxes and check/uncheck roundtrip if voyage is multileg
	// @updates: Ronald Meran
	function is_check_disabled(){
		// Evaluate checkbox if checked
		var checked = $("#round_trip").attr('disabled') == true ? false : true;
		checked == true ? $("#round_trip").prop('checked', false) : $(this).prop('checked', true);
		// Refresh select picker
		$("#return_date").prop('disabled', checked);
		$("#return_voyage").prop('disabled', checked);
		$("#return_voyage").selectpicker('refresh');
	}

	// Clear the trip and ticket issuance details
	// @updates: Ronald Meran
	$("#origin, #destination").on('change', function(){
		$("#voyage, #return_voyage").html("<option value=''>Please Select</option>").selectpicker('refresh');
		$("#departure_date").val('').datetimepicker('update')
		$("#return_date").val('').datetimepicker('update')
		$("#ticket_issuance").css('display', 'none');
	});

	// Get passenger fare details
	// @updates: Ronald Meran
	$('.passenger_fare').on('change', function(){
		// Subvoyage fare
		$("#subvoyage_fare").html('');
		var objData = evalData();
		var valData = {	
						module:'passenger_fare',
						mode: $(this).attr('id'),
						origin: objData.origin,
						destination: objData.destination,
						passenger_fare_id: $(this).val()
					  };
		async(valData);
	});

	// Disable roundtrip
	// @updates: Ronald Meran
	function disableRoundtrip(){
		var objData = evalData();
		var valData = {	
						module:'count_subvoyages',
						origin: objData.origin,
						destination: objData.destination,
						subvoyage_management_id: $("#voyage").val()
					  };
		async(valData);
	}
	
	// On change of voyage
	// @updates: Ronald Meran
	$('#voyage').on('change', function(e) {
		is_check_disabled();
		disableRoundtrip();
	});

	$(document).on('change', '.voyage_fare_code', function(e) {
		var id_fare_code = $('.voyage_fare_code option:selected').val();
		
		$.post(requestPath()+'/get_total_fare', {id_fare_code:id_fare_code}, function(callback) {
			console.log(callback)
		});
	});
	
	$(document).on('blur', '#voyage_evoucher_code', function(e) {
		var evoucher_code = $(this).val();
		var total_amount = $(this).parent().next().children('#voyage_amount').val();
		var htmlInput = $(this).parent().next().children('#voyage_amount');
		var original_amount = $(this).parent().next().children('#voyage_amount').attr('data-original-value');
		
		$.post(requestPath()+'/get_evoucher', {evoucher_code:evoucher_code}, function(callback) {
			var obj = JSON.parse(callback);
			
			if(obj.length !== 0) {
				if(obj[0]['evoucher_type'] == 1) {
					var changed_amount = original_amount - obj[0]['amount'];
				} else {
					var changed_amount = original_amount - (original_amount * (obj[0]['amount'] / 100));
				}
				
				htmlInput.val(parseFloat(changed_amount).toFixed(2));
				console.log(htmlInput.val())
			} else if(obj.length === 0) {
				htmlInput.val(original_amount);
			} else {
				return false;
			}
		});
	});
	
	$(document).on('change', '#voyage_discount_code', function(e) {
		var discount_id = $(this).val();
		var total_amount = $(this).parent().next().next().children('#voyage_amount').val();
		var htmlInput = $(this).parent().parent().next().next().children('#voyage_amount');
		var original_amount = $(this).parent().parent().next().next().children('#voyage_amount').val();
		
		$.post(requestPath()+'/get_discount', {discount_id:discount_id}, function(callback) {
			var obj = JSON.parse(callback);
			
			if(obj.length !== 0) {
				if(obj[0]['discount_type'] == 'amount') {
					var changed_amount = original_amount - obj[0]['discount_amount'];
				} else {
					var changed_amount = original_amount - (original_amount * (obj[0]['discount_amount'] / 100));
				}
				
				htmlInput.val(parseFloat(changed_amount).toFixed(2));
			} else if(obj.length === 0) {
				htmlInput.val(original_amount);
			} else {
				return false;
			}
		});
	});

});
