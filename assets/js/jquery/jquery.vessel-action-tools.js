/*
|--------------------------------------------------------------------------
| jQuery Tools for vessel action
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                jQuery(document).on('click', '#fileUpload', checkExtension);
                jQuery(document).on('click', '#clear_seats', clearSeatPlan);
                
        }
);

function clearSeatPlan()
{
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>Vessel</h3>', // modal title
			message: '<h4>Are you sure you want to remove seat plan?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        jQuery("#seat_plan_container").html('<div class="oj-box2-header"></div><div class="oj-box2-content"><table class="oj-vessel" cellpadding="0" cellspacing="0"><tbody><tr><td colspan="3" class="col-xs-12 table-action-custom"><span class="not-available">No File Upload</span></td></tr></tbody></table></div>');
                                        jQuery("#total_row").val('0');
                                        jQuery("#total_column").val('0');
                                        jQuery("#total_seats").val('');
                                        jQuery("#hidden_total_seats").val('0');
                                        dialog.close(); // close modal
				}
			}]
		});
    
    return false;
}

function checkExtension()
{
    var ext = $('#fileInput').val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['csv']) == -1) {
        $('body').addClass('margin-dialog');
        BootstrapDialog.show({
			title: '<h3>Vessel</h3>', // modal title
			message: '<h4>Invalid file format. Please select CSV file.</h4>', // modal body
			buttons: [{
				label: 'Dismiss',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}]
		});
        return false;
    }
}