/*
 |--------------------------------------------------------------------------
 | jQuery Tools for evoucher
 |--------------------------------------------------------------------------
 */

jQuery(function ()
{
    jQuery(document).on('click', '#is_return', displayReturn);
    jQuery(document).on('change', '#origin_id', displayDeparture);
    jQuery(document).on('change', '#destination_id', displayDeparture);
    jQuery(document).on('change', '#ret_departure_date', displayRetVoyage);
    jQuery(document).on('change', '#departure_date', displayVoyage);
    jQuery(document).on('change', '#accommodation_id', displayFare);
    jQuery(document).on('change', '#fare_select', displayFareList);
    jQuery(document).on('change', '#return_fare_select', displayRetFareList);
    jQuery(document).on('blur', '#pax', checkOldValue);

    jQuery(".return-container").hide();
    jQuery(".round-trip-div").hide();

    jQuery("#expiration_date").datetimepicker({format: "YYYY-MM-DD"});
    jQuery("#dep_date").datetimepicker({format: "YYYY-MM-DD"});

    jQuery("#expiration_date").on("dp.change", function(){jQuery("#expiration_date").data("DateTimePicker").minDate(new Date());});
 
    // check voyage count
    if (voyage_count == 1){jQuery(".round-trip-div").show();}else{jQuery(".round-trip-div").hide();}
    // check if select return
    if (is_return == '1') {jQuery(".return-container").show();}else{jQuery(".return-container").hide();}

    // remove all disabled attribute before submit
    jQuery('#myForm').submit(function(){jQuery("#myForm :disabled").removeAttr('disabled');});
    
    oldPax = '';
//    
    if(jQuery('#pax').val() != '')
    {
        oldPax = jQuery('#pax').val();
    }
    
}
);

function checkOldValue()
{
    var new_value = $(this).val();
//    alert('haller');
//    if(oldPax == '')
//    {
//        oldPax = new_value;
//    }
//    else
//    {
//        if(new_value < oldPax)
//        {
//            $(this).val(oldPax);
//        }
//    }
    
    computeTotal();
}

function displayRetFareList()
{
    var passenger_fare_id = jQuery('#return_fare_select').val();
    
    if(passenger_fare_id)
    {
        jQuery.ajax({
            type: 'POST',
            url: adminURL + 'reservation/displayFareList',
            data: {'passenger_fare_id' : passenger_fare_id,
                'csrf_token': securedHash},
            success: function(data)
            {
                jQuery('.return-fare-details-div').html(data);
                computeTotal();
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function()
            {
                jQuery('.return-fare-details-div').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
            }
        });
    }
    else
    {
        jQuery('.return-fare-details-div').html('');
        computeTotal();
    }
}

function displayFareList()
{
    var passenger_fare_id = jQuery('#fare_select').val();
    
    if(passenger_fare_id)
    {
        jQuery.ajax({
            type: 'POST',
            url: adminURL + 'reservation/displayFareList',
            data: {'passenger_fare_id' : passenger_fare_id,
                'csrf_token': securedHash},
            success: function(data)
            {
                jQuery('.fare-details-div').html(data);
                computeTotal();
            },
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert(xhr.status);
                alert(thrownError);
            },
            beforeSend: function()
            {
                jQuery('.fare-details-div').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
            }
        });
    }
    else
    {
        jQuery('.fare-details-div').html('');
        computeTotal();
    }
}

function displayFare()
{
    var accommodation_id = jQuery("#accommodation_id").val();
    var voyage_management_id = jQuery("#hidden_departure_date").val();
    var voyage_id = jQuery("#voyage_id").val();
    var origin_id = jQuery("#hidden_origin_id").val();
    var destination_id = jQuery("#hidden_destination_id").val();
    
    if (accommodation_id)
    {
        if (origin_id)
        {
            if (destination_id)
            {
                if (voyage_management_id)
                {
                    // display voyage details
                    $.ajax({
                        type: 'POST',
                        url: adminURL + 'reservation/displayFare',
                        data: {'origin_id': origin_id,
                            'destination_id': destination_id,
                            'voyage_id': voyage_id,
                            'accommodation_id': accommodation_id,
                            'voyage_management_id': voyage_management_id,
                            'reservation_type': 'departure',
                            'origin_subvoyage_id': jQuery("select#departure_date").find('option:selected').attr('origin_subvoyage_id'),
                            'destination_subvoyage_id': jQuery("select#departure_date").find('option:selected').attr('destination_subvoyage_id'),
                            'csrf_token': securedHash},
                        success: function (data)
                        {
                            jQuery('.fare-select-div').html(data);
                            jQuery('#fare_select').selectpicker('refresh');
                            jQuery('.fare-details-div').html('');
                        },
                        error: function (xhr, ajaxOptions, thrownError)
                        {
                            alert(xhr.status);
                            alert(thrownError);
                        },
                        beforeSend: function ()
                        {
                            jQuery('.fare-select-div').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
                        }
                    });
                }
                else
                {
                    jQuery(".fare-select-div").html('<div class="col-xs-3"><label class="control-label">Fare Details</label><select class="selectpicker form-control" id="fare_select" name="fare_select"><option value="">Please select</option></select></div>');
                    jQuery('#fare_select').selectpicker('refresh');
                    jQuery('.fare-details-div').html('');
                }
            }
            else
            {
                jQuery(".fare-select-div").html('<div class="col-xs-3"><label class="control-label">Fare Details</label><select class="selectpicker form-control" id="fare_select" name="fare_select"><option value="">Please select</option></select></div>');
                jQuery('#fare_select').selectpicker('refresh');
                jQuery('.fare-details-div').html('');
            }
        }
        else
        {
            jQuery(".fare-select-div").html('<div class="col-xs-3"><label class="control-label">Fare Details</label><select class="selectpicker form-control" id="fare_select" name="fare_select"><option value="">Please select</option></select></div>');
            jQuery('#fare_select').selectpicker('refresh');
            jQuery('.fare-details-div').html('');
        }
    }
    else
    {
        jQuery(".fare-select-div").html('<div class="col-xs-3"><label class="control-label">Fare Details</label><select class="selectpicker form-control" id="fare_select" name="fare_select"><option value="">Please select</option></select></div>');
        jQuery('#fare_select').selectpicker('refresh');
        jQuery('.fare-details-div').html('');
    }
    
    if (jQuery("#is_return").is(':checked'))
    {
        displayRetFare();
    }    
    
    computeTotal();
}

function displayRetFare()
{
    var accommodation_id = jQuery("#accommodation_id").val();
    var voyage_management_id = jQuery("#hidden_ret_departure_date").val();
    var voyage_id = jQuery("#voyage_id").val();
    var origin_id = jQuery("#hidden_ret_origin_id").val();
    var destination_id = jQuery("#hidden_ret_destination_id").val();
    
    if (accommodation_id)
    {
        if (origin_id)
        {
            if (destination_id)
            {
                if (voyage_management_id)
                {
                    // display voyage details
                    $.ajax({
                        type: 'POST',
                        url: adminURL + 'reservation/displayFare',
                        data: {'origin_id': origin_id,
                            'destination_id': destination_id,
                            'voyage_id': voyage_id,
                            'accommodation_id': accommodation_id,
                            'voyage_management_id': voyage_management_id,
                            'reservation_type': 'return',
                            'origin_subvoyage_id': jQuery("select#ret_departure_date").find('option:selected').attr('origin_subvoyage_id'),
                            'destination_subvoyage_id': jQuery("select#ret_departure_date").find('option:selected').attr('destination_subvoyage_id'),
                            'csrf_token': securedHash},
                        success: function (data)
                        {
                            jQuery('.return-fare-select-div').html(data);
                            jQuery('#return_fare_select').selectpicker('refresh');
                            jQuery('.return-fare-details-div').html('');
                        },
                        error: function (xhr, ajaxOptions, thrownError)
                        {
                            alert(xhr.status);
                            alert(thrownError);
                        },
                        beforeSend: function ()
                        {
                            jQuery('.return-fare-select-div').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
                        }
                    });
                }
                else
                {
                    jQuery(".return-fare-select-div").html('<div class="col-xs-3"><label class="control-label">Fare Details</label><select class="selectpicker form-control" id="return_fare_select" name="return_fare_select"><option value="">Please select</option></select></div>');
                    jQuery('#return_fare_select').selectpicker('refresh');
                    jQuery('.return-fare-details-div').html('');
                }
            }
            else
            {
                jQuery(".return-fare-select-div").html('<div class="col-xs-3"><label class="control-label">Fare Details</label><select class="selectpicker form-control" id="return_fare_select" name="return_fare_select"><option value="">Please select</option></select></div>');
                jQuery('#return_fare_select').selectpicker('refresh');
                jQuery('.return-fare-details-div').html('');
            }
        }
        else
        {
            jQuery(".return-fare-select-div").html('<div class="col-xs-3"><label class="control-label">Fare Details</label><select class="selectpicker form-control" id="return_fare_select" name="return_fare_select"><option value="">Please select</option></select></div>');
            jQuery('#return_fare_select').selectpicker('refresh');
            jQuery('.return-fare-details-div').html('');
        }
    }
    else
    {
        jQuery(".return-fare-select-div").html('<div class="col-xs-3"><label class="control-label">Fare Details</label><select class="selectpicker form-control" id="return_fare_select" name="return_fare_select"><option value="">Please select</option></select></div>');
        jQuery('#return_fare_select').selectpicker('refresh');
        jQuery('.return-fare-details-div').html('');
    }
    
    computeTotal();
}

function displayRetVoyage()
{
    var voyage_id = jQuery(this).val();
    var origin_id = jQuery("#hidden_ret_origin_id").val();
    var destination_id = jQuery("#hidden_ret_destination_id").val();

    // display voyage details
    $.ajax({
        type: 'POST',
        url: adminURL + 'reservation/displayVoyage',
        data: {'origin_id': origin_id,
            'destination_id': destination_id,
            'voyage_id': voyage_id,
            'reservation_type': 'return',
            'origin_subvoyage_id': jQuery("select#ret_departure_date").find('option:selected').attr('origin_subvoyage_id'),
            'destination_subvoyage_id': jQuery("select#ret_departure_date").find('option:selected').attr('destination_subvoyage_id'),
            'csrf_token': securedHash},
        success: function (data)
        {
            jQuery('.ret-trip-details-div').html(data);
        },
        error: function (xhr, ajaxOptions, thrownError)
        {
            alert(xhr.status);
            alert(thrownError);
        },
        beforeSend: function ()
        {
            jQuery('.ret-trip-details-div').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
        }
    });

    // copy voyage id
    jQuery("#hidden_ret_departure_date").val(jQuery(this).val());
    jQuery("#hidden_ret_origin_subvoyage_id").val(jQuery("select#ret_departure_date").find('option:selected').attr('origin_subvoyage_id'));
    jQuery("#hidden_ret_destination_subvoyage_id").val(jQuery("select#ret_departure_date").find('option:selected').attr('destination_subvoyage_id'));

    returnStats();
}

function displayVoyage()
{
    var voyage_id = jQuery(this).val();
    var origin_id = jQuery("#origin_id").val();
    var destination_id = jQuery("#destination_id").val();

    // display voyage details
    $.ajax({
        type: 'POST',
        url: adminURL + 'reservation/displayVoyage',
        data: {'origin_id': origin_id,
            'destination_id': destination_id,
            'voyage_id': voyage_id,
            'reservation_type': 'departure',
            'origin_subvoyage_id': jQuery("select#departure_date").find('option:selected').attr('origin_subvoyage_id'),
            'destination_subvoyage_id': jQuery("select#departure_date").find('option:selected').attr('destination_subvoyage_id'),
            'csrf_token': securedHash},
        success: function (data)
        {
            jQuery('.trip-details-div').html(data);
            var count = jQuery("#voyage_count").val();
            if (count == '1')
            {
                jQuery(".round-trip-div").show();
            }
            else
            {
                jQuery(".round-trip-div").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError)
        {
            alert(xhr.status);
            alert(thrownError);
        },
        beforeSend: function ()
        {
            $('.trip-details-div').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
        }
    });

    // copy voyage id
    jQuery("#hidden_departure_date").val(jQuery(this).val());
    jQuery("#hidden_origin_subvoyage_id").val(jQuery("select#departure_date").find('option:selected').attr('origin_subvoyage_id'));
    jQuery("#hidden_destination_subvoyage_id").val(jQuery("select#departure_date").find('option:selected').attr('destination_subvoyage_id'));
    jQuery("#terminal_fee").val(jQuery("select#departure_date").find('option:selected').attr('terminal_fee'));
    jQuery("#port_charge").val(jQuery("select#departure_date").find('option:selected').attr('port_charge'));
    
    // call function to display trip details
    departureStats();
}

function displayReturn()
{
    var origin_id = jQuery("#origin_id").val();
    var destination_id = jQuery("#destination_id").val();
    var departure_date = jQuery("#departure_date").val();

    if (jQuery("#is_return").is(':checked'))
    {
        if (origin_id == '')
        {
            jQuery('body').addClass('margin-dialog');
            BootstrapDialog.show({
                title: '<h3>Reservation</h3>', // modal title
                message: '<h4>Please select origin.</h4>', // modal body
                buttons: [{
                        label: 'Dismiss',
                        cssClass: 'btn oj-button',
                        action: function (dialog) {
                            dialog.close(); // close modal
                        }
                    }]
            });
            jQuery(this).prop('checked', false);
            jQuery('#origin_id').prop('disabled', false);
            jQuery('#destination_id').prop('disabled', false);
            jQuery('#departure_date').prop('disabled', false);
            jQuery(".return-details").html('');
            jQuery(".return-fare-select-div").html('');
            jQuery(".return-fare-details-div").html('');
        }
        else if (destination_id == '')
        {
            jQuery('body').addClass('margin-dialog');
            BootstrapDialog.show({
                title: '<h3>Reservation</h3>', // modal title
                message: '<h4>Please select destination</h4>', // modal body
                buttons: [{
                        label: 'Dismiss',
                        cssClass: 'btn oj-button',
                        action: function (dialog) {
                            dialog.close(); // close modal
                        }
                    }]
            });
            jQuery(this).prop('checked', false);
            jQuery('#origin_id').prop('disabled', false);
            jQuery('#destination_id').prop('disabled', false);
            jQuery('#departure_date').prop('disabled', false);
            jQuery(".return-details").html('');
            jQuery(".return-fare-select-div").html('');
            jQuery(".return-fare-details-div").html('');
        }
        else if (departure_date == '')
        {
            jQuery('body').addClass('margin-dialog');
            BootstrapDialog.show({
                title: '<h3>Reservation</h3>', // modal title
                message: '<h4>Please select departure date</h4>', // modal body
                buttons: [{
                        label: 'Dismiss',
                        cssClass: 'btn oj-button',
                        action: function (dialog) {
                            dialog.close(); // close modal
                        }
                    }]
            });
            jQuery(this).prop('checked', false);
            jQuery('#origin_id').prop('disabled', false);
            jQuery('#destination_id').prop('disabled', false);
            jQuery('#departure_date').prop('disabled', false);
            jQuery(".return-details").html('');
            jQuery(".return-fare-select-div").html('');
            jQuery(".return-fare-details-div").html('');
        }
        else
        {
            jQuery(".return-container").show();
            jQuery('#origin_id').prop('disabled', true).selectpicker('refresh');
            jQuery('#destination_id').prop('disabled', true).selectpicker('refresh');
            jQuery('#departure_date').prop('disabled', true).selectpicker('refresh');
            jQuery(".return-details").html('');
            jQuery(".return-fare-details-div").html('');

            // display return details
            $.ajax({
                type: 'POST',
                url: adminURL + 'reservation/displayReturn',
                data: {'origin_id': origin_id,
                    'destination_id': destination_id,
                    'csrf_token': securedHash},
                success: function (data)
                {
                    jQuery('.return-container').html(data);
                    jQuery('#ret_origin_id').prop('disabled', true).selectpicker('refresh');
                    jQuery('#ret_destination_id').prop('disabled', true).selectpicker('refresh');
                    jQuery('#ret_departure_date').selectpicker('refresh');
                    jQuery(".return-details").html('<div class="col-xs-12"><div class="form-box form-box--blank no-content"><span class="no-selected">No Input Data</span></div></div>');
                    jQuery(".return-fare-select-div").html('<div class="col-xs-3"><label class="control-label">Return Fare Details</label><select class="selectpicker form-control" id="return_fare_select" name="return_fare_select"><option value="">Please select</option></select></div>');
                    jQuery('#return_fare_select').selectpicker('refresh');
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    alert(xhr.status);
                    alert(thrownError);
                },
                beforeSend: function ()
                {
                    jQuery('.return-container').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
                }
            });
        }
    }
    else
    {
        jQuery(".return-container").hide();
        jQuery('#origin_id').prop('disabled', false).selectpicker('refresh');
        jQuery('#destination_id').prop('disabled', false).selectpicker('refresh');
        jQuery('#departure_date').prop('disabled', false).selectpicker('refresh');
        jQuery(".return-details").html('');
        jQuery(".return-fare-select-div").html('');
        jQuery(".return-fare-details-div").html('');
    }    
}

function displayDeparture()
{
    var origin_id = jQuery("#origin_id").val();
    var destination_id = jQuery("#destination_id").val();

    jQuery("#hidden_origin_id").val(origin_id);
    jQuery("#hidden_destination_id").val(destination_id);

    if (origin_id)
    {
        if (destination_id)
        {
            $.ajax({
                type: 'POST',
                url: adminURL + 'reservation/displayDeparture',
                data: {'origin_id': origin_id,
                    'destination_id': destination_id,
                    'csrf_token': securedHash},
                success: function (data)
                {
                    jQuery('.dept_date').html(data);
                    jQuery('#departure_date').selectpicker('refresh');
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    alert(xhr.status);
                    alert(thrownError);
                },
                beforeSend: function ()
                {
                    jQuery('.dept_date').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
                }
            });
        }
        else
        {
            jQuery("#departure_date").html('<option value="0" origin_subvoyage_id="0" destination_subvoyage_id="0" terminal_fee="0.00" port_charge="0.00">Please select</option>');
            jQuery('#departure_date').selectpicker('refresh');
        }
    }
    else
    {
        jQuery("#departure_date").html('<option value="0" origin_subvoyage_id="0" destination_subvoyage_id="0" terminal_fee="0.00" port_charge="0.00">Please select</option>');
        jQuery('#departure_date').selectpicker('refresh');
    }

    jQuery(".round-trip-div").hide();
    jQuery(".trip-details-div").html('<div class="col-xs-12"><div class="form-box form-box--blank no-content"><span class="no-selected">No Input Data</span><input type="hidden" name="voyage_count" id="voyage_count" value="0"></div></div>');
    jQuery(".departure-details").html('<div class="col-xs-12"><div class="form-box form-box--blank no-content"><span class="no-selected">No Input Data</span></div></div>');
    jQuery(".return-details").html('');
    jQuery("#terminal_fee").val('0.00');
    jQuery("#port_charge").val('0.00');
    
    displayFare();
}

// ------------------------------------------

/*
 *  Function to display departure trip statistics
 */
function departureStats()
{
    var voyage_id = jQuery("#departure_date").val();
    var origin_id = jQuery("#origin_id").val();
    var destination_id = jQuery("#destination_id").val();

    // display voyage details
    $.ajax({
        type: 'POST',
        url: adminURL + 'reservation/displayTripStats',
        data: {'origin_id': origin_id,
            'destination_id': destination_id,
            'voyage_id': voyage_id,
            'reservation_type': 'departure',
            'origin_subvoyage_id': jQuery("select#departure_date").find('option:selected').attr('origin_subvoyage_id'),
            'destination_subvoyage_id': jQuery("select#departure_date").find('option:selected').attr('destination_subvoyage_id'),
            'csrf_token': securedHash},
        success: function (data)
        {
            jQuery('.departure-details').html(data);
        },
        error: function (xhr, ajaxOptions, thrownError)
        {
            alert(xhr.status);
            alert(thrownError);
        },
        beforeSend: function ()
        {
            jQuery('.departure-details').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
        }
    });
    
    displayFare();
}

// ------------------------------------------

/*
 *  Function to display return trip statistics
 */
function returnStats()
{
    var voyage_id = $("#ret_departure_date").val();
    var origin_id = jQuery("#ret_origin_id").val();
    var destination_id = jQuery("#ret_destination_id").val();

    // display voyage details
    $.ajax({
        type: 'POST',
        url: adminURL + 'reservation/displayTripStats',
        data: {'origin_id': origin_id,
            'destination_id': destination_id,
            'voyage_id': voyage_id,
            'reservation_type': 'return',
            'origin_subvoyage_id': jQuery("select#ret_departure_date").find('option:selected').attr('origin_subvoyage_id'),
            'destination_subvoyage_id': jQuery("select#ret_departure_date").find('option:selected').attr('destination_subvoyage_id'),
            'csrf_token': securedHash},
        success: function (data)
        {
            jQuery('.return-details').html(data);
        },
        error: function (xhr, ajaxOptions, thrownError)
        {
            alert(xhr.status);
            alert(thrownError);
        },
        beforeSend: function ()
        {
            jQuery('.return-details').html('<center><img src="' + baseURL + 'assets/img/ajax-loader.gif"></center>');
        }
    });
    
    displayRetFare();
}

function computeTotal()
{
    var total_fare = 0.00;
    
    var pax = $("#pax").val();
    var terminal_fee = jQuery('#terminal_fee').val();
    var port_charge = jQuery('#port_charge').val();
    var total_discount = jQuery('#total_discount').val();
    
    
    jQuery(".control_sum").each(function(){
        total_fare = parseFloat(total_fare) + parseFloat(jQuery(this).val());
    });
    
    jQuery("#total_fare").val(parseFloat(total_fare).toFixed(2));
    
    if(pax == '')
    {
        jQuery("#group_total").val('0.00');
    }
    else
    {
        total_fare = parseFloat(total_fare) + parseFloat(terminal_fee);
        total_fare = parseFloat(total_fare) + parseFloat(port_charge);
        
        var grand_total = parseFloat(pax * total_fare).toFixed(2);
        jQuery('#group_total').val(grand_total);
    }
    
    var group_total = jQuery("#group_total").val();
    
    jQuery("#grand_total").val(parseFloat(parseFloat(group_total) - parseFloat(total_discount)).toFixed(2));
    jQuery("#balance").val(parseFloat(parseFloat(group_total) - parseFloat(total_discount)).toFixed(2));
}