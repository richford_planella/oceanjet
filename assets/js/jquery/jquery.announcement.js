tinymce.init({
  selector: "textarea",
  
  // ===========================================
  // INCLUDE THE PLUGIN
  // ===========================================
	
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste jbimages"
  ],
	
  // ===========================================
  // PUT PLUGIN'S BUTTON on the toolbar
  // ===========================================
	
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages",
	
  // ===========================================
  // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
  // ===========================================
	
  relative_urls: false
	
});

// Validation for conditions
$(document).on('submit', '.oj-form', function(event){

	// Module Name
	var module = "Announcement";
	var checked = jQuery('.oj-form input[type=checkbox]:checked').length;
	var total_checkbox = (jQuery('.oj-form input[type=checkbox]').length) - 1;

	if(checked < 1){
		modalDismissDialog($(this), module, "Please select at least one user role");
		return false;
	}
	
	
});

// Call checkall
checkall();

// Check and uncheck checkbox
$(".chkrole").on("click", function(){
	// Call checkall
	checkall();
});

// Initialize
function checkall(){

	var checked = $('.chkrole:checked').length;
	var total_checkbox = $('.chkrole').length;
		
	if(checked == total_checkbox)
		$("#checkAll").prop("checked", true)
	else
		$("#checkAll").prop("checked", false)
}
		
// Module Name
var module = "Announcement";

// Modal dialog
$(document).on('click', '.activate', function(event){
	modalDialog($(this), module, "Are you sure you want to re-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.deactivate', function(event){
	modalDialog($(this), module, "Are you sure you want to de-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.remove', function(event){
	modalDialog($(this), module, "Are you sure you want to delete " + module.toLowerCase() + " ?"); event.preventDefault();
});

// Activate, Deactivate, Remove Modal
function modalDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');

	// Add Body
	$('body').addClass('margin-dialog');   
	
	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'No',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}, 
		{
			label: 'Yes',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				// ajax post/jQuery post code
				window.location.replace(link);
			}
		}]
	});
        
    return false;
}

// Activate, Deactivate, Remove Modal
function modalDismissDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'Dismiss',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}]
	});
        
    return false;
}
