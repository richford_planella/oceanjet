$(function(){

// Check discount type
discount_type();

$(document).on("change", function(){
	discount_type();
});

// Change label
function discount_type()
{
	var discount_type = $("#discount_type").val();

	if(discount_type == 'percentage'){
		$("#lbldiscount").text('Percentage (%)');
		$("#lbldiscount_amount").text("");
		$("#discount_amount").attr("max", "100");
	} else {
		$("#lbldiscount").text('Amount (P)');
		$("#lbldiscount_percentage").text("");
		$("#discount_amount").removeAttr("max");
	}
}


// Age Bracket actions
$(document).on("click", "#age-bracket", function(){
	showAgeClass(); ischeck();
});

// Enable or disable
ischeck();

// Show and hide
showAgeClass();

// Show class
showClass()

// Get checkbox status
function checkbox(){
	var check = $("#age-bracket").is(":checked");
	return check;
}

function ischeck(){
	var check = checkbox();
	 if(check)
		$(".age").prop('disabled', false);
	else 
		$(".age").prop('disabled', true);
}

function showAgeClass(){
	var check = checkbox();
	if(check)
		$('.age-range-wrap').css({ display : "block" });
	else	
		$('.age-range-wrap').css({ display : "none" });
}

function showClass(){
	var check = checkbox();
	if(check)
		$('.age-range-wrap').addClass('show');
	else	
		$('.age-range-wrap').removeClass('show');
}

// Check data
ageBracket();

	$(document).on("keyup", "#age-from", function(){
		var age_from = $(this).val();			
		$("#age-to").val(age_from);
	});

	$(document).on("change", "#age-to", function(){
		ageBracket();
	});
	
	$(document).on("change", "#age-from", function(){
		ageBracket();
	});
	
	function ageBracket()
	{
		var age_from = $("#age-from").val();
		var age_to = $("#age-to").val();

		if(parseInt(age_to) < parseInt(age_from)){
			$("#age-from").val(''); $("#age-to").val('');
			// Module Name
			var module = "Discounts";
			modalDismissDialog($(this), module, "Age To Should not be less than Age From");
		}
	}
	
});

// Module Name
var module = "Discount";

// Modal dialog
$(document).on('click', '.activate', function(event){
	modalDialog($(this), module, "Are you sure you want to re-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.deactivate', function(event){
	modalDialog($(this), module, "Are you sure you want to de-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.remove', function(event){
	modalDialog($(this), module, "Are you sure you want to delete " + module.toLowerCase() + " ?"); event.preventDefault();
});

// Activate, Deactivate, Remove Modal
function modalDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');

	// Add Body
	$('body').addClass('margin-dialog');   
	
	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'No',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}, 
		{
			label: 'Yes',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				// ajax post/jQuery post code
				window.location.replace(link);
			}
		}]
	});
        
    return false;
}

// Activate, Deactivate, Remove Modal
function modalDismissDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'Dismiss',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}]
	});
        
    return false;
}
