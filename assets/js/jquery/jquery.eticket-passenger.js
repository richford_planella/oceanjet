

$(document).ready(function(){

	var voyage_id ="";
	var return_origin="";
	var return_destination="";
	var return_voyage_id="";
	//var is_checked = false;
	var voyage_id = "";
	var orig_id = "";
	var dest_id = "";
	var voyage_mgmt_id = "";
	var is_checked = $("#is-return").val();
	// return check box
	$(document).on("click","#is-return",function(){
		if ($("#is-return").is(":checked")) {
			$(".return_voyage_date").removeAttr('disabled');
			$("#return-departure").removeAttr('disabled');
			$("#return-departure").selectpicker('refresh');
			
		} else {
			$(".return_voyage_date").attr('disabled',true);
			$("#return-departure").attr('disabled',true);
			$("#return-departure").selectpicker('refresh');
		}
	});
	// enable infant name
	$(document).on('change','#with_infant',function(){
		if($(this).is(':checked')) {
			$("#infant_name").removeAttr('disabled');
		} else {
			$("#infant_name").attr('disabled',true);
		}
	});

	$(document).on('change','#destination',function(){
		getDeparture($("#origin").val(),$(this).val());
	});
	// select voyage per date
	// todo:must be future date
	// $(document).on('change','.voyage_date',function(){
	// 	// $(this).data("DateTimePicker").minDate(new Date());
	// 	getDeparture($("#origin").val(),$("#destination").val(),$(this).val());
	// });
	// for testing
	$(document).on('change','#voyage_id',function(){
		// $(this).data("DateTimePicker").minDate(new Date());
		getDeparture($("#origin").val(),$("#destination").val());
	});
	// select return voyage per date
	// $(document).on('blur','.return_voyage_date',function(){
	// 	console.log($('#destination option:selected').val());
	// 	console.log($('#origin option:selected').val());
	// 	// $(this).data("DateTimePicker").minDate(new Date());
	// 	// getReturnDeparture($('#destination').val(),$('#origin').val(),$(this).val());
	// 	getReturnDeparture($('#destination option:selected').val(),$('#origin option:selected').val(),$(this).val());
	// });
	// get discount
	$(document).on('change', '#discount_id', function(e) {
	
		var discount_id = $(this).val();
		console.log(discount_id);
		var total_amount = $(this).parent().next().next().children('#fare_total_amount').val();
		var htmlInput = $(this).parent().next().next().children('#fare_total_amount');
		console.log(htmlInput);
		var original_amount = $(this).parent().next().next().children('#fare_total_amount').val();
		
		console.log(original_amount);
		$.post(adminURL+'eticket_passenger/get_discount', {discount_id:discount_id}, function(callback) {
			var obj = JSON.parse(callback);
			
			if(obj.length !== 0) {
				if(obj[0]['discount_type'] == 'amount') {
					var changed_amount = original_amount - obj[0]['discount_amount'];
				} else {
					var changed_amount = original_amount - (original_amount * (obj[0]['discount_amount'] / 100));
				}
				console.log(changed_amount);
				htmlInput.val(parseFloat(changed_amount).toFixed(2));
				console.log(htmlInput);
			} else if(obj.length === 0) {
				htmlInput.val(original_amount);

			} else {
				return false;
			}
		});
	});

	
	// get voyage
	$(document).on('change','#voyage_id',function(){
		voyage_id = $('#voyage_id option:selected').val();
		orig_id = $('#voyage_id option:selected').attr('data-orig');
		dest_id = $('#voyage_id option:selected').attr('data-dest');
		var no_of_subvoyages = $('#voyage_id option:selected').attr('data-no-sub');
		voyage_mgmt_id = $('#voyage_id option:selected').attr('data-voyage-mgmt');
		
		$('.orig-dest-details').html('');
		$('.orig-dest-details').append('<input type="hidden" name="origin" value="'+ orig_id +'" /><input type="hidden" name="destination" value="'+ dest_id +'"><input type="hidden" name="voyage_management" value="'+ voyage_mgmt_id +'" /><input type="hidden" name="voy_id" value="'+ voyage_id +'" />');
		if (no_of_subvoyages == "1") {
			$(".return-ticket-details").show();
		} else {
			$(".return-ticket-details").hide();
		}
		getTripDetails(orig_id,dest_id,voyage_mgmt_id);	
	});
	//get return voyage
	$(document).on('change','#return-departure',function(){
		
		return_voyage_id = $('#return-departure option:selected').val();
		return_orig_id = $('#return-departure option:selected').attr('data-orig');
		return_dest_id = $('#return-departure option:selected').attr('data-dest');
		return_voyage_mgmt_id = $('#return-departure option:selected').attr('data-voyage-mgmt');

		$('.return-orig-dest-details').html('');
		$('.return-orig-dest-details').append('<input type="hidden" name="return_origin" value="'+ return_orig_id +'" /><input type="hidden" name="return_destination" value="'+ return_dest_id +'"><input type="hidden" name="return_voyage_management" value="'+ return_voyage_mgmt_id +'" />');
		getReturnTripDetails(return_orig_id,return_dest_id,return_voyage_mgmt_id);
		getReturnPassengerFare(return_voyage_id,accommodation);

	});

	$(document).on('change','#accommodation',function(){
		accommodation = $(this).val();
		getPassengerFare(voyage_id,accommodation);
		
	});
	
	$(document).on("change","#passenger_fare_id",function(){
		
		if ($('#passenger_fare_id option:selected').val() !== "") {
		 	// get passenger fare
			getFareDetails($('#passenger_fare_id option:selected').val(),orig_id,dest_id,voyage_mgmt_id);	
		 }
	});

	$(document).on('blur', '#voyage_evoucher_code', function(e) {
		var evoucher_code = $(this).val();
		var total_amount = $(this).parent().next().children('#voyage_amount').val();
		var htmlInput = $(this).parent().next().children('#voyage_amount');
		var original_amount = $(this).parent().next().children('#voyage_amount').attr('data-original-value');
		
		$.post(requestPath()+'/get_evoucher', {evoucher_code:evoucher_code}, function(callback) {
			var obj = JSON.parse(callback);
			
			if(obj.length !== 0) {
				if(obj[0]['evoucher_type'] == 1) {
					var changed_amount = original_amount - obj[0]['amount'];
				} else {
					var changed_amount = original_amount - (original_amount * (obj[0]['amount'] / 100));
				}
				
				htmlInput.val(parseFloat(changed_amount).toFixed(2));
				console.log(htmlInput.val())
			} else if(obj.length === 0) {
				htmlInput.val(original_amount);
			} else {
				return false;
			}
		});
	});

	$(document).on("blur","#birthdate",computeAge);

	$(document).on('blur', '#voyage_evoucher_code', function(e) {
		var evoucher_code = $(this).val();
		var total_amount = $(this).parent().next().children('#voyage_amount').val();
		var htmlInput = $(this).parent().next().children('#voyage_amount');
		var original_amount = $(this).parent().next().children('#voyage_amount').attr('data-original-value');
		
		$.post(requestPath()+'/get_evoucher', {evoucher_code:evoucher_code}, function(callback) {
			var obj = JSON.parse(callback);
			
			if(obj.length !== 0) {
				if(obj[0]['evoucher_type'] == 1) {
					var changed_amount = original_amount - obj[0]['amount'];
				} else {
					var changed_amount = original_amount - (original_amount * (obj[0]['amount'] / 100));
				}
				
				htmlInput.val(parseFloat(changed_amount).toFixed(2));
				console.log(htmlInput.val())
			} else if(obj.length === 0) {
				htmlInput.val(original_amount);
			} else {
				return false;
			}
		});
	});
});


function getTripDetails(intOriginID,intDestinationID,intVoyageMgmt) {

	$.ajax({
		url: adminURL + "eticket_passenger/get_voyage_info",
		type:"POST",
		data:{origin:intOriginID,destination:intDestinationID,voyage_management:intVoyageMgmt},
		success : function(data){
			console.log(data);
			$(".ticket-details").html('');
			$(".ticket-details").html(data);

			var total_amount = parseFloat($("#terminal-fee").val()) + parseFloat($("#port-charge").val());
			$("#total_amount").val(total_amount.toFixed(2));
			
		}
	});
}

function getReturnTripDetails(intOriginID,intDestinationID,intVoyageMgmt) {

	$.ajax({
		url: adminURL + "eticket_passenger/get_return_voyage_info",
		type:"POST",
		data:{return_origin:intOriginID,return_destination:intDestinationID,return_voyage_management:intVoyageMgmt},
		success : function(data){
			
			$(".return-eticket-details").html('');
			$(".return-eticket-details").html(data);

		}
	});
}

function getFareDetails(intPassengerFareID,intOriginID,intDestinationID,intVoyageMgmt) {
	$.ajax({
		url:adminURL +'eticket_passenger/get_fare_details',
		type:'POST',
		data:{passenger_fare_id:intPassengerFareID,origin:intOriginID,destination:intDestinationID,voyage_management:intVoyageMgmt},
		success:function(data){
			var total_fare_amount =0;
			var new_total_amt=0;
			//console.log(result);
			$(".fare-details").html('');
			$(".fare-details").html(data);

			$('*[id*=fare_total_amount]').each(function(key,val){
				
				total_fare_amount += parseFloat($(this).val());
			});

			new_total_amt = parseFloat($("#total_amount").val()) + parseFloat(total_fare_amount);
			$("#total_amount").val(new_total_amt.toFixed(2));
		}
	});

}

function getReturnFareDetails(intPassengerFareID,intOriginID,intDestinationID,intVoyageMgmt) {
	$.ajax({
		url:adminURL +'eticket_passenger/get_return_fare_details',
		type:'POST',
		data:{return_origin:intOriginID,return_destination:intDestinationID,return_voyage_management:intVoyageMgmt},
		success:function(data){
			console.log("haller");
			console.log(data);
			$(".return-eticket-fare-details").html('');
			$(".return-eticket-fare-details").html(data);
		}
	});

}

function getDeparture(intOriginID,intDestinationID,dateDeparture) {

	$.ajax({
		url:adminURL +'eticket_passenger/get_voyage',
		type:'POST',
		dataType:'json',
		// data:{origin:intOriginID,destination:intDestinationID,departure_date:dateDeparture},
		data:{origin:intOriginID,destination:intDestinationID},
		success:function(result){
			$("#voyage_id").empty();
			$("#voyage_id").append($("<option></option>").val('').html("Please Select"));
			$.each(result,function(key,val){
				var voyage = val.voyage_code+"|"+val.port_origin+"-"+val.port_destination+"|"+val.dept_date+"|"+val.ETA+"-"+val.ETD;
				// $.each(val.departure_date,function(key1,val1){
					$("#voyage_id").append($("<option data-no-sub='"+ val.no_of_subvoyages +"' data-orig='"+val.origin_subvoyage_id+"' data-dest='"+val.destination_subvoyage_id+"' data-voyage-mgmt='"+val.voyage_management_id+"'></option>").val(val.voyage_id).html(voyage));
				// });
				
			});

			$("#voyage_id").selectpicker("refresh");
		}
	});
}



function getReturnDeparture(intOriginID,intDestinationID,dateDeparture) {
	
	$.ajax({
		url:adminURL +'eticket_passenger/get_voyage',
		type:'POST',
		dataType:'json',
		data:{origin:intOriginID,destination:intDestinationID,departure_date:dateDeparture},
		success:function(result){
			 console.log(result);
			$("#return-departure").empty();
			$("#return-departure").append($("<option></option>").val('').html("Please Select"));
			$.each(result,function(key,val){
				var voyage = val.voyage_code+"|"+val.port_origin+"-"+val.port_destination+"|"+val.dept_date+"|"+val.ETA+"-"+val.ETD;
				// $.each(val.departure_date,function(key1,val1){
					$("#return-departure").append($("<option data-orig='"+val.origin_subvoyage_id+"' data-dest='"+val.destination_subvoyage_id+"' data-voyage-mgmt='"+val.voyage_management_id+"'></option>").val(val.voyage_id).html(voyage));
				// });
				
			});
			$("#return-departure").selectpicker("refresh");
		}
	});
}

function getPassengerFare(intVoyageID,intAccommodationID) {
	$.ajax({
		url:adminURL +'eticket_passenger/get_passenger_fare',
		type:'GET',
		dataType:'json',
		data:{voyage_id:intVoyageID,accommodation_id:intAccommodationID},
		success:function(result){
			console.log(result);
			$("#passenger_fare_id").empty();
			$("#passenger_fare_id").append($("<option></option>").val('').html("Please Select"));
			$.each(result,function(key,val){
				
				$("#passenger_fare_id").append($("<option></option>").val(val.id_passenger_fare).html(val.passenger_fare));
				
			});
			$("#passenger_fare_id").selectpicker("refresh");
		}
	});
}

function getReturnPassengerFare(intVoyageID,intAccommodationID) {
	$.ajax({
		url:adminURL +'eticket_passenger/get_return_passenger_fare',
		type:'GET',
		dataType:'json',
		data:{voyage_id:intVoyageID,accommodation_id:intAccommodationID},
		success:function(data){
			console.log(data);
			$(".return-eticket-fare-details").html('');
			$(".return-eticket-fare-details").html(data);
			// console.log(result);
			// $("#passenger_fare_id").empty();
			// $("#passenger_fare_id").append($("<option></option>").val('').html("Please Select"));
			// $.each(result,function(key,val){
				
			// 	$("#passenger_fare_id").append($("<option></option>").val(val.id_passenger_fare).html(val.passenger_fare));
				
			// });
			// $("#passenger_fare_id").selectpicker("refresh");
		}
	});
}

function computeAge(){
	
	//var id = $(this).attr('call_id').split("_");
	var today = new Date();
	console.log(today);

	var birthDate = new Date($(this).val());
	console.log(birthDate);
	var age = today.getFullYear() - birthDate.getFullYear();
	console.log(age);
	var m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		age--;
	}
	
	if(age <= 0){
		alert("Age value is invalid.");
	}
	else{
		// Display age
		$("#age").val(age);
	}

	return false;
}











