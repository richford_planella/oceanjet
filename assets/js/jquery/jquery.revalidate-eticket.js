$(document).ready(function(){
	// initialise eticket no.
	$("#abc").click(function(e){
		//
		if ($("#eticket_no").val() !== "" && $("#last_name").val() !== "") {
			// get result
			$.ajax({
				url: adminURL + "revalidate_eticket/get_revalidate_eticket",
				type:'GET',
				dataType:'json',
				data:{eticket_no:$("#eticket_no").val(),lastname:$("#last_name").val()},
				success:function(result){
					console.log(result);
					//
					if(result.length > 0 && result != null) {
						// console.log("test");
						var table = $(".table tbody");
						// table.empty();
					    $.each(result, function(idx, elem){
					    	fullname = elem.firstname + " " + elem.lastname;
					        $("#fullname").val(fullname);
					        $("#voyage_no").val(elem.voyage);
					        $("#departure_date").val(elem.departure_date);
					        $("#origin").val(elem.origin);
					        $("#destination").val(elem.destination);
					        $("#etd").val(elem.ETD);
					        $("#eta").val(elem.ETA);
					        $("#vessel").val(elem.vessel);
					        $("#booking_id").val(elem.id_booking);
					        $("#accommodation_type").val(elem.accommodation);
					        $("#voyage_status").val(elem.description);
					        $("#origin-id").val(elem.origin_id);
					        $("#destination-id").val(elem.destination_id);
					        // get departure
					        getDeparture(elem.origin_id,elem.destination_id);
					       
					    });
					} else {
						$('#myModal').modal('show');
					}
				}
			});	
		} else {
			alert("no result");
		}
		
		return false;
	});
});

function getDeparture(intOriginID,intDestinationID) {
	$.ajax({
		url:adminURL +'eticket_passenger/get_voyage',
		type:'GET',
		dataType:'json',
		data:{origin:intOriginID,destination:intDestinationID},
		success:function(result){
			console.log(result);
			$("#upgrade-departure").empty();
			$("#upgrade-departure").append($("<option></option>").val('').html("Please Select"));
			$.each(result,function(key,val){
				var voyage = val.voyage_code+"|"+val.port_origin+"-"+val.port_destination+"|"+val.dept_date+"|"+val.ETA+"-"+val.ETD;
				// $.each(val.departure_date,function(key1,val1){
					$("#upgrade-departure").append($("<option></option>").val(val.voyage_id).html(voyage));
				// });
				
			});
			$('#upgrade-departure').selectpicker('refresh');

		}
	});
}

//php
