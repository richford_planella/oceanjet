/*
|--------------------------------------------------------------------------
| jQuery Tools for user
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                jQuery(".filter_val").hide();
                                
                jQuery(document).on('change', '#user_profile_id', showOutlet);
                
                if(user_profile == '1')
                {
                    jQuery(".filter_val").show();
                }
                
                $("#imgInp").change(function(){
                    readURL(this);
                });
        }        
);
function showOutlet()
{
    var user_profile_id = $(this).val();
    
    if(user_profile_id)
    {
        var is_outlet = $('option:selected', this).attr('outlet');
        
        if(is_outlet == '1')
        {
            jQuery(".filter_val").show();
        }
        else
        {
            jQuery(".filter_val").hide();
        }
        
    }
    else
    {
        jQuery(".filter_val").hide();
    }
}

function readURL(input) {
	console.log(input.files);
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#img_uploaded').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}