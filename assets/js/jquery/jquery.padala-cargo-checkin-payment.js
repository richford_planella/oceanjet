/*
|--------------------------------------------------------------------------
| jQuery Tools for Padala Cargo Check - In Payment
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                checkPorts();
                jQuery(document).on('change', 'select[name=origin_id], select[name=destination_id]', checkPorts);
                jQuery(document).on('change keyup', 'select[name=subvoyage_management_id]', fillTripDetails);
                jQuery(document).on('click', '#btn-search-ref-no', searchReferenceNo);
                jQuery(document).on('change keyup', '#amount-tendered', computeChange);
        }
);


function computeChange()
{
    var total_amount = $("#total-amount").val();
    var amount_tendered = $("#amount-tendered").val();

    var change = amount_tendered - total_amount;

    $("#change").val(round(change));
}

function searchReferenceNo()
{
    var subvoyage_management_id = $("select[name=subvoyage_management_id] option:selected").val();
    var reference_no = $("#reference_no").val();

    if (subvoyage_management_id && reference_no)
    {
        var post_data = {subvoyage_management_id : subvoyage_management_id, reference_no : reference_no };

        $.post(adminURL+"padala_cargo_checkin_payment/searchReferenceNo", post_data, function(data){
            var data = JSON.parse(data);
            if (data.result) {
                fillCargoDetails(data.checkin_details[0]);
            } else {
                // Add Body
                $('body').addClass('margin-dialog');   

                 BootstrapDialog.show({
                    title: '<h3>Padala Cargo Check-In</h3>', // modal title
                    message: '<h4>'+data.checkin_details+'</h4>', // modal body
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn oj-button',
                        action: function(dialog) {
                           dialog.close(); // close modal
                        }
                    }]
                });
                emptyCargoDetails();
            }
        });
    }
}

function fillCargoDetails(details)
{
    $("#padala-cargo-rate").val("TBD");
    $("#particulars").val(details.particulars);
    $("#sender").val(details.sender_first_name + " " + details.sender_last_name);
    $("#sender-contact-no").val(details.sender_contact_no);
    $("#recipient").val(details.recipient_first_name + " " + details.recipient_last_name);
    $("#recipient-contact-no").val(details.recipient_contact_no);
    $("#total-amount").val(details.total_amount);

    $("#checkin_id").val(details.id_padala_cargo_checkin);
}

function emptyCargoDetails()
{
    $("#div-cargo-details input:not(#reference_no)").val("");
    $("#total-amount").val("");
}

function fillTripDetails()
{
    var subvoyage_management = $("select[name=subvoyage_management_id] option:selected");

    if ($(subvoyage_management).val()) {
        var voyage                      =   $(subvoyage_management).attr("voyage");
        var departure_date              =   $(subvoyage_management).attr("departure_date");
        var origin                      =   $(subvoyage_management).attr("origin");
        var destination                 =   $(subvoyage_management).attr("destination");
        var ETD                         =   $(subvoyage_management).attr("ETD");
        var ETA                         =   $(subvoyage_management).attr("ETA");
        var vessel                      =   $(subvoyage_management).attr("vessel");
        var subvoyage_management_status =   $(subvoyage_management).attr("subvoyage_management_status");

        $("#trip-voyage-code").val(voyage);
        $("#trip-departure-date").val(departure_date);
        $("#trip-origin").val(origin);
        $("#trip-destination").val(destination);
        $("#trip-ETD").val(ETD);
        $("#trip-ETA").val(ETA);
        $("#trip-vessel").val(vessel);
        $("#trip-subvoyage-management-status").val(subvoyage_management_status);
        $("#subvoyage_management_id").val($(subvoyage_management).val());
    } else {
        $("#div-trip-details input").val("");
        $("#trip-subvoyage-management-status").val("");
    }

    emptyCargoDetails();
}

function round(x) {
    return parseFloat(Math.round(x * 100) / 100).toFixed(2);
}

function checkPorts()
{
    var origin_id = $('select[name=origin_id] option:selected').val();
    var destination_id = $('select[name=destination_id] option:selected').val();

    if (origin_id.length != 0 && destination_id.length != 0) {
       if ($('select[name=origin_id] option:selected').val() == $('select[name=destination_id] option:selected').val()) {
            // Add Body
            $('body').addClass('margin-dialog');   

            BootstrapDialog.show({
                title: '<h3>Padala Cargo Check-In Payment</h3>', // modal title
                message: '<h4>Origin and Destination should not be the same.</h4>', // modal body
                buttons: [{
                    label: 'Ok',
                    cssClass: 'btn oj-button',
                    action: function(dialog) {
                    dialog.close(); // close modal
                    }
                }]
            });
        } else {
            var post_data = {origin_id : origin_id, destination_id : destination_id };

            $.post(adminURL+"padala_cargo_checkin/getVoyages", post_data, function(data){
                var parsed_data = JSON.parse(data);
                var del = " | ";
                var html = "";

                var subvoyage_management_id = $("#subvoyage_management_id").val();

                $("select[name=subvoyage_management_id] option:not(:first)").remove();
                for (x in parsed_data['subvoyages']) {
                    var voyage_code = parsed_data['subvoyages'][x].voyage_code;
                    var origin = parsed_data['subvoyages'][x].port_origin;
                    var destination = parsed_data['subvoyages'][x].port_destination;
                    var departure_date = parsed_data['subvoyages'][x].dept_date;
                    var departure_date_f = moment(departure_date).format('MMMM D, YYYY - ddd');
                    var ETD = parsed_data['subvoyages'][x].ETD;
                    var ETA = parsed_data['subvoyages'][x].ETA;
                    var vessel = parsed_data['subvoyages'][x].vessel;
                    var id_subvoyage_management = parsed_data['subvoyages'][x].id_subvoyage_management;
                    var subvoyage_management_status = parsed_data['subvoyages'][x].subvoyage_management_status;

                    var subvoyage_details = voyage_code + del + origin + " - " + destination + del + departure_date_f + del + ETD + del + ETA;

                    if (id_subvoyage_management == subvoyage_management_id)
                        html += "<option value="+id_subvoyage_management+" voyage=\""+voyage_code+"\" departure_date=\""+departure_date+"\" origin=\""+origin+"\" destination=\""+destination+"\" vessel=\""+vessel+"\" ETA=\""+ETA+"\" ETD=\""+ETD+"\" subvoyage_management_status=\""+subvoyage_management_status+"\" selected>"+subvoyage_details+"</option>";
                    else
                        html += "<option value="+id_subvoyage_management+" voyage=\""+voyage_code+"\" departure_date=\""+departure_date+"\" origin=\""+origin+"\" destination=\""+destination+"\" vessel=\""+vessel+"\" ETA=\""+ETA+"\" ETD=\""+ETD+"\" subvoyage_management_status=\""+subvoyage_management_status+"\">"+subvoyage_details+"</option>";
                }
                $("select[name=subvoyage_management_id]").append(html).selectpicker("refresh");

                fillTripDetails(); //if there's selected option, fill trip details
                searchReferenceNo();
            });
        }
    }

    return false;
}