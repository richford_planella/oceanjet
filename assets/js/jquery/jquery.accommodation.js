/*
|--------------------------------------------------------------------------
| jQuery Tools for Accommodation Type
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                jQuery(document).on('click', '.btn-delete-accommodation', deleteValidation);
                jQuery(document).on('click', '.btn-activate-accommodation', activateValidation);
                jQuery(document).on('click', '.btn-deactivate-accommodation', deActivateValidation);
        }
);

function activateValidation()
{            
    var link = $(this).attr('href');

    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Accommodation Type<h3>', // modal title
            message: '<h4>Are you sure you want to re-activate accommodation type?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deActivateValidation()
{            
    var link = $(this).attr('href');
    
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Accommodation Type<h3>', // modal title
            message: '<h4>Are you sure you want to de-activate accommodation type?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deleteValidation()
{            
    var link = $(this).attr('href');
    
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Accommodation Type<h3>', // modal title
            message: '<h4>Are you sure you want to delete accommodation type?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}