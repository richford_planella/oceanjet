/*
|--------------------------------------------------------------------------
| jQuery Tools for Voyage
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                jQuery(document).on('click', '.btn-delete-voyage', deleteValidation);
                jQuery(document).on('click', '.btn-activate-voyage', activateValidation);
                jQuery(document).on('click', '.btn-deactivate-voyage', deActivateValidation);
//                jQuery(document).on('dp.change', '.oj-etd', checkETD);
//                jQuery(document).on('dp.change', '.oj-eta', checkETA);
                
                var dTd = datepickerDiff;
    
                $("#add-leg-btn").click(function () {
                    var addLeg = $(".oj-leg:first").clone();
                    //var addLeg = '<div class="form-group oj-leg"> <div class="col-lg-12"> <div class="input-group"> <div class="oj-leg__title oj-leg__title--gray"> Leg 1 </div> <span class="input-group-btn"> <button class="btn btn-default oj-leg__btn" type="button">Remove</button> </span> </div> </div> <div class="col-xs-12 oj-leg__body"> <div class="form-group"> <div class="col-xs-3"> <label class="control-label">Voyage Number</label> <input class="form-control" type="text" placeholder="Input Number" /> <span class="input-notes-bottom">*Note: your note</span> </div> <div class="col-xs-3"> <label class="control-label">Defualt Vessel</label> <select class="selectpicker form-control" title="Choose"> <option value="">Vessel one</option> <option value="">Vessel two</option> <option value="">Vessel three</option> </select> </div> <div class="col-xs-3"> <label class="control-label">Origin</label> <select class="selectpicker form-control" title="Choose"> <option value="">MNL</option> <option value="">DAV</option> <option value="">CBU</option> </select> <span class="input-notes-bottom">*Note: your note</span> </div> </div> <div class="form-group"> <div class="col-xs-3 has-feedback"> <label class="control-label">ETD <small>(Estimated time of Departure)</small></label> <input class="form-control oj-time-picker oj-etd" type="text" placeholder="HH:MM" /> <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span> <span class="input-notes-bottom">*Note: your note</span> </div> <div class="col-xs-3 has-feedback"> <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label> <input class="form-control oj-time-picker oj-eta" type="text" placeholder="HH:MM" /> <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span> <span class="input-notes-bottom">*Note: your note</span> </div> <div class="col-xs-3"> <label class="control-label">Travel Time</label> <input class="form-control oj-time-picker oj-travel-time" type="text" value="300" readonly/> <span class="input-notes-bottom">*Note: your note</span> </div> </div> </div> </div>';
                    $("#oj-leg--voyage-cms").append(addLeg);
                    addLeg.find('.selectpicker').selectpicker('refresh');
                    addLeg.find('.oj-time-picker').val("");
                    addLeg.find('.points_redemption').val("");
                    
                    //reload price class
                    loadNumeric();   
                    
                    //remove the extra selectpicker
                    addLeg.find('.bootstrap-select > .bootstrap-select').unwrap();
                    addLeg.find('.col-xs-3 > .dropdown-toggle').remove();
                    addLeg.find('.col-xs-3 > .dropdown-menu').remove();
                    
                    
                    legCounter();

                    //datetimepicker
                    dTd();
                });
                
                $(document).on('click',".oj-leg__btn",removeBtn);
        }
);

function checkETD()
{
    alert($(this).val());
}

function checkETA()
{
    alert($(this).val());
}


function activateValidation()
{            
    var link = $(this).attr('href');
        
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Voyage Code</h3>', // modal title
            message: '<h4>Are you sure you want to re-activate voyage code?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deActivateValidation()
{            
    var link = $(this).attr('href');
        
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Voyage Code</h3>', // modal title
            message: '<h4>Are you sure you want to de-activate voyage code?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deleteValidation()
{            
    var link = $(this).attr('href');
        
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Voyage Code</h3>', // modal title
            message: '<h4>Are you sure you want to delete voyage code?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

var legCounter = function () {
    $(".oj-leg").each(function (index) {
      $(this).find(".oj-leg__title").html("Leg " + (index + 1));
    });
    $("#leg_count").val($(".oj-leg").length);
};

function removeBtn() {
        if ($('.oj-leg').length != 1) {
            $(this).parents(".oj-leg").remove();
            legCounter();
        }
    }

window.onload = function () {
$('.oj-time-picker').trigger('dp.change');
};