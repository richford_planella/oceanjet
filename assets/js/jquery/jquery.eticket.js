/*
|--------------------------------------------------------------------------
| TinyMCE
|--------------------------------------------------------------------------
*/

tinymce.init({
  selector: "textarea",
  
  // ===========================================
  // INCLUDE THE PLUGIN
  // ===========================================
	
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste jbimages"
  ],
	
  // ===========================================
  // PUT PLUGIN'S BUTTON on the toolbar
  // ===========================================
	
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages",
	
  // ===========================================
  // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
  // ===========================================
	
  relative_urls: false
	
});

$(function(){
    jQuery(document).on('click', '#btn-preview', preview);
});

function preview()
{
	// Get tinymce contents
	var inst, contents = new Object();
	for (inst in tinyMCE.editors) {
	    if (tinyMCE.editors[inst].getContent)
	        contents[inst] = tinyMCE.editors[inst].getContent();
	}

	// Initialize
	var image = $("#img_uploaded");
	var image_html = "";

	// Build the image
	image_html = "<img src='"+$("#img_uploaded").attr('src')+"' alt='image' />";

	// HTML Contents
	var html =  image_html+"\n";
		html += contents.eticket_details+"\n";
		html += contents.eticket_terms+"\n";
		html += contents.eticket_reminders+"\n";
	
	// Set the content
	tinyMCE.get('eticket_full').setContent(html);

	// Preview the eticket
	tinyMCE.get('eticket_full').execCommand('mcePreview');

}

// Onchange handler
$("#imgInp").change(function(){
  	readURL(this);
});

// Read Image Uploaded
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#img_uploaded').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}