

$(document).ready(function() {
	

	$(document).on("change","#departure",function(e){
		var voyage_id = $(this).val();
		// ajax
		$.post(adminURL+"roro_eticket/get_voyage_details",{voyage_id:voyage_id},function(callback){
			var obj = $.parseJSON(callback);
			console.log(obj);
			$("#port_origin").val(obj[0].origin);
			$("#port_destination").val(obj[0].destination);
			$("#eticket_no").val(obj.eticket_no);
			$("#port_charge").val(obj.port_charge);
			$("#terminal_fee").val(obj.terminal_fee);
			total_due = parseInt(obj.terminal_fee) + parseInt(obj.port_charge); 
			console.log(total_due);
			$("#total_due").val(total_due);
		});

		return false;
		
	});

	$(document).on("change","#roro_rate",function(){
		console.log($(this).val());
		var roro_rate = $(this).val();
		$.post(adminURL+"roro_eticket/get_roro_rate",{rid:roro_rate},function(callback){
			var obj = $.parseJSON(callback);
			var current_due = $("#total_due").val();
			
			console.log(obj);
			$("#roro_amount").val(obj[0].amount);
			total_due = parseInt(obj[0].amount) + parseInt(current_due); 
			console.log(total_due);
			$("#total_due").val(total_due);
		});

		return false;

	});

	$(document).on("change","#discount_id",function(){
		console.log($(this).val());
		var discount_id = $(this).val();
		$.post(adminURL+"roro_eticket/get_discount",{discount_id:discount_id},function(callback){
			var obj = $.parseJSON(callback);
			var current_due = $("#total_due").val();
			
			console.log(obj);
			if (obj[0].discount_type == 'percentage') {
				total_due = parseInt(current_due) - (parseInt(current_due) * parseInt(obj[0].discount_amount) / 100);	
			} else {
				total_due = parseInt(current_due) - parseInt(obj[0].discount_amount);
			}
			$("#discount_amount").val(obj[0].discount_amount);
			 
			console.log(total_due);
			$("#total_due").val(total_due);
		});

		return false;

	});



	
});




// function getRORORate(intRoroRateID) {
// 	$.ajax({
// 		url: adminURL + "roro_eticket/get_roro_rate",
// 		type:"GET",
// 		dataType:"json",
// 		data:{rid:intRoroRateID},
// 		success : function(result){
// 			console.log(result);
// 			$("#roro_amount").val(result[0].amount);
			
// 		}
// 	});
// }