/*
|--------------------------------------------------------------------------
| jQuery Tools for Attended Baggage Rate
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                jQuery(document).on('click', '.btn-delete-attended-baggage-rate', deleteValidation);
                jQuery(document).on('click', '.btn-activate-attended-baggage-rate', activateValidation);
                jQuery(document).on('click', '.btn-deactivate-attended-baggage-rate', deActivateValidation);
                jQuery(document).on('change', 'select[name=origin_id], select[name=destination_id]', checkPorts);
                jQuery(document).on('click', '#btn-submit', checkPorts);
        }
);

function checkPorts()
{
    var origin_id = $('select[name=origin_id] option:selected').val();
    var destination_id = $('select[name=destination_id] option:selected').val();

    if (origin_id.length != 0 || destination_id.length != 0) {
       if ($('select[name=origin_id] option:selected').val() == $('select[name=destination_id] option:selected').val()) {

            // Add Body
            $('body').addClass('margin-dialog');   
            
            BootstrapDialog.show({
                title: '<h4>Attended Baggage Rate</h4>', // modal title
                message: '<h3>Origin and Destination should not be the same.</h3>', // modal body
                buttons: [{
                    label: 'Ok',
                    cssClass: 'btn oj-button',
                    action: function(dialog) {
                    dialog.close(); // close modal
                    }
                }]
            });
                
            return false;
        }
    }
}

function activateValidation()
{            
    var link = $(this).attr('href');
    
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Attended Baggage Rate</h3>', // modal title
            message: '<h4>Are you sure you want to re-activate attended baggage rate?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deActivateValidation()
{            
    var link = $(this).attr('href');
    
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Attended Baggage Rate</h3>', // modal title
            message: '<h4>Are you sure you want to de-activate attended baggage rate?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deleteValidation()
{            
    var link = $(this).attr('href');
    
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Attended Baggage Rate</h3>', // modal title
            message: '<h4>Are you sure you want to delete attended baggage rate?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}