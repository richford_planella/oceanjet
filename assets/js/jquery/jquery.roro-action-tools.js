/*
|--------------------------------------------------------------------------
| jQuery Tools for vessel action
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                jQuery(document).on('click', '#add_voyage', addVoyage);
        }
);

function addVoyage()
{
    var voyage_id = $('select[name="voyage_id"]').val();
    
    if(voyage_id)
    {
        $('select option[value="'+voyage_id+'"]').attr('disabled', 'disabled');
        $('select option[value="'+voyage_id+'"]').removeAttr('selected');
        $('select[name="voyage_id"]').val(0);
        $('tr#voyage_list').before("<tr><td>"+voyage_id+"</td><td>DELETE</td></tr>");
    }
    else
    {
        alert("wow");
    }
    return false;
}