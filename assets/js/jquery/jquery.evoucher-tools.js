/*
|--------------------------------------------------------------------------
| jQuery Tools for evoucher
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                jQuery(document).on('click', '#submit_btn', checkSubmit);
                jQuery(document).on('change', '#evoucher_type', changeAmount);
                
                jQuery(document).on('click', '.cancel-all', checkAll);
                jQuery(document).on('click', '.cancel-single', checkSingle);
                jQuery(document).on('click', '#udpate_modal', updateBilling);
                
                // hide percent 
                $("#select_amount").show();
                $("#select_percent").hide();
                
                if(set_amount == '1')
                {
                    $("#select_amount").show();
                    $("#select_percent").hide();
                }
                else if(set_amount == '2')
                {
                    $("#select_amount").show();
                    $("#select_percent").hide();
                }
                else
                {
                    $("#select_amount").hide();
                    $("#select_percent").show();
                }
                                            
                // Initialize the table
                var tableInput = $('#table-evoucher');
                var table = tableInput.DataTable({
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            searchable: false,
                        }
                ],
                    select: {
                        style: 'multi'
                    },
                    order: [[1, 'asc']],
                    "dom": 'rt<"bottom"lp>'
                });
                dataTableCheckAll(tableInput, table);
                
                $('#column4_search').on( 'keyup', function () {
                                                                    table
                                                                        .search(this.value)
                                                                        .draw();
                                                                } );
        }
);

function checkDate()
{
    //alert($(this).val());
}

function updateBilling()
{
    $("#mainForm").submit();
}

function checkAll()
{
    // check length
    var len = $(".checkbox1:checked").length;
    
    if(len > 0)
    {
        $('body').addClass('margin-dialog');
        BootstrapDialog.show({
                            title: '<h3>E-Voucher</h3>', // modal title
                            message: '<h4>Are you sure you want to cancel selected e-voucher series?</h4>', // modal body
                            buttons: [{
                                    label: 'No',
                                    cssClass: 'btn oj-button',
                                    action: function(dialog) {
                                            dialog.close(); // close modal
                                    }
                            }, {
                                    label: 'Yes',
                                    cssClass: 'btn oj-button',
                                    action: function(dialog) {
                                            // ajax post/jQuery post code
                                            jQuery("#evoucher_form").submit();
                                    }
                            }]
                    });
        
        return false;
    }
    else
    {
        $('body').addClass('margin-dialog');
        BootstrapDialog.show({
                    title: '<h3>E-Voucher</h3>', // modal title
                    message: '<h4>Please select a voucher!</h4>', // modal body
                    buttons: [{
                            label: 'Dismiss',
                            cssClass: 'btn oj-button',
                            action: function(dialog) {
                                    dialog.close(); // close modal
                            }
                    }]
            });
        return false;
    }
    
    return false;
}

function checkSingle()
{
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>E-Voucher</h3>', // modal title
			message: '<h4>Are you sure you want to cancel e-voucher?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        window.location.replace(link);
				}
			}]
		});
                    
    return false;
}

function checkMainSelect()
{
    //declare flag
    var flag = 1;
    
    // loop all textbox 
    $(".myTextBox").each(function(){
        if($(this).val() == '0')
        {
            flag = 0;
        }
    });
    
    if(flag == 1)
    {
        if($('.myTextBox').length > 0)
        {
            $("div#uniform-selectall span").addClass('checked');
        }
    }
    else
    {
        $("div#uniform-selectall span").removeClass('checked'); 
    }
}

function checkSubmit()
{
    var orig_id         = $("#origin_id").val();
    var destination_id  = $("#destination_id").val();
    
    if(orig_id != "")
    {
        if(orig_id == destination_id)
        {
            $('body').addClass('margin-dialog');
            BootstrapDialog.show({
			title: '<h3>E-Voucher</h3>', // modal title
			message: '<h4>Destination should not be same as Origin!</h4>', // modal body
			buttons: [{
				label: 'Dismiss',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}]
		});
                
            return false;
        }
    }
    
    return true;
    
}

function changeAmount()
{
    var id = $(this).val();
    
    if(id == '1')
    {
        $("#select_amount").show();
        $("#select_percent").hide();
    }
    else if(id == '2')
    {
        $("#select_amount").show();
        $("#select_percent").hide();
    }
    else
    {
        $("#select_amount").hide();
        $("#select_percent").show();
    }
}

function checkOrigDestination()
{
    var orig = $("#origin_id").val();
    var destination = $("#destination_id").val();
    
    if(orig != "")
    {
        if(orig == destination)
        {
            $('#message_box').html("Origin and Destination ");
            $('#myModal').modal('show');
        }
    }
    
}