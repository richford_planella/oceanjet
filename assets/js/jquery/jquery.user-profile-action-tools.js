/*
|--------------------------------------------------------------------------
| jQuery Tools for user profile action add/edit
|--------------------------------------------------------------------------
*/

jQuery(function()
	{
		jQuery(document).on('change', '#select_main_page', updateSelect);
                jQuery(document).on('click', '#has_menu', displayPage);
                                
                displayPage();
                //alert(default_page);
	}
);

function displayPage()
{
    
    if (jQuery("#has_menu").is(':checked'))
    {
        jQuery('.landing-page').hide();
    }
    else
    {
        jQuery('.landing-page').show();
    }
}

function updateSelect()
{
    // get selected value
    var id = $(this).val();
    // add hidden class
    $(".main-page").addClass("hidden");
    // remove hidden from selected page
    $(".admin-page-"+id).removeClass("hidden");
}