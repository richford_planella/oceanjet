/*
|--------------------------------------------------------------------------
| Rule Set
|--------------------------------------------------------------------------
*/
// Check if the percentage is more than 100
$(".checkbox-fields__radio").on("change", function(){
	checkPercentage($(this), "onchange")
});

$(".checkbox-fields").each(function(){
	checkPercentage($(this), "onload")
});

// Check the percentage
function checkPercentage(module, ev){

	if(ev == "onload"){		
		var _module = module.find(".checkbox-fields__radio:first");
		var other_class = module.find(".checkbox-fields__radio:first").attr('class');

		if(other_class){
			var _class = other_class.split(" ")[1];
			var def_class = _class.split("-");	
			var textbox = $('.accommodation-table--height-auto').find('#val-'+def_class[1]);
			
			if(_module.is(":checked"))
				textbox.removeAttr("max")
			else
				textbox.attr("max", "100")
		}

	}
	
	if(ev == "onchange"){
		var other_class = module.attr('class').split(" ")[1];
		var def_class = other_class.split("-");	
		var textbox = $('.accommodation-table--height-auto').find('#val-'+def_class[1]);

		if(module.val() == "percentage")
			textbox.attr("max", "100")
		else
			textbox.removeAttr("max")
	}
	
}

// Checkbox fix
var cntTestBox = 0;
var textbox = $('.accommodation-table--height-auto').find('.price');

$(".checkbox-fields__check").each(function(){
	var is_check = $(this).is(":checked");
	var value = $(this).attr('value');
	var txt_box_id = textbox[cntTestBox++].id;
	var split_txt = txt_box_id.split("-");
	
	if(value == split_txt[1])
		if(is_check){
			$(".rad-"+value).attr('disabled', false); 
			$("#"+txt_box_id).attr('disabled', false); 
		}
		else{
			$(".rad-"+value).attr('disabled', true); 
			$("#"+txt_box_id).attr('disabled', true); 
		}
});


// Validation for conditions
$(document).on('submit', '.oj-form', function(event){

	// Module Name
	var module = "Rule Set";

	if(jQuery('.oj-form input[type=checkbox]:checked').length < 1){
		modalDismissDialog($(this), module, "Please select at least one condition");
		return false;
	}
});

// Module Name
var module = "Rule Set";
	
// Modal dialog
$(document).on('click', '.activate', function(event){
	modalDialog($(this), module, "Are you sure you want to re-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.deactivate', function(event){
	modalDialog($(this), module, "Are you sure you want to de-activate " + module.toLowerCase() + " ?"); event.preventDefault();
});

$(document).on('click', '.remove', function(event){
	modalDialog($(this), module, "Are you sure you want to delete " + module.toLowerCase() + " ?"); event.preventDefault();
});

// Activate, Deactivate, Remove Modal
function modalDialog(obj, title, message)
{
	// Get anchor	
    var link = $(obj).attr('href');
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'No',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}, 
		{
			label: 'Yes',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				// ajax post/jQuery post code
				window.location.replace(link);
			}
		}]
	});
        
    return false;
}

// Activate, Deactivate, Remove Modal
function modalDismissDialog(obj, title, message)
{            
	// Get anchor	
    var link = $(obj).attr('href');
	
	// Add Body
	$('body').addClass('margin-dialog');   

	BootstrapDialog.show({
		title: '<h3>' + title + '</h3>', // modal title
		message: '<h4>' + message + '</h4>', // modal body
		buttons: [{
			label: 'Dismiss',
			cssClass: 'btn oj-button',
			action: function(dialog) {
				dialog.close(); // close modal
			}
		}]
	});
        
    return false;
}