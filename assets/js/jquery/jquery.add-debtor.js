
$(function(){
    var selectVal='';
    var selectText='';
    var selectRel="";
    var arOutlet =[];
    var arPostOutlet = [];

    $('#table-v1').DataTable();
	$("#outlet_id").change(function(){
        selectVal = $(this).val();
        selectText = $("#outlet_id option:selected").text();
        selectRel = $("#outlet_id option:selected").attr("rel");
        
 
    });


    // get post data
    $('*[id*=test]').each(function(){
        arOutlet.push($(this).val());
    });

    $(document).on("click",".add-outlet",function(event){
       
        if (selectVal !== "" ) {
           if ($.inArray(selectVal,arOutlet) !='-1') {
                $('body').addClass('margin-dialog');
                 BootstrapDialog.show({
                     title: '<h4>Debtor Management</h4>', // modal title
                     message: '<h3>Outlet Already in use</h3>', // modal body
                     buttons: [{
                     label: 'Dismiss',
                     cssClass: 'btn oj-button',
                    action: function(dialog) {
                        dialog.close(); // close modal
                    }
                }]
               });
                 return false;
           } else {
            var index = $('#outlet_id').get(0).selectedIndex;
            $('li[data-original-index='+index+']:contains("'+selectText+'")').removeClass('selected').addClass('disabled');
            //console.log(test);
            $("#hidden_outlet").val(selectVal);
            $("#outlet_id").each(function(){
                var addtbl = $('.table #outlet_tbody');
                addtbl.append("<tr><td>"+selectText+"<input id='test' type='hidden' name='outlet_id[]' value='"+selectVal+"'/></td><td>"+selectRel+"</td><td><a href='' alt='"+index+"'rel='"+selectVal+"' class='remove-outlet'>Delete</a></td></tr>");
            });
                      
             arOutlet.push(selectVal); 
              
           }
            return false;   
        } else {
            $('body').addClass('margin-dialog');
              BootstrapDialog.show({
                     title: '<h4>Debtor Management</h4>', // modal title
                     message: '<h3>Please Select an Outlet</h3>', // modal body
                     buttons: [{
                     label: 'Dismiss',
                     cssClass: 'btn oj-button',
                    action: function(dialog) {
                        dialog.close(); // close modal
                    }
                }]
               });
        
            return false;
        }
        return false;
    });
    
    $(document).on("click",".remove-outlet",function(event){
        var outletid = $(this).attr("rel");
        var parent_tr = $(this).closest("tr");
        if (parent_tr.parent("tbody").children("tr").length >= 1) {

            // var index = $('#outlet_id').get(0).selectedIndex;
            var index = $(this).attr("alt");
            console.log(index);
            // remove outlet           
            arOutlet.splice($.inArray(outletid,arOutlet),1);
            // $('li[data-original-index='+index+']:contains("'+selectText+'")').removeClass('disabled');
            $('li[data-original-index='+index+']').removeClass('disabled');

            // remove tr
            parent_tr.remove();
        }
       return false;
      
        // return false;
          
    });    

    	
 //   $(document).on('click', '.delete-debtor', deleteValidation);
    $(document).on('click', '.activate-debtor', activateValidation);
    $(document).on('click', '.de-activate-debtor', deActivateValidation);
    $(document).on('click', '.delete-debtor', deleteValidation);
});
function activateValidation()
{            
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
            title: '<h4>Debtor Management</h4>', // modal title
            message: '<h3>Are you sure you want to re-activate Debtor?</h3>', // modal body
            buttons: [{
                label: 'No',
                                cssClass: 'btn oj-button',
                action: function(dialog) {
                                        dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                                        window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deActivateValidation()
{     
    $('body').addClass('margin-dialog');       
    var link = $(this).attr('href');
    
    BootstrapDialog.show({
            title: '<h3>Debtor Management</h3>', // modal title
            message: '<h4>Are you sure you want to de-activate Debtor?</h4>', // modal body
            buttons: [{
                label: 'No',
                                cssClass: 'btn oj-button',
                action: function(dialog) {
                                        dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                                        window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deleteValidation()
{    
    $('body').addClass('margin-dialog');        
    var link = $(this).attr('href');
    
    BootstrapDialog.show({
            title: '<h3>Debtor Management</h3>', // modal title
            message: '<h4>Are you sure you want to delete Debtor ?</h4>', // modal body
            buttons: [{
                label: 'No',
                                cssClass: 'btn oj-button',
                action: function(dialog) {
                                        dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                                        window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function outletValidation()
{        
    var link = $(this).attr('href');
    
    BootstrapDialog.show({
            title: '<h3>Debtor Management</h3>', // modal title
            message: '<h4>Please Select an Outlet</h4>', // modal body
            buttons: [{
                label: 'Dissmiss',
                                cssClass: 'btn oj-button',
                action: function(dialog) {
                                        dialog.close(); // close modal
                }
            
            }]
        });
        
    return false;
}

