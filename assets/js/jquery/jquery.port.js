/*
|--------------------------------------------------------------------------
| jQuery Tools for Voyage
|--------------------------------------------------------------------------
*/

jQuery(function() {
	jQuery(document).on('click', '.btn-delete-port', deleteValidation);
	jQuery(document).on('click', '.btn-activate-port', activateValidation);
	jQuery(document).on('click', '.btn-deactivate-port', deActivateValidation);
	
	// $(document).on('keyup', '.port_code', function(e) {
		// var characters = $(this).val();
		// if(characters.length = 5)
			
	// });
});

function activateValidation()
{            
    var link = $(this).attr('href');
	$('body').addClass('margin-dialog');
    
    BootstrapDialog.show({
            title: '<h3>Port</h3>', // modal title
            message: '<h4>Are you sure you want to re-activate this port?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deActivateValidation()
{            
    var link = $(this).attr('href');
	$('body').addClass('margin-dialog');
    
    BootstrapDialog.show({
            title: '<h3>Port</h3>', // modal title
            message: '<h4>Are you sure you want to de-activate this port?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deleteValidation()
{            
    var link = $(this).attr('href');
	$('body').addClass('margin-dialog');
    
    BootstrapDialog.show({
            title: '<h3>Port</h3>', // modal title
            message: '<h4>Are you sure you want to delete this port?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}