/*
|--------------------------------------------------------------------------
| jQuery Tools for Padala Cargo Rate
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                jQuery(document).on('click', '.btn-delete-padala-cargo-rate', deleteValidation);
                jQuery(document).on('click', '.btn-activate-padala-cargo-rate', activateValidation);
                jQuery(document).on('click', '.btn-deactivate-padala-cargo-rate', deActivateValidation);
                jQuery(document).on('change', 'select[name=origin_id], select[name=destination_id]', checkPorts);
                jQuery(document).on('click', '#btn-submit', checkPorts);
        }
);

function checkPorts()
{
    var origin_id = $('select[name=origin_id] option:selected').val();
    var destination_id = $('select[name=destination_id] option:selected').val();

    if (origin_id.length != 0 || destination_id.length != 0) {
       if ($('select[name=origin_id] option:selected').val() == $('select[name=destination_id] option:selected').val()) {

            // Add Body
            $('body').addClass('margin-dialog');   
            
            BootstrapDialog.show({
                title: '<h4>Padala Cargo Rate</h4>', // modal title
                message: '<h3>Origin and Destination should not be the same.</h3>', // modal body
                buttons: [{
                    label: 'Ok',
                    cssClass: 'btn oj-button',
                    action: function(dialog) {
                    dialog.close(); // close modal
                    }
                }]
            });
                
            return false;
        }
    }
}

function activateValidation()
{            
    var link = $(this).attr('href');
    
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Padala Cargo Rate</h3>', // modal title
            message: '<h4>Are you sure you want to re-activate padala cargo rate?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deActivateValidation()
{            
    var link = $(this).attr('href');
    
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Padala Cargo Rate</h3>', // modal title
            message: '<h4>Are you sure you want to de-activate padala cargo rate?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}

function deleteValidation()
{            
    var link = $(this).attr('href');
    
    // Add Body
    $('body').addClass('margin-dialog');   
    
    BootstrapDialog.show({
            title: '<h3>Padala Cargo Rate</h3>', // modal title
            message: '<h4>Are you sure you want to delete padala cargo rate?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
        });
        
    return false;
}