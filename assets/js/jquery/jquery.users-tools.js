/*
|--------------------------------------------------------------------------
| jQuery Tools for user profile
|--------------------------------------------------------------------------
*/

jQuery(function()
	{
		jQuery(document).on('click', '.unlock-user', unlockValidation);
		jQuery(document).on('click', '.lock-user', lockValidation);
                jQuery(document).on('click', '.activate-user', activateValidation);
		jQuery(document).on('click', '.de-activate-user', deActivateValidation);
	}
);

function lockValidation()
{            
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>User Accounts</h3>', // modal title
			message: '<h4>Are you sure you want to lock user?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        window.location.replace(link);
				}
			}]
		});
        
    return false;
}

function unlockValidation()
{            
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>User Accounts</h3>', // modal title
			message: '<h4>Are you sure you want to unlock user?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        window.location.replace(link);
				}
			}]
		});
        
    return false;
}

function activateValidation()
{            
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>User Accounts</h3>', // modal title
			message: '<h4>Are you sure you want to re-activate user?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        window.location.replace(link);
				}
			}]
		});
        
    return false;
}

function deActivateValidation()
{            
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>User Accounts</h3>', // modal title
			message: '<h4>Are you sure you want to de-activate user?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        window.location.replace(link);
				}
			}]
		});
        
    return false;
}
