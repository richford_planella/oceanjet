$(function(){

    $(document).ready(function(){
        if(is_swap!=0)
            table_stats_seats();
    });

    //set Origin and Destination
    //Return list of sub voyages drop down list START
    $(document).on("change", "#origin_id, #destination_id", function () {
        //Get Origin and destination IDs
        var origin_id       = $('#origin_id').val();
        var destination_id  = $('#destination_id').val();

        //If same origin and destination add warning message
        if(origin_id==destination_id) {
            $('#myConfirmationModalLabel').html('Invalid origin and destination. Please try again.');
            return $('#confirmation-modal').modal('show');
        }

        //Reset Fields
        resetVoyageDetails();

        //Fetch list of legs
        $.ajax({
            type: 'post',
            async: true,
            dataType: 'json',
            url: adminURL + 'passenger_baggage_checkin/ajax',
            data: "module=subvoyage&origin_id=" + origin_id+'&destination_id='+destination_id,
            success: function (data) {
                //Print list of legs

                $("#voyage_management_id").html(data.option);
                $('#voyage_management_id').selectpicker('refresh');
            }
        });

    });
    //Return list of sub voyages drop down list END

    //Sub voyage Details start
    $(document).on("change", "#voyage_management_id", function() {

        checkSelectedsubvoyages();
        var voyage_management_id = $("#voyage_management_id").val();
        var origin_id            = $("#origin_id").val();
        var destination_id       = $("#destination_id").val();

        //Reset Fields
        resetVoyageDetails();

        $.ajax({
            type        :   'post',
            async       :   true,
            dataType    :   'json',
            url         :   adminURL + 'passenger_baggage_checkin/ajax',
            data        :   "module=details&voyage_management_id=" + voyage_management_id+"&origin_id="+origin_id+"&destination_id="+destination_id,
            success     :   function(data) {
                console.log(data);
                $("#departure_date").val(data.subvoyage.details.departure_date);
                $("#select_voyages").val(data.subvoyage.details.voyage);
                $("#origin").val(data.subvoyage.details.port_origin);
                $("#destination").val(data.subvoyage.details.port_destination);
                $("#etd").val(data.subvoyage.details.ETD);
                $("#eta").val(data.subvoyage.details.ETA);
                $("#vessel").val(data.subvoyage.details.vessel_code);
                $("#subvoyage_status_text").val(data.subvoyage.details.description);
                $("#subvoyage_status_id").val(data.subvoyage.details.subvoyage_management_status_id);
                $("#vessel_id").val(data.subvoyage.details.vessel_id);

                //If voyage has been swap change Vessel to new Vessel
                if(data.subvoyage.details.is_swap==1){
                    $(".is_swap").html('New Vessel');
                    $(".disembark-button").show();
                }else{
                    $(".is_swap").html('Vessel');
                    $(".disembark-button").hide();
                }

                //Print attended baggage lists
                //Add List of baggage based on Voyage ID start
                $("#id_attended_baggage").html(data.attended_baggage_option);
                //Add List of baggage based on Voyage ID end

                //View Voyage Details
                table_stats_seats();
            }
        });

    });
    //Sub voyage Details end

    //Get user details as Agent type
    // Passenger Checkin Rate
    $(document).on("keyup", "#ticket_no", function () {
        // Excess Baggage ID
        var ticket_no = $("#ticket_no").val();
        $("#check-in-messages").html('');

        $.ajax({
            type: 'post',
            async: true,
            dataType: 'json',
            url: adminURL + 'passenger_baggage_checkin/ajax',
            data: "module=passenger_details&ticket_no=" + ticket_no,
            success: function (data) {

                if (!jQuery.isEmptyObject(data)) {
                    // Add validation for valid ticket number
                    // Voyage code of booking Vs. Voyage code selected
                    if(data.details.subvoyage_management_id!=$('#voyage_management_id').val()) {
                        $('.confirm-button').hide();
                        return $("#check-in-messages").html('Invalid voyage, cannot proceed to check-in. Please try again.');
                    }

                    if($('#vessel_seats').val()=='') {
                        $('.confirm-button').hide();
                        return $("#check-in-messages").html('No selected seat. Please try again.');
                    }

                    if($('#accomodation_id').val()!=data.details.accommodation_id) {
                        $('.confirm-button').hide();
                        return $("#check-in-messages").html('Invalid accomodation type, choose another seat. Please try again.');
                    }

                    if(data.details.booking_status_id==8) {
                        $('.confirm-button').hide();
                        return $("#check-in-messages").html('Passenger: '+data.details.passenger_name+' already check-in. Please try again.');
                    }

                    //If there are no errors
                    $('.confirm-button').show();
                    $("#check-in-messages").html('Passenger name: '+data.details.passenger_name)
                    $("#booking_id").val(data.details.booking_id);
                    $("#transaction_ref").val(data.details.transaction_ref);
                    $("#id_number_required").val(data.details.id_number);
                    $("#id_number").val(data.details.id_number);
                } else {
                    $("#booking_id").val('');
                    $("#transaction_ref").val('');
                    $("#id_number_required").val('');
                    $("#id_number").val('');
                    $('.confirm-button').hide();
                    $('.print-boarding-pass-button').hide();
                    $("#check-in-messages").html('Invalid ticket number, no match found. Please try again.');
                }
            },
        });
    }); // Excess Baggage ID
    //Get user details as Agent type


    //Print boarding pass start
    $(document).on("click", ".print-boarding-pass-button", function() {
        var ticket_no = $('#ticket_no').val();
        var voyage_management_id = $('#voyage_management_id').val();
        var url = adminURL + 'passenger_checkin/printBoardingPass/'+ticket_no+'/'+voyage_management_id;

        window.open(url, '_blank');

    });
    //Print boarding pass end

    //Print passenger manifest start
    $(document).on("click", ".print-passenger-baggage-manifest", function() {
        checkSelectedsubvoyages();

        var voyage_management_id = $('#voyage_management_id').val();
        var vessel_id = $('#vessel_id').val();
        var url = adminURL + 'passenger_checkin/printPassengerManifest/'+voyage_management_id+'/'+vessel_id;

        window.open(url, '_blank');

    });
    //Print passenger manifest end

    //Print disembarked start
    $(document).on("click", ".disembark-button", function() {
        checkSelectedsubvoyages();

        var voyage_management_id = $('#voyage_management_id').val();
        var url = adminURL + 'passenger_checkin/printDisembarkedPassenger/'+voyage_management_id;

        window.open(url, '_blank');

    });
    //Print disembarked manifest end


    //Swap Vessel start
    $(document).on("click", ".swap-vessel", function() {
        checkSelectedsubvoyages();

        var voyage_management_id = $('#voyage_management_id').val();
        var url = adminURL + 'passenger_checkin/swapVessel/'+voyage_management_id;

        window.location.href = url;

    });
    //Swap Vessel end


    //Print passenger manifest start
    $(document).on("click", ".passenger-baggage-check-in", function() {
        checkSelectedsubvoyages();

        var voyage_management_id = $('#voyage_management_id').val();
        var url = adminURL + 'passenger_baggage_checkin/add/'+voyage_management_id;

        window.location.href = url;

    });
    //Print passenger manifest end

    //Hide confirm and print boarding pass buttons start
    $(document).ready(function(){
        $('.confirm-button').hide();
        $('.print-boarding-pass-button').hide();
    });
    //Hide confirm and print boarding pass buttons end

    // Passenger Check-in Save Data Start
    $(document).on("click", ".confirm-button", function () {
        var voyage_management_id = $("#voyage_management_id").val();
        var reason               = $("#reason_id").val();
        var vessel_id            = $("#vessel_id").val();
        var data  = 'module=save_checkin&'+$('#check-in-form').serialize()+'&voyage_management_id='+ voyage_management_id+'&reason='+reason+'&vessel_id='+vessel_id;

        //If Id number is required & no value
        //User must set If ID is required and no ID was provided by the passenger,
        // the check-in clerk may override confirmation but shall be required to indicate a reason.
        if($('#id_number_required').val()!= '' && $('#id_number').val()=='') {
            return $('#reason-for-not-having-id').modal('show');
        }

        //Check if passenger check-in
        if($('#subvoyage_status_id').val()!=9 && $('#subvoyage_status_id').val()!=10){
            return $("#check-in-messages").html('Change voyage status to open for check-in or open for chance passenger. Please try again.');
        }

        $.ajax({
            type: 'post',
            async: true,
            dataType: 'json',
            url: adminURL + 'passenger_checkin/ajax',
            data: data,
            success: function (data) {
                if(data.msg=='ok') {
                    table_stats_seats();

                    $('.confirm-button').hide();
                    $('.print-boarding-pass-button').show();
                    $("#check-in-messages").html('Passenger successfully check-in, you can now print boarding pass.');
                }
            },
        });
    }); // Excess Baggage ID
    // Passenger Check-in Save Data end


    //Update sub voyage status
    $(document).on("click", ".update-sub-voyage-status", function() {

        checkSelectedsubvoyages();

        var stat = $(this).attr('data-status');
        var msg = '';
        //Switch according based on status
        switch (stat){
            case '9':
                //Open for check-in
                msg = 'Voyage is now open for check-in.';
                updateSubVoyageStatus(stat,'','',msg);
                break;
            case '2':
                //Departed on time
                msg = 'Voyage is departed on time.';
                updateSubVoyageStatus(stat,'','',msg);
                break;
            case '3':
                //Departed but delayed
                $('#delayed-voyage').modal('show');
                break;
            case '4':
                //Undepart
                //Check if current Voyage is undeparted
                checkLegDeparted();

                msg = 'Voyage is undeparted.';
                if($('#subvoyage_status_id').val()=="2" || $('#subvoyage_status_id').val()=="3")
                    updateSubVoyageStatus(stat,'','',msg);
                break;
            case '6':
                //Canceled
                $('#canceled-voyage').modal('show');
                break;
            case '10':
                //Open for check-in
                msg = 'Voyage is now open for chance passenger.';
                updateSubVoyageStatus(stat,'','',msg);
                break;
        }
    });
    //Update sub voyage status

    //Tag voyage as delayed
        $(document).on("click", ".button-tag-voyage-delayed", function() {
            var stat = 3;
            var delayed_time = $('#delayed_time').val();
            var msg = "Voyage tagged as delayed."
            //Update status
            updateSubVoyageStatus(stat,delayed_time,'',msg);
            //Close Modal
            $('#delayed-voyage').modal('hide');
        });
    //Tag voyage as delayed

    //Tag voyage as canceled
        $(document).on("click", ".button-tag-voyage-canceled", function() {
            var stat = 6;
            var reason = $('#reason-for-cancelling').val();
            var msg = "Voyage is canceled."
            //Update status
            updateSubVoyageStatus(stat,'',reason,msg);
            //Close Modal
            $('#canceled-voyage').modal('hide');
        });
    //Tag voyage as canceled

});

//Check if we have selected voyages
//check if we have voyage selected
function checkSelectedsubvoyages(){
    if($('#voyage_management_id').val()=='' || $('#voyage_management_id').val()==0){
        $('#myConfirmationModalLabel').html("No voyage selected. Please try again."); //Set available users
        $('#confirmation-modal').modal('show');

        throw new Error("No voyage selected. Please try again.");
    }
}


//check if we Leg is Undeparted
//Departed on time
//Departed but late
function checkLegDeparted(){
    if($('#subvoyage_status_id').val()!="2" || $('#subvoyage_status_id').val()!="3") {
        $('#myConfirmationModalLabel').html('Cannot set voyage to undepart, make sure that voyage is departed. Please try again.');
        return $('#confirmation-modal').modal('show');

    }
}
//check if we Leg is Undeparted


//Sub Voyage Status Update Start
function updateSubVoyageStatus(status, delayed_time, canceled_reason,msg) {
    var voyage_management_id = $('#voyage_management_id').val();

    $.ajax({
        type        :   'post',
        async       :   true,
        dateType    :   'json',
        url         :   adminURL + "passenger_baggage_checkin/ajax",
        data        :   'module=update_subvoyage_status&status='+status+"&voyage_management_id="+voyage_management_id+"&delayed_time="+delayed_time+"&canceled_reason="+canceled_reason,
        success     :   function(data) {
            var data = JSON.parse(data);
            //Update Leg Status after the update
            $("#subvoyage_status_text").val(data.subvoyage.details.description);
            $("#subvoyage_status_id").val(data.subvoyage.details.subvoyage_management_status_id);

            //Open Modal
            $('#myConfirmationModalLabel').html(msg);
            $('#confirmation-modal').modal('show');
        }
    });
}
//Sub Voyage Status Update End

function table_stats_seats()
{

    var subvoyage_management_id = $('#voyage_management_id').val();
    var subvoyage_status_id = $('#subvoyage_status_id').val();
    var vessel_id = $('#vessel_id').val();

    $(".table-details").show();
    $(".seatplan-details").show();

    $.ajax({
        type        :   'post',
        async       :   true,
        dateType    :   'json',
        url         :   adminURL + 'passenger_checkin/ajax',
        data        :   'module=table&subvoyage_management_id='+subvoyage_management_id+'&subvoyage_status_id='+subvoyage_status_id+'&vessel_id='+vessel_id,
        success     :   function(data) {
            var parse_data = JSON.parse(data);
            $('#vessel-details').html(parse_data.details);

            var vessel_id               = $('#vessel_id').val();
            var voyage_management_id    = $('#voyage_management_id').val();
            $.ajax({
                type        :   'post',
                async       :   true,
                dateType    :   'json',
                url         :   adminURL + 'passenger_checkin/ajax',
                data        :   'module=seatplan&vessel_id='+vessel_id+'&voyage_management_id='+voyage_management_id,
                success     :   function(data) {
                    var data = JSON.parse(data);
                    console.log(data.seat_plan);
                    var seat='';
                    //Draw seat plan
                    $.each(data.seat_plan, function(index, value){
                        seat += '<tr>';
                        $.each(value, function(i, v){

                            //Add identifier Vacant or Occupied
                            var seat_stat = "<span class='box-status is-vacant'></span>";
                            if(v.booking_id!=0)
                                seat_stat = "<span class='box-status is-occupied'></span>";

                            //If Seat is N/A
                            if(v.value=='N/A')
                                seat_stat = '';

                            seat += "<td style='background-color:"+v.color+"' class='vessel-seat' data-booking-id='"+v.booking_id+"' data-vessel-seats='"+v.value+"' data-accomodation='"+ v.accommodation +"' >"+ seat_stat + v.value  +"</td>";
                        });
                        seat += '</tr>';
                    });

                    //Set vessel seats
                    $('#arrowkey-table').html(seat);

                    //Initialize seat details functions
                    $('.vessel-seat').on('click', seatDetails);

                    //Arrow navigation on table
                    $('#arrowkey-table > tbody td').keynavigator({
                        useCache: false,
                        activateOn: 'click',
                        parentFocusOn: 'mouseover',
                        onAfterActive: function($el){
                            seatDetails2($el)
                        }
                    });
                }
            });
        },
        beforeSend: function() {
            $('.accommodation-table, .vessel-seats-table').html(animation_logo);  // show loading indicator
        },
    });
}

//View seat details with Parameter
function seatDetails2(p)
{
    var data_accomodation = $(p).attr('data-accomodation');
    var vessel_seats = $(p).attr('data-vessel-seats');
    var booking_id = $(p).attr('data-booking-id');

    seat_business_logic(data_accomodation,vessel_seats,booking_id);
}

//Focus on seat using this as pointer
function seatDetails()
{
    var data_accomodation = $(this).attr('data-accomodation');
    var vessel_seats = $(this).attr('data-vessel-seats');
    var booking_id = $(this).attr('data-booking-id');

    seat_business_logic(data_accomodation,vessel_seats,booking_id);
}

//Seat details business logic
function seat_business_logic(data_accomodation,vessel_seats,booking_id)
{
    //If the current seat id not available
    if(data_accomodation!='0'){
        $('#vessel_seats').val(vessel_seats);
        $('#accomodation_id').val(data_accomodation);

        //If there's a passenger within the seat
        //Get passenger details
        if(booking_id!=0){
            $.ajax({
                type: 'post',
                async: true,
                dataType: 'json',
                url: adminURL + 'passenger_checkin/ajax',
                data: "module=passenger_details&booking_id=" + booking_id,
                success: function (data) {
                    console.log(data);
                    $('#ticket_no').val(data.details.ticket_no);
                    $('#id_number').val(data.details.passenger_valid_id_number);
                    $('#transaction_ref').val(data.details.transaction_ref);
                    $('#booking_id').val(data.details.booking_id);
                    $('#transaction_ref').val(data.details.transaction_ref);
                    //Set if ID  number is required
                    //If this has a value then make this required
                    $('#id_number_required').val(data.details.transaction_ref);

                    //Set passenger name and success message
                    var pname = data.details.lastname +', '+ data.details.firstname +' '+ data.details.middlename.substr(0,1)+'.';
                    $('#check-in-messages').html('Seat assigned to: '+pname);
                    $('.confirm-button').hide();
                    $('.print-boarding-pass-button').show();
                },
                beforeSend: function() {
                    $('#check-in-messages').html('Please wait...');
                }
            });
        }else{
            $('#booking_id').val('');
            $('#transaction_ref').val('');
            $('#id_number_required').val('');
            $('#ticket_no').val('');
            $('#id_number').val('');
            $('#check-in-messages').html('Available seat.');
            $('.confirm-button').hide();
            $('.print-boarding-pass-button').hide();
        }

    }else{
        $('#vessel_seats').val('');
        $('#accomodation_id').val('');
        $('#booking_id').val('');
        $('#transaction_ref').val('');
        $('#id_number_required').val('');
        $('#ticket_no').val('');
        $('#id_number').val('');
        $('#check-in-messages').html('Seat not available.');
        $('.confirm-button').hide();
        $('.print-boarding-pass-button').hide();
    }
}

//Set fields to be blank reset Voyage Fields
function resetVoyageDetails()
{
    $("#departure_date").val('');
    $("#select_voyages").val('');
    $("#origin").val('');
    $("#destination").val('');
    $("#etd").val('');
    $("#eta").val('');
    $("#vessel").val('');
    $("#subvoyage_status_text").val('');
    $("#subvoyage_status_id").val('');

    //Print attended baggage lists
    //Add List of baggage based on Voyage ID start
    $("#id_attended_baggage").val("");
}