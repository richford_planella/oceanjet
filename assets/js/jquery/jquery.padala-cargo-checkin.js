/*
|--------------------------------------------------------------------------
| jQuery Tools for Padala Cargo Check - In
|--------------------------------------------------------------------------
*/

jQuery(function()
        {
                //hideButtons();
                checkPorts();
                jQuery(document).on('change', 'select[name=origin_id], select[name=destination_id]', checkPorts);
                jQuery(document).on('click', '#btn-add-cargo', addCargo);
                jQuery(document).on('click', '#btn-submit-add-cargo', addToTable);
                jQuery(document).on('change keyup', '#padala-cargo-rates', fillFields);
                jQuery(document).on('change keyup', '#input-quantity', computeAmount);
                jQuery(document).on('click', '.btn-delete-cargo', deleteCargo);
                jQuery(document).on('click', '#btn-padala-cargo-list', viewPadalaCargoList);
                //jQuery(document).on('click', '#btn-view', fillTripDetails);
                jQuery(document).on('change keyup', 'select[name=subvoyage_management_id]', fillTripDetails);
                //jQuery(document).on('change keyup', 'select[name=subvoyage_management_id]', toggleButtons);
                jQuery(document).on('click', '#btn-check-in', removeDisable);


                /* Voyage status */
                jQuery(document).on('click', '.btn-voyage-status', updateStatus);
        }
);

function updateStatus() 
{
    var link = "";

    var voyage_status = $(this)[0].innerHTML;
    var status_id = 0;
    var title = "<h3>Padala Cargo Check-In</h3>";
    var message = "";
    var subvoyage_management_id = $("select[name=subvoyage_management_id] option:selected").val();
    if (voyage_status == "Open for Check - In") {
        status_id = 9;
        message = "<h4>Voyage is now open for check-in.</h4>";
    } else if (voyage_status == "Depart") {
        status_id = 2;
        message = "<h4>Voyage is departed on time.</h4>";
    } else if (voyage_status == "Undepart") {
        status_id = 4;
        message = "<h4>Voyage is undeparted.</h4>";
    } else if (voyage_status == "Delayed") {
        status_id = 3;
        //message = "Tag voyage status as “delayed”, input no. of minutes delayed.";
        message +=  "<label class=\"control-label\">No. of minutes late.</label>";
        message += "<div class=\"btn-group bootstrap-select form-control\">";
        message += "<input class=\"form-control\" id=\"delayed_time\" type=\"text\" placeholder=\"Input number of minutes delayed\" onkeypress=\"numeric_validation(event)\" required>";
        message += "</div>";
        message += "<span class=\"input-notes-bottom\" id=\"modal-error\"></span>";
    } else if (voyage_status == "Cancel") {
        status_id = 6;
        //message = "Cancel voyage, input reason for cancelling.";
        message +=  "<label class=\"control-label\">Reason for cancelling.</label>";
        message += "<div class=\"btn-group bootstrap-select form-control\">";
        message += "<input class=\"form-control\" id=\"reason-for-cancelling\" type=\"text\" placeholder=\"Input reason for cancelling\" required>";
        message += "</div>";
        message += "<span class=\"input-notes-bottom\" id=\"modal-error\"></span>";
    }

    if (status_id != 0 && subvoyage_management_id) {
        $(".modal-error").val("");

        if (status_id == 3 || status_id == 6) { // if delayed or cancelled
            // Add Body
            $('body').addClass('margin-dialog');   
            BootstrapDialog.show({
                title: title, // modal title
                message: message, // modal body
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn oj-button',
                    action: function(dialog) {
                       dialog.close(); // close modal
                    }
                }, {
                    label: 'Save',
                    cssClass: 'btn oj-button',
                    action: function(dialog) {
                        var remarks = $("#reason-for-cancelling").val();
                        var delayed_time = $("#delayed_time").val();
                        var modal_error_message = "";

                        if (remarks == "" || delayed_time == ""){
                            if (status_id == 3)
                                modal_error_message = "Please supply number of minutes delayed";
                            else
                                modal_error_message = "Please supply reason for cancelling";
                        }

                        if  (!modal_error_message) {
                            var post_data = {status_id : status_id, subvoyage_management_id : subvoyage_management_id, remarks : remarks, delayed_time : delayed_time };

                            $.post(adminURL+"padala_cargo_checkin/updateSchedStatus", post_data, function(data){
                                dialog.close(); // close modal
                                checkPorts();
                                fillTripDetails();
                            });
                        } else {
                            $("#modal-error").text(modal_error_message);
                        }
                    }
                }]
            });
        } else {
            if (status_id == 4){ //Undepart
                if ($("#trip-subvoyage-management-status").val() != "Departed on time") {
                    status_id = 0; //You cannot undepart voyage if the status is not departed
                }
            }
            var post_data = {status_id : status_id, subvoyage_management_id : subvoyage_management_id, remarks : "", delayed_time : 0 };
            $.post(adminURL+"padala_cargo_checkin/updateSchedStatus", post_data, function(data){
                var data = JSON.parse(data);

                if (!data.result)
                    message = "Could not undepart non-Departed voyages";


                // Add Body
                $('body').addClass('margin-dialog');   
                BootstrapDialog.show({
                    title: title, // modal title
                    message: message, // modal body
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn oj-button',
                        action: function(dialog) {
                           dialog.close(); // close modal
                           checkPorts();
                           fillTripDetails();
                        }
                    }]
                });
            });
        }
    }
    return false;
}


function removeDisable()
{
    if ($("#trip-subvoyage-management-status").val() != "Open for Check-in") {
        // Add Body
        $('body').addClass('margin-dialog');   
        BootstrapDialog.show({
            title: "<h3>Padala Cargo Check-In</h3>",
            message: '<h4>Voyage is not open for Check-In</h4>', // modal body
            buttons: [{
                label: 'Ok',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }]
        });

        return false;
    }
    else {
        $("select").prop("disabled", false);
    }
}

function toggleButtons()
{
    if ($("option:selected", this).val()) {
        showButtons();
    } else {
        hideButtons();
        $("#div-trip-details > input").val();
    }
}

function fillTripDetails()
{
    var subvoyage_management = $("select[name=subvoyage_management_id] option:selected");

    if ($(subvoyage_management).val()) {
        var voyage                      =   $(subvoyage_management).attr("voyage");
        var departure_date              =   $(subvoyage_management).attr("departure_date");
        var origin                      =   $(subvoyage_management).attr("origin");
        var destination                 =   $(subvoyage_management).attr("destination");
        var ETD                         =   $(subvoyage_management).attr("ETD");
        var ETA                         =   $(subvoyage_management).attr("ETA");
        var vessel                      =   $(subvoyage_management).attr("vessel");
        var subvoyage_management_status =   $(subvoyage_management).attr("subvoyage_management_status");

        $("#trip-voyage-code").val(voyage);
        $("#trip-departure-date").val(departure_date);
        $("#trip-origin").val(origin);
        $("#trip-destination").val(destination);
        $("#trip-ETD").val(ETD);
        $("#trip-ETA").val(ETA);
        $("#trip-vessel").val(vessel);
        $("#trip-subvoyage-management-status").val(subvoyage_management_status);
        $("#subvoyage_management_id").val($(subvoyage_management).val());
    } else {
        $("#div-trip-details input").val("");
        $("#trip-subvoyage-management-status").val("");
    }
}

function viewPadalaCargoList()
{
    var subvoyage_management_id = $("select[name=subvoyage_management_id] option:selected").val();

    if (subvoyage_management_id) {
        var post_data = {subvoyage_management_id : subvoyage_management_id };

        $.post(adminURL+"padala_cargo_checkin/getPadalaCargoList", post_data, function(data){
            var parsed_data = JSON.parse(data)["padala_cargo_checkin"];

            var table = $("#padala-cargo-list").DataTable();

            table.clear().draw();

            for(x in parsed_data) {
                var reference_no = parsed_data[x].reference_no;
                var sender_name = parsed_data[x].sender_first_name + " " + parsed_data[x].sender_last_name;
                var recipient_name = parsed_data[x].recipient_first_name + " " + parsed_data[x].recipient_last_name;
                var sender_contact_no = parsed_data[x].sender_contact_no;
                var particulars = parsed_data[x].particulars;
                var total_amount = parsed_data[x].total_amount;

                table.row.add([
                    reference_no,
                    sender_name,
                    recipient_name,
                    sender_contact_no,
                    particulars,
                    total_amount,
                    "<a class=\"btn-print-bill-of-lading\" href=\"\">Re-print Bill of Lading</a>",
                ]).draw();
            }

            $("#modal-table-v1").modal("show");
        });
    } else {
        // Add Body
        $('body').addClass('margin-dialog');   
        BootstrapDialog.show({
            title: '<h3>Padala Cargo Check-In</h3>', // modal title
            message: '<h4>Please select a voyage.</h4>', // modal body
            buttons: [{
                label: 'Ok',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                dialog.close(); // close modal
                }
            }]
        });
    }
    return false;
}

function deleteCargo()
{
    var tdRow = $(this);
    // Add Body
    $('body').addClass('margin-dialog');   
    BootstrapDialog.show({
            title: '<h3>Padala Cargo Rate</h3>', // modal title
            message: '<h4>Are you sure you want to delete padala cargo?</h4>', // modal body
            buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                   dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function(dialog) {
                    var table = $('#main_table').DataTable();
                    table
                        .row( $(tdRow).parents('tr') )
                        .remove()
                        .draw();

                    if (!table.data().count()) {
                        //enable origin and destination selections
                        $('select[name=origin_id]').prop('disabled', false);
                        $('select[name=destination_id]').prop('disabled', false);
                        $('select[name=origin_id]').selectpicker('refresh');
                        $('select[name=destination_id]').selectpicker('refresh');
                    }                    

                    //compute total amount
                    $('#total-amount').val(computeTotal());

                    dialog.close(); // close modal
                }
            }]
        });
    return false;
}

function round(x) {
    return parseFloat(Math.round(x * 100) / 100).toFixed(2);
}

function computeTotal()
{
    var sum = 0;
    var table = $("#main_table").DataTable();
    table.column(4).every(function(){
        $(this.data()).each(function(i, v) {
            sum += Number(v);
        });
    });
    return round(sum);
}

function computeAmount()
{
    var quantity = $(this).val();
    var amount_value = $('#padala-cargo-rates option:selected').attr("amount");

    $('#input-amount').val(quantity * amount_value);
}

function fillFields()
{
    var quantity = $('#input-quantity').val();
    var uom = $('option:selected', this).attr("uom");
    var amount_value = $('option:selected', this).attr("amount");

    $('#input-uom').val(uom);
    $('#input-amount').val(quantity*amount_value);
}

function addToTable()
{
    var particular = $('#input-particular').val();
    var quantity = $('#input-quantity').val();
    var padala_cargo_rate = $('#padala-cargo-rates').val();
    var padala_cargo_rate_text = $('#padala-cargo-rates option:selected').text();
    var amount = $('#input-amount').val();
    var uom = $('#input-uom').val();

    if (particular.trim().length == 0  || quantity == "" || quantity == "0" || !padala_cargo_rate) {
        //trigger html required validation
    } else {
       var table = $('#main_table').DataTable();
        table.row.add([
                particular + "<input type='hidden' name='particular[]' value='"+particular+"'/>",
                padala_cargo_rate_text + "<input type='hidden' name='padala_cargo[]' value='"+padala_cargo_rate+"'/>",
                quantity + "<input type='hidden' name='quantity[]' value='"+quantity+"'/>",
                uom,
                amount,
                "<a class=\"btn-delete-cargo\" href=\"\">Delete</a>",
            ]).draw();


        //Disable origin and destination selections
        $('select[name=origin_id]').prop('disabled', true);
        $('select[name=destination_id]').prop('disabled', true);
        $('select[name=origin_id]').selectpicker('refresh');
        $('select[name=destination_id]').selectpicker('refresh');

        //compute total amount
        $('#total-amount').val(computeTotal());

        return false;
    }   
}

function addCargo()
{
    $("#div-padala-cargo-details").toggle();
    if ($("#div-padala-cargo-details").is(":visible")) {
        //if visible add required attribute to elements
        $("#input-particular").prop("required", true);
        $("#padala-cargo-rates").prop("required", true);
        $("#input-quantity").prop("required", true);
    } else {
        //if not remove required attribute from elements
        $("#input-particular").removeAttr("required");
        $("#padala-cargo-rates").removeAttr("required");
        $("#input-quantity").removeAttr("required");
    }
}

function checkPorts()
{
    var origin_id = $('select[name=origin_id] option:selected').val();
    var destination_id = $('select[name=destination_id] option:selected').val();

    if (origin_id.length != 0 && destination_id.length != 0) {
       if ($('select[name=origin_id] option:selected').val() == $('select[name=destination_id] option:selected').val()) {
            // Add Body
            $('body').addClass('margin-dialog');   
            BootstrapDialog.show({
                title: '<h3>Padala Cargo Check-In</h3>', // modal title
                message: '<h4>Origin and Destination should not be the same.</h4>', // modal body
                buttons: [{
                    label: 'Ok',
                    cssClass: 'btn oj-button',
                    action: function(dialog) {
                    dialog.close(); // close modal
                    }
                }]
            });
        } else {
            var post_data = {origin_id : origin_id, destination_id : destination_id };

            $.post(adminURL+"padala_cargo_checkin/getVoyages", post_data, function(data){
                var parsed_data = JSON.parse(data);
                var del = " | ";
                var html = "";

                var subvoyage_management_id = $("#subvoyage_management_id").val();

                $("select[name=subvoyage_management_id] option:not(:first)").remove();
                for (x in parsed_data['subvoyages']) {
                    var voyage_code = parsed_data['subvoyages'][x].voyage_code;
                    var origin = parsed_data['subvoyages'][x].port_origin;
                    var destination = parsed_data['subvoyages'][x].port_destination;
                    var departure_date = parsed_data['subvoyages'][x].dept_date;
                    var departure_date_f = moment(departure_date).format('MMMM D, YYYY - ddd');
                    var ETD = parsed_data['subvoyages'][x].ETD;
                    var ETA = parsed_data['subvoyages'][x].ETA;
                    var vessel = parsed_data['subvoyages'][x].vessel;
                    var id_subvoyage_management = parsed_data['subvoyages'][x].id_subvoyage_management;
                    var subvoyage_management_status = parsed_data['subvoyages'][x].subvoyage_management_status;

                    var subvoyage_details = voyage_code + del + origin + " - " + destination + del + departure_date_f + del + ETD + del + ETA;

                    if (id_subvoyage_management == subvoyage_management_id)
                        html += "<option value="+id_subvoyage_management+" voyage=\""+voyage_code+"\" departure_date=\""+departure_date+"\" origin=\""+origin+"\" destination=\""+destination+"\" vessel=\""+vessel+"\" ETA=\""+ETA+"\" ETD=\""+ETD+"\" subvoyage_management_status=\""+subvoyage_management_status+"\" selected>"+subvoyage_details+"</option>";
                    else
                        html += "<option value="+id_subvoyage_management+" voyage=\""+voyage_code+"\" departure_date=\""+departure_date+"\" origin=\""+origin+"\" destination=\""+destination+"\" vessel=\""+vessel+"\" ETA=\""+ETA+"\" ETD=\""+ETD+"\" subvoyage_management_status=\""+subvoyage_management_status+"\">"+subvoyage_details+"</option>";
                }
                $("select[name=subvoyage_management_id]").append(html).selectpicker("refresh");

                var html = "";
                $("#padala-cargo-rates option:not(:first)").remove();
                for (x in parsed_data['padala_cargo_rates']) {
                    var id_padala_cargo = parsed_data['padala_cargo_rates'][x].id_padala_cargo;
                    var uom = parsed_data['padala_cargo_rates'][x].unit_of_measurement;
                    var amount = parsed_data['padala_cargo_rates'][x].amount;
                    var padala_cargo = parsed_data['padala_cargo_rates'][x].padala_cargo;

                    html += "<option value="+id_padala_cargo+" uom=\""+uom+"\" amount="+amount+">"+padala_cargo+"</option>";
                }
                $("#padala-cargo-rates").append(html).selectpicker("refresh");

                fillTripDetails(); //if there's selected option, fill trip details
            });
        }
    }

    return false;
}

function hideButtons()
{
    $("#btn-view").prop("disabled", true);
    $("#btn-padala-cargo-list").prop("disabled", true);
}

function showButtons()
{
    $("#btn-view").prop("disabled", false);
    $("#btn-padala-cargo-list").prop("disabled", false);
}