
$(function(){

	$( ".transfer-button" ).click(function() {
		var no_tickets = $(this).attr('data-available-tickets');
		var selected_user_id = $(this).attr('data-user-id');

		//Generate ticket range
		var select_no_tickets = '';
		var x;
		for(x=1;x<=no_tickets;x++)
			select_no_tickets += "<option value='"+ x +"'> "+ x +" </option>";

		$('#available_tickets_select_drop_down').html(select_no_tickets); //Set available tickets
        $('#available_tickets_select_drop_down').selectpicker('refresh');

		//Generate user agent lists
		var select_user_agents='';
		for(var i=0;i<available_agents.length;i++){
			var obj = available_agents[i];
				if(obj['id_user_account']!=selected_user_id)
					select_user_agents += "<option value='"+ obj['id_user_account'] +"'>"+ obj['firstname']+" "+ obj['middlename'] +" " + obj['lastname'] +"</option>";
		}

		$('#available_sales_agents_select_drop_down').html(select_user_agents); //Set available users
		$('#assigned_sales_agent').val(selected_user_id);
        $('#available_sales_agents_select_drop_down').selectpicker('refresh');


		//show modal
		$('#myModal').modal('show');

	});
});