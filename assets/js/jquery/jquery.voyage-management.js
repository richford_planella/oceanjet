/*
 |--------------------------------------------------------------------------
 | jQuery Tools for Voyage
 |--------------------------------------------------------------------------
 */

jQuery(function ()
{
    jQuery(document).on('click', '#block-btn', blockVoyageSchedule);
    jQuery(document).on('click', '#unblock-btn', unblockVoyageSchedule);
    jQuery(document).on('click', '#cancel-btn', cancelVoyageSchedule);
    jQuery(document).on('click', '.btn-delete-voyage', deleteVoyageSchedule);
    jQuery(document).on('change', '#voyages', changeVoyage);

    /*
     * 
     * Index
     * 
     */
    //Table Header filter
    $('#show-select-forms').change(function () {
        $('.forms-hide').hide();
        $('#' + $(this).val()).css('display', 'inline-block');
    });

    //Checkbox List Toggle
    if (!$(".checkbox-fields__check").is(":checked")) {
        $(".checkbox-fields .form-control, .checkbox-fields .checkbox-fields__radio").prop("disabled", true);
        $(".checkbox-fields .form-control, .checkbox-fields .checkbox-fields__radio").selectpicker('refresh');
    }

    $(".checkbox-fields__check").click(function () {
        if ($(this).is(":checked")) {
            $(this).parents('.checkbox-fields').find('.form-control, .checkbox-fields__radio').prop("disabled", false);
        } else {
            $(this).parents('.checkbox-fields').find('.form-control, .checkbox-fields__radio').prop("disabled", true);
        }
        $(this).parents('.checkbox-fields').find('.form-control, .checkbox-fields__radio').selectpicker('refresh');
    });


    //Checkbox toggle all
    $(".condition-check").click(function () {
        $(".oj-check").prop('checked', $(this).prop('checked'));
    });

    $('#check-all').click(function () {
        $('.oj-check').prop('checked', $(this).prop('checked'));
    });

    //Batch button 
    $('.oj-check, #check-all').click(function () {
        if ($('.oj-check').is(':checked')) {
            $('.batch-btn').removeAttr('disabled');
        } else {
            $('.batch-btn').attr('disabled', true);
        }
    })

    var table = $('#voyage-management-table').DataTable({
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
        "order": [[1, "asc"]],
        "dom": 'rt<"bottom"lp>'
    });

    $('#voyage_search').on('keyup', function () {
        table
                .search(this.value)
                .draw();
    });

    $('.filter-button').click(function () {
        var voyage_code = $('#filter-page option:selected').val();
        table.column(1).search(voyage_code).draw();
    });

    $("#date-from, #date-to").datetimepicker({
        format: "YYYY-MM-DD"
    });

    $("#date-from").on("dp.change", function (e) {
        $("#date-to").data("DateTimePicker").minDate(e.date);
        table.draw();
    });

    $("#date-to").on("dp.change", function (e) {
        table.draw();
    });
    
    $("#date_from, #date-to").datetimepicker({
        format: "YYYY-MM-DD"
    });

    $("#date_from").on("dp.change", function (e) {
        $("#date_to").data("DateTimePicker").minDate(e.date);
        table.draw();
    });

    $("#date_to").on("dp.change", function (e) {
        table.draw();
    });
    

    $("#show-select-forms").on("change", function (e) {
        //reset filters
        $("#date-from").val("");
        $("#date-to").val("");
        $("#filter-page").val("");
        table.column(1).search("").draw();
        table.draw();
    });
    
    $(".custom-datepicker").datetimepicker({
        format: "YYYY-MM-DD"
    });
}
);

function blockVoyageSchedule()
{
    var link = $(this).attr('href');

    var ids = [];

    $.each($("input[type=checkbox]:checked"), function (index, value) {
        ids.push($(this).val());
    });

    // Add Body
    $('body').addClass('margin-dialog');

    BootstrapDialog.show({
        title: '<h3>Voyage schedule</h3>', // modal title
        message: '<h4>Are you sure you want to block voyage schedule?</h4>', // modal body
        buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function (dialog) {
                    dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function (dialog) {
                    var post_data = {voyage_management_array: ids, status: 5}; //5 = Blocked

                    // ajax post/jQuery post code
                    $.post(link, post_data, function (data) {
                        var parsed_data = JSON.parse(data);
                        window.location.replace(parsed_data["url"]);
                    });
                }
            }]
    });

    return false;
}

function unblockVoyageSchedule()
{
    var link = $(this).attr('href');

    var ids = [];

    $.each($("input[type=checkbox]:checked"), function (index, value) {
        ids.push($(this).val());
    });

    // Add Body
    $('body').addClass('margin-dialog');

    BootstrapDialog.show({
        title: '<h3>Voyage schedule</h3>', // modal title
        message: '<h4>Are you sure you want to unblock voyage schedule?</h4>', // modal body
        buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function (dialog) {
                    dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function (dialog) {
                    var post_data = {voyage_management_array: ids, status: 7}; //7 = Unblocked

                    // ajax post/jQuery post code
                    $.post(link, post_data, function (data) {
                        var parsed_data = JSON.parse(data);
                        window.location.replace(parsed_data["url"]);
                    });
                }
            }]
    });

    return false;
}

function cancelVoyageSchedule()
{
    var link = $(this).attr('href');

    var ids = [];

    $.each($("input[type=checkbox]:checked"), function (index, value) {
        ids.push($(this).val());
    });

    // Add Body
    $('body').addClass('margin-dialog');

    BootstrapDialog.show({
        title: '<h3>Voyage schedule</h3>', // modal title
        message: '<h4>Are you sure you want to cancel voyage schedule?</h4>', // modal body
        buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function (dialog) {
                    dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function (dialog) {
                    var post_data = {voyage_management_array: ids, status: 6}; //6 = Cancelled

                    // ajax post/jQuery post code
                    $.post(link, post_data, function (data) {
                        var parsed_data = JSON.parse(data);
                        window.location.replace(parsed_data["url"]);
                    });
                }
            }]
    });

    return false;
}

function deleteVoyageSchedule()
{
    var link = $(this).attr('href');

    // Add Body
    $('body').addClass('margin-dialog');

    BootstrapDialog.show({
        title: '<h3>Voyage schedule</h3>', // modal title
        message: '<h4>Are you sure you want to delete voyage schedule?</h4>', // modal body
        buttons: [{
                label: 'No',
                cssClass: 'btn oj-button',
                action: function (dialog) {
                    dialog.close(); // close modal
                }
            }, {
                label: 'Yes',
                cssClass: 'btn oj-button',
                action: function (dialog) {
                    // ajax post/jQuery post code
                    window.location.replace(link);
                }
            }]
    });

    return false;
}

function changeVoyage()
{
    var post_data = {voyage_id: $(this).val()};
    $.post(adminURL + "voyage_management/subvoyages", post_data, function (data) {
        var parsed_data = JSON.parse(data);
        var leg_count = 1;

        var html = "";
        $(".oj-leg").children().remove();
        for (x in parsed_data["subvoyages"]) {
            var fromTime = moment(parsed_data["subvoyages"][x].ETD, 'hh:mm:ss');
            var toTime = moment(parsed_data["subvoyages"][x].ETA, 'hh:mm:ss');
            var timeDiff = toTime.diff(fromTime);
            var travelTime = moment.utc(timeDiff).format('hh:mm:ss');
            var ETD = fromTime.format('h:mm A');
            var ETA = toTime.format('h:mm A');

            html += "<input type=\"hidden\" name=\"subvoyage_id[]\" value=\"" + parsed_data["subvoyages"][x].id_subvoyage + "\"/>";
            html += "<div class=\"col-lg-12\">";
            html += "<div class=\"input-group\">";
            html += "<div class=\"oj-leg__title oj-leg__title--gray\">";
            html += "Leg " + leg_count++;
            html += "</div>";
            html += "<span class=\"input-group-btn\">";
            //html += "<button class=\"btn btn-default oj-leg__btn\" type=\"button\">Remove</button>";
            html += "</span>";
            html += "</div>";
            html += "</div>";

            html += "<div class=\"col-xs-12 oj-leg__body\">";
            html += "<div class=\"form-group\">";
            html += "<div class=\"col-xs-3\">";
            html += "<label class=\"control-label\">Origin</label>";
            html += "<input class=\"form-control oj-etd\" type=\"text\" placeholder=\"Input Voyage\" name=\"origin_id[]\" value=\"" + parsed_data["subvoyages"][x].origin + "\" disabled/>";
            html += "</div>";
            html += "<div class=\"col-xs-3\">";
            html += "<label class=\"control-label\">Destination</label>";
            html += "<input class=\"form-control oj-etd\" type=\"text\" placeholder=\"Input Voyage\" name=\"origin_id[]\" value=\"" + parsed_data["subvoyages"][x].destination + "\" disabled/>";
            html += "</div>";
            html += "<div class=\"col-xs-3\">";
            html += "<label class=\"control-label\">Vessel</label>";
            html += "<select class=\"selectpicker form-control\" name=\"vessel_id[]\" required>";
            html += "<option value=\"\">Please select</option>";
            for (y in parsed_data["vessel_list"]) {
                if (parsed_data["vessel_list"][y].id_vessel == parsed_data["subvoyages"][x].vessel_id)
                    html += "<option value=\"" + parsed_data["vessel_list"][y].id_vessel + "\" selected>" + parsed_data["vessel_list"][y].vessel + "</option>";
                else
                    html += "<option value=\"" + parsed_data["vessel_list"][y].id_vessel + "\">" + parsed_data["vessel_list"][y].vessel + "</option>";
            }
            html += "</select>";
            //html += "<span class=\"input-notes-bottom\">< ?=form_error('vessel_id['.$i.']')?></span>";
            html += "</div>";
            //html += "<div class=\"col-xs-3\">";
            //html += "<label class=\"control-label\">Departure Date</label>";
            //html += "<input class=\"form-control custom-datepicker\" type=\"text\" placeholder=\"YY-MM-DD\" name=\"departure_date[]\" required/>";
            //html += "<span class=\"input-notes-bottom\">< ?=form_error('vessel_id['.$i.']')?></span>";
            //html += "</div>";
            html += "</div>";
            html += "<div class=\"form-group\">";
            html += "<div class=\"col-xs-3 has-feedback\">";
            html += "<label class=\"control-label\">ETD <small>(Estimated time of Departure)</small></label>";
            html += "<input class=\"form-control oj-time-picker oj-etd\" type=\"text\" placeholder=\"HH:MM\" name=\"ETD[]\" value=\"" + ETD + "\" disabled/>";
            html += "<span class=\"glyphicon glyphicon-time form-control-feedback\" aria-hidden=\"true\"></span>";
            html += "</div>";
            html += "<div class=\"col-xs-3 has-feedback\">";
            html += "<label class=\"control-label\">ETA <small>(Estimated Time of Arrival)</small></label>";
            html += "<input class=\"form-control oj-time-picker oj-eta\" type=\"text\" placeholder=\"HH:MM\" name=\"ETA[]\" value=\"" + ETA + "\" disabled/>";
            html += "<span class=\"glyphicon glyphicon-time form-control-feedback\" aria-hidden=\"true\"></span>";
            html += "</div>";
            html += "<div class=\"col-xs-3\">";
            html += "<label class=\"control-label\">Travel Time</label>";
            html += "<input class=\"form-control oj-time-picker oj-travel-time\" type=\"text\" value=\"" + travelTime + "\" readonly/>";
            html += "<span class=\"input-notes-bottom\"></span>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
        }

        $(html).appendTo($(".oj-leg"));

        $(".selectpicker").selectpicker("refresh");

        $(".custom-datepicker").datetimepicker({
            format: "YYYY-MM-DD",
            minDate: new Date()
        });

        //Disable all datepicker except the first leg's
        $(".custom-datepicker:not(:first)").prop('disabled', true);

        $(".custom-datepicker").on("dp.change", function (e) {
            var next_datepicker = $(this).closest(".oj-leg__body").nextAll(".oj-leg__body:first").find("input.custom-datepicker");
            //console.log(next_datepicker);
            if (next_datepicker.length > 0) {
                next_datepicker.data("DateTimePicker").minDate(e.date);
                $(next_datepicker).prop('disabled', false);
            }
        });


    });
}

//when legs are fully loaded
window.onload = function () {
    //Trigger travel time computation
    $('.oj-time-picker').trigger('dp.change');

    //Minimum dates
    $.each($(".custom-datepicker"), function (index, value) {
        var next_datepicker = $(this).closest(".oj-leg").nextAll(".oj-leg:first").find("input.custom-datepicker");
        if (next_datepicker.length > 0) {
            next_datepicker.data("DateTimePicker").minDate($(value).val());
        }
    });

    $(".custom-datepicker").on("dp.change", function (e) {
        var next_datepicker = $(this).closest(".oj-leg").nextAll(".oj-leg:first").find("input.custom-datepicker");
        if (next_datepicker.length > 0) {
            next_datepicker.data("DateTimePicker").minDate(e.date);
        }
    });
}