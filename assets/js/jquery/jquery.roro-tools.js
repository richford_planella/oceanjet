/*
|--------------------------------------------------------------------------
| jQuery Tools for user profile
|--------------------------------------------------------------------------
*/

jQuery(function()
	{
		jQuery(document).on('click', '.delete-roro', deleteValidation);
		jQuery(document).on('click', '.activate-roro', activateValidation);
		jQuery(document).on('click', '.de-activate-roro', deActivateValidation);
	}
);

function activateValidation()
{            
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>RORO Rate</h3>', // modal title
			message: '<h4>Are you sure you want to re-activate RORO rate?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        window.location.replace(link);
				}
			}]
		});
        
    return false;
}

function deActivateValidation()
{            
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>RORO Rate</h3>', // modal title
			message: '<h4>Are you sure you want to de-activate RORO rate?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        window.location.replace(link);
				}
			}]
		});
        
    return false;
}

function deleteValidation()
{            
    var link = $(this).attr('href');
    $('body').addClass('margin-dialog');
    BootstrapDialog.show({
			title: '<h3>RORO Rate</h3>', // modal title
			message: '<h4>Are you sure you want to delete RORO rate?</h4>', // modal body
			buttons: [{
				label: 'No',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
                                        dialog.close(); // close modal
				}
			}, {
				label: 'Yes',
                                cssClass: 'btn oj-button',
				action: function(dialog) {
					// ajax post/jQuery post code
                                        window.location.replace(link);
				}
			}]
		});
        
    return false;
}