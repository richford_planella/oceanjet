/*
|--------------------------------------------------------------------------
| jQuery Tools for profile
|--------------------------------------------------------------------------
*/
jQuery(function()
        {
                jQuery(document).on('click', '.reset', resetImg);
                $("#imgInp").change(function(){
                    readURL(this);
                });
        }        
);

function resetImg()
{
    // reset input file
    var $el = $('#imgInp');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
    
    var img = jQuery('#cur_img').val();
    $('#img_uploaded').attr('src', img);
}

function readURL(input) {
    console.log(input.files);
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#img_uploaded').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}