/*
 |--------------------------------------------------------------------------
 | jQuery Tools for Swap Vessel
 |--------------------------------------------------------------------------
 */

jQuery(function()
    {
        jQuery(document).on('click', '.load-vessel', loadVesselSeats);
        jQuery(document).on('click', '.move-seats', moveSeats);
        jQuery(document).on('click', '.save-changes', saveChanges);
        jQuery(document).on('change', '#select-current-seats', highlightVesselSeatCurrent);
        jQuery(document).on('change', '#select-new-seats', highlightVesselSeatNew);
        jQuery(document).on('change', '.vessel-seat', setVesselSeat);

        $('#select-current-seats').select2();
        $('#select-new-seats').select2();
    }
);

//Set Seat no setting
function setVesselSeat(vesselSelected)
{

}


//Highlight Vessel Seat Current Seat
function highlightVesselSeatCurrent()
{
    var current_seats = $('#select-current-seats').val() || [];

    //Reset seats CURRENT
    $('#current_vessel_seats').find('td').each (function() {
        $(this).removeClass('active');
    });

    //Set highlighted Seat
    $('#current_vessel_seats').find('td').each (function() {
        for (i = 0; i < current_seats.length; i++) {
            var seat_no = current_seats[i];
            if($(this).attr('data-vessel-seats')==seat_no){
                $(this).addClass('active');
            }
        }
    });
}

//Highlight Vessel Seat Current Seat
function highlightVesselSeatNew()
{
    var new_seats     = $('#select-new-seats').val();

    //Reset seats CURRENT
    $('#new_vessel_seats').find('td').each (function() {
        $(this).removeClass('active');
    });

    //Set highlighted Seat
    $('#new_vessel_seats').find('td').each (function() {
        for (i = 0; i < new_seats.length; i++) {
            var seat_no = new_seats[i];
            if($(this).attr('data-vessel-seats')==seat_no){
                $(this).addClass('active');
            }
        }
    });
}

//Function swap from current vessel to new vessel
function saveChanges()
{
    checkVessel();
    $('.swap-form').submit();
}

//function move seats
function moveSeats()
{
    //check if number of seats are equal
    checkEqualSeats();
    //Check if we have the same accommodation type
    checkSameAccommodationType();

    //Swap seats
    var current_seats = $('#select-current-seats').val();
    var new_seats     = $('#select-new-seats').val();
    var data  = 'module=swap_seats&'+$('.swap-form').serialize()+'&current_seats='+current_seats+'&new_seats='+new_seats;

    $.ajax({
        type: 'post',
        async: true,
        dataType: 'json',
        url: adminURL + 'passenger_checkin/ajax',
        data: data,
        success: function (data) {
            if(data.msg=='ok') {

                $('#myConfirmationModalLabel').html("Seats successfully transferred. Thank you!"); //Set available users
                $('#confirmation-modal').modal('show');

                loadVesselSeats();
            }
        }
    });

}

function checkSameAccommodationType()
{
    //Current Seat
    var current_seat = $('#select-current-seats').val();
    var current_seat_filter = [];

    for (var i = 0; i < current_seat.length; i++)
        current_seat_filter.push(current_seat[i].substr(0,2));

    var current_seat_unique=current_seat_filter.filter(function(itm,i,current_seat_filter){
        return i==current_seat_filter.indexOf(itm);
    });

    //New Seat
    var new_seat = $('#select-new-seats').val();
    var new_seat_filter = [];

    for (var i = 0; i < new_seat.length; i++)
        new_seat_filter.push(new_seat[i].substr(0,2));

    var new_seat_unique=new_seat_filter.filter(function(itm,i,new_seat_filter){
        return i==new_seat_filter.indexOf(itm);
    });

    if(current_seat_unique.length!=1 && new_seat_unique.length!=1)
        callModal("Please select seats with same accommodation type. Please try again.");

    var is_same = current_seat_unique.length == new_seat_unique.length && current_seat_unique.every(function(element, index) {
            return element === new_seat_unique[index];
        });

    if(is_same==false)
        callModal("Please select seats with same accommodation type. Please try again.");
}

//Check if number of seats are equal
function checkEqualSeats()
{
    //No selected seats on current vessel
    if($('#select-current-seats').val() == null || $('#select-new-seats').val() == null){
        callModal("Please select seats from current vessel or from new vessel. Please try again.");
    }

    //If not equal on quantity
    if($('#select-current-seats').val().length != $('#select-new-seats').val().length){
        callModal("Number of selected seats are not equal. Please try again.");
    }
}

//Call modal function
function callModal(error_msg)
{
    $('#myConfirmationModalLabel').html(error_msg); //Set available users
    $('#confirmation-modal').modal('show');
    throw new Error("No vessel selected. Please try again.");
}


//Load New / Old Vessel Seats
function loadVesselSeats()
{
    checkVessel();
    table_seats();
}

//Check if there is selected vessel
function checkVessel(){
    if($('#new_vessel').val()==0){
        $('#myConfirmationModalLabel').html("No vessel selected. Please try again."); //Set available users
        $('#confirmation-modal').modal('show');

        throw new Error("No vessel selected. Please try again.");
    }
}

//Call draw seats function
function table_seats()
{
    var voyage_management_id = $('#voyage_management_id').val();
    var current_vessel_id = $('#current_vessel_id').val();
    var new_vessel = $('#new_vessel').val();
    //Display New Vessel Name
    var new_vessel_text = $("#new_vessel option:selected").text();
    $('#new_vessel_text').html('Vessel Name: '+new_vessel_text);

    drawVesselSeats(current_vessel_id,new_vessel,voyage_management_id)
}

//Darw current and new seats
function drawVesselSeats(current_vessel_id,new_vessel,voyage_management_id)
{
    $.ajax({
        type        :   'post',
        async       :   true,
        dateType    :   'json',
        url         :   adminURL + 'passenger_checkin/ajax',
        data        :   'module=seatplan&vessel_id='+current_vessel_id+'&voyage_management_id='+voyage_management_id,
        success     :   function(data) {
            var data = JSON.parse(data);
            console.log(data.seat_plan);
            var seats='';
            //Draw seat plan
            $.each(data.seat_plan, function(index, value){
                seats += '<tr>';
                $.each(value, function(i, v){

                    //Add identifier Vacant or Occupied
                    var seat_stat = "<span class='box-status is-vacant'></span>";
                    if(v.booking_id!=0)
                        seat_stat = "<span class='box-status is-occupied'></span>";

                    //If Seat is N/A
                    if(v.value=='N/A')
                        seat_stat = '';

                    seats += "<td style='background-color:"+v.color+"' class='vessel-seat' data-booking-id='"+v.booking_id+"' data-vessel-seats='"+v.value+"' data-accomodation='"+ v.accommodation +"' >"+ seat_stat + v.value  +"</td>";
                });
                seats += '</tr>';
            });

            //Set vessel seats
            $('#current_vessel_seats').html(seats);

            //Draw New Vessel
            $.ajax({
                type        :   'post',
                async       :   true,
                dateType    :   'json',
                url         :   adminURL + 'passenger_checkin/ajax',
                data        :   'module=seatplan&vessel_id='+new_vessel+'&voyage_management_id='+voyage_management_id,
                success     :   function(data) {
                    var data = JSON.parse(data);
                    console.log(data.seat_plan);
                    var seats='';
                    //Draw seat plan
                    $.each(data.seat_plan, function(index, value){
                        seats += '<tr>';
                        $.each(value, function(i, v){

                            //Add identifier Vacant or Occupied
                            var seat_stat = "<span class='box-status is-vacant'></span>";
                            if(v.booking_id!=0)
                                seat_stat = "<span class='box-status is-occupied'></span>";

                            //If Seat is N/A
                            if(v.value=='N/A')
                                seat_stat = '';

                            seats += "<td style='background-color:"+v.color+"' class='vessel-seat' data-booking-id='"+v.booking_id+"' data-vessel-seats='"+v.value+"' data-accomodation='"+ v.accommodation +"' >"+ seat_stat + v.value  +"</td>";
                        });
                        seats += '</tr>';
                    });

                    //Set new vessel seats
                    $('#new_vessel_seats').html(seats);

                    //Get available seats
                    $.ajax({
                        type        :   'post',
                        async       :   true,
                        dataType    :   'json',
                        url         :   adminURL + 'passenger_checkin/ajax',
                        data        :   "module=vacant_available_seats&voyage_management_id=" + voyage_management_id+"&new_vessel_id="+new_vessel+"&current_vessel="+current_vessel_id,
                        success     :   function(data) {
                            //Set current seats
                            var seats = '';
                            for (x in data.current_seats) {
                                var vessel_seats = data.current_seats[x].vessel_seats;
                                var full_name    = data.current_seats[x].name;
                                var ticket_no    = data.current_seats[x].ticket_no;
                                var valid_id     = data.current_seats[x].valid_id;
                                    seats += "<option value="+vessel_seats+" >"+ vessel_seats +" | "+ ticket_no +" | "+ full_name +" </option>";
                            }

                            $('#select-current-seats').html(seats); //Set seats on current vessel
                            var $vesselSelectedCurrent = $('#select-current-seats').select2();

                            //Set new seats
                            var seats = '';
                            for (x in data.new_seats) {
                                var vessel_seats = data.new_seats[x];
                                seats += "<option value="+vessel_seats+" >"+ vessel_seats +" </option>";
                            }

                            $('#select-new-seats').html(seats); //Set seats on current vessel
                            var $vesselSelectedNew = $('#select-new-seats').select2();


                            //Put seat num on Select2 field Current Vessel
                            $('table.current-seat-table tbody tr td.vessel-seat').on('click',function(){
                                var vessel_seats = $(this).attr('data-vessel-seats');
                                var booking_id   = $(this).attr('data-booking-id');

                                //Get current value
                                var newValue = $vesselSelectedCurrent.val() || [];
                                console.log(newValue);

                                //Value not currently set
                                if(!$(this).hasClass('active')){
                                    newValue.push(vessel_seats);
                                }else{
                                    var oldValue = $vesselSelectedCurrent.val();
                                    var index = oldValue.indexOf(vessel_seats);
                                    newValue.splice(index,1)
                                }


                                console.log(newValue);
                                if(booking_id!=0){
                                    $vesselSelectedCurrent.val(newValue);
                                    $vesselSelectedCurrent.trigger("change");
                                }
                            });

                            //Put seat num on Select2 field new
                            $('table.new-seat-table tbody tr td.vessel-seat').on('click',function(){
                                var vessel_seats = $(this).attr('data-vessel-seats');
                                var booking_id   = $(this).attr('data-booking-id');

                                //Get current value
                                var newValue1 = $vesselSelectedNew.val() || [];
                                console.log('current:'+newValue1);

                                //Value not currently set
                                if(!$(this).hasClass('active')){
                                    newValue1.push(vessel_seats);
                                }else{
                                    var oldValue = $vesselSelectedNew.val();
                                    var index = oldValue.indexOf(vessel_seats);
                                    newValue1.splice(index,1)
                                }


                                console.log(newValue1);
                                if(booking_id==0){
                                    $vesselSelectedNew.val(newValue1);
                                    $vesselSelectedNew.trigger("change");
                                }
                            });

                        }
                    });

                },
                beforeSend: function() {
                    $('#new_vessel_seats').html(animation_logo);  // show loading indicator
                }
            });

        },
        beforeSend: function() {
            $('#current_vessel_seats').html(animation_logo);  // show loading indicator
        }
    });
}