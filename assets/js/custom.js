
$(document).ready(
                    function()
                    {
                        // set table as DataTable
                        var table = $('#main_table').DataTable({
                        //Positioning and display row Length in datatable
                         "dom": 'rt<"bottom"lp>'
                       });
                        
                        $('#column3_search').on( 'keyup', function () {
                            table
                                .search(this.value)
                                .draw();
                        } );
                        
                        $('.numeric-only').bind('contextmenu',function(e){
                            e.preventDefault();
                            alert('Right Click is not allowed on textbox');
                        });
                        
                        jQuery('.numbersOnly').on('keydown blur',function () { 
                            this.value = this.value.replace(/[^0-9\.]/g,'');
                        });
                        

                        $(".numeric-only").on('keydown',function (e) {
                            // Allow: backspace, delete, tab, escape, enter and .
                            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                                 // Allow: Ctrl+A, Command+A
                                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                                 // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                                     // let it happen, don't do anything
                                     return;
                            }
                            // Ensure that it is a number and stop the keypress
                            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                e.preventDefault();
                            }
                        }).on('blur',function(){
                            // get value
                            var value = $(this).val();

                            if($(this).val().length == 0)
                                $(this).val('00');
                            else if($(this).val().length == 1)
                                $(this).val('0'+value);
                            else
                                $(this).val(value);

                        });

//                        // for phone number
//                        $(".phone_number").mask("(999) 999-9999");
//
//                        $(".phone_number").on("blur", function() {
//                            var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
//
//                            if( last.length == 3 ) {
//                                var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
//                                var lastfour = move + last;
//
//                                var first = $(this).val().substr( 0, 9 );
//
//                                $(this).val( first + '-' + lastfour );
//                            }
//                        });
//                        // for mobile number
//                        $(".mobile_number").mask("(99) 999-9999999");
//
//                        $(".mobile_number").on("blur", function() {
//                            var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
//
//                            if( last.length == 3 ) {
//                                var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
//                                var lastfour = move + last;
//
//                                var first = $(this).val().substr( 0, 9 );
//
//                                $(this).val( first + '-' + lastfour );
//                            }
//                        });

                        datepickerDiff();
						
                        $(".text-to-upper").bind('keyup', function (e) {
                                if (e.which >= 97 && e.which <= 122) {
                                        var newKey = e.which - 32;
                                        // I have tried setting those
                                        e.keyCode = newKey;
                                        e.charCode = newKey;
                                }

                                $(this).val(($(this).val()).toUpperCase());
                        });
                    }
);

//Time Picker with output difference
var datepickerDiff = function () {

  //Timepicker    
  $('.oj-time-picker').datetimepicker({
    format: 'LT',
    //    debug: true
  });


  $('.oj-time-picker').on('dp.change', function (e) {
    var pdate = $(this).parents('.oj-leg');
    var fromTime = moment(pdate.find('.oj-etd').val(), 'h:mm a');
    var toTime = moment(pdate.find('.oj-eta').val(), 'h:mm a');
    var timeDiff = toTime.diff(fromTime)
    pdate.find('.oj-travel-time').val(moment.utc(timeDiff).format('H:mm'));

  })
}