

$(document).ready(function () {
  //dataTables
  var table = $('#example').DataTable();
  $('#btnSearch').click(function () {
    table.search("search").draw();
  });


  //Search btn Fade in Fade out action 
  $('.btn-search').click(function () {
    $('div.noinfo').fadeOut(250, function () {
      $('.hide').removeClass('hide');
      $('div.info').fadeIn(250);
    })
  });

  // clicked after successful void
  $('.btn-oj-yes').click(function () {
    $('div.info').fadeOut(250, function () {
      $('div.info').addClass('hide');
      $('div.noinfo').fadeIn(250);
    })
  });

  
  //  page open 
  $('.btn-open').click(function() {
     url = $('select.open-page').val();
     window.open(url,"_self");
  });
  
//  //  Modal Active button
//  $(".dropdown-menu.status li a").click(function(){
//    var selText = $(this).text();
//    $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
//  });
  
  //  Bootstrap Select
  $('.selectpicker').selectpicker();

  
});


