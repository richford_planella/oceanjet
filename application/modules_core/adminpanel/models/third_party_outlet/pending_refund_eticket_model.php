<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Model Class
|--------------------------------------------------------------------------
|
| Handles the outlet table on the DB
|
| @category	Model
| @author		Philip Reamon
*/
class Pending_Refund_Eticket_Model extends MY_Model
{
	/* int booking  id */
	public $id_booking = NULL;
	
        /* id user id */
	public $user_id;
        
        /* id ticket series id */
	public $ticket_series_info_id;
        
        /* id passenger id */
	public $passenger_id;
        
        /* id passenger fare id */
	public $passenger_fare_id;
        
        /* id booking source id */
	public $booking_source_id;
        
        /* id booking status id */
	public $booking_status_id;
        
        /* id discount id */
	public $discount_id;
        
	/* decimal points */
	public $points;
        
        /* string jetter number */
	public $jetter_no;
        
        /* string payment */
	public $payment;
        
        /* decimal fare price */
	public $fare_price;
        
        /* decimal return fare price */
	public $return_fare_price;
        
        /* decimal total discount */
	public $total_discount;
        
        /* decimal terminal fee */
	public $terminal_fee;
        
        /* decimal port charge */
	public $port_charge;
        
        /* decimal total amount */
	public $total_amount;
        
        /* id reference id */
	public $reference_id;
        
        /* string passenger valid ids */
	public $passenger_valid_id_number;
        
        /* datetime date added */
	public $date_added;
        
        /* datetime date update */
	public $date_update;
        
	/* string table name */
	protected $table = 'booking';

	/* string table identifier */
	protected $identifier = 'id_booking';

	/* string foreign key */
	protected $foreign_key = 'booking_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_booking'] 	= (int)($this->id);
			$fields['trip_type_id'] 	=  $this->accommodation_code;
			$fields['leg_id'] 		=  $this->accommodation;
			$fields['departure_date'] 				=  $this->enabled;
			$fields['voyage_id'] 			=  $this->date_added;
			$fields['vessel_id'] 			=  $this->date_update;
			$fields['accommodation_id'] 			=  $this->date_update;
			$fields['ticket_type'] 			=  "eticket";
			$fields['ticket_status'] 			=  (int) $this->ticket_status;
		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'booking');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_booking' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		 self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('booking u'));

		return parent::get('booking u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for outlet
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*,p.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('passenger p', 'p.id_passenger = u.passenger_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_accommodation' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}

	public function get_eticket_details($id_booking) {

		$sql = "
			select
				b.*,
				pf.*,
				v.voyage,
				v.origin as voyage_origin,
				v.destination as voyage_destination,
				sv.*,
				p.*,
				ts.*,
				a.*,
				vt.*,
				o.port as origin_name,
				d.port as destination_name,
				vs.*,
				rs.*,
				rt.*
			from
				booking b
			left join
				passenger_fare pf 
			on
				pf.id_passenger_fare = b.passenger_fare_id
			left join
				voyage v
			on
				v.id_voyage = pf.voyage_id
			left join
				subvoyage sv
			on
				sv.voyage_id = v.id_voyage
			left join
				vessel vs
			on
				vs.id_vessel = sv.vessel_id
			left join
				passenger p
			on
				p.id_passenger = passenger_id
			left join
				ticket_series_info ts
			on
				ts.id_ticket_series_info = b.ticket_series_info_id
			left join
				accommodation a
			on
				a.id_accommodation = pf.accommodation_id
			left join
				void_tickets vt
			on
				vt.booking_id = b.id_booking
			left join
				port o
			on
				o.id_port = sv.origin
			left join
				port d
			on
				d.id_port = sv.destination
			left join
				rule_set rs
			on
				rs.id_rule_set = pf.rule_set_id
			left join
				rule_type rt
			on
				rt.rule_set_id = rs.id_rule_set
			where
				b.id_booking = ".$id_booking;

		$query = $this->db->query($sql);

		 foreach($query->result() as $row) {
		 	$arData['id_booking'] = $row->id_booking;
			$arData['fullname']  = $row->firstname." ".$row->lastname;
			$arData['voyage'] = $row->voyage;
			$arData['voyage_origin'] = $row->voyage_origin;
			$arData['voyage_destination'] = $row->voyage_destination;
			$arData['void_reason'] = $row->void_reason;
			$arData['rule_code'] = $row->rule_code;
			$arData['rule_type_amount'] = $row->rule_type_amount;
			$arData['refund_amount'] = $row->total_amount - $row->rule_type_amount;
			$arData['subvoyage'][] =$row;

		 }

		return $arData;


	}

	
}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/outlet_model.php */