<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Model Class
|--------------------------------------------------------------------------
|
| Handles the outlet table on the DB
|
| @category	Model
| @author		Philip Reamon
*/
class Approved_Refund_Eticket_Model extends MY_Model
{
	/* int booking  id */
	public $id_booking = NULL;
	
        /* id user id */
	public $user_id;
        
        /* id ticket series id */
	public $ticket_series_info_id;
        
        /* id passenger id */
	public $passenger_id;
        
        /* id passenger fare id */
	public $passenger_fare_id;
        
        /* id booking source id */
	public $booking_source_id;
        
        /* id booking status id */
	public $booking_status_id;
        
        /* id discount id */
	public $discount_id;
        
	/* decimal points */
	public $points;
        
        /* string jetter number */
	public $jetter_no;
        
        /* string payment */
	public $payment;
        
        /* decimal fare price */
	public $fare_price;
        
        /* decimal return fare price */
	public $return_fare_price;
        
        /* decimal total discount */
	public $total_discount;
        
        /* decimal terminal fee */
	public $terminal_fee;
        
        /* decimal port charge */
	public $port_charge;
        
        /* decimal total amount */
	public $total_amount;
        
        /* id reference id */
	public $reference_id;
        
        /* string passenger valid ids */
	public $passenger_valid_id_number;
        
        /* datetime date added */
	public $date_added;
        
        /* datetime date update */
	public $date_update;
        
	/* string table name */
	protected $table = 'booking';

	/* string table identifier */
	protected $identifier = 'id_booking';

	/* string foreign key */
	protected $foreign_key = 'booking_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_booking'] 	= (int)($this->id);
			$fields['trip_type_id'] 	=  $this->accommodation_code;
			$fields['leg_id'] 		=  $this->accommodation;
			$fields['departure_date'] 				=  $this->enabled;
			$fields['voyage_id'] 			=  $this->date_added;
			$fields['vessel_id'] 			=  $this->date_update;
			$fields['accommodation_id'] 			=  $this->date_update;
			$fields['ticket_type'] 			=  "eticket";
			$fields['ticket_status'] 			=  (int) $this->ticket_status;
		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'booking');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_booking' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		 self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('booking b'));

		return parent::get('booking b');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for outlet
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('b.*,p.*,ts.*,rt.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('passenger p', 'p.id_passenger = b.passenger_id', 'left');
		$this->db->join('ticket_series_info ts', 'ts.id_ticket_series_info = b.ticket_series_info_id', 'left');
		$this->db->join('refund_tickets rt', 'rt.booking_id = b.id_booking', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_accommodation' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}

	public function get_eticket_details($id_booking) {

		$sql = "
			select 
				b.*,
				p.lastname,
				p.firstname,
				ts.ticket_no,
				sp.*,
				spa.*,
				ss.id_subvoyage_management_status,
				ss.description,
				v.id_voyage,
				v.voyage,
				p1.id_port as origin_id,
				p2.id_port as destination_id,
				p1.port as origin,
				p2.port as destination,
				pf.*,
				vs.vessel,
				ac.accommodation
			from
				booking b
			left join
				ticket_series_info ts
			on
				ts.id_ticket_series_info = b.ticket_series_info_id
			left join
				passenger p
			on
				p.id_passenger = b.passenger_id
			left join
				subvoyage_management spa
			on
				spa.id_subvoyage_management = b.subvoyage_management_id
			left join
				subvoyage_management_status ss
			on
				ss.id_subvoyage_management_status = spa.subvoyage_management_status_id
			left join
				subvoyage sp
			on
				sp.id_subvoyage = spa.subvoyage_id
			left join
				voyage v
			on
				v.id_voyage = sp.voyage_id	
			left join
				port p1
			on
				p1.id_port = sp.origin_id
			left join
				port p2
			on
				p2.id_port = sp.destination_id
			left join
				vessel vs
			on
				vs.id_vessel = spa.vessel_id
			left join
				passenger_fare pf
			on
				pf.id_passenger_fare = b.passenger_fare_id
			
			left join
				accommodation ac
			on
				ac.id_accommodation = pf.accommodation_id
			where
				b.id_booking = ".$id_booking;


		$query = $this->db->query($sql);

		 foreach($query->result() as $row) {
		 	$arData['id_booking'] = $row->id_booking;
			$arData['fullname']  = $row->firstname." ".$row->lastname;
			$arData['voyage'] = $row->voyage;
			$arData['voyage_origin'] = $row->origin_id;
			$arData['voyage_destination'] = $row->destination_id;
			$arData['id_subvoyage_management_status'] = $row->id_subvoyage_management_status;
			$arData['rule_set_id'] = $row->rule_set_id;
			$arData['ticket_total_amount'] = $row->total_amount;
			$arData['accommodation_id'] = $row->accommodation_id;
			$arData['subvoyage_management_status'] = $row->description;
			$arData['eta'] = $row->ETA;
			$arData['etd'] = $row->ETD;
			// // $arData['rule_type_amount'] = $row->rule_type_amount;
			// // $arData['refund_amount'] = $row->total_amount - $row->rule_type_amount;
			// $arData['subvoyage'][] =$row;

		 }

		return $arData;

	}

}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/outlet_model.php */