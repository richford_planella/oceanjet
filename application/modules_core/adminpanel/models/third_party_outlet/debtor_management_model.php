<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Commission Model Class
|--------------------------------------------------------------------------
|
| Handles the commission table on the DB
|
| @category	Model
| @author		Philip Reamon
*/
class Debtor_Management_Model extends MY_Model
{
	/* int id_outlet */
	public $id_debtor = NULL;
        
	/* int commission code */
	public $debtor_code;
	
	/* string commission description */
	public $user_id;
	/* int status */
	public $enabled;
		/* datetime date_added */
	public $amount;
	/* datetime date_update */
	// public $date_update;
     
	/* string table name */
	protected $table = 'debtor';

	/* string table identifier */
	protected $identifier = 'id_debtor';
	 /* string foreign key */
	protected $foreign_key = 'debtor_id';  
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_debtor']	= (int)($this->id);
			$fields['debtor_code'] =  $this->debtor_code;
			$fields['user_id'] 	=  (int)($this->user_id);
			$fields['enabled'] =  (int)($this->enabled);
			// $fields['date_update'] 				=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'debtor');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_debtor' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);
		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('debtor u'));

		return parent::get('debtor d');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));
		
		return $this->db->count_all_results('debtor_wallet');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('debtor d');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('DISTINCT(d.id_debtor),d.debtor_code,d.enabled,d.amount,ua.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		//$this->db->join('debtor_outlet do', 'do.debtor_id = d.id_debtor', 'left');
		//$this->db->join('outlet o', 'o.id_outlet = do.outlet_id', 'left');
		$this->db->join('user_account ua ', 'ua.user_id = d.user_id', 'left');
	
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_debtor' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}

}

/* End of file commission_model.php */
/* Location: ./application/modules_core/adminpanel/models/commission/commission_model.php */