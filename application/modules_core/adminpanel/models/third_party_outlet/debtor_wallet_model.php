<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Commission Model Class
|--------------------------------------------------------------------------
|
| Handles the commission table on the DB
|
| @category	Model
| @author		Philip Reamon
*/
class Debtor_Wallet_Model extends MY_Model
{
	/* int id_outlet */
	public $id_debtor_wallet = NULL;
        
	/* int commission code */
	public $debtor_id;
	
	/* string commission description */
	public $wallet_amount;
	
	/* decimal commission percentage */
	public $payment_account_id;
	
	/* int status */
	public $deposit_date;
	
	/* datetime date_added */
	public $reference;
	/* datetime date_added */
	public $type;
	/* datetime date_update */
	// public $date_update;
        
	/* string table name */
	protected $table = 'debtor_wallet';

	/* string table identifier */
	protected $identifier = 'id_debtor_wallet';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_debtor_wallet']	= (int)($this->id);
			$fields['debtor_id'] =  (int)($this->debtor_id);
			$fields['payment_account_id'] 	=  (int)($this->payment_account_id);
            $fields['wallet_amount'] = $this->wallet_amount;
			$fields['reference'] =  $this->reference;
			$fields['deposit_date'] =  date("Y-m-d H:i:s",strtotime($this->deposit_date));
			$fields['type'] =  (int)($this->type);
			// $fields['date_update'] 				=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'debtor_wallet');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_debtor' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);
		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('debtor_wallet dw'));

		return parent::get('debtor_wallet dw');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('debtor_wallet dw');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('dw.*,d.*,pa.*,ua.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('payment_account pa ', 'pa.id_payment_account = dw.payment_account_id', 'left');
		$this->db->join('debtor d ', 'd.id_debtor = dw.debtor_id', 'left');
		$this->db->join('user_account ua','ua.user_id = d.user_id','left');
		
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_commission' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file commission_model.php */
/* Location: ./application/modules_core/adminpanel/models/commission/commission_model.php */