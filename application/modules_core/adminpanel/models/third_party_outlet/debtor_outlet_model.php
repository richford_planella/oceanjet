<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Commission Model Class
|--------------------------------------------------------------------------
|
| Handles the commission table on the DB
|
| @category	Model
| @author		Philip Reamon
*/
class Debtor_Outlet_Model extends MY_Model
{
	/* int id_outlet */
	public $id_debtor_outlet = NULL;
        
	/* int commission code */
	public $debtor_id;

	/* decimal commission percentage */
	public $outlet_id;
	
	/* int status */
	public $enabled;
	
	/* datetime date_added */
	public $commission_id;
		/* datetime date_added */
	public $amount;
	/* datetime date_update */
	// public $date_update;
        
	/* string table name */
	protected $table = 'debtor_outlet';

	/* string table identifier */
	protected $identifier = 'id_debtor_outlet';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_debtor_outlet']	= (int)($this->id);
			$fields['debtor_id']	= (int)($this->id);
			
            $fields['outlet_id'] = (int)($this->outlet_id);
			
			// $fields['date_update'] 				=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'debtor');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_debtor_outlet' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);
		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('debtor_outlet u'));

		return parent::get('debtor_outlet u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('debtor_outlet u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*,o.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('outlet o ', 'o.id_outlet = u.outlet_id', 'left');
		
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_commission' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}

	
}

/* End of file commission_model.php */
/* Location: ./application/modules_core/adminpanel/models/commission/commission_model.php */