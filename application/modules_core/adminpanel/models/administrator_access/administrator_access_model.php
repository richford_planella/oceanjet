<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Access Model Class
|--------------------------------------------------------------------------
|
| Handles the administrator permissions table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Administrator_Access_Model extends MY_Model
{
	/* int user profile id */
	public $user_profile_id;

	/* int administrator page id */
	public $administrator_page_id;
        
        /* bool access permission */
	public $access;
        
	/* bool view permission */
	public $view;

	/* bool add permission */
	public $add;

	/* bool edit permission */
	public $edit;

	/* bool delete permission */
	public $delete;
        
        /* bool approve permission */
	public $approve;
        
        /* bool verify permission */
	public $verify;
        
        /* bool print permission */
	public $print;
        
        /* bool request permission */
	public $request;
        
        /* bool close permission */
	public $close;
        
        /* bool received permission */
	public $received;
        
        /* bool allocate permission */
	public $allocate;
        
        /* bool activate permission */
	public $activate;
        
        /* bool reactivate permission */
	public $reactivate;
        
        /* bool export permission */
	public $export;
	
	/* string table name */
	protected $table = 'administrator_access';

	/* string table identifier */
	protected $identifier = 'id_administrator_access';

	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------
	
	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		$fields['user_profile_id'] = $this->user_profile_id;
		$fields['administrator_page_id'] = $this->administrator_page_id;
		$fields['view'] = $this->view;
		$fields['add'] = $this->add;
		$fields['edit'] = $this->edit;
		$fields['delete'] = $this->delete;
                $fields['approve'] = $this->approve;
                $fields['verify'] = $this->verify;
                $fields['print'] = $this->print;
                $fields['request'] = $this->request;
                $fields['close'] = $this->close;
                $fields['received'] = $this->received;
                $fields['allocate'] = $this->allocate;
                $fields['activate'] = $this->activate;
                $fields['reactivate'] = $this->reactivate;
                $fields['export'] = $this->export;

		return $fields;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get all permissions
	 *
	 * @access		public
	 * @param		mixed
	 * @return		void
	 */
	public function getPermissions($where = array())
	{
		// WHERE
		$this->db->where($where);

		// JOIN
		//$this->db->join('administrator_page ap', 'ap.id_administrator_page = aa.administrator_page_id', 'left');
		
		$query = $this->db->get('administrator_access aa');
		$result = $query->result();
		
		return $result;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Get all permissions of the profile
	 *
	 * @access		public
	 * @param		mixed
	 * @return		void
	 */
	public function getActions($where = array())
	{
		// SELECT
		$this->db->select('view, add, edit, delete, approve, verify, print, request, close, received, allocate, activate, reactivate, export');

		// WHERE
		$this->db->where($where);

		// JOIN
		//$this->db->join('administrator_page ap', 'ap.id_administrator_page = aa.administrator_page_id', 'left');
		
		$query = $this->db->get('administrator_access aa');
		$result = $query->row();
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get admin access based on action
	 *
	 * @access		public
	 * @param		string
	 * @param		int
	 * @param		int
	 * @return		bool
	 */
	public function getAccess($action = 'access', $admin_page_id = '', $admin_profile_id = 1)
	{
		// SELECT
		$this->db->select($action);
		
		// WHERE
		$this->db->where(array('administrator_page_id' => $admin_page_id, 'user_profile_id' => $admin_profile_id));
		
		$query = $this->db->get('administrator_access');
		$row = $query->row();
		
		if ($row)
			return $row->$action;
			
		return FALSE;
	}
}
