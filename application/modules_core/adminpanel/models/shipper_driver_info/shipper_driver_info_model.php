<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Shipper/Driver Information Model Class
|--------------------------------------------------------------------------
|
| Handles the shipper_driver_info table on the DB
|
| @category	Model
| @author		Richford Planella
*/ 
class Shipper_Driver_Info_Model extends MY_Model
{
	/* int shipper_driver_info id */
	public $id_shipper_driver_info = NULL;
	
    /* varchar shipper's first name */
	public $shipper_firstname;
	
	/* varchar shipper's last name */
	public $shipper_lastname;
	
	/* varchar shipper's middle initial */
	public $shipper_middlename;
	
	/* varchar shipper's contact no */
	public $shipper_contactno;
	
	/* varchar driver's first name */
	public $driver_firstname;
	
	/* varchar driver's last name */
	public $driver_lastname;
	
	/* varchar driver's middle initial */
	public $driver_middlename;
	
	/* varchar driver's contact no */
	public $driver_contactno;

	/* string table name */
	protected $table = 'shipper_driver_info';

	/* string table identifier */
	protected $identifier = 'id_shipper_driver_info';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_passenger'] 		= (int)($this->id);
			$fields['shipper_firstname'] 	=  $this->shipper_firstname;
			$fields['shipper_lastname'] 	=  $this->shipper_lastname;
			$fields['shipper_middlename'] 	=  $this->shipper_middlename;
			$fields['shipper_contactno'] 	=  $this->shipper_contactno;
			$fields['driver_firstname'] 	=  $this->driver_firstname;
			$fields['driver_lastname'] 		=  $this->driver_lastname;
			$fields['driver_middlename'] 	=  $this->driver_middlename;
			$fields['driver_contactno'] 	=  $this->driver_contactno;
			

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'shipper_driver_info');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_shipper_driver_info' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		// self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		// self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('shipper_driver_info u'));

		return parent::get('shipper_driver_info u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for outlet
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('shipper_driver_info u');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_shipper_driver_info' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file shipper_driver_info.php */
/* Location: ./application/modules_core/adminpanel/models/shipper_driver_info/shipper_driver_info_model.php */