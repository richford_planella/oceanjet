<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Booking Model Class
|--------------------------------------------------------------------------
|
| Handles the booking table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Booking_Model extends MY_Model
{
	/* int booking  id */
	public $id_booking = NULL;
	
        /* int user id */
	public $user_id;
        
        /* int ticket series id */
	public $ticket_series_info_id;
        
        /* int passenger id */
	public $passenger_id;
        
        /* int passenger fare id */
	public $passenger_fare_id;
        
        /* int booking source id */
	public $booking_source_id;
        
        /* int booking status id */
	public $booking_status_id;
        
        /* int discount id */
	public $discount_id;
        
        /* int subvoyage management id */
	public $subvoyage_management_id;
        
        /* string transaction ref */
	public $transaction_ref;
        
	/* decimal points */
	public $points;
        
        /* string jetter number */
	public $jetter_no;
        
        /* string payment */
	public $payment;
        
        /* decimal fare price */
	public $fare_price;
        
        /* decimal return fare price */
	public $return_fare_price;
        
        /* decimal total discount */
	public $total_discount;
        
        /* decimal terminal fee */
	public $terminal_fee;
        
        /* decimal port charge */
	public $port_charge;
        
        /* decimal total amount */
	public $total_amount;
        
        /* int payment method id */
	public $payment_method_id;
        
        /* int reference id */
	public $reference_id;
        
        /* tinyint is return */
	public $is_return;
        
        /* string passenger valid ids */
	public $passenger_valid_id_number;
        
        /* datetime date added */
	public $date_added;
        
        /* datetime date update */
	public $date_update;
        
	/* string table name */
	protected $table = 'booking';

	/* string table identifier */
	protected $identifier = 'id_booking';

	/* string foreign key */
	protected $foreign_key = 'booking_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_booking'] = (int)($this->id);
		$fields['user_id'] =  $this->user_id;
                $fields['ticket_series_info_id'] =  $this->ticket_series_info_id;
                $fields['passenger_id'] =  $this->passenger_id;
                $fields['passenger_fare_id'] =  $this->passenger_fare_id;
                $fields['booking_source_id'] =  $this->booking_source_id;
                $fields['booking_status_id'] =  $this->booking_status_id;
                $fields['discount_id'] =  $this->discount_id;
                $fields['subvoyage_management_id'] =  $this->subvoyage_management_id;
                $fields['transaction_ref'] =  $this->transaction_ref;
                $fields['points'] =  $this->points;
                $fields['jetter_no'] =  $this->jetter_no;
                $fields['payment'] =  $this->payment;
                $fields['fare_price'] =  $this->fare_price;
                $fields['return_fare_price'] =  $this->return_fare_price;
                $fields['total_discount'] =  $this->total_discount;
                $fields['terminal_fee'] =  $this->terminal_fee;
                $fields['port_charge'] =  $this->port_charge;
                $fields['total_amount'] =  $this->total_amount;
                $fields['reference_id'] =  $this->reference_id;
                $fields['is_return'] =  $this->is_return;
                $fields['passenger_valid_id_number'] =  $this->passenger_valid_id_number;
                $fields['date_added'] =  $this->date_added;
                $fields['date_update'] =  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'booking');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display booking list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_booking' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('booking b'));

		return parent::get('booking b');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for booking to checkin
	 *
	 * @access		public
	 * @return		int
	 */
	public function countCheckIn()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('checkin ci');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get booking field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('booking b');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('b.*,u.user_profile_id,ua.email_address,ua.firstname as u_firstname,ua.middlename as u_middlename,ua.lastname as u_lastname,ua.outlet_id,'
                        . 'ts.ticket_series_id,ts.ticket_no,ts.issued,'
                        . 'p.firstname,p.middlename,p.lastname,p.birthdate,p.age,p.gender,p.contactno,p.id_no,p.nationality,p.with_infant,p.infant_name,'
                        . 'pf.passenger_fare,pf.trip_type_id,pf.rule_set_id,pf.accommodation_id,pf.seat_limit,pf.regular_fare,pf.points_earned,pf.points_redemption,'
                        . 'bs.booking_source,bss.booking_status,'
                        . 'd.discount_code,d.discount,d.discount_type,d.discount_amount,d.age_from,d.age_to,d.vat_exempt,d.require_id,'
                        . 'pm.payment_method');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('user u', 'u.id_user = b.user_id', 'left');
                $this->db->join('user_account ua', 'ua.user_id = u.id_user', 'left');
                $this->db->join('ticket_series_info ts', 'ts.id_ticket_series_info = b.ticket_series_info_id', 'left');
                $this->db->join('passenger p', 'p.id_passenger = b.passenger_id', 'left');
                $this->db->join('passenger_fare pf', 'pf.id_passenger_fare = b.passenger_fare_id', 'left');
                $this->db->join('booking_source bs', 'bs.id_booking_source = b.booking_source_id', 'left');
                $this->db->join('booking_status bss', 'bss.id_booking_status = b.booking_status_id', 'left');
                $this->db->join('discounts d', 'u.id_user = b.discount_id', 'left');
                $this->db->join('payment_method pm', 'pm.id_payment_method = b.payment_method_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_booking' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get available voyage
	 *
	 * @access		public
	 * @param		array
	 * @return		object
	 */
	public function get_voyages($input = array()) {
            
		$query = $this->db->query(
			'SELECT
                            sm.id_subvoyage_management,
                            sm.voyage_management_id,
                            p.voyage_id,
                            p.origin_id as origin,
                            p.id_subvoyage as origin_subvoyage_id,
                            p.ETD as ETD,
                            sp.destination_id as destination,
                            sp.id_subvoyage as destination_subvoyage_id,
                            sp.ETA as ETA,
                            sm.departure_date as dept_date,
                            ve.vessel as vessel,
                            v.voyage as voyage_code,
                            p1.port_code as port_origin,
                            p2.port_code as port_destination,
                            p1.terminal_fee,
                            p1.port_charge,
                            sms.description as subvoyage_management_status,
                            sm.subvoyage_management_status_id,
                            v.no_of_subvoyages
			FROM 
                            subvoyage p, 
                            subvoyage sp, 
                            subvoyage_management sm, 
                            voyage_management vm,
                            voyage v,
                            port p1,
                            port p2,
                            vessel ve,
                            subvoyage_management_status sms
			WHERE
                            sm.vessel_id = ve.id_vessel
                        AND
                            sm.subvoyage_management_status_id = sms.id_subvoyage_management_status
			AND
                            p1.id_port = p.origin_id
			AND
                            p2.id_port = sp.destination_id
			AND
                            p.voyage_id = v.id_voyage
			AND
                            p.voyage_id = vm.voyage_id
			AND
                            p.id_subvoyage = sm.subvoyage_id
			AND
                            sm.voyage_management_id = vm.id_voyage_management
			AND	
                            p.voyage_id = sp.voyage_id 
			AND 
                            p.origin_id = '. $input['origin_id'] .' 
			AND 
                            sp.destination_id = '. $input['destination_id'] .'
			AND
                            p.id_subvoyage <= sp.id_subvoyage
                        AND 
                            v.enabled = 1
			GROUP BY
                            sm.voyage_management_id
                        ORDER BY
                            sm.departure_date ASC'
		);
		
	
		return $query->result();
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get available voyage using id(string)
	 *
	 * @access		public
	 * @param		array
	 * @return		object
	 */
	public function get_voyage_from_id($origin_subvoyage_id, $destination_subvoyage_id, $voyage_management_id, $count = FALSE) 
    {
                $query = $this->db->query(
                                        'SELECT '
                                            . 's.id_subvoyage, s.ETD, s.ETA, sm.departure_date, sm.id_subvoyage_management, '
                                            . 'vs.id_vessel,vs.vessel_code, vs.vessel, '
                                            . 'v.voyage, p1.id_port as origin_port_id, p2.id_port as destination_port_id, '
                                            . 'p1.port_code as origin_port_code, p1.port as origin_port, p1.terminal_fee as origin_terminal_fee, p1.port_charge as origin_port_charge, '
                                            . 'p2.port_code as destination_port_code, p2.port as destination_port, p2.terminal_fee as destination_terminal_fee, p2.port_charge as destination_port_charge '
                                        . 'FROM '
                                            . 'subvoyage s, '
                                            . 'subvoyage_management sm,'
                                            . 'voyage_management vm, '
                                            . 'voyage v, '
                                            . 'vessel vs, '
                                            . 'port p1, '
                                            . 'port p2 '
                                        . 'WHERE '
                                            . "s.id_subvoyage >= $origin_subvoyage_id "
                                        . "AND "
                                            . "s.id_subvoyage <= $destination_subvoyage_id "
                                        . "AND "
                                            . "vm.id_voyage_management = $voyage_management_id "
                                        . "AND "
                                            . "sm.subvoyage_id = s.id_subvoyage "
                                        . "AND "
                                            . "sm.voyage_management_id = vm.id_voyage_management "
                                        . "AND "
                                            . "vm.voyage_id = s.voyage_id "
                                        . "AND "
                                            . "vm.voyage_id = v.id_voyage "
                                        . "AND "
                                            . "vs.id_vessel = sm.vessel_id "
                                        . "AND "
                                            . "p1.id_port = s.origin_id "
                                        . "AND "
                                            . "p2.id_port = s.destination_id "
                                        . "ORDER BY "
                                            . "id_subvoyage ASC"
                    );
//		$query = $this->db->query(
//			'SELECT
//				s.id_subvoyage, s.ETD, s.ETA, sm.departure_date, sm.id_subvoyage_management,
//				vs.vessel_code, vs.vessel,
//				v.voyage,
//				p1.id_port as origin_port_id,
//				p2.id_port as destination_port_id,
//				p1.port_code as origin_port_code,
//				p1.port as origin_port,
//				p1.terminal_fee as origin_terminal_fee,
//				p1.port_charge as origin_port_charge,
//				p2.port_code as destination_port_code,
//				p2.port as destination_port,
//				p2.terminal_fee as destination_terminal_fee,
//				p2.port_charge as destination_port_charge,
//				sf.amount as fare_per_leg
//			FROM
//				subvoyage s,
//				subvoyage_management sm,
//				voyage_management vm,
//				voyage v,
//				vessel vs,
//				port p1,
//				port p2,
//				subvoyage_fare sf
//			WHERE
//				s.id_subvoyage >= '. $origin_subvoyage_id .'
//			AND
//				s.id_subvoyage <= '. $destination_subvoyage_id .'
//			AND
//				vm.id_voyage_management = '. $voyage_management_id .'
//			AND
//				sm.subvoyage_id = s.id_subvoyage
//			AND
//				sm.voyage_management_id = vm.id_voyage_management
//			AND
//				vm.voyage_id = s.voyage_id
//			AND
//				vm.voyage_id = v.id_voyage
//			AND
//				vs.id_vessel = sm.vessel_id
//			AND
//				p1.id_port = s.origin_id
//			AND
//				p2.id_port = s.destination_id
//			ORDER BY
//				id_subvoyage ASC'
//		);
		
		 // return count immediately
		if ($count)
				return count($query->result());
			
		return $query->result();
	}
	
	// public function get_trip_details($id_voyage = "")
	// {
		// $query = $this->db->query(
			// 'SELECT
                            // sm.id_subvoyage_management,
                            // p.voyage_id,
                            // p.origin_id as origin,
                            // p.id_subvoyage as origin_subvoyage_id,
                            // p.ETD as ETD,
                            // sp.destination_id as destination,
                            // sp.id_subvoyage as destination_subvoyage_id,
                            // sp.ETA as ETA,
                            // sm.departure_date as dept_date,
                            // v.voyage as voyage_code,
                            // p1.port_code as port_origin,
                            // p2.port_code as port_destination,
							// ve.vessel_code,
							// ve.vessel
			// FROM 
                            // subvoyage p, 
                            // subvoyage sp, 
                            // subvoyage_management sm, 
                            // voyage_management vm,
                            // voyage v,
                            // port p1,
                            // port p2,
							// vessel ve
			// WHERE
                            // p1.id_port = p.origin_id
                        // AND
                            // p2.id_port = sp.destination_id
			// AND
                            // p.voyage_id = v.id_voyage
			// AND
                            // p.voyage_id = vm.voyage_id
			// AND
                            // p.id_subvoyage = sm.subvoyage_id
			// AND
                            // sm.voyage_management_id = vm.id_voyage_management
			// AND	
                            // p.voyage_id = sp.voyage_id 
			// AND
                            // p.id_subvoyage = sp.id_subvoyage
			// AND
							// v.id_voyage = '. $id_voyage .'
			// GROUP BY
                            // p.voyage_id
                        // ORDER BY
                            // sm.departure_date ASC'
		// );
		
		// return $query->result();
	// }
	
}

/* End of file booking_model.php */
/* Location: ./application/modules_core/adminpanel/models/booking/booking_model.php */