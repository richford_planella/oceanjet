<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');



class Booking_Status_Model extends MY_Model
{
    public $id_booking_status = NULL;

    public $booking_status;

    protected $table = 'booking_status';

    protected $identifier = 'id_booking_status';

    protected $foreign_key = 'booking_status_id';

    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    public function getFields()
    {
        if (isset($this->id))
            $fields['id_booking_status'] = (int)($this->id);
            $fields['booking_status'] = $this->booking_status;
            $fields['enabled'] = $this->enabled;

        return $fields;
    }

    public function checkColumn($column = '')
    {
        return parent::checkColumn($column, 'booking_status');
    }

    public function displayList($where = array(), $order_by = array('id_booking_id' => 'ASC'), $count = FALSE)
    {
        self::_select();
        self::_join();
        self::_where($where);
        self::_orderby($order_by);

        if($count)
            return count(parent::get('booking_status bs'));

        return parent::get('booking_status bs');
    }

    public function countFromUser()
    {
        self::_where(array($this->foreign_key => $this->id));

        return $this->db->count_all_results('booking b');
    }

    public function getValue($fieldname = '', $where = array())
    {
        $this->db->select($fieldname);
        $this->db->where($where);

        $query = $this->db->get('booking_status bs');
        $row = $query->row();

        if($row)
            return $row->{$fieldname};

        return false;
    }

    private function _select()
    {
        $this->db->select('*');
    }

    private function _join()
    {

    }

    private function _where($where)
    {
        $this->db->where($where);
    }

    private function _orderby($order_by = array('id_booking_status' => 'ASC'))
    {

    }

    private function _limit($limit, $offset)
    {

    }
}