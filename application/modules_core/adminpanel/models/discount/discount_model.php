<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Discount Model Class
|--------------------------------------------------------------------------
|
| Handles the announcement table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Discount_Model extends MY_Model
{
	/* int id_discount */
	public $id_discount = NULL;
        
	/* string discount code */
	public $discount_code;
	
	/* string name */
	public $discount;
	
	/* string type */
	public $discount_type;
	
	/* decimal amount */
	public $discount_amount;
	
	/* age tag if not empty */
	public $with_age;
	
	/* int age from */
	public $age_from;

	/* int age to */
	public $age_to;
	
	/* int vat exempted */
	public $vat_exempt;
	
	/* int require_id */
	public $require_id;
	
	/* int status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'discounts';

	/* string table identifier */
	protected $identifier = 'id_discount';
	
	/* int foreign key */
	protected $foreign_key = 'discount_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_discount']			= (int)($this->id);
			$fields['discount_code']		=  strtoupper($this->discount_code);
			$fields['discount'] 				=  $this->discount;
			$fields['discount_type']		=	$this->discount_type;
			$fields['discount_amount']	=	$this->discount_amount;
            $fields['with_age']				=	(int)($this->with_age);
            $fields['age_from']				=	(int)($this->age_from);
            $fields['age_to']					=	(int)($this->age_to);
            $fields['vat_exempt']			=	(int)($this->vat_exempt);
            $fields['require_id']			=	(int)($this->require_id);
			$fields['enabled'] 				=  (int)($this->enabled);
			$fields['date_added'] 			=  $this->date_added;
			$fields['date_update'] 		=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'discounts');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Discount Master List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_discount' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		// self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('discounts u'));

		return parent::get('discounts u');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Return count result
	 *
	 * @access	private
	 * @return		int
	 */
	private function _countResult($class)
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));
		
		// Return class
		return $this->db->count_all_results($class);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Add results for count
	 *
	 * @access	public
	 * @return		int
	 */
	public function countForeignKey()
	{
		// Initialize
		$fk = array();

		// Get count
		$fk["booking"]					= $this->_countResult('booking b');
		$fk["reservation"]				= $this->_countResult('reservation r');
		$fk["reservation_booking"]	= $this->_countResult('reservation_booking c');
	
		// Return sum
		return array_sum($fk);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('discounts u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', '');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_discount' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file announcement_model.php */
/* Location: ./application/modules_core/adminpanel/models/announcement/announcement_model.php */