<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Outlet Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the outlet table on the DB
  |
  | @category	Model
  | @author		Kenneth Bahia
 */

class Subvoyage_Model extends MY_Model
{
    /* int voyage id */

    public $id_subvoyage = NULL;

    /* string voyage_no */
    public $voyage_id;

    /* int vessel id */
    public $vessel_id;

    /* datetime ETD */
    public $ETD;

    /* datetime ETA */
    public $ETA;

    /* int id */
    public $origin_id;

    /* int id */
    public $destination_id;
    
    /* decimal points redemption */
    public $points_redemption;
    
    /* string table name */
    protected $table = 'subvoyage';

    /* string table identifier */
    protected $identifier = 'id_subvoyage';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 	public
     * @return		array
     */
    public function getFields()
    {
        if (isset($this->id_subvoyage))
            $fields['id_subvoyage'] = (int) ($this->id_subvoyage);
        $fields['voyage_id'] = $this->voyage_id;
        $fields['vessel_id'] = $this->vessel_id;
        $fields['ETD'] = $this->ETD;
        $fields['ETA'] = $this->ETA;
        $fields['origin_id'] = $this->origin_id;
        $fields['destination_id'] = $this->destination_id;
        $fields['points_redemption'] = $this->points_redemption;

        return $fields;
    }

    // ------------------------------------------------------------------------

    /*
     * Check if column exist
     *
     * @access 	public
     * @return		array
     */
    public function checkColumn($column = '')
    {
        return parent::checkColumn($column, 'subvoyage');
    }

    // --------------------------------------------------------------------

    /*
     * Display user list
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function displayList($where = array(), $order_by = array('id_subvoyage' => 'ASC'), $count = FALSE)
    {
        // SELECT
        self::_select();

        // JOIN
        self::_join();

        // WHERE
        self::_where($where);

        // ORDER BY
        self::_orderby($order_by);

        // return count immediately
        if ($count)
            return count(parent::get('subvoyage u'));

        return parent::get('subvoyage u');
    }

    // --------------------------------------------------------------------

    /*
     * Count for outlet
     *
     * @access	public
     * @return		int
     */
    public function countFromUser()
    {
        // WHERE
        self::_where(array($this->foreign_key => $this->id));

        return $this->db->count_all_results('accounts a');
    }

    // --------------------------------------------------------------------

    /*
     * Get user field value
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function getValue($fieldname = '', $where = array())
    {
        // SELECT
        $this->db->select($fieldname);

        // WHERE
        $this->db->where($where);

        $query = $this->db->get('subvoyage u');
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    // --------------------------------------------------------------------

    /*
     * SELECT
     *
     * @return		void
     */
    private function _select()
    {
        $this->db->select('u.*,u.id_subvoyage,u.voyage_id,vo.voyage,u.vessel_id,u.ETD,u.ETA,u.origin_id,u.destination_id,p.port AS origin,p2.port AS destination,p.port AS origin_port,p2.port AS destination_port,v.`vessel`');
    }

    // --------------------------------------------------------------------

    /*
     * JOIN
     *
     * @return		void
     */
    private function _join()
    {
        $this->db->join('voyage vo', 'u.voyage_id = vo.id_voyage', 'left');
        $this->db->join('vessel v', 'u.vessel_id = v.id_vessel', 'left');
        $this->db->join('port p', 'u.origin_id = p.id_port', 'left');
        $this->db->join('port p2', 'u.destination_id = p2.id_port', 'left');
    }

    // --------------------------------------------------------------------

    /*
     * WHERE
     *
     * @return		void
     */
    private function _where($where)
    {
        $this->db->where($where);
    }

    // --------------------------------------------------------------------

    /*
     * ORDER BY
     *
     * @return		void
     */
    private function _orderby($order_by = array('id_subvoyage' => 'ASC'))
    {
        if (!empty($order_by))
        {
            foreach ($order_by as $field => $direction)
                $this->db->order_by($field, $direction);
        }
    }

    // --------------------------------------------------------------------

    /*
     * LIMIT - OFFSET
     *
     * @return		void
     */
    private function _limit($limit, $offset)
    {
        if ($offset > 0)
        {
            $offset = ($offset * $limit) - $limit;
            $this->db->limit($limit, $offset);
        }
    }

}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/outlet_model.php */