<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Evoucher Series Model Class
|--------------------------------------------------------------------------
|
| Handles the evoucher series table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Evoucher_Series_Model extends MY_Model
{
	/* int evoucher series  id */
	public $id_evoucher_series = NULL;
	
        /* id evoucher id */
	public $evoucher_id;
        
	/* string evoucher code */
	public $evoucher_code;
        
        /* decimal evoucher amount */
	public $amount;
        
        /* string estatus */
	public $estatus;
        
        /* date date generated */
	public $date_generated;
        
        /* date date used */
	public $date_used;
        
        /* date expiration used */
	public $expiration_date;
        
	/* string table name */
	protected $table = 'evoucher_series';

	/* string table identifier */
	protected $identifier = 'id_evoucher_series';

	/* string foreign key */
	protected $foreign_key = 'evoucher_series_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_evoucher_series'] = (int)($this->id);
		$fields['evoucher_id'] =  $this->evoucher_id;
                $fields['evoucher_code'] =  $this->evoucher_code;
                $fields['amount'] =  $this->amount;
                $fields['estatus'] =  $this->estatus;
                $fields['date_generated'] =  $this->date_generated;
                $fields['expiration_date'] =  $this->expiration_date;
		$fields['date_used'] =  $this->date_used;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'evoucher') OR parent::checkColumn($column, 'evoucher_series') OR parent::checkColumn($column, 'port') OR parent::checkColumn($column, 'accomodation');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display evoucher series list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_evoucher_series' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('evoucher_series es'));

		return parent::get('evoucher_series es');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for evoucher series to ticket
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('ticket t');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get evoucher series field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('evoucher_series es');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('es.*,e.evoucher,e.origin_id,e.destination_id,'
                        . 'e.accommodation_id,e.quantity,e.evoucher_type,e.evoucher_amount,e.validity,e.status,'
                        . 'o.outlet_code,o.outlet_type,o.outlet,'
                        . 'p.port_code as orig_port_code,p.port as orig_port,'
                        . 'pr.port_code as des_port_code,pr.port as des_port,'
                        . 'a.accommodation_code,a.accommodation');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('evoucher e', 'e.id_evoucher = es.evoucher_id', 'left');
                $this->db->join('outlet o', 'o.id_outlet = e.outlet_id', 'left');
                $this->db->join('port p', 'p.id_port = e.origin_id', 'left');
                $this->db->join('port pr', 'pr.id_port = e.destination_id', 'left');
                $this->db->join('accommodation a', 'a.id_accommodation = e.accommodation_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_evoucher_series' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file evoucher_series_model.php */
/* Location: ./application/modules_core/adminpanel/models/evoucher/evoucher_series_model.php */