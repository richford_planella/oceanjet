<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Evoucher Model Class
|--------------------------------------------------------------------------
|
| Handles the evoucher table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Evoucher_Model extends MY_Model
{
	/* int evoucher id */
	public $id_evoucher = NULL;
	
        /* string evoucher code */
	public $evoucher;
        
	/* int origin port id */
	public $origin_id;
        
        /* int destination port id */
	public $destination_id;
        
        /* int accomodation id */
	public $accommodation_id;
        
        /* int quantity */
	public $quantity;

	/* int allocated evoucher */
	public $alloted_evoucher;
        
        /* int generated evoucher */
	public $generated_evoucher;
        
        /* int cancelled evoucher */
	public $cancelled_evoucher;
        
        /* tinyint evoucher type */
	public $evoucher_type;
        
        /* decimal evoucher amount */
	public $evoucher_amount;
        
        /* int validity */
	public $validity;
        
        /* int outlet id */
	public $outlet_id;
        
        /* string status */
	public $status;
        
	/* string table name */
	protected $table = 'evoucher';

	/* string table identifier */
	protected $identifier = 'id_evoucher';

	/* string foreign key */
	protected $foreign_key = 'evoucher_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_evoucher'] = (int)($this->id);
		$fields['evoucher'] =  $this->evoucher;
                $fields['origin_id'] =  $this->origin_id;
                $fields['destination_id'] =  $this->destination_id;
                $fields['accommodation_id'] =  $this->accommodation_id;
                $fields['quantity'] =  $this->quantity;
		$fields['alloted_evoucher'] =  $this->alloted_evoucher;
                $fields['generated_evoucher'] =  $this->generated_evoucher;
                $fields['cancelled_evoucher'] =  $this->cancelled_evoucher;
                $fields['evoucher_type'] =  $this->evoucher_type;
                $fields['evoucher_amount'] =  $this->evoucher_amount;
                $fields['validity'] =  $this->validity;
                $fields['outlet_id'] =  $this->outlet_id;
                $fields['status'] =  $this->status;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'evoucher') OR parent::checkColumn($column, 'port') OR parent::checkColumn($column, 'accomodation');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display evoucher list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_evoucher' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('evoucher e'));

		return parent::get('evoucher e');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for evoucher to evoucher series
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('evoucher_series es');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get evoucher field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('evoucher e');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('e.*,p.port_code as orig_port_code,p.port as orig_port,pr.port_code as des_port_code,pr.port as des_port,a.accommodation_code,a.accommodation,o.outlet_code,o.outlet_type,o.outlet');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('port p', 'p.id_port = e.origin_id', 'left');
                $this->db->join('port pr', 'pr.id_port = e.destination_id', 'left');
                $this->db->join('outlet o', 'o.id_outlet = e.outlet_id', 'left');
                $this->db->join('accommodation a', 'a.id_accommodation = e.accommodation_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_evoucher' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file evoucher_model.php */
/* Location: ./application/modules_core/adminpanel/models/evoucher/evoucher_model.php */