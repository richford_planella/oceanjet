<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Passengers Baggage Check In Model Class
|--------------------------------------------------------------------------
|
| Handles the rolling cargo check in table on the DB
|
| @category	Model
| @author		Gian Asuncion
*/

class Passenger_Baggage_Checkin_Model extends MY_Model
{
    /* int id_rule_set */
    public $id_passenger_baggage_checkin = NULL;

    /* int voyage_management_id */
    public $voyage_management_id;

    /* string reference_no */
    public $reference_no;

    /* int ticket_series_info_id  */
    public $ticket_series_info_id;

    /* int passenger_id */
    public $passenger_id;

    /* int rule_type_id */
    public $rule_type_id;

    /* int excess_baggage_id */
    public $excess_baggage_id;

    /* int weight */
    public $weight;

    /* int paid status */
    public $paid_status;

    /* decimal quantity */
    public $quantity;

    /* decimal cash */
    public $cash;

    /* decimal attended_baggage */
    public $attended_baggage;

    /* decimal total_amount*/
    public $total_amount;

    /* datetime date_added */
    public $date_added;

    /* datetime date_update */
    public $date_update;

    /* string table name */
    protected $table = 'passenger_baggage_checkin';

    /* string table identifier */
    protected $identifier = 'id_passenger_baggage_checkin';

    /* string foreign key */
    protected $foreign_key = 'passenger_baggage_checkin_id';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 	public
     * @return		array
     */
    public function getFields()
    {
        if (isset($this->id))
            $fields['id_passenger_baggage_checkin'] = (int)($this->id);
        $fields['voyage_management_id'] = $this->voyage_management_id;
        $fields['reference_no'] = $this->reference_no;
        $fields['ticket_series_info_id'] = $this->ticket_series_info_id;
        $fields['passenger_id'] = $this->passenger_id;
        $fields['rule_type_id'] = $this->rule_type_id;
        $fields['excess_baggage_id'] = $this->excess_baggage_id;
        $fields['weight'] = $this->weight;
        $fields['quantity'] = $this->quantity;
        $fields['cash']      = $this->cash;
        $fields['paid_status']      = $this->paid_status;
        $fields['attended_baggage'] = $this->attended_baggage;
        $fields['total_amount'] = $this->total_amount;
        $fields['date_added'] = $this->date_added;
        $fields['date_update'] = $this->date_update;

        return $fields;
    }


    // ------------------------------------------------------------------------

    /*
     * Get Passenger Full name and Free Baggage Weight
     * Booking Status set to 5
     *
     * @access 	public
     * @param $ticket_no
     * @return		array
     */
    public function passengerDetails($ticket_no)
    {
        $query = $this->db->query("SELECT
                                        passenger_id,
                                        id_ticket_series_info,
                                        CONCAT(p.`lastname`, ', ',p.`firstname`,' ', SUBSTR(p.`middlename`,1,1),'.') AS passenger_name,
                                        `id_rule_type`,
                                        `rule_type_amount`,
                                        subvoyage_management_id,
                                        `transaction_ref`,
                                        b.booking_status_id,
                                        b.id_booking,
                                        b.passenger_valid_id_number,
                                        pf.accommodation_id
                                    FROM
                                        `passenger` AS p
                                    LEFT JOIN
                                        `booking` AS b
                                    ON
                                        b.`passenger_id` = p.`id_passenger`
                                    LEFT JOIN
                                      `subvoyage_management` as svm
                                    ON
                                      svm.`id_subvoyage_management` = b.`subvoyage_management_id`
                                    LEFT JOIN
                                        `ticket_series_info` AS tsi
                                    ON
                                        tsi.`id_ticket_series_info` = b.`ticket_series_info_id`
                                    LEFT JOIN
                                        `passenger_fare` AS pf
                                    ON
                                        pf.`id_passenger_fare` = b.`passenger_fare_id`
                                    LEFT JOIN
                                        `rule_type` AS rt
                                    ON
                                        rt.`rule_set_id`       = pf.`rule_set_id`
                                    WHERE
                                        (tsi.`ticket_no`        = '$ticket_no')
                                    AND
                                        rt.`rule_type_name_id` = (SELECT `id_rule_type_name` FROM `rule_type_name` WHERE `free_baggage` = 1 LIMIT 0,1)
                                    GROUP BY
                                        b.id_booking");

        return $query->result();
    }

    // ------------------------------------------------------------------------

    /*
     * Check if column exist
     *
     * @access 	public
     * @return		array
     */
    public function checkColumn($column = '')
    {
        return parent::checkColumn($column, 'passenger_baggage_checkin');
    }

    // --------------------------------------------------------------------

    /*
     * Display Rolling Cargo Check In List
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function displayList($where = array(), $order_by = array('id_passenger_baggage_checkin' => 'ASC'), $count = FALSE)
    {
        // SELECT
        self::_select();

        // JOIN
        self::_join();

        // WHERE
        self::_where($where);

        // ORDER BY
        self::_orderby($order_by);

        // return count immediately
        if ($count)
            return count(parent::get('passenger_baggage_checkin u'));

        return parent::get('passenger_baggage_checkin u');
    }


    // --------------------------------------------------------------------

    /*
     * Count for user to user
     *
     * @access	public
     * @return		int
     */
    public function countFromUser()
    {
        // WHERE
        self::_where(array($this->foreign_key => $this->id));

        return $this->db->count_all_results('accounts a');
    }

    // --------------------------------------------------------------------

    /*
     * Get user field value
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function getValue($fieldname = '', $where = array())
    {
        // SELECT
        $this->db->select($fieldname);

        // WHERE
        $this->db->where($where);

        $query = $this->db->get('passenger_baggage_checkin u');
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    // --------------------------------------------------------------------

    /*
     * SELECT
     *
     * @return		void
     */
    private function _select()
    {
        $this->db->select("u.*,
                           tsi.ticket_no,
                           p.lastname,
                           p.firstname,
                           p.middlename,
                           p.contactno,
                           p.gender,
                           u.total_amount,
                           u.transaction_ref,
                           u.reference_no,
                           u.total_amount,
                           u.quantity,
                           u.paid_status,
                           u.free_baggage,
                           u.amount_tendered,
                           ab.attended_baggage,
                           ab.description,
                           ab.unit_of_measurement,
                           pc.vessel_seats");
    }

    // --------------------------------------------------------------------

    /*
     * JOIN
     *
     * @return		void
     */
    private function _join()
    {
        $this->db->join('booking b', 'b.id_booking = u.booking_id', 'left');
        $this->db->join('passenger_checkin pc', 'pc.booking_id = u.booking_id', 'left');
        $this->db->join('ticket_series_info tsi', 'b.ticket_series_info_id = tsi.id_ticket_series_info', 'left');
        $this->db->join('passenger p', 'p.id_passenger = b.passenger_id', 'left');
        $this->db->join('subvoyage_management svm', 'svm.id_subvoyage_management = b.subvoyage_management_id', 'left');
        $this->db->join('attended_baggage ab', 'ab.id_attended_baggage = u.attended_baggage_id', 'left');
    }

    // --------------------------------------------------------------------

    /*
     * WHERE
     *
     * @return		void
     */
    private function _where($where)
    {
        $this->db->where($where);
    }

    // --------------------------------------------------------------------

    /*
     * ORDER BY
     *
     * @return		void
     */
    private function _orderby($order_by = array('id_passenger_baggage_checkin' => 'ASC'))
    {
        if (!empty($order_by)) {
            foreach ($order_by as $field => $direction)
                $this->db->order_by($field, $direction);
        }
    }

    // --------------------------------------------------------------------

    /*
     * LIMIT - OFFSET
     *
     * @return		void
     */
    private function _limit($limit, $offset)
    {
        if ($offset > 0) {
            $offset = ($offset * $limit) - $limit;
            $this->db->limit($limit, $offset);
        }
    }

    /*
     * Insert to Transaction table
     * */
    public function insertTransaction($transaction_ref,$data)
    {
        $this->db->delete('transaction',array('transaction_ref' => $transaction_ref));
        $this->db->insert('transaction',$data);
    }

}

/* End of file rolling_cargo_checkin_model.php */
/* Location: ./application/modules_core/adminpanel/models/rolling_cargo_checkin/rolling_cargo_checkin_model.php */