<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Port Model Class
|--------------------------------------------------------------------------
|
| Handles the port table on the DB
|
| @category	Model
| @author		Richford Planella
*/
class Port_Model extends MY_Model
{
	/* int id_outlet */
	public $id_port = NULL;
        
	/* varchar port code */
	public $port_code;
	
	/* varchar port description */
	public $port;
	
	/* decimal terminal fee */
	public $terminal_fee;
	
	/* decimal port charge */
	public $port_charge;
	
	/* int status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'port';

	/* string table identifier */
	protected $identifier = 'id_port';
	
	/* foreign key */
	protected $foreign_key = 'origin_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_port']				= (int)($this->id);
			$fields['port_code']			= $this->port_code;
			$fields['port'] 				= $this->port;
            $fields['terminal_fee'] 		= $this->terminal_fee;
            $fields['port_charge'] 			= $this->port_charge;
			$fields['enabled'] 				= $this->enabled;
			$fields['date_added'] 			= $this->date_added;
			$fields['date_update'] 			= $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'port');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_port' => 'ASC'), $count = FALSE, $pass_id = array())
	{
		// SELECT
		self::_select();
		
		// JOIN
		// self::_join();

		// WHERE
		self::_where($where);
                
                if(!empty($pass_id))
                    parent::orWhere($pass_id);
                
		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('port u'));

		return parent::get('port u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// OR WHERE
		self::_orwhere(array('origin_id' => $this->id, 'destination_id' => $this->id));

		return $this->db->count_all_results('voyage a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('port u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * OR WHERE
	 *
	 * @return		void
	 */
	private function _orwhere($where)
	{
		$this->db->or_where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_port' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file port_model.php */
/* Location: ./application/modules_core/adminpanel/models/port/port_model.php */