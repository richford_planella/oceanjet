<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Announcement Visibility Model Class
|--------------------------------------------------------------------------
|
| Handles the announcement visibility table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Announcement_Visibility_Model extends MY_Model
{
	/* int id_announcement */
	public $id_announcement_visibility = NULL;
        
	/* int announcement id */
	public $announcement_id;
	
	/* int user role */
	public $user_profile_id;
	
	/* string table name */
	protected $table = 'announcement_visibility';

	/* string table identifier */
	protected $identifier = 'id_announcement_visibility';
	
	/* int foreign key */
	protected $foreign_key = 'announcement_visibility_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_announcement_visibility']	= (int)($this->id);
			$fields['announcement_id'] 				=  $this->announcement_id;
			$fields['user_profile_id'] 					=  $this->user_profile_id;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'announcement_visibility');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Announcement visibility List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_announcement_visibility' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('announcement_visibility u'));

		return parent::get('announcement_visibility u');
	}
        
        
  // --------------------------------------------------------------------
	
	/*
	 * Return count result
	 *
	 * @access	private
	 * @return		int
	 */
	private function _countResult($class)
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));
		
		// Return class
		return $this->db->count_all_results($class);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Add results for count
	 *
	 * @access	public
	 * @return		int
	 */
	public function countForeignKey()
	{
		// Initialize
		$fk = array();

		// Get count / technically zero
		$fk["announcement"] = 0;
	
		// Return sum
		return array_sum($fk);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('announcement_visibility u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*, up.id_user_profile, up.user_profile, up.user_description, up.is_outlet, up.has_mobile_access, up.has_transaction, up.session_timeout, up.user_level_id');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('user_profile up', 'u.user_profile_id = up.id_user_profile', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_announcement_visibility' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file announcement_visibility_model.php */
/* Location: ./application/modules_core/adminpanel/models/announcement_visibility/announcement_visibility_model.php */