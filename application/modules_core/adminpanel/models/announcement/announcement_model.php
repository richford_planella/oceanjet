<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Announcement Model Class
|--------------------------------------------------------------------------
|
| Handles the announcement table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Announcement_Model extends MY_Model
{
	/* int id_announcement */
	public $id_announcement = NULL;
        
	/* string announcement code */
	public $announcement_code;
	
	/* string title */
	public $announcement;
	
	/* string content */
	public $announcement_content;
	
	/* string visibility */
	public $announcement_visibility_id;
	
	/* int status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'announcement';

	/* string table identifier */
	protected $identifier = 'id_announcement';
	
	/* int foreign key */
	protected $foreign_key = 'announcement_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_announcement']				= (int)($this->id);
			$fields['announcement']					=  $this->announcement;
			$fields['announcement_code'] 		=  strtoupper($this->announcement_code);
            $fields['announcement_content']		=	$this->announcement_content;
			$fields['enabled'] 							=  $this->enabled;
			$fields['date_added'] 						=  $this->date_added;
			$fields['date_update'] 					=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'announcement');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Announcement List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_announcement' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		// self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('announcement u'));

		return parent::get('announcement u');
	}
        
        
 // --------------------------------------------------------------------
	
	/*
	 * Return count result
	 *
	 * @access	private
	 * @return		int
	 */
	private function _countResult($class)
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));
		
		// Return class
		return $this->db->count_all_results($class);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Add results for count
	 *
	 * @access	public
	 * @return		int
	 */
	public function countForeignKey()
	{
		// Initialize
		$fk = array();

		// Get count / technically zero
		$fk["announcement"] = 0;
	
		// Return sum
		return array_sum($fk);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('announcement u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', '');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_announcement' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file announcement_model.php */
/* Location: ./application/modules_core/adminpanel/models/announcement/announcement_model.php */