<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Payment Method Model Class
|--------------------------------------------------------------------------
|
| Handles the payment method table on the DB
|
| @category	Model
| @author       Baladeva Juganas
*/
class Payment_Method_Model extends MY_Model
{
	/* int payment method id */
	public $id_payment_method = NULL;
        
	/* str payment method */
	public $payment_method;
		
	/* int status */
	public $enabled;
	        
	/* string table name */
	protected $table = 'payment_method';

	/* string table identifier */
	protected $identifier = 'id_payment_method';
	
	/* foreign key */
	protected $foreign_key = 'payment_method_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
                    $fields['id_payment_method']= (int)($this->id);
                $fields['payment_method']	= $this->payment_method;
                $fields['enabled']              = $this->enabled;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'payment_method');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display payment method list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_payment_method' => 'ASC'), $count = FALSE, $pass_id = array())
	{
		// SELECT
		self::_select();
		
		// WHERE
		self::_where($where);
                
                if(!empty($pass_id))
                    parent::orWhere($pass_id);
                
		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('payment_method pm'));

		return parent::get('payment_method pm');
	}
        
                
	// --------------------------------------------------------------------
	
	/*
	 * Get payment method field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('payment_method pm');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('pm.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * OR WHERE
	 *
	 * @return		void
	 */
	private function _orwhere($where)
	{
		$this->db->or_where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_payment_method' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file payment_method_model.php */
/* Location: ./application/modules_core/adminpanel/models/payment_method/payment_method_model.php */