<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| ticket Series Ref Class
|--------------------------------------------------------------------------
|
| Handles the outlet table on the DB
|
| @ticket Series Ref Model
| @author		Gian Asuncion
*/

class Ticket_Series_Ref_Model extends MY_Model
{
    /* int ticket series ref id */
    public $id_ticket_series_info = NULL;

    /* int parent table identifier */
    public $ticket_series_id;

    /*int ticket no*/
    public $ticket_no;

    /* int issued */
    public $issued;

    /* int user agent id */
    public $user_id;

    /* int enabled */
    public $enabled;

    /* datetime date_added */
    public $date_added;

    /* datetime date_update */
    public $date_updated;

    /* string table name */
    protected $table = 'ticket_series_info';

    /* string table identifier */
    protected $identifier = 'id_ticket_series_info';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    /*
     * @desc disable child ref
     *
     * */
    public function toggleRefStatus($parent_id,$enabled,$reverse)
    {
        if($reverse==true)
            $enabled = ($enabled) == 1 ? 0 : 1;

        $this->db->where('ticket_series_id', $parent_id);
        $this->db->update($this->table, array('enabled'=> $enabled));
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 	public
     * @return		array
     */
    public function getFields()
    {
        if (isset($this->id))
            $fields['id_ticket_series_info']    = (int)($this->id);
            $fields['ticket_series_id']         = $this->ticket_series_id;
            $fields['ticket_no']                = $this->ticket_no;
            $fields['issued']                   = $this->issued;
            $fields['enabled']                  = $this->enabled;
            $fields['date_added']               = $this->date_added;
            $fields['date_updated']             = $this->date_updated;

        return $fields;
    }

    // ------------------------------------------------------------------------

    /*
     * Check if column exist
     *
     * @access 	public
     * @return		array
     */
    public function checkColumn($column = '')
    {
        return parent::checkColumn($column, 'ticket_series_info');
    }

    // --------------------------------------------------------------------

    /*
     * Display  list
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function displayList($where = array(), $order_by = array('id_ticket_series_info' => 'ASC'),$count = FALSE,$limit=0,$offset=0)
    {
        // SELECT
        self::_select();

        // WHERE
        self::_where($where);

        // ORDER BY
        self::_orderby($order_by);

        // LIMIT
        self::_limit($limit, $offset);

        // return count immediately
        if ($count)
            return count(parent::get('ticket_series_info ts'));

        return parent::get('ticket_series_info ts');
    }

    /*
    * Get assigned tickets group per agent
    *
    * @access	public
    * @param ticket series ID
    * @return		object
    */
    public function getAssignedTickets($ticket_series_id)
    {
        $query = $this->db->query("SELECT
                            COUNT(`id_ticket_series_info`) AS assigned_tickets,
                            SUM(IF(`ticket_series_info`.`issued` = 1, 1,0)) AS issued,
                            CONCAT(`firstname`,\" \",`middlename`,\" \",`lastname`) AS agent_name,
                            `ticket_series_info`.`user_id` as user_id
                        FROM
                            `ticket_series`
                        LEFT JOIN
                            `ticket_series_info`
                        ON
                            `ticket_series`.`id_ticket_series` = `ticket_series_info`.`ticket_series_id`
                        RIGHT JOIN
                            `user_account`
                        ON
                            `user_account`.`user_id` = `ticket_series_info`.`user_id`
                        WHERE
                            `ticket_series_info`.`ticket_series_id` = $ticket_series_id
                        GROUP BY
                            `user_account`.`user_id`
                        ORDER BY
                            agent_name ASC");

        return $query->result();
    }



    /*
     * Get user field value
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function getValue($fieldname = '', $where = array())
    {
        // SELECT
        $this->db->select($fieldname);

        // WHERE
        $this->db->where($where);

        $query = $this->db->get('ticket_series_info ts');
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    // --------------------------------------------------------------------

    /*
     * SELECT
     *
     * @return		void
     */
    private function _select()
    {
        $this->db->select('*');
    }

    // --------------------------------------------------------------------

    /*
     * JOIN
     *
     * @return		void
     */
    private function _join()
    {
        $this->db->join('', '', 'left');
    }

    // --------------------------------------------------------------------

    /*
     * WHERE
     *
     * @return		void
     */
    private function _where($where)
    {
        $this->db->where($where);
    }

    // --------------------------------------------------------------------

    /*
     * ORDER BY
     *
     * @return		void
     */
    private function _orderby($order_by = array('id_outlet' => 'ASC'))
    {
        if (!empty($order_by)) {
            foreach ($order_by as $field => $direction)
                $this->db->order_by($field, $direction);
        }
    }

    // --------------------------------------------------------------------

    /*
     * LIMIT - OFFSET
     *
     * @return		void
     */
    private function _limit($limit, $offset)
    {
        if ($offset > 0) {
            $offset = ($offset * $limit) - $limit;
            $this->db->limit($limit, $offset);
        } else {
			$this->db->limit($limit, $offset);
		}
    }
}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/ticket_series_ref_model.php */