<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Ticket Series Class
|--------------------------------------------------------------------------
|
| Handles the outlet table on the DB
|
| @Ticket Series Model
| @author		Gian Asuncion
*/

class Ticket_Series_Model extends MY_Model
{
    /* int ticket series id */
    public $id_ticket_series = NULL;

    /* varchar ticket start from */
    public $ticket_starts_from;

    /* varchar ticket end to */
    public $ticket_ends_to;

    /*int num of generated tickets*/
    public $no_tickets;

    /* int status */
    public $enabled;

    /* datetime date_added */
    public $date_added;

    /* datetime date_update */
    public $date_updated;

    /* string table name */
    protected $table = 'ticket_series';

    /* string table identifier */
    protected $identifier = 'id_ticket_series';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 	public
     * @return		array
     */
    public function getFields()
    {
        if (isset($this->id))
            $fields['id_ticket_series']     = (int)($this->id);
            $fields['ticket_starts_from']   = $this->ticket_starts_from;
            $fields['ticket_ends_to']       = $this->ticket_ends_to;
            $fields['enabled']              = $this->enabled;
            $fields['no_tickets']           = $this->no_tickets;
            $fields['date_added']           = $this->date_added;
            $fields['date_updated']         = $this->date_updated;

        return $fields;
    }

    // ------------------------------------------------------------------------

    /*
     * Check if column exist
     *
     * @access 	public
     * @return		array
     */
    public function checkColumn($column = '')
    {
        return parent::checkColumn($column, 'ticket_series');
    }

    // --------------------------------------------------------------------

    /*
     * Display  list
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function displayList($where = array(), $order_by = array('id_ticket_series' => 'ASC'), $count = FALSE)
    {
        // SELECT
        self::_select();

        // JOIN
        // self::_join();

        // WHERE
        self::_where($where);

        // ORDER BY
        self::_orderby($order_by);

        // return count immediately
        if ($count)
            return count(parent::get('ticket_series ts'));

        return parent::get('ticket_series ts');
    }


    // --------------------------------------------------------------------

    /*
     * Get list of tickets available: issued and not yet issued
     *
     * @access	public
     * @return		int
     */
    public function getGeneratedTickets()
    {
        $query = $this->db->query("SELECT
                                        `id_ticket_series`,
                                        `ticket_starts_from`,
                                        `ticket_ends_to`,
                                        `no_tickets`,
                                        `ticket_series`.`enabled` AS ts_enabled,
                                        SUM(IF(`ticket_series_info`.`issued` = 1, 1,0)) AS issued
                                    FROM
                                        `ticket_series`
                                    LEFT JOIN
                                        `ticket_series_info`
                                    ON
                                        `ticket_series`.`id_ticket_series` = `ticket_series_info`.`ticket_series_id`
                                    GROUP BY
                                        `id_ticket_series`");

        return $query->result();
    }

    // --------------------------------------------------------------------

    /*
     * Sum of all generated Ticket Series
     *
     * @access	public
     * @return		int
     */
    public function sumGeneratedTicket()
    {
        $prev_generated_ticket = self::getSum('no_tickets');
        return $prev_generated_ticket + 100000000001; //define starting point of ticket
    }

    // --------------------------------------------------------------------

    /*
     * Get user field value
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function getValue($fieldname = '', $where = array())
    {
        // SELECT
        $this->db->select($fieldname);

        // WHERE
        $this->db->where($where);

        $query = $this->db->get('ticket_series ts');
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    // --------------------------------------------------------------------

    /*
     * SELECT
     *
     * @return		void
     */
    private function _select()
    {
        $this->db->select('*');
    }

    // --------------------------------------------------------------------

    /*
     * JOIN
     *
     * @return		void
     */
    private function _join()
    {
        $this->db->join('', '', 'left');
    }

    // --------------------------------------------------------------------

    /*
     * WHERE
     *
     * @return		void
     */
    private function _where($where)
    {
        $this->db->where($where);
    }

    // --------------------------------------------------------------------

    /*
     * ORDER BY
     *
     * @return		void
     */
    private function _orderby($order_by = array('id_outlet' => 'ASC'))
    {
        if (!empty($order_by)) {
            foreach ($order_by as $field => $direction)
                $this->db->order_by($field, $direction);
        }
    }

    // --------------------------------------------------------------------

    /*
     * LIMIT - OFFSET
     *
     * @return		void
     */
    private function _limit($limit, $offset)
    {
        if ($offset > 0) {
            $offset = ($offset * $limit) - $limit;
            $this->db->limit($limit, $offset);
        }
    }
}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/ticket_series_model.php */