<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Gender Model Class
|--------------------------------------------------------------------------
|
| Handles the gender table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Gender_Model extends MY_Model
{
	/* int gender id */
	public $id_gender = NULL;
	
	/* string gender iso code */
	public $gender_iso_code;
        
        /* string gender code */
	public $gender_code;

	/* boolean status */
	public $gender_status;

	/* string table name */
	protected $table = 'gender';

	/* string table identifier */
	protected $identifier = 'id_gender';

	/* string foreign key */
	protected $foreign_key = 'gender_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_gender'] = (int)($this->id);
		$fields['gender_iso_code'] =  $this->gender_iso_code;
                $fields['gender_code'] =  $this->gender_code;
		$fields['gender_status'] =  $this->gender_status;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'gender');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Search gender details
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function searchList($where = array(), $order_by = array('id_gender' => 'ASC'), $count = FALSE, $limit = 25, $offset = 1)
	{		
		if (MY_Session::get('genderSearch'))
			foreach (MY_Session::get('genderSearch') as $fieldname => $value)
			{
				if ($value != '')
					if ($fieldname == 'gender_iso_code' OR $fieldname == 'gender_code')
						$this->db->like(array($fieldname => $value));
					else
						$this->db->where(array($fieldname => $value));
			}

		self::_where($where);
                
		// ORDER BY
		if (MY_Session::get('genderOrder') AND is_array(MY_Session::get('genderOrder')))
			$order_by = array(MY_Session::get('genderOrder', 'order') => MY_Session::get('genderOrder', 'by'));

		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('gender g'));

		// LIMIT - OFFSET
		self::_limit($limit, $offset);

		return parent::get('gender g');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display gender list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_gender' => 'ASC'), $count = FALSE)
	{
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('gender g'));

		return parent::get('gender g');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for gender to user
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('user_account ua');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get gender field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('gender g');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_gender' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file gender_model.php */
/* Location: ./application/modules_core/adminpanel/models/cms/gender_model.php */