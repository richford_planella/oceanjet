<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Civil Status Model Class
|--------------------------------------------------------------------------
|
| Handles the civil status table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Civil_Status_Model extends MY_Model
{
	/* int civil status id */
	public $id_civil_status = NULL;
	
	/* string civil status iso code */
	public $civil_status_iso_code;
        
        /* string civil status code */
	public $civil_status_code;

	/* boolean status */
	public $civil_status_status;

	/* string table name */
	protected $table = 'civil_status';

	/* string table identifier */
	protected $identifier = 'id_civil_status';

	/* string foreign key */
	protected $foreign_key = 'civil_status_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_civil_status'] = (int)($this->id);
		$fields['civil_status_iso_code'] =  $this->gender_iso_code;
                $fields['civil_status_code'] =  $this->gender_code;
		$fields['civil_status_status'] =  $this->gender_status;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'civil_status');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Search civil status details
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function searchList($where = array(), $order_by = array('id_civil_status' => 'ASC'), $count = FALSE, $limit = 25, $offset = 1)
	{		
		if (MY_Session::get('civil_statusSearch'))
			foreach (MY_Session::get('civil_statusSearch') as $fieldname => $value)
			{
				if ($value != '')
					if ($fieldname == 'civil_status_iso_code' OR $fieldname == 'civil_status_code')
						$this->db->like(array($fieldname => $value));
					else
						$this->db->where(array($fieldname => $value));
			}

		self::_where($where);
                
		// ORDER BY
		if (MY_Session::get('civil_statusOrder') AND is_array(MY_Session::get('civil_statusOrder')))
			$order_by = array(MY_Session::get('civil_statusOrder', 'order') => MY_Session::get('civil_statusOrder', 'by'));

		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('civil_status cv'));

		// LIMIT - OFFSET
		self::_limit($limit, $offset);

		return parent::get('civil_status cv');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display civil status list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_civil_status' => 'ASC'), $count = FALSE)
	{
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('civil_status cv'));

		return parent::get('civil_status cv');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for civil status to user account
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('user_account ua');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get civil status field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('civil_status cv');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_civil_status' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file civil_status_model.php */
/* Location: ./application/modules_core/adminpanel/models/cms/civil_status_model.php */