<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Booking Rolling Cargo Model Class
|--------------------------------------------------------------------------
|
| Handles the booking in rolling cargo table on the DB
|
| @category	Model
| @author	Ronald Meran
*/
class Booking_Roro_Model extends MY_Model
{
	/* int rolling cargo checkin id */
	public $id_booking_roro = NULL;
        
	/* int subvoyage management id */
	public $origin_id;
	public $destination_id;
	
	/* int rolling cargo id */
	public $rolling_cargo_id;

	/* int discount id */
	public $discount_id;

	/* int ticket series id */
	public $ticket_series_info_id;

	/* int booking source id  */
	public $booking_source_id;

	/* int booking source status id  */
	public $booking_status_id;
	
	/* string rolling cargo */
	public $rolling_cargo_description;
	
	/* string generated reference no */
	public $reference_no;
	
	/* string shipper first name */
	public $sender_first_name;
	
	/* string shipper middle name */
	public $sender_middle_name;
	
	/* string shipper last name */
	public $sender_last_name;
	
	/* string shipper contact no */
	public $sender_contact_no;

	/* date shipper birthday */
	public $sender_birthdate;
	
	/* string driver first name */
	public $driver_first_name;
	
	/* string driver middle name */
	public $driver_middle_name;
	
	/* string driver last name */
	public $driver_last_name;
	
	/* string driver contact no */
	public $driver_contact_no;

	/* date driver birthday */
	public $driver_birthdate;
	
	/* bool status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'booking_roro';

	/* string table identifier */
	protected $identifier = 'id_booking_roro';
	
	/* string foreign key */
	protected $foreign_key = 'booking_roro_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_booking_roro'] 				= (int)($this->id);
			$fields['origin_id']		=  $this->origin_id;
			$fields['destination_id']		=  $this->destination_id;
			$fields['rolling_cargo_id']				=  $this->rolling_cargo_id;
			$fields['discount_id']					=  $this->discount_id;
			$fields['ticket_series_info_id']		=  $this->ticket_series_info_id;
			$fields['booking_source_id']			=  $this->booking_source_id;
			$fields['booking_status_id']			=  $this->booking_status_id;
			$fields['rolling_cargo_description']	=  $this->rolling_cargo_description;
			$fields['reference_no']					=  $this->reference_no;
			$fields['sender_first_name']			=  $this->sender_first_name;
			$fields['sender_middle_name']			=  $this->sender_middle_name;
			$fields['sender_last_name']				=  $this->sender_last_name;
			$fields['sender_contact_no']			=  $this->sender_contact_no;
			$fields['sender_birthdate']				=  $this->sender_birthdate;
			$fields['driver_first_name']			=  $this->driver_first_name;
			$fields['driver_middle_name']			=  $this->driver_middle_name;
			$fields['driver_last_name']				=  $this->driver_last_name;
			$fields['driver_contact_no']			=  $this->driver_contact_no;
			$fields['driver_birthdate']				=  $this->driver_birthdate;
			$fields['enabled'] 			=  $this->enabled;
			$fields['date_added'] 		=  $this->date_added;
			$fields['date_update'] 	=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'booking_roro');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Rolling Cargo Check In List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_booking_roro' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('booking_roro u'));

		return parent::get('booking_roro u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('booking_roro a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return	object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('booking_roro u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*, r.*, t.*, bs.*, bt.*, d.*, sm.subvoyage_id, sm.departure_date, sm.vessel_id, sm.subvoyage_management_status_id,
									sm.departure_delay_time, vs.vessel_code, vs.vessel, s.description as subvoyage_status, 
									sv.ETD, sv.ETA, sv.origin_id, sv.destination_id');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		// Get the vessel and subvoyage management details
		$this->db->join('subvoyage_management sm', 'u.subvoyage_management_id = sm.id_subvoyage_management', 'left');
		$this->db->join('subvoyage_management_status s', 'sm.subvoyage_management_status_id = s.id_subvoyage_management_status', 'left');
		$this->db->join('subvoyage sv', 'sm.subvoyage_id = sv.id_subvoyage', 'left');
		$this->db->join('rolling_cargo r', 'u.rolling_cargo_id = r.id_rolling_cargo', 'left');
		$this->db->join('discounts d', 'u.discount_id = d.id_discount', 'left');
		$this->db->join('transaction t', 'u.reference_no = t.transaction_ref', 'left');
		$this->db->join('vessel vs', 'sm.vessel_id = vs.id_vessel', 'left');
		$this->db->join('booking_source bs', 'u.booking_source_id = bs.id_booking_source', 'left');
		$this->db->join('booking_status bt', 'u.booking_status_id = bt.id_booking_status', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_booking_roro' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file rolling_cargo_checkin_model.php */
/* Location: ./application/modules_core/adminpanel/models/rolling_cargo_checkin/rolling_cargo_checkin_model.php */