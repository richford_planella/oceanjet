<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Vessel Model Class
|--------------------------------------------------------------------------
|
| Handles the vessel table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Vessel_Model extends MY_Model
{
	/* int vessel id */
	public $id_vessel = NULL;
	
        /* string vessel code */
	public $vessel_code;
        
	/* string vessel name */
	public $vessel;
        
        /* int total number of row */
	public $total_row;
        
        /* int total number of column */
	public $total_column;
        
        /* int total number of seats */
	public $total_seats;

	/* boolean enabled */
	public $enabled;

	/* string table name */
	protected $table = 'vessel';

	/* string table identifier */
	protected $identifier = 'id_vessel';

	/* string foreign key */
	protected $foreign_key = 'vessel_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_vessel'] = (int)($this->id);
		$fields['vessel_code'] =  $this->vessel_code;
                $fields['vessel'] =  $this->vessel;
                $fields['total_row'] =  $this->total_row;
                $fields['total_column'] =  $this->total_column;
                $fields['total_seats'] =  $this->total_seats;
		$fields['enabled'] =  $this->enabled;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'vessel');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display vessel list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_vessel' => 'ASC'), $count = FALSE)
	{
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('vessel v'));

		return parent::get('vessel v');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for vessel to vessel plan
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromSubvoyageManagement()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('subvoyage_management sm');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get vessel field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('vessel v');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_vessel' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file vessel_model.php */
/* Location: ./application/modules_core/adminpanel/models/vessel/vessel_model.php */