<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Vessel Seats Model Class
|--------------------------------------------------------------------------
|
| Handles the vessel seats table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Vessel_Seats_Model extends MY_Model
{
        /* int vessel id */
	public $vessel_id;
        
	/* string vessel name */
	public $vessel;
        
        /* int row */
	public $row;
        
        /* int column */
	public $column;

	/* string vessel seats */
	public $vessel_seats;
        
        /* int accommodation_id */
	public $accommodation_id;
        
	/* string table name */
	protected $table = 'vessel_seats';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		$fields['vessel_id'] =  $this->vessel_id;
                $fields['row'] =  $this->row;
                $fields['column'] =  $this->column;
		$fields['vessel_seats'] =  $this->vessel_seats;
                $fields['accommodation_id'] =  $this->accommodation_id;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'vessel_seats') OR parent::checkColumn($column, 'vessel') OR parent::checkColumn($column, 'accommodation');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display vessel seats list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('row' => 'ASC', 'column' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('vessel_seats vs'));

		return parent::get('vessel_seats vs');
	}
                
        // --------------------------------------------------------------------
	
	/*
	 * Get vessel seats field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('vessel_seats vs');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('vs.*,v.vessel_code,v.vessel,v.total_seats,v.total_row,v.total_column, a.accommodation_code, a.accommodation');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('vessel v', 'v.id_vessel = vs.vessel_id', 'left');
                $this->db->join('accommodation a', 'a.id_accommodation = vs.accommodation_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('row' => 'ASC', 'column' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file vessel_seats_model.php */
/* Location: ./application/modules_core/adminpanel/models/vessel/vessel_seats_model.php */