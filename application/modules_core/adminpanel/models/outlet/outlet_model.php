<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Model Class
|--------------------------------------------------------------------------
|
| Handles the outlet table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Outlet_Model extends MY_Model
{
	/* int id_outlet */
	public $id_outlet = NULL;
        
	/* int outlet code */
	public $outlet_code;
	
	/* int port id */
	public $port_id;
	
	/* string outlet type */
	public $outlet_type;
        
	/* string outlet name */
	public $outlet;
     
	/* int commission */
	public $outlet_commission;
	
	/* string outlet address */
	public $outlet_address;

	/* string outlet contact no */
	public $outlet_contact_no;
	
	/* int status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'outlet';

	/* string table identifier */
	protected $identifier = 'id_outlet';
	
	/* int foreign key */
	protected $foreign_key = 'outlet_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
                $fields['id_outlet'] 			= (int)($this->id);
                $fields['port_id'] 				=  $this->port_id;
                $fields['outlet_code'] 			=  strtoupper($this->outlet_code);
                $fields['outlet_type'] 			=  $this->outlet_type;
                $fields['outlet'] 				=  $this->outlet;
                $fields['outlet_address'] 		=  $this->outlet_address;
                $fields['outlet_contact_no'] 	=  $this->outlet_contact_no;
                $fields['enabled'] 				=  $this->enabled;
                $fields['date_added'] 			=  $this->date_added;
                $fields['date_update'] 		=  $this->date_update;
                $fields['outlet_commission'] =  $this->outlet_commission;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'outlet');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_outlet' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('outlet u'));

		return parent::get('outlet u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Return count result
	 *
	 * @access	private
	 * @return		int
	 */
	private function _countResult($class)
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));
		
		// Return class
		return $this->db->count_all_results($class);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Add results for count
	 *
	 * @access	public
	 * @return		int
	 */
	public function countForeignKey()
	{
		// Initialize
		$fk = array();

		// Get count
		$fk["reservation"] 			= $this->_countResult('reservation r');
		$fk["user_account"] 		= $this->_countResult('user_account u');
		$fk["passenger_fare"] 	= $this->_countResult('passenger_fare_conditions p');
		$fk["debtor_outlet"] 		= $this->_countResult('debtor_outlet d');
	
		// Return sum
		return array_sum($fk);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('outlet o');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*, p.port_code, p.port, p.terminal_fee, p.port_charge');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('port p', 'p.id_port = u.port_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_outlet' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/outlet_model.php */