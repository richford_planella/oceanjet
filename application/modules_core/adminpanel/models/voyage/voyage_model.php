<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Voyage Model Class
  |--------------------------------------------------------------------------
  |
  | Handles the voyage table on the DB
  |
  | @category	Model
  | @author		Kenneth Bahia
 */

class Voyage_Model extends MY_Model
{
    /* int voyage id */

    public $id_voyage = NULL;

    /* string voyage_code */
    public $voyage;

    /* int enabled */
    public $enabled;

    /* int origin id of first subvoyage */
    public $origin_id;

    /* int destination id of last subvoyage */
    public $destination_id;

    /* time ETD of first subvoyage */
    public $ETD;

    /* time ETA of last subvoyage */
    public $ETA;

    /* int trip_type_id */
    public $trip_type_id;

    /* int no of subvoyages */
    public $no_of_subvoyages;

    /* string table name */
    protected $table = 'voyage';

    /* string table identifier */
    protected $identifier = 'id_voyage';

    /* string foreign key */
    protected $foreign_key = 'voyage_id';

    // ------------------------------------------------------------------------

    /*
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    // ------------------------------------------------------------------------

    /*
     * Get values from object
     *
     * @access 	public
     * @return		array
     */
    public function getFields()
    {
        if (isset($this->id))
            $fields['id_voyage'] = (int) ($this->id_voyage);
        $fields['voyage'] = $this->voyage;
        $fields['no_of_subvoyages'] = $this->no_of_subvoyages;
        $fields['ETD'] = $this->ETD;
        $fields['ETA'] = $this->ETA;
        $fields['enabled'] = $this->enabled;
        $fields['origin_id'] = $this->origin_id;
        $fields['destination_id'] = $this->destination_id;
        $fields['trip_type_id'] = $this->trip_type_id;        

        return $fields;
    }

    // ------------------------------------------------------------------------

    /*
     * Check if column exist
     *
     * @access 	public
     * @return		array
     */
    public function checkColumn($column = '')
    {
        return parent::checkColumn($column, 'voyage');
    }

    // --------------------------------------------------------------------

    /*
     * Display voyage list
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function displayList($where = array(), $order_by = array('id_voyage' => 'ASC'), $count = FALSE)
    {
        // SELECT
        self::_select();

        // JOIN
        self::_join();

        // WHERE
        self::_where($where);

        // ORDER BY
        self::_orderby($order_by);

        // return count immediately
        if ($count)
            return count(parent::get('voyage u'));

        return parent::get('voyage u');
    }

    // --------------------------------------------------------------------

    /*
     * Count for voyage
     *
     * @access	public
     * @return		int
     */
    public function countFromVoyageManagement()
    {
        // WHERE
        self::_where(array($this->foreign_key => $this->id));

        return $this->db->count_all_results('voyage_management a');
    }

    // --------------------------------------------------------------------

    /*
     * Get user field value
     *
     * @access	public
     * @param	mixed
     * @param	array
     * @return		object
     */
    public function getValue($fieldname = '', $where = array())
    {
        // SELECT
        $this->db->select($fieldname);

        // WHERE
        $this->db->where($where);

        $query = $this->db->get('voyage u');
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    // --------------------------------------------------------------------

    /*
     * SELECT
     *
     * @return		void
     */
    private function _select()
    {
        $this->db->select('u.id_voyage,
							u.voyage,
							u.origin_id,
							u.destination_id,
							u.ETD,
							u.ETA,
							u.no_of_subvoyages,
							p.port AS origin, 
							p2.port AS destination,
							u.enabled');
    }

    // --------------------------------------------------------------------

    /*
     * JOIN
     *
     * @return		void
     */
    private function _join()
    {
        //$this->db->join('vessel v', 'u.vessel_id = v.id_vessel', 'left');
        $this->db->join('port p', 'u.origin_id = p.id_port', 'left');
        $this->db->join('port p2', 'u.destination_id = p2.id_port', 'left');
    }

    // --------------------------------------------------------------------

    /*
     * WHERE
     *
     * @return		void
     */
    private function _where($where)
    {
        $this->db->where($where);
    }

    // --------------------------------------------------------------------

    /*
     * ORDER BY
     *
     * @return		void
     */
    private function _orderby($order_by = array('id_voyage' => 'ASC'))
    {
        if (!empty($order_by))
        {
            foreach ($order_by as $field => $direction)
                $this->db->order_by($field, $direction);
        }
    }

    // --------------------------------------------------------------------

    /*
     * LIMIT - OFFSET
     *
     * @return		void
     */
    private function _limit($limit, $offset)
    {
        if ($offset > 0)
        {
            $offset = ($offset * $limit) - $limit;
            $this->db->limit($limit, $offset);
        }
    }

}

/* End of file voyage_model.php */
/* Location: ./application/modules_core/adminpanel/models/voyage/voyage_model.php */