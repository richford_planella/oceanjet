<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Booking Model Class
|--------------------------------------------------------------------------
|
| Handles the booking table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Refund_Tickets_Model extends MY_Model
{
	/* int booking  id */
	public $id_refund_tickets = NULL;
	
    /* int user id */
	public $booking_id;
	
    /* int passenger id */
	public $user_id;
	
	/* int refund ticket status id */
	public $refund_ticket_status_id;
	
	/* decimal refund amount */
	public $refund_amount;
	
    /* datetime date refunded */
	public $date_refunded;
    
	/* string table name */
	protected $table = 'refund_tickets';

	/* string table identifier */
	protected $identifier = 'id_refund_tickets';

	/* string foreign key */
	protected $foreign_key = 'booking_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_refund_tickets'] = (int)($this->id);
			$fields['booking_id'] = $this->booking_id;
			$fields['user_id'] =  $this->user_id;
			$fields['date_refunded'] = $this->date_refunded;
                

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'refund_tickets');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display booking list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_refund_tickets' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		// self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('refund_tickets rt'));

		return parent::get('refund_tickets');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for booking to checkin
	 *
	 * @access		public
	 * @return		int
	 */
	public function countCheckIn()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('checkin ci');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get booking field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('refund_tickets rt');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('rt.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('user u', 'u.id_user = b.user_id', 'left');
                $this->db->join('user_account ua', 'ua.user_id = u.id_user', 'left');
                $this->db->join('ticket_series_info ts', 'ts.id_ticket_series_info = b.ticket_series_info_id', 'left');
                $this->db->join('passenger p', 'p.id_passenger = b.passenger_id', 'left');
                $this->db->join('passenger_fare pf', 'pf.id_passenger_fare = b.passenger_fare_id', 'left');
                $this->db->join('booking_source bs', 'bs.id_booking_source = b.booking_source_id', 'left');
                $this->db->join('booking_status bss', 'bss.id_booking_status = b.booking_status_id', 'left');
                $this->db->join('discounts d', 'u.id_user = b.discount_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_refund_tickets' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
	
	// --------------------------------------------------------------------
	
	public function get_refund_ticket_details($input = array())
	{
		$query = $this->db->query(
			'SELECT 
				CONCAT(pa.firstname, " ", pa.middlename, " ", pa.lastname) AS fullname,
				tsi.id_ticket_series_info,
				vo.voyage AS voyage_code,
				vo.ETD,
				vo.ETA,
				p1.port AS origin,
				p2.port AS destination,
				sm.departure_date,
				ve.vessel,
				ac.accommodation,
				sms.description AS voyage_status,
				b.id_booking,
				b.total_amount,
				rs.rule_code
			FROM
				ticket_series_info tsi,
				passenger pa,
				booking b,
				voyage vo,
				subvoyage_management sm,
				voyage_management vm,
				PORT p1,
				PORT p2,
				vessel ve,
				passenger_fare pf,
				accommodation ac,
				subvoyage_management_status sms,
				rule_set rs
			WHERE
				pa.lastname LIKE "%'. $input['last_name'] .'%" 
			AND
				tsi.ticket_no LIKE "%'. $input['ticket_number'] .'%"
			AND
				pa.id_passenger = b.passenger_id
			AND
				tsi.id_ticket_series_info = b.ticket_series_info_id
			AND
				b.subvoyage_management_id = sm.id_subvoyage_management
			AND
				sm.voyage_management_id = vm.id_voyage_management
			AND
				vm.voyage_id = vo.id_voyage
			AND
				vo.origin_id = p1.id_port
			AND
				vo.destination_id = p2.id_port
			AND
				sm.vessel_id = ve.id_vessel
			AND
				pf.id_passenger_fare = b.passenger_fare_id
			AND
				pf.accommodation_id = ac.id_accommodation
			AND
				sm.subvoyage_management_status_id = sms.id_subvoyage_management_status
			AND pf.rule_set_id = rs.id_rule_set'
		);
		
		return $query->result();
	}
}

/* End of file booking_model.php */
/* Location: ./application/modules_core/adminpanel/models/booking/booking_model.php */