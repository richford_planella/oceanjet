<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Passenger Model Class
|--------------------------------------------------------------------------
|
| Handles the passenger table on the DB
|
| @category	Model
| @author		Richford Planella
*/ 
class Passenger_Model extends MY_Model
{
	/* int passenger id */
	public $id_passenger = NULL;
	
    /* varchar first name */
	public $firstname;
	
	/* varchar middle name */
	public $middlename;
	
	/* varchar last name */
	public $lastname;
	
	/* date birthdate */
	public $birthdate;
	
	/* varchar age */
	public $age;
	
	/* varchar gender */
	public $gender;
	
	/* varchar contact number */
	public $contactno;
	
	/* varchar valid ID number */
	public $id_no;
	
	/* varchar nationality */
	public $nationality;
	
	/* tinyint with infant */
	public $with_infant;
	
	/* varchar infant name */
	public $infant_name;

	/* string table name */
	protected $table = 'passenger';

	/* string table identifier */
	protected $identifier = 'id_passenger';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_passenger'] 		= (int)($this->id);
			$fields['firstname'] 			=  $this->firstname;
			$fields['middlename'] 			=  $this->middlename;
			$fields['lastname'] 			=  $this->lastname;
			$fields['birthdate'] 			=  $this->birthdate;
			$fields['age'] 					=  $this->age;
			$fields['gender'] 				=  $this->gender;
			$fields['contactno'] 			=  $this->contactno;
			$fields['id_no'] 				=  $this->id_no;
			$fields['nationality']			=  $this->nationality;
			$fields['with_infant']			=  $this->with_infant;
			$fields['infant_name']			=  $this->infant_name;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'passenger');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_passenger' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('passenger u'));

		return parent::get('passenger u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for outlet
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_passenger' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file passenger_model.php */
/* Location: ./application/modules_core/adminpanel/models/passenger/passenger_model.php */