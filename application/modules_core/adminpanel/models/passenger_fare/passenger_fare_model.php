<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Passenger Fare Model Class
|--------------------------------------------------------------------------
|
| Handles the passenger fare table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Passenger_Fare_Model extends MY_Model
{
	/* int id_rule_set */
	public $id_passenger_fare = NULL;
        
	/* string fare code */
	public $passenger_fare;
	
	/* int voyage id */
	public $voyage_id;
	
	/* int accommodation id */
	public $accommodation_id;
	
	/* int rule set id */
	public $rule_set_id;
	
	/* int seat limit */
	public $seat_limit;
	
	/* int regular fare */
	public $regular_fare;
	
	/* int default fare */
	public $default_fare;
	
	/* int require id */
	public $require_id;
	
	/* int return fare */
	public $return_fare;
	
	/* int points earned */
	public $points_earning;

	/* int origin id */
	public $origin_id;

	/* int destination id */
	public $destination_id;
	
	/* bool status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'passenger_fare';

	/* string table identifier */
	protected $identifier = 'id_passenger_fare';
	
	/* int foreign key */
	protected $foreign_key = 'passenger_fare_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_passenger_fare'] = (int)($this->id);
			$fields['passenger_fare']	=  strtoupper($this->passenger_fare);
			$fields['voyage_id']		=  $this->voyage_id;
			$fields['accommodation_id']	=  $this->accommodation_id;
			$fields['rule_set_id']		=  $this->rule_set_id;
			$fields['seat_limit']		=  $this->seat_limit;
			$fields['regular_fare']		=  $this->regular_fare;
			$fields['default_fare']		=  $this->default_fare;
			$fields['require_id']		=  $this->require_id;
			$fields['return_fare']		=  $this->return_fare;
			$fields['points_earning']	=  $this->points_earning;
			$fields['origin_id']		=  $this->origin_id;
			$fields['destination_id']	=  $this->destination_id;
			$fields['enabled'] 			=  $this->enabled;
			$fields['date_added'] 		=  $this->date_added;
			$fields['date_update'] 	=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'passenger_fare p');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Passenger Fare List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_passenger_fare' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('passenger_fare u'));

		return parent::get('passenger_fare u');
	}
        
   // --------------------------------------------------------------------
	
	/*
	 * Return count result
	 *
	 * @access	private
	 * @return		int
	 */
	private function _countResult($class)
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));
		
		// Return class
		return $this->db->count_all_results($class);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Add results for count
	 *
	 * @access	public
	 * @return		int
	 */
	public function countForeignKey()
	{
		// Initialize
		$fk = array();

		// Get count
		$fk["booking"]	= $this->_countResult('booking b');
		$fk["reservation"]	= $this->_countResult('reservation r');
		$fk["reservation_booking"]	= $this->_countResult('reservation_booking r1');
	
		// Return sum
		return array_sum($fk);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('passenger_fare u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		
		$this->db->select('u.*, a.accommodation_code, a.accommodation, r.rule_code, r.rule_set, t.type as trip_type,
                                    v.voyage, v.origin_id, v.destination_id, v.trip_type_id, v.ETD, v.ETA, v.no_of_subvoyages');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('accommodation a', 'u.accommodation_id = a.id_accommodation', 'left');
		$this->db->join('rule_set r', 'u.rule_set_id = r.id_rule_set', 'left');
		$this->db->join('voyage v', 'u.voyage_id = v.id_voyage', 'left');
		$this->db->join('trip_type t', 'v.trip_type_id = t.id_trip_type', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_passenger_fare' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file passenger_fare.php */
/* Location: ./application/modules_core/adminpanel/models/passenger_fare/passenger_fare_model.php */