<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Subvoyage Model Class
|--------------------------------------------------------------------------
|
| Handles the leg table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Subvoyage_Fare_Model extends MY_Model
{
	/* int subvoyage id */
	public $id_subvoyage_fare = NULL;
	
	/* int passenger fare id */
	public $passenger_fare_id;
	
	/* int subvoyage id */
	public $subvoyage_id;
	
	/* int amount */
	public $amount;

	/* string table name */
	protected $table = 'subvoyage_fare';

	/* string table identifier */
	protected $identifier = 'id_subvoyage_fare';
	
	/* string foreign key */
	protected $foreign_key = 'subvoyage_fare_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_subvoyage_fare'] 	= (int)($this->id);
			$fields['amount']	 				=  $this->amount;
			$fields['subvoyage_id']	 		=  $this->subvoyage_id;
			$fields['passenger_fare_id']	=  $this->passenger_fare_id;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'conditions');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Subvoyage Fare List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_subvoyage_fare' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('subvoyage_fare u'));

		return parent::get('subvoyage_fare u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('subvoyage_fare a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('subvoyage_fare u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		// Get passenger fare fields
		$pfare = (array) $this->_fields("passenger_fare");
		
		// Passenger Fare 
		$passenger_fare = implode(", p.", $pfare);
		
		// Select
		$this->db->select('u.*,'.$passenger_fare.", s.id_subvoyage, s.vessel_id, s.ETD, s.ETA, s.origin_id, s.destination_id, p1.port_code as origin, p1.port as origin_port, p2.port_code as destination, p2.port as destination_port");
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return	void
	 */
	private function _join()
	{
		$this->db->join('passenger_fare p', 'u.passenger_fare_id = p.id_passenger_fare', 'left');
		$this->db->join('subvoyage s', 'u.subvoyage_id = s.id_subvoyage', 'left');
		$this->db->join('port p1', 's.origin_id = p1.id_port', 'left');
		$this->db->join('port p2', 's.destination_id = p2.id_port', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_subvoyage_fare' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
	
	/*
	 * List Table Fields
	 *
	 * @return	array
	 */
	public function _fields($tbl=NULL)
	{
		// Initialize
		$obj = new stdClass();
		
		// Check param
		$table  = !empty($tbl) ? $tbl : $this->table;
		
		// Fields
		$field = $this->db->list_fields($table);
		
		// Unset primary key
		unset($field[0]);
		
		foreach($field as $key => $value){
			$obj->$key = $value;
		}
		
		return $obj;
	}
	
}

/* End of file leg_model.php */
/* Location: ./application/modules_core/adminpanel/passenger_fare/passenger_fare/subvoyage_fare_model.php */