<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Passenger Fare Conditions Model Class
|--------------------------------------------------------------------------
|
| Handles the passenger fare conditions name table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Passenger_Fare_Conditions_Model extends MY_Model
{
	/* int id_passenger_fare_conditions */
	public $id_passenger_fare_conditions = NULL;
        
	/* int passenger fare id */
	public $passenger_fare_id;
	
	/* int conditions id */
	public $conditions_id;
	
	/* int age bracket from */
	public $age_bracket_from;
	
	/* int age bracket to */
	public $age_bracket_to;
	
	/* int outlet id */
	public $outlet_id;
	
	/* int max advance booking */
	public $max_advance_booking;
	
	/* int min advance booking */
	public $min_advance_booking;
	
	/* datetime booking period from */
	public $booking_period_from;
	
	/* datetime booking period to */
	public $booking_period_to;
	
	/* datetime travel period from */
	public $travel_period_from;
	
	/* datetime travel period from */
	public $travel_period_to;
	
	/* string travel days */
	public $travel_days;
	
	/* int maximum leg interval */
	public $max_leg_interval;
	
	/* string table name */
	protected $table = 'passenger_fare_conditions';

	/* string table identifier */
	protected $identifier = 'id_passenger_fare_conditions';
	
	/* string foreign key */
	protected $foreign_key = 'passenger_fare_conditions_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_passenger_fare_conditions']	= (int)($this->id);
			$fields['passenger_fare_id']		=  $this->passenger_fare_id;
			$fields['conditions_id']				=  $this->conditions_id;
			$fields['age_bracket_from']		=  $this->age_bracket_from;
			$fields['age_bracket_to']			=  $this->age_bracket_to;
			$fields['outlet_id']						=  $this->outlet_id;
			$fields['max_advance_booking']	=  $this->max_advance_booking;
			$fields['min_advance_booking']	=  $this->min_advance_booking;
			$fields['booking_period_from']	=  $this->booking_period_from;
			$fields['booking_period_to']	=  $this->booking_period_to;
			$fields['travel_period_from']	=  $this->travel_period_from;
			$fields['travel_period_to']		=  $this->travel_period_to;
			$fields['travel_days']				=  $this->travel_days;
			$fields['max_leg_interval']		=  $this->max_leg_interval;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'passenger_fare_conditions');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Passenger Fare Conditons List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_passenger_fare_conditions' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('passenger_fare_conditions u'));

		return parent::get('passenger_fare_conditions u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('passenger_fare_conditions a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('passenger_fare_conditions u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*, conditions, passenger_fare, voyage_id, accommodation_id, rule_set_id, seat_limit, regular_fare,
		default_fare, require_id, return_fare, points_earning, outlet_code, outlet_type, outlet, port_id, outlet_commission, outlet_address');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('passenger_fare p', 'u.passenger_fare_id = p.id_passenger_fare', 'left');
		$this->db->join('conditions c', 'u.conditions_id = c.id_conditions', 'left');
		$this->db->join('outlet o', 'u.outlet_id = o.id_outlet', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_passenger_fare_conditions' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
	
	/*
	 * List Table Fields
	 *
	 * @return	array
	 */
	public function _fields()
	{
		// Initialize
		$obj = new stdClass();
		
		// Fields
		$field = $this->db->list_fields($this->table);
		
		// Unset primary key
		unset($field[0]);
		
		foreach($field as $key => $value){
			$obj->$key = $value;
		}
		
		return $obj;
	}
}

/* End of file passenger_fare_conditions_model.php */
/* Location: ./application/modules_core/adminpanel/models/passenger_fare_conditions/passenger_fare_conditions_model.php */