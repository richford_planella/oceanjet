<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| User Profile Level Model Class
|--------------------------------------------------------------------------
|
| Handles the users profile level table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class User_Profile_Level_Model extends MY_Model
{
	/* int user profile id */
	public $user_profile_id = NULL;
        
        /* int user level id */
	public $user_level_id = NULL;
        
	/* string table name */
	protected $table = 'user_profile_level';

	/* string table identifier */
	protected $identifier = 'id_user_profile_level';

	/* string foreign key */
	protected $foreign_key = 'user_profile_level_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		$fields['user_profile_id'] =  $this->user_profile_id;
                $fields['user_level_id'] =  $this->user_level_id;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'user_profile') OR parent::checkColumn($column, 'user_level') OR parent::checkColumn($column, 'user_profile_level');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user profile level list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('user_profile_id' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('user_profile_level upl'));

		return parent::get('user_profile_level upl');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('upl.*, ul.user_level, up.user_profile,up.user_description, up.is_outlet, up.has_mobile_access, up.enabled');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('user_level ul', 'ul.id_user_level = upl.user_level_id', 'left');
		$this->db->join('user_profile up', 'up.id_user_profile = upl.user_profile_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('user_profile_id' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get all user profile base on user level
	 *
	 * @access		public
	 * @param		mixed
	 * @return		object
	 */
        public function getUserProfileLevel($user_profile_id = NULL)
        {
            $result = $this->db->query("SELECT "
                                        . "up.* "
                                        . "FROM "
                                        . "user_profile up "
                                        . "WHERE up.user_level_id "
                                        . "IN "
                                        . "(SELECT "
                                        . "ul.user_level_id "
                                        . "FROM "
                                        . "user_profile_level ul "
                                        . "WHERE ul.user_profile_id = $user_profile_id ) "
                                        . "AND "
                                        . "up.enabled = 1");
            
            return $result->result();
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Check user level 
	 *
	 * @access		public
	 * @param		mixed
	 * @return		object
	 */
        public function checkUserLevel($user_profile_id, $user_level_id)
        {
            // WHERE
            self::_where(array('user_profile_id' => $user_profile_id, 'user_level_id' => $user_level_id));

            // return count
            return count(parent::get('user_profile_level upl'));
        }
}

/* End of file user_profile_level_model.php */
/* Location: ./application/modules_core/adminpanel/models/user/user_profile_level_model.php */