<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Users Accounts Model Class
|--------------------------------------------------------------------------
|
| Handles the users account table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class User_Account_Model extends MY_Model
{
	/* int user account id */
	public $id_user_account = NULL;
        
        /* int user id */
	public $user_id;
        
	/* string email address */
	public $email_address;
        
        /* string firstname */
        public $firstname;
        
        /* string middlename */
	public $middlename;
        
        /* string lastname */
	public $lastname;
        
        /* int outlet id */
	public $outlet_id;
        
        /* string profile picture */
        public $profile_pic;
        
	/* string table name */
	protected $table = 'user_account';

	/* string table identifier */
	protected $identifier = 'id_user_account';

	/* string foreign key */
	protected $foreign_key = 'user_account_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_user_account'] = (int)($this->id);
		$fields['user_id'] =  $this->user_id;
                $fields['email_address'] =  $this->email_address;
		$fields['firstname'] =  $this->firstname;
                $fields['middlename'] =  $this->middlename;
                $fields['lastname'] =  $this->lastname;
		$fields['outlet_id'] =  $this->outlet_id;
                $fields['profile_pic'] =  $this->profile_pic;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'user_account') OR parent::checkColumn($column, 'user') OR parent::checkColumn($column, 'outlet');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user account list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_user_account' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('user_account ua'));

		return parent::get('user_account ua');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for user to timesheet
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('timesheet t');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user_account ua');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('ua.*,u.user_profile_id,u.username,up.user_profile, up.user_description, o.outlet');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('user u', 'u.id_user = ua.user_id', 'left');
		$this->db->join('user_profile up', 'up.id_user_profile = u.user_profile_id', 'left');
                $this->db->join('outlet o', 'o.id_outlet = ua.outlet_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_user_account' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file user_account_model.php */
/* Location: ./application/modules_core/adminpanel/models/user/user_account_model.php */