<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| User Profile Model Class
|--------------------------------------------------------------------------
|
| Handles the user profile table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Profile_Model extends MY_Model
{
	/* int profile id */
	public $id_user_profile = NULL;
	
	/* string user profile */
	public $user_profile;
        
        /* string user description */
	public $user_description;
        
        /* int with outlet */
	public $is_outlet;

	/* int has mobile access */
	public $has_mobile_access;
        
        /* int can open transaction */
	public $has_transaction;
        
        /* int session timeout */
	public $session_timeout;
        
	/* boolean enabled */
	public $enabled;
        
        /* boolean has menu */
	public $has_menu;
        
        /* int administrator page id */
	public $administrator_page_id;
        
        /* int user level id */
	public $user_level_id;
        
	/* string table name */
	protected $table = 'user_profile';

	/* string table identifier */
	protected $identifier = 'id_user_profile';

	/* string foreign key */
	protected $foreign_key = 'user_profile_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_user_profile'] = (int)($this->id);
		$fields['user_profile'] =  $this->user_profile;
                $fields['user_description'] =  $this->user_description;
		$fields['is_outlet'] =  $this->is_outlet;
		$fields['has_mobile_access'] =  $this->has_mobile_access;
                $fields['has_transaction'] =  $this->has_transaction;
                $fields['session_timeout'] =  $this->session_timeout;
                $fields['enabled'] =  $this->enabled;
                $fields['has_menu'] =  $this->has_menu;
                $fields['administrator_page_id'] =  $this->administrator_page_id;
                $fields['user_level_id'] =  $this->user_level_id;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'user_profile') OR parent::checkColumn($column, 'user_level');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display user profile list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_user_profile' => 'ASC'), $count = FALSE, $pass_id = array())
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);
                
                if(!empty($pass_id))
                    parent::orWhere($pass_id);
                
		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('user_profile up'));

		return parent::get('user_profile up');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for user profile to user
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('user u');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get user profile field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user_profile up');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('up.*, ul.user_level');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('user_level ul', 'ul.id_user_level = up.user_level_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_user_profile' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file profile_model.php */
/* Location: ./application/modules_core/adminpanel/models/user/profile_model.php */