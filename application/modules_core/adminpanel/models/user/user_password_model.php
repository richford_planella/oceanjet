<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| User Password Model Class
|--------------------------------------------------------------------------
|
| Handles the user password table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class User_Password_Model extends MY_Model
{
	/* int user password id */
	public $id_user_password = NULL;
	
	/* int user id */
	public $user_id;
        
        /* string password */
	public $password;

	/* string salt */
	public $salt;

	/* string table name */
	protected $table = 'user_password';

	/* string table identifier */
	protected $identifier = 'id_user_password';

	/* string foreign key */
	protected $foreign_key = 'user_password_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_user_password'] = (int)($this->id);
		$fields['user_id'] =  $this->user_id;
                $fields['password'] =  $this->password;
		$fields['salt'] =  $this->salt;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'user_password');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Search user password details
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function searchList($where = array(), $order_by = array('id_user_password' => 'ASC'), $count = FALSE, $limit = 25, $offset = 1)
	{		
		if (MY_Session::get('user_passwordSearch'))
			foreach (MY_Session::get('user_passwordSearch') as $fieldname => $value)
			{
				if ($value != '')
					if ($fieldname == 'salt' OR  $fieldname == 'password')
						$this->db->like(array($fieldname => $value));
					else
						$this->db->where(array($fieldname => $value));
			}

		self::_where($where);
                
		// ORDER BY
		if (MY_Session::get('user_passwordOrder') AND is_array(MY_Session::get('user_passwordOrder')))
			$order_by = array(MY_Session::get('user_passwordOrder', 'order') => MY_Session::get('user_passwordOrder', 'by'));

		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('user_password up'));

		// LIMIT - OFFSET
		self::_limit($limit, $offset);

		return parent::get('user_password up');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user password list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_user_password' => 'ASC'), $count = FALSE)
	{
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('user_password up'));

		return parent::get('user_password up');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get user profile field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user_password up');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_user_password' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file user_password_model.php */
/* Location: ./application/modules_core/adminpanel/models/user/user_password_model.php */