<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| User Level Model Class
|--------------------------------------------------------------------------
|
| Handles the user level table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class User_Level_Model extends MY_Model
{
	/* int user level id */
	public $id_user_level = NULL;
	
	/* string user level */
	public $user_profile;
        
	/* string table name */
	protected $table = 'user_level';

	/* string table identifier */
	protected $identifier = 'id_user_level';

	/* string foreign key */
	protected $foreign_key = 'user_level_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_user_level'] = (int)($this->id);
		$fields['user_level'] =  $this->user_level;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'user_level');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user level list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_user_level' => 'ASC'), $count = FALSE)
	{
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('user_level ul'));

		return parent::get('user_level ul');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for user profile to user
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUserProfileLevel()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('user_profile_level upl');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get user level field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user_level ul');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_user_level' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file user_level_model.php */
/* Location: ./application/modules_core/adminpanel/models/user/user_level_model.php */