<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Users Model Class
|--------------------------------------------------------------------------
|
| Handles the users table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class User_Model extends MY_Model
{
	/* int user id */
	public $id_user = NULL;
        
        /* int user profile id */
	public $user_profile_id;
	
	/* string username */
	public $username;
        
        /* string password */
	public $password;
        
        /* string secure token */
	public $secure_token;
        
        /* string password salt */
	public $salt;
        
        /* datetime last successful login */
        public $last_login;
        
        /* date last failed login */
        public $last_failed_login;

        /* int number of failed login */
        public $num_failed_login;
        
	/* boolean enabled */
	public $enabled;
        
        /* boolean is deleted */
	public $is_deleted;
        
        /* boolean is locked */
	public $is_locked;
        
        /* boolean is new */
	public $is_new;
        
        /* date password generated */
        public $password_generated;

	/* string table name */
	protected $table = 'user';

	/* string table identifier */
	protected $identifier = 'id_user';

	/* string foreign key */
	protected $foreign_key = 'user_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_user'] = (int)($this->id);
		$fields['user_profile_id'] =  $this->user_profile_id;
                $fields['username'] =  $this->username;
		$fields['password'] =  $this->password;
                $fields['secure_token'] =  $this->secure_token;
                $fields['salt'] =  $this->salt;
		$fields['last_login'] =  $this->last_login;
                $fields['last_failed_login'] =  date("Y-m-d",strtotime($this->last_failed_login));
                $fields['num_failed_login'] =  $this->num_failed_login;
		$fields['enabled'] =  $this->enabled;
                $fields['is_deleted'] =  $this->is_deleted;
                $fields['is_locked'] =  $this->is_locked;
                $fields['is_new'] =  $this->is_new;
                $fields['password_generated'] =  $this->password_generated;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'user_profile') OR parent::checkColumn($column, 'user');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Search user details
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function searchList($where = array(), $order_by = array('id_user' => 'ASC'), $count = FALSE, $limit = 25, $offset = 1)
	{		
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
		if (MY_Session::get('userSearch'))
			foreach (MY_Session::get('userSearch') as $fieldname => $value)
			{
				if ($value != '')
					if ($fieldname == 'username' OR  $fieldname == 'firstname' OR $fieldname == 'middlename' OR $fieldname == 'lastname')
						$this->db->like(array($fieldname => $value));
					else
						$this->db->where(array($fieldname => $value));
			}

		self::_where($where);
                
		// ORDER BY
		if (MY_Session::get('userOrder') AND is_array(MY_Session::get('userOrder')))
			$order_by = array(MY_Session::get('userOrder', 'order') => MY_Session::get('userOrder', 'by'));

		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('user u'));

		// LIMIT - OFFSET
		self::_limit($limit, $offset);

		return parent::get('user u');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_user' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('user u'));

		return parent::get('user u');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get user id
	 *
	 * @access		public
	 * @param		mixed
	 * @return		int
	 */
	public function getUserID($where = array())
	{
		// SELECT
		$this->db->select('id_user');
		
		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user');
		$row = $query->row();
		
		if ($row)
			return $row->id_user;

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*,up.id_user_profile, up.user_profile, up.user_description');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('user_profile up', 'up.id_user_profile = u.user_profile_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_user' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/user/user_model.php */