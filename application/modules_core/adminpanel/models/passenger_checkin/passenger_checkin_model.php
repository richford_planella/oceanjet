<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Passenger Checkin Model Class
|--------------------------------------------------------------------------
|
| Handles the outlet table on the DB
|
| @category	Model
| @author		Gian Asuncion
*/
class Passenger_Checkin_Model extends MY_Model
{
    public $id_passenger_checkin = NULL;

    public $booking_id;

    public $subvoyage_management_id;

    public $vessel_seats;

    public $accomodation_id;

    public $transaction_ref;

    public $vessel_id;

    public $reason;

    public $date_added;

    public $date_updated;

    protected $table = 'passenger_checkin';

    protected $identifier = 'id_passenger_checkin';

    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    public function getFields()
    {
        if (isset($this->id))
            $fields['id_passenger_checkin']          = (int)($this->id);
            $fields['booking_id']                    = $this->booking_id;
            $fields['subvoyage_management_id']       = $this->subvoyage_management_id;
            $fields['vessel_seats']                  = $this->vessel_seats;
            $fields['accomodation_id']               = $this->accomodation_id;
            $fields['transaction_ref']               = $this->transaction_ref;
            $fields['reason']                        = $this->reason;
            $fields['vessel_id']                     = $this->vessel_id;
            $fields['date_added']                    = $this->date_added;
            $fields['date_updated']                  = $this->date_updated;

        return $fields;
    }

    public function checkColumn($column = '')
    {
        return parent::checkColumn($column, 'passenger_checkin');
    }

    public function displayList($where = array(), $order_by = array('id_passenger_checkin' => 'ASC'), $count = FALSE)
    {
        self::_select();
        self::_join();
        self::_where($where);
        self::_orderby($order_by);
        self::_groupby("u.id_passenger_checkin");

        if ($count)
            return count(parent::get('passenger_checkin u'));

        return parent::get('passenger_checkin u');
    }

    public function countFromUser()
    {
        self::_where(array($this->foreign_key => $this->id));
        return $this->db->count_all_results('accounts a');
    }

    public function getValue($fieldname = '', $where = array())
    {
        $this->db->select($fieldname);
        $this->db->where($where);
        $query = $this->db->get('passenger_checkin u');
        $row = $query->row();

        if ($row)
            return $row->{$fieldname};

        return FALSE;
    }

    private function _select()
    {
        $this->db->select('
                        tsi.ticket_no,
                        u.id_passenger_checkin,
                        u.booking_id,
                        u.subvoyage_management_id,
                        u.vessel_seats,
                        u.accomodation_id,
                        u.transaction_ref,
                        u.vessel_id,
                        p.lastname,
                        p.firstname,
                        p.middlename,
                        p.age,
                        p.nationality,
                        p.gender,
                        b.passenger_valid_id_number
                      ');
    }

    private function _join()
    {
          $this->db->join('booking b',   'u.booking_id = b.id_booking','left');
          $this->db->join('passenger p', 'b.passenger_id = p.id_passenger','left');
          $this->db->join('passenger_checkin pc', 'pc.booking_id = b.id_booking','left');
          $this->db->join('ticket_series_info tsi', 'b.ticket_series_info_id = tsi.id_ticket_series_info', 'left');
    }

    private function _join_custom($data)
    {

    }

    private function _where($where)
    {
        $this->db->where($where);
    }

    private function _orderby($order_by = array('id_passenger_checkin' => 'ASC'))
    {
        if (!empty($order_by)) {
            foreach ($order_by as $field => $direction)
                $this->db->order_by($field, $direction);
        }

    }

    private function _groupby()
    {
        $this->db->group_by('u.id_passenger_checkin');
    }

    private function _limit($limit, $offset)
    {
        if($offset > 0)
        {
            $offset = ($offset * $limit) - $limit;
            $this->db->limit($limit, $offset);
        }
    }

    /*
     * Subvoyage Table
     * */
    public function vesselStatusTable($subvoyage_management_id,$vessel_id)
    {
        $query = $this->db->query("SELECT
                                        CONCAT(ac.accommodation,' (', ac.accommodation_code ,')') AS aCode,
                                        p1.accommodation_id,
                                        ac.accommodation_code,
                                        p1.total_capacity,
                                        IFNULL(p2.total_book,0) AS ticketed_pax,
                                        IFNULL(p3.total_checkin,0) AS checkin_pax,
                                        IFNULL(p4.occupied_seats,0) AS occupied_seats,
                                        (p1.total_capacity - IFNULL(p4.occupied_seats,0)) AS vacant_seats,
                                        IFNULL(p5.ticket_not_transfered,0) AS ticket_not_transfered,
                                        IF((IFNULL(p2.total_book,0) - IFNULL(p3.total_checkin,0))<=-1,0,IFNULL(p2.total_book,0) - IFNULL(p3.total_checkin,0)) AS no_show_pax,
                                        (p1.total_capacity - IFNULL(p3.total_checkin,0)) AS available_seats
                                    FROM (
                                        SELECT accommodation_id, COUNT(accommodation_id) AS total_capacity
                                        FROM vessel_seats WHERE vessel_id = $vessel_id GROUP BY accommodation_id
                                        )p1
                                    LEFT JOIN (
                                        SELECT    pf.accommodation_id, COUNT(pf.accommodation_id) AS total_book
                                        FROM      booking b
                                        LEFT JOIN passenger_fare pf
                                        ON        pf.id_passenger_fare = b.passenger_fare_id
                                        WHERE     subvoyage_management_id = $subvoyage_management_id
                                        AND       b.`booking_status_id` IN (1,5) GROUP BY pf.accommodation_id
                                    ) p2
                                    ON
                                        p1.accommodation_id = p2.accommodation_id
                                    LEFT JOIN (
                                        SELECT    pf.accommodation_id, COUNT(pf.accommodation_id) AS total_checkin
                                        FROM      booking b
                                        LEFT JOIN passenger_fare pf
                                        ON        pf.id_passenger_fare = b.passenger_fare_id
                                        WHERE     subvoyage_management_id = $subvoyage_management_id
                                        AND       b.`booking_status_id` = 5  GROUP BY pf.accommodation_id
                                    ) p3
                                    ON
                                     p3.accommodation_id = p1.accommodation_id
                                     LEFT JOIN (
                                        SELECT `accomodation_id`, COUNT(`booking_id`) AS occupied_seats
                                        FROM `passenger_checkin`
                                        WHERE subvoyage_management_id = $subvoyage_management_id AND vessel_id= $vessel_id
                                        GROUP BY accomodation_id
                                    ) p4
                                    ON
                                     p4.accomodation_id = p1.accommodation_id
                                    LEFT JOIN
                                      accommodation ac ON ac.id_accommodation = p1.accommodation_id
                                     LEFT JOIN (
                                        SELECT `accomodation_id`, COUNT(`booking_id`) AS ticket_not_transfered
                                        FROM `passenger_checkin`
                                        WHERE subvoyage_management_id = $subvoyage_management_id
                                        AND vessel_id = (SELECT `old_vessel_id` FROM `subvoyage_management` WHERE `id_subvoyage_management` = $subvoyage_management_id)
                                        GROUP BY accomodation_id
                                    ) p5
                                    ON
                                     p5.accomodation_id = p1.accommodation_id
                                    LEFT JOIN
                                      accommodation a ON a.id_accommodation = p1.accommodation_id
                                    WHERE a.`id_accommodation` IN (SELECT `id_accommodation` FROM `accommodation` WHERE `enabled` = 1) ORDER BY p1.accommodation_id ASC
                                ");

        return $query->result_array();
    }

    /*
     * Get all Voyages based on departure date
     * @return Lists of active voyages
     * */
    public function getSubVoyages($origin_id,$destination_id)
    {
        $query = $this->db->query("SELECT
                                    svm.`id_subvoyage_management`,
                                    v.`voyage`,
                                    svm.`departure_date`,
                                    p.`ETD`,
                                    sp.`ETA`,
                                    port.`port` as port_origin,
                                    port2.`port` as port_destination,
                                    p.id_subvoyage,
                                    p.voyage_id,
                                    svms.`description`,
                                    svm.`subvoyage_management_status_id`
                                FROM
                                    subvoyage p, subvoyage sp
                                LEFT JOIN
                                    `subvoyage_management` AS svm
                                ON
                                    svm.`subvoyage_id` = sp.`id_subvoyage`
                                LEFT JOIN
                                    voyage AS v
                                ON
                                    v.`id_voyage` = sp.voyage_id
                                LEFT JOIN
                                    `port`
                                ON
                                    port.`id_port` = sp.`origin_id`
                                LEFT JOIN
                                    `port` AS port2
                                ON
                                    port2.`id_port` = sp.`destination_id`
                                LEFT JOIN
                                    `subvoyage_management_status` AS svms
                                ON
                                    svms.`id_subvoyage_management_status` = svm.`subvoyage_management_status_id`
                                WHERE
                                    p.voyage_id = sp.voyage_id
                                AND
                                    p.origin_id       = $origin_id
                                AND
                                    sp.destination_id = $destination_id
                                AND
                                    p.id_subvoyage    = sp.id_subvoyage
                                AND
                                    svm.`id_subvoyage_management` IS NOT NULL
                                AND
                                    v.`enabled` = 1
                                GROUP BY
                                    svm.voyage_management_id
                                ORDER BY
                                     svm.`departure_date` ASC");

        return $query->result();
    }

    /*
     * Get Available seats from new vessel
     * */
    public function getAvailableSeatsNewVessel($subvoyage_management_id,$new_vessel_id)
    {
        $query = $this->db->query("SELECT
                                        `vessel_seats`
                                    FROM
                                        `vessel_seats` AS vs
                                    LEFT JOIN
                                        `vessel` AS v
                                    ON
                                        v.`id_vessel` = vs.`vessel_id`
                                    WHERE
                                        `vessel_seats` NOT IN (SELECT `vessel_seats` FROM `passenger_checkin` WHERE `subvoyage_management_id`=$subvoyage_management_id AND `vessel_id`=$new_vessel_id)
                                    AND
                                        vs.`vessel_id` = $new_vessel_id
                                    AND
                                        `vessel_seats` != 'N/A'
                                    ORDER BY vessel_seats ASC
                                    ");

        return $query->result();
    }
}

/* End of file passenger_checkin_model.php */
/* Location: ./application/modules_core/adminpanel/models/passenger_checkin/passenger_checkin_model.php */