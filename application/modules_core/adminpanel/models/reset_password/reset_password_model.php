<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Reset Password Model Class
|--------------------------------------------------------------------------
|
| Handles the reset password table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Reset_Password_Model extends MY_Model
{
	/* int reset password id */
	public $id_reset_password = NULL;
	
	/* int user id */
	public $user_id;
        
        /* string value */
	public $value;
        
        /* strtime date added */
	public $date_added;
        
        /* strtime expiration date */
	public $exp_date;

	/* string table name */
	protected $table = 'reset_password';

	/* string table identifier */
	protected $identifier = 'id_reset_password';

	/* string foreign key */
	protected $foreign_key = 'reset_password_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_reset_password'] = (int)($this->id);
		$fields['user_id'] =  $this->user_id;
                $fields['value'] =  $this->value;
		$fields['date_added'] =  $this->date_added;
                $fields['exp_date'] =  $this->exp_date;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'reset_password');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display reset password list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_reset_password' => 'ASC'), $count = FALSE)
	{
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('reset_password rp'));

		return parent::get('reset_password rp');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get reset password field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('reset_password rp');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_reset_password' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file reset_password_model.php */
/* Location: ./application/modules_core/adminpanel/models/reset_password/reset_password_model.php */