<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Rolling Cargo Model Class
|--------------------------------------------------------------------------
|
| Handles the rolling cargo table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Rolling_Cargo_Model extends MY_Model
{
	/* int rolling cargo id */
	public $id_rolling_cargo = NULL;
	
        /* string rolling cargo code */
	public $rolling_cargo_code;
        
	/* string rolling cargo name */
	public $rolling_cargo;
        
        /* int voyage id */
	public $voyage_id;
        
        /* decimal amount */
	public $amount;
        
        /* string unit of measuremment */
	public $unit_measurement;
        
        /* int rule set id */
	public $rule_set_id;

	/* boolean enabled */
	public $enabled;

	/* string table name */
	protected $table = 'rolling_cargo';

	/* string table identifier */
	protected $identifier = 'id_rolling_cargo';

	/* string foreign key */
	protected $foreign_key = 'rolling_cargo_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_rolling_cargo'] = (int)($this->id);
		$fields['rolling_cargo_code'] =  $this->rolling_cargo_code;
                $fields['rolling_cargo'] =  $this->rolling_cargo;
                $fields['voyage_id'] =  $this->voyage_id;
                $fields['amount'] =  $this->amount;
                $fields['unit_measurement'] =  $this->unit_measurement;
                $fields['rule_set_id'] =  $this->rule_set_id;
		$fields['enabled'] =  $this->enabled;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'rolling_cargo');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display rolling cargo list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_rolling_cargo' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('rolling_cargo rc'));

		return parent::get('rolling_cargo rc');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for rolling cargo to schedule??
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromROROCheckin()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('rolling_cargo_checkin rcc');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get rolling cargo field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('rolling_cargo rc');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('rc.*,rs.rule_code,rs.rule_set,v.voyage');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('rule_set rs', 'rs.id_rule_set = rc.rule_set_id', 'left');
                $this->db->join('voyage v', 'v.id_voyage = rc.voyage_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_rolling_cargo' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file rolling_cargo_model.php */
/* Location: ./application/modules_core/adminpanel/models/rolling_cargo/rolling_cargo_model.php */