<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Padala Cargo Check In Model Class
|--------------------------------------------------------------------------
|
| Handles the padala_cargo_checkin table on the DB
|
| @category	Model
| @author		Kenneth Bahia
*/
class Padala_Cargo_Checkin_Model extends MY_Model
{
	/* int padala cargo checkin id */
	public $id_padala_cargo_checkin = NULL;
	
	/* int subvoyage management id */
	public $subvoyage_management_id;
        
    /* string reference no */
	public $reference_no;
        
    /* string sender's first name */
	public $sender_first_name;
        
    /* string sender's last name */
    public $sender_last_name;

    /* string sender's middle initial */
    public $sender_middle_initial;

    /* string sender's contact no */
	public $sender_contact_no;
        
    /* string recipient's first name */
    public $recipient_first_name;

    /* string recipient's last name */
    public $recipient_last_name;

    /* string recipient's middle initial */
    public $recipient_middle_initial;

    /* string recipient's contact no */
	public $recipient_contact_no;

	/* string table name */
	protected $table = 'padala_cargo_checkin';

	/* string table identifier */
	protected $identifier = 'id_padala_cargo_checkin';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_padala_cargo_checkin'] 	= (int)($this->id);
			$fields['subvoyage_management_id'] 	=  $this->subvoyage_management_id;
			$fields['reference_no'] 			=  $this->reference_no;
			$fields['sender_first_name'] 		=  $this->sender_first_name;
			$fields['sender_last_name'] 		=  $this->sender_last_name;
			$fields['sender_middle_initial'] 	=  $this->sender_middle_initial;
			$fields['sender_contact_no'] 		=  $this->sender_contact_no;
			$fields['recipient_first_name'] 	=  $this->recipient_first_name;
			$fields['recipient_last_name'] 		=  $this->recipient_last_name;
			$fields['recipient_middle_initial'] =  $this->recipient_middle_initial;
			$fields['recipient_contact_no']		=  $this->recipient_contact_no;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'padala_cargo_checkin');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_padala_cargo_checkin' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		self::_groupby("u.id_padala_cargo_checkin");

		// return count immediately
		if ($count)
			return count(parent::get('padala_cargo_checkin u'));

		return parent::get('padala_cargo_checkin u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for padala cargo check in
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('padala_cargo_checkin a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('padala_cargo_checkin u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.id_padala_cargo_checkin,
							u.subvoyage_management_id,
							u.reference_no,
							u.sender_first_name,
							u.sender_last_name,
							u.sender_middle_initial,
							u.sender_contact_no,
							u.recipient_first_name,
							u.recipient_last_name,
							u.recipient_middle_initial,
							u.recipient_contact_no,
							SUM(c.quantity * c.amount) AS total_amount,
							GROUP_CONCAT(c.particular SEPARATOR ", ") AS particulars', false);
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('cargo_description c', 'c.padala_cargo_checkin_id = u.id_padala_cargo_checkin', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_padala_cargo_checkin' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _groupby()
	{
		$this->db->group_by("u.id_padala_cargo_checkin");
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file padala_cargo_checkin_model.php */
/* Location: ./application/modules_core/adminpanel/models/padala_cargo_checkin/padala_cargo_checkin_model.php */