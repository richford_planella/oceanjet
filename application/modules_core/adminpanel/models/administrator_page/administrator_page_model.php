<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Page Model Class
|--------------------------------------------------------------------------
|
| Handles the administrator page table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Administrator_Page_Model extends MY_Model
{
	/* int administrator page id */
	public $id_administrator_page;

	/* int parent page id */
	public $administrator_page_id;

	/* string administrator page */
	public $administrator_page;
        
        /* string classname */
	public $classname;
        
        /* string image name */
	public $img;
        
        /* int page order */
	public $page_order;
        
        /* bool enabled */
	public $enabled;
	
	/* string table name */
	protected $table = 'administrator_page';

	/* string table identifier */
	protected $identifier = 'id_administrator_page';

	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------
	
	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_administrator_page'] = (int)($this->id);
		$fields['administrator_page_id'] =  $this->administrator_page_id;
                $fields['administrator_page'] =  $this->administrator_page;
                $fields['classname'] =  $this->classname;
                $fields['img'] =  $this->img;
                $fields['page_order'] =  $this->page_order;
		$fields['enabled'] =  $this->enabled;

		return $fields;
	}
	
        // --------------------------------------------------------------------
	
	/*
	 * Display administrator page list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_administrator_page' => 'ASC'), $count = FALSE)
	{
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('administrator_page ap'));

		return parent::get('administrator_page ap');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get administrator page field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('administrator_page ap');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
	
        // --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_user_profile' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get page ID based on classname
	 *
	 * @access		public
	 * @return		string
	 */
	public function getPageID($classname = '')
	{
		// SELECT
		$this->db->select('id_administrator_page');
		
		// WHERE
		$this->db->where(array('classname' => $classname));
		
		$query = $this->db->get('administrator_page');
		$row = $query->row();
		
		if ($row)
			return $row->id_administrator_page;
		
		return NULL;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get parent page ID based on classname
	 *
	 * @access		public
	 * @return		string
	 */
	public function getParentPageID($classname = '')
	{
		// SELECT
		$this->db->select('parent_administrator_page_id');

		// WHERE
		$this->db->where(array('classname' => $classname));
		
		$query = $this->db->get('administrator_page a');
		$row = $query->row();
		
		if ($row)
			return $row->parent_administrator_page_id;
			
		return NULL;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get subpage based on parent administrator page id
	 *
	 * @access		public
	 * @return		string
	 */
        public function getSubPage($administrator_page_id = 0, $profile_id = '')
        {
                // SELECT
		$this->db->select(' id_administrator_page, parent_administrator_page_id, administrator_page, classname, page_order, img,
                                    user_profile_id, aa.administrator_page_id, access, view, add, edit, delete');
                
                // JOIN
		$this->db->join('administrator_access aa', 'aa.administrator_page_id = ap.id_administrator_page', 'left');
                
                // WHERE
		$this->db->where(array(	// parent page
                                        'parent_administrator_page_id' => $administrator_page_id,

                                        // check if viewable
                                        'access' => 1,

                                        // admin profile id
                                        'user_profile_id' => $profile_id,

                                        // check if permitted to display link
                                        'enabled' => 1

                                        )
                                );
		
                // ORDER BY
		$this->db->order_by('page_order', 'ASC');
                                
		$query = $this->db->get('administrator_page ap');
		$result = $query->result();
		
		return $result;
        }
                
        // --------------------------------------------------------------------
	
	/*
	 * Get page classname of subpage based on parent administrator page id
	 *
	 * @access		public
	 * @return		string
	 */
        public function getSubClass($administrator_page_id = 0, $profile_id = '')
        {
                // SELECT
		$this->db->select('classname');
                
                // JOIN
		$this->db->join('administrator_access aa', 'aa.administrator_page_id = ap.id_administrator_page', 'left');
                
                // WHERE
		$this->db->where(array(	// parent page
                                        'parent_administrator_page_id' => $administrator_page_id,

                                        // check if viewable
                                        'access' => 1,

                                        // admin profile id
                                        'user_profile_id' => $profile_id,

                                        // check if permitted to display link
                                        'enabled' => 1

                                        )
                                );
		
                // ORDER BY
		$this->db->order_by('page_order', 'ASC');
                                
		$query = $this->db->get('administrator_page ap');
		$row = $query->row();
		
		if ($row)
			return $row->classname;
			
		return NULL;
        }


        // --------------------------------------------------------------------
	
	/*
	 * Get main admin tabs
	 *
	 * @access		public
	 * @param		int
	 * @return		object
	 */
	public function getHeaderTabs($profile_id = '')
	{
		// SELECT
		$this->db->select(' id_administrator_page, parent_administrator_page_id, administrator_page, classname, page_order, img,
                                    user_profile_id, aa.administrator_page_id, access, view, add, edit, delete');
		
		// JOIN
		$this->db->join('administrator_access aa', 'aa.administrator_page_id = ap.id_administrator_page', 'left');
		//$this->db->join('administrator_page_lang apl', 'apl.administrator_page_id = a.id_administrator_page', 'left');
		
		// WHERE
		$this->db->where(array(	// no parent page
                                        'parent_administrator_page_id' => 0,

                                        // check if viewable
                                        'access' => 1,

                                        // admin profile id
                                        'user_profile_id' => $profile_id,

                                        // check if permitted to display link
                                        'enabled' => 1

                                        )
                                );
		
		// ORDER BY
		$this->db->order_by('page_order', 'ASC');
		
		$query = $this->db->get('administrator_page ap');
		$result = $query->result();
		
		return $result;
	}
        
        
        
}
