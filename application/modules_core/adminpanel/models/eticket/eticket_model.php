<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Model Class
|--------------------------------------------------------------------------
|
| Handles the outlet table on the DB
|
| @category	Model
| @author		Kenneth Bahia
*/
class Eticket_Model extends MY_Model
{
	/* int accommodation id */
	public $id_eticket = NULL;
        
    /* string accommodation_code */
	public $eticket_details;
	
	/* string eticket terms */
	public $eticket_terms;
        
    /* string reminders */
	public $eticket_reminders;

	/* string full */
	public $eticket_full;

	/* image */
	public $img_path;
        
    /* datetime date created */
	public $date_added;
        
    /* datetime update */
    public $date_update;

	/* string table name */
	protected $table = 'eticket';

	/* string table identifier */
	protected $identifier = 'id_eticket';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_eticket'] 			= (int)($this->id);
			$fields['eticket_details'] 		=  $this->eticket_details;
			$fields['eticket_terms'] 		=  $this->eticket_terms;
			$fields['eticket_reminders'] 	=  $this->eticket_reminders;
			$fields['eticket_full'] 		=  $this->eticket_full;
			$fields['img_path']				=  $this->img_path;
			$fields['date_added'] 			=  $this->date_added;
			$fields['date_update'] 			=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'eticket');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_eticket' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		// self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('eticket u'));

		return parent::get('eticket u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for outlet
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('user u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_eticket' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/outlet_model.php */