<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Model Class
|--------------------------------------------------------------------------
|
| Handles the outlet table on the DB
|
| @category	Model
| @author		Kenneth Bahia
*/
class Subvoyage_Management_Model extends MY_Model
{
	/* int voyage id */
	public $id_subvoyage_management = NULL;
        
    /* int voyage_management_id */
    public $voyage_management_id;

    /* string voyage_no */
	public $subvoyage_id;
	
	/* int vessel id */
	public $departure_date;
        
        /* datetime ETD */
	public $vessel_id;
        
        /* datetime ETA */
	public $subvoyage_management_status_id;

	/* int departure delay time in mins */
	public $departure_delay_time;

	/* string remarks */
	public $remarks;

	/* string table name */
	protected $table = 'subvoyage_management';

	/* string table identifier */
	protected $identifier = 'id_subvoyage_management';
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_subvoyage_management'] 		= (int)($this->id_subvoyage_management);
			$fields['voyage_management_id'] 		=  $this->voyage_management_id;
			$fields['subvoyage_id'] 				=  $this->subvoyage_id;
			$fields['departure_date'] 				=  $this->departure_date;
			$fields['vessel_id'] 					=  $this->vessel_id;
			$fields['subvoyage_management_status_id'] 	=  $this->subvoyage_management_status_id;
			$fields['departure_delay_time'] 		=  $this->departure_delay_time;
			$fields['remarks']						=  $this->remarks;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'subvoyage_management');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('u.id_subvoyage_management' => 'ASC'), $count = FALSE, $where_in = array())
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		//WHERE IN
		self::_where_in($where_in);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('subvoyage_management u'));

		return parent::get('subvoyage_management u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for outlet
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('subvoyage_management u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select("u.id_subvoyage_management,u.voyage_management_id,u.subvoyage_id,u.departure_date,u.vessel_id,u.old_vessel_id,u.is_swap,u.subvoyage_management_status_id,s.ETD,s.ETA,ve.vessel AS vessel_code,s.origin_id AS origin_id,s.destination_id AS destination_id,p.port_code AS origin,p2.port_code AS destination,p.port AS port_origin,p2.port AS port_destination,subvoyage_management_status_id,vms.description,u.departure_delay_time,v.voyage");
	}

	// --------------------------------------------------------------------

	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join("subvoyage s", "s.id_subvoyage = u.subvoyage_id", "left");
		// $this->db->join('subvoyage s', 's.id_subvoyage = u.subvoyage_id', 'left');
		$this->db->join('voyage v', 's.voyage_id = v.id_voyage', 'left');
		$this->db->join('vessel ve', 've.id_vessel = u.vessel_id', 'left');
		$this->db->join('subvoyage_management_status vms', 'u.subvoyage_management_status_id = vms.id_subvoyage_management_status', 'left');
		$this->db->join('port p', 's.origin_id = p.id_port', 'left');
		$this->db->join('port p2', 's.destination_id = p2.id_port', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where_in($where_in)
	{
		if ( ! empty($where_in)) {
			$column_name = "";
			foreach ($where_in as $key => $value) {
				$this->db->where_in($key, $value);
			}
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('u.id_subvoyage_management' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/outlet_model.php */