<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Voyage Management Model Class
|--------------------------------------------------------------------------
|
| Handles the voyage_management table on the DB
|
| @category	Model
| @author		Kenneth Bahia
*/
class Voyage_Management_Model extends MY_Model
{
	/* int voyage management id */
	public $id_voyage_management = NULL;
        
    /* string voyage id */
	public $voyage_id;

	/* int enabled */
	public $enabled;

	/* string table name */
	protected $table = 'voyage_management';

	/* string table identifier */
	protected $identifier = 'id_voyage_management';

	/* string foreign key*/
	protected $foreign_key = 'subvoyage_management_id';

	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_voyage_management'] 	= (int)($this->id_voyage_management);
			$fields['voyage_id'] 				=  $this->voyage_id;
			$fields['enabled']					=  $this->enabled;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'voyage_management');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Voyage Management list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('u.id_voyage_management' => 'ASC'), $count = FALSE, $where_in = array())
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		//WHERE IN
		self::_where_in($where_in);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('voyage_management u'));

		return parent::get('voyage_management u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for Subvoyage Management
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromBooking($subvoyage_array)
	{
		// WHERE IN
		//self::_where(array($this->foreign_key => $subvoyage_array));

		$this->db->where_in($this->foreign_key, $subvoyage_array);

		return $this->db->count_all_results('booking a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get Voyage Management field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('voyage_management u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.id_voyage_management,u.voyage_id,v.voyage,v.origin_id,p.port AS origin,v.destination_id,p2.port AS destination,v.ETD,v.ETA,v.no_of_subvoyages');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join("voyage v", "u.voyage_id = v.id_voyage", "left");
		$this->db->join("port p", "v.origin_id = p.id_port", "left");
		$this->db->join("port p2", "v.destination_id = p2.id_port", "left");
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where_in($where_in)
	{
		if ( ! empty($where_in)) {
			$column_name = "";
			foreach ($where_in as $key => $value) {
				$this->db->where_in($key, $value);
			}
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('u.id_voyage_management' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file user_model.php */
/* Location: ./application/modules_core/adminpanel/models/outlet/outlet_model.php */