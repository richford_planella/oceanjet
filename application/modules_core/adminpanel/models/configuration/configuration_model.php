<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Configuration Model Class
|--------------------------------------------------------------------------
|
| Handles the configuration table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Configuration_Model extends MY_Model
{
	/* int configuration id */
	public $id_configuration = NULL;
	
	/* string configuration */
	public $configuration;
        
        /* string value */
	public $value;

	/* boolean config type */
	public $config_type;

	/* string table name */
	protected $table = 'configuration';

	/* string table identifier */
	protected $identifier = 'id_configuration';

	/* string foreign key */
	protected $foreign_key = 'configuration_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_configuration'] = (int)($this->id);
		$fields['configuration'] =  $this->configuration;
                $fields['value'] =  $this->value;
		$fields['config_type'] =  $this->config_type;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'configuration');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Search configuration details
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function searchList($where = array(), $order_by = array('id_configuration' => 'ASC'), $count = FALSE, $limit = 25, $offset = 1)
	{		
		if (MY_Session::get('configurationrSearch'))
			foreach (MY_Session::get('configurationSearch') as $fieldname => $value)
			{
				if ($value != '')
					if ($fieldname == 'configuration' OR $fieldname == 'value')
						$this->db->like(array($fieldname => $value));
					else
						$this->db->where(array($fieldname => $value));
			}

		self::_where($where);
                
		// ORDER BY
		if (MY_Session::get('configurationOrder') AND is_array(MY_Session::get('configurationOrder')))
			$order_by = array(MY_Session::get('configurationOrder', 'order') => MY_Session::get('configurationOrder', 'by'));

		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('configuration c'));

		// LIMIT - OFFSET
		self::_limit($limit, $offset);

		return parent::get('configuration c');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display configuration list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_configuration' => 'ASC'), $count = FALSE)
	{
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('configuration c'));

		return parent::get('configuration c');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get configuration field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('configuration c');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_configuration' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file configuration_model.php */
/* Location: ./application/modules_core/adminpanel/models/configuration/configuration_model.php */