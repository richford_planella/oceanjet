<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Cargo description Model Class
|--------------------------------------------------------------------------
|
| Handles the Cargo description table on the DB
|
| @category	Model
| @author		Kenneth Bahia
*/
class Cargo_Description_Model extends MY_Model
{
	/* int cargo description id */
	public $id_cargo_description = NULL;

	/* int padala cargo checkin id */
	public $padala_cargo_checkin_id;
        
	/* string particular */
	public $particular;
	
	/* int padala cargo rate id */
	public $padala_cargo_id;
	
	/* int quantity */
	public $quantity;
	
	/* int amount */
	public $amount;

	/* string unit of measurement */
	public $unit_of_measurement;
        
	/* string table name */
	protected $table = 'cargo_description';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_cargo_description']		= (int)($this->id);
			$fields['padala_cargo_checkin_id']	= (int)($this->padala_cargo_checkin_id);
			$fields['particular']				= $this->particular;
			$fields['padala_cargo_id']			= $this->padala_cargo_id;
			$fields['quantity'] 				= $this->quantity;
            $fields['amount'] 					= $this->amount;
            $fields['unit_of_measurement']		= $this->unit_of_measurement;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'cargo_description');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_cargo_description' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		// self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('cargo_description u'));

		return parent::get('cargo_description u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('cargo_description u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('padala_cargo_checkin_id' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file transaction_model.php */
/* Location: ./application/modules_core/adminpanel/models/cargo_description/cargo_description_model.php */