<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Log Model Class
|--------------------------------------------------------------------------
|
| Handles the administrator log table for the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Administrator_Log_Model extends MY_Model
{
	/* int administrator log id */
	public $id_administrator_log;

	/* int administrator id */
	public $administrator_id;

	/* int admin profile id */
	public $administrator_profile_id;

	/* string classname */
	public $classname;

	/* string action/method */
	public $action;

	/* int reference id */
	public $reference_id;
        
        /* string description */
	public $description;
        
	/* date date added */
	public $date_added;

	/* string table name */
	protected $table = 'administrator_log';

	/* int table identifier */
	protected $identifier = 'id_administrator_log';

	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// ------------------------------------------------------------------------
	
	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_administrator_log'] = (int)($this->id);
		$fields['administrator_id'] =  (int)($this->administrator_id);
		$fields['administrator_profile_id'] =  (int)$this->administrator_profile_id;
		$fields['classname'] =  $this->classname;
		$fields['action'] =  $this->action;
                $fields['description'] =  $this->description;
		$fields['reference_id'] =  $this->reference_id;
		$fields['date_added'] =  $this->date_added;

		return $fields;
	}
        
        // ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'administrator_log');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Search administrator log details
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function searchList($where = array(), $order_by = array('id_administrator_log' => 'ASC'), $count = FALSE, $limit = 25, $offset = 1)
	{		
		if (MY_Session::get('logsSearch'))
			foreach (MY_Session::get('logsSearch') as $fieldname => $value)
			{
				if ($value != '')
					if ($fieldname == 'client' OR $fieldname == 'description' OR $fieldname == 'date_start')
						$this->db->like(array($fieldname => $value));
					else
						$this->db->where(array($fieldname => $value));
			}

		self::_where($where);
                
		// ORDER BY
		if (MY_Session::get('logsOrder') AND is_array(MY_Session::get('logsOrder')))
			$order_by = array(MY_Session::get('logsOrder', 'order') => MY_Session::get('logsOrder', 'by'));

		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('administrator_log al'));

		// LIMIT - OFFSET
		self::_limit($limit, $offset);

		return parent::get('administrator_log al');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display administrator log list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_administrator_log' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('administrator_log al'));

		return parent::get('administrator_log al');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get administratot log field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('administrator_log al');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('al.*,ua.*,up.user_profile,up.user_description');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('user u', 'u.id_user = al.administrator_id', 'left');
		$this->db->join('user_profile up', 'up.id_user_profile = u.user_profile_id', 'left');
                $this->db->join('user_account ua', 'ua.user_id = u.id_user', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_administrator_log' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file administrator_log_model.php */
/* Location: ./application/modules_core/adminpanel/models/administrator_log/administrator_log_model.php */