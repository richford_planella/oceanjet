<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Reservation Status Model Class
|--------------------------------------------------------------------------
|
| Handles the reservation status table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Reservation_Status_Model extends MY_Model
{
	/* int reservation status id */
	public $id_reservation_status = NULL;
	
        /* string reservation status */
	public $reservation_status;
                
        /* int enabled */
	public $enabled;
        
	/* string table name */
	protected $table = 'reservation_status';

	/* string table identifier */
	protected $identifier = 'id_reservation_status';

	/* string foreign key */
	protected $foreign_key = 'reservation_status_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_reservation_status'] = (int)($this->id);
		$fields['reservation_status'] =  $this->reservation_status;
                $fields['enabled'] =  $this->enabled;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'reservation_status');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display reservation status list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_reservation_status' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('reservation_status rs'));

		return parent::get('reservation_status rs');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for reservation status to reservation
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('reservation r');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get reservation status field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('reservation_status rs');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
            
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_reservation_status' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file reservation_status_model.php */
/* Location: ./application/modules_core/adminpanel/models/reservation/reservation_status_model.php */