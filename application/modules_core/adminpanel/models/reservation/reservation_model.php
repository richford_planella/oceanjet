<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Reservation Model Class
|--------------------------------------------------------------------------
|
| Handles the reservation table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Reservation_Model extends MY_Model
{
	/* int reservation id */
	public $id_reservation = NULL;
	
        /* string reservation */
	public $reservation;
        
	/* string firstname */
	public $contact_firstname;
        
        /* string lastname */
	public $contact_lastname;
        
        /* string contacat number */
	public $contact_number;
        
        /* int pax count */
	public $pax;

	/* int outlet id */
	public $outlet_id;
        
        /* date expiration date */
	public $expiration_date;
        
        /* string booking reservation code */
	public $reservation_code;
        
        /* tinyint evoucher type */
	public $evoucher_type;
        
        /* int reservation status */
	public $reservation_status_id;
                
	/* string table name */
	protected $table = 'reservation';

	/* string table identifier */
	protected $identifier = 'id_reservation';

	/* string foreign key */
	protected $foreign_key = 'reservation_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_reservation'] = (int)($this->id);
		$fields['reservation'] =  $this->reservation;
                $fields['contact_firstname'] =  $this->contact_firstname;
                $fields['contact_lastname'] =  $this->contact_lastname;
                $fields['contact_number'] =  $this->contact_number;
                $fields['pax'] =  $this->pax;
		$fields['outlet_id'] =  $this->outlet_id;
                $fields['expiration_date'] =  $this->expiration_date;
                $fields['reservation_code'] =  $this->reservation_code;
                $fields['reservation_status_id'] =  $this->reservation_status_id;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'reservation') OR parent::checkColumn($column, 'reservation_status') OR parent::checkColumn($column, 'outlet');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display reservation list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_reservation' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('reservation r'));

		return parent::get('reservation r');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for reservation to reservation booking
	 *
	 * @access		public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('reservation r');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get reservation field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('reservation r');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('r.*,rs.reservation_status,o.outlet_type,o.outlet_code,o.outlet,o.outlet_address');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('reservation_status rs', 'rs.id_reservation_status = r.reservation_status_id', 'left');
                $this->db->join('outlet o', 'o.id_outlet = r.outlet_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_reservation' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get all subvoyage available on voyage
	 *
	 * @access		public
	 * @param		array
	 * @return		object
	 */
        public function getSubVoyage($origin_subvoyage_id, $destination_subvoyage_id, $voyage_management_id, $count = FALSE)
        {
            $query = $this->db->query(
                                        'SELECT '
                                            . 's.id_subvoyage, s.ETD, s.ETA, sm.departure_date, sm.id_subvoyage_management, '
                                            . 'vs.vessel_code, vs.vessel, '
                                            . 'v.voyage, p1.id_port as origin_port_id, p2.id_port as destination_port_id, '
                                            . 'p1.port_code as origin_port_code, p1.port as origin_port, p1.terminal_fee as origin_terminal_fee, p1.port_charge as origin_port_charge, '
                                            . 'p2.port_code as destination_port_code, p2.port as destination_port, p2.terminal_fee as destination_terminal_fee, p2.port_charge as destination_port_charge '
                                        . 'FROM '
                                            . 'subvoyage s, '
                                            . 'subvoyage_management sm,'
                                            . 'voyage_management vm, '
                                            . 'voyage v, '
                                            . 'vessel vs, '
                                            . 'port p1, '
                                            . 'port p2 '
                                        . 'WHERE '
                                            . "s.id_subvoyage >= $origin_subvoyage_id "
                                        . "AND "
                                            . "s.id_subvoyage <= $destination_subvoyage_id "
                                        . "AND "
                                            . "vm.id_voyage_management = $voyage_management_id "
                                        . "AND "
                                            . "sm.subvoyage_id = s.id_subvoyage "
                                        . "AND "
                                            . "sm.voyage_management_id = vm.id_voyage_management "
                                        . "AND "
                                            . "vm.voyage_id = s.voyage_id "
                                        . "AND "
                                            . "vm.voyage_id = v.id_voyage "
                                        . "AND "
                                            . "vs.id_vessel = sm.vessel_id "
                                        . "AND "
                                            . "p1.id_port = s.origin_id "
                                        . "AND "
                                            . "p2.id_port = s.destination_id "
                                        . "ORDER BY "
                                            . "id_subvoyage ASC"
                    );
            
            // return count immediately
            if ($count)
                    return count($query->result());
                
            return $query->result();
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Count subvoyage of voyage
	 *
	 * @access		public
	 * @param		array
	 * @return		integer
	 */
        public function countSubVoyage($voyage_id)
        {
            // WHERE
            self::_where(array('voyage_id' => $voyage_id));

            return $this->db->count_all_results('subvoyage sv');
        }
}

/* End of file reservation_model.php */
/* Location: ./application/modules_core/adminpanel/models/reservation/reservation_model.php */