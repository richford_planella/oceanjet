<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Reservation Booking Model Class
|--------------------------------------------------------------------------
|
| Handles the reservation booking table on the DB
|
| @category		Model
| @author		Baladeva Juganas
*/
class Reservation_Booking_Model extends MY_Model
{
	/* int reservation booking  id */
	public $id_reservation_booking = NULL;
	
        /* id user id */
	public $user_id;
        
        /* id ticket series id */
	public $ticket_series_info_id;
        
        /* id reservation passenger id */
	public $reservation_passenger_id;
        
        /* id passenger fare id */
	public $passenger_fare_id;
        
        /* id booking source id */
	public $booking_source_id;
        
        /* id booking status id */
	public $booking_status_id;
        
        /* id discount id */
	public $discount_id;
        
	/* decimal points */
	public $points;
        
        /* string jetter number */
	public $jetter_no;
        
        /* string payment */
	public $payment;
        
        /* decimal fare price */
	public $fare_price;
        
        /* decimal return fare price */
	public $return_fare_price;
        
        /* decimal total discount */
	public $total_discount;
        
        /* decimal terminal fee */
	public $terminal_fee;
        
        /* decimal port charge */
	public $port_charge;
        
        /* decimal total amount */
	public $total_amount;
        
        /* id reference id */
	public $reference_id;
        
        /* string passenger valid ids */
	public $passenger_valid_id_number;
        
        /* id booking id */
	public $booking_id;
        
        /* datetime date added */
	public $date_added;
        
        /* datetime date update */
	public $date_update;
        
	/* string table name */
	protected $table = 'reservation_booking';

	/* string table identifier */
	protected $identifier = 'id_reservation_booking';

	/* string foreign key */
	protected $foreign_key = 'reservation_booking_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_reservation_booking'] = (int)($this->id);
		$fields['user_id'] =  $this->user_id;
                $fields['ticket_series_info_id'] =  $this->ticket_series_info_id;
                $fields['reservation_passenger_id'] =  $this->reservation_passenger_id;
                $fields['passenger_fare_id'] =  $this->passenger_fare_id;
                $fields['booking_source_id'] =  $this->booking_source_id;
                $fields['booking_status_id'] =  $this->booking_status_id;
                $fields['discount_id'] =  $this->discount_id;
                $fields['points'] =  $this->points;
                $fields['jetter_no'] =  $this->jetter_no;
                $fields['payment'] =  $this->payment;
                $fields['fare_price'] =  $this->fare_price;
                $fields['return_fare_price'] =  $this->return_fare_price;
                $fields['total_discount'] =  $this->total_discount;
                $fields['terminal_fee'] =  $this->terminal_fee;
                $fields['port_charge'] =  $this->port_charge;
                $fields['total_amount'] =  $this->total_amount;
                $fields['reference_id'] =  $this->reference_id;
                $fields['booking_id'] =  $this->booking_id;
                $fields['passenger_valid_id_number'] =  $this->passenger_valid_id_number;
                $fields['date_added'] =  $this->date_added;
                $fields['date_update'] =  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 		public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'reservation_booking');
	}

	
	// --------------------------------------------------------------------
	
	/*
	 * Display reservation sbooking list
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_reservation_booking' => 'ASC'), $count = FALSE)
	{
                // SELECT
		self::_select();

		// JOIN
		self::_join();
                
                // WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('reservation_booking b'));

		return parent::get('reservation_booking b');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Count for booking to checkin
	 *
	 * @access		public
	 * @return		int
	 */
	public function countCheckIn()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('checkin ci');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Get booking field value
	 *
	 * @access		public
	 * @param		mixed
	 * @param		array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('reservation_booking b');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('b.*,u.user_profile_id,ua.email_address,ua.firstname as u_firstname,ua.middlename as u_middlename,ua.lastname as u_lastname,ua.outlet_id,'
                        . 'ts.ticket_series_id,ts.ticket_no,ts.issued,'
                        . 'p.firstname,p.middlename,p.lastname,p.birthdate,p.age,p.gender,p.contactno,p.id_no,p.nationality,p.with_infant,p.infant_name,'
                        . 'pf.passenger_fare,pf.trip_type_id,pf.rule_set_id,pf.accommodation_id,pf.seat_limit,pf.regular_fare,pf.points_earned,pf.points_redemption,'
                        . 'bs.booking_source,bss.booking_status,'
                        . 'd.discount_code,d.discount,d.discount_type,d.discount_amount,d.age_from,d.age_to,d.vat_exempt,d.require_id');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
                $this->db->join('user u', 'u.id_user = b.user_id', 'left');
                $this->db->join('user_account ua', 'ua.user_id = u.id_user', 'left');
                $this->db->join('ticket_series_info ts', 'ts.id_ticket_series_info = b.ticket_series_info_id', 'left');
                $this->db->join('reservation_passenger p', 'p.id_reservation_passenger = b.reservation_passenger_id', 'left');
                $this->db->join('passenger_fare pf', 'pf.id_passenger_fare = b.passenger_fare_id', 'left');
                $this->db->join('booking_source bs', 'bs.id_booking_source = b.booking_source_id', 'left');
                $this->db->join('booking_status bss', 'bss.id_booking_status = b.booking_status_id', 'left');
                $this->db->join('discounts d', 'd.id_discount = b.discount_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_reservation_booking' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file reservation_booking_model.php */
/* Location: ./application/modules_core/adminpanel/models/reservation/reservation_booking_model.php */