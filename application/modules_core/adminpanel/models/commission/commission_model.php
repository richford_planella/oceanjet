<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Commission Model Class
|--------------------------------------------------------------------------
|
| Handles the commission table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Commission_Model extends MY_Model
{
	/* int id_outlet */
	public $id_commission = NULL;
        
	/* int commission code */
	public $commission_code;
	
	/* string commission description */
	public $commission;
	
	/* decimal commission percentage */
	public $commission_percentage;
	
	/* int status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'commission';

	/* string table identifier */
	protected $identifier = 'id_commission';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 		public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_commission']				= (int)($this->id);
			$fields['commission_code']			=  $this->commission_code;
			$fields['commission'] 					=  $this->commission;
            $fields['commission_percentage'] =$this->commission_percentage;
			$fields['enabled'] 						=  $this->enabled;
			$fields['date_added'] 					=  $this->date_added;
			$fields['date_update'] 				=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'commission');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display user list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_commission' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		// self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('commission u'));

		return parent::get('commission u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('commission u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_commission' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file commission_model.php */
/* Location: ./application/modules_core/adminpanel/models/commission/commission_model.php */