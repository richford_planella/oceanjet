<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Rule Type Model Class
|--------------------------------------------------------------------------
|
| Handles the rule type table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Rule_Type_Model extends MY_Model
{
	/* int id_rule_type */
	public $id_rule_type = NULL;
        
	/* int rule_set_id */
	public $rule_set_id;
	
	/* int rule_type_name */
	public $rule_type_name_id;
	
	/* decimal rule_type_amount */
	public $rule_type_amount;
	
	/* string rule_amount_type */
	public $rule_amount_type;
	
	/* decimal rule_type_weight */
	public $rule_type_weight;
	
	/* string table name */
	protected $table = 'rule_type';

	/* string table identifier */
	protected $identifier = 'id_rule_type';
	
	/* string foreign key */
	protected $foreign_key = 'rule_type_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_rule_type']				= (int)($this->id);
			$fields['rule_set_id']				=  $this->rule_set_id;
			$fields['rule_type_name_id']	=  $this->rule_type_name_id;
			$fields['rule_amount_type'] 	=  $this->rule_amount_type;
			$fields['rule_type_amount'] 	=  $this->rule_type_amount;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'rule_type');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Rule Type List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_rule_type' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('rule_type u'));

		return parent::get('rule_type u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('accounts a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('rule_type u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*, rn.*, rs.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return	void
	 */
	private function _join()
	{
		$this->db->join('rule_set rs', 'rs.id_rule_set = rule_set_id', 'left');
		$this->db->join('rule_type_name rn', 'rn.id_rule_type_name = rule_type_name_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_rule_type' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
	
	/*
	 * List Table Fields
	 *
	 * @return	array
	 */
	public function _fields($tbl=NULL)
	{
		// Initialize
		$obj = new stdClass();
		
		// Check param
		$table  = !empty($tbl) ? $tbl : $this->table;
		
		// Fields
		$field = $this->db->list_fields($table);
		
		// Unset primary key
		unset($field[0]);
		
		foreach($field as $key => $value){
			$obj->$key = $value;
		}
		
		return $obj;
	}
	
}

/* End of file rule_type_model.php */
/* Location: ./application/modules_core/adminpanel/models/rule_type/rule_type_model.php */