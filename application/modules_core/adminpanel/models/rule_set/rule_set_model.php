<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Rule Set Model Class
|--------------------------------------------------------------------------
|
| Handles the rule_set table on the DB
|
| @category	Model
| @author		Ronald Meran
*/
class Rule_Set_Model extends MY_Model
{
	/* int id_rule_set */
	public $id_rule_set = NULL;
        
	/* string rule code */
	public $rule_code;
	
	/* string rule set */
	public $rule_set;
	
	/* bool status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'rule_set';

	/* string table identifier */
	protected $identifier = 'id_rule_set';
	
	/* string foreign key */
	protected $foreign_key = 'rule_set_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_rule_set']		= (int)($this->id);
			$fields['rule_code']			=  strtoupper($this->rule_code);
			$fields['rule_set']			=  $this->rule_set;
			$fields['enabled'] 			=  $this->enabled;
			$fields['date_added'] 		=  $this->date_added;
			$fields['date_update'] 	=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'rule_set');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Rule Set List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_rule_set' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		// self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('rule_set u'));

		return parent::get('rule_set u');
	}
        
        
  // --------------------------------------------------------------------
	
	/*
	 * Return count result
	 *
	 * @access	private
	 * @return		int
	 */
	private function _countResult($class)
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));
		
		// Return class
		return $this->db->count_all_results($class);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Add results for count
	 *
	 * @access	public
	 * @return		int
	 */
	public function countForeignKey()
	{
		// Initialize
		$fk = array();

		// Get count
		$fk["passenger_fare"]	= $this->_countResult('passenger_fare p');
	
		// Return sum
		return array_sum($fk);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('rule_set u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('', '', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_rule_set' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file rule_set_model.php */
/* Location: ./application/modules_core/adminpanel/models/rule_set/rule_set_model.php */