<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Attended Baggage Rate Model Class
|--------------------------------------------------------------------------
|
| Handles the attended_baggage table on the DB
|
| @category	Model
| @author		Kenneth Bahia
*/
class Attended_Baggage_Model extends MY_Model
{
	/* int attended baggage id */
	public $id_attended_baggage = NULL;
        
    /* string attended baggage */
	public $attended_baggage;
	
	/* string description */
	public $description;
        
        /* int origin id */
	public $origin_id;

	/* int destination id */
	public $destination_id;
        
        /* datetime date created */
	public $amount;
        
    /* datetime update */
    public $unit_of_measurement;

    /* datetime update */
    public $enabled;

	/* string table name */
	protected $table = 'attended_baggage';

	/* string table identifier */
	protected $identifier = 'id_attended_baggage';

	/* string foreign key */
	protected $foreign_key = 'attended_baggage_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_attended_baggage'] 		= (int)($this->id);
			$fields['attended_baggage'] 		=  $this->attended_baggage;
			$fields['description'] 				=  $this->description;
			$fields['origin_id'] 				=  $this->origin_id;
			$fields['destination_id'] 			=  $this->destination_id;
			$fields['amount'] 					=  $this->amount;
			$fields['unit_of_measurement'] 		=  $this->unit_of_measurement;
			$fields['enabled']					=  $this->enabled;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'attended_baggage');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display attended baggage rate list
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_attended_baggage' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('attended_baggage u'));

		return parent::get('attended_baggage u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for attended baggage rate
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromPassengerBaggageCheckin()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('passenger_baggage_checkin a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get attended baggage rate field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('attended_baggage u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.id_attended_baggage,u.attended_baggage,u.description,u.origin_id,u.destination_id,p.port_code AS origin,p2.port_code AS destination,u.amount,u.unit_of_measurement,u.enabled');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		$this->db->join('port p', 'p.id_port = u.origin_id', 'left');
		$this->db->join('port p2', 'p2.id_port = u.destination_id', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_attended_baggage' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file attended_baggage.php */
/* Location: ./application/modules_core/adminpanel/models/attended_baggage/attended_baggage_model.php */