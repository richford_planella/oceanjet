<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Rolling Cargo Check In Model Class
|--------------------------------------------------------------------------
|
| Handles the rolling cargo check in table on the DB
|
| @category	Model
| @author	Ronald Meran
*/
class Rolling_Cargo_Checkin_Model extends MY_Model
{
	/* int rolling cargo checkin id */
	public $id_rolling_cargo_checkin = NULL;
        
	/* int subvoyage management id */
	public $subvoyage_management_id;
	
	/* int booking roro id */
	public $booking_roro_id;
	
	/* bool status */
	public $enabled;
	
	/* datetime date_added */
	public $date_added;
	
	/* datetime date_update */
	public $date_update;
        
	/* string table name */
	protected $table = 'rolling_cargo_checkin';

	/* string table identifier */
	protected $identifier = 'id_rolling_cargo_checkin';
	
	/* string foreign key */
	protected $foreign_key = 'rolling_cargo_checkin_id';
	
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = '')
	{
		parent::__construct($id);
	}

	// ------------------------------------------------------------------------

	/*
	 * Get values from object
	 *
	 * @access 	public
	 * @return		array
	 */
	public function getFields()
	{
		if (isset($this->id))
			$fields['id_rolling_cargo_checkin'] = (int)($this->id);
			$fields['subvoyage_management_id']	=  $this->subvoyage_management_id;
			$fields['booking_roro_id']			=  $this->booking_roro_id;
			$fields['enabled'] 			=  $this->enabled;
			$fields['date_added'] 		=  $this->date_added;
			$fields['date_update'] 	=  $this->date_update;

		return $fields;
	}

	// ------------------------------------------------------------------------

	/*
	 * Check if column exist
	 *
	 * @access 	public
	 * @return		array
	 */
	public function checkColumn($column = '')
	{
		return parent::checkColumn($column, 'rolling_cargo_checkin');
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display Rolling Cargo Check In List
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function displayList($where = array(), $order_by = array('id_rolling_cargo_checkin' => 'ASC'), $count = FALSE)
	{
		// SELECT
		self::_select();
		
		// JOIN
		self::_join();

		// WHERE
		self::_where($where);

		// ORDER BY
		self::_orderby($order_by);

		// return count immediately
		if ($count)
			return count(parent::get('rolling_cargo_checkin u'));

		return parent::get('rolling_cargo_checkin u');
	}
        
        
   // --------------------------------------------------------------------
	
	/*
	 * Count for user to user
	 *
	 * @access	public
	 * @return		int
	 */
	public function countFromUser()
	{
		// WHERE
		self::_where(array($this->foreign_key => $this->id));

		return $this->db->count_all_results('rolling_cargo_checkin a');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Get user field value
	 *
	 * @access	public
	 * @param	mixed
	 * @param	array
	 * @return		object
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get('rolling_cargo_checkin u');
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * SELECT
	 *
	 * @return		void
	 */
	private function _select()
	{
		$this->db->select('u.*, sm.id_subvoyage_management, sm.voyage_management_id, sm.subvoyage_id, sm.departure_date,
							sm.vessel_id, sm.old_vessel_id, sm.is_swap, sm.subvoyage_management_status_id, sm.departure_delay_time,
							sm.remarks, br.*');
	}

	// --------------------------------------------------------------------
	
	/*
	 * JOIN
	 *
	 * @return		void
	 */
	private function _join()
	{
		// Get the subvoyage management details
		$this->db->join('booking_roro br', 'u.booking_roro_id = br.id_booking_roro', 'left');
		$this->db->join('subvoyage_management sm', 'u.subvoyage_management_id = sm.id_subvoyage_management', 'left');
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * WHERE
	 *
	 * @return		void
	 */
	private function _where($where)
	{
		$this->db->where($where);
	}

	// --------------------------------------------------------------------
	
	/*
	 * ORDER BY
	 *
	 * @return		void
	 */
	private function _orderby($order_by = array('id_rolling_cargo_checkin' => 'ASC'))
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * LIMIT - OFFSET
	 *
	 * @return		void
	 */
	private function _limit($limit, $offset)
	{
		if ($offset > 0)
		{
			$offset = ($offset * $limit) - $limit;
			$this->db->limit($limit, $offset);
		}
	}
}

/* End of file rolling_cargo_checkin_model.php */
/* Location: ./application/modules_core/adminpanel/models/rolling_cargo_checkin/rolling_cargo_checkin_model.php */