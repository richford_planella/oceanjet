<?=$header;?>
<form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Accomodation Code</label>
      <!--<span class="input-notes-top overflow-right-70">*Note: (i.e. BC, OA, TC)</span>-->
      <input class="form-control" type="text" placeholder="Input code" name="accommodation_code" value="<?=set_value('accommodation_code', $accommodation->accommodation_code);?>" disabled/>
      <span class="input-notes-bottom"><?=form_error('accommodation_code')?></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-5">
      <label class="control-label">Accomodation Name</label>
      <!--<span class="input-notes-top overflow-right-70">*Note: (i.e Business Class, Open Air, Tourist Class)</span>-->
      <input class="form-control" type="text" placeholder="Input name" name="accommodation" value="<?=set_value('accommodation', $accommodation->accommodation);?>" disabled/>
      <span class="input-notes-bottom"><?=form_error('accommodation')?></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-5">
      <label class="control-label">Colour Code</label>
      <!--<span class="input-notes-top overflow-right-70">*Note: (i.e Business Class, Open Air, Tourist Class)</span>-->
      <input class="form-control" type="color" placeholder="Input name" name="palette" value="<?=set_value('palette', $accommodation->palette);?>" disabled/>
      <span class="input-notes-bottom"><?=form_error('palette')?></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Status</label>
      <select class="selectpicker form-control" title="Choose" name="enabled" disabled>
          <option value="1" <?=set_select('enabled', '1',(($accommodation->enabled) ? TRUE : ''));?>>Active</option>
          <option value="0" <?=set_select('enabled', '0',((!$accommodation->enabled) ? TRUE : ''));?>>Inactive</option>
      </select>
      <span class="input-notes-bottom"><?=form_error('enabled')?></span>
    </div>
  </div>
  
  <div class="form-group oj-form-footer">
    <div class="col-xs-12">
      <div class="btn-oj-group right">
        <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Back</a>
        <a class="btn oj-button" href="<?=admin_url($this->classname, 'edit', $accommodation->id_accommodation);?>">Edit</a>
      </div>
    </div>
  </div>
</form>
<?=$footer;?>