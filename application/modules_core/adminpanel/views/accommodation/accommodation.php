<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
			<tr>
				<th>Accommodation Code</th>
				<th>Accommodation Name</th>
				<th>Colour Code</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
        <tbody>
            <?php if(!empty($accommodation)):?>
			<?php foreach($accommodation as $u):?>
			<tr>
				<td><?=$u->accommodation_code?></td>
				<td><?=$u->accommodation?></td>
				<td bgcolor="<?=$u->palette?>"></td>
				<td><?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
				<td>
					<a href="<?=admin_url($this->classname, 'view', $u->id_accommodation);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $u->id_accommodation);?>">Edit</a>
                    <a href="<?=admin_url($this->classname, 'delete', $u->id_accommodation);?>" class="btn-delete-accommodation">Delete</a>
                    <?php if($u->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_accommodation, md5($token.$this->classname.$u->id_accommodation));?>"  class="btn-deactivate-accommodation">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_accommodation, md5($token.$this->classname.$u->id_accommodation));?>"  class="btn-activate-accommodation">
                                Re-activate
                        </a>
                    <?php endif;?>
				</td>
			</tr>   
			<?php endforeach;?>
			<?php endif;?>
        </tbody>
    </table>
<?=$footer;?>