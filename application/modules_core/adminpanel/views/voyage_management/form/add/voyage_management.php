<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-5">
            <label class="control-label">Voyage Code</label>
            <select class="selectpicker form-control" name="voyage_id" id="voyages">
                <option value="">Please select</option>
                <?php foreach ($voyage_list as $key => $value) : ?>
                    <?php
                    $option = $value->voyage . " | " . $value->origin . " - " . $value->destination . " | " . date_format(new DateTime($value->ETD), 'g:i a') . " - " . date_format(new DateTime($value->ETA), 'g:i a');
                    ?>
                    <option value="<?= $value->id_voyage ?>" <?=set_select("voyage_id", $value->id_voyage); ?>><?= $option ?></option>
                <?php endforeach; ?>
            </select>
            <span class="input-notes-bottom"><?=form_error('voyage_id') ?></span>
        </div>
        <div class="col-xs-2 has-feedback">
            <label class="control-label">From</label>
            <input class="form-control oj-date-picker date-from" id="date_from" name="date_from" type="text" placeholder="YYYY-MM-DD" value="<?= set_value('date_from'); ?>" />
            <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
            <span class="input-notes-bottom"><?php echo form_error('date_from'); ?></span>
        </div>
        <div class="col-xs-2 has-feedback">
            <label class="control-label">To</label>
            <input class="form-control oj-date-picker date-to" id="date_to" name="date_to" type="text" placeholder="YYYY-MM-DD" value="<?= set_value('date_to'); ?>" />
            <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
            <span class="input-notes-bottom"><?php echo form_error('date_to'); ?></span>
        </div>
    </div>
    <div id="oj-leg--voyage-cms">
        <div class="form-group oj-leg">
        <?php foreach($subvoyage as  $s):?>
            <?php $leg_count = 1;?>
            <!--// HTML CODE -->
            <input type="hidden" name="subvoyage_id[]" value="<?=$s->id_subvoyage;?>" />
            <div class="col-lg-12">
                <div class="input-group">
                    <div class="oj-leg__title oj-leg__title--gray">
                        Leg <?= $leg_count++; ?>
                    </div>
                    <span class="input-group-btn">
                        <!--<button class="btn btn-default oj-leg__btn" type="button">Remove</button>-->
                    </span>
                </div>
            </div>
            <div class="col-xs-12 oj-leg__body">
                <div class="form-group">
                    <div class="col-xs-3">
                        <label class="control-label">Origin</label>
                        <input class="form-control oj-etd" type="text" placeholder="Input Voyage" name="origin_id[]" value="<?= $s->origin; ?>" disabled/>
                    </div>
                    <div class="col-xs-3">
                        <label class="control-label">Destination</label>
                        <input class="form-control oj-etd" type="text" placeholder="Input Voyage" name="origin_id[]" value="<?= $s->destination; ?>" disabled/>
                    </div>
                    <div class="col-xs-3">
                        <label class="control-label">Vessel</label>
                        <select class="selectpicker form-control" name="vessel_id[<?=$s->id_subvoyage;?>]" required>
                            <option value="">Please select</option>
                            <?php foreach($vessel_list as $vl):?>
                                <option value="<?=$vl->id_vessel;?>" <?=set_select("vessel_id[$s->id_subvoyage]", $vl->id_vessel, ($s->vessel_id == $vl->id_vessel) ? TRUE : "");?>><?=$vl->vessel;?></option>>
                            <?php endforeach;?>
                        </select>
                        <span class="input-notes-bottom"><?= form_error("vessel_id[$s->id_subvoyage]") ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-3 has-feedback">
                        <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                        <input class="form-control oj-time-picker oj-etd" type="text" placeholder="HH:MM" name="ETD[]" value="<?= date('H:i A', strtotime($s->ETD)); ?>" disabled/>
                        <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-3 has-feedback">
                        <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                        <input class="form-control oj-time-picker oj-eta" type="text" placeholder="HH:MM" name="ETA[]" value="<?= date('H:i A', strtotime($s->ETA)); ?>" disabled/>
                        <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-3">
                        <label class="control-label">Travel Time</label>
                        <input class="form-control oj-time-picker oj-travel-time" type="text" value="" readonly/>
                        <span class="input-notes-bottom"></span>
                    </div>
                </div>
            </div>
            <!--// END HTML CODE -->
            
            <?php endforeach;?>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
                <button type="submit" id="submit_btn" name="submit" value="submit" class="btn oj-button">Save</button>
            </div>
        </div>
    </div>
</form>
<?=$footer;?>       