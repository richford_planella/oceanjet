<?=$header;?>
<form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
  <div class="form-group">
    <div class="col-xs-6">
      <label class="control-label">Voyage Code</label>
      <?php $voyage_management = $voyage->voyage . " | " . $voyage->origin . " - " . $voyage->destination . " | " . date_format(new DateTime($voyage->ETD), 'g:i a') . " - " .date_format(new DateTime($voyage->ETA), 'g:i a'); ?>
      <input class="form-control voyage" type="text" placeholder="Input Voyage" value="<?=$voyage_management ?>" autocomplete="off" readonly/>
      <input type="hidden" name="voyage_id" value="<?=$voyage->voyage_id?>"/>
      <span class="input-notes-bottom"><?=form_error('voyage')?></span>
    </div>
    <div class="col-xs-3 text-right">
      <!--<button id="add-leg-btn" class="btn oj-button oj-button--margin-top-30" type="button">Add Leg</button>-->
    </div>
  </div>
  <div id="oj-leg--voyage-cms">
  	<?php $i = 1; ?>
  	<?php foreach ($subvoyages as $key => $value) :?>
    <div class="form-group oj-leg">
      <div class="col-lg-12">
        <div class="input-group">
          <div class="oj-leg__title oj-leg__title--gray">
            Leg <?=$i++?>
          </div>
          <span class="input-group-btn">
           <!--<button class="btn btn-default oj-leg__btn" type="button">Remove</button>-->
        </span>
        </div>
      </div>
      <div class="col-xs-12 oj-leg__body">
      	<input type="hidden" name="id_subvoyage_management[]" value="<?=set_value('id_subvoyage_management[]', $value->id_subvoyage_management);?>"/>
        <div class="form-group">
          <div class="col-xs-3">
            <label class="control-label">Origin</label>
            <input class="form-control" type="text" placeholder="Input Origin" value="<?=set_value('origin_id[]', $value->origin) ?>" readonly/>
            <span class="input-notes-bottom"><?=form_error('origin_id[]')?></span>
          </div>
          <div class="col-xs-3">
            <label class="control-label">Destination</label>
            <input class="form-control" type="text" placeholder="Input Destination" value="<?=set_value('destination_id[]', $value->destination) ?>" readonly/>
            <span class="input-notes-bottom"><?=form_error('destination_id[]')?></span>
          </div>
          <div class="col-xs-3">
            <label class="control-label">Vessel</label>
            <select class="selectpicker form-control" name="vessel_id[]">
              <option value="">Please select</option>
              <?php foreach ($vessel_list as $k => $v) : ?>
              <?php if($value->vessel_id == $v->id_vessel): ?>
                <option value="<?=$v->id_vessel ?>" selected><?=$v->vessel ?></option>
              <?php else: ?>
                <option value="<?=$v->id_vessel ?>" <?=set_select('vessel_id[]', $v->id_vessel);?>><?=$v->vessel ?></option>
              <?php endif; ?>
              <?php endforeach; ?>
            </select>
            <span class="input-notes-bottom"><?=form_error('vessel_id[]')?></span>
          </div>
		  <div class="col-xs-3">
			<label class="control-label">Departure Date</label>
			<input class="form-control custom-datepicker" type="text" placeholder="YY-MM-DD" name="departure_date[]" value="<?=set_value('departure_date[]', $value->departure_date) ?>" required/>
			<span class="input-notes-bottom"><?=form_error('departure_date[]')?></span>
		  </div>
        </div>
        <div class="form-group">
          <div class="col-xs-3 has-feedback">
            <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
            <input class="form-control oj-time-picker oj-etd" type="text" placeholder="HH:MM" name="ETD[]" value="<?=set_value('ETD[]', $value->ETD);?>" disabled/>
            <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
            <span class="input-notes-bottom"><?=form_error('ETD[]')?></span>
          </div>
          <div class="col-xs-3 has-feedback">
            <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
            <input class="form-control oj-time-picker oj-eta" type="text" placeholder="HH:MM" name="ETA[]" value="<?=set_value('ETA[]', $value->ETA);?>" disabled/>
            <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
            <span class="input-notes-bottom"><?=form_error('ETA[]')?></span>
          </div>
          <div class="col-xs-3">
            <label class="control-label">Travel Time</label>
            <input class="form-control oj-time-picker oj-travel-time" type="text" value="300" readonly/>
            <span class="input-notes-bottom"></span>
          </div>
          <div class="col-xs-3">
            <label class="control-label">Status</label>
              <?php foreach ($subvoyage_management_status_list as $k => $v) : ?>
              <?php if($value->subvoyage_management_status_id == $v->id_subvoyage_management_status): ?>
                <input class="form-control" type="text" placeholder="Input status" value="<?=set_value('subvoyage_management_status_id[]', $v->description) ?>" readonly/>
              <?php endif; ?>
              <?php endforeach; ?>
            <span class="input-notes-bottom"><?=form_error('subvoyage_management_status_id[]')?></span>
          </div>
        </div>
      </div>

    </div>
    <?php endforeach; ?>
  </div>



  <div class="form-group oj-form-footer">
    <div class="col-xs-12">
      <div class="btn-oj-group right">
        <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
        <button type="submit" class="btn oj-button" id="btn-submit">Update</button>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div id="add-type-btn" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title" id="myModalLabel">Modal title</h4>
        </div>
        <div class="modal-body">
          <form action="">
            <div class="row">
              <div class="col-xs-6">
                <label class="control-label">Accommodation Type</label>
                <select class="selectpicker form-control" title="Choose">
                  <option value="">Choose one</option>
                  <option value="">Choose two</option>
                  <option value="">Choose three</option>
                </select>
              </div>
              <div class="col-xs-6">
                <label class="control-label">Passenger Fare</label>
                <select class="selectpicker form-control" title="Choose">
                  <option value="">Choose one</option>
                  <option value="">Choose two</option>
                  <option value="">Choose three</option>
                </select>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn oj-button">Add</button>
        </div>

      </div>
    </div>
  </div>
</form>
<?=$footer;?>