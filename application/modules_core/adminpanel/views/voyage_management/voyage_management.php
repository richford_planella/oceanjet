<?= $header; ?>
<div class="row table-header-adv">
    <div class="col-xs-12 hide-filter">
        <div class="row">
            <div class="col-xs-3">
                <div class="input-group">
                    <input type="text" class="form-control" id="voyage_search" placeholder="Search">
                </div>
            </div>
            <div class="col-xs-9">
                <div class="form-inline ojfilter-action">
                    <div class="form-group">
                        <label class="control-label label-adj-v1">Filter by:</label>
                        <select class="form-control open-page" id="show-select-forms">
                            <option>Please select</option>
                            <option value="select-date">Date</option>
                            <option value="select-voyage">Voyage</option>
                        </select>
                    </div>
                    <div class="form-group forms-hide" id="select-voyage">
                        <select class="form-control open-page" id="filter-page" name="voyage_id">
                            <option value="">Choose</option>
                            <?php foreach ($voyage_list as $key => $value) : ?>
                                <option value="<?= $value->voyage ?>"><?= $value->voyage ?></option>
                            <?php endforeach; ?>
                        </select>
                        <button type="button" class="btn oj-button box-btn filter-button">Go</button>
                    </div>
                    <div class="forms-hide" id="select-date">
                        <div class="form-group has-feedback">
                            <label class="control-label">From</label>
                            <input type="text" class="form-control oj-date-picker date-from" id="date-from" placeholder="YYYY-MM-DD">
                            <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label">To</label>
                            <input type="text" class="form-control oj-date-picker date-to" id="date-to" placeholder="YYYY-MM-DD">
                            <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 hide-filter">
        <div class="row ">
            <div class="col-xs-9 btn-oj-group">
                <label class="control-label label-adj-v2 ">Batch Action:</label>
                <input href="<?= admin_url($this->classname, 'editVoyage'); ?>" class="btn oj-button batch-btn" type="submit" value="Edit" id="edit-btn" disabled>
                <input href="<?= admin_url($this->classname, 'updateSchedStatus'); ?>" class="btn oj-button batch-btn" type="submit" value="Block" id="block-btn" disabled>
                <input href="<?= admin_url($this->classname, 'updateSchedStatus'); ?>" class="btn oj-button batch-btn" type="submit" value="Unblock" id="unblock-btn" disabled>
                <input href="<?= admin_url($this->classname, 'updateSchedStatus'); ?>" class="btn oj-button batch-btn" type="submit" value="Cancel" id="cancel-btn" disabled>
            </div>
            <div class="col-xs-3 text-right">
                <a href="<?= admin_url($this->classname, 'add'); ?>" class="btn oj-button">Create</a>
            </div>
        </div>
    </div>
</div>
<?= $this->load->view(admin_dir('notification')); ?>
<!-- /Table -->
<table id="voyage-management-table" class="example-table table global-table nopadding">
    <thead>
        <tr>
            <th><input type="checkbox" value="" id="check-all"></th>
            <th>Voyage Code</th>
            <th>No. of Legs</th>
            <th>Departure Date</th>
            <th>Origin</th>
            <th>Destination</th>
            <th>ETD</th>
            <th>ETA</th>
            <th>Travel Time</th>
            <th>Departure Delay Time</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($voyage_management)): ?>
            <?php foreach ($voyage_management as $u): ?>
                <tr>
                    <td><input type="checkbox" value="<?= $u->id_voyage_management ?>" class="oj-check"></td>
                    <td><?= $u->voyage ?></td>
                    <td><?= $u->no_of_subvoyages ?></td>
                    <td><?= $u->departure_date ?></td>
                    <td><?= $u->origin ?></td>
                    <td><?= $u->destination ?></td>
                    <td><?= date_format(new DateTime($u->ETD), 'g:i a'); ?></td>
                    <td><?= date_format(new DateTime($u->ETA), 'g:i a'); ?></td>
                    <td><?= $u->travel_time ?></td>
                    <td><?= $u->departure_delay_time ?></td>
                    <td>
                        <a href="<?= admin_url($this->classname, 'view', $u->id_voyage_management); ?>">View</a>
                        <a href="<?= admin_url($this->classname, 'edit', $u->id_voyage_management); ?>">Edit</a>
                        <a href="<?= admin_url($this->classname, 'delete', $u->id_voyage_management); ?>" class="btn-delete-voyage">Delete</a>
                    </td>
                </tr>   
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>
<?= $footer; ?>