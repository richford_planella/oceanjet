<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">Password History Count</label>
            <input class="form-control numbersOnly" placeholder="Input password history count" id="NUM_PASSWORD_CHANGE" name="NUM_PASSWORD_CHANGE" type="text" value="<?=set_value('NUM_PASSWORD_CHANGE', $NUM_PASSWORD_CHANGE);?>" />
            <span class="input-notes-bottom"><?php echo form_error('NUM_PASSWORD_CHANGE'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">No. of days password need to change</label>
            <input class="form-control numbersOnly" placeholder="Input no. of days password need to change" id="REQ_PASSWORD_CHANGE" name="REQ_PASSWORD_CHANGE" type="text" value="<?=set_value('REQ_PASSWORD_CHANGE', $REQ_PASSWORD_CHANGE);?>" />
            <span class="input-notes-bottom"><?php echo form_error('REQ_PASSWORD_CHANGE'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Minimum Password Character</label>
            <input class="form-control numbersOnly" placeholder="Input minimum password character" id="MIN_PASSWORD_CHAR" name="MIN_PASSWORD_CHAR" type="text" value="<?=set_value('MIN_PASSWORD_CHAR', $MIN_PASSWORD_CHAR);?>" />
            <span class="input-notes-bottom"><?php echo form_error('MIN_PASSWORD_CHAR'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Maximum Password Character</label>
            <input class="form-control numbersOnly" placeholder="Input maximum password character" id="MAX_PASSWORD_CHAR" name="MAX_PASSWORD_CHAR" type="text" value="<?=set_value('MAX_PASSWORD_CHAR', $MAX_PASSWORD_CHAR);?>" />
            <span class="input-notes-bottom"><?php echo form_error('MAX_PASSWORD_CHAR'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4">
            <label class="control-label">Password Required Uppercase Character Occurrence</label>
            <input class="form-control numbersOnly" placeholder="Input password required uppercase character occurrence" id="PASSWORD_REQ_UPPERCASE" name="PASSWORD_REQ_UPPERCASE" type="text" value="<?=set_value('PASSWORD_REQ_UPPERCASE', $PASSWORD_REQ_UPPERCASE);?>" />
            <span class="input-notes-bottom"><?php echo form_error('PASSWORD_REQ_UPPERCASE'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4">
            <label class="control-label">Password Required Lowercase Character Occurrence</label>
            <input class="form-control numbersOnly" placeholder="Input password required lowercase character occurrence" id="PASSWORD_REQ_LOWERCASE" name="PASSWORD_REQ_LOWERCASE" type="text" value="<?=set_value('PASSWORD_REQ_LOWERCASE', $PASSWORD_REQ_LOWERCASE);?>" />
            <span class="input-notes-bottom"><?php echo form_error('PASSWORD_REQ_LOWERCASE'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4">
            <label class="control-label">Password Required Number Occurrence</label>
            <input class="form-control numbersOnly" placeholder="Input password required number occurrence" id="PASSWORD_REQ_NUMBER" name="PASSWORD_REQ_NUMBER" type="text" value="<?=set_value('PASSWORD_REQ_NUMBER', $PASSWORD_REQ_NUMBER);?>" />
            <span class="input-notes-bottom"><?php echo form_error('PASSWORD_REQ_NUMBER'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-4">
            <label class="control-label">Password Required Special Characters Occurrence</label>
            <input class="form-control numbersOnly" placeholder="Input password required special characters occurrence" id="PASSWORD_REQ_SPL_CHAR" name="PASSWORD_REQ_SPL_CHAR" type="text" value="<?=set_value('PASSWORD_REQ_SPL_CHAR', $PASSWORD_REQ_SPL_CHAR);?>" />
            <span class="input-notes-bottom"><?php echo form_error('PASSWORD_REQ_SPL_CHAR'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">No.Log in attempt</label>
            <input class="form-control numbersOnly" placeholder="Input no.Log in attempt" id="LOGIN_ATTEMPT" name="LOGIN_ATTEMPT" type="text" value="<?=set_value('LOGIN_ATTEMPT', $LOGIN_ATTEMPT);?>" />
            <span class="input-notes-bottom"><?php echo form_error('LOGIN_ATTEMPT'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Minutes to re-activate locked account</label>
            <input class="form-control numbersOnly" placeholder="Input minutes to re-activate locked account" id="ENABLE_LOCKED_ACCT" name="ENABLE_LOCKED_ACCT" type="text" value="<?=set_value('ENABLE_LOCKED_ACCT', $ENABLE_LOCKED_ACCT);?>" />
            <span class="input-notes-bottom"><?php echo form_error('ENABLE_LOCKED_ACCT'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Minutes to re-authenticate after idle</label>
            <input class="form-control numbersOnly" placeholder="Input minutes to re-authenticate after idle" id="RE_AUTHENTICATE_ACCT" name="RE_AUTHENTICATE_ACCT" type="text" value="<?=set_value('RE_AUTHENTICATE_ACCT', $RE_AUTHENTICATE_ACCT);?>" />
            <span class="input-notes-bottom"><?php echo form_error('RE_AUTHENTICATE_ACCT'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">No. of days to auto deactivate account</label>
            <input class="form-control numbersOnly" placeholder="Input no. of days to auto deactivate account" id="INACTIVE_ACCT" name="INACTIVE_ACCT" type="text" placeholder="Input days" value="<?=set_value('INACTIVE_ACCT', $INACTIVE_ACCT);?>" />
            <span class="input-notes-bottom"><?php echo form_error('INACTIVE_ACCT'); ?></span>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <button type="submit" class="btn oj-button">Save</button>
            </div>
        </div>
    </div>
</form> 
<!-- END -->
<?=$footer;?>