<!--Warning Notification -->
<?php if($this->session->flashdata('note')):?>
<div class="oj-notify alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <strong>Warning!</strong> <?=$this->session->flashdata('note');?>
</div>
<?php endif;?>
<!--!End Warning Notification-->

<!--Success Notification-->
<?php if($this->session->flashdata('info')):?>
<div class="oj-notify alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <?=$this->session->flashdata('info');?>
</div>
<?php endif;?>
<!--!End Success Notification-->

<!--Success Notification-->
<?php if($this->session->flashdata('confirm')):?>
<div class="oj-notify alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <strong>Well done!</strong> <?=$this->session->flashdata('confirm');?>
</div>
<?php endif;?>
<!--!End Success Notification-->
      
<!--Error Notification-->
<?php if($this->session->flashdata('error')):?>
<div class="oj-notify alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <strong>Error!</strong> <?=$this->session->flashdata('error');?>
</div>
<?php endif;?>
<!--!End Error Notification-->