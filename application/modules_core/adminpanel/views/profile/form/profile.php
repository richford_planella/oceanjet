<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="row">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3 text-center profile-img-wrap">            
            <div class="form-group">
              <div class="col-xs-12">
                <label class="control-label pull-left">User Photo</label>
                <img id="img_uploaded" src="<?=($user->user_account->profile_pic)? img_dir("users/".$user->id_user.'/'.$user->user_account->profile_pic) : img_dir("Dummy_image.png");?>" alt="User Profile Picture" class="img-thumbnail">
                <div class="upload-img-wrap">
                    <input type="file" name="photo" id="imgInp" class="uploader" />
                    <label class="btn oj-button white-ghost-button oj-sm-button reset" for="reset">Reset</label>
                    <label class="btn oj-button white-ghost-button oj-sm-button" for="imgInp">Choose Image</label>
                    <input type="hidden" id="cur_img" value="<?=($user->user_account->profile_pic)? img_dir("users/".$user->id_user.'/'.$user->user_account->profile_pic) : img_dir("Dummy_image.png");?>">
                </div>
              </div>
          </div>
        </div>
        <div class="col-xs-9">
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Username</label>
                    <p class="form-control-static"><?=$user->username;?></p>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">User Role</label>
                    <?php foreach($user_profile as $up):?>
                    <?php if($user->user_profile_id == $up->id_user_profile):?>
                    <p class="form-control-static"><?=$up->user_profile;?></p>
                    <?php endif;?>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Last Name</label>
                    <p class="form-control-static"><?=$user->user_account->lastname;?></p>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">First Name</label>
                    <p class="form-control-static"><?=$user->user_account->firstname;?></p>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">Middle Initial</label>
                    <p class="form-control-static"><?=$user->user_account->middlename;?></p>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Email Address</label>
                    <p class="form-control-static"><?=$user->user_account->email_address;?></p>
                </div>
                <?php if($user_type):?>
                <div class="col-xs-4 filter_val">
                    <label class="control-label">Outlet</label>
                    <?php foreach($outlet as $o):?>
                    <?php if($user->user_account->outlet_id == $o->id_outlet):?>
                    <input disabled="disabled" class="form-control" id="outlet_id" name="outlet_id" type="text" value="<?=set_value('outlet_id', $o->outlet);?>">
                    <?php endif;?>
                    <?php endforeach;?>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
    <div class="form-group oj-form-footer">
      <div class="col-xs-12">
        <div class="btn-oj-group right">
            <button type="submit" class="btn oj-button">Update profile picture</button>
        </div>
      </div>
    </div>
</form>
<?=$footer;?>