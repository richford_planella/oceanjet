<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="row">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3 text-center profile-img-wrap">
          <div class="form-group">
            <div class="col-xs-12">
                <label class="control-label pull-left">*Note</label>
                <p class="form-control-custom">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at malesuada est. Nulla tincidunt elementum nisi, eget blandit dolor consequat vel. Nullam in risus quis nisl suscipit pellentesque a dictum neque.  </p>
            </div>
          </div>
        </div>
        <div class="col-xs-9">
            <div class="form-group">
                <div class="col-xs-6">
                    <label class="control-label">Current Password</label>
                    <input class="form-control" name="cur_password" type="password" placeholder="Input current password" />
                    <span class="input-notes-bottom"><?php echo form_error('cur_password'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <label class="control-label">New Password</label>
                    <input class="form-control" name="password" type="password" placeholder="Input new password" />
                    <span class="input-notes-bottom"><?php echo form_error('password'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <label class="control-label">Confirm New Password</label>
                    <input class="form-control" name="new_password" type="password" placeholder="Input confirm new password" />
                    <span class="input-notes-bottom"><?php echo form_error('new_password'); ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <button type="submit" class="btn oj-button">Update</button>
            </div>
        </div>
    </div>
</form>
<?=$footer;?>