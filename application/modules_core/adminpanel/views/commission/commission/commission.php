<?=$header;?>

		<div class="row-fluid sortable">		
			<div class="box span12">
				<div class="box-header" data-original-title>
					<h2><i class="icon-reorder"></i><span class="break"></span><b>Commission</b></h2>
					<div class="box-icon">
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<p class="center">
								<a class="btn btn-large btn-success" href="<?=admin_url($this->classname, 'add');?>"><i class="halflings-icon white plus"></i>Create</a>
						</p>
						
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
					  <thead>
						  <tr>
								<th class="align-center">Commission Code</th>
								<th>Description</th>
								<th>Percentage (%)</th>
								<th class="align-center">Status</th>
								<th class="align-center">Actions</th>
						  </tr>
					  </thead>   
					  <tbody>
					  
								<?php if(!empty($commission)):?>
									<?php foreach($commission as $u):?>
										<tr>
											<td class="align-center"><?=$u->commission_code;?></td>
											<td class="center"><?=$u->commission?></td>
											<td class="center"><?=$u->commission_percentage;?></td>
											<td class="align-center">
												<?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-important">Inactive</span>';?>
											</td>
											<td class="align-center">
												<a class="btn" href="<?=admin_url('commission', 'view', $u->id_commission);?>" title="View" data-rel="tooltip">
													<i class="halflings-icon white zoom-in"></i>  
												</a>
												<a class="btn btn-info" href="<?=admin_url($this->classname, 'edit', $u->id_commission);?>" title="Edit" data-rel="tooltip">
													<i class="halflings-icon white edit"></i>  
												</a>
												<?php if($u->enabled):?>
													<a class="btn btn-danger" href="<?=admin_url($this->classname, 'toggle', $u->id_commission, md5($token.$this->classname.$u->id_commission));?>" title="De-activate" data-rel="tooltip">
															<i class="halflings-icon white remove"></i> 
													</a>
												<?php else: ?>
													<a class="btn btn-success" href="<?=admin_url($this->classname, 'toggle', $u->id_commission, md5($token.$this->classname.$u->id_commission));?>" title="Re-activate" data-rel="tooltip">
															<i class="halflings-icon white ok"></i> 
													</a>
												<?php endif;?>
											</td>
									</tr>   
								<?php endforeach;?>
							<?php endif;?>
					  </tbody>
					  
					</table>            
				</div>
			</div><!--/span-->
		
		</div><!--/row-->

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<?=$footer;?>