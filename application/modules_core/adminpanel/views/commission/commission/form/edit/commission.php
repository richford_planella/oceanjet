<?=$header;?>
	<form class="form-horizontal" action="<?=current_url();?>" method="POST">
			<div class="row-fluid ">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>Modify Commission</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="commission_code">Commission Code</label>
								<div class="controls">
								  <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="commission_code" name="commission_code" type="text" value="<?=set_value('commission_code', $commission->commission_code);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission">Description</label>
								<div class="controls">
								  <input class="input-xlarge number focused commission filter_val" id="commission" name="commission" type="text" value="<?=set_value('commission', $commission->commission);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="outlet_code">Percentage</label>
								<div class="controls">
								  <input class="input-xlarge number focused commission_percentage filter_val" id="commission_percentage" name="commission_percentage" type="text" value="<?=set_value('commission_percentage', $commission->commission_percentage);?>" onkeypress="numeric_validation(event)">
								</div>
							</div>
							<div class="control-group">
								  <label class="control-label" for="enabled">Status</label>
								  <div class="controls">
									<select id="enabled" name="enabled" data-rel="chosen">
										<option value="">Please Select</option>
										<option value="1" <?=set_select('enabled', '1',(($commission->enabled) ? TRUE : ''));?>>Active</option>
										<option value="0" <?=set_select('enabled', '0',((!$commission->enabled) ? TRUE : ''));?>>Inactive</option>
									</select>
								  </div>
							</div>
						   
							  <div class="form-actions">
								<button type="submit" class="btn btn-primary" >Save changes</button>
								<a href="<?=admin_url($this->classname);?>" class="btn">Cancel</a>
							</div>
															
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->    
                    
	</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>