<?=$header;?>
<form class="form-horizontal oj-form " action="<?=admin_url($this->classname,'issue');?>" method="POST">
<?=$this->load->view(admin_dir('notification'));?>
	
	<div class="form-group">
		<div class="col-xs-4">
			<label class="control-label">Ticket Number</label>
			<input type="text" id="ticket_number" name="ticket_number" class="form-control" />
			<span class="input-notes-bottom"><?=form_error('ticket_number')?></span>
		</div>
		
		<div class="col-xs-4">
			<label class="control-label">Last Name</label>
			<input type="text" id="last_name" name="last_name" class="form-control" />
			<span class="input-notes-bottom"><?=form_error('last_name')?></span>
		</div>
		
		<div class="col-xs-4">
			<label class="control-label">&nbsp;</label>
			<div class="btn-oj-group right">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname, 'add');?>">Clear</a>
				<button type="submit" class="btn oj-button">Search</button>
			</div>
		</div>
	</div>
	
</form>
<?=$footer;?>