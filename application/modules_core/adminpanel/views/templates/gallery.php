                        <?=$header;?>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon picture"></i><span class="break"></span> Gallery</h2>
						<div class="box-icon">
							<a href="#" id="toggle-fullscreen" class="hidden-phone hidden-tablet"><i class="halflings-icon fullscreen"></i></a>
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<div class="masonry-gallery">
														<div id="image-1" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo1.jpg');?>)" title="Sample Image 1" href="<?=img_dir('gallery/photo1.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo1.jpg');?>" alt="Sample Image 1"></a>
							</div>
														<div id="image-2" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo2.jpg');?>)" title="Sample Image 2" href="<?=img_dir('gallery/photo2.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo2.jpg');?>" alt="Sample Image 2"></a>
							</div>
														<div id="image-3" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo3.jpg');?>)" title="Sample Image 3" href="<?=img_dir('gallery/photo3.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo3.jpg');?>" alt="Sample Image 3"></a>
							</div>
														<div id="image-4" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo4.jpg');?>)" title="Sample Image 4" href="<?=img_dir('gallery/photo4.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo4.jpg');?>" alt="Sample Image 4"></a>
							</div>
														<div id="image-5" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo5.jpg');?>)" title="Sample Image 5" href="<?=img_dir('gallery/photo5.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo5.jpg');?>" alt="Sample Image 5"></a>
							</div>
														<div id="image-6" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo6.jpg');?>)" title="Sample Image 6" href="<?=img_dir('gallery/photo6.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo6.jpg');?>" alt="Sample Image 6"></a>
							</div>
														<div id="image-7" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo7.jpg');?>)" title="Sample Image 7" href="<?=img_dir('gallery/photo7.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo7.jpg');?>" alt="Sample Image 7"></a>
							</div>
														<div id="image-8" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo8.jpg');?>)" title="Sample Image 8" href="<?=img_dir('gallery/photo8.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo8.jpg');?>" alt="Sample Image 8"></a>
							</div>
														<div id="image-9" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo9.jpg');?>)" title="Sample Image 9" href="<?=img_dir('gallery/photo9.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo9.jpg');?>" alt="Sample Image 9"></a>
							</div>
														<div id="image-10" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo10.jpg');?>)" title="Sample Image 10" href="<?=img_dir('gallery/photo10.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo10.jpg');?>" alt="Sample Image 10"></a>
							</div>
														<div id="image-11" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo11.jpg');?>)" title="Sample Image 11" href="<?=img_dir('gallery/photo11.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo11.jpg');?>" alt="Sample Image 11"></a>
							</div>
														<div id="image-12" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo12.jpg');?>)" title="Sample Image 12" href="<?=img_dir('gallery/photo12.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo12.jpg');?>" alt="Sample Image 12"></a>
							</div>
														<div id="image-13" class="masonry-thumb">
								<a style="background:url(<?=img_dir('gallery/photo13.jpg');?>)" title="Sample Image 13" href="<?=img_dir('gallery/photo13.jpg');?>"><img class="grayscale" src="<?=img_dir('gallery/photo13.jpg');?>" alt="Sample Image 13"></a>
							</div>
													</div>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
    

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<?=$footer;?>