<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>User Role</th>
                <th>With Outlet</th>
                <th>Status</th>
                <th>Level</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($user_profile)):?>
            <?php foreach($user_profile as $up):?>
            <tr>
                <td><?=$up->user_profile;?></td>
                <td><?=($up->is_outlet)? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>';?></td>
                <td><?=($up->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
                <td><?=$up->user_level;?></td>
                <td>
                    <a href="<?=admin_url($this->classname, 'view', $up->id_user_profile);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $up->id_user_profile);?>">Edit</a>
                    <a href="<?=admin_url($this->classname, 'delete', $up->id_user_profile);?>" class="delete-user-profile">Delete</a>
                    <?php if($up->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $up->id_user_profile, md5($token.$this->classname.$up->id_user_profile));?>" class="de-activate-user-profile">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $up->id_user_profile, md5($token.$this->classname.$up->id_user_profile));?>" class="activate-user-profile">
                                Re-activate
                        </a>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
<?=$footer;?>