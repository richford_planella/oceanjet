<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">User Role</label>
            <input class="form-control" type="text" name="user_profile" placeholder="Input user role" value="<?=set_value('user_profile', $user_profile->user_profile);?>" />
            <span class="input-notes-bottom"><?php echo form_error('user_profile'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Level</label>
            <select class="selectpicker form-control" id="user_level_id" name="user_level_id">
                <option value="">Please select</option>
                <?php foreach($level as $l):?>
                    <option value="<?=$l->id_user_level;?>" <?=set_select('user_level_id', $l->id_user_level, ($l->id_user_level == $user_profile->user_level_id) ? TRUE : "");?> ><?=$l->user_level;?></option>
                <?php endforeach;?>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('user_level_id'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Accessible Levels</label>
            <div class="checkbox ">
                <?php foreach($level as $l):?>
                <label class="checkbox-inline checkbox__label">
                    <input class="checkbox__btn" type="checkbox" name="user_profile_level[<?=$l->id_user_level;?>]" <?=set_checkbox("user_profile_level[$l->id_user_level]", $l->id_user_level, (isset($l->checked) AND $l->checked) ? TRUE : ''); ?> value="<?=$l->id_user_level;?>"> <?=$l->user_level;?>
                </label>
                <?php endforeach;?>
            </div>
            <span class="input-notes-bottom"><?php echo form_error('user_profile_level'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Transaction Option</label>
            <div class="checkbox ">
                <label class="checkbox-inline checkbox__label">
                    <input class="checkbox__btn" type="checkbox" name="has_transaction" id="has_transaction" <?=set_checkbox("has_transaction", 1, ($user_profile->has_transaction) ? TRUE : ''); ?> value="1"> With sales transactions
                </label>
            </div>
            <span class="input-notes-bottom"><?php echo form_error('has_transaction'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Outlet Enabled</label>
            <select class="selectpicker form-control" title="Choose" id="is_outlet" name="is_outlet">
                <option value="">Please select</option>
                <option value="1" <?=set_select('is_outlet', '1',(($user_profile->is_outlet) ? TRUE : ''));?>>Yes</option>
                <option value="0" <?=set_select('is_outlet', '0',((!$user_profile->is_outlet) ? TRUE : ''));?>>No</option>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('is_outlet'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Session Expiry</label>
            <select class="selectpicker form-control" title="Select session expiry" id="session_timeout" name="session_timeout">
                <option value="">Please select</option>
                <option value="5" <?=set_select('session_timeout', '5', ('5' == $user_profile->session_timeout) ? TRUE : "");?>>5 minutes</option>
                <option value="15" <?=set_select('session_timeout', '15', ('15' == $user_profile->session_timeout) ? TRUE : "");?>>15 minutes</option>
                <option value="30" <?=set_select('session_timeout', '30', ('30' == $user_profile->session_timeout) ? TRUE : "");?>>30 minutes</option>
                <option value="60" <?=set_select('session_timeout', '60', ('60' == $user_profile->session_timeout) ? TRUE : "");?>>1 hour</option>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('session_timeout'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Mobile Access</label>
            <div class="checkbox ">
                <label class="checkbox-inline checkbox__label">
                    <input class="checkbox__btn" type="checkbox" name="has_mobile_access" id="has_mobile_access" <?=set_checkbox("has_mobile_access", 1, ($user_profile->has_mobile_access) ? TRUE : ''); ?> value="1"> Access system via mobile
                </label>
            </div>
            <span class="input-notes-bottom"><?php echo form_error('has_mobile_access'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Status</label>
            <select class="selectpicker form-control" id="enabled" name="enabled">
                <option value="1" <?=set_select('enabled', '1',(($user_profile->enabled) ? TRUE : ''));?>>Active</option>
                <option value="0" <?=set_select('enabled', '0',((!$user_profile->enabled) ? TRUE : ''));?>>Inactive</option>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('enabled'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Menu Option</label>
            <div class="checkbox ">
                <label class="checkbox-inline checkbox__label">
                    <input class="checkbox__btn" type="checkbox" name="has_menu" id="has_menu" <?=set_checkbox("has_menu", 1, ($user_profile->has_menu) ? TRUE : ""); ?> value="1" > Display menu header
                </label>
            </div>
            <span class="input-notes-bottom"><?php echo form_error('has_menu'); ?></span>
        </div>
        <div class="col-xs-3 landing-page">
            <label class="control-label">Landing Page</label>
            <select class="selectpicker form-control" id="administrator_page_id" name="administrator_page_id">
                <option value="">Please select</option>
                <?php foreach($sub_page as $sp):?>
                    <option value="<?=$sp->id_administrator_page;?>" <?=set_select('administrator_page_id', $sp->id_administrator_page, ($user_profile->administrator_page_id == $sp->id_administrator_page) ? TRUE : '' );?> ><?=$sp->administrator_page;?></option>
                <?php endforeach;?>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('administrator_page_id'); ?></span>
        </div>
    </div>
    <!-- Main pages -->
    <div class="form-group margin-top-30">
        <div class="col-xs-3">
            <label class="control-label">Main Module</label>
            <span class="input-notes-top overflow-right-10">*Important</span>
            <!--Change the value attribute base your set ID for user-matrix__modules DOM-->
            <select class="selectpicker form-control" id="select_main_page" name="select_main_page">
                <?php foreach($pages as $p):?>
                <option value="<?=$p->id_administrator_page;?>"><?=$p->administrator_page;?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="col-xs-9">
            <label class="control-label">Accessible Levels</label>
            <?php $counter = 1;?>
            <?php foreach($pages as $p):?>
            <div class="checkbox checkbox--padding main-page admin-page-<?=$p->id_administrator_page;?> <?=($counter <> 1)? 'hidden' : '';?>">
                <input type="hidden" name="page[<?=$p->id_administrator_page;?>][page_id]" value="<?=$p->id_administrator_page;?>" />
                <label class="checkbox-inline checkbox__label">
                    <input type="checkbox" name="page[<?=$p->id_administrator_page;?>][access]" value="1" <?=set_checkbox('page['.$p->id_administrator_page.'][access]',1, ($p->access) ? TRUE : '');?> /> Access
                </label>
                <label class="checkbox-inline checkbox__label">
                    <input type="checkbox" name="page[<?=$p->id_administrator_page;?>][view]" value="1" <?=set_checkbox('page['.$p->id_administrator_page.'][view]',1, ($p->view) ? TRUE : '');?>/> View
                </label>
                <label class="checkbox-inline checkbox__label">
                    <input type="checkbox" name="page[<?=$p->id_administrator_page;?>][add]" value="1" <?=set_checkbox('page['.$p->id_administrator_page.'][add]',1, ($p->add) ? TRUE : '');?>/> Add
                </label>
                <label class="checkbox-inline checkbox__label">
                    <input type="checkbox" name="page[<?=$p->id_administrator_page;?>][edit]" value="1" <?=set_checkbox('page['.$p->id_administrator_page.'][edit]',1, ($p->edit) ? TRUE : '');?>/> Edit
                </label>
                <label class="checkbox-inline checkbox__label">
                    <input type="checkbox" name="page[<?=$p->id_administrator_page;?>][delete]" value="1" <?=set_checkbox('page['.$p->id_administrator_page.'][delete]',1, ($p->delete) ? TRUE : '');?>/> Delete
                </label>
                <label class="checkbox-inline checkbox__label">
                    <input type="checkbox" name="page[<?=$p->id_administrator_page;?>][export]" value="1" <?=set_checkbox('page['.$p->id_administrator_page.'][export]',1, ($p->export) ? TRUE : '');?>/> Export
                </label>
                <label class="checkbox-inline checkbox__label">
                    <input type="checkbox" name="page[<?=$p->id_administrator_page;?>][print]" value="1" <?=set_checkbox('page['.$p->id_administrator_page.'][print]',1, ($p->print) ? TRUE : '');?>/> Print
                </label>
            </div>
            <?php $counter++;?>
            <?php endforeach;?>
        </div>
    </div>
    <!-- End Main page -->
    <!-- Sub-Module -->
    <div class="form-group user-matrix__modules default" id="module1">
        <div class="col-xs-12">
            <?php $subcounter = 1;?>
            <?php foreach($pages as $p):?>
            <div class="main-page admin-page-<?=$p->id_administrator_page;?> <?=($subcounter <> 1)? 'hidden' : '';?>">
                <table class="table table-fixed table-striped">
                    <thead>
                        <tr>
                            <th class="col-xs-3">Sub Module for <?=$p->administrator_page;?></th>
                            <th class="col-xs-9">Select Sub Function</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($p->subpage as $sp):?>
                        <tr>
                            <td class="col-xs-3"><?=$sp->administrator_page;?></td>
                            <td class="col-xs-9 user-matrix--checkboxs">
                                <label class="checkbox-inline">
                                    <input type="hidden" name="page[<?=$sp->id_administrator_page;?>][page_id]" value="<?=$sp->id_administrator_page;?>" />
                                    <input type="checkbox" name="page[<?=$sp->id_administrator_page;?>][access]" value="1" <?=set_checkbox('page['.$sp->id_administrator_page.'][access]',1, ($sp->access) ? TRUE : '');?> /> Access
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="page[<?=$sp->id_administrator_page;?>][view]" value="1" <?=set_checkbox('page['.$sp->id_administrator_page.'][view]',1, ($sp->view) ? TRUE : '');?>/> View
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="page[<?=$sp->id_administrator_page;?>][add]" value="1" <?=set_checkbox('page['.$sp->id_administrator_page.'][add]',1, ($sp->add) ? TRUE : '');?>/> Add
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="page[<?=$sp->id_administrator_page;?>][edit]" value="1" <?=set_checkbox('page['.$sp->id_administrator_page.'][edit]',1, ($sp->edit) ? TRUE : '');?>/> Edit
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="page[<?=$sp->id_administrator_page;?>][delete]" value="1" <?=set_checkbox('page['.$sp->id_administrator_page.'][delete]',1, ($sp->delete) ? TRUE : '');?>/> Delete
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="page[<?=$sp->id_administrator_page;?>][export]" value="1" <?=set_checkbox('page['.$sp->id_administrator_page.'][export]',1, ($sp->export) ? TRUE : '');?>/> Export
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="page[<?=$sp->id_administrator_page;?>][print]" value="1" <?=set_checkbox('page['.$sp->id_administrator_page.'][print]',1, ($sp->print) ? TRUE : '');?>/> Print
                                </label>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <?php $subcounter++;?>
            <?php endforeach;?>
        </div>
    </div>
    <!-- End Sub-Module -->
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
                <button type="submit" class="btn oj-button">Update</button>
            </div>
        </div>
    </div>
</form> 
<!-- END -->
<?=$footer;?>