<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="row">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3 text-center profile-img-wrap">
            <div class="form-group">
                <div class="col-xs-12">
                    <label class="control-label pull-left">User Photo</label>
                    <img id="img_uploaded" src="<?=($user->user_account->profile_pic)? img_dir("users/".$user->id_user.'/'.$user->user_account->profile_pic) : img_dir("Dummy_image.png");?>" alt="User Profile Picture" class="img-thumbnail">
                </div>
          </div>
        </div>
        <div class="col-xs-9">
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Username</label>
                    <input disabled="disabled" class="form-control" id="username" name="username" type="text" placeholder="Input username" value="<?=set_value('username', $user->username);?>" />
                    <span class="input-notes-bottom"><?php echo form_error('username'); ?></span>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">User Role</label>
                    <?php foreach($user_profile as $up):?>
                    <?php if($user->user_profile_id == $up->id_user_profile):?>
                    <input disabled="disabled" class="form-control" id="user_profile_id" name="user_profile_id" type="text" value="<?=set_value('user_profile_id', $up->user_profile);?>" />
                    <?php endif;?>
                    <?php endforeach;?>
                    <span class="input-notes-bottom"><?php echo form_error('username'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Last Name</label>
                    <input disabled="disabled" class="form-control" id="lastname" name="lastname" type="text" placeholder="Input lastname" value="<?=set_value('lastname', $user->user_account->lastname);?>" />
                    <span class="input-notes-bottom"><?php echo form_error('lastname'); ?></span>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">First Name</label>
                    <input disabled="disabled" class="form-control" id="firstname" name="firstname" type="text" placeholder="Input firstname" value="<?=set_value('firstname', $user->user_account->firstname);?>" />
                    <span class="input-notes-bottom"><?php echo form_error('firstname'); ?></span>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">Middle Initial</label>
                    <input disabled="disabled" class="form-control" id="middlename" name="middlename" type="text" placeholder="Input middlename" value="<?=set_value('middlename', $user->user_account->middlename);?>" />
                    <span class="input-notes-bottom"><?php echo form_error('middlename'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Email Address</label>
                    <input disabled="disabled" class="form-control" id="email_address" name="email_address" type="text" placeholder="Input email address" value="<?=set_value('email_address', $user->user_account->email_address);?>" />
                    <span class="input-notes-bottom"><?php echo form_error('email_address'); ?></span>
                </div>
                <div class="col-xs-4 filter_val">
                    <label class="control-label">Outlet</label>
                    <?php foreach($outlet as $o):?>
                    <?php if($user->user_account->outlet_id == $o->id_outlet):?>
                    <input disabled="disabled" class="form-control" id="outlet_id" name="outlet_id" type="text" value="<?=set_value('outlet_id', $o->outlet);?>">
                    <?php endif;?>
                    <?php endforeach;?>
                    <span class="input-notes-bottom"><?php echo form_error('outlet_id'); ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group oj-form-footer">
      <div class="col-xs-12">
        <div class="btn-oj-group right">
            <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
            <a href="<?=admin_url($this->classname, 'edit', $user->id_user);?>" class="btn oj-button">Edit</a>
        </div>
      </div>
    </div>
</form>
<script type="text/javascript">
    var user_profile = '<?=$user_type;?>';
</script>
<?=$footer;?>