<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="row">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3 text-center profile-img-wrap">
            <div class="form-group">
              <div class="col-xs-12">
                <label class="control-label pull-left">User Photo</label>
                <img id="img_uploaded" src="<?=img_dir("Dummy_image.png");?>" alt="User Profile Picture" class="img-thumbnail">
                <div class="upload-img-wrap">
                    <input type="file" name="photo" id="imgInp" class="uploader" />
                    <label class="btn oj-button white-ghost-button oj-sm-button" for="imgInp">Choose Image</label>
                </div>
              </div>
          </div>
        </div>
        <div class="col-xs-9">
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Username</label>
                    <input class="form-control" id="username" name="username" type="text" placeholder="Input username" value="<?=set_value('username');?>" />
                    <span class="input-notes-bottom"><?php echo form_error('username'); ?></span>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">User Role</label>
                    <select class="selectpicker form-control" id="user_profile_id" name="user_profile_id">
                        <option value="">Please select</option>
                        <?php foreach($user_profile as $up):?>
                            <option value="<?=$up->id_user_profile;?>" <?=set_select('user_profile_id', $up->id_user_profile);?> outlet="<?=$up->is_outlet;?>"><?=$up->user_profile?></option>
                        <?php endforeach;?>
                    </select>
                    <span class="input-notes-bottom"><?php echo form_error('user_profile_id'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Last Name</label>
                    <input class="form-control" id="lastname" name="lastname" type="text" placeholder="Input lastname" value="<?=set_value('lastname');?>" />
                    <span class="input-notes-bottom"><?php echo form_error('lastname'); ?></span>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">First Name</label>
                    <input class="form-control" id="firstname" name="firstname" type="text" placeholder="Input firstname" value="<?=set_value('firstname');?>" />
                    <span class="input-notes-bottom"><?php echo form_error('firstname'); ?></span>
                </div>
                <div class="col-xs-4">
                    <label class="control-label">Middle Initial</label>
                    <input class="form-control" id="middlename" name="middlename" type="text" placeholder="Input middlename" value="<?=set_value('middlename');?>" />
                    <span class="input-notes-bottom"><?php echo form_error('middlename'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <label class="control-label">Email Address</label>
                    <input class="form-control" id="email_address" name="email_address" type="text" placeholder="Input email address" value="<?=set_value('email_address');?>" />
                    <span class="input-notes-bottom"><?php echo form_error('email_address'); ?></span>
                </div>
                <div class="col-xs-4 filter_val">
                    <label class="control-label">Outlet</label>
                    <select class="selectpicker form-control" id="outlet_id" name="outlet_id">
                        <option value="">Please select</option>
                        <?php foreach($outlet as $o):?>
                            <option value="<?=$o->id_outlet;?>" <?=set_select('outlet_id', $o->id_outlet);?>><?=$o->outlet;?></option>
                        <?php endforeach;?>
                    </select>
                    <span class="input-notes-bottom"><?php echo form_error('outlet_id'); ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group oj-form-footer">
      <div class="col-xs-12">
        <div class="btn-oj-group right">
          <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
          <button type="submit" class="btn oj-button">Save</button>
        </div>
      </div>
    </div>
</form>
<script type="text/javascript">
    var user_profile = '<?=$user_type;?>';
    var page = 'add';
</script>
<?=$footer;?>