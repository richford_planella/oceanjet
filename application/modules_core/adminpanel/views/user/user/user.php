<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>Username</th>
                <th>User Role</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Middle Initial</th>
                <th>Email Address</th>
                <th>Outlet</th>
                <th>Status</th>
                <th>Locked</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($user)):?>
            <?php foreach($user as $u):?>
            <tr>
                <td><?=$u->username;?></td>
                <td><?=$u->user_profile;?></td>
                <td><?=$u->user_account->firstname;?></td>
                <td><?=$u->user_account->lastname;?></td>
                <td><?=$u->user_account->middlename;?></td>
                <td><?=$u->user_account->email_address;?></td>
                <td><?=$u->user_account->outlet;?></td>
                <td><?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
                <td><?=($u->is_locked)? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>';?></td>
                <td>
                    <a href="<?=admin_url($this->classname, 'view', $u->id_user);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $u->id_user);?>">Edit</a>
                    <?php if($u->is_locked):?>
                        <a href="<?=admin_url($this->classname, 'toggleLocked', $u->id_user, md5($token.$this->classname.$u->id_user));?>" class="unlock-user">
                                Unlock Account
                        </a>
                    <?php else:?>
                        <a href="<?=admin_url($this->classname, 'toggleLocked', $u->id_user, md5($token.$this->classname.$u->id_user));?>" class="lock-user">
                                Lock Account
                        </a>
                    <?php endif;?>

                    <?php if($u->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_user, md5($token.$this->classname.$u->id_user));?>" class="de-activate-user">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_user, md5($token.$this->classname.$u->id_user));?>" class="activate-user">
                                Re-activate
                        </a>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
<?=$footer;?>