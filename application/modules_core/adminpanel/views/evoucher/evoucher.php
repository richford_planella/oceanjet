<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>E-voucher Type</th>
                <th>Origin</th>
                <th>Destination</th>
                <th>Accommodation Type</th>
                <th>No. of Allotted E-vouchers</th>
                <th>No. of Generated E-vouchers</th>
                <th>Override Rate</th>
                <th>Amount/Percentage</th>
                <th>Validity</th>
                <th>Issuing Outlet</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($evoucher)):?>
            <?php foreach($evoucher as $v):?>
            <tr>
                <td><?=$v->evoucher;?></td>
                <td><?=$v->orig_port;?></td>
                <td><?=$v->des_port;?></td>
                <td><?=$v->accommodation;?></td>
                <td><?=$v->alloted_evoucher;?></td>
                <td><?=$v->generated_evoucher;?></td>
                <td>
                    <?php if($v->evoucher_type == 1):?>
                        <?='Override Amount';?>
                    <?php elseif($v->evoucher_type == 2):?>
                        <?='Discount Amount';?>
                    <?php elseif($v->evoucher_type == 3):?>
                        <?='Discount Percent';?>
                    <?php endif;?>
                </td>
                <td><?=($v->evoucher_type == 1 OR $v->evoucher_type == 2)? "P ".number_format($v->evoucher_amount,2,'.',',') : $v->evoucher_amount." %";?></td>
                <td><?=$v->validity;?></td>
                <td><?=$v->outlet;?></td>
                <td>
                    <a href="<?=admin_url($this->classname, 'generate', $v->id_evoucher);?>">Generate E-vouchers</a>
                    <a href="<?=admin_url($this->classname, 'view', $v->id_evoucher);?>">View List</a>
                    <a href="<?=admin_url($this->classname, 'edit', $v->id_evoucher);?>">Edit</a>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
<?=$footer;?>