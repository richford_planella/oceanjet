<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">E-voucher Type</label>
            <input class="form-control" id="evoucher" name="evoucher" type="text" value="<?=set_value('evoucher');?>" placeholder="Input e-voucher type" />
            <span class="input-notes-bottom"><?php echo form_error('evoucher'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Origin</label>
            <select class="selectpicker form-control" id="origin_id" name="origin_id">
                <option value="">Please select</option>
                <?php foreach($port as $p):?>
                    <option value="<?=$p->id_port;?>" <?=set_select('origin_id', $p->id_port);?>><?=$p->port;?></option>
                <?php endforeach;?>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('origin_id'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Destination</label>
            <select class="selectpicker form-control" id="destination_id" name="destination_id">
                <option value="">Please select</option>
                <?php foreach($port as $p):?>
                    <option value="<?=$p->id_port;?>" <?=set_select('destination_id', $p->id_port);?>><?=$p->port;?></option>
                <?php endforeach;?>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('destination_id'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Accommodation</label>
            <select class="selectpicker form-control" id="accommodation_id" name="accommodation_id">
                <option value="">Please select</option>
                <?php foreach($accommodation as $a):?>
                    <option value="<?=$a->id_accommodation;?>" <?=set_select('accommodation_id', $a->id_accommodation);?>><?=$a->accommodation;?></option>
                <?php endforeach;?>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('accommodation_id'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Quantity</label>
            <input class="form-control numbersOnly" id="quantity" name="quantity" type="text" value="<?=set_value('quantity');?>" placeholder="Input quantity" />
            <span class="input-notes-bottom"><?php echo form_error('quantity'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Override Rate</label>
            <select class="selectpicker form-control" id="evoucher_type" name="evoucher_type">
                <option value="1" <?=set_select('evoucher_type', '1');?>>Override Amount</option>
                <option value="2" <?=set_select('evoucher_type', '2');?>>Discount Amount</option>
                <option value="3" <?=set_select('evoucher_type', '3');?>>Discount Percent</option>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('evoucher_type'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Amount / Percent</label>
            (<span id="select_amount">P</span><span id="select_percent">%</span>)
            <input class="form-control price" id="evoucher_amount" name="evoucher_amount" type="text" value="<?=set_value('evoucher_amount');?>" placeholder="Input unit of amount/percent" />
            <span class="input-notes-bottom"><?php echo form_error('evoucher_amount'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Issuing outlet</label>
            <select class="selectpicker form-control" id="outlet_id" name="outlet_id">
                <option value="">Please select</option>
                <?php foreach($outlet as $o):?>
                    <option value="<?=$o->id_outlet;?>" <?=set_select('outlet_id', $o->id_outlet);?>><?=$o->outlet;?></option>
                <?php endforeach;?>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('outlet_id'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Validity</label>
            <input class="form-control numbersOnly" id="validity" name="validity" type="text" value="<?=set_value('validity');?>" placeholder="Input validity" />
            <span class="input-notes-bottom"><?php echo form_error('validity'); ?></span>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
                <button type="submit" id="submit_btn" name="submit" value="submit" class="btn oj-button">Save</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var set_amount = '<?=$set_amount;?>';
</script>
<?=$footer;?>       