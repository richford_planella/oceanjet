<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">E-voucher Type</label>
            <input disabled="disabled" class="form-control" id="evoucher" name="evoucher" type="text" value="<?=set_value('evoucher', $evoucher->evoucher);?>" placeholder="Input e-voucher type" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Origin</label>
            <?php foreach($port as $p):?>
            <?php if($p->id_port == $evoucher->origin_id):?>
                <input disabled="disabled" class="form-control" id="origin_id" name="origin_id" type="text" value="<?=set_value('origin_id', $p->port);?>">
            <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Destination</label>
            <?php foreach($port as $p):?>
            <?php if($p->id_port == $evoucher->destination_id):?>
                <input disabled="disabled" class="form-control" id="destination_id" name="destination_id" type="text" value="<?=set_value('destination_id', $p->port);?>">
            <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Accommodation</label>
            <?php foreach($accommodation as $a):?>
                <?php if($a->id_accommodation == $evoucher->accommodation_id):?>
                <input disabled="disabled" class="form-control" id="accommodation_id" name="accommodation_id" type="text" value="<?=set_value('accommodation_id', $a->accommodation);?>">
                <?php endif;?>
            <?php endforeach;?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Quantity</label>
            <input disabled="disabled" class="form-control numbersOnly" id="quantity" name="quantity" type="text" value="<?=set_value('quantity', $evoucher->quantity);?>" placeholder="Input quantity" />
        </div>
        <div class="col-xs-3">
            <?php if($evoucher->evoucher_type == 1):?>
                <?php $voucher = 'Override Amount';?>
            <?php elseif($evoucher->evoucher_type == 2):?>
                <?php $voucher = 'Discount Amount';?>
            <?php elseif($evoucher->evoucher_type == 3):?>
                <?php $voucher = 'Discount Percent';?>
            <?php endif;?>
            <label class="control-label">Override Rate</label>
            <input disabled="disabled" class="form-control" id="evoucher_type" name="evoucher_type" type="text" value="<?=set_value('evoucher_type', $voucher);?>">
        </div>
        <div class="col-xs-3">
            <label class="control-label">Amount / Percent</label>
            (<span id="select_amount">P</span><span id="select_percent">%</span>)
            <input disabled="disabled" class="form-control price" id="evoucher_amount" name="evoucher_amount" type="text" value="<?=set_value('evoucher_amount', $evoucher->evoucher_amount);?>" placeholder="Input unit of amount/percent" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Issuing outlet</label>
            <?php foreach($outlet as $o):?>
            <?php if($o->id_outlet == $evoucher->outlet_id):?>
                <input disabled="disabled" class="form-control" id="outlet_id" name="outlet_id" type="text" value="<?=set_value('outlet_id', $o->outlet);?>">
            <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Validity</label>
            <input disabled="disabled" class="form-control numbersOnly" id="validity" name="validity" type="text" value="<?=set_value('validity', $evoucher->validity);?>" placeholder="Input validity" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Allocated E-voucher</label>
            <input disabled="disabled" class="form-control numbersOnly" id="alloted_evoucher" name="alloted_evoucher" type="text" value="<?=set_value('alloted_evoucher', $evoucher->alloted_evoucher);?>" />
        </div>
        <div class="col-xs-3">
            <label class="control-label">Generated E-voucher</label>
            <input disabled="disabled" class="form-control numbersOnly" id="generated_evoucher" name="generated_evoucher" type="text" value="<?=set_value('generated_evoucher', $evoucher->generated_evoucher);?>" />
        </div>
        <div class="col-xs-3">
            <label class="control-label">Canceled E-voucher</label>
            <input disabled="disabled" class="form-control numbersOnly" id="cancelled_evoucher" name="cancelled_evoucher" type="text" value="<?=set_value('cancelled_evoucher', $evoucher->cancelled_evoucher);?>" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Generate Series</label>
            <input class="form-control numbersOnly" id="gen_quantity" name="gen_quantity" type="text" value="<?=set_value('gen_quantity');?>" maxlength="4" />
            <span class="input-notes-bottom"><?php echo form_error('gen_quantity'); ?></span>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
                <button type="submit" id="submit_btn" name="submit" value="submit" class="btn oj-button">Generate</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var set_amount = '<?=$set_amount;?>';
</script>
<?=$footer;?>