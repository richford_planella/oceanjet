<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" id="evoucher_form" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">E-voucher Type</label>
            <input disabled="disabled" class="form-control" id="evoucher" name="evoucher" type="text" value="<?=set_value('evoucher', $evoucher->evoucher);?>" placeholder="Input e-voucher type" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Origin</label>
            <?php foreach($port as $p):?>
            <?php if($p->id_port == $evoucher->origin_id):?>
                <input disabled="disabled" class="form-control" id="origin_id" name="origin_id" type="text" value="<?=set_value('origin_id', $p->port);?>">
            <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Destination</label>
            <?php foreach($port as $p):?>
            <?php if($p->id_port == $evoucher->destination_id):?>
                <input disabled="disabled" class="form-control" id="destination_id" name="destination_id" type="text" value="<?=set_value('destination_id', $p->port);?>">
            <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Accommodation</label>
            <?php foreach($accommodation as $a):?>
                <?php if($a->id_accommodation == $evoucher->accommodation_id):?>
                <input disabled="disabled" class="form-control" id="accommodation_id" name="accommodation_id" type="text" value="<?=set_value('accommodation_id', $a->accommodation);?>">
                <?php endif;?>
            <?php endforeach;?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Quantity</label>
            <input disabled="disabled" class="form-control numbersOnly" id="quantity" name="quantity" type="text" value="<?=set_value('quantity', $evoucher->quantity);?>" placeholder="Input quantity" />
        </div>
        <div class="col-xs-3">
            <?php if($evoucher->evoucher_type == 1):?>
                <?php $voucher = 'Override Amount';?>
            <?php elseif($evoucher->evoucher_type == 2):?>
                <?php $voucher = 'Discount Amount';?>
            <?php elseif($evoucher->evoucher_type == 3):?>
                <?php $voucher = 'Discount Percent';?>
            <?php endif;?>
            <label class="control-label">Override Rate</label>
            <input disabled="disabled" class="form-control" id="evoucher_type" name="evoucher_type" type="text" value="<?=set_value('evoucher_type', $voucher);?>">
        </div>
        <div class="col-xs-3">
            <label class="control-label">Amount / Percent</label>
            (<span id="select_amount">P</span><span id="select_percent">%</span>)
            <input disabled="disabled" class="form-control price" id="evoucher_amount" name="evoucher_amount" type="text" value="<?=set_value('evoucher_amount', $evoucher->evoucher_amount);?>" placeholder="Input unit of amount/percent" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Issuing outlet</label>
            <?php foreach($outlet as $o):?>
            <?php if($o->id_outlet == $evoucher->outlet_id):?>
                <input disabled="disabled" class="form-control" id="outlet_id" name="outlet_id" type="text" value="<?=set_value('outlet_id', $o->outlet);?>">
            <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Validity</label>
            <input disabled="disabled" class="form-control numbersOnly" id="validity" name="validity" type="text" value="<?=set_value('validity', $evoucher->validity);?>" placeholder="Input validity" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Allocated E-voucher</label>
            <input disabled="disabled" class="form-control numbersOnly" id="alloted_evoucher" name="alloted_evoucher" type="text" value="<?=set_value('quanalloted_evouchertity', $evoucher->alloted_evoucher);?>" />
        </div>
        <div class="col-xs-3">
            <label class="control-label">Generated E-voucher</label>
            <input disabled="disabled" class="form-control numbersOnly" id="generated_evoucher" name="generated_evoucher" type="text" value="<?=set_value('generated_evoucher', $evoucher->generated_evoucher);?>" />
        </div>
        <div class="col-xs-3">
            <label class="control-label">Canceled E-voucher</label>
            <input disabled="disabled" class="form-control numbersOnly" id="cancelled_evoucher" name="cancelled_evoucher" type="text" value="<?=set_value('cancelled_evoucher', $evoucher->cancelled_evoucher);?>" />
        </div>
    </div>
    <a href="<?=admin_url($this->classname,'generate',$evoucher->id_evoucher);?>" class="btn oj-button oj-lg-button margin-top-50">Generate Voucher</a>
    <!-- new code -->
    <h5 class="oj-form__title oj-form__title--margin-10-btm">Generated E-voucher List</h5>
    <div class="form-group">
        <div class="col-xs-8">
            <span>Batch Action</span>
            <button class="btn oj-button oj-button--margin-adj batch-btn cancel-all"  type="submit" disabled>Cancel</button>
        </div>
        <div class="col-xs-4 text-right">
            <div class="input-group">
                <input type="text" class="form-control" id="column4_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-12">
            <table id="table-evoucher" class="display select table global-table nopadding">
                <thead>
                    <tr>
                        <th>
                            <input name="select_all" value="1" type="checkbox">
                        </th>
                        <th>E-voucher Code</th>
                        <th>E-voucher Type</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Accommodation Type</th>
                        <th>Override Rate</th>
                        <th>Amount/Percent</th>
                        <th>Status</th>
                        <th>Issuing Outlet</th>
                        <th>Validity</th>
                        <th>Date Generated</th>
                        <th>Date Used</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($evoucher_series)):?>
                    <?php foreach($evoucher_series as $v):?>
                    <tr>
                        <td>
                            <?php if($v->estatus == 'Unused'):?>
                                <input type="checkbox" class="checkbox1" name="check[<?=$v->id_evoucher_series;?>]" id="item_<?=$v->id_evoucher_series;?>" value="<?=$v->id_evoucher_series;?>">
<!--                                <input class="checkbox1" type="checkbox" name="check[<?=$v->id_evoucher_series;?>]" id="item_<?=$v->id_evoucher_series;?>" value="<?=$v->id_evoucher_series;?>">
                                <input class="myTextBox" type="hidden" name="myCheckBox[<?=$v->id_evoucher_series;?>]" id="mycheckbox_<?=$v->id_evoucher_series;?>" value="0">-->
                            <?php endif;?>
                        </td>
                        <td><?=$v->evoucher_code;?></td>
                        <td><?=$v->evoucher;?></td>
                        <td><?=$v->orig_port;?></td>
                        <td><?=$v->des_port;?></td>
                        <td><?=$v->accommodation;?></td>
                        <td>
                            <?php if($v->evoucher_type == 1):?>
                                <?='Override Amount';?>
                            <?php elseif($v->evoucher_type == 2):?>
                                <?='Discount Amount';?>
                            <?php elseif($v->evoucher_type == 3):?>
                                <?='Discount Percent';?>
                            <?php endif;?>
                        </td>
                        <td><?=($v->evoucher_type == 1 OR $v->evoucher_type == 2)? "P ".number_format($v->evoucher_amount,2,'.',',') : $v->evoucher_amount." %";?></td>
                        <td>
                                <?=$v->estatus;?>
                        </td>
                        <td><?=$v->outlet;?></td>
                        <td><?=date("Y-m-d",strtotime($v->expiration_date));?></td>
                        <td><?=date("Y-m-d",strtotime($v->date_generated));?></td>
                        <td><?=($v->date_used <> '0000-00-00') ? date("Y-m-d",strtotime($v->date_used)) : "";?></td>
                        <td>
                            <?php if($v->estatus == 'Unused'):?>
                                <a class="cancel-single" href="<?=admin_url($this->classname, 'cancel', $evoucher->id_evoucher, $v->id_evoucher_series);?>" id="cancel_<?=$v->id_evoucher_series;?>">
                                       Cancel
                                </a>
                            <?php endif;?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <?php endif;?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
                <a href="<?=admin_url($this->classname,'edit',$evoucher->id_evoucher);?>" class="btn oj-button oj-button">Edit</a>
            </div>
        </div>
    </div>
    <!-- end new code -->
    
</form>
<script type="text/javascript">
    var set_amount = '<?=$set_amount;?>';
</script>
<?=$footer;?>