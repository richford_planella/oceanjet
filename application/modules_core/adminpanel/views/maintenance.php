<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="restricted-page">
                <div class="restricted-page__body">
                    <h2><i class="fa fa-exclamation-triangle"></i> Oops... This page is under construction please return to <br/>
                        <a href="<?=admin_url();?>" class="btn oj-button oj-lg-button">Dashboard</a></h2>
                </div>
            </div>
        </div>
    </div>
</form>
<?=$footer;?>