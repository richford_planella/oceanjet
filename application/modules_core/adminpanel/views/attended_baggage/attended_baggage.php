<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
			<tr>
				<th>Attended Baggage Code</th>
				<th>Description</th>
				<th>Origin</th>
				<th>Destination</th>
				<th>Amount</th>
				<th>Unit of Measurement</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
        <tbody>
            <?php if(!empty($attended_baggage)):?>
			<?php foreach($attended_baggage as $u):?>
			<tr>
				<td><?=$u->attended_baggage?></td>
				<td><?=$u->description?></td>
				<td><?=$u->origin?></td>
				<td><?=$u->destination?></td>
				<td><?=$u->amount?></td>
				<td><?=$u->unit_of_measurement?></td>
				<td><?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
				<td>
					<a href="<?=admin_url($this->classname, 'view', $u->id_attended_baggage);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $u->id_attended_baggage);?>">Edit</a>
                    <a href="<?=admin_url($this->classname, 'delete', $u->id_attended_baggage);?>" class="btn-delete-attended-baggage-rate">Delete</a>
                    <?php if($u->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_attended_baggage, md5($token.$this->classname.$u->id_attended_baggage));?>"  class="btn-deactivate-attended-baggage-rate">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_attended_baggage, md5($token.$this->classname.$u->id_attended_baggage));?>"  class="btn-activate-attended-baggage-rate">
                                Re-activate
                        </a>
                    <?php endif;?>
				</td>
			</tr>   
			<?php endforeach;?>
			<?php endif;?>
        </tbody>
    </table>
<?=$footer;?>