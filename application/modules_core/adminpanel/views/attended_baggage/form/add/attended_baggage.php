<?=$header;?>
<form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Attended Baggage Rate Code</label>
      <input class="form-control textToUpper" type="text" placeholder="Input attended baggage rate code" name="attended_baggage" value="<?=set_value('attended_baggage');?>"/>
      <span class="input-notes-bottom"><?=form_error('attended_baggage')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Origin</label>
      <select class="selectpicker form-control" name="origin_id">
        <option value="">Please Select</option>
      <?php foreach ($port_list as $key => $value) : ?>
            <option value="<?=$value->id_port ?>" <?=set_select("origin_id", $value->id_port);?>><?=$value->port_code?></option>
      <?php endforeach; ?>
      </select>
      <span class="input-notes-bottom"><?=form_error('origin_id')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Destination</label>
      <select class="selectpicker form-control" name="destination_id">
        <option value="">Please Select</option>
      <?php foreach ($port_list as $key => $value) : ?>
            <option value="<?=$value->id_port ?>" <?=set_select("destination_id", $value->id_port);?>><?=$value->port_code?></option>
      <?php endforeach; ?>
      </select>
      <span class="input-notes-bottom"><?=form_error('destination_id')?></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-6">
      <label class="control-label">Description</label>
      <input class="form-control" type="text" placeholder="Input description" name="description" value="<?=set_value('description');?>"/>
      <span class="input-notes-bottom"><?=form_error('description')?></span>
    </div>
    <div class="col-xs-2">
      <label class="control-label">Amount</label>
      <input class="form-control price" type="number" placeholder="XX.XX" name="amount" value="<?=set_value('amount');?>" onkeypress="numeric_validation(event)" step=".01"/>
      <span class="input-notes-bottom"><?=form_error('amount')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Unit of Measurement</label>
      <input class="form-control" type="text" placeholder="Input unit of measurement" name="unit_of_measurement" value="<?=set_value('unit_of_measurement');?>"/>
      <span class="input-notes-bottom"><?=form_error('unit_of_measurement')?></span>
    </div>

  </div>
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Status</label>
        <select class="selectpicker form-control" name="enabled">
          <option value="1" <?=set_select('enabled', '1');?>>Active</option>
          <option value="0" <?=set_select('enabled', '0');?>>Inactive</option>
        </select>
        <span class="input-notes-bottom"><?=form_error('enabled')?></span>
    </div>
  </div>





  <div class="form-group oj-form-footer">
    <div class="col-xs-12">
      <div class="btn-oj-group right">
        <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
        <button type="submit" class="btn oj-button" id="btn-submit">Save</button>
      </div>
    </div>
  </div>


</form>
<?=$footer;?>