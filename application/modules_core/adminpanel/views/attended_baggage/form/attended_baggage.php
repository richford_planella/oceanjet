<?=$header;?>
<form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Attended Baggage Rate Code</label>
      <input class="form-control" type="text" placeholder="Input Code" name="attended_baggage" value="<?=set_value('attended_baggage', $attended_baggage->attended_baggage);?>" disabled/>
      <span class="input-notes-bottom"><?=form_error('attended_baggage')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Origin</label>
      <select class="selectpicker form-control" title="Choose" name="origin_id" disabled>
      <?php foreach ($port_list as $k => $v): ?>
		<?php if($attended_baggage->origin_id == $v->id_port): ?>
			<option value="<?=$v->id_port ?>" selected><?=$v->port_code ?></option>
		<?php else: ?>
			<option value="<?=$v->id_port ?>" <?=set_select('origin_id', $v->id_port);?>><?=$v->port_code ?></option>
		<?php endif; ?>
	  <?php endforeach; ?>
      </select>
      <span class="input-notes-bottom"><?=form_error('origin_id')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Destination</label>
      <select class="selectpicker form-control" title="Choose" name="destination_id" disabled>
      <?php foreach ($port_list as $k => $v): ?>
		<?php if($attended_baggage->destination_id == $v->id_port): ?>
			<option value="<?=$v->id_port ?>" selected><?=$v->port_code ?></option>
		<?php else: ?>
			<option value="<?=$v->id_port ?>" <?=set_select('destination_id', $v->id_port);?>><?=$v->port_code ?></option>
		<?php endif; ?>
	  <?php endforeach; ?>
      </select>
      <span class="input-notes-bottom"><?=form_error('destination_id')?></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-6">
      <label class="control-label">Description</label>
      <input class="form-control" type="text" placeholder="Input Code" name="description" value="<?=set_value('description', $attended_baggage->description);?>" disabled/>
      <span class="input-notes-bottom"><?=form_error('description')?></span>
    </div>
    <div class="col-xs-2">
      <label class="control-label">Amount</label>
      <input class="form-control" type="text" placeholder="XX.XX" name="amount" value="<?=set_value('amount', $attended_baggage->amount);?>" disabled/>
      <span class="input-notes-bottom"><?=form_error('amount')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Unit of Measurement</label>
      <input class="form-control" type="text" placeholder="Input details" name="unit_of_measurement" value="<?=set_value('unit_of_measurement', $attended_baggage->unit_of_measurement);?>" disabled/>
      <span class="input-notes-bottom"><?=form_error('unit_of_measurement')?></span>
    </div>

  </div>
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Status</label>
        <select class="selectpicker form-control" title="Choose" name="enabled" disabled>
          <option value="1" <?=set_select('enabled', '1',(($attended_baggage->enabled) ? TRUE : ''));?>>Active</option>
          <option value="0" <?=set_select('enabled', '0',((!$attended_baggage->enabled) ? TRUE : ''));?>>Inactive</option>
        </select>
        <span class="input-notes-bottom"><?=form_error('enabled')?></span>
    </div>
  </div>

  <div class="form-group oj-form-footer">
    <div class="col-xs-12">
      <div class="btn-oj-group right">
        <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Back</a>
        <a class="btn oj-button" href="<?=admin_url($this->classname, 'edit', $attended_baggage->id_attended_baggage);?>">Edit</a>
      </div>
    </div>
  </div>
<?=$footer;?>