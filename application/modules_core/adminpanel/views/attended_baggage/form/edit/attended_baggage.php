<?=$header;?>
<form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Attended Baggage Rate Code</label>
      <input class="form-control textToUpper" type="text" placeholder="Input attended baggage rate code" name="attended_baggage" value="<?=set_value('attended_baggage', $attended_baggage->attended_baggage);?>" readonly/>
      <span class="input-notes-bottom"><?=form_error('attended_baggage')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Origin</label>
      <select class="selectpicker form-control" title="Choose" name="origin_id">
      <?php foreach ($port_list as $k => $v): ?>
		<?php if($attended_baggage->origin_id == $v->id_port): ?>
			<option value="<?=$v->id_port ?>" selected><?=$v->port_code ?></option>
		<?php else: ?>
			<option value="<?=$v->id_port ?>" <?=set_select('origin_id', $v->id_port);?>><?=$v->port_code ?></option>
		<?php endif; ?>
	  <?php endforeach; ?>
      </select>
      <span class="input-notes-bottom"><?=form_error('origin_id')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Destination</label>
      <select class="selectpicker form-control" title="Choose" name="destination_id">
      <?php foreach ($port_list as $k => $v): ?>
		<?php if($attended_baggage->destination_id == $v->id_port): ?>
			<option value="<?=$v->id_port ?>" selected><?=$v->port_code ?></option>
		<?php else: ?>
			<option value="<?=$v->id_port ?>" <?=set_select('destination_id', $v->id_port);?>><?=$v->port_code ?></option>
		<?php endif; ?>
	  <?php endforeach; ?>
      </select>
      <span class="input-notes-bottom"><?=form_error('destination_id')?></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-6">
      <label class="control-label">Description</label>
      <input class="form-control" type="text" placeholder="Input description" name="description" value="<?=set_value('description', $attended_baggage->description);?>"/>
      <span class="input-notes-bottom"><?=form_error('description')?></span>
    </div>
    <div class="col-xs-2">
      <label class="control-label">Amount</label>
      <input class="form-control price" type="number" placeholder="XX.XX" name="amount" value="<?=set_value('amount', $attended_baggage->amount);?>" onkeypress="numeric_validation(event)" step=".01"/>
      <span class="input-notes-bottom"><?=form_error('amount')?></span>
    </div>
    <div class="col-xs-3">
      <label class="control-label">Unit of Measurement</label>
      <input class="form-control" type="text" placeholder="Input unit of measurement" name="unit_of_measurement" value="<?=set_value('unit_of_measurement', $attended_baggage->unit_of_measurement);?>"/>
      <span class="input-notes-bottom"><?=form_error('unit_of_measurement')?></span>
    </div>

  </div>
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Status</label>
        <select class="selectpicker form-control" title="Choose" name="enabled">
          <option value="1" <?=set_select('enabled', '1',(($attended_baggage->enabled) ? TRUE : ''));?>>Active</option>
          <option value="0" <?=set_select('enabled', '0',((!$attended_baggage->enabled) ? TRUE : ''));?>>Inactive</option>
        </select>
        <span class="input-notes-bottom"><?=form_error('enabled')?></span>
    </div>
  </div>

  <div class="form-group oj-form-footer">
    <div class="col-xs-12">
      <div class="btn-oj-group right">
        <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
        <button type="submit" class="btn oj-button" id="btn-submit">Update</button>
      </div>
    </div>
  </div>
<?=$footer;?>