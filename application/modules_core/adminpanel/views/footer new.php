		</div>
	</div>
</section>

	<footer>
		<div class="container-fluid">
			<p>&copy; OceanJet <?=date('Y');?>  | Powered by Yondu</p>
		</div>
	</footer>

	<script src="<?=js_dir('app.js');?>"></script>
	<?php if (isset($js_files) && ! empty($js_files)):?>
		<?php foreach($js_files as $js):?>
			<script type="text/javascript" src="<?=$js;?>"></script>
		<?php endforeach;?>
	<?php endif;?>

</body>
</html>