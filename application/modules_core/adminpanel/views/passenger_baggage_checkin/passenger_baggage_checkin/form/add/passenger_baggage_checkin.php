<?= $header; ?>
<form class="form-horizontal oj-form" action="<?= base_url(); ?>adminpanel/passenger_baggage_checkin/add" method="POST" id="passenger-baggage-check-in-form">
<!--    Voyage Selection-->
    <div class="form-group">
        <div class="col-xs-2">
            <label class="control-label">Origin</label>
            <select id="origin_id" name="origin_id" class="selectpicker form-control">
                <option value="0">Please select</option>
                <?php if($ports):?>
                    <?php foreach($ports as $s):?>
                        <option value="<?=$s->id_port;?>" <?=set_select('origin_id',$s->id_port);?>> <?=$s->port;?> </option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
            <span class="input-notes-bottom"><?=form_error('origin_id');?></span>
        </div>
        <div class="col-xs-2">
            <label class="control-label">Destination</label>
            <select id="destination_id" name="destination_id" class="selectpicker form-control">
                <option value="0">Please select</option>
                <?php if($ports):?>
                    <?php foreach($ports as $s):?>
                        <option value="<?=$s->id_port;?>" <?=set_select('destination_id',$s->id_port);?>> <?=$s->port;?></option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
            <span class="input-notes-bottom"><?=form_error('destination_id');?></span>
        </div>
        <div class="col-xs-7">
            <label class="control-label">Voyage</label>
            <select id="voyage_management_id" name="voyage_management_id" class="selectpicker form-control">
                <option value="">Please select</option>
                <?php if($subvoyage):?>
                    <?php foreach($subvoyage as $value): ?>
                        <option value="<?=$value->id_subvoyage_management;?>" <?=set_select('voyage_management_id',$value->id_subvoyage_management);?>><?=$value->voyage." | ". $value->port_origin."-".$value->port_destination ." | ".  date("F j, Y - D", strtotime($value->departure_date))." | ". date('g:i a',strtotime($value->ETD)) ."-". date('g:i a',strtotime($value->ETA));?></option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
            <span class="input-notes-bottom"><?=form_error('voyage_management_id');?></span>
        </div>
        <div class="col-xs-1">

        </div>
    </div>
<!--    Trip Details-->
<!--    Trip Details-->
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Trip Details</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Voyage Code</label>
                    <input name="voyage_text" class="form-control" value="<?=set_value('voyage_text');?>" id="select_voyages" placeholder="Autofill voyage code" readonly>
                    <span class="input-notes-bottom"><?=form_error('voyage_text');?></span>
                </div>
                <div class="col-xs-3 col-xs-offset-6">
                    <button class="btn oj-button view-baggage-lists" type="button">View Baggage List</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-9">
                    <div class="seat-preview__title">
                        <h4>Voyage Details</h4>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3 has-feedback">
                    <label class="control-label">Departure Date</label>
                    <input class="form-control" id="departure_date" name="departure_date" type="text"
                           value="<?=set_value('departure_date');?>" placeholder="Autofill departure date" readonly/>
                    <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                    <span class="input-notes-bottom"><?=form_error('departure_date');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Origin</label>
                    <input class="form-control" id="origin" name="origin" type="text"
                           value="<?=set_value('origin');?>" placeholder="Autofill origin"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('origin');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Destination</label>
                    <input class="form-control" id="destination" name="destination" type="text"
                           value="<?=set_value('destination');?>" placeholder="Autofill destination"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('destination');?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3 has-feedback">
                    <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                    <input class="form-control" id="etd" name="etd" type="text"
                           value="<?=set_value('etd');?>" placeholder="Autofill ETD"
                           readonly/>
                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                    <span class="input-notes-bottom"><?=form_error('etd');?></span>
                </div>
                <div class="col-xs-3 has-feedback">
                    <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                    <input class="form-control" id="eta" name="eta" type="text"
                           value="<?=set_value('eta');?>" placeholder="Autofill ETA"
                           readonly/>
                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                    <span class="input-notes-bottom"><?=form_error('eta');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Vessel</label>
                    <input class="form-control" id="vessel" name="vessel" type="text"
                           value="<?=set_value('vessel');?>" placeholder="Autofill vessel"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('vessel');?></span>
                </div>
            </div>
        </div>
    </div>
<!--    Trip Details-->
<!--    Voyage Status-->
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Voyage Status</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Status</label>
                    <input class="form-control" id="subvoyage_status_text" name="subvoyage_status_text" type="text"
                           value="<?=set_value('subvoyage_status_text');?>" placeholder="Autofill voyage status"
                           readonly/>
                    <input type="hidden" name="subvoyage_status_id" id="subvoyage_status_id" value="<?=set_value('subvoyage_status_id');?>">
                    <span class="input-notes-bottom"><?=form_error('subvoyage_status_text');?></span>
                </div>
                <div class="col-xs-9">
                    <label class="control-label">Action Button</label>
                    <div class="btn-oj-group">
                        <button class="btn oj-button update-sub-voyage-status" data-status="9" type="button">Open for Check-In</button>
                        <button class="btn oj-button update-sub-voyage-status" data-status="2" type="button">Depart</button>
                        <button class="btn oj-button update-sub-voyage-status" data-status="4" type="button">Undepart</button>
                        <button class="btn oj-button update-sub-voyage-status" data-status="3" type="button">Delayed</button>
                        <button class="btn oj-button update-sub-voyage-status" data-status="6" type="button">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    Voyage Status-->
<!--    Baggage Details-->
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Baggage Details</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Reference Number</label>
                    <input class="input-xlarge focused" id="transaction_ref" name="transaction_ref" type="hidden"
                           value="<?=set_value('transaction_ref');?>"
                           readonly/>
                    <input class="form-control" id="reference_no" name="reference_no" type="text"
                           value="<?=set_value('reference_no');?>" placeholder="Autofill reference no." onkeypress="numeric_validation(event)"
                    />
                    <span class="input-notes-bottom"><?=form_error('reference_no');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Ticket Number</label>
                    <input class="form-control" id="ticket_no" name="ticket_no" type="text"
                           value="<?=set_value('ticket_no');?>" placeholder="Input Ticket no." onkeypress="numeric_validation(event)"
                    />
                    <div id="ticket_no_validation_div"></div>
                    <span class="input-notes-bottom"><?=form_error('ticket_no');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Passenger Name</label>
                    <input type="hidden" name="passenger_id" id="passenger_id" value="<?=set_value('passenger_id');?>">
                    <input type="hidden" name="booking_status_id" id="booking_status_id" value="<?=set_value('booking_status_id');?>">
                    <input class="form-control" id="passenger_name" name="passenger_name" type="text"
                           value="<?=set_value('passenger_name');?>" placeholder="Autofill passenger name"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('passenger_name');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Baggage Rate</label>
                    <select id="id_attended_baggage" name="id_attended_baggage" class="selectpicker form-control">
                        <option value="">Please select</option>
                        <?php if($baggage_rates):?>
                            <?php foreach($baggage_rates as $s):?>
                                <option value="<?=$s->id_attended_baggage;?>" <?=set_select('id_attended_baggage', $s->id_attended_baggage);?>><?="[". $s->attended_baggage . "] " . $s->description."]";?></option>
                            <?php endforeach;?>
                        <?php endif;?>
                    </select>
                    <span class="input-notes-bottom"><?=form_error('id_attended_baggage');?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Quantity (No. of bags)</label>
                    <input class="form-control" id="quantity" name="quantity"
                           onkeypress='numeric_validation(event)'
                           value="<?= set_value('quantity'); ?>" placeholder="Input quantity" type="number"/>
                    <span class="input-notes-bottom"><?=form_error('quantity');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Weight</label>
                    <input class="form-control" id="weight" name="weight"
                           onkeypress='numeric_validation(event)'
                           value="<?= set_value('weight'); ?>" placeholder="Input weight" type="number"/>
                    <span class="input-notes-bottom"><?=form_error('weight');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Unit Of Measurement (UOM)</label>
                    <input class="form-control" id="uom" name="uom" type="text"
                           value="<?= set_value('uom'); ?>" placeholder="Autofill UOM" readonly/>
                    <span class="input-notes-bottom"><?=form_error('uom');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Free Baggage</label>
                    <input class="form-control" id="free_baggage" name="free_baggage" type="text"
                           value="<?=set_value('free_baggage');?>" placeholder="Autofill free baggage" readonly/>
                    <span class="input-notes-bottom"><?=form_error('free_baggage');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Attended Baggage</label>
                    <input class="form-control" id="attended_baggage" name="attended_baggage"
                           type="text" placeholder="Autofill attended baggage"
                           value="<?=set_value('attended_baggage');?>" readonly/>
                    <div id="attended_baggage_multiplier">
                        <?php if(set_value('excess_baggage_amount'))
                            echo "<label class=\"control-label\">Attended baggage multiplier: ₱ ".set_value('excess_baggage_amount')."</label>";
                        ?>
                    </div>
                    <span class="input-notes-bottom"><?=form_error('attended_baggage');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Total Amount</label>
                    <input class="input-xlarge focused" id="excess_baggage_amount" name="excess_baggage_amount" value="<?=set_value('excess_baggage_amount')?>"
                           type="hidden"/>
                    <input class="form-control" id="total_amount" name="total_amount"
                             value="<?=set_value('total_amount','0.00');?>" readonly/>
                    <span class="input-notes-bottom"><?=form_error('total_amount');?></span>
                </div>
                <div class="col-xs-2">
                    <button type="button" class="btn oj-button oj-button--margin-top-30 validate-form">Check-In</button>
                </div>
            </div>
        </div>
    </div>
<!--    Baggage Details-->
</form>


<!--Some generic modal coming right up start-->
<div id="confirmation-modal" class="modal bootstrap-dialog type-primary fade size-normal in" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h3 class="modal-title">Passenger Baggage Check-In</h3>
            </div>
            <div class="modal-body">
                <h4 id="myConfirmationModalLabel"></h4>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Dismissk </button>
            </div>
        </div>
    </div>
</div>
<!--Some generic modal coming right up end-->

<!--Modal Delayed Voyage Start-->
<div id="delayed-voyage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h3 class="modal-title" id="myModalLabel">Passenger Baggage Check-In</h3>
            </div>
            <div class="modal-body">
                <h4>Add number of minutes late</h4>
                <form action="">
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label">Number of minutes late</label>
                            <div class="btn-group bootstrap-select form-control">
                                <input class="form-control" name="delayed_time" id="delayed_time" type="text" placeholder="Input number of minutes delayed" onkeypress="numeric_validation(event)">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Cancel </button>
                <button type="button" class="btn oj-button button-tag-voyage-delayed">Save </button>
            </div>

        </div>
    </div>
</div>
<!--Modal Delayed Voyage End-->

<!--Modal Canceled Voyage Start-->
<div id="canceled-voyage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h3 class="modal-title" id="myModalLabel">Passenger Baggage Check-In</h3>
            </div>
            <div class="modal-body">
                <h4>Cancel voyage, input reason for cancelling.</h4>
                <form action="">
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label">Reason for cancelling.</label>
                            <div class="btn-group bootstrap-select form-control">
                                <input class="form-control" type="text" id="reason-for-cancelling" placeholder="Input input reason for cancelling">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Cancel </button>
                <button type="button" class="btn oj-button button-tag-voyage-canceled">Save </button>
            </div>

        </div>
    </div>
</div>
<!--Modal Canceled Voyage End-->

<?= $footer; ?>
<script type="text/javascript">
    $(function () {

        //validate if form can be check-in start
        $(document).on("click", ".validate-form", function () {
            //Check if passenger is already in
            if($('#passenger_id').val()==''){
                $('#myConfirmationModalLabel').html("No passenger to check-in. Please try again."); //Set available users
                return $('#confirmation-modal').modal('show');
            }

            //Check if Baggage already check-in on passenger
            if($('#booking_status_id').val()!='5'){
                $('#myConfirmationModalLabel').html("Passenger is not yet check-in. Please try again."); //Set available users
                return $('#confirmation-modal').modal('show');
            }

            //Check Leg status
            if($('#subvoyage_status_id').val()=='9' || $('#subvoyage_status_id').val()=='10'){
                //Submit Form
                $('#passenger-baggage-check-in-form').submit();
            }else{
                $('#myConfirmationModalLabel').html("Cannot proceed to baggage check-in, set the Voyage Status to 'Open for Check-in'."); //Set available users
                return $('#confirmation-modal').modal('show');
            }

        });
        //validate if form can be check-in end

        //View Baggage Lists start
        $(document).on("click", ".view-baggage-lists", function () {

            //Check if we have selected subvoyage
            var voyage_management_id = $("#voyage_management_id").val();

            if(checkSelectedsubvoyages()!=0)
                window.location.href = adminURL+"passenger_baggage_checkin_payment/view/"+voyage_management_id;

        });
        //View Baggage Lists end

        // Attended Baggage Rate
        $(document).on("change", "#id_attended_baggage", function () {
            // Excess Baggage ID
            var id_attended_baggage = $("#id_attended_baggage").val();
            $.ajax({
                type: 'post',
                async: true,
                dataType: 'json',
                url: adminURL + 'passenger_baggage_checkin/ajax',
                data: "module=excess_baggage&id_attended_baggage=" + id_attended_baggage,
                success: function (data) {
                    $("#uom").val(data.details.unit_of_measurement);
                    $("#excess_baggage_amount").val(data.details.amount);
                    $("#attended_baggage_multiplier").html("<label class='control-label'>Attended baggage multiplier: ₱ " + data.details.amount + "</label>");

                    //iF QUANTITY has value compute total amount
                    var weight = $('#weight').val();
                    if(weight!='')
                        computeTotalAmount();
                }
            });
        }); // Excess Baggage ID

        //set Origin and Destination
        //Return list of sub voyages drop down list
        $(document).on("change", "#origin_id, #destination_id", function () {
            //Get Origin and destination IDs
            var origin_id       = $('#origin_id').val();
            var destination_id = $('#destination_id').val();

            //If same origin and destination add warning message
            if(origin_id==destination_id) {
                $('#myConfirmationModalLabel').html('Invalid origin and destination. Please try again.');
                return $('#confirmation-modal').modal('show');
            }

            //Reset Fields
            resetVoyageDetails();

            //Fetch list of legs
            $.ajax({
                type: 'post',
                async: true,
                dataType: 'json',
                url: adminURL + 'passenger_baggage_checkin/ajax',
                data: "module=subvoyage&origin_id=" + origin_id+'&destination_id='+destination_id,
                success: function (data) {
                    //Print list of legs
                    $("#voyage_management_id").html(data.option);
                    $('#voyage_management_id').selectpicker('refresh');
                }
            });

        });


        //Subvoyage Details start
        $(document).on("change", "#voyage_management_id", function() {
            var voyage_management_id = $("#voyage_management_id").val();
            var origin_id            = $("#origin_id").val();
            var destination_id       = $("#destination_id").val();

            //Reset Fields
            resetVoyageDetails();

            $.ajax({
                type        :   'post',
                async       :   true,
                dataType    :   'json',
                url         :   adminURL + 'passenger_baggage_checkin/ajax',
                data        :   "module=details&voyage_management_id=" + voyage_management_id+"&origin_id="+origin_id+"&destination_id="+destination_id,
                success     :   function(data) {
                    $("#departure_date").val(data.subvoyage.details.departure_date);
                    $("#select_voyages").val(data.subvoyage.details.voyage);
                    $("#origin").val(data.subvoyage.details.port_origin);
                    $("#destination").val(data.subvoyage.details.port_destination);
                    $("#etd").val(data.subvoyage.details.ETD);
                    $("#eta").val(data.subvoyage.details.ETA);
                    $("#vessel").val(data.subvoyage.details.vessel_code);
                    $("#subvoyage_status_text").val(data.subvoyage.details.description);
                    $("#subvoyage_status_id").val(data.subvoyage.details.subvoyage_management_status_id);

                    //Print attended baggage lists
                    //Add List of baggage based on Voyage ID start
                    $("#id_attended_baggage").html(data.attended_baggage_option);
                    $('#id_attended_baggage').selectpicker('refresh');
                    //Add List of baggage based on Voyage ID end
                }
            });
        });
        //Subvoyage Details end

        //Get user details as Agent type
        // Passenger Checkin Rate
        $(document).on("keyup", "#ticket_no", function () {
            // Excess Baggage ID
            var ticket_no = $("#ticket_no").val();

            $.ajax({
                type: 'post',
                async: true,
                dataType: 'json',
                url: adminURL + 'passenger_baggage_checkin/ajax',
                data: "module=passenger_details&ticket_no=" + ticket_no,
                success: function (data) {

                    if (!jQuery.isEmptyObject(data)) {
                        // Add validation for valid ticket number
                        // Voyage code of booking Vs. Voyage code selected
                        if(data.details.subvoyage_management_id!=$('#voyage_management_id').val())
                            return $("#ticket_no_validation_div").html("<span class='input-notes-bottom'>Invalid voyage, cannot proceed to check-in. Please try again.</span>");

                        $("#booking_status_id").val(data.details.booking_status_id);
                        $("#passenger_id").val(data.details.passenger_id);
                        $("#ticket_series_info_id").val(data.details.id_ticket_series_info);
                        $("#passenger_name").val(data.details.passenger_name);
                        $("#rule_type_id").val(data.details.id_rule_type);
                        $("#free_baggage").val(data.details.rule_type_amount);
                        $("#transaction_ref").val(data.details.transaction_ref);
                        $("#ticket_no_validation_div").html('');
                    } else {
                        $("#ticket_series_info_id").val(0);
                        $("#passenger_name").val("No match found");
                        $("#free_baggage").val("0.00");
                        $("#ticket_no_validation_div").html("<span class='input-notes-bottom'>Invalid ticket no. Please try again.</span>");
                    }
                },
            });
        }); // Excess Baggage ID
        //Get user details as Agent type

        //Compute Attended Baggage Start
        $(document).on("keyup", "#weight", function () {
            computeTotalAmount();
        }); // Excess Baggage ID
        //Compute Attended Baggage End

        //Update sub voyage status
        $(document).on("click", ".update-sub-voyage-status", function() {

            checkSelectedsubvoyages();

            var stat = $(this).attr('data-status');
            var msg = '';
            //Switch according based on status
            switch (stat){
                case '9':
                    //Open for check-in
                    msg = 'Voyage is now open for check-in.';
                    updateSubVoyageStatus(stat,'','',msg);
                    break;
                case '2':
                    //Departed on time
                    msg = 'Voyage is departed on time.';
                    updateSubVoyageStatus(stat,'','',msg);
                    break;
                case '3':
                    //Departed but delayed
                    $('#delayed-voyage').modal('show');
                    break;
                case '4':
                    //Undepart
                    //Check if current Voyage is undeparted
                    checkLegDeparted();

                    msg = 'Voyage is undeparted.';
                    if($('#subvoyage_status_id').val()=="2" || $('#subvoyage_status_id').val()=="3")
                        updateSubVoyageStatus(stat,'','',msg);
                    break;
                case '6':
                    //Canceled
                    $('#canceled-voyage').modal('show');
                    break;
                case '10':
                    //Open for check-in
                    msg = 'Voyage is now open for chance passenger.';
                    updateSubVoyageStatus(stat,'','',msg);
                    break;
            }
        });
        //Update sub voyage status

        //Tag voyage as delayed
        $(document).on("click", ".button-tag-voyage-delayed", function() {
            var stat = 3;
            var delayed_time = $('#delayed_time').val();
            var msg = "Voyage tagged as delayed."
            //Update status
            updateSubVoyageStatus(stat,delayed_time,'',msg);
            //Close Modal
            $('#delayed-voyage').modal('hide');
        });
        //Tag voyage as delayed

        //Tag voyage as canceled
        $(document).on("click", ".button-tag-voyage-canceled", function() {
            var stat = 6;
            var reason = $('#reason-for-cancelling').val();
            var msg = "Voyage is canceled."
            //Update status
            updateSubVoyageStatus(stat,'',reason,msg);
            //Close Modal
            $('#canceled-voyage').modal('hide');
        });
        //Tag voyage as canceled
    });

    //Check if we have selected voyages
    //check if we have voyage selected
    function checkSelectedsubvoyages(){
        if($('#voyage_management_id').val()==''){
            $('#myConfirmationModalLabel').html("No voyage selected. Please try again."); //Set available users
            $('#confirmation-modal').modal('show');

            throw new Error("No voyage selected. Please try again.");
        }
    }


    //check if we Leg is Undeparted
    //Departed on time
    //Departed but late
    function checkLegDeparted(){
        if($('#subvoyage_status_id').val()!="2" || $('#subvoyage_status_id').val()!="3") {
            $('#myConfirmationModalLabel').html('Cannot set voyage to undepart, make sure that voyage is departed. Please try again.');
            $('#confirmation-modal').modal('show');

            throw new Error("No voyage selected. Please try again.");
        }
    }
    //check if we Leg is Undeparted


    //Sub Voyage Status Update Start
    function updateSubVoyageStatus(status, delayed_time, canceled_reason,msg) {
        var voyage_management_id = $('#voyage_management_id').val();

        $.ajax({
            type        :   'post',
            async       :   true,
            dateType    :   'json',
            url         :   adminURL + "passenger_baggage_checkin/ajax",
            data        :   'module=update_subvoyage_status&status='+status+"&voyage_management_id="+voyage_management_id+"&delayed_time="+delayed_time+"&canceled_reason="+canceled_reason,
            success     :   function(data) {
                var data = JSON.parse(data);
                //Update Leg Status after the update
                $("#subvoyage_status_text").val(data.subvoyage.details.description);
                $("#subvoyage_status_id").val(data.subvoyage.details.subvoyage_management_status_id);

                //Open Modal
                $('#myConfirmationModalLabel').html(msg);
                $('#confirmation-modal').modal('show');
            }
        });
    }
    //Sub Voyage Status Update End

    // Number input only
    function numeric_validation(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[\u00080-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }

    //Compute Total Amount based on weight
    function computeTotalAmount()
    {
        // Excess Baggage ID
        var weight = $("#weight").val();
        var free_baggage = $("#free_baggage").val();
        var attended_baggage = weight - free_baggage;
        var excess_baggage_amount = $("#excess_baggage_amount").val();

        if (attended_baggage > 0)
            $("#attended_baggage").val(attended_baggage);  //Set Attended Baggage
        else
            $("#attended_baggage").val(0);  //Set Attended Baggage

        var total_amount = parseFloat(attended_baggage * excess_baggage_amount)
        if (total_amount > 0) {
            $("#total_amount").val((total_amount).toFixed(2));
            $("#total_amount_preview").val(String.fromCharCode(8369) + " " + (total_amount).toFixed(2)); //Add peso sign
        } else {
            $("#total_amount").val(0.00);
        }
    }


    //Set fields to be blank reset Voyage Fields
    function resetVoyageDetails()
    {
        $("#departure_date").val('');
        $("#select_voyages").val('');
        $("#origin").val('');
        $("#destination").val('');
        $("#etd").val('');
        $("#eta").val('');
        $("#vessel").val('');
        $("#subvoyage_status_text").val('');
        $("#subvoyage_status_id").val('');

        //Print attended baggage lists
        //Add List of baggage based on Voyage ID start
        $("#id_attended_baggage").html("<option value='0'>Please select</option>");
    }


</script>