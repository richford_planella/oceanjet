<?= $header; ?>
<style>@media print {
        .noprint {
            display: none;
        }

        .pagebreak {
            page-break-before: always;
        }
    }</style>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-content">
            <?= $this->load->view(admin_dir('notification')); ?>
            <table class="table table-striped table-bordered bootstrap-datatable datatable1">
                <?php if (!empty($subvoyage_details)): ?>
                    <?php foreach ($subvoyage_details as $vd): ?>
                        <tr>
                            <td>Departure Date</td>
                            <td><?= date("F j, Y - D", strtotime($vd->departure_date)); ?></td>
                        </tr>
                        <tr>
                            <td>Sub Voyage</td>
                            <td><?= $vd->voyage; ?></td>
                        </tr>
                        <tr>
                            <td>Origin</td>
                            <td><?= $vd->port_origin; ?></td>
                        </tr>
                        <tr>
                            <td>Destination</td>
                            <td><?= $vd->port_destination; ?></td>
                        </tr>
                        <tr>
                            <td>ETD</td>
                            <td><?= date('g:i a',strtotime($vd->ETD)) ?></td>
                        </tr>
                        <tr>
                            <td>ETA</td>
                            <td><?= date('g:i a',strtotime($vd->ETA))  ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="2">No Voyage Details, please select voyage.</td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    </div><!--/span-->
</div><!--/row-->

<div class="row table-header-custom nopadding">
    <div class="col-xs-4">
        <div class="input-group">
            <input type="text" placeholder="Search" id="column3_search" class="form-control">
        </div>
    </div>
    <div class="col-xs-8 text-right">
    </div>
</div>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-content">
            <table class="table global-table nopadding" id="main_table">
                <thead>
                <tr>
                    <th>Reference No.</th>
                    <th>Ticket No.</th>
                    <th>Passenger</th>
                    <th>Total Amount</th>
                    <th>Paid Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($baggages as $rvalue):
                    $button = "<input class='btn custom-btn-test print-baggage-claim'
                                                       data-id-passenger-baggage-checkin='".$rvalue->id_passenger_baggage_checkin."'
                                                       data-subvoyage-management-id='".$rvalue->subvoyage_management_id."'
                                                       type='button' value='Re-print baggage receipt'>";
                    echo "<tr>
                              <td>". $rvalue->reference_no ." </td>
                              <td>". $rvalue->ticket_no ." </td>
                              <td>". $rvalue->lastname  .", ".$rvalue->firstname ." ".$rvalue->middlename." </td>
                              <td>". $rvalue->total_amount." </td>
                              <td>". (($rvalue->paid_status) == 1 ? 'Paid' : 'Not yet paid' )." </td>
                              <td>
                              ". (($rvalue->paid_status) == 1 ? $button  : '-' ) ."
                              </td>
                            </tr>";
                     endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div><!--/span-->
</div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<div class="modal hide fade" id="myModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Settings</h3>
    </div>
    <div class="modal-body">
        <p>Here settings can be configured...</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
    </div>
</div>

<?= $footer; ?>
<script type="text/javascript">
    $(function () {
        //View
        $(document).on("click", ".print-baggage-claim", function () {

            //Check if we have selected subvoyage
            var subvoyage_management_id = $(this).attr('data-subvoyage-management-id');
            var id_passenger_baggage_checkin = $(this).attr('data-id-passenger-baggage-checkin');

            link = adminURL+"passenger_baggage_checkin/printBaggageReceipt/"+id_passenger_baggage_checkin+"/"+subvoyage_management_id;

            window.open(link, 'baggage_claim_tag');

        });
        //View Baggage Lists end
    });

</script>