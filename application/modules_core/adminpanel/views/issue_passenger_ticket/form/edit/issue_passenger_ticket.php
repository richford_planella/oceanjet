<?=$header;?>
	<form class="form-horizontal" action="<?=current_url().'/edit/1';?>" method="POST">
			<div class="row-fluid ">
				<div class="box span12">
					<!--container dropdown start-->
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>Trip Details</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					
					<div class="box-content">
						<div id="trip_details">
							<div class="control-group">
								<label class="control-label" for="ticket_no">Ticket Number</label>
								<div class="controls">
								<?php
									echo "<input type='text' value='T123adw' disabled />";
								?>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="departure_date">Departure Date</label>
								<div class="controls">
									<input type="date" class="form-control" placeholder="Departure Date" name="departure_date" id="departure_date" required autocomplete="off" />
								</div>
							</div>
							<div class="control-group leg-group">
								<label class="control-label" for="voyage">Voyage</label>
								<div class="controls">
									<!--
									<select id="voyage" name="voyage" data-rel="chosen">
										 <option value="">Please Select</option>
										 <?php
											foreach ($voyage_options as $key => $value) {
												echo "<option value=". $value->id_voyage .">". $value->voyage ."</option>";
											}
										 ?>
									</select>
									-->
									<div class="input-group">
										<input type="text" class="form-control voyage" placeholder="No voyage selected" disabled />
										<span class="input-group-btn">
											<button class="btn btn-default add-voyage" type="button">Add</button>
										</span>
									</div>
									<span>
										<label class="" for="origin">Origin</label>
										<input input="text" class="" placeholder="Departure Date" name="origin" id="origin" disabled />
									</span>
									<span>
										<label class="" for="destination">Destination</label>
										<input input="text" class="" placeholder="Departure Date" name="destination" id="destination" disabled />
									</span>
									<span>
										<label class="" for="etd">ETD</label>
										<input input="text" class="" placeholder="Departure Date" name="etd" id="etd" disabled />
									</span>
									<span>
										<label class="" for="eta">ETA</label>
										<input input="text" class="" placeholder="Departure Date" name="eta" id="eta" disabled />
									</span>
									<span>
										<label class="" for="vessel">Vessel</label>
										<input input="text" class="" placeholder="Departure Date" name="vessel" id="vessel" disabled />
									</span>
									<button class="btn btn-default btn-add-leg" type="button">+ Add New Leg</button>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="accommodation">Accommodation Type</label>
								<div class="controls">
									<select id="accommodation" name="accommodation" data-rel="chosen">
										 <option value="">Please Select</option>
										 <?php
											foreach ($accommodation_options as $key => $value) {
												echo "<option value=". $value->id_accommodation .">". $value->accommodation ."</option>";
											}
										 ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<!--container dropdown end-->
					
					<!--container dropdown start-->
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>Passenger's Information</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					
					<div class="box-content">
						<div id="passenger_info">
							<div class="control-group">
								<label class="control-label" for="firstname">First Name</label>
								<div class="controls">
									<input class="input-xlarge number focused firstname filter_val" id="firstname" name="firstname" type="text" value="<?=set_value('firstname', $passenger->firstname);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="lastname">Last Name</label>
								<div class="controls">
									<input class="input-xlarge number focused lastname filter_val" id="lastname" name="lastname" type="text" value="<?=set_value('lastname', $passenger->lastname);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="birthday">Birthday</label>
								<div class="controls">
									<input type="date" class="form-control" placeholder="Birthday" name="birthday" id="birthday" required autocomplete="off" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="age">Age</label>
								<div class="controls">
									<input class="input-xlarge number focused age filter_val" id="age" name="age" type="text" value="<?=set_value('age', $passenger->age);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="gender">Gender</label>
								<div class="controls">
									<select id="gender" name="gender" data-rel="chosen">
										<option value="">Please Select</option>
										<option value="male">Male</option>
										<option value="female">Female</option>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="contact_no">Contact Number</label>
								<div class="controls">
									<input class="input-xlarge number focused contact_no filter_val" id="contact_no" name="contact_no" type="text" value="<?=set_value('contact_no', $passenger->contactno);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="id_no">ID Number</label>
								<div class="controls">
									<input class="input-xlarge number focused id_no filter_val" id="id_no" name="id_no" type="text" value="<?=set_value('id_no', $passenger->id_no);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="nationality">Nationality</label>
								<div class="controls">
									<input class="input-xlarge number focused nationality filter_val" id="nationality" name="nationality" type="text" value="<?=set_value('nationality', $passenger->nationality);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="with_infant">With Infant</label>
								<div class="controls">
									<input type="checkbox" value="1" id="with_infant" name="with_infant" class="input-xlarge number focused with_infant filter_val" />
								</div>
							</div>
							<div class="control-group infant_name_container hide">
								<label class="control-label" for="infant_name">Infant's Name</label>
								<div class="controls">
									<input class="input-xlarge number focused infant_name filter_val" id="infant_name" name="infant_name" type="text" value="<?=set_value('infant_name', $passenger->infant_name);?>">
								</div>
							</div>
						</div>
					</div>
					<!--container dropdown end-->
					
					<!--container dropdown start-->
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>Passenger's Information</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					
					<div class="box-content">
						<div id="fare_details">
							<div class="control-group">
								<label class="control-label" for="passenger_fare_code">Fare Code</label>
								<div class="controls">
									<select id="passenger_fare_code" name="passenger_fare_code" data-rel="chosen">
										<option value="">Please Select</option>
										<?php
											foreach ($passenger_fare_options as $key => $value) {
												echo "<option value=". $value->id_passenger_fare .">". $value->passenger_fare ."</option>";
											}
										?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="fare_per_leg">Fare Per Leg</label>
								<div class="controls fare_per_leg_container">
									<input class="input-xlarge number focused infant_name fare_per_leg" id="fare_per_leg" name="fare_per_leg" type="text" disabled />
								</div>
							</div>
							<!--
*Passenger Fare Code - Dropdown Required, display dropdown list of applicable passenger fares
*Discount Code - Dropdown Required, display dropdown list of applicable discount codes; Allow user to select discount code
*Points - Text Display no. of points that the passenger will earn after check-in; Only registered passengers will be eligible to earn points
*Jetter’s No. - Text Field Required, display user input; Allow user to input Jetter’s No.
*Mode of Payment - Text Display selection of mode of payment; Default is “cash”
*TOTAL - Text Display header of total amount
*Fare - Decimal Display total fare
*Discount - Decimal Display total discount
*Terminal Fee - Decimal Display terminal fee
*Port Charge - Decimal Display port charge
*Total Due - Decimal Display total due
							-->
							<div class="form-actions">
								<button type="submit" class="btn btn-primary" >Save changes</button>
								<a href="<?=admin_url($this->classname);?>" class="btn">Cancel</a>
							</div>	
						</div>
					</div>
					<!--container dropdown end-->
				</div>
			</div><!--/span-->
		
		</div><!--/row-->    
                    
	</form>
	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	

<?=$footer;?>