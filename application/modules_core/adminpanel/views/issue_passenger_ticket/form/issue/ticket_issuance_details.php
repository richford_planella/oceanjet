<div class="form-group form-group__custom">
  <!-- Trip Details -->
  <div class="col-xs-12">
    <div class="inner-header">
      <h3 class="inner-header__title">Trip Details</h3>
    </div>
    <!-- Voyage Management -->
    <div class="form-group">
      <div class="col-xs-3">
        <label class="control-label">Voyage Code</label>
        <input class="form-control" type="text" value="<?=isset($subvoyage_management[0]) ? $subvoyage_management[0]->voyage : NULL ?>" disabled />
      </div>
    </div>
    <?php $c = 1; ?>
    <?php foreach($subvoyage_management as $key => $value): ?>
    <div class="form-group">
      <div class="col-xs-12">
        <div class="seat-preview__title">
          <h4>Leg <?=$c++; ?></h4>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-3 ">
        <label class="control-label">Ticket Number</label>
        <input class="form-control" type="text" value="<?=$value->ticket_number ?>" disabled/>
      </div>
      <div class="col-xs-3 has-feedback">
        <label class="control-label">Depature Date</label>
        <input class="form-control oj-date-value" name="departure_date" type="text" value="<?=$value->departure_date ?>" disabled/>
        <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Origin</label>
        <input class="form-control" type="text" value="<?=$value->origin_port ?>" disabled/>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Destination</label>
        <input class="form-control" type="text" value="<?=$value->destination_port ?>" disabled/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-3 has-feedback">
        <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
        <input class="form-control oj-time-picker" type="text" value="<?=$value->ETD ?>" disabled/>
        <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="col-xs-3 has-feedback">
        <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
        <input class="form-control oj-time-picker" type="text" value="<?=$value->ETA ?>" disabled/>
        <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Vessel</label>
        <input class="form-control" type="text" value="<?=$value->vessel_code ?>" disabled/>
      </div>
    </div>
  <?php endforeach; ?>
    <!-- End of Voyagement Management -->
  </div>
  <!-- End of Trip Details -->

  <div class="col-xs-12">
    <hr class="hr-custom">
  </div>
  <div class="col-xs-12">

    <?php if(isset($return_subvoyage_management[0])): ?>
    <div class="form-group">
      <div class="col-xs-3">
        <label class="control-label">Return Voyage Code</label>
        <input class="form-control" type="text" value="<?=isset($return_subvoyage_management[0]) ? $return_subvoyage_management[0]->voyage : NULL ?>" disabled />
      </div>
    </div>
  <?php endif; ?>
    <?php $d = 1; ?>
    <?php foreach($return_subvoyage_management as $key => $value): ?>
    <div class="form-group">
      <div class="col-xs-12">
        <div class="seat-preview__title">
          <h4>Leg <?=$d++; ?></h4>
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-3 ">
        <label class="control-label">Ticket Number</label>
        <input class="form-control" type="text" value="<?=$value->ticket_number ?>" disabled />
      </div>
      <div class="col-xs-3 has-feedback">
        <label class="control-label">Depature Date</label>
        <input class="form-control oj-date-value" type="text" value="<?=$value->departure_date ?>" disabled />
        <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Origin</label>
        <input class="form-control" type="text" value="<?=$value->origin_port ?>" disabled />
      </div>
      <div class="col-xs-3">
        <label class="control-label">Destination</label>
        <input class="form-control" type="text" value="<?=$value->destination_port ?>" disabled />
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-3 has-feedback">
        <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
        <input class="form-control oj-time-picker" type="text" value="<?=$value->ETD ?>" disabled/>
        <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="col-xs-3 has-feedback">
        <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
        <input class="form-control oj-time-picker" type="text" value="<?=$value->ETA ?>" disabled/>
        <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Vessel</label>
        <input class="form-control" type="text" value="<?=$value->vessel_code ?>" disabled/>
      </div>
    </div>
    <?php endforeach;?>
  </div>
</div>

<div class="form-group form-group__custom">
  <div class="col-xs-12">
    <div class="inner-header">
      <h3 class="inner-header__title">Personal Information</h3>
    </div>
    <div class="form-group">

      <div class="col-xs-4">
        <label class="control-label">First Name</label>
        <input class="form-control" type="text" placeholder="Input first name" name="firstname" id="firstname" />
      </div>
      <div class="col-xs-4">
        <label class="control-label">Last Name</label>
        <input class="form-control" type="text" placeholder="Input last name" name="lastname" id="lastname" />
      </div>
      <div class="col-xs-2">
        <label class="control-label">Middle Intial</label>
        <input class="form-control" type="text" placeholder="Input middle name" name="middlename" id="middlename" />
      </div>

      <div class="col-xs-2">
        <label class="control-label">Gender</label>
        <select class="selectpicker form-control" name="gender" id="gender">
          <option value="Male">Male</option>
          <option value="Female">Female</option>
        </select>
      </div>

    </div>
    <div class="form-group">

      <div class="col-xs-3 has-feedback">
        <label class="control-label">Birth date</label>
        <input class="form-control custom-datepicker" type="text" placeholder="YYYY-MM-DD" name="birthdate" id="birthdate" />
        <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
      </div>
      <div class="col-xs-2">
        <label class="control-label">Age</label>
        <input class="form-control" type="text" value="Age" name="age" id="age" disabled />
      </div>

      <div class="col-xs-3">
        <label class="control-label">Contact Number</label>
        <input class="form-control" type="text" placeholder="Input contact number" name="contactno" id="contactno" />
      </div>
      <div class="col-xs-4">
        <label class="control-label">ID Number</label>
        <input class="form-control" type="text" placeholder="Input ID number" name="id_no" id="id_no" />
      </div>


    </div>
    <div class="form-group f-checked">
      <div class="col-xs-3">
        <label class="control-label">Nationality</label>
        <input class="form-control" type="text" placeholder="Input nationality" name="nationality" id="nationality" />
      </div>

      <div class="col-xs-2">
        <label class="control-label">With Infant</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" class="f-checked--check" name="with_infant" id="with_infanct" value="1" /> Yes
          </label>
        </div>
      </div>

      <div class="col-xs-7">
        <label class="control-label">Infant's Name</label>
        <input class="form-control f-checked--fields" type="text" placeholder="Input infant's name" name="infant_name" id="infant_name" disabled />
      </div>


    </div>
  </div>
</div>

<div class="form-group form-group__custom">
  <div class="col-xs-12">
    <div class="inner-header">
      <h3 class="inner-header__title">Accomodation Details</h3>
    </div>
    <div class="form-group">
      <div class="col-xs-12">
        <table class="table  table-striped table-form">
          <thead>
            <tr>
              <th>Items</th>
              <th>Name</th>
              <th>Name</th>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Total Capacity</th>
              <td>N/A</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row">Tickted PAX</th>
              <td>N/A</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row">Check-In PAX</th>
              <td>N/A</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row">No Show PAX</th>
              <td>N/A</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row">Available Seats</th>
              <td>N/A</td>
              <td></td>
              <td></td>
            </tr>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="form-group form-group__custom">
  <div class="col-xs-12">
    <div class="inner-header">
      <h3 class="inner-header__title">Fare Details</h3>
    </div>

    <div class="form-group">
      <div class="col-xs-3">
        <label class="control-label">Voyage Code</label>
        <input class="form-control" type="text" value="<?=$subvoyage_management[0]->voyage ?>" disabled/>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Passenger Fare Code</label>
        <select class="selectpicker form-control passenger_fare" name="main_passenger_fare" id="main_passenger_fare">
          <option value="">Please Select</option>
          <?php foreach($passenger_fare as $key => $value): ?>
            <option value="<?=$value->id_passenger_fare ?>"><?=$value->passenger_fare ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Points Earning</label>
        <input class="form-control" type="text" value="XX" id="main_points_earning" disabled />
      </div>
      <div class="col-xs-3">
        <label class="control-label">Discount Code</label>
        <select class="selectpicker form-control" name="main_discount" id="main_discount">
           <option value="">Please Select</option>
          <?php foreach($discount as $key => $value): ?>
            <option value="<?=$value->id_discount ?>"><?=$value->discount ?></option>
          <?php endforeach; ?>
        </select>
      </div>

    </div>

    <div class="row">
      <div class="col-xs-3">
        <div class="form-group">
          <div class="col-xs-12">
            <label class="control-label">Fare Per Leg</label>
            <ul class="list-unstyled fare-per-leg" id='subvoyage_fare'>
              <li>
                <span>Leg 01:</span>
                <span>P. XXX.XX</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-xs-9">
        <div class="form-group">
          <div class="col-xs-4">
            <label class="control-label">Amount</label>
            <input class="form-control" type="text" value="P X,XXX.XX" disabled />
          </div>
          <div class="col-xs-4">
            <label class="control-label">E-Voucher Code</label>
            <input class="form-control" type="text" placeholder="Input Details" />
          </div>
          <div class="col-xs-4">
            <label class="control-label">Override Amount</label>
            <input class="form-control" type="text" value="P XX.XX" disabled />
          </div>


        </div>
        <div class="form-group">
          <div class="col-xs-4">
            <label class="control-label">E-Voucher Discount</label>
            <input class="form-control" type="text" value="P XX.XX" disabled/>
          </div>
          <div class="col-xs-4">
            <label class="control-label">Terminal Fee</label>
            <input class="form-control" type="text" value="<?=isset($subvoyage_management[0]->origin_port_charge) ? $subvoyage_management[0]->origin_port_charge : 'P XX.XX' ?>" disabled/>
          </div>
          <div class="col-xs-4">
            <label class="control-label">Port Charge</label>
            <input class="form-control" type="text" value="<?=isset($subvoyage_management[0]->origin_terminal_fee) ? $subvoyage_management[0]->origin_terminal_fee : 'P XX.XX' ?>" disabled/>
          </div>


        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="col-xs-3">
        <label class="control-label">Total Amount</label>
        <input class="form-control" type="text" value="P XXXX.XX" disabled/>
      </div>
    </div>
  </div>

  <div class="col-xs-12">
    <hr class="hr-custom">
  </div>
  <div class="col-xs-12">

    <div class="form-group">
      <div class="col-xs-3 ">
        <label class="control-label">Return Voyage Code</label>
        <input class="form-control" type="text" value="TN0002" disabled/>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Passenger Fare Code</label>
        <select class="selectpicker form-control passenger_fare" name="return_passenger_fare">
          <option value="">Please Select</option>
          <?php foreach($passenger_fare as $key => $value): ?>
            <option value="<?=$value->id_passenger_fare ?>"><?=$value->passenger_fare ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Points Earning</label>
        <input class="form-control" type="text" value="XX" id="return_points_earning" disabled />
      </div>
      <div class="col-xs-3">
        <label class="control-label">Discount Code</label>
        <select class="selectpicker form-control" name="return_discount">
          <option value="">Please Select</option>
          <?php foreach($discount as $key => $value): ?>
            <option value="<?=$value->id_discount ?>"><?=$value->discount ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-3 ">
        <label class="control-label">Amount</label>
        <input class="form-control" type="text" value="P XXXX.XX" disabled />
      </div>
      <div class="col-xs-3">
        <label class="control-label">E-Voucher Code</label>
        <input class="form-control" type="text" placeholder="Input Details" />
      </div>
      <div class="col-xs-3">
        <label class="control-label">Override Amount</label>
        <input class="form-control" type="text" value="P XX.XX" disabled/>
      </div>
      <div class="col-xs-3">
        <label class="control-label">E-Voucher Discount</label>
        <input class="form-control" type="text" value="P XX.XX" disabled/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-3 ">
        <label class="control-label">Terminal Fee</label>
        <input class="form-control" type="text" value="P XXXX.XX" disabled/>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Port Charge</label>
        <input class="form-control" type="text" value="P XXXX.XX" disabled/>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Total Amount</label>
        <input class="form-control" type="text" value="P XXXX.XX" disabled/>
      </div>

    </div>
  </div>
</div>


<div class="form-group form-group__custom">
  <div class="col-xs-12">
    <div class="inner-header">
      <h3 class="inner-header__title">Total</h3>
    </div>
    <div class="form-group">

      <div class="col-xs-3">
        <label class="control-label">Total Due</label>
        <input class="form-control" type="text" value="P XXXX.XX" disabled/>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Mode of Payment</label>
        <select class="selectpicker form-control" title="Choose">
          <option value="">Mode of Payment 1 </option>
          <option value="">Mode of Payment 2 </option>
        </select>
      </div>
      <div class="col-xs-3">
        <label class="control-label">Reference Number</label>
        <input class="form-control" type="text" placeholder="Input Details" />
      </div>

    </div>
    <div class="form-group">

      <div class="col-xs-3">
        <label class="control-label">Amount Tendered</label>
        <input class="form-control" type="text" placeholder="Input Details" />
      </div>
      <div class="col-xs-3">
        <label class="control-label">Change</label>
        <input class="form-control" type="text" value="P XXXX.XX" disabled/>
      </div>
    </div>
  </div>
</div>

<div class="form-group oj-form-footer">
  <div class="col-xs-12">
    <div class="btn-oj-group right">
      <button type="submit" class="btn oj-button">Pay</button>
      <button type="submit" class="btn oj-button">Print Ticket</button>
    </div>
  </div>
</div>
            