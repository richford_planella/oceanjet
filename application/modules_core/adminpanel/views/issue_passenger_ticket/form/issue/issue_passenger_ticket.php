<?=$header;?>

  <form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
    <?=$this->load->view(admin_dir('notification'));?>
  	<div class="form-group">
      <div class="col-xs-2">
        <label class="control-label">Origin</label>
        <select class="selectpicker form-control ticket-orig-dest" name="origin" id="origin">
          <option value="">Please Select</option>
        	 <?php foreach ($port as $key => $value):?>
            <option value="<?=$value->id_port?>" <?=set_select('origin', $value->id_port);?>><?=$value->port?></option>
     		   <?php endforeach;?>
        </select>
        <span class="input-notes-bottom"><?=form_error('origin')?></span>
      </div>
      <div class="col-xs-2">
        <label class="control-label">Destination</label>
        <select class="selectpicker form-control ticket-orig-dest" name="destination" id="destination">
         <option value="">Please Select</option>
        	 <?php foreach ($port as $key => $value):?>
            <option value="<?=$value->id_port?>" <?=set_select('destination', $value->id_port);?>><?=$value->port?></option>
     		   <?php endforeach;?>
        </select>
        <span class="input-notes-bottom"><?=form_error('destination')?></span>
      </div>
      <div class="col-xs-3 has-feedback">
        <label class="control-label">Depature Date</label>
        <input class="form-control custom-datepicker voyage_date" type="text" name="departure_date" id="departure_date" value="<?=set_value('departure_date');?>" placeholder="YYYY-MM-DD" />
        <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
        <span class="input-notes-bottom"><?=form_error('departure_date')?></span>
      </div>
      <div class="col-xs-4">
        <label class="control-label">Voyage</label>
        <select class="selectpicker form-control select-voyage" id="voyage" name="voyage">
           <option value="">Please Select</option>
           <?php if(!empty($voyage)):?>
             <?php foreach ($voyage as $key => $value):?>
              <option value="<?=$value->id_subvoyage_management?>" <?=set_select('voyage', $value->id_subvoyage_management);?>><?=$value->voyage_code." | ".$value->vessel." | ".$value->ETD." - ".$value->ETA ?></option>
             <?php endforeach;?>
           <?php endif;?>
        </select>
        <span class="input-notes-bottom"><?=form_error('voyage')?></span>
      </div>
  	</div>

    <div class="form-group">
      <div class="col-xs-3">
        <label class="control-label">Accomodation Type</label>
        <select class="selectpicker form-control select-accomodation" id="accommodation" name="accommodation">
          <option value="">Please Select</option>
           <?php foreach ($accommodation as $key => $value):?>
          <option value="<?=$value->id_accommodation?>" <?=set_select('accommodation', $value->id_accommodation);?>><?="[".$value->accommodation_code."] ".$value->accommodation ?></option>
        <?php endforeach;?>
        </select>
        <span class="input-notes-bottom"><?=form_error('accommodation')?></span>
      </div>
      <div class="col-xs-1">
        <label class="control-label">Round Trip</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" id="round_trip" name="round_trip" value="1" <?=set_checkbox('round_trip', 1);?> /> Yes
          </label>
        </div>
      </div>
      <div class="col-xs-3 has-feedback">
        <label class="control-label">Return Date</label>
        <input class="form-control custom-datepicker voyage_date" type="text" name="return_date" id="return_date" value="<?=set_value('return_date');?>" placeholder="YYYY-MM-DD" disabled />
        <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
        <span class="input-notes-bottom"><?=form_error('return_date')?></span>
      </div>
      <div class="col-xs-4">
        <label class="control-label">Return Voyage</label>
        <select class="selectpicker form-control" id="return_voyage" name="return_voyage" disabled>
          <option value="">Please Select</option>
           <?php if(!empty($return_voyage)):?>
             <?php foreach ($return_voyage as $key => $value):?>
              <option value="<?=$value->id_subvoyage_management?>" <?=set_select('return_voyage', $value->id_subvoyage_management);?>><?=$value->voyage_code." | ".$value->vessel." | ".$value->ETD." - ".$value->ETA ?></option>
             <?php endforeach;?>
           <?php endif;?>
        </select>
         <span class="input-notes-bottom"><?=form_error('return_voyage')?></span>
      </div>
      <div class="col-xs-11" style="text-align:right">
        <div class="btn-oj-group oj-button--margin-top-30">
          <input type="hidden" name="validateTrip" value="" id="validateTrip" />
          <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Clear</a>
          <button type="submit" class="btn oj-button">Submit</button>
        </div>
      </div>
    </div>
    
    <!-- Trip details Ajax sync -->
    <div id="ticket_issuance"><?=$ticket_issuance_details?></div>

  </form>


<?=$footer;?>