<?=$header;?>
<form class="form-horizontal oj-form " action="<?=admin_url($this->classname,'issue');?>" method="POST">
<?=$this->load->view(admin_dir('notification'));
?>
	<div class="form-group">
		<div class="col-xs-3">
			<label class="control-label">Ticket Number</label>
			<?php
			foreach($ticket_series as $key => $value) {
				echo '<input type="text" id="ticket_number" class="form-control" value="'. $value->ticket_no .'" disabled /><input type="hidden" name="ticket_number" value="'. $value->id_ticket_series_info .'">';
			}
			?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-xs-3">
			<label class="control-label">Origin</label>
			<select class="selectpicker form-control ticket-orig-dest" id="origin" name="origin" title="Choose">
				 <?php
					foreach ($port_options as $key => $value) {
						echo "<option value=". $value->id_port .">". $value->port ."</option>";
					}
				 ?>
			</select>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">Destination</label>
			<select class="selectpicker form-control ticket-orig-dest" id="destination" name="destination" title="Choose">
				 <?php
					foreach ($port_options as $key => $value) {
						echo "<option value=". $value->id_port .">". $value->port ."</option>";
					}
				 ?>
			</select>
		</div>
		
		<div class="col-xs-5">
			<label class="control-label">Voyage</label>
			<div class="select-holder">
				<select class="selectpicker form-control select-voyage" id="voyage" name="voyage" title="Choose">
				</select>
			</div>
			<span class="input-notes-bottom"><?=form_error('voyage')?></span>
		</div>
		
		<div class="col-xs-1">
			<label class="control-label">Round Trip</label>
			<div class="checkbox-wrapper">
				<label class="checkbox-inline">
					<input type="checkbox" id="voyage_round_trip" name="voyage_round_trip" value="1" />Yes
				</label>
			</div>
		</div>
		
		<div class="col-xs-5">
			<label class="control-label">Return Voyage</label>
			<select class="selectpicker form-control select-return-voyage" id="return_voyage" name="return_voyage" title="Choose">
			</select>
		</div>
		
		<div class="col-xs-4">
			<label class="control-label">Accommodation Type</label>
			<select class="selectpicker form-control select-accommodation" id="accommodation" name="accommodation" title="Choose">
			<?php
				foreach($accommodation_options as $key=>$value) {
					echo '<option value="'. $value->id_accommodation .'">'. $value->accommodation_code .'</option>';
				}
			?>
			</select>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">&nbsp;</label>
			<div class="btn-oj-group">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname, 'add');?>">Clear</a>
				<button type="submit" class="btn oj-button">Submit</button>
			</div>
		</div>
	</div>
</form>
	
<form class="form-horizontal oj-form " action="<?=admin_url($this->classname,'add');?>" method="POST">
	<!-- START TRIP DETAILS -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Trip Details</span></div>
		
		<div class="oj-box2-content">
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Voyage Code</label>
					<input type="text" id="voyage_code" class="form-control" value="<?= print_r($session_data['voyage_details'][0]->voyage) ?>" disabled />
				</div>
			</div>
				
			<?php
			$count = 1;
			foreach($session_data['voyage_details'] as $key=>$value) {
			?>
			<div class="oj-box2">
				<div class="oj-box2-header"><span class="ojb-title">Leg <?=str_pad($count++, 2, "0", STR_PAD_LEFT);?></span></div>
				
				<div class="oj-box2-content">
					<div class="form-group">
						<div class="col-xs-3">
							<label class="control-label">Ticket Number</label>
							<input type="text" id="ticket_number" class="form-control" value="001" disabled />
						</div>
						
						<div class="col-xs-3">
							<label class="control-label">Origin</label>
							<input type="text" id="origin" class="form-control" value="<?= $value->origin_port_code ?>" disabled />
						</div>
						
						<div class="col-xs-3">
							<label class="control-label">Destination</label>
							<input type="text" id="destination" class="form-control" value="<?= $value->destination_port_code ?>" disabled />
						</div>
						
						<div class="col-xs-3">
							<label class="control-label">Departure Date</label>
							<input type="text" id="departure_date" class="form-control" value="<?= $value->departure_date ?>" disabled />
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-xs-3">
							<label class="control-label">Departure Time (ETD)</label>
							<input type="text" id="etd" class="form-control" value="<?= $value->ETD ?>" disabled />
						</div>
						
						<div class="col-xs-3">
							<label class="control-label">Departure Time (ETA)</label>
							<input type="text" id="eta" class="form-control" value="<?= $value->ETA ?>" disabled />
						</div>
						
						<div class="col-xs-3">
							<label class="control-label">Vessel</label>
							<input type="text" id="vessel" class="form-control" value="<?= $value->vessel; ?>" disabled />
						</div>
					</div>
				</div>
			</div>
			<?php
			}
			?>
		</div>
	</div>
	<br />
	<!-- END TRIP DETAILS -->
	
	<!-- START PASSENGER INFORMATION -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Passenger Information</span></div>
		<div class="oj-box2-content">
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">First Name</label>
					<input type="text" id="firstname" name="firstname" class="form-control" placeholder="First" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Last Name</label>
					<input type="text" id="lastname" name="lastname" class="form-control" placeholder="Last" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Middle Initial</label>
					<input type="text" id="middle_initial" name="middle_initial" class="form-control" placeholder="Middle" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Gender</label>
					<select class="selectpicker form-control" id="gender" name="gender">
						<option value="0">Male</option>
						<option value="1">Female</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Birth Date</label>
					<input type="text" id="birthdate" name="birthdate" class="form-control custom-datepicker" placeholder="MM/DD/YYYY" />
				</div>
				
				<div class="col-xs-2">
					<label class="control-label">Age</label>
					<input type="text" id="age" name="age" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Contact Number</label>
					<input type="text" id="contact_number" name="contact_number" class="form-control" placeholder="Contact Number" />
				</div>
				
				<div class="col-xs-2">
					<label class="control-label">ID Number</label>
					<input type="text" id="id_number" name="id_number" class="form-control" placeholder="ID Number" />
				</div>
				
				<div class="col-xs-2">
					<label class="control-label">Nationality</label>
					<input type="text" id="nationality" name="nationality" class="form-control" placeholder="Nationality" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-2">
					<label class="control-label">With Infant&#63;</label>
					<div class="checkbox-wrapper">
						<label class="checkbox-inline">
							<input type="checkbox" id="with_infant" name="with_infant" value="1" />Yes
						</label>
					</div>
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Infant's Name</label>
					<input type="text" id="infant_name" name="infant_name" class="form-control" placeholder="Infant's Name" />
				</div>
			</div>
		</div>
	</div>
	<br />
	<!-- END PASSENGER INFORMATION -->
	
	<!-- START TRIP STATISTICS TABLE -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Trip Statistics Table</span></div>
		<div class="oj-box2-content">
			<table id="table-v1" class="table global-table nopadding">
				<thead>
					<tr>
						<th>Items</th>
						<th>N/A</th>
						<th>N/A</th>
						<th>N/A</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Total Capacity</td>
						<td>N/A</td>
					</tr>
					<tr>
						<td>Issued Ticket</td>
						<td>N/A</td>
					</tr>
					<tr>
						<td>Reservation</td>
						<td>N/A</td>
					</tr>
					<tr>
						<td>Available</td>
						<td>N/A</td>
					</tr>
					<tr>
						<td>Promo Seats</td>
						<td>N/A</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<br />
	<!-- END TRIP STATISTICS TABLE -->
	
	<!-- START FARE DETAILS -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Fare Details</span></div>
		
		<div class="oj-box2-content">
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Voyage Code</label>
					<input type="text" id="voyage_code" name="voyage_code" class="form-control" value="Voyage Code" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Passenger Fare Code</label>
					<select class="selectpicker form-control" id="voyage_fare_code" name="voyage_fare_code" title="Choose">
					<?php
						foreach($passenger_fare_code_options as $key=>$value) {
							echo '<option value="'. $value->id_passenger_fare .'">'. $value->passenger_fare .'</option>';
						}
					?>
					</select>
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Points Earning</label>
					<?php
						foreach($session_data['passenger_fare_details'] as $key=>$value) {
							$points_earning = $value->points_earning;
							echo '<input value="'. $value->points_earning .'" type="text" id="voyage_points_earning" name="voyage_points_earning" class="form-control" disabled />';
						}
					?>
				</div>
			</div>
			
			<hr class="hr-custom" />
			
			<?php
			$count = 1;
			$total_breakdown = "";
			foreach($session_data['voyage_details'] as $key=>$val) {
				$total_breakdown[] = $val->fare_per_leg;
				$total[] = $val->fare_per_leg;
			?>
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Breakdown of Fare Per Leg</label>
					<input type="text" class="form-control" value="Leg <?=str_pad($count++, 2, "0", STR_PAD_LEFT);?> : Php<?php echo $val->fare_per_leg; ?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Discount Code</label>
					<select class="selectpicker form-control" id="voyage_discount_code" name="voyage_discount_code" title="Choose">
					<?php
						foreach($discount_code_options as $key=>$value) {
							echo '<option value="'. $value->id_discount .'">'. $value->discount_code .'</option>';
						}
					?>
					</select>
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">E-Voucher Code</label>
					<input type="text" id="voyage_evoucher_code" name="voyage_evoucher_code" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Amount</label>
					<input type="number" id="voyage_amount" name="voyage_amount" class="form-control price" data-original-value="<?php echo $val->fare_per_leg; ?>" value="<?php echo $val->fare_per_leg; ?>" disabled />
				</div>
			</div>
			<?php
			}
			?>
			
			<hr class="hr-custom" />
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Terminal Fee</label>
					<?php
						$total[] = $val->origin_terminal_fee;
						echo '<input value="'. $val->origin_terminal_fee .'" type="text" id="voyage_terminal_fee" name="voyage_terminal_fee" class="form-control" disabled />';
					?>
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Port Charge</label>
					<?php
						$total[] = $val->origin_port_charge;
						echo '<input value="'. $val->origin_port_charge .'" type="text" id="voyage_port_charge" name="voyage_port_charge" class="form-control" disabled />';
					?>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Total Amount</label>
					<input class="form-control price" value="<?php print array_sum($total) - $points_earning; ?>" type="number" id="voyage_total_amount" name="voyage_total_amount" disabled />
				</div>
			</div>
		</div>
	</div>
	<br />
	<!-- END FARE DETAILS -->
	
	<!-- START TOTAL -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Total</span></div>
		<div class="oj-box2-content">
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Total Due</label>
					<input type="text" id="total_due" name="total_due" class="form-control" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Mode of Payment</label>
					<select class="selectpicker form-control" id="mode_of_payment" name="mode_of_payment">
						<option value="">SELECT</option>
					</select>
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Reference Number</label>
					<input type="text" id="reference_number" name="reference_number" class="form-control" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Amount Tendered</label>
					<input type="text" id="amount_tendered" name="amount_tendered" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Change</label>
					<input type="text" id="change" name="change" class="form-control" disabled />
				</div>
			</div>
		</div>
	</div>
	<!-- END TOTAL -->
	
	<!-- START FORM BUTTONS FOOTER -->
	<div class="form-group oj-form-footer">
		<div class="col-xs-12">
			<div class="btn-oj-group left">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Pay</a>
				<button type="submit" class="btn oj-button">Print Ticket</button>
			</div>
		</div>
	</div>
	<!-- END FORM BUTTONS FOOTER -->
	
</form>
<?php $this->session->unset_userdata('passenger_session_data'); ?>
<?=$footer;?>