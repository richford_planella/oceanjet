<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
<div class="form-group">
  <div class="col-xs-12">
      <?=$this->load->view(admin_dir('notification'));?>
  </div>
</div>
<div class="form-group">
  <div class="col-xs-12">
    <label class="control-label">Terms and Conditions</label>
    <textarea class="form-control" rows="5" placeholder="No content available" name="eticket_terms" maxlength="3000"><?=set_value("eticket_terms", $eticket->eticket_terms); ?></textarea>
    <span class="input-notes-bottom"><?=form_error('eticket_terms')?></span>
  </div>
</div>
<div class="form-group">
  <div class="col-xs-12">
    <label class="control-label">Reminders</label>
    <textarea class="form-control" rows="5" placeholder="No content available" name="eticket_reminders" maxlength="3000"><?=set_value("eticket_reminders", $eticket->eticket_reminders); ?></textarea>
    <span class="input-notes-bottom"><?=form_error('eticket_reminders')?></span>
  </div>
</div>
<div class="form-group" style="display:none">
  <div class="col-xs-12">
    <label class="control-label">Eticket Details</label>
    <textarea class="form-control" rows="5" placeholder="No content available" name="eticket_details" maxlength="3000"><?=set_value("eticket_details", $eticket->eticket_details); ?></textarea>
    <span class="input-notes-bottom"><?=form_error('eticket_details')?></span>
  </div>
</div>
<div class="form-group" style="display:none">
  <div class="col-xs-12">
    <label class="control-label">Full Eticket</label>
    <textarea class="form-control" rows="5" placeholder="No content available" name="eticket_full" maxlength="3000"><?=set_value("eticket_full", $eticket->eticket_full); ?></textarea>
    <span class="input-notes-bottom"><?=form_error('eticket_full')?></span>
  </div>
</div>
<div class="col-xs-3 profile-img-wrap">            
  <div class="form-group">
    <div class="col-xs-12">
    <label class="control-label">Upload Image</label>
    <img id="img_uploaded" src="<?=!empty($eticket->img_path) ? $eticket->img_path : img_dir("eticket/oj-ticket-header.png");?>" alt="User Profile Picture" class="img-thumbnail">
    <div class="upload-img-wrap">
      <input type="file" name="img_path" id="imgInp" class="uploader" value="<?=!empty($eticket->img_path) ? $eticket->img_path : img_dir("eticket/oj-ticket-header.png");?>" />
      <label class="btn oj-button white-ghost-button oj-sm-button" for="imgInp">Choose Image</label>
      <input type="hidden" id="cur_img" value="<?=!empty($eticket->img_path) ? $eticket->img_path : img_dir("eticket/oj-ticket-header.png");?>" />
    </div>
  </div>
  </div>
</div>
<div class="form-group">
  <div class="col-xs-12">
    <label class="control-label control-label--full-width">Preview Itinerary</label>
    <input class="btn oj-button" type="button" value="Preview" id="btn-preview">
  </div>
</div>
<!-- <img id="blah" src="#" alt="your image"/> -->
<div class="form-group oj-form-footer">
  <div class="col-xs-12">
    <div class="btn-oj-group right">
      <button type="reset" class="btn oj-button gray-button">Cancel</button>
      <button type="submit" class="btn oj-button">Update</button>
    </div>
  </div>
</div>
</form>
<?=$footer;?>