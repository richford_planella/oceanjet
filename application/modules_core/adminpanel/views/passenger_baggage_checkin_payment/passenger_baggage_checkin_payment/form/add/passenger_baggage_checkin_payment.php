<?= $header; ?>
<form class="form-horizontal oj-form" action="<?= current_url(); ?>"  method="POST" id="passenger-baggage-check-in-form">
    <!--    Voyage Selection-->
    <div class="form-group">
        <div class="col-xs-2">
            <label class="control-label">Origin</label>
            <select id="origin_id" name="origin_id" class="selectpicker form-control">
                <option value="0">Please select</option>
                <?php if($ports):?>
                    <?php foreach($ports as $s):?>
                        <option value="<?=$s->id_port;?>" <?=set_select('origin_id',$s->id_port);?>> <?=$s->port;?> </option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
            <span class="input-notes-bottom"><?=form_error('origin_id');?></span>
        </div>
        <div class="col-xs-2">
            <label class="control-label">Destination</label>
            <select id="destination_id" name="destination_id" class="selectpicker form-control">
                <option value="0">Please select</option>
                <?php if($ports):?>
                    <?php foreach($ports as $s):?>
                        <option value="<?=$s->id_port;?>" <?=set_select('destination_id',$s->id_port);?>> <?=$s->port;?></option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
            <span class="input-notes-bottom"><?=form_error('destination_id');?></span>
        </div>
        <div class="col-xs-7">
            <label class="control-label">Voyage</label>
            <select id="voyage_management_id" name="voyage_management_id" class="selectpicker form-control">
                <option value="">Please select</option>
                <?php if($subvoyage):?>
                    <?php foreach($subvoyage as $value): ?>
                        <option value="<?=$value->id_subvoyage_management;?>" <?=set_select('voyage_management_id',$value->id_subvoyage_management);?>><?=$value->voyage." | ". $value->port_origin."-".$value->port_destination ." | ".  date("F j, Y - D", strtotime($value->departure_date))." | ". date('g:i a',strtotime($value->ETD)) ."-". date('g:i a',strtotime($value->ETA));?></option>
                    <?php endforeach;?>
                <?php endif;?>
            </select>
            <span class="input-notes-bottom"><?=form_error('voyage_management_id');?></span>
        </div>
        <div class="col-xs-1">

        </div>
    </div>
    <!--    Trip Details-->
    <!--    Trip Details-->
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Voyage Details</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Voyage Code</label>
                    <input name="voyage_text" class="form-control" value="<?=set_value('voyage_text');?>" id="select_voyages" placeholder="Autofill voyage code" readonly>
                    <span class="input-notes-bottom"><?=form_error('voyage_text');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Status</label>
                    <input class="form-control" id="subvoyage_status_text" name="subvoyage_status_text" type="text"
                           value="<?=set_value('subvoyage_status_text');?>" placeholder="Autofill voyage status"
                           readonly/>
                    <input type="hidden" name="subvoyage_status_id" id="subvoyage_status_id" value="<?=set_value('subvoyage_status_id');?>">
                    <span class="input-notes-bottom"><?=form_error('subvoyage_status_text');?></span>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="btn oj-button oj-button--margin-top-30 view-baggage-lists">View Baggage List</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3 has-feedback">
                    <label class="control-label">Departure Date</label>
                    <input class="form-control" id="departure_date" name="departure_date" type="text"
                           value="<?=set_value('departure_date');?>" placeholder="Autofill departure date" readonly/>
                    <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                    <span class="input-notes-bottom"><?=form_error('departure_date');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Origin</label>
                    <input class="form-control" id="origin" name="origin" type="text"
                           value="<?=set_value('origin');?>" placeholder="Autofill origin"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('origin');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Destination</label>
                    <input class="form-control" id="destination" name="destination" type="text"
                           value="<?=set_value('destination');?>" placeholder="Autofill destination"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('destination');?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3 has-feedback">
                    <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                    <input class="form-control" id="etd" name="etd" type="text"
                           value="<?=set_value('etd');?>" placeholder="Autofill ETD"
                           readonly/>
                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                    <span class="input-notes-bottom"><?=form_error('etd');?></span>
                </div>
                <div class="col-xs-3 has-feedback">
                    <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                    <input class="form-control" id="eta" name="eta" type="text"
                           value="<?=set_value('eta');?>" placeholder="Autofill ETA"
                           readonly/>
                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                    <span class="input-notes-bottom"><?=form_error('eta');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Vessel</label>
                    <input class="form-control" id="vessel" name="vessel" type="text"
                           value="<?=set_value('vessel');?>" placeholder="Autofill vessel"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('vessel');?></span>
                </div>
            </div>
        </div>
    </div>
    <!--    Trip Details-->
    <!--    Baggage Details-->
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Baggage Details</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Reference Number</label>
                    <input class="input-xlarge focused" id="transaction_ref" name="transaction_ref" type="hidden"
                           value="<?=set_value('transaction_ref');?>"
                           readonly/>
                    <input class="form-control" id="reference_no" name="reference_no" type="text"
                           value="<?=set_value('reference_no');?>" placeholder="Input reference no." readonly
                    />
                    <span class="input-notes-bottom"><?=form_error('reference_no');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Ticket Number</label>
                    <input class="form-control" id="ticket_no" name="ticket_no" type="text"
                           value="<?=set_value('ticket_no');?>" placeholder="Input Ticket no."
                    />
                    <div id="ticket_no_validation_div"></div>
                    <span class="input-notes-bottom"><?=form_error('ticket_no');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Passenger Name</label>
                    <input type="hidden" name="id_passenger_baggage_checkin" id="id_passenger_baggage_checkin" value="<?=set_value('id_passenger_baggage_checkin');?>">
                    <input type="hidden" name="passenger_id" id="passenger_id" value="<?=set_value('passenger_id');?>">
                    <input type="hidden" name="booking_status_id" id="booking_status_id" value="<?=set_value('booking_status_id');?>">
                    <input class="form-control" id="passenger_name" name="passenger_name" type="text"
                           value="<?=set_value('passenger_name');?>" placeholder="Autofill passenger name"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('passenger_name');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Baggage Rate</label>
                    <input class="form-control" id="id_attended_baggage" name="attended_baggage" type="text"
                           value="<?=set_value('attended_baggage');?>" placeholder="Autofill attended baggage"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('attended_baggage');?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Quantity (No. of bags)</label>
                    <input class="form-control" id="quantity" name="quantity"
                           onkeypress='numeric_validation(event)' type="text"
                           value="<?= set_value('quantity'); ?>" placeholder="Autofill quantity" readonly/>
                    <span class="input-notes-bottom"><?=form_error('quantity');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Weight</label>
                    <input class="form-control" id="weight" name="weight"
                           onkeypress='numeric_validation(event)' type="text"
                           value="<?= set_value('weight'); ?>" placeholder="Autofill weight" readonly/>
                    <span class="input-notes-bottom"><?=form_error('weight');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Unit Of Measurement (UOM)</label>
                    <input class="form-control" id="uom" name="uom" type="text"
                           value="<?= set_value('uom'); ?>" placeholder="Autofill UOM" readonly/>
                    <span class="input-notes-bottom"><?=form_error('uom');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Free Baggage</label>
                    <input class="form-control" id="free_baggage" name="free_baggage" type="text"
                           value="<?=set_value('free_baggage');?>" placeholder="Autofill free baggage" readonly/>
                    <span class="input-notes-bottom"><?=form_error('free_baggage');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Attended Baggage</label>
                    <input class="form-control" id="attended_baggage" name="attended_baggage"
                           type="text" placeholder="Autofill attended baggage"
                           value="<?=set_value('attended_baggage');?>" readonly/>
                    <span class="input-notes-bottom"><?=form_error('attended_baggage');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Total Amount</label>
                    <input class="form-control" id="total_amount" name="total_amount" placeholder="Autofill total amount"
                           value="<?=set_value('total_amount');?>" readonly/>
                    <span class="input-notes-bottom"><?=form_error('total_amount');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Amount Tendered</label>
                    <input class="form-control" id="amount_tendered" type="number"
                           name="amount_tendered" value="<?=set_value('amount_tendered')?>" placeholder="Input amount tendered"
                           onkeypress='numeric_validation(event)'
                    />
                    <span class="input-notes-bottom"><?=form_error('amount_tendered');?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Change</label>
                    <input class="form-control" id="change" name="change" value="<?=set_value('change')?>"
                           placeholder="Auto compute change"
                           readonly/>
                    <span class="input-notes-bottom"><?=form_error('change');?></span>
                </div>
                <div class="col-xs-2">
                    <button type="button" class="btn oj-button oj-button--margin-top-30 validate-form">Pay</button>
                </div>
            </div>
        </div>
    </div>
    <!--    Baggage Details-->
</form>

<!--Some generic modal coming right up start-->
<div id="confirmation-modal" class="modal bootstrap-dialog type-primary fade size-normal in" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h3 class="modal-title">Passenger Baggage Check-In</h3>
            </div>
            <div class="modal-body">
                <h4 id="myConfirmationModalLabel"></h4>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Dismiss </button>
            </div>
        </div>
    </div>
</div>
<!--Some generic modal coming right up end-->

<?= $footer; ?>
<script type="text/javascript">
    $(function () {

        //validate if form can be check-in start
        $(document).on("click", ".validate-form", function () {
            //Check Leg status
            if($('#change').val()<=-1){
                $('#myConfirmationModalLabel').html("Cannot proceed to baggage check-in payment, amount tendered is invalid. Please try again."); //Set available users
                $('#confirmation-modal').modal('show');
            }else{
                //Submit Form
                $('#passenger-baggage-check-in-form').submit();
            }

        });
        //validate if form can be check-in end

        //View Baggage Lists start
        $(document).on("click", ".view-baggage-lists", function () {

            //Check if we have selected subvoyage
            var voyage_management_id = $("#voyage_management_id").val();

            if(checkSelectedsubvoyages()!=0)
                window.location.href = adminURL+"passenger_baggage_checkin_payment/view/"+voyage_management_id;

        });
        //View Baggage Lists end

        //set Origin and Destination
        //Return list of sub voyages drop down list
        $(document).on("change", "#origin_id, #destination_id", function () {
            //Get Origin and destination IDs
            var origin_id       = $('#origin_id').val();
            var destination_id = $('#destination_id').val();

            //If same origin and destination add warning message
            if(origin_id==destination_id) {
                $('#myConfirmationModalLabel').html('Invalid origin and destination. Please try again.');
                return $('#confirmation-modal').modal('show');
            }

            //Reset Fields
            resetVoyageDetails();

            //Fetch list of legs
            $.ajax({
                type: 'post',
                async: true,
                dataType: 'json',
                url: adminURL + 'passenger_baggage_checkin/ajax',
                data: "module=subvoyage&origin_id=" + origin_id+'&destination_id='+destination_id,
                success: function (data) {
                    //Print list of legs
                    $("#voyage_management_id").html(data.option);
                    $('#voyage_management_id').selectpicker('refresh');
                }
            });

        });


        //Leg Details start
        $(document).on("change", "#voyage_management_id", function() {
            var voyage_management_id = $("#voyage_management_id").val();
            var origin_id            = $("#origin_id").val();
            var destination_id       = $("#destination_id").val();

            //Reset Fields
            resetVoyageDetails();

            $.ajax({
                type        :   'post',
                async       :   true,
                dataType    :   'json',
                url         :   adminURL + 'passenger_baggage_checkin/ajax',
                data        :   "module=details&voyage_management_id=" + voyage_management_id+"&origin_id="+origin_id+"&destination_id="+destination_id,
                success     :   function(data) {
                    $("#departure_date").val(data.subvoyage.details.departure_date);
                    $("#select_voyages").val(data.subvoyage.details.voyage);
                    $("#origin").val(data.subvoyage.details.port_origin);
                    $("#destination").val(data.subvoyage.details.port_destination);
                    $("#etd").val(data.subvoyage.details.ETD);
                    $("#eta").val(data.subvoyage.details.ETA);
                    $("#vessel").val(data.subvoyage.details.vessel_code);
                    $("#subvoyage_status_text").val(data.subvoyage.details.description);
                    $("#subvoyage_status_id").val(data.subvoyage.details.subvoyage_management_status_id);

                    //Print attended baggage lists
                    //Add List of baggage based on Voyage ID start
                    $("#id_attended_baggage").html(data.attended_baggage_option);
                    //Add List of baggage based on Voyage ID end
                }
            });
        });
        //Leg Details end

        //Get user details as Agent type
        // Passenger Checkin Rate
        $(document).on("keyup", "#ticket_no", function () {
            // Excess Baggage ID
            var ticket_no = $("#ticket_no").val();

            $.ajax({
                type: 'post',
                async: true,
                dataType: 'json',
                url: adminURL + 'passenger_baggage_checkin_payment/ajax',
                data: "module=passenger_details&ticket_no=" + ticket_no,
                success: function (data) {

                    if (!jQuery.isEmptyObject(data)) {
                        // Add validation for valid ticket number
                        // Voyage code of booking Vs. Voyage code selected
                        if(data.details.subvoyage_management_id!=$('#voyage_management_id').val())
                            return $("#ticket_no_validation_div").html("<span class='input-notes-bottom'>Invalid voyage, cannot proceed to check-in payment. Please try again.</span>");

                        if(data.details.quantity==null)
                            return $("#ticket_no_validation_div").html("<span class='input-notes-bottom'>It looks like your baggage is not yet process on baggage check-in. Please try again.</span>");

                        $("#passenger_name").val(data.details.lastname +', '+ data.details.firstname +' '+ data.details.middlename.substr(0,1)+'.');
                        $("#id_attended_baggage").val('['+data.details.attended_baggage +'] '+ data.details.description );
                        $("#transaction_ref").val(data.details.transaction_ref);
                        $("#reference_no").val(data.details.reference_no);
                        $("#quantity").val(data.details.quantity);
                        $("#weight").val(data.details.weight);
                        $("#quantity").val(data.details.quantity);
                        $("#uom").val(data.details.unit_of_measurement);
                        $("#free_baggage").val(data.details.free_baggage);
                        $("#id_passenger_baggage_checkin").val(data.details.id_passenger_baggage_checkin);

                        var attended_baggage = (data.details.weight - data.details.free_baggage).toFixed(2);
                        if(attended_baggage <=-1)
                            attended_baggage=0;

                        $("#attended_baggage").val(attended_baggage);
                        $("#total_amount").val(data.details.total_amount);

                        $("#ticket_no_validation_div").html('');
                    } else {
                        $("#ticket_series_info_id").val(0);
                        $("#passenger_name").val("No match found");
                        $("#free_baggage").val("0.00");
                        $("#ticket_no_validation_div").html("<span class='input-notes-bottom'>Invalid ticket no. Please try again.</span>");
                    }
                },
            });
        }); // Excess Baggage ID
        //Get user details as Agent type

        //Compute Change Start
        $(document).on("keyup", "#amount_tendered", function () {

            var total_amount = $("#total_amount").val();
            var amount_tendered = $("#amount_tendered").val();

            var change = parseFloat(amount_tendered - total_amount);
            $("#change").val((change).toFixed(2));
        });
        //Compute Change End
    });

    //Check if we have selected voyages
    //check if we have voyage selected
    function checkSelectedsubvoyages(){
        if($('#voyage_management_id').val()==''){
            $('#myConfirmationModalLabel').html("No voyage selected. Please try again."); //Set available users
            $('#confirmation-modal').modal('show');

            throw new Error("No voyage selected. Please try again.");
        }
    }

    // Number input only
    function numeric_validation(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[\u00080-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }


    //Set fields to be blank reset Voyage Fields
    function resetVoyageDetails()
    {
        $("#departure_date").val('');
        $("#select_voyages").val('');
        $("#origin").val('');
        $("#destination").val('');
        $("#etd").val('');
        $("#eta").val('');
        $("#vessel").val('');
        $("#subvoyage_status_text").val('');
        $("#subvoyage_status_id").val('');

        //Print attended baggage lists
        //Add List of baggage based on Voyage ID start
        $("#id_attended_baggage").val("");
    }

    // Print Bill
    $(document).on("click", "#print_bill", function () {
        window.print();
    });

    $(document).on("click", "#manifest", function () {
        window.print();
    });


</script>