<?= $header; ?>
<style>@media print {
        .noprint {
            display: none;
        }

        .pagebreak {
            page-break-before: always;
        }
    }</style>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="icon-reorder noprint"></i><span class="break"></span><b>Voyage Details</b></h2>
            <div class="box-icon">
                <a href="#" class="btn-minimize noprint"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?= $this->load->view(admin_dir('notification')); ?>
            <p class="center">
                <a class="btn btn-large btn-success noprint" href="<?= admin_url($this->classname, 'add',$departure_date, $voyage_id); ?>"><i
                        class="halflings-icon white plus"></i>Check In</a>
                <a class="btn btn-large btn-success noprint" href="javascript:void(0);" id="manifest"><i
                        class="halflings-icon white plus"></i>Print RORO Manifest</a>
            </p>
            <table class="table table-striped table-bordered bootstrap-datatable datatable1">
                <?php if (!empty($voyage_details)): ?>
                    <?php foreach ($voyage_details as $vd): ?>
                        <tr>
                            <td>Departure Date</td>
                            <td><?= $vd->departure_date; ?></td>
                        </tr>
                        <tr>
                            <td>Sub Voyage</td>
                            <td><?= $vd->voyage; ?></td>
                        </tr>
                        <tr>
                            <td>Origin</td>
                            <td><?= $vd->port_origin; ?></td>
                        </tr>
                        <tr>
                            <td>Destination</td>
                            <td><?= $vd->port_destination; ?></td>
                        </tr>
                        <tr>
                            <td>ETA</td>
                            <td><?= $vd->ETA; ?></td>
                        </tr>
                        <tr>
                            <td>ETD</td>
                            <td><?= $vd->ETD; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="2">No Voyage Details, please select Departure date and Voyage.</td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    </div><!--/span-->
</div><!--/row-->
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="icon-reorder noprint"></i><span class="break"></span><b>Baggage Check-In </b></h2>
            <div class="box-icon">
                <a href="#" class="btn-minimize noprint"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th class="align-center">Reference No.</th>
                    <th>Ticket No.</th>
                    <th>Passenger</th>
                    <th>Total Amount</th>
                    <th>Paid </th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($passenger_baggage_checkins)): ?>
                    <?php foreach ($passenger_baggage_checkins as $u): ?>
                        <tr>
                            <td class="align-center"><?= $u->reference_no; ?></td>
                            <td class="align-center"><?= $u->ticket_no; ?></td>
                            <td class="center"><?= $u->lastname . ", " . $u->firstname . " " . substr($u->middlename, 0, 1) . "."; ?></td>
                            <td class="center">&#8369; <?= $u->total_amount; ?></td>
                            <td class="center"><?= ($u->paid_status)==1 ? 'Paid' : 'Not Paid'; ?></td>
                            <td class="center">
                                <input type="button" value="Re-print baggage claim tag" class="btn btn-primary noprint">
                                <!-- if passenger is not yet paid-->
                                <!--  enabled edit button-->
                                <a class="btn btn-info" href="<?=admin_url($this->classname, 'edit', $u->id_passenger_baggage_checkin);?>" title="Edit" data-rel="tooltip">
                                    <i class="halflings-icon white edit"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                        <tr>
                            <td colspan="5">No available record.</td>
                        </tr>
                <?php endif; ?>
                </tbody>

            </table>
        </div>
    </div><!--/span-->
</div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<div class="modal hide fade" id="myModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Settings</h3>
    </div>
    <div class="modal-body">
        <p>Here settings can be configured...</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
    </div>
</div>

<?= $footer; ?>
<script type="text/javascript">
    $(function () {

        // Print Bill
        $(document).on("click", "#print_bill", function () {
            window.print();
        });

        $(document).on("click", "#manifest", function () {
            $(".dataTables_length").hide();
            $("#DataTables_Table_0_filter").hide();
            window.print();
        });

        var beforePrint = function () {
        };

        var afterPrint = function () {
            location.reload();
        };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(function (mql) {
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }

        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;

    });

</script>