<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>Vessel Code</th>
                <th>Vessel Name</th>
                <th>Total No. of Seats</th>
                <th>Status </th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($vessel)):?>
            <?php foreach($vessel as $v):?>
            <tr>
                <td><?=$v->vessel_code;?></td>
                <td><?=$v->vessel;?></td>
                <td><?=$v->total_seats;?></td>
                <td><?=($v->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
                <td>
                    <a href="<?=admin_url($this->classname, 'view', $v->id_vessel);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $v->id_vessel);?>">Edit</a>
                    <a href="<?=admin_url($this->classname, 'delete', $v->id_vessel);?>" class="delete-vessel">Delete</a>
                    <?php if($v->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $v->id_vessel, md5($token.$this->classname.$v->id_vessel));?>" class="de-activate-vessel">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $v->id_vessel, md5($token.$this->classname.$v->id_vessel));?>" class="activate-vessel">
                                Re-activate
                        </a>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
<?=$footer;?>