<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Vessel Code</label>
            <input readonly="readonly" class="form-control" id="vessel_code" name="vessel_code" type="text" value="<?=set_value('vessel_code', $vessel->vessel_code);?>" placeholder="Input vessel code" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-5">
            <label class="control-label">Vessel Name</label>
            <input readonly="readonly" class="form-control" id="vessel" name="vessel" type="text" value="<?=set_value('vessel', $vessel->vessel);?>" placeholder="Input vessel name" />
        </div>
        <div class="col-xs-3">
            <label class="control-label">Total No. of Seats</label>
            <input readonly="readonly" class="form-control" type="number" name="total_seats" id="total_seats" value="<?=set_value('total_seats', $vessel->total_seats);?>"  />
        </div>
        <div class="col-xs-3">
            <label class="control-label">Status</label>
            <div class="controls">
                <input disabled="disabled" class="form-control dis" type="text" name="enabled" placeholder="Input outlet enabled" value="<?=set_value('enabled', ($vessel->enabled)? 'Active' : 'Inactive');?>" />
            </div>
        </div>
    </div>
    <!--Vessel Table -->
    <div class="form-group">
        <div class="col-xs-12">
            <!-- -->
            <div class="row">
                <div class="col-xs-7">
                    <label class="control-label">Seat Plan Preview</label>
                </div>
                <div class="col-xs-5">
                    <ul class="list-unstyled list-inline text-right oj-seats">
                        <li class="oj-seats__list"><span class="oj-seats__box" style="background-color: #000"></span>N/A</li>
                        <?php foreach($accommodation as $a):?>
                        <li class="oj-seats__list"><span class="oj-seats__box" style="background-color: <?=$a->palette;?>"></span><?=$a->accommodation_code;?></li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <div class="oj-box2" id="seat_plan_container">
            <?php if(!empty($seat_plan)):?>
                <div class="oj-box2-header"></div>
                <div class="oj-box2-content">
                    <table class="oj-vessel" cellpadding="0" cellspacing="0">
                        <tbody>
                            <?php for($i=1;$i<=$vessel->total_row;$i++):?>
                                <tr>
                                    <?php for($x=1;$x<=$vessel->total_column;$x++):?>
                                        <td style="text-align: center;<?=($seat_plan[$i][$x]['value'] == "N/A") ? 'background-color: #000;color:#fff' : 'background-color: '.$seat_plan[$i][$x]['color']?>">
                                            <input type="hidden" name="vessel_seats[<?=$i;?>][<?=$x;?>]" value="<?=($seat_plan[$i][$x]['value'] == "N/A") ? 'N/A' : $seat_plan[$i][$x]['value']?>">
                                            <input type="hidden" name="accommodation[<?=$i;?>][<?=$x;?>]" value="<?=$seat_plan[$i][$x]['accommodation'];?>">
                                            <?=($seat_plan[$i][$x]['value'] == "N/A") ? 'N/A' : $seat_plan[$i][$x]['value']?>
                                        </td>
                                    <?php endfor;?>
                                </tr>
                            <?php endfor;?>
                        </tbody>
                    </table>
                </div>
            <?php else:?>
                <div class="oj-box2-header"></div>
                <div class="oj-box2-content">
                    <table class="oj-vessel" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="3" class="col-xs-12 table-action-custom">
                                    <span class="not-available">No File Upload</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            <?php endif;?>
            </div>
            
        </div>
    </div>
    <!--!End Vessel Table -->
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
                <a href="<?=admin_url($this->classname,'edit',$vessel->id_vessel);?>" class="btn oj-button oj-button">Edit</a>
            </div>
        </div>
    </div>
</form>
<?=$footer;?>