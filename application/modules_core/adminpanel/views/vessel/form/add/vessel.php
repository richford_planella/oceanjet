<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">Vessel Code</label>
            <input class="form-control textToUpper" id="vessel_code" name="vessel_code" type="text" value="<?=set_value('vessel_code');?>" placeholder="Input vessel code" />
            <span class="input-notes-bottom"><?php echo form_error('vessel_code'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-5">
            <label class="control-label">Vessel Name</label>
            <input class="form-control" id="vessel" name="vessel" type="text" value="<?=set_value('vessel');?>" placeholder="Input vessel name" />
            <span class="input-notes-bottom"><?php echo form_error('vessel'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Total No. of Seats</label>
            <input readonly="readonly" class="form-control" type="number" name="total_seats" id="total_seats" value="<?=$total_seats;?>"  />
            <span class="input-notes-bottom"><?php echo form_error('total_seats'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Status</label>
            <select class="selectpicker form-control" id="enabled" name="enabled">
                <option value="1" <?=set_select('enabled', '1');?>>Active</option>
                <option value="0" <?=set_select('enabled', '0');?>>Inactive</option>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('enabled'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <label class="control-label" for="vfile">Upload Seat Plan</label>
            <div class="oj-box">
                <ul>
                    <li>*Note</li>
                    <li>- The file should be in CSV format.</li>
                    <li>- The seat no. shall follow this format: (Accommodation Type Code)-(Seat No.)</li>
                </ul>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="ojinput" id="fileInput" name="fileInput" type="file" accept=".csv">
                        <button class="btn oj-button" type="submit" id="fileUpload" name="submit" value="upload">Upload File</button>
                        <span class="input-notes-bottom"><?php echo form_error('fileInput'); ?></span>
                        <input type="hidden" name="total_row" id="total_row" value="<?=$total_row;?>" >
                        <input type="hidden" name="total_column" id="total_column" value="<?=$total_column;?>" >
                        <input type="hidden" name="hidden_total_seats" id="hidden_total_seats" value="<?=$total_seats;?>" >
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Vessel Table -->
    <div class="form-group">
        <div class="col-xs-12">
            <!-- -->
            <div class="row">
                <div class="col-xs-7">
                    <label class="control-label">Seat Plan Preview</label>
                </div>
                <div class="col-xs-5">
                    <ul class="list-unstyled list-inline text-right oj-seats">
                        <li class="oj-seats__list"><span class="oj-seats__box" style="background-color: #000"></span>N/A</li>
                        <?php foreach($accommodation as $a):?>
                        <li class="oj-seats__list"><span class="oj-seats__box" style="background-color: <?=$a->palette;?>"></span><?=$a->accommodation_code;?></li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <div class="oj-box2" id="seat_plan_container">
            <?php if(!empty($seat_plan)):?>
                <div class="oj-box2-header"><a class="btn oj-button oj-sm-button" href="#" class="ojlink disabled" id='clear_seats'>Remove</a></div>
                <div class="oj-box2-content">
                    <table class="oj-vessel" cellpadding="0" cellspacing="0">
                        <tbody>
                            <?php for($i=1;$i<=$total_row;$i++):?>
                                <tr>
                                    <?php for($x=1;$x<=$total_column;$x++):?>
                                        <td style="text-align: center;<?=($seat_plan[$i][$x]['value'] == "N/A") ? 'background-color: #000;color:#fff' : 'background-color: '.$seat_plan[$i][$x]['color']?>">
                                            <input type="hidden" name="vessel_seats[<?=$i;?>][<?=$x;?>]" value="<?=($seat_plan[$i][$x]['value'] == "N/A") ? 'N/A' : $seat_plan[$i][$x]['value']?>">
                                            <input type="hidden" name="accommodation[<?=$i;?>][<?=$x;?>]" value="<?=$seat_plan[$i][$x]['accommodation'];?>">
                                            <?=($seat_plan[$i][$x]['value'] == "N/A") ? 'N/A' : $seat_plan[$i][$x]['value']?>
                                        </td>
                                    <?php endfor;?>
                                </tr>
                            <?php endfor;?>
                        </tbody>
                    </table>
                </div>
            <?php else:?>
                <div class="oj-box2-header"></div>
                <div class="oj-box2-content">
                    <table class="oj-vessel" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="3" class="col-xs-12 table-action-custom">
                                    <span class="not-available">No File Upload</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            <?php endif;?>
            </div>
            
        </div>
    </div>
    <!--!End Vessel Table -->
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
                <button type="submit" name="submit" value="submit" class="btn oj-button">Save</button>
            </div>
        </div>
    </div>
</form>
<?=$footer;?>