<?=$header;?>
	
    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Passenger Fare Code</label>
          <input class="form-control textToUpper" type="text" placeholder="Input passenger fare code" name="passenger_fare" value="<?=set_value('passenger_fare');?>" />
		  <span class="input-notes-bottom"><?=form_error('passenger_fare')?></span>
        </div>
        <div class="col-xs-4">
          <label class="control-label">Voyage Code</label>
          <select class="selectpicker form-control" id="voyage" name="voyage_id">
            <option value="">Please Select</option>
			<?php 	foreach ($voyage as $key => $value): ?>
				<option value="<?=$value->id_voyage; ?>" <?=set_select('voyage_id', $value->id_voyage);?>><?=$value->voyage." | ".$value->origin." - ".$value->destination; ?></option>
			 <?php endforeach; ?>
          </select>
		  <span class="input-notes-bottom"><?=form_error('voyage_id')?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Origin Leg Selection</label>
          <select class="selectpicker form-control" id="subvoyage" name="subvoyage">
            <option value="">Please Select</option>
			<?php 	if (!empty($subvoyage)): ?>
				<?php 	foreach ($subvoyage as $key => $value): ?>
					<option value="<?=$value->id_subvoyage; ?>" <?=set_select('subvoyage', $value->id_subvoyage);?>><?=$value->origin; ?></option>
				 <?php endforeach; ?>
			 <?php endif; ?>
          </select>
		    <span class="input-notes-bottom"><?=form_error('subvoyage')?></span>
        </div>
        <div class="col-xs-2 text-right">
          <button id="add-leg-btn" class="btn oj-button oj-button--margin-top-30" type="button">Add Leg</button>
        </div>
      </div>
	 
	<div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Accommodation Type</label>
         <select class="selectpicker form-control" id="accommodation_id" name="accommodation_id">
            <option value="">Please Select</option>
			<?php foreach($accommodation as $akey => $avalue): ?>
				<option value="<?=$avalue->id_accommodation ?>" <?=set_select('accommodation_id', $avalue->id_accommodation);?>><?="[".$avalue->accommodation_code."] ".$avalue->accommodation ?></option>
			<?php endforeach; ?>
          </select>
		  <span class="input-notes-bottom"><?=form_error('accommodation_id')?></span>
        </div>
        <div class="col-xs-4">
          <label class="control-label">Rule Set</label>
          <select class="selectpicker form-control" id="rule_set_id" name="rule_set_id">
            <option value="">Please Select</option>
			<?php foreach($rule_set as $rkey => $rvalue): ?>
				<option value="<?=$rvalue->id_rule_set ?>" <?=set_select('rule_set_id', $rvalue->id_rule_set);?>><?=$rvalue->rule_set ?></option>
			<?php endforeach; ?>
          </select>
		  <span class="input-notes-bottom"><?=form_error('rule_set_id')?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Regular Fare</label>
          <select class="selectpicker form-control" id="regular_fare" name="regular_fare">
            <option value="">Please Select</option>
			<option value="1" <?=set_select('regular_fare', '1');?>>Yes</option>
			<option value="0" <?=set_select('regular_fare', '0');?>>No</option>
          </select>
		  <span class="input-notes-bottom"><?=form_error('regular_fare')?></span>
        </div>
      </div>
	  
	 <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Seat Limit</label>
          <input class="form-control" type="number" min="0" name="seat_limit" type="text" value="<?=set_value('seat_limit');?>" placeholder="Input seat limit" />
		  <span class="input-notes-bottom"><?=form_error('seat_limit')?></span>
        </div>
		<div class="col-xs-4">
          <label class="control-label">Points Earning</label>
          <input class="form-control" type="number" min="0" name="points_earning" type="text" value="<?=set_value('points_earning');?>" placeholder="Input points earning" />
		  <span class="input-notes-bottom"><?=form_error('points_earning')?></span>
        </div>		
		
		<div class="col-xs-2">
           <label class="control-label">Default</label>
           <div class="checkbox">
               <label class="checkbox-inline checkbox__label">				   
				   <input id="default_fare" name="default_fare" type="hidden" value="0">
				   <input class="checkbox-fields__check" id="default_fare" name="default_fare" type="checkbox" value="1" <?=set_checkbox('default_fare', 1);?> /> Default Fare
               </label>
           </div>
           <span class="input-notes-bottom"></span>
        </div>

        <!--Return Fare-->
        <div class="col-xs-2">
           <label class="control-label">Return</label>
           <div class="checkbox">
               <label class="checkbox-inline checkbox__label">				   
				   <input id="return_fare" name="return_fare" type="hidden" value="0">
				   <input class="checkbox-fields__check return_fare" id="return_fare" name="return_fare" type="checkbox" value="1" <?=set_checkbox('return_fare', 1);?> /> Return Fare
               </label>
           </div>
           <span class="input-notes-bottom"></span>
        </div>
		<!--!End Return Fare-->
	   
		<div class="col-xs-4">
			<input class="form-control leg_count" name="leg_count" type="hidden" value="" />
			<input class="form-control leg_order" name="leg_order" type="hidden" value="" />
			<input class="form-control origin_id" name="origin_id" type="hidden" value="<?=set_value('origin_id');?>" />
			<input class="form-control destination_id" name="destination_id" type="hidden" value="<?=set_value('destination_id');?>" />
        </div>
	  </div>
	  
	<hr class="hr-custom ">
	  
	<div id="subvoyage_details">
		<?php if (!empty($subvoyages)): ?>
			<?php foreach ($subvoyages as $skey => $svalue): ?>
				<div class="oj-leg-fare">
					<div class="form-group oj-leg">
					  <div class="col-lg-12">
						<div class="input-group">
						  <div class="oj-leg__title oj-leg__title--gray">
							Leg No. 1
						  </div>
						  <span class="input-group-btn">
						   <button class="btn btn-default oj-leg__btn" name="subvoyages[<?=$svalue->id_subvoyage?>][remove]" id="sub-<?=$svalue->id_subvoyage?>" type="button">Remove</button>
						  </span>
						</div>
					  </div>
					  <div class="col-xs-12 oj-leg__body">
						<div class="form-group">
						  <div class="col-xs-2">
							<label class="control-label">Origin</label>
							<input class="form-control" name="subvoyages[<?=$svalue->id_subvoyage?>][origin]" type="text" value="<?=$svalue->origin?>" id="origin" readonly/>
						  </div>
						  <div class="col-xs-2">
							<label class="control-label">Destination</label>
							<input class="form-control" name="subvoyages[<?=$svalue->id_subvoyage?>][destination]" type="text" value="<?=$svalue->destination?>" id="destination" readonly />
						  </div>
						  <div class="col-xs-3 has-feedback">
							<label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
							<input class="form-control oj-time-picker oj-etd" name="subvoyages[<?=$svalue->id_subvoyage?>][ETD]" type="text" placeholder="HH:MM" id="etd" value="<?=$svalue->ETD?>" readonly />
							<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
						  </div>
						  <div class="col-xs-3 has-feedback">
							<label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
							<input class="form-control oj-time-picker oj-eta" name="subvoyages[<?=$svalue->id_subvoyage?>][ETA]" type="text" placeholder="HH:MM" id="eta" value="<?=$svalue->ETA?>" readonly />
							<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
						  </div>
						  <div class="col-xs-2">
							<label class="control-label">Amount</label>
							<input class="form-control price" name="subvoyages[<?=$svalue->id_subvoyage?>][amount]" type="number" value="<?=$svalue->amount ?>" placeholder="Input amount" />
							<input class="form-control subvoyage_ids" name="subvoyages[<?=$svalue->id_subvoyage?>][id_subvoyage]" type="hidden" value="<?=$svalue->id_subvoyage ?>" />
							<span class="input-notes-bottom"></span>
						  </div>
						</div>
					  </div>
					</div>
				</div>
			 <?php endforeach; ?>
		 <?php endif; ?>
	</div>

	<!-- Return voyage details -->
	<div id="subvoyage_return_details">
	<?php if(!empty($return_voyage)): ?>
		<div class="oj-leg-fares">
			<div class="form-group oj-legs" style="background-color: #333;padding: 8px;color: #FFF;">
			  <div class="col-lg-12">
				<div class="input-group">
				  <div class="oj-leg__title oj-leg__title--gray">
					Return Voyage
				  </div>
				</div>
			  </div>
			  <div class="col-xs-12 oj-leg__body">
			  	<div class="form-group">
			  	  <div class="col-xs-4">
					<label class="control-label">Return Voyages</label>
					 <select class="selectpicker form-control" id="return_voyage" name="return_voyage_id">
			            <option value="">Please Select</option>
						<?php 	foreach ($voyage as $key => $value): ?>
							<option value="<?=$value->id_voyage; ?>" <?=set_select('return_voyage_id', $value->id_voyage);?>><?=$value->voyage; ?></option>
						 <?php endforeach; ?>
			          </select>
				  </div>
			  	</div>
			  </div>
			  <div class="col-xs-12 oj-leg__body">
				<div class="form-group">
				  <div class="col-xs-2">
					<label class="control-label">Origin</label>
					<input class="form-control" name="return_subvoyage[origin]" type="text" value="" id="return_origin" readonly/>
				  </div>
				  <div class="col-xs-2">
					<label class="control-label">Destination</label>
					<input class="form-control" name="return_subvoyage[destination]" type="text" value="" id="return_destination" readonly />
				  </div>
				  <div class="col-xs-3 has-feedback">
					<label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
					<input class="form-control oj-time-picker oj-etd" name="return_subvoyage[ETD]" type="text" placeholder="HH:MM" id="return_etd" value="?>" readonly />
					<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				  </div>
				  <div class="col-xs-3 has-feedback">
					<label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
					<input class="form-control oj-time-picker oj-eta" name="return_subvoyage[ETA]" type="text" placeholder="HH:MM" id="return_eta" value="" readonly />
					<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				  </div>
				  <div class="col-xs-2">
					<label class="control-label">Amount</label>
					<input class="form-control price" name="return_subvoyage[amount]" type="number" value="" placeholder="Input amount" />
					<input class="form-control return_subvoyage_ids" name="return_subvoyage[id_subvoyage]" type="hidden" value="" />
					<span class="input-notes-bottom"></span>
				  </div>
				</div>
			  </div>
			</div>
		</div>
	<?php endif;?>
	</div>
	<!-- End of Return Subvoyage -->

      <h5 class="oj-form__title">Condition List</h5>
      
	  <?php foreach($conditions as $ckey => $cvalue): ?>
	   <!--Condition List-->
      <div class="form-group checkbox-fields">
        <label class="col-xs-3 control-label  text-left-impt checkbox-fields__label">
          <input type="checkbox" class="checkbox-fields__check chk_conditions" 
					name="conditions[<?=$cvalue->id_conditions ?>]" 
					value="<?=$cvalue->id_conditions ?>" 
					<?=set_checkbox('conditions['.$cvalue->id_conditions.']', $cvalue->id_conditions);?>><?=$cvalue->conditions ?>
        </label>
		
		<?php switch($ckey): ?><?php case 0: ?>
		
		<!--Age Bracket-->
		<div class="col-xs-6 form-inline">
          <div class="form-group">
            <label for="inputEmail3" class="control-label checkbox-fields__label--margin">From</label>
            <input type="number" class="form-control checkbox-fields__textbox--date" name="fare_conditions[age_bracket_from]" value="<?=set_value('fare_conditions[age_bracket_from]');?>" placeholder="00" id="age_bracket_from" min="0" />
            <label for="inputEmail3" class="control-label checkbox-fields__label--margin">To</label>
            <input type="number" class="form-control checkbox-fields__textbox--date" name="fare_conditions[age_bracket_to]" value="<?=set_value('fare_conditions[age_bracket_to]');?>" placeholder="00" id="age_bracket_to" min="1" />
          </div>
        </div>
		<!--!End Age Bracket-->
		
		<?php break ?>
		<?php case 1: ?>
		
		 <!--Selling Source-->
		<div class="col-xs-4">
          <select name="fare_conditions[outlet_id]" class="selectpicker form-control" id="outlet_id">
			 <option value="">Please Select</option>
            <?php 	foreach ($outlet as $okey => $ovalue): ?>
			<option value="<?=$ovalue->id_outlet ?>" <?=set_select('fare_conditions[outlet_id]', $ovalue->id_outlet);?>><?=$ovalue->outlet ?></option>
            <?php endforeach; ?>
          </select>
        </div>
		<!--!End Selling Source-->
		
		<?php break ?>
		<?php case 2: ?>
		
		 <!--Required Advance Booking-->
		<div class="col-xs-6 form-inline">
          <div class="form-group">
            <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">Min</label>
            <input type="number" name="fare_conditions[min_advance_booking]" min="0" value="<?=set_value('fare_conditions[min_advance_booking]');?>" class="form-control checkbox-fields__textbox " id="min_advance_booking" placeholder="Min" />
            <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">Max</label>
            <input type="number" name="fare_conditions[max_advance_booking]" min="1" value="<?=set_value('fare_conditions[max_advance_booking]');?>" class="form-control checkbox-fields__textbox " id="max_advance_booking" placeholder="Max" />
          </div>
        </div>
		<!--!End Required Advance Booking-->
	  
	  <?php break ?>
	  <?php case 3: ?>
	  
	   <!--Booking Period-->
       <div class="col-xs-9 form-inline date-from-to">
          <div class="form-group">
            <div class="has-feedback checkbox-fields__feedback">
              <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">From</label>
              <input name="fare_conditions[booking_period_from]" value="<?=set_value('fare_conditions[booking_period_from]');?>" class="form-control custom-datepicker checkbox-fields__date " type="text" id="booking_period_from" placeholder="YYYY-MM-DD" />
              <span class="glyphicon glyphicon-calendar form-control-feedback adj-right" aria-hidden="true"></span>
            </div>
            <div class="has-feedback checkbox-fields__feedback">
              <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">To</label>
              <input name="fare_conditions[booking_period_to]" value="<?=set_value('fare_conditions[booking_period_to]');?>" class="form-control custom-datepicker checkbox-fields__date " type="text" id="booking_period_to" placeholder="YYYY-MM-DD" />
              <span class="glyphicon glyphicon-calendar form-control-feedback adj-right" aria-hidden="true"></span>

            </div>
          </div>
        </div>
		<!--!End Booking Period-->
		
	  <?php break ?>
	  <?php case 4: ?>
	  
		<!--Travel Period-->
		<div class="col-xs-9 form-inline date-from-to">
          <div class="form-group">
            <div class="has-feedback checkbox-fields__feedback">
              <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">From</label>
              <input name="fare_conditions[travel_period_from]" value="<?=set_value('fare_conditions[travel_period_from]');?>" class="form-control custom-datepicker checkbox-fields__date " type="text" id="travel_period_from" placeholder="YYYY-MM-DD" />
              <span class="glyphicon glyphicon-calendar form-control-feedback adj-right" aria-hidden="true"></span>
            </div>
            <div class="has-feedback checkbox-fields__feedback">
              <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">To</label>
              <input name="fare_conditions[travel_period_to]" value="<?=set_value('fare_conditions[travel_period_to]');?>" class="form-control custom-datepicker checkbox-fields__date " type="text" id="travel_period_to" placeholder="YYYY-MM-DD" />
              <span class="glyphicon glyphicon-calendar form-control-feedback adj-right" aria-hidden="true"></span>

            </div>
          </div>
        </div>
		<!--!End Travel Period-->
	  
	  <?php break ?>
	  <?php case 5: ?>
	  
		 <!--Travel Days-->
		 <div class="col-xs-4">
          <select class="selectpicker form-control" name="fare_conditions[travel_days]" id="travel_days">
            <option value="">Please Select</option>
			<option value="1" <?=set_select('fare_conditions[travel_days]', '1');?>>Sunday</option>
			<option value="2" <?=set_select('fare_conditions[travel_days]', '2');?>>Monday</option>
			<option value="3" <?=set_select('fare_conditions[travel_days]', '3');?>>Tuesday</option>
			<option value="4" <?=set_select('fare_conditions[travel_days]', '4');?>>Wednesday</option>
			<option value="5" <?=set_select('fare_conditions[travel_days]', '5');?>>Thursday</option>
			<option value="6" <?=set_select('fare_conditions[travel_days]', '6');?>>Friday</option>
			<option value="7" <?=set_select('fare_conditions[travel_days]', '7');?>>Saturday</option>
          </select>
        </div>
		<!--!End Travel Days-->
		
		<?php break ?>
		<?php case 6: ?>
		
		<!--Maximum Leg Interval-->
		<div class="col-xs-4">
          <input type="number" name="fare_conditions[max_leg_interval]" value="<?=set_value('fare_conditions[max_leg_interval]');?>" class="form-control checkbox-fields__textbox " placeholder="Maximum Leg Interval" id="max_leg_interval" />
        </div>
		<!--!End Maximum Leg Interval-->
		
		<?php break ?>
		<?php endswitch ?>	
      </div>
      <!--!End Condition List-->
	  <?php endforeach ?>

	<!--Require ID-->
	<div class="form-group checkbox-fields">
		<label class="col-xs-3 control-label  text-left-impt checkbox-fields__label">
			<input id="require_id" name="require_id" type="hidden" value="0" />
			<input class="checkbox-fields__check chk_conditions" id="require_id" name="require_id" type="checkbox" value="1" id="require_id" <?=set_checkbox('require_id', 1);?> /> Require ID
		</label>
	</div>
	<!--!End Require ID-->

	<!--Return Fare-->
<!-- 	<div class="form-group checkbox-fields">
		<label class="col-xs-3 control-label  text-left-impt checkbox-fields__label">
			<input id="return_fare" name="return_fare" type="hidden" value="0" />
			<input class="checkbox-fields__check chk_conditions" id="return_fare" name="return_fare" type="checkbox" value="1" id="return_fare" <? //set_checkbox('return_fare', 1); ?> /> Return Fare
		</label>
	</div> -->
	<!--!End Return Fare-->
	  
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Status</label>
          <select class="selectpicker form-control" name="enabled">
			<option value="1" <?=set_select('enabled', '1');?>>Active</option>
			<option value="0" <?=set_select('enabled', '0');?>>Inactive</option>
          </select>
        </div>
      </div>

      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
            <button type="submit" class="btn oj-button">Save</button>
          </div>
        </div>
      </div>
    </form>
            
<?=$footer;?>