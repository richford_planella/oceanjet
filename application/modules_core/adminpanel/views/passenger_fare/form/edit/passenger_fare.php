<?=$header;?>
	
    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Passenger Fare Code</label>
          <input class="form-control" type="text" placeholder="Input passenger fare code" name="passenger_fare" value="<?=set_value('passenger_fare', $passenger_fare->passenger_fare);?>" disabled />
		  <span class="input-notes-bottom"><?=form_error('passenger_fare')?></span>
        </div>
        <div class="col-xs-4">
          <label class="control-label">Voyage Code</label>
          <select class="selectpicker form-control" id="voyage" name="voyage_id" disabled>
            <option value="">Please Select</option>
			<?php 	foreach ($passenger_fare->voyage as $key => $value): ?>
				<?php if($passenger_fare->voyage_id == $value->id_voyage): ?>
					<option value="<?=$value->id_voyage; ?>" selected <?=set_select('voyage_id', $value->id_voyage);?>><?=$value->voyage; ?></option>
				<?php else: ?>
					<option value="<?=$value->id_voyage; ?>" <?=set_select('voyage_id', $value->id_voyage);?>><?=$value->voyage; ?></option>
				<?php endif; ?>
			 <?php endforeach; ?>
          </select>
		  <span class="input-notes-bottom"><?=form_error('voyage_id')?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Leg Selection</label>
          <select class="selectpicker form-control" id="subvoyage" name="subvoyage" disabled>
            <option value="">Please Select</option>
			<?php 	if (!empty($subvoyage)): ?>
				<?php 	foreach ($subvoyage as $key => $value): ?>
					<?php if($passenger_fare->subvoyage_id == $value->id_subvoyage): ?>
						<option value="<?=$value->id_subvoyage; ?>" selected <?=set_select('subvoyage', $value->id_subvoyage);?>><?=$value->origin; ?></option>
					<?php else: ?>
						<option value="<?=$value->id_subvoyage; ?>" <?=set_select('subvoyage', $value->id_subvoyage);?>><?=$value->origin; ?></option>
					<?php endif; ?>
				 <?php endforeach; ?>
			 <?php endif; ?>
          </select>
		    <span class="input-notes-bottom"><?=form_error('subvoyage')?></span>
        </div>
        <div class="col-xs-2 text-right">
          <button id="add-leg-btn" class="btn oj-button oj-button--margin-top-30" type="button" disabled >Add Leg</button>
        </div>
      </div>
	 
	<div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Accommodation Type</label>
         <select class="selectpicker form-control" id="accommodation_id" name="accommodation_id" disabled>
            <option value="">Please Select</option>
			<?php foreach($passenger_fare->accommodation as $akey => $avalue): ?>
				<?php if($passenger_fare->accommodation_id == $avalue->id_accommodation): ?>
					<option value="<?=$avalue->id_accommodation ?>" selected <?=set_select('accommodation_id', $avalue->id_accommodation);?>><?="[".$avalue->accommodation_code."] ".$avalue->accommodation ?></option>
				<?php else: ?>
					<option value="<?=$avalue->id_accommodation ?>" <?=set_select('accommodation_id', $avalue->id_accommodation);?>><?="[".$avalue->accommodation_code."] ".$avalue->accommodation ?></option>
				<?php endif; ?>
			<?php endforeach; ?>
          </select>
		  <span class="input-notes-bottom"><?=form_error('accommodation_id')?></span>
        </div>
        <div class="col-xs-4">
          <label class="control-label">Rule Set</label>
          <select class="selectpicker form-control" id="rule_set_id" name="rule_set_id">
            <option value="">Please Select</option>
			<?php foreach($passenger_fare->rule_set as $rkey => $rvalue): ?>				
				<?php if($passenger_fare->rule_set_id == $rvalue->id_rule_set): ?>
					<option value="<?=$rvalue->id_rule_set ?>" selected <?=set_select('rule_set_id', $rvalue->id_rule_set);?>><?=$rvalue->rule_set ?></option>
				<?php else: ?>
					<option value="<?=$rvalue->id_rule_set ?>" <?=set_select('rule_set_id', $rvalue->id_rule_set);?>><?=$rvalue->rule_set ?></option>
				<?php endif; ?>
			<?php endforeach; ?>
          </select>
		  <span class="input-notes-bottom"><?=form_error('rule_set_id')?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Regular Fare</label>
          <select class="selectpicker form-control" id="regular_fare" name="regular_fare">
            <option value="">Please Select</option>
			<option value="1" <?=set_select('regular_fare', '1', (($passenger_fare->regular_fare == 1) ? TRUE : ''));?>>Yes</option>
			<option value="0" <?=set_select('regular_fare', '0', (($passenger_fare->regular_fare == 0) ? TRUE : ''));?>>No</option>
          </select>
		  <span class="input-notes-bottom"><?=form_error('regular_fare')?></span>
        </div>
      </div>
	  
	 <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Seat Limit</label>
          <input class="form-control" type="number" min="0" name="seat_limit" type="text" value="<?=set_value('seat_limit', $passenger_fare->seat_limit);?>" placeholder="Input seat limit" />
		  <span class="input-notes-bottom"><?=form_error('seat_limit')?></span>
        </div>
		<div class="col-xs-4">
          <label class="control-label">Points Earning</label>
          <input class="form-control" type="number" min="0" name="points_earning" type="text" value="<?=set_value('points_earning', $passenger_fare->points_earning);?>" placeholder="Input points earning" />
		  <span class="input-notes-bottom"><?=form_error('points_earning')?></span>
        </div>		
		
		<div class="col-xs-3">
           <label class="control-label">Default</label>
           <div class="checkbox ">
               <label class="checkbox-inline checkbox__label">				   
				   <input id="default_fare" name="default_fare" type="hidden" value="0">
				   <input class="checkbox-fields__check" id="default_fare" name="default_fare" type="checkbox" value="1" <?=set_checkbox('default_fare', $passenger_fare->default_fare, !empty($passenger_fare->default_fare) ? true : false);?> /> Default Fare
               </label>
           </div>
           <span class="input-notes-bottom"></span>
       </div>
	   
		<div class="col-xs-4">
			<input class="form-control leg_count" name="leg_count" type="hidden" value="" />
			<input class="form-control leg_order" name="leg_order" type="hidden" value="" />
        </div>
	  </div>
	  
	<hr class="hr-custom ">
	  
	<div id="subvoyage_details">
		<?php 	if (!empty($passenger_fare->subvoyage_fare)): ?>
			<?php 	foreach ($passenger_fare->subvoyage_fare as $skey => $svalue): ?>
				<div class="oj-leg-fare">
					<div class="form-group oj-leg">
					  <div class="col-lg-12">
						<div class="input-group">
						  <div class="oj-leg__title oj-leg__title--gray">
							Leg No. 1
						  </div>
						  <span class="input-group-btn">
						   <button class="btn btn-default oj-leg__btn" name="subvoyages[<?=$svalue->id_subvoyage?>][remove]" id="sub-<?=$svalue->id_subvoyage?>" type="button" disabled >Remove</button>
						  </span>
						</div>
					  </div>
					  <div class="col-xs-12 oj-leg__body">
						<div class="form-group">
						  <div class="col-xs-2">
							<label class="control-label">Origin</label>
							<input class="form-control" name="subvoyages[<?=$svalue->id_subvoyage?>][origin]" type="text" value="<?=$svalue->origin?>" id="origin" readonly/>
						  </div>
						  <div class="col-xs-2">
							<label class="control-label">Destination</label>
							<input class="form-control" name="subvoyages[<?=$svalue->id_subvoyage?>][destination]" type="text" value="<?=$svalue->destination?>" id="destination" readonly />
						  </div>
						  <div class="col-xs-3 has-feedback">
							<label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
							<input class="form-control oj-time-picker oj-etd" name="subvoyages[<?=$svalue->id_subvoyage?>][ETD]" type="text" placeholder="HH:MM" id="etd" value="<?=$svalue->ETD?>" readonly />
							<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
						  </div>
						  <div class="col-xs-3 has-feedback">
							<label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
							<input class="form-control oj-time-picker oj-eta" name="subvoyages[<?=$svalue->id_subvoyage?>][ETA]" type="text" placeholder="HH:MM" id="eta" value="<?=$svalue->ETA?>" readonly />
							<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
						  </div>
						  <div class="col-xs-2">
							<label class="control-label">Amount</label>
							<input class="form-control price" name="subvoyages[<?=$svalue->id_subvoyage?>][amount]" type="text" value="<?=$svalue->amount?>" placeholder="Input amount" />
							<input class="form-control subvoyage_ids" name="subvoyages[<?=$svalue->id_subvoyage?>][id_subvoyage]" type="hidden" value="<?=$svalue->id_subvoyage?>" />
							<span class="input-notes-bottom"></span>
						  </div>
						</div>
					  </div>
					</div>
				</div>
			 <?php endforeach; ?>
		 <?php endif; ?>
	</div>

      <h5 class="oj-form__title">Condition List</h5>
      
	  <?php foreach($passenger_fare->conditions as $ckey => $cvalue): ?>
	   <!--Condition List-->
      <div class="form-group checkbox-fields">
        <label class="col-xs-3 control-label  text-left-impt checkbox-fields__label">
			<?php if(isset($cvalue->id_passenger_fare_conditions)): ?>
				<input type="checkbox" class="checkbox-fields__check chk_conditions" 
					name="conditions[<?=$cvalue->conditions_id ?>]" 
					value="<?=$cvalue->conditions_id ?>" 
					<?=set_checkbox('conditions['.$cvalue->conditions_id.']', $cvalue->conditions_id);?> checked /><?=$cvalue->conditions ?>
			<?php else: ?>
				<input type="checkbox" class="checkbox-fields__check chk_conditions" 
					name="conditions[<?=$cvalue->id_conditions ?>]" 
					value="<?=$cvalue->id_conditions ?>" 
					<?=set_checkbox('conditions['.$cvalue->id_conditions.']', $cvalue->id_conditions);?> /><?=$cvalue->conditions ?>
			<?php endif; ?>
        </label>
		
		<?php switch($ckey): ?><?php case 0: ?>
		
		<!--Age Bracket-->
		<div class="col-xs-6 form-inline">
          <div class="form-group">
            <label for="inputEmail3" class="control-label checkbox-fields__label--margin">From</label>
            <input type="number" class="form-control checkbox-fields__textbox--date" name="fare_conditions[age_bracket_from]" value="<?=set_value('fare_conditions[age_bracket_from]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->age_bracket_from : NULL);?>" placeholder="00" id="age_bracket_from" min="0" />
            <label for="inputEmail3" class="control-label checkbox-fields__label--margin">To</label>
            <input type="number" class="form-control checkbox-fields__textbox--date" name="fare_conditions[age_bracket_to]" value="<?=set_value('fare_conditions[age_bracket_to]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->age_bracket_to : NULL);?>" placeholder="00" id="age_bracket_to" min="1" />
          </div>
        </div>
		<!--!End Age Bracket-->
		
		<?php break ?>
		<?php case 1: ?>
		
		 <!--Selling Source-->
		<div class="col-xs-4">
          <select name="fare_conditions[outlet_id]" class="selectpicker form-control" id="outlet_id">
			 <option value="">Please Select</option>
            <?php 	foreach ($passenger_fare->outlet as $okey => $ovalue): ?>
			<option value="<?=$ovalue->id_outlet ?>" <?=set_select('fare_conditions[outlet_id]', $ovalue->id_outlet, isset($cvalue->id_passenger_fare_conditions) ? ($cvalue->outlet_id == $ovalue->id_outlet ? true : false) : NULL);?>><?=$ovalue->outlet ?></option>
            <?php endforeach; ?>
          </select>
        </div>
		<!--!End Selling Source-->
		
		<?php break ?>
		<?php case 2: ?>
		
		 <!--Required Advance Booking-->
		<div class="col-xs-6 form-inline">
          <div class="form-group">
            <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">Min</label>
            <input type="number" name="fare_conditions[min_advance_booking]" min="0" value="<?=set_value('fare_conditions[min_advance_booking]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->min_advance_booking : NULL);?>" class="form-control checkbox-fields__textbox " id="min_advance_booking" placeholder="Min" />
            <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">Max</label>
            <input type="number" name="fare_conditions[max_advance_booking]" min="1" value="<?=set_value('fare_conditions[max_advance_booking]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->max_advance_booking : NULL);?>" class="form-control checkbox-fields__textbox " id="max_advance_booking" placeholder="Max" />
          </div>
        </div>
		<!--!End Required Advance Booking-->
	  
	  <?php break ?>
	  <?php case 3: ?>
	  
	   <!--Booking Period-->
       <div class="col-xs-9 form-inline date-from-to">
          <div class="form-group">
            <div class="has-feedback checkbox-fields__feedback">
              <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">From</label>
              <input name="fare_conditions[booking_period_from]" value="<?=set_value('fare_conditions[booking_period_from]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->booking_period_from : NULL);?>" class="form-control custom-datepicker checkbox-fields__date " type="text" id="booking_period_from" placeholder="YYYY-MM-DD" />
              <span class="glyphicon glyphicon-calendar form-control-feedback adj-right" aria-hidden="true"></span>
            </div>
            <div class="has-feedback checkbox-fields__feedback">
              <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">To</label>
              <input name="fare_conditions[booking_period_to]" value="<?=set_value('fare_conditions[booking_period_to]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->booking_period_to : NULL);?>" class="form-control custom-datepicker checkbox-fields__date " type="text" id="booking_period_to" placeholder="YYYY-MM-DD" />
              <span class="glyphicon glyphicon-calendar form-control-feedback adj-right" aria-hidden="true"></span>

            </div>
          </div>
        </div>
		<!--!End Booking Period-->
		
	  <?php break ?>
	  <?php case 4: ?>
	  
		<!--Travel Period-->
		<div class="col-xs-9 form-inline date-from-to">
          <div class="form-group">
            <div class="has-feedback checkbox-fields__feedback">
              <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">From</label>
              <input name="fare_conditions[travel_period_from]" value="<?=set_value('fare_conditions[travel_period_from]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->travel_period_from : NULL);?>" class="form-control custom-datepicker checkbox-fields__date " type="text" id="travel_period_from" placeholder="YYYY-MM-DD" />
              <span class="glyphicon glyphicon-calendar form-control-feedback adj-right" aria-hidden="true"></span>
            </div>
            <div class="has-feedback checkbox-fields__feedback">
              <label for="inputEmail3" class="control-label checkbox-fields__label--margin ">To</label>
              <input name="fare_conditions[travel_period_to]" value="<?=set_value('fare_conditions[travel_period_to]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->travel_period_to : NULL);?>" class="form-control custom-datepicker checkbox-fields__date " type="text" id="travel_period_to" placeholder="YYYY-MM-DD" />
              <span class="glyphicon glyphicon-calendar form-control-feedback adj-right" aria-hidden="true"></span>

            </div>
          </div>
        </div>
		<!--!End Travel Period-->
	  
	  <?php break ?>
	  <?php case 5: ?>
	  
		 <!--Travel Days-->
		 <div class="col-xs-4">
          <select class="selectpicker form-control" name="fare_conditions[travel_days]" id="travel_days">
            <option value="">Please Select</option>
			<option value="1" <?=set_select('fare_conditions[travel_days]', '1', isset($cvalue->travel_days) ? ($cvalue->travel_days == 1 ? true : false) : false);?>>Sunday</option>
			<option value="2" <?=set_select('fare_conditions[travel_days]', '2', isset($cvalue->travel_days) ? ($cvalue->travel_days == 2 ? true : false) : false);?>>Monday</option>
			<option value="3" <?=set_select('fare_conditions[travel_days]', '3', isset($cvalue->travel_days) ? ($cvalue->travel_days == 3 ? true : false) : false);?>>Tuesday</option>
			<option value="4" <?=set_select('fare_conditions[travel_days]', '4', isset($cvalue->travel_days) ? ($cvalue->travel_days == 4 ? true : false) : false);?>>Wednesday</option>
			<option value="5" <?=set_select('fare_conditions[travel_days]', '5', isset($cvalue->travel_days) ? ($cvalue->travel_days == 5 ? true : false) : false);?>>Thursday</option>
			<option value="6" <?=set_select('fare_conditions[travel_days]', '6', isset($cvalue->travel_days) ? ($cvalue->travel_days == 6 ? true : false) : false);?>>Friday</option>
			<option value="7" <?=set_select('fare_conditions[travel_days]', '7', isset($cvalue->travel_days) ? ($cvalue->travel_days == 7 ? true : false) : false);?>>Saturday</option>
          </select>
        </div>
		<!--!End Travel Days-->
		
		<?php break ?>
		<?php case 6: ?>
		
		<!--Maximum Leg Interval-->
		<div class="col-xs-4">
          <input type="number" name="fare_conditions[max_leg_interval]" value="<?=set_value('fare_conditions[max_leg_interval]', isset($cvalue->id_passenger_fare_conditions) ? $cvalue->max_leg_interval : NULL);?>" class="form-control checkbox-fields__textbox " placeholder="Maximum Leg Interval" id="max_leg_interval" />
        </div>
		<!--!End Maximum Leg Interval-->
		
		<?php break ?>
		<?php endswitch ?>	
      </div>
      <!--!End Condition List-->
	  <?php endforeach ?>

	<!--Require ID-->
	<div class="form-group checkbox-fields">
		<label class="col-xs-3 control-label  text-left-impt checkbox-fields__label">
			<input id="require_id" name="require_id" type="hidden" value="0" />
			<input class="checkbox-fields__check chk_conditions" id="require_id" name="require_id" type="checkbox" value="1" id="require_id" <?=set_checkbox('require_id', 1);?> <?= ($passenger_fare->require_id == 1) ? 'checked' : FALSE ?> /> Require ID
		</label>
	</div>
	<!--!End Require ID-->

	<!--Return Fare-->
	<div class="form-group checkbox-fields">
		<label class="col-xs-3 control-label  text-left-impt checkbox-fields__label">
			<input id="return_fare" name="return_fare" type="hidden" value="0" />
			<input class="checkbox-fields__check chk_conditions" id="return_fare" name="return_fare" type="checkbox" value="1" id="return_fare" <?=set_checkbox('return_fare', 1);?> <?= ($passenger_fare->return_fare == 1) ? 'checked' : FALSE ?> /> Return Fare
		</label>
	</div>
	<!--!End Return Fare-->
	  
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Status</label>
          <select class="selectpicker form-control" name="enabled">
			<option value="1" <?=set_select('enabled', '1',(($passenger_fare->enabled) ? TRUE : ''));?>>Active</option>
			<option value="0" <?=set_select('enabled', '0',((!$passenger_fare->enabled) ? TRUE : ''));?>>Inactive</option>
          </select>
        </div>
      </div>

      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
            <button type="submit" class="btn oj-button">Save</button>
          </div>
        </div>
      </div>
    </form>
            
<?=$footer;?>