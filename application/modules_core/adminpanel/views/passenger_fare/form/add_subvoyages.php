<?php  if(!empty($subvoyages)): ?>
<?php 	foreach ($subvoyages as $skey => $svalue): ?>
	<div class="oj-leg-fare">
		<div class="form-group oj-leg">
		  <div class="col-lg-12">
			<div class="input-group">
			  <div class="oj-leg__title oj-leg__title--gray">
				Leg No. 1
			  </div>
			  <span class="input-group-btn">
			   <button class="btn btn-default oj-leg__btn" name="subvoyages[<?=$svalue->id_subvoyage?>][remove]" id="sub-<?=$svalue->id_subvoyage?>" type="button">Remove</button>
			  </span>
			</div>
		  </div>
		  <div class="col-xs-12 oj-leg__body">
			<div class="form-group">
			  <div class="col-xs-2">
				<label class="control-label">Origin</label>
				<input class="form-control" name="subvoyages[<?=$svalue->id_subvoyage?>][origin]" type="text" value="<?=$svalue->origin?>" id="origin" readonly/>
			  </div>
			  <div class="col-xs-2">
				<label class="control-label">Destination</label>
				<input class="form-control" name="subvoyages[<?=$svalue->id_subvoyage?>][destination]" type="text" value="<?=$svalue->destination?>" id="destination" readonly />
			  </div>
			  <div class="col-xs-3 has-feedback">
				<label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
				<input class="form-control oj-time-picker oj-etd" name="subvoyages[<?=$svalue->id_subvoyage?>][ETD]" type="text" placeholder="HH:MM" id="etd" value="<?=$svalue->ETD?>" readonly />
				<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
			  </div>
			  <div class="col-xs-3 has-feedback">
				<label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
				<input class="form-control oj-time-picker oj-eta" name="subvoyages[<?=$svalue->id_subvoyage?>][ETA]" type="text" placeholder="HH:MM" id="eta" value="<?=$svalue->ETA?>" readonly />
				<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
			  </div>
			  <div class="col-xs-2">
				<label class="control-label">Amount</label>
				<input class="form-control price" name="subvoyages[<?=$svalue->id_subvoyage?>][amount]" type="number" value="" placeholder="Input amount" />
				<input class="form-control subvoyage_ids" name="subvoyages[<?=$svalue->id_subvoyage?>][id_subvoyage]" type="hidden" value="<?=$svalue->id_subvoyage?>" />
				<span class="input-notes-bottom"></span>
			  </div>
			</div>
		  </div>
		</div>
	</div>
<?php endforeach; ?>
<?php endif; ?>