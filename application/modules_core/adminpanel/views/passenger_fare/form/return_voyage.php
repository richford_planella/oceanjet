<!-- Return voyage details -->
<div id="subvoyage_return_details">
	<div class="oj-leg-fares">
		<div class="form-group oj-legs" style="background-color: #333;padding: 8px;color: #FFF;">
		  <div class="col-lg-12">
			<div class="input-group">
			  <div class="oj-leg__title oj-leg__title--gray">
				Return Voyage
			  </div>
			</div>
		  </div>
		  <div class="col-xs-12 oj-leg__body">
		  	<div class="form-group">
		  	  <div class="col-xs-4">
				<label class="control-label">Return Voyages</label>
				 <select class="selectpicker form-control" id="return_voyage" name="return_voyage_id">
		            <option value="">Please Select</option>
					<?php 	foreach ($return_voyage as $key => $value): ?>
						<option value="<?=$value->voyage_id; ?>" <?=set_select('return_voyage_id', $value->voyage_id);?>><?=$value->voyage." | ".$value->origin." - ".$value->destination; ?></option>
					 <?php endforeach; ?>
		          </select>
			  </div>
		  	</div>
		  </div>
		  <div class="col-xs-12 oj-leg__body">
			<div class="form-group">
			  <div class="col-xs-2">
				<label class="control-label">Origin</label>
				<input class="form-control" name="return_subvoyage[origin]" type="text" value="" id="return_origin" readonly />
			  </div>
			  <div class="col-xs-2">
				<label class="control-label">Destination</label>
				<input class="form-control" name="return_subvoyage[destination]" type="text" value="" id="return_destination" readonly />
			  </div>
			  <div class="col-xs-3 has-feedback">
				<label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
				<input class="form-control oj-time-picker oj-etd" name="return_subvoyage[ETD]" type="text" placeholder="HH:MM" id="return_etd" value="" readonly />
				<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
			  </div>
			  <div class="col-xs-3 has-feedback">
				<label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
				<input class="form-control oj-time-picker oj-eta" name="return_subvoyage[ETA]" type="text" placeholder="HH:MM" id="return_eta" value="" readonly />
				<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
			  </div>
			  <div class="col-xs-2">
				<label class="control-label">Amount</label>
				<input class="form-control price" name="return_subvoyage[amount]" type="number" value="" placeholder="Input amount" />
				<input class="form-control return_subvoyage_ids" name="return_subvoyage[id_subvoyage]" type="hidden" value="" />
				<span class="input-notes-bottom"></span>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div>
<!-- End of Return Subvoyage -->