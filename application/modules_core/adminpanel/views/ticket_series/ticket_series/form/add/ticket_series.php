<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
	<div class="form-group">
		<div class="col-xs-3">
			<label class="control-label">Ticket Set Starts From</label>
			<input class="form-control" type="text" placeholder="Input Ticket Series Start From" value="<?=$prev_generated_ticket;?>" name="ticket_starts_from" readonly/>
		</div>
		<div class="col-xs-3">
			<label class="control-label">No. of Tickets</label>
			<input class="form-control" type="text" placeholder="Input number of tickets to generate" value="<?=set_value('tickets_to_generate');?>" name="no_tickets" onkeypress="numeric_validation(event)"/>
			<span class="input-notes-bottom"><?=form_error('no_tickets');?></span>
            <input type="hidden" name="enabled" value="1">
		</div>
	</div>
	<div class="form-group oj-form-footer">
		<div class="col-xs-12">
			<div class="btn-oj-group right">
				<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
				<button type="submit" class="btn oj-button">Save</button>
			</div>
		</div>
	</div>
</form>
<?=$footer;?>