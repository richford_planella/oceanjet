<?= $header; ?>

<form class="form-horizontal oj-form" action="<?= current_url(); ?>" method="POST">
    <?=$this->load->view(admin_dir('notification'));?>
    <div class="form-group">
        <div class="col-xs-5">
            <label class="control-label">Ticket Series Starts From</label>
            <input readonly="readonly" class="form-control"
                   id="ticket_starts_from" name="ticket_starts_from" type="text"
                   value="<?= set_value('ticket_starts_from', $ticket_series->ticket_starts_from); ?>">
        </div>
        <div class="col-xs-5">
            <label class="control-label">Ticket Series Ends To</label>
            <input readonly="readonly" class="form-control"
                   id="ticket_ends_to" name="ticket_ends_to" type="text"
                   value="<?= set_value('ticket_ends_to', $ticket_series->ticket_ends_to); ?>">
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-5">
            <label class="control-label">Number of Tickets</label>
            <input readonly="readonly" class="form-control"
                   id="no_tickets" name="no_tickets" type="text"
                   value="<?= set_value('no_tickets', $ticket_series->no_tickets); ?>">
        </div>
        <div class="col-xs-5">

        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">

            </div>
        </div>
    </div>
</form>

<div class="ojpage-header">
    <h1 class="pheader">Ticket Assignment Form</h1>
</div>
<form class="form-horizontal oj-form" action="<?= current_url(); ?>" method="POST">
    <div class="form-group">
        <div class="col-xs-5">
            <label class="control-label">Available Tickets</label>
            <input type="hidden" name="ticket_series_id" value="<?= $ticket_series_id; ?>">
            <select id="limit_ticket" name="limit_ticket" class="selectpicker form-control" >
                <option value="">Please select</option>
                <?php
                if ($available_tickets == 0): ?>
                    <option value='0' <?= set_select('limit_ticket', '0'); ?>>0</option>
                <?php endif;

                for ($ctr = 1; $ctr <= $available_tickets; $ctr++):?>
                    <option
                        value='<?= $ctr; ?>' <?= set_select('limit_ticket', $ctr); ?>><?= $ctr; ?></option>
                <?php endfor; ?>

            </select>
            <span class="input-notes-bottom"><?=form_error('limit_ticket');?></span>
        </div>
        <div class="col-xs-5">
            <label class="control-label">Sales Agents</label>
            <select id="sales_agent_id" name="sales_agent_id" class="selectpicker form-control" >
                <option value="">Please select</option>
                <?php $sales_agents;
                foreach ($sales_agents as $sales_agent): ?>
                    <option
                        value='<?= $sales_agent->id_user_account; ?>' <?= set_select('sales_agent_id', $sales_agent->id_user_account); ?>><?= $sales_agent->firstname . " " . $sales_agent->middlename . " " . $sales_agent->lastname; ?></option>
                <?php endforeach; ?>
            </select>
            <span class="input-notes-bottom"><?=form_error('sales_agent_id');?></span>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?= admin_url($this->classname); ?>" class="btn oj-button gray-button">Cancel</a>
                <button type="submit" class="btn oj-button" name="submit" value="Assign Tickets">Assign</button>
            </div>
        </div>
    </div>
</form>

<div class="ojpage-header">
    <h1 class="pheader">Ticket Assignment Table</h1>
</div>
<!-- /Table -->
<table id="main_table" class="table global-table nopadding">
    <thead>
    <tr>
        <th> Assigned Tickets</th>
        <th> Sales Agent</th>
        <th> Issued Ticket</th>
        <th> Not Issued</th>
        <th> Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($assigned_tickets_per_agents as $assigned_tickets_per_agent): ?>
        <tr>
            <td class="align-center"> <?= $assigned_tickets_per_agent->assigned_tickets; ?> </td>
            <td class="center">       <?= $assigned_tickets_per_agent->agent_name; ?>   </td>
            <td class="align-center"> <?= $assigned_tickets_per_agent->issued; ?>     </td>
            <td class="align-center"> <?= $assigned_tickets_per_agent->assigned_tickets - $assigned_tickets_per_agent->issued; ?>    </td>
            <td class="align-center">
                <?php if (($assigned_tickets_per_agent->assigned_tickets - $assigned_tickets_per_agent->issued) > 0): ?>
                    <button type="button"
                            data-available-tickets="<?= $assigned_tickets_per_agent->assigned_tickets - $assigned_tickets_per_agent->issued; ?>"
                            data-user-id="<?= $assigned_tickets_per_agent->user_id ?>"
                            class="btn oj-button transfer-button">Transfer Tickets
                    </button>
                <?php else: ?>
                    No Availble options
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>


<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h4 class="modal-title" id="myModalLabel">Transfer Not Issued Tickets </h4>
            </div>
            <form class="form-horizontal" action="<?= current_url(); ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <label class="control-label">Available Tickets</label>
                            <input type="hidden" name="ticket_series_id" value="<?= $ticket_series_id; ?>">
                            <input type="hidden" id="assigned_sales_agent" name="assigned_sales_agent">
                            <select id="available_tickets_select_drop_down" name="limit_ticket" class="form-control" title="Choose"></select>
                        </div>
                        <div class="col-xs-6">
                            <label class="control-label">Sales Agents</label>
                            <select id="available_sales_agents_select_drop_down" name="sales_agent_id" class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer btn-oj-group center">
                    <button type="button" class="btn oj-button" data-dismiss="modal">Cancel </button>
                    <button type="submit" class="btn oj-button" name="submit" value="Assign Tickets">Transfer
                        Tickets
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    var available_agents = <?= json_encode($sales_agents); ?>;
</script>
<?= $footer; ?>