<?=$header;?>
<div class="row table-header-custom nopadding">
    <div class="col-xs-4">
        <div class="input-group">
            <input type="text" class="form-control" id="column3_search" placeholder="Search">
        </div>
    </div>
    <div class="col-xs-8 text-right">
        <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
    </div>
</div>
<?=$this->load->view(admin_dir('notification'));?>
<!-- /Table -->
<table id="main_table" class="table global-table nopadding">
	<thead>
	<tr>
		<th>Ticket Range</th>
		<th>No. of Tickets</th>
		<th>Issued Tickets</th>
		<th>Not Issued</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
	<?php if(!empty($ticket_series)):?>
		<?php foreach($ticket_series as $u):?>
			<tr>
				<td><?=$u->ticket_starts_from." - ".$u->ticket_ends_to;?></td>
				<td><?=$u->no_tickets;?></td>
				<td><?=$u->issued;?></td>
				<td><?=$u->no_tickets - $u->issued;?></td>
				<td>
                    <a href="<?=admin_url($this->classname, 'view', $u->id_ticket_series);?>" title="View" data-rel="tooltip">View
                    </a>
					<a href="<?=admin_url($this->classname, 'edit', $u->id_ticket_series);?>" title="Edit" data-rel="tooltip">Edit
					</a>
				</td>
			</tr>
		<?php endforeach;?>
	<?php endif;?>
	</tbody>
</table>

<?=$footer;?>