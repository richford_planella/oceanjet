<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- <meta name="viewport" content="width=800"> -->
    <!--  <meta name="viewport" content="min-width=800", initial-scale=1.0>-->
    <!--  <meta name="viewport" content="width=device-width, initial-scale=1.0">-->

    <title><?=$this->configuration->getValue('value', array('configuration' => 'SITE_TITLE'));?></title>

    <link rel="stylesheet" href="<?=css_dir('admin/app.css');?>">

    <!--[if lt IE 9]>
        <script src="<?=js_dir('html5shiv.min.js');?>"></script>
        <script src="<?=js_dir('respond.min.js');?>"></script>
    <![endif]-->
    
    <?php if (isset($css_files) && ! empty($css_files)):?>
        <?php foreach($css_files as $css):?>
                <link rel="stylesheet" type="text/css" href="<?=$css;?>"/>
        <?php endforeach;?>
    <?php endif;?>

    <!-- start: Favicon -->
    <link rel="shortcut icon" href="<?=img_dir('favicon.png');?>">
    <!-- end: Favicon -->        
</head>
<body>
  <section class="login-page">
    <div class="login-form">
    <div class="login-input-container">
    <div class="login-title"><img src="<?=img_dir('logo.png');?>" alt="Ocean Jet Logo" />
    <h1><?=$this->configuration->getValue('value', array('configuration' => 'SITE_TITLE'));?></h1></div>