<?=$header;?>		
        <form action="" method="post">
            <?=$this->load->view(admin_dir('notification'));?>
            <div class="input-container row clearfix">
                <label for="cur_password">Old Password</label>
                <span class="error-message"><?php echo form_error('cur_password'); ?></span>
                <input type="password" class="login-input " id="cur_password" name="cur_password" />
            </div>
            <ul class="message-list">
                <li>The new password shall have at least 5 characters.</li>
                <li>The new password shall consist of at least a single uppercase letter.</li>
                <li>The new password shall consist of at least a single lower case letter.</li>
                <li>The new password shall consist of at least a single special character from the following list: ! @ # $ % ^ & * ( ).</li>
                <li>The new password shall have no spaces or indents</li>
            </ul>
            <div class="input-container row clearfix">
                <label for="password">New Password</label>
                <span class="error-message"><?php echo form_error('password'); ?></span>
                <input type="password" class="login-input" id="password" name="password" />
            </div>
            <div class="input-container row clearfix">
                <label for="re_password">Confirm Password</label>
                <span class="error-message"><?php echo form_error('re_password'); ?></span>
                <input type="password" class="login-input" id="re_password" name="re_password" />
            </div>
            <div class="input-container row clearfix login-btn-group text-right">
                <a class="btn oj-button light-gray-button" href="<?=admin_url('logout');?>" role="button">Back</a>
                <input class="btn oj-button orange-button " type="submit" value="Confirm">
            </div>
        </form>
        <!--End your Login Forms component-->
<?=$footer;?>