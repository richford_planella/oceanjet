<?=$header;?>
	<?=$this->load->view(admin_dir('notification'));?>
	<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
		<div class="form-group">
			<div class="col-xs-3">
				<label class="control-label" for="commission_code">E-Ticket No.</label>
				<input  class="form-control" id="eticket_no" name="eticket_no" type="text" value="">
			</div>
			<div class="col-xs-3">
				<label class="control-label" for="commission_code">Lastname</label>
				<input  class="form-control" id="last_name" name="last_name" type="text" value="">
			</div>
			<div class="col-xs-3">
  				<div class="btn-oj-group oj-button--margin-top-30">
					<a href="#" id="abc" class="btn oj-button" >Search</button>
					<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Clear</a>
				</div>
			</div>
		</div>
		<div class="form-group form-group__custom">
		<div class="col-xs-12">
			<div class="inner-header">
            	<h3 class="inner-header__title">E-Ticket Details</h3>
          	</div>
          	<div class="form-group">
          		<div class="col-xs-6">
					<label class="control-label" for="commission_code">Name</label>			
					<input readonly="readonly" class="form-control" id="fullname" name="full_name" type="text" value="">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Voyage No.</label>
					<input readonly="readonly" class="form-control" id="voyage_no" name="voyage_no" type="text" value="">
				</div>
				<div class="col-xs-3">
					<label class="control-label">Origin</label>
					<input readonly="readonly" class="form-control" id="origin" name="origin" type="text" value="">
				</div>
				<div class="col-xs-3">		
					<label class="control-label">Destination</label>
					<input readonly="readonly" class="form-control" id="destination" name="destination" type="text" value="">	
				</div>
				<div class="col-xs-3 has-feedback">
					<label class="control-label">Departure Date</label>
					<input readonly="readonly" class="form-control" placeholder="MM/DD/YYYY" id="departure_date" name="departure_date" type="text" value="">				
					<span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
				</div>
          	</div>
          	<div class="form-group">
          		<div class="col-xs-3 has-feedback">
					<label class="control-label" for="commission_percentage">Departure Time (ETD)</label>
					<input readonly="readonly" class="form-control" id="etd" name="ETD" type="text" value="" >
					<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Arrival Time (ETA)</label>
					<input readonly="readonly" class="form-control" id="eta" name="ETA" type="text" value="" >
					<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Vessel</label>
					<input readonly="readonly" class="form-control" id="vessel" name="vessel" type="text" value="" >
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Accommodation Type</label>
					<input readonly="readonly" class="form-control" id="accommodation_type" name="accommodation_type" type="text" value="" >				
				</div>
          	</div>

          		<div class="col-xs-3">
					<label class="control-label" for="outlet_code">Voyage Status</label>
					<input class="form-control" placeholder="Input Nationality" id="nationality" name="nationality" type="text" value="<?=set_value('nationality');?>">
				</div>
          	</div>
		</div>
		         	<div class="col-xs-12">
                            <hr class="hr-custom">
                          </div>

          	<div class="form-group">
		 <div class="col-xs-12">
                            <div class="col-xs-12">
                              <label class="control-label">Reason to Void</label>
                              <span class="input-notes-top">*Note: your note</span>
                              <textarea class="form-control" rows="5" placeholder="No content available" name="void_reason"><?=set_value('void_reason');?></textarea>
                              <span class="input-notes-bottom"><?=form_error('void_reason')?></span>
                            </div>
                          </div>
                        </div>
                        
                        
                        
                        <div class="form-group oj-form-footer">
                          <div class="col-xs-12">
                            <div class="btn-oj-group right">
                              <button type="submit" class="btn oj-button">Void</button>
                            </div>
                          </div>
                        </div>

	
    <input type="hidden" id="booking_id" name="booking_id" value=""/>
	</form>

	
<?=$footer;?>