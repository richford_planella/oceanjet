<?=$header;?>
	<form class="form-horizontal" action="<?=current_url();?>" method="POST">		    
             <div class="row-fluid ">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>E-Ticket Details</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="commission_code">Name</label>
								<div class="controls">
								  <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$void_eticket['fullname'];?>" >
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission">Voyage No.</label>
								<div class="controls">
									 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$void_eticket['voyage'];?>" >
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission_percentage">Origin</label>
								<div class="controls">
									<?php foreach ($port as $origin) {?>
										<?php if ($origin->id_port == $void_eticket['voyage_origin']) {?>
									 		<input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$origin->port;?>">
										<?php } ?>
									<?php } ?>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission_percentage">Destination</label>
								<div class="controls">
									<?php foreach ($port as $destination) {?>
										<?php if ($destination->id_port == $void_eticket['voyage_destination']) {?>
									 		<input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$destination->port;?>">
										<?php } ?>
									<?php } ?>
								</div>
							</div>
							
							<div class="control-group">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<td>Origin</td>
											<td>Destination</td>
											<td>ETA</td>
											<td>ETD</td>
											<td>Vessel</td>
											<td>Accommodation Type</td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($void_eticket['subvoyage'] as $subvoyages) {?>
											<tr>
												<td><?=$subvoyages->origin_name;?></td>
												<td><?=$subvoyages->destination_name;?></td>
												<td><?=$subvoyages->ETA;?></td>
												<td><?=$subvoyages->ETD;?></td>
												<td><?=$subvoyages->vessel;?></td>
												<td><?=$subvoyages->accommodation;?></td>
											<tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<div class="control-group">
								  <label class="control-label" for="enabled">Reason to Void</label>
								  <div class="controls">	  	
								  		<input  readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="void_reason" name="void_reason" type="text" value="<?=$void_eticket['void_reason'];?>">
								  </div>
							</div> 
							  <div class="form-actions">
								<a href="<?=admin_url($this->classname);?>" class="btn">Return to List</a>
							</div>
															
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->         
	</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>