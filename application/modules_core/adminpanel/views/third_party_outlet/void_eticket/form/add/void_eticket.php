<?=$header;?>
	<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-4">
					<label class="control-label" for="commission_code">E-Ticket No.</label>
					<input  class="form-control" id="eticket_no" name="debtor_code" type="text" value="">
				</div>
				<div class="col-xs-4">
					<label class="control-label" for="commission_code">Lastname</label>
					<input  class="form-control" id="last_name" name="debtor_code" type="text" value="">
				</div>

					<div class="col-xs-4">
						<div class="col-xs-12"> 
	      				<div class="btn-oj-group">
							<a href="#" id="abc" class="btn oj-button" >Search</button>
							<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Clear</a>
						</div>
					</div>
					</div>

			 </div>
		</div>
		<div class="row">
			<div class="col-xs-6">
        		<h4>E-Ticket Details</h4>
        	</div>
		</div>	    
        <div class="row">

        	<div class="col-xs-6">
        		<div class="col-xs-6">
    				<label class="control-label" for="commission_code">Name</label>			
					<input readonly="readonly" class="form-control" id="fullname" name="full_name" type="text" value="">
        		</div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-xs-12">
        		<div class="col-xs-3">
        			<label class="control-label" for="commission">Voyage No.</label>
					<input readonly="readonly" class="form-control" id="voyage_no" name="voyage_no" type="text" value="">
        		</div>
        		<div class="col-xs-3">
        			<label class="control-label" for="commission_percentage">Origin</label>
					<input readonly="readonly" class="form-control" id="origin" name="origin" type="text" value="">
        		</div>
        		<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Destination</label>
					<input readonly="readonly" class="form-control" id="destination" name="destination" type="text" value="">				
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Departure Date</label>
					<input readonly="readonly" class="form-control" id="departure_date" name="departure_date" type="text" value="">				
				</div>
        	</div>
       </div>
       <div class="row">
       		<div class="col-xs-12">
       			<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Departure Time (ETD)</label>
					<input readonly="readonly" class="form-control" id="etd" name="ETD" type="text" value="" >
				</div>
       			<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Arrival Time (ETA)</label>
					<input readonly="readonly" class="form-control" id="eta" name="ETA" type="text" value="" >
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Vessel</label>
					<input readonly="readonly" class="form-control" id="vessel" name="vessel" type="text" value="" >
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Accommodation Type</label>
					<input readonly="readonly" class="form-control" id="accommodation_type" name="accommodation_type" type="text" value="" >
				</div>
       		</div>
       </div>
       <div class="row">
       		<div class="col-xs-12">
       			<div class="col-xs-3">
			  		<label class="control-label" for="enabled">Reason to Void</label>				  	
					<input  class="form-control" id="void_reason" name="void_reason" type="text" value="<?=set_value('void_reason');?>">					  	
				</div>
			</div>
       </div>
       <input type="hidden" id="booking_id" name="booking_id" value=""/>
		<div class="row">
	    	<div class="form-group oj-form-footer">
	      		<div class="col-xs-12">
	        		<div class="btn-oj-group right">
	          			<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
	          			<button id="void-btn" type="submit" class="btn oj-button">Void</button>
	          		</div>
	        	</div>
	      	</div>
      	</div>
	</form>

	
<?=$footer;?>