<?=$header;?>
	<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
		<div class="form-group">
			<div class="col-xs-3">
				<label class="control-label" for="commission_code">E-Ticket No.</label>
				<input  class="form-control" id="eticket_no" name="debtor_code" type="text" value="">
			</div>
			<div class="col-xs-3">
				<label class="control-label" for="commission_code">Lastname</label>
				<input  class="form-control" id="last_name" name="debtor_code" type="text" value="">
			</div>

			<div class="col-xs-3">
  				<div class="btn-oj-group oj-button--margin-top-30">
					<a href="#" id="abc" class="btn oj-button" >Search</button>
					<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Clear</a>
				</div>
			</div>
		</div>
		<div class="form-group form-group__custom">
			<div class="col-xs-12">
				<div class="inner-header">
					<h3 class="inner-header__title">E-Ticket Details</h3>
				</div>
        	   
        	<div class="form-group">
        		<div class="col-xs-6">
    				<label class="control-label" for="commission_code">Passenger's Name</label>			
					<input readonly="readonly" class="form-control" id="fullname" name="full_name" type="text" value="<?=set_value('full_name');?>">
        		</div>
        	</div>
        	<div class="form-group">
        		<div class="col-xs-3">
        			<label class="control-label" for="commission">Voyage No.</label>
					<input readonly="readonly" class="form-control" id="voyage_no" name="voyage_no" type="text" value="<?=set_value('voyage_no');?>">
        		</div>
        		<div class="col-xs-3">
        			<label class="control-label" for="commission_percentage">Origin</label>
					<input readonly="readonly" class="form-control" id="origin" name="origin" type="text" value="<?=set_value('origin');?>">
        		</div>
        		<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Destination</label>
					<input readonly="readonly" class="form-control" id="destination" name="destination" type="text" value="<?=set_value('destination');?>">				
				</div>
				<div class="col-xs-3 has-feedback">
					<label class="control-label">Departure Date</label>
					<input readonly="readonly" class="form-control" id="departure_date" name="departure_date" type="text" value="<?=set_value('vessel');?>">	
					<span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>			
				</div>
        	</div>
       
       		<div class="form-group">
       			<div class="col-xs-3 has-feedback">
					<label class="control-label" for="commission_percentage">Departure Time (ETD)</label>
					<input readonly="readonly" class="form-control" id="etd" name="ETD" type="text" value="<?=set_value('ETD');?>" >
					 <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				</div>
       			<div class="col-xs-3 has-feedback">
					<label class="control-label" for="commission_percentage">Arrival Time (ETA)</label>
					<input readonly="readonly" class="form-control" id="eta" name="ETA" type="text" value="<?=set_value('ETA');?>" >
					 <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Vessel</label>
					<input readonly="readonly" class="form-control" id="vessel" name="vessel" type="text" value="<?=set_value('vessel');?>" >
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Accommodation Type</label>
					<input readonly="readonly" class="form-control" id="accommodation_type" name="accommodation_type" type="text" value="<?=set_value('accommodation_type');?>" >
				</div>
       		</div>
       
      
       		<div class="form-group">
       			<div class="col-xs-3">
			  		<label class="control-label" for="enabled">Voyage Status</label>				  	
					<input readonly="readonly" class="form-control" id="voyage_status" name="voyage_status" type="text" value="<?=set_value('voyage_status');?>">					  	
				</div>
				<div class="col-xs-3">
			  		<label class="control-label" for="enabled">Ticket Amount</label>				  	
					<input readonly="readonly" class="form-control" id="ticket_amount" name="ticket_amount" type="text" value="<?=set_value('ticket_amount');?>">					  	
				</div>
			</div>
       </div>	 
        <div class="col-xs-12">
          <hr class="hr-custom">
        </div>
       
       		<div class="col-xs-12">
       			<div class="form-group">
       			<div class="col-xs-3">
			  		<label class="control-label" for="enabled">Rule Code</label>				  	
					<input readonly="readonly" class="form-control" id="rule-code" name="rule_code" type="text" value="<?=set_value('rule_code');?>">					  	
				</div>
				<div class="col-xs-3">
			  		<label class="control-label" for="enabled">Refund Charge</label>				  	
					<input readonly="readonly" class="form-control" id="refund-charge" name="refund_charge" type="text" value="<?=set_value('refund_charge');?>">					  	
				</div>
				<div class="col-xs-3">
			  		<label class="control-label" for="enabled">Refund Amount</label>				  	
					<input readonly="readonly" class="form-control" id="refund-amount" name="refund_amount" type="text" value="<?=set_value('refund_amount');?>">					  	
				</div>
				</div>
			</div>
       
       <input type="hidden" id="booking_id" name="booking_id" value=""/>
		
	    	<div class="form-group oj-form-footer">
	      		<div class="col-xs-12">
	        		<div class="btn-oj-group right">
	          			<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
	          			<button id="request-refund" type="submit" class="btn oj-button">Request Refund</button>
	          		</div>
	        	</div>
	      	</div>
      
	</form>

	
<?=$footer;?>