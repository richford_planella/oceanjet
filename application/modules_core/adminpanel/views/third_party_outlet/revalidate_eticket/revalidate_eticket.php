<?=$header;?>
	<?=$this->load->view(admin_dir('notification'));?>
	<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
		<div class="form-group">
			<div class="col-xs-3">
				<label class="control-label" for="commission_code">E-Ticket No.</label>
				<input  class="form-control" id="eticket_no" name="eticket_no" type="text" value="">
			</div>
			<div class="col-xs-3">
				<label class="control-label" for="commission_code">Lastname</label>
				<input  class="form-control" id="last_name" name="last_name" type="text" value="">
			</div>
			<div class="col-xs-3">
  				<div class="btn-oj-group oj-button--margin-top-30">
					<a href="#" id="abc" class="btn oj-button" >Search</button>
					<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Clear</a>
				</div>
			</div>
		</div>
		<div class="form-group form-group__custom">
		<div class="col-xs-12">
			<div class="inner-header">
            	<h3 class="inner-header__title">E-Ticket Details</h3>
          	</div>
          	<div class="form-group">
          		<div class="col-xs-6">
					<label class="control-label" for="commission_code">Passenger's Name</label>			
					<input readonly="readonly" class="form-control" id="fullname" name="full_name" type="text" value="">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Voyage No.</label>
					<input readonly="readonly" class="form-control" id="voyage_no" name="voyage_no" type="text" value="">
				</div>
				<div class="col-xs-3">
					<label class="control-label">Origin</label>
					<input readonly="readonly" class="form-control" id="origin" name="origin" type="text" value="">
					<input id="origin-id" type="hidden" name="origin_id" value="">
				</div>
				<div class="col-xs-3">		
					<label class="control-label">Destination</label>
					<input readonly="readonly" class="form-control" id="destination" name="destination" type="text" value="">
					<input id="destination-id" type="hidden" name="destination_id" value="">	
				</div>
			</div>
			<div class="form-group">
				
          		<div class="col-xs-3 has-feedback">
					<label class="control-label" for="commission_percentage">ETD <small>(Estimated time of Departure)</small></label>
					<input readonly="readonly" class="form-control" id="etd" name="ETD" type="text" value="" >
					<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				</div>
				<div class="col-xs-3 has-feedback">
					<label class="control-label" for="commission_percentage">ETA <small>(Estimated time of Arrival)</small></label>
					<input readonly="readonly" class="form-control" id="eta" name="ETA" type="text" value="" >
					<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Vessel</label>
					<input readonly="readonly" class="form-control" id="vessel" name="vessel" type="text" value="" >
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Accommodation Type</label>
					<input readonly="readonly" class="form-control" id="accommodation_type" name="accommodation_type" type="text" value="" >				
				</div>
          		<div class="col-xs-3">
					<label class="control-label" for="outlet_code">Voyage Status</label>
					<input class="form-control" placeholder="Input Nationality" id="nationality" name="nationality" type="text" value="<?=set_value('nationality');?>">
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="outlet_code">Ticket Amount</label>
					<input class="form-control" placeholder="Input Nationality" id="nationality" name="nationality" type="text" value="<?=set_value('nationality');?>">
				</div>
          	</div>
		</div>
		         	<div class="col-xs-12">
                            <hr class="hr-custom">
                          </div>

         <div class="col-xs-12">
          <div class="form-group">


            <div class="col-xs-5">
              <label class="control-label">Voyage Code</label>
              <select id="upgrade-departure" class="selectpicker form-control" title="Choose">
                
              </select>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Upgrade Accommodation Type</label>
              <select class="selectpicker form-control" title="Choose">
              		<?php foreach ($accommodation as $a) { ?>
					<option value="<?=$a->id_accommodation;?>"><?=$a->accommodation;?></option>
					<?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="form-group">

            <div class="col-xs-3">
              <label class="control-label">Rule Code</label>
              <input class="form-control" type="text" value="" disabled/>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Re-schedule Charge</label>
              <input class="form-control" placeholder="0.00" type="text" value="" disabled/>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Upgrade Charge</label>
              <input class="form-control" placeholder="0.00"  type="text" value="" disabled/>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Total Charge</label>
              <input class="form-control" placeholder="0.00"  type="text" value="" disabled/>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
            <button type="submit" class="btn oj-button">Revalidate</button>
          </div>
        </div>
      </div>

	
    <input type="hidden" id="booking-id" name="booking_id" value=""/>
    <input type="hidden" id="passenger-id" name="passenger_id" value=""/>
    <input type="hidden" id="discount-id" name="discount_id" value=""/>
    <input type="hidden" id="subvoyage-management_id" name="subvoyage_management_id" value=""/>
    <input type="hidden" id="transaction-ref" name="transaction_ref" value=""/>
    <input type="hidden" id="points" name="points" value=""/>
    <input type="hidden" id="jetter-no" name="jetter_no" value=""/>
    <input type="hidden" id="payment" name="payment" value=""/>
    <input type="hidden" id="fare-price" name="fare_price" value=""/>
    <input type="hidden" id="return-fare-price" name="return_fare_price" value=""/>
    <input type="hidden" id="total-discount" name="total_discount" value=""/>
    <input type="hidden" id="terminal-fee" name="terminal_fee" value=""/>
    <input type="hidden" id="port-charge" name="port_charge" value=""/>
    <input type="hidden" id="is-return" name="is_return" value=""/>
    <input type="hidden" id="passenger-validid-number" name="passenger_valid_id_number" value=""/>
	</form>

	
<?=$footer;?>