<?=$header;?>
		<form class="form-horizontal" action="<?=current_url();?>" method="POST">
				<div class="row-fluid ">
					<div class="box span12">
						<div class="box-header" data-original-title>
							<h2><i class="icon-exclamation-sign"></i><span class="break"></span><b>View Debtor Amount Details</b></h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							</div>
						</div>
						<div class="box-content">
							<?=$this->load->view(admin_dir('notification'));?>
							<fieldset>
								<div class="control-group">
										<label class="control-label" for="commission_code">Debtor Code</label>
										<div class="controls">
											<?php foreach($debtor_management as $d) { ?>
												<?php if ($d->id_debtor == $debtor->debtor_id) { ?>
													<input disabled="disabled" class="input-xlarge number focused commission_code" id="commission_code" name="debtor_code" type="text" value="<?=set_value('debtor_code', $d->debtor_code);?>">
												<?php } ?>
											<?php }?>
										</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission">Amount</label>
									<div class="controls">
											<input disabled="disabled" class="input-xlarge number focused commission_code" id="commission_code" name="debtor_code" type="text" value="<?=set_value('amount', $debtor->amount);?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission_code">Payment Account Code</label>
									<div class="controls">
										<?php foreach($payment_account as $pa) { ?>
											<?php if ($pa->id_payment_account == $debtor->payment_account_id) { ?>
												<input disabled="disabled" class="input-xlarge number focused commission_code" id="commission_code" name="debtor_code" type="text" value="<?=set_value('payment_account', $pa->payment_account);?>">
											<?php } ?>
										<?php }?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission_percentage">Date of Deposit</label>
									<div class="controls">
										<input disabled="disabled" class="input-xlarge number focused commission_code" id="commission_code" name="commission_id" type="text" value="<?=set_value('commission', $debtor->deposit_date);?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission_percentage">Reference</label>
									<div class="controls">
										
												<input disabled="disabled" class="input-xlarge number focused commission_code" id="commission_code" name="commission_id" type="text" value="<?=set_value('commission', $debtor->reference);?>">
									</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Status</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge number focused enabled" id="outlet_address" name="enabled" type="text" value="<?=set_value('enabled', $debtor->type == 1 ? 'Add' : 'Deduct');?>">
										</div>
								</div>
								<div class="form-actions">
									<a href="<?=admin_url($this->classname);?>" class="btn">Return to List</a>
								</div>
								
							</fieldset>
						</div>
					</div><!--/span-->
				
				</div><!--/row-->    
		</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>