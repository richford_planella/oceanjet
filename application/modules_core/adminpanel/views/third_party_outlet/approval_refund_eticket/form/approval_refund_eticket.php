<?=$header;?>
<form id="form1" class="oj-form form-horizontal" action="<?=current_url();?>" method="POST">
	<div class="form-group form-group__ciustom">
		<div class="col-xs-12">
			<div class="inner-header">
            	<h3 class="inner-header__title">ETicket Details</h3>
          	</div>
          	<div class="form-group">
          		<div class="col-xs-9">
					<label class="control-label">Passenger's Name</label>
					<input readonly="readonly" class="form-control" id="fullname" name="fullname" type="text" value="<?=$eticket['fullname'];?>">
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission">Voyage No.</label>
					<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket['voyage'];?>" >
				</div>
          	</div>
			<div class="form-group">
				
				<div class="col-xs-3">
					<label class="control-label" for="commission">Origin</label>
					<?php foreach ($port as $origin) {?>
						<?php if ($origin->id_port == $eticket['voyage_origin']) {?>
							<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$origin->port;?>">
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-xs-3 has-feedback">
					<label class="control-label" for="commission">Destination</label>
					<?php foreach ($port as $destination) {?>
						<?php if ($destination->id_port == $eticket['voyage_destination']) {?>
					 		<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$destination->port;?>">
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-xs-3 has-feedback">
					<label class="control-label" for="commission">Departure Time (ETD)</label>
					<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket['etd'];?>" >
					<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				</div>
				<div class="col-xs-3 has-feedback">
					<label class="control-label" for="commission">Arrival Time (ETA)</label>
					<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket['eta'];?>" >
					<span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label" for="commission">Vessel</label>
					<?php foreach ($port as $destination) {?>
						<?php if ($destination->id_port == $eticket['voyage_destination']) {?>
					 		<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$destination->port;?>">
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission">Accommodation Type</label>
					<?php foreach ($accommodation as $ac) {?>
						<?php if ($ac->id_accommodation == $eticket['accommodation_id']) {?>
					 		<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$ac->accommodation;?>">
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission">Voyage Status</label>
					<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket['subvoyage_management_status'];?>">
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission">Ticket Amount</label>
					<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket['ticket_total_amount'];?>">
				</div>
			</div>	
		</div>
	</div>
	
	<div class="form-group form-group__ciustom">
		<div class="col-xs-12">
			<div class="inner-header">
            	<h3 class="inner-header__title">Refund Details</h3>
          	</div>
          	<div class="form-group">
          		<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Rule Code</label>
					<?php foreach($rule_set as $rs) {?>
						<?php if ($rs->id_rule_set == $eticket['rule_set_id']) {?>
							<input readonly="readonly" class="form-control" id="debtor_code" name="debtor_code" type="text" value="<?=$rs->rule_code;?>" readonly>
						<?php } ?>
					<?php } ?>
					
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Refund Charge</label>
					<?php foreach($rule_type as $rt) {?>
						<?php if ($rt->rule_set_id == $eticket['rule_set_id']) {?>
							<?php $refund_charge = $rt->rule_type_amount++;?>
							
						<?php } ?>
					<?php } ?>
					<input readonly="readonly" class="form-control" id="debtor_code" name="refund_charge" type="text" value="<?=$refund_charge;?>">
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="commission_percentage">Refund Amount</label>	
					<input readonly="readonly" class="form-control" id="debtor_code" name="refund_amount" type="text" value="<?=$eticket['ticket_total_amount'] - $refund_charge;?>">
				</div>	
          	</div>
		</div>	
	</div>	
	
	    	<div class="form-group oj-form-footer">
	      		<div class="col-xs-12">
	        		<div class="btn-oj-group right">
	        			
	          			<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
	          			
	          		</div>
	        	</div>
	      	</div>
       
                
</form>
<?=$footer;?>