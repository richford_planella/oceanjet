<?=$header;?>
	<?=$this->load->view(admin_dir('notification'));?>
	 <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
     
      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">Trip Details</h3>
            
          </div>
          <div class="form-group">
            <div class="col-xs-3">
              <label class="control-label">E-Ticket No.</label>
              <input id="eticket_no" name="eticket_no" class="form-control" type="text" value="<?=set_value('voyage_code');?>" disabled/>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Origin</label>
               <select id="origin_id" class="selectpicker form-control" name="origin_id" title="Please Select">
              <?php foreach ($port as $p) {?>
                  <option value="<?=$p->id_port;?>"><?=$p->port;?></option>
              <?php } ?>
            </select>
          </div>
        <div class="col-xs-3">
          <label class="control-label">Destination</label>
          <select id="destination_id" class="selectpicker form-control" name="destination_id" title="Please Select">
              <?php foreach ($port as $p) {?>
                  <option value="<?=$p->id_port;?>"><?=$p->port;?></option>
              <?php } ?>
          </select>
        </div>
         
      </div>

      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">Shipper & Driver Information</h3>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <h4>Shipper's Details</h4>
            </div>

            <div class="col-xs-3">
              <label class="control-label">First Name</label>
              <input class="form-control" type="text" name="sender_first_name" placeholder="Input Sender Firstname"  value="<?=set_value('sender_first_name');?>"/>
              <span class="input-notes-bottom"><?=form_error('sender_first_name')?></span>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Last Name</label>
              <input class="form-control" type="text" name="sender_last_name" placeholder="Input Sender Lastname" value="<?=set_value('sender_last_name');?>"/>
              <span class="input-notes-bottom"><?=form_error('sender_last_name')?></span>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Middle Initial</label>
              <input class="form-control" type="text" name="sender_middle_name" placeholder="Input Sender Middlename" value="<?=set_value('sender_middle_name');?>"/>
              <span class="input-notes-bottom"><?=form_error('sender_middle_name')?></span>
            </div>
             <div class="col-xs-3">
              <label class="control-label">Birthday</label>
              <input class="form-control" type="text" name="sender_birthday" placeholder="Input Sender Contact Number" value="<?=set_value('sender_birthday');?>"/>
              <span class="input-notes-bottom"><?=form_error('sender_birthday')?></span>
            </div>
            
          </div>
          <div class="form-group">
              <div class="col-xs-3">
              <label class="control-label">Contact Number</label>
              <input class="form-control" type="text" name="sender_contact_no" placeholder="Input Sender Contact Number" value="<?=set_value('sender_contact_no');?>"/>
              <span class="input-notes-bottom"><?=form_error('sender_contact_no')?></span>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <h4>Driver's Details</h4>
            </div>

            <div class="col-xs-3">
              <label class="control-label">First Name</label>
              <input class="form-control" type="text" name="driver_first_name" placeholder="Input Driver Firstname" />
              <span class="input-notes-bottom"><?=form_error('driver_first_name')?></span>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Last Name</label>
              <input class="form-control" type="text" name="driver_last_name" placeholder="Input Driver Lastname" />
            </div>
            <div class="col-xs-3">
              <label class="control-label">Middle Initial</label>
              <input class="form-control" type="text" name="driver_middle_name" placeholder="Input Driver Middle Initial" />
            </div>
            <div class="col-xs-3">
              <label class="control-label">Birthday</label>
              <input class="form-control" type="text" name="driver_birthday" placeholder="Input Driver Birthday" value="<?=set_value('driver_birthday');?>"/>
              <span class="input-notes-bottom"><?=form_error('driver_birthday')?></span>
            </div>
          
          </div>
          <div class="form-group">
              <div class="col-xs-3">
              <label class="control-label">Contact Number</label>
              <input class="form-control" type="text" name="driver_contact_no" placeholder="Input Driver Contact Number" value="<?=set_value('driver_contact_no');?>"/>
              <span class="input-notes-bottom"><?=form_error('driver_contact_no')?></span>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">Rate Details</h3>
          </div>
          <div class="form-group">


            <div class="col-xs-3">
              <label class="control-label">RORO Rate Code</label>
              <select id="roro_rate" class="selectpicker form-control" title="Please Select" name="roro_id">
              	<?php foreach ($roro_rate as $rr) {?>
              		<option value="<?=$rr->id_rolling_cargo;?>"><?=$rr->rolling_cargo_code;?></option>
              	<?php } ?>
              </select>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Discount Code</label>
              <select id="discount_id" class="selectpicker form-control" title="Please Select" name="discount_id">
                <?php foreach ($discount as $d) {?>
              		<option value="<?=$d->id_discount;?>"><?=$d->discount_code;?></option>
              	<?php } ?>
              </select>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Mode of Payment</label>
              <select class="selectpicker form-control" title="Please Select" name="mode_of_payment">
                <option value="">Cash</option>
                <option value="">Credit Card</option>
                <option value="">Debit Card</option>
                <option value="">Check</option>
              </select>
            </div>
            
          </div>

        </div>
      </div>

      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">Total</h3>
          </div>
          <div class="form-group">
            <div class="col-xs-3">
              <label class="control-label">RORO Rate</label>
              <input id="roro_amount" class="form-control" name="roro_rate" placeholder="0.00"  value="" type="text" value="<?=set_value('roro_rate');?>" disabled/>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Discount</label>
              <input id="discount_amount" class="form-control" name="discount_amount" placeholder="0.00"  value="" type="text" value="<?=set_value('discount_amount');?>" disabled/>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Terminal Fee</label>
              <input class="form-control" id="terminal_fee" name="terminal_fee" type="text" placeholder="0.00"  value="" value="<?=set_value('terminal_fee');?>" disabled/>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Port Charge</label>
              <input class="form-control" id="port_charge" name="port_charge" type="text" placeholder="0.00"  value="" value="<?=set_value('port_charge');?>" disabled/>
            </div>
          </div>
          <div class="form-group">
           
            <div class="col-xs-3">
              <label class="control-label">Total Due</label>
              <input id="total_due" class="form-control" name="total_due" type="text" placeholder="0.00" value="<?=set_value('total_due');?>" disabled/>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
            <button type="submit" class="btn oj-button">Print</button>
          </div>
        </div>
      </div>
    </form>

	
<?=$footer;?>