<?=$header;?>

		<div class="row-fluid sortable">		
			<div class="box span12">
				<div class="box-header" data-original-title>
					<h2><i class="icon-reorder"></i><span class="break"></span><b>Revalidate E-ticket</b></h2>
					<div class="box-icon">
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
					</div>
				</div>

				<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
				<label>E-Ticket No.</label>
				<input type="text" id="eticket_no" value=""/>
				<label>Lastname</label>
				<input type="text" id="last_name" value=""/>	
				<button class="btn abc" >Search</button>	
					<table class="table table-striped table-bordered">
					  <thead>
						  <tr>
								<th class="align-center">E-Ticket No.</th>
								<th>Full Name</th>
								<th class="align-center">Actions</th>
						  </tr>
					  </thead>   
					  <tbody>			
					  </tbody>
					  
					</table>            
				</div>
			</div><!--/span-->
		</div><!--/row-->
	</div><!--/.fluid-container-->
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>	
<?=$footer;?>