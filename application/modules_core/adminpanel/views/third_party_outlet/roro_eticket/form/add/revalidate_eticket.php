<?=$header;?>
	<form class="form-horizontal" action="<?=current_url();?>" method="POST">
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-plus"></i><span class="break"></span><b>Trip Details</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
								<div class="control-group">
									<label class="control-label" for="commission">Trip Type</label>
									<div class="controls">
										<?php foreach ($trip_type as $t) { ?>
											<input type="radio" value="<?=$t->id_trip_type;?>"><?=$t->type?><br/>
										<?php } ?>
									</div>
								</div>
								<!--one way trip -->
								<div class="control-group">
										<label class="control-label" for="outlet_code">Departure Date</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="departure_date" name="departure_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code" >Voyage</label>
										<select id="voyage" name="voyage">
											 <option value="">Please Select</option>
											
										</select>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Origin</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="origin" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Destination</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="destination" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Estimated Time of Departure</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="etd" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Estimated Time of Arrival</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="eta" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Vessel</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="vessel" name="vessel_name" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<!--return -->
								<div id="return">
								<h3>Return</h3>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Departure Date</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="departure_date" name="departure_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code" >Voyage</label>
										<select id="voyage" name="voyage">
											 <option value="">Please Select</option>
											
										</select>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Origin</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="origin" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Destination</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="destination" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Estimated Time of Departure</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="etd" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Estimated Time of Arrival</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="eta" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Vessel</label>
										<div class="controls">
											<input disabled class="input-xlarge datepicker focused" id="vessel" name="vessel_name" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
							</div>
								<div class="control-group">
									<label class="control-label" for="commission_percentage">Accommodation Type</label>
									<div class="controls">
										<select id="enabled" name="payment_account_id">
											 <option value="">Please Select</option>
											<?php foreach ($accommodation as $a) { ?>
												<option value="<?=$a->id_accommodation;?>"><?=$a->accommodation;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
														
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row--> 
			<!-- passenger info --> 
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-plus"></i><span class="break"></span><b>Passenger Information</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
								<div class="control-group">
										<label class="control-label" for="outlet_code">First Name</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="firstname" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Last Name</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="lastname" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Middle Name</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="middlename" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Birthday</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Sex</label>
										
										<select id="enabled" name="sex" rel="chosen">
											<option value="">Please Select</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
										</select>
							
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Contact Number</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">ID Number</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Nationality</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">With Infant?</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="with_infant" type="radio" value="Yes">Yes<br/>
											<input class="input-xlarge datepicker focused" id="deposit_date" name="with_infant" type="radio" value="No">No
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Infant's Name</label>
										<div class="controls">
											<input class="input-xlarge datepicker focused" id="deposit_date" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>						
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->   
			<!-- fare details --> 
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-plus"></i><span class="break"></span><b>Fare Details</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
								<div class="control-group">
									<label class="control-label" for="commission">Passenger Fare Code</label>
									<div class="controls">
										<select id="debtor_id" name="debtor_id">
											 <option value="">Please Select</option>
											<?php foreach ($debtor_management as $u) { ?>
												<option value="<?=$u->id_debtor?>"><?php echo $u->debtor_code;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission_percentage">Discount Code</label>
									<div class="controls">
										<select id="enabled" name="payment_account_id">
											 <option value="">Please Select</option>
											<?php foreach ($payment_account as $o) { ?>
												<option value="<?=$o->id_payment_account;?>"><?=$o->payment_account;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="outlet_code">Points</label>
										<div class="controls">
											<input  class="input-xlarge datepicker focused" id="deposit_date" name="deposit_date" type="text" value="<?=set_value('date_deposit');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="reference">Jetter Number</label>
										<div class="controls">
											<input  class="input-xlarge focused reference" id="referencee" name="reference" type="text" value="<?=set_value('reference');?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="reference">Mode of Payment</label>
										<div class="controls">
											<input  class="input-xlarge focused reference" id="referencee" name="reference" type="text" value="<?=set_value('reference');?>">
										</div>
								</div>
								<div class="control-group">
										<h2><b>TOTAL</b></h2>
										<label class="control-label" for="reference">Fare</label>
										<div class="controls">
											<input  class="input-xlarge focused reference" id="referencee" name="reference" type="text" value="<?=set_value('reference');?>">
										</div>
										<label class="control-label" for="reference">Discount</label>
										<div class="controls">
											<input  class="input-xlarge focused reference" id="referencee" name="reference" type="text" value="<?=set_value('reference');?>">
										</div>
										<label class="control-label" for="reference">Terminal Fee</label>
										<div class="controls">
											<input  class="input-xlarge focused reference" id="referencee" name="reference" type="text" value="<?=set_value('reference');?>">
										</div>
										<label class="control-label" for="reference">Port Charge</label>
										<div class="controls">
											<input  class="input-xlarge focused reference" id="referencee" name="reference" type="text" value="<?=set_value('reference');?>">
										</div>
										<label class="control-label" for="reference">Total Due</label>
										<div class="controls">
											<input  class="input-xlarge focused reference" id="referencee" name="reference" type="text" value="<?=set_value('reference');?>">
										</div>
										
								</div>
								
								<div class="form-actions">
									<button type="submit" class="btn btn-primary" >SUBMIT</button>
									<a href="<?=admin_url($this->classname);?>" class="btn">CANCEL</a>
								</div>
															
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->   
                        
	</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>
<?=$footer;?>