<?php //print '<pre>'.print_r($voyage_fare_details,true).'</pre>';?>

<?php foreach($voyage_fare_details as $key => $val) { ?>
	<?php foreach($val as $key1 => $val1) {?>
		<div class="form-group">
		  	<div class="col-xs-3">
		        <label class="control-label">Breakdown per Leg</label>
				<input class="form-control fare-amount" type="form-control" value="<?=$val1->amount;?>" disabled/>
		    </div>
		  	<div class="col-xs-3">
				<label class="control-label" >Discount Code</label>
				<select class="selectpicker form-control" id="discount_id" name="discount_id">
					<option value="">Please Select</option>
					<?php foreach ($discount as $d) { ?>
						<option value="<?=$d->id_discount;?>"><?=$d->discount_code;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-xs-3">
		        <label class="control-label">E-voucher Code</label>
				<input class="form-control" type="form-control"/>
		    </div>
		    <div class="col-xs-3">
		        <label class="control-label">Amount</label>
				<input  readonly="readonly" id="fare_total_amount" class="form-control" placeholder="0.00" name="fare_total_amount" type="text" value="<?=$val1->amount;?>"/>
		    </div>
		</div>
	<?php } ?>
<?php } ?>

