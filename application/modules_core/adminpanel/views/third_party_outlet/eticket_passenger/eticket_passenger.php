<?=$header;?>
<?=$this->load->view(admin_dir('notification'));?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
	<div class="form-group">
		<div class="col-xs-2">
			<label class="control-label" for="origin">Origin</label>
			<select class="selectpicker form-control ticket-orig-dest" name="origin" id="origin" title="Please Select">
	        	 <?php foreach ($port as $key => $value){?>
	           		 <option value="<?=$value->id_port?>"><?=$value->port?></option>
	     		  <?php }?>
	        </select>
		</div>
		<div class="col-xs-2">
			<label class="control-label" for="destination">Destination</label>
			 <select class="selectpicker form-control ticket-orig-dest" name="destination" id="destination" title="Please Select">
        	 	<?php foreach ($port as $key => $value){?>
            		<option value="<?=$value->id_port?>"><?=$value->port?></option>
     		 	<?php }?>
        	</select>
		</div>
		<div class="col-xs-3 has-feedback">
	        <label class="control-label">Depature Date</label>
	        <input class="form-control custom-datepicker voyage_date" type="text" name="departure_date" id="departure_date" value="<?=set_value('departure_date');?>" placeholder="YYYY-MM-DD" />
	        <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
	        <span class="input-notes-bottom"><?=form_error('departure_date')?></span>
      	</div>
		<div class="col-xs-5">
			<label class="control-label" for="departure_date">Voyage</label>	
			<select id="voyage_id" class="selectpicker form-control" name="voyage_id" title="Please Select">
			</select>
		</div>
		<div class="col-xs-2">
			<label class="control-label">Round Trip</label>
			<div class="checkbox">
		        <label>
		              <input type="checkbox" id="is-return" name="is_return"  /> Yes
		        </label>
            </div>
		</div>
		<div class="col-xs-3 has-feedback">
        	<label class="control-label">Return Depature Date</label>
        	<input class="form-control custom-datepicker return_voyage_date" type="text" name="return_departure_date" value="<?=set_value('departure_date');?>" placeholder="YYYY-MM-DD" disabled/>
       	 	<span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
        	<span class="input-notes-bottom"><?=form_error('departure_date')?></span>
  		</div>
		<div class="col-xs-3">
			<label class="control-label" for="departure_date">Return Voyage</label>	
			<select class="return-departure selectpicker form-control" name="return_departure" title="Please Select" disabled>
			</select>
		</div>
		<div class="col-xs-3">
			<label class="control-label" for="commission_percentage">Accommodation Type</label>	
			<select id="accommodation" class="selectpicker form-control" name="accommodation_type_id">
				 <option value="">Please Select</option>
				<?php foreach ($accommodation as $a) { ?>
					<option value="<?=$a->id_accommodation;?>"><?=$a->accommodation;?></option>
				<?php } ?>
			</select>
		</div>
	</div>		
	<div class="orig-dest-details"></div>
	<div class="return-orig-dest-details"></div>
<div class="ticket-details">
	<?php if (count($voyage_details) == 0) {?>
		<div class="form-group">
	        <div class="col-xs-12">
	          <div class="form-box form-box--blank no-content">
	            <span class="no-selected">No Input Data</span>
	          </div>
	        </div>
	    </div>
	<?php } else { ?>
	<?php $counter=1;?>
	<div class="form-group form-group__custom">
		<div class="col-xs-12">
	      	<div class="inner-header">
		        <h3 class="inner-header__title">Trip Details</h3>
		    </div>
	      	<div class="form-group">
	            <div class="col-xs-3">
	              <label class="control-label">Voyage Code</label>
	              <input class="form-control voyage_code" name="voyage_code" type="text" value="<?=$voyage_details[0]->voyage;?>" disabled />
	            </div>
	        </div>
		</div>
		<?php foreach ($voyage_details as $key => $val) {?>
		    <input type="hidden" name="subvoyage_management_id[]" value="<?=$val->id_subvoyage_management;?>"/>
		    <!-- foreach voyage details -->
		    <div class='form-group'>
				<div class='col-xs-12'>
					<div class='seat-preview__title'>
						<h4> Leg <?php echo $counter;?></h4>
					</div>		
				</div>
			</div>
			<div class='form-group'>
				<div class='col-xs-3'>
					<label class='control-label'>Ticket Number</label>
					<input class='form-control' name='ticket_no[]' readonly='readonly' value='<?php echo "test-".str_pad(rand(0, pow(10, 5)-1), 5, '0', STR_PAD_LEFT);?>'/>
				</div>
				<div class='col-xs-3 has-feedback'>
					<label class='control-label'>Departure</label>
					<input readonly='readonly'  class='form-control oj-date-value' value='<?=$val->departure_date;?>'/>
					<span class='glyphicon glyphicon-calendar form-control-feedback' aria-hidden='true'></span>
				</div>
				<div class='col-xs-3'>
					<label class='control-label'>Origin</label>
					<input readonly='readonly' class='form-control' value='<?=$val->origin_port;?>'/>
				</div>
				<div class='col-xs-3'>
					<label class='control-label'>Destination</label>
					<input readonly='readonly' class='form-control' value='<?=$val->destination_port;?>'/>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-xs-3 has-feedback'>
					<label class='control-label'>Depature Time (ETD)</label>
					<input type='text' readonly='readonly' class='form-control' value='<?=date("H:i A",strtotime($val->ETD));?>'/>
					<span class='glyphicon glyphicon-calendar form-control-feedback' aria-hidden='true'></span>
				</div>
				<div class='col-xs-3 has-feedback'>
					<label class='control-label'>Arrival Time (ETA)</label>
					<input type='text' readonly='readonly' class='form-control' value='<?=date("H:i A",strtotime($val->ETA));?>'/>
					<span class='glyphicon glyphicon-calendar form-control-feedback' aria-hidden='true'></span>
				</div>
				<div class='col-xs-3'>
					<label class='control-label'>Vessel</label>
					<input readonly='readonly' class='form-control' value='<?=$val->vessel;?>'/>
				</div>
			</div>
			<div class='col-xs-12'><hr class='hr-custom'></div>
				<?php $counter++;?>
		<?php } ?> 
	</div>
	<div class="return-eticket-details"></div>	
	<!-- passenger info --> 
	<div class="form-group form-group__custom">
		<div class="col-xs-12">
			<div class="inner-header">
	        	<h3 class="inner-header__title">Personal Information</h3>
	      	</div>
	      	<div class="form-group">
	      		<div class="col-xs-4">
					<label class="control-label" for="outlet_code">First Name</label>
					<input class="form-control"  id="firstname" name="firstname" type="text" value="<?=set_value('firstname');?>">
					<span class="input-notes-bottom"><?=form_error('firstname')?></span>
				</div>
				<div class="col-xs-4">
					<label class="control-label" for="outlet_code">Last Name</label>
					<input class="form-control" id="lastname" name="lastname" type="text" value="<?=set_value('lastname');?>">
					<span class="input-notes-bottom"><?=form_error('lastname')?></span>
				</div>
				<div class="col-xs-2">
					<label class="control-label" for="outlet_code">Middle Initial</label>
					<input class="form-control" id="middlename" name="middlename" type="text" value="<?=set_value('middlename');?>">
					<span class="input-notes-bottom"><?=form_error('middlename')?></span>
				</div>
				<div class="col-xs-2">
					<label class="control-label" for="outlet_code">Sex</label>			
					<select id="gender" class="selectpicker form-control" name="gender" rel="chosen">
						<option value="">Please Select</option>
						<option value="Male">Male</option>
						<option value="Female">Female</option>
					</select>
					<span class="input-notes-bottom"><?=form_error('gender')?></span>
				</div>
	      	</div>
	        <div class="form-group">
          		<div class="col-xs-3 has-feedback">
					<label class="control-label" for="outlet_code">Birthdate</label>
					<input id="birthdate" class="custom-datepicker form-control"  placeholder="MM/DD/YYYY" name="birthdate" type="text" value="<?=set_value('birthdate');?>">
					<span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
					<span class="input-notes-bottom"><?=form_error('birthdate')?></span>
				</div>
				<div class="col-xs-2">
					<label class="control-label" for="outlet_code">Age</label>
					<input readonly="readonly" class="form-control" id="age" name="age" type="text" value="<?=set_value('age');?>">
				</div>
				<div class="col-xs-3">
					<label class="control-label" for="outlet_code">Contact Number</label>
					<input class="form-control" id="contactno" name="contactno" placeholder="Input Number" type="text" value="<?=set_value('contactno');?>">
					<span class="input-notes-bottom"><?=form_error('contactno')?></span>
				</div>
				<div class="col-xs-4">
					<label class="control-label" for="outlet_code">ID Number</label>
					<input class="form-control" id="id_no" name="id_no" type="text" placeholder="Input Number" value="<?=set_value('id_no');?>">				
					<span class="input-notes-bottom"><?=form_error('id_no')?></span>
				</div>
          	</div>
          	<div class="form-group">
          		<div class="col-xs-3">
					<label class="control-label" for="outlet_code">Nationality</label>
					<input class="form-control" placeholder="Input Nationality" id="nationality" name="nationality" type="text" value="<?=set_value('nationality');?>">
					<span class="input-notes-bottom"><?=form_error('nationality')?></span>
				</div>
				<div class="col-xs-2">
					<label class="control-label" for="outlet_code">With Infant?</label>
					<div class="checkbox">
						<label>
		             		<input type="checkbox" id="with_infant" value="1" name="is_return" <?=set_checkbox('with_infant', 1);?> />
	        			</label>
	        		</div>
		        </div>
	        	<div class="col-xs-7">
					<label class="control-label" for="outlet_code">Infant's Name</label>					
					<input class="form-control" id="infant_name" name="infant_name" placeholder="Input Infant Name" type="text" value="<?=set_value('infant_name');?>">								
				</div>
          	</div>
		</div>
	</div>
	<div class="form-group form-group__custom">
        <div class="col-xs-12">
        	<div class="inner-header">
            	<h3 class="inner-header__title">Accomodation Details</h3>
          	</div>
        </div>
	    <div class="form-group">
            <div class="col-xs-12">
            	<table class="table  table-striped table-form">
                	<thead>
                  		<tr>
	                    	<th>Items</th>
                    		<?php foreach ($voyage_details as $key => $val) {?>
	                    		<th><?=$val->origin_port_code;?> - <?=$val->destination_port_code;?></th>
	                    	<?php }?>
                  		</tr>
                	</thead>
	                <tbody>
                  		<tr>
		                    <th scope="row">Total Capacity</th>
		                    <td>N/A</td>
		                    <td></td>
		                    <td></td>
		                </tr>
	                  	<tr>
		                    <th scope="row">Tickted PAX</th>
		                    <td>N/A</td>
		                    <td></td>
		                    <td></td>
	                  	</tr>
		                <tr>
		                    <th scope="row">Check-In PAX</th>
		                    <td>N/A</td>
		                    <td></td>
		                    <td></td>
		                </tr>
                  		<tr>
		                    <th scope="row">No Show PAX</th>
		                    <td>N/A</td>
		                    <td></td>
		                    <td></td>
                  		</tr>
                  		<tr>
		                    <th scope="row">Available Seats</th>
		                    <td>N/A</td>
		                    <td></td>
		                    <td></td>
                  		</tr>
                	</tbody>
              	</table>
            </div>
        </div>
    </div>
	<div class="form-group form-group__custom">
        <div class="col-xs-12">
          	<div class="inner-header">
	            <h3 class="inner-header__title">Fare Details</h3>
          	</div>
	        <div class="form-group">
	          	<div class="col-xs-3">
					<label class="control-label">Voyage Code</label>
					<input  readonly="readonly" class="form-control voyage_code" id="vcode" name="points" type="text" value="<?=$voyage_details[0]->voyage?>">
				</div>
				<div class="col-xs-3">
					<label class="control-label" >Passenger Fare Code</label>
					<select class="form-control selectpicker" id="passenger_fare_id" title="Please Select" name="passenger_fare_id">
						<option value="">Please Select</option>
					</select>
				</div>
				<div class="col-xs-3">
					<label class="control-label" >Points Earning</label>
					<input  readonly="readonly" class="form-control" id="points-earning" name="points" type="text" value="">
				</div>
	        </div>
	        <div class='col-xs-12'><hr class='hr-custom'></div>
	        <div class="fare-details">
	          	<div class="form-group">
	          		<div class="col-xs-3">
	                	<label class="control-label">Breakdown per Leg</label>
						<input class="form-control" placeholder="0.00" type="form-control" value="" readonly="readonly"/>
	           		</div>
	            	<div class="col-xs-3">
	                	<label class="control-label">Amount</label>
						<input  readonly="readonly" id="fare_total_amount" class="form-control" placeholder="0.00" name="fare_total_amount" type="text" value=""/>
	            	</div>
	          	</div>
	        </div>  	
	        <div class='col-xs-12'><hr class='hr-custom'></div>
            	<div class="form-group">
	              	<div class="col-xs-3">
						<label class="control-label" >Discount Code</label>
						<select class="selectpicker form-control" id="discount_id" name="discount_id">
							<option value="">Please Select</option>
							<?php foreach ($discount as $d) { ?>
								<option value="<?=$d->id_discount;?>"><?=$d->discount_code;?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-xs-3">
	                	<label class="control-label">E-voucher Code</label>
						<input class="form-control" id="voyage_evoucher_code" type="form-control" value=""/>
	            	</div>
	                <div class="col-xs-3">
	                  	<label class="control-label" for="commission_percentage">E-voucher Discount</label>
						<input readonly="readonly" placeholder="0.00" class="form-control" type="form-control"/>
	                </div>   
	            </div>
	            <div class="form-group">
	              	<div class="col-xs-3">
	                	<label class="control-label" for="commission_percentage">Terminal Fee</label>
						<input id="terminal-fee" readonly="readonly" placeholder="0.00" class="form-control" type="text" value="<?=$voyage_details[0]->origin_terminal_fee;?>"/>
	                </div>
	                <div class="col-xs-3">
	                	<label class="control-label" for="commission_percentage">Port Fee</label>
						<input id="port-charge" readonly="readonly" class="form-control" placeholder="0.00" type="text" value="<?=$voyage_details[0]->origin_port_charge;?>"/>
	                </div>
		            <div class="col-xs-3">
		            	<label class="control-label">Total Amount</label>
		            	<input id="total_amount" placeholder="0.00" class="form-control" type="text" value="" disabled/>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
	<div class="return-eticket-fare-details"></div> 
	<div class="form-group form-group__custom">
        <div class="col-xs-12">
        	<div class="inner-header">
            	<h3 class="inner-header__title">Total</h3>
          	</div>
          	<div class="form-group">
		      	<div class="col-xs-3">
		        	<label class="control-label">Total Due</label>
		        	<input readonly class="form-control" id="total_due" name="total_amount" type="text" value=""/>
		        </div>
		        <div class="col-xs-3">
		         	<label class="control-label" for="commission">Mode of Payment</label>
					<select class="selectpicker form-control"id="payment_method_id" name="payment_method_id" title="Please Select">
						<?php foreach ($payment_method as $pm) {?>
							<option value="<?=$pm->id_payment_method;?>"><?=$pm->payment_method;?></option>
						<?php } ?>
					</select>
		        </div>
		        <div class="col-xs-3">
		          	<label class="control-label" for="reference">Reference No.</label>					
						<input   readonly class="form-control" id="total_due" name="total_amount" placeholder="Input Reference No." type="text" value="<?=set_value('total_amount');?>">
			        </div>
	        	</div>
	          	<div class="form-group">
	            	<div class="col-xs-3">
	              		<label class="control-label">Amount Tendered</label>
	              		<input class="form-control" type="text" placeholder="Input Details" />
	            	</div>
	            	<div class="col-xs-3">
	              		<label class="control-label">Change</label>
	              		<input class="form-control" type="text" placeholder="0.00" value="" disabled/>
	            	</div>
	        	</div>
	        </div>
	    </div>	
		<div class="form-group oj-form-footer">
	        <div class="col-xs-12">
	          <div class="btn-oj-group right">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
	            <button type="submit" name="add-ticket"  class="btn oj-button">Print E-Ticket</button>
	          </div>
	        </div>
	    </div>
		<?php } ?>  
	</div>
</div>        
</form>

<?=$footer;?>