<?php $xcounter=1;?>
<div class="form-group form-group__custom">
	        <div class="col-xs-12">
	          <div class="inner-header">
	            <h3 class="inner-header__title">Return Trip Details</h3>
	          </div>
	          <div class="form-group">
	            <div class="col-xs-3">
	              <label class="control-label">Voyage Code</label>
	              <input class="form-control voyage_code" name="voyage_code" type="text" value="<?=$return_voyage_details[0]->voyage;?>" disabled />
	            </div>
	          </div>
	      	</div>
	     
	    <?php foreach ($return_voyage_details as $key => $val) {?>
	     <input type="hidden" name="return_subvoyage_management_id" value="<?=$val->id_subvoyage_management;?>"/>
	     
	        <!-- foreach voyage details -->
	        <div class='form-group'>
				<div class='col-xs-12'>
					<div class='seat-preview__title'>
						<h4> Leg <?php echo $xcounter;?></h4>
					</div>		
				</div>
			</div>
			<div class='form-group'>
				<div class='col-xs-3'>
					<label class='control-label'>Ticket Number</label>
					<input class='form-control' name='return_ticket_no' readonly='readonly' value='<?php echo "test-".str_pad(rand(0, pow(10, 5)-1), 5, '0', STR_PAD_LEFT);?>'/>
				</div>
				<div class='col-xs-3 has-feedback'>
					<label class='control-label'>Departure</label>
					<input readonly='readonly'  class='form-control oj-date-value' value='2/2/2016'/>
					<span class='glyphicon glyphicon-calendar form-control-feedback' aria-hidden='true'></span>
				</div>
				<div class='col-xs-3'>
					<label class='control-label'>Origin</label>
					<input readonly='readonly' class='form-control' value='<?=$val->origin_port;?>'/>
				</div>
				<div class='col-xs-3'>
					<label class='control-label'>Destination</label>
					<input readonly='readonly' class='form-control' value='<?=$val->destination_port;?>'/>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-xs-3 has-feedback'>
					<label class='control-label'>Depature Time (ETD)</label>
					<input type='text' readonly='readonly' class='form-control' value='<?=date("H:i A",strtotime($val->ETD));?>'/>
					<span class='glyphicon glyphicon-calendar form-control-feedback' aria-hidden='true'></span>
				</div>
				<div class='col-xs-3 has-feedback'>
					<label class='control-label'>Arrival Time (ETA)</label>
					<input type='text' readonly='readonly' class='form-control' value='<?=date("H:i A",strtotime($val->ETA));?>'/>
					<span class='glyphicon glyphicon-calendar form-control-feedback' aria-hidden='true'></span>
				</div>
				<div class='col-xs-3'>
					<label class='control-label'>Vessel</label>
					<input readonly='readonly' class='form-control' value='<?=$val->vessel;?>'/>
				</div>
			</div>
			<div class='col-xs-12'><hr class='hr-custom'></div>
		<?php } ?> 
	</div>