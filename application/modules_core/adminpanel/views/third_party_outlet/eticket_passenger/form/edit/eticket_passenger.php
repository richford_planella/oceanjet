<?=$header;?>
	<form class="form-horizontal" action="<?=current_url();?>" method="POST">
			<div class="row-fluid ">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>Modify Commission</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="commission_code">Debtor Code</label>
								<div class="controls">
								  <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=set_value('debtor_code', $debtor->debtor_code);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission">Debtor Name</label>
								<div class="controls">
									<select id="enabled" name="user_id">
										 <option value="">Please Select</option>
										<?php foreach ($user as $u) { ?>
											<option value="<?=$u->id_user_account;?>" <?php echo ($u->id_user_account == $debtor->user_id ? 'selected' : '');?>><?php echo $u->firstname." ".$u->lastname;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission_percentage">Outlet/s</label>
								<div class="controls">
									<select id="enabled" name="outlet">
										 <option value="">Please Select</option>
										<?php foreach ($outlet as $o) { ?>
											<option value="<?=$o->id_outlet;?>" <?php echo ($o->id_outlet == $debtor->outlet_id ? 'selected' : '');?>><?=$o->outlet_name;?></option>
										<?php } ?>
									</select>
								</div>
								<a href="#">add</a>
								<a href="#">delete</a>
							</div>
							<div class="control-group">
									<label class="control-label" for="commission_percentage">Commission</label>
									<div class="controls">
										<select id="enabled" name="commission">
											 <option value="">Please Select</option>
											<?php foreach ($commission as $c) { ?>
												<option value="<?=$c->id_commission;?>" <?php echo ($c->id_commission == $debtor->commission_id ? 'selected' : '');?>><?=$c->commission;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							<div class="control-group">
								  <label class="control-label" for="enabled">Status</label>
								  <div class="controls">
									<select id="enabled" name="enabled" data-rel="chosen">
										<option value="">Please Select</option>
										<option value="1" <?=set_select('enabled', '1',(($debtor->enabled) ? TRUE : ''));?>>Active</option>
										<option value="0" <?=set_select('enabled', '0',((!$debtor->enabled) ? TRUE : ''));?>>Inactive</option>
									</select>
								  </div>
							</div>
						   
							  <div class="form-actions">
								<button type="submit" class="btn btn-primary" >Save changes</button>
								<a href="<?=admin_url($this->classname);?>" class="btn">Cancel</a>
							</div>
															
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->    
                    
	</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>