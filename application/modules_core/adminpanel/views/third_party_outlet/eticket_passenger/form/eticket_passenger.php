<?=$header;?>
		<form class="form-horizontal" action="<?=current_url();?>" method="POST">
				<div class="row-fluid ">
					<div class="box span12">
						<div class="box-header" data-original-title>
							<h2><i class="icon-exclamation-sign"></i><span class="break"></span><b>E-Ticket Details</b></h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							</div>
						</div>
						<div class="box-content">
							<?=$this->load->view(admin_dir('notification'));?>
							<fieldset>
								<div class="control-group">
										<label class="control-label" for="commission_code">E-Ticket No.</label>
										<div class="controls">
											<?php foreach($ticket_series_info as $ts) { ?>
												<?php if ($eticket_passenger->ticket_series_info_id == $ts->id_ticket_series_info) { ?>
													<input disabled="disabled" class="input-xlarge number focused " id="ticket_no" name="ticket_no" type="text" value="<?=$ts->ticket_no;?>">
												<?php } ?>
											<?php }?>
										</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission">Origin</label>
									<div class="controls">
										<?php foreach($port as $origin) {?>
											<?php if ($eticket_passenger->origin == $origin->id_port) {?>
												<input disabled="disabled" class="input-xlarge number focused " id="origin" name="origin" type="text" value="<?=$origin->port;?>">
											<?php } ?>
										<?php } ?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission">Destination</label>
									<div class="controls">
										<?php foreach($port as $destination) {?>
											<?php if ($eticket_passenger->destination == $destination->id_port) {?>
												<input disabled="disabled" class="input-xlarge  focused " id="destination" name="destination" type="text" value="<?=$destination->port;?>">
											<?php } ?>
										<?php } ?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission">Accommodation Type</label>
									<div class="controls">
										<?php foreach($accommodation as $a) {?>
											<?php if ($eticket_passenger->accommodation_id == $a->id_accommodation) {?>
												<input disabled="disabled" class="input-xlarge number focused " id="accommodation" name="accommodation" type="text" value="<?=$a->accommodation;?>">
											<?php } ?>
										<?php } ?>
									</div>
								</div
							
							</fieldset>
						</div>
					</div><!--/span-->
				
				</div><!--/row-->   
				<div class="row-fluid ">
					<div class="box span12">
						<div class="box-header" data-original-title>
							<h2><i class="icon-exclamation-sign"></i><span class="break"></span><b>Passenger Information</b></h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							</div>
						</div>
						<div class="box-content">
							<fieldset>
								<div class="control-group">
										<label class="control-label" for="commission_code">Firstname</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused commission_code" id="firstname" name="firstname" type="text" value="<?=$eticket_passenger->firstname;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Lastname</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused commission_code" id="lastname" name="lastname" type="text" value="<?=$eticket_passenger->lastname;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Middle Initial</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused commission_code" id="middlename" name="middlename" type="text" value="<?=$eticket_passenger->middlename;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Birthday</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused commission_code" id="birthdate" name="birthdate" type="text" value="<?=date("m-d-Y",strtotime($eticket_passenger->birthdate));?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Age</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused commission_code" id="age" name="age" type="text" value="<?=$eticket_passenger->age;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Gender</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused commission_code" id="gender" name="gender" type="text" value="<?=$eticket_passenger->gender;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Contact No.</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused commission_code" id="contactno" name="contactno" type="text" value="<?=$eticket_passenger->contactno;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">ID No.</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused commission_code" id="id_no" name="id_no" type="text" value="<?=$eticket_passenger->id_no;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Nationality</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused " id="nationality" name="nationality" type="text" value="<?=$eticket_passenger->nationality;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">With Infant</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge focused " id="with_infant" name="with_infant" type="text" value="<?php echo ($eticket_passenger->with_infant == 1) ? "Yes" : "No";?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Infant's Name</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused " id="infant_name" name="infant_name" type="text" value="<?=$eticket_passenger->infant_name;?>">
										</div>
								</div>
								
								
							</fieldset>
						</div>
					</div><!--/span-->
				</div><!--/row--> 
				<div class="row-fluid ">
					<div class="box span12">
						<div class="box-header" data-original-title>
							<h2><i class="icon-exclamation-sign"></i><span class="break"></span><b>Fare Details</b></h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							</div>
						</div>
						<div class="box-content">
							<fieldset>
								<div class="control-group">
										<label class="control-label" for="commission_code">Passenger Code</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge  focused " id="passenger_fare" name="passenger_fare" type="text" value="<?=$eticket_passenger->passenger_fare;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Discount Code</label>
										<div class="controls">
											<?php foreach($discount as $dc) {?>
												<?php if ($dc->id_discount == $eticket_passenger->discount_id) { ?>
													<input disabled="disabled" class="input-xlarge  focused " id="discount" name="discount" type="text" value="<?=$dc->discount;?>">
												<?php } ?>
											<?php } ?>
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Points</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge number focused commission_code" id="points" name="points" type="text" value="<?=$eticket_passenger->points;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Jetter Number</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge focused" id="jetter_no" name="jetter_no" type="text" value="<?=$eticket_passenger->jetter_no;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Mode of Payment</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge focused" id="payment" name="payment" type="text" value="<?=$eticket_passenger->payment;?>">
										</div>
								</div>
								<div class="control-group">
									<h2><b>TOTAL</b></h2>
										<label class="control-label" for="commission_code">Fare</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge number focused " id="fare_price" name="fare_price" type="text" value="<?=$eticket_passenger->fare_price;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Discount</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge number focused " id="total_discount" name="total_discount" type="text" value="<?=$eticket_passenger->total_discount;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Terminal Fee</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge number focused " id="terminal_fee" name="terminal_fee" type="text" value="<?=$eticket_passenger->terminal_fee;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Port Charge</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge number focused " id="port_charge" name="port_charge" type="text" value="<?=$eticket_passenger->port_charge;?>">
										</div>
								</div>
								<div class="control-group">
										<label class="control-label" for="commission_code">Total Due</label>
										<div class="controls">
											<input disabled="disabled" class="input-xlarge number focused " id="total_amount" name="total_amount" type="text" value="<?=$eticket_passenger->total_amount;?>">
										</div>
								</div>
								
								<div class="form-actions">
									<a href="<?=admin_url($this->classname);?>" class="btn">Return to List</a>
								</div>
								
							</fieldset>
						</div>
					</div><!--/span-->
				</div><!--/row-->  
		</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>