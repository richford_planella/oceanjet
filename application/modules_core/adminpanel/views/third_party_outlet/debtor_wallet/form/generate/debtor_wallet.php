<?=$header;?>

<form class="form-horizontal oj-form" method="POST">
  	<div class="form-group margin-bottom-0 date-from-to">
        <div class="col-xs-3 has-feedback">
        	<label class="control-label">Date From</label>
          	<input id="dw-datefrom" id="dw-datefrom" type="text" name="date_from"  class="form-control checkbox-fields__date date-from" type="text" placeholder="YYYY-MM-DD" value="<?php echo !empty($date_to) ? $date_from : '';?>"/>
         	<span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="col-xs-3 has-feedback">
        	<label class="control-label">Date To</label>
          	<input id="dw-dateto" name="date_to" class="form-control checkbox-fields__date date-to" type="text" placeholder="YYYY-MM-DD" value="<?php echo !empty($date_to) ? $date_to : '';?>"/>
          	<span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="col-xs-3">
          	<button type="submit" name="generate_btn" value="1" class="btn oj-button oj-button--margin-top-30" type="button">Generate</button>
        </div>
    </div>

    <div class="form-group margin-bottom-10">
        <div class="col-xs-7">
        	<h4 class="oj-button--margin-top-30">Report Date:<span class="date_from"></span> <span class="date_to"></span></h4>
        </div>
        <div class="col-xs-5">
         	<div class="btn-oj-group oj-button--margin-top-30">
         		<a class="btn oj-button print_wallet" <?php echo empty($debtor) ? 'disabled' : '';?>>Print</a>
	            <a class="btn oj-button export-wallet-pdf" <?php echo empty($debtor) ? 'disabled' : '';?>>Export to PDF</a>
	            <a class="btn oj-button export-wallet-excel" <?php echo empty($debtor) ? 'disabled' : '';?>>Export to Excel</a>
	            <a href="<?=admin_url($this->classname);?>"class="btn oj-button">Back</a>	
          	</div>
        </div>
    </div>
   	<?php if (empty($debtor)) { ?>
   		<div class="no-data">
   			<div class="form-group">
        		<div class="col-xs-12">
          			<div class="form-box form-box--blank no-content">
            			<span class="no-selected">No Input Data</span>
          			</div>
        		</div>
      		</div>
   		</div>
   	<?php } else { ?>
   		<div class="has-data">
   			<div class="form-group">
   				<div class="col-xs-12">
	   				<table class="example-table table global-table nopadding">
						<thead>
							<tr>
								<td>Deposit No.<td>
								<td>Agent Code<td>
								<td>Agent Name<td>
								<td>Amount Loaded By<td>
								<td>Payment Details<td>	
							</tr>
						</thead>
						<tbody>
							<?php foreach ($debtor as $x) {?>
								<tr>
									<td><?=$x->id_debtor_wallet?><td>
									<td><?=$x->debtor_code;?><td>
									<td><?php echo $x->firstname." ".$x->lastname;?><td>
									<td>TBA<td>
									<td><?php echo $x->type == 1 ? "Added" : "Deducted";?> <?=$x->wallet_amount;?> <td>	
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
   			</div>
   		</div>
   	<?php } ?>
      

    </form>
<?=$footer;?>