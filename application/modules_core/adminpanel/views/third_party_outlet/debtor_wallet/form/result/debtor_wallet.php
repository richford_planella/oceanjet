<html>
	<head>
		<style type="text/css">

			.table,.table tr,.table td{
				font-size: 100px;
				border: 1px solid black;
				border-collapse: collapse;
				width:100%;
			}

			.table1{
				font-size: 16px;
				width:100%;
			}

			.test {
				float:left;
				border:1px solid black;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div class="row table-header-custom nopadding">
	       <h1>Load Wallet Reporting</h1>
	       <?php if (!empty($date_from) && !empty($date_to)) {?>
	       		<h3>Date From: <?=$date_from;?></h3>
	       		<h3>Date to: <?=$date_to;?></h3>
	   	    <?php } ?>
	       
	    </div>
	    <div>
			<table class="table">
				<thead>
					<tr>
						<td>Deposit No.</td>
						<td>Agent Code</td>
						<td>Agent Name</td>
						<td>Amount Loaded By</td>
						<td>Payment Details</td>	
					</tr>
				</thead>
				<tbody>
					<?php foreach ($debtor as $x) {?>
						<tr>
							<td><?=$x->id_debtor_wallet;?></td>
							<td><?=$x->debtor_code;?></td>
							<td><?php echo $x->firstname." ".$x->lastname;?></td>
							<td>TBAw</td>
							<td><?php echo $x->type == 1 ? "Added" : "Deducted";?> <?=$x->wallet_amount;?> </td>	
						</tr>
					<?php } ?>
					
				</tbody>
			</table>
		</div>
		<div>
			<table class="table1">
					<thead>
						<tr>
							<td>Printed by:</td>
							<td>Checked by:</td>
							<td>Audited By:</td>
						</tr>
					</thead>
					<tbody>
						
						<tr>
							<td><?=$print_name;?> <?=date("d-m-Y H:i:s");?></td>
							<td></td>
							<td></td>	
						</tr>
						
						
					</tbody>
				</table>
		</div>
	</body>
</html>