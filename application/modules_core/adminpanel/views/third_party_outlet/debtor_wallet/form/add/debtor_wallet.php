<?=$header;?>
	<form class="oj-form form-horizontal" action="<?=current_url();?>" method="POST">	
		<div class="row">
			<?=$this->load->view(admin_dir('notification'));?>
			<div class="col-xs-12">
				<div class="form-group">
					<div class="col-xs-3">
						<label class="control-label" for="commission">Debtor Code</label>
						<select class="select-picker form-control" id="debtor_id" name="debtor_id">
							 <option value="">Please Select</option>
							
								<?php foreach ($debtor_management as $u) { ?>
									<?php if (!empty($u)) {?>
										<option value="<?=$u->id_debtor;?>" <?=set_select('debtor_id', $u->id_debtor);?>><?php echo $u->debtor_code;?></option>
									<?php }?>
								<?php } ?>
						</select>
						<span class="input-notes-bottom"><?=form_error('debtor_id')?></span>
					</div>
					<div class="col-xs-3 has-feedback">
						<label class="control-label" for="outlet_code">Date of Deposit</label>
							<input  placeholder="MM-DD-YY" class="custom-datepicker form-control" id="deposit_date" placeholder="YYYY-MM-DD" name="deposit_date" type="text" value="<?php echo (!empty($depdate) ? $depdate: '');?>">
							<span class='glyphicon glyphicon-calendar form-control-feedback' aria-hidden='true'></span>
						<span class="input-notes-bottom"><?=form_error('deposit_date')?></span>
					</div>
					<div class="col-xs-3">
						<label class="control-label" for="outlet_code">Amount</label>
						<input class="form-control amount" type="number" id="amount" name="wallet_amount" type="text" placeholder="0.00" value="<?=set_value('wallet_amount');?>">
						<span class="input-notes-bottom"><?=form_error('wallet_amount')?></span>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<div class="col-xs-3">
						<label class="control-label" for="commission_percentage">Payment Account Code</label>
						<select class="selectpicker form-control" id="enabled" name="payment_account_id">
						 	<option value="">Please Select</option>
							<?php foreach ($payment_account as $o) { ?>
								<option value="<?=$o->id_payment_account;?>" <?=set_select('payment_account_id', $o->id_payment_account);?>><?=$o->payment_account;?></option>
							<?php } ?>
						</select>
						<span class="input-notes-bottom"><?=form_error('payment_account_id')?></span>
					</div>
					<div class="col-xs-6">
						<label class="control-label" for="reference">Reference</label>
						<span class="input-notes-top overflow-right-10">*Optional</span>
						<input  class="input-xlarge focused reference form-control" id="referencee" name="reference" type="text" value="<?=set_value('reference');?>">
						<span class="input-notes-bottom"><?=form_error('reference')?></span>
					</div>
				</div>
			</div>
		</div>
	
			<div class="row">
		    	<div class="form-group oj-form-footer">
		      		<div class="col-xs-12">
	        		<div class="btn-oj-group right">
	          			<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
	          			<button type="submit" name="add_wallet" value="1" class="btn oj-button">Add</button>
	          			<button type="submit" name="deduct_wallet" value="2" class="btn oj-button">Deduct</button>
	        	</div>
	      	</div>
      	</div>				                        
	</form>

<?=$footer;?>