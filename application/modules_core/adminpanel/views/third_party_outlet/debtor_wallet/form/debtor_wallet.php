<?=$header;?>
<form class="oj-form form-horizontal" action="<?=current_url();?>" method="POST">
		<div class="row">
			<?=$this->load->view(admin_dir('notification'));?>
				<div class="col-xs-12">
					<div class="form-group">
						<div class="col-xs-3">
							<label class="control-label">Debtor Code</label>
							<?php foreach($debtor_management as $d) { ?>
									<?php if ($d->id_debtor == $debtor->debtor_id) { ?>
										<input disabled="disabled" class="form-control" id="commission_code" name="debtor_code" type="text" value="<?=set_value('debtor_code', $d->debtor_code);?>">
									<?php } ?>
								<?php }?>
						</div>
						<div class="col-xs-3 has-feedback">
							<label class="control-label" for="commission_percentage">Date of Deposit</label>	
							<input disabled="disabled" class="form-control" id="commission_code" name="commission_id" type="text" value="<?=set_value('commission', $debtor->deposit_date);?>">			
							<span class='glyphicon glyphicon-calendar form-control-feedback' aria-hidden='true'></span>
						</div>
						<div class="col-xs-3">
							<label class="control-label" for="commission">Amount</label>
							<input disabled="disabled" class="form-control" id="commission_code" name="debtor_code" type="text" value="<?=set_value('wallet_amount', $debtor->wallet_amount);?>">
						</div>
						
					</div>	
				</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<div class="col-xs-3">
						<label class="control-label" for="commission_code">Payment Account Code</label>
						<?php foreach($payment_account as $pa) { ?>
							<?php if ($pa->id_payment_account == $debtor->payment_account_id) { ?>
								<input disabled="disabled" class="form-control"  id="commission_code" name="debtor_code" type="text" value="<?=set_value('payment_account', $pa->payment_account);?>">
							<?php } ?>
						<?php }?>
					</div>
					<div class="col-xs-6">
						<label class="control-label" for="reference">Reference</label>
						<span class="input-notes-top overflow-right-10">*Optional</span>
						<input disabled="disabled" class="form-control" id="reference" name="commission_id" type="text" value="<?=set_value('commission', $debtor->reference);?>">
					</div>
				</div>
			</div>
		</div>
		
	    	<div class="form-group oj-form-footer">
	      		<div class="col-xs-12">
	      			<div class="btn-oj-group right">
	      				<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
	      			</div>
	      		</div>
	      	</div>
	   			
</form>
<?=$footer;?>