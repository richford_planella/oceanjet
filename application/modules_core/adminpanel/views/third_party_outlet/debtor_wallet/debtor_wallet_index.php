<?=$header;?>
	<div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
            <a href="<?=admin_url($this->classname, 'generateWalletReport');?>" class="btn oj-button">Generate Wallet Report</a>
            
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
					
						
					<table id="main_table" class="example-table table global-table nopadding">
					  <thead>
						  <tr>
								<th class="align-center">Debtor Code</th>
								<th>Amount</th>
								<th>Payment Account Code</th>
								<th>Date of Deposit</th>
								<th>Reference</th>
								
								<th class="align-center">Actions</th>
						  </tr>
					  </thead>   
					  <tbody>
					
								<?php if(!empty($commission)):?>
									<?php foreach($commission as $u):?>
										<tr>
											<td class="align-center"><?=$u->debtor_code;?></td>
											<td class="center"><?=$u->wallet_amount;?></td>
											<td class="center"><?=$u->payment_account;?></td>
											<td class="center"><?=date("Y-m-d",strtotime($u->deposit_date));?></td>
											<td class="center"><?=$u->reference;?></td>
											<td>
												<a href="<?=admin_url('debtor_wallet', 'view', $u->id_debtor_wallet);?>">
													View 
												</a>
											</td>
									</tr>   
								<?php endforeach;?>
							<?php endif;?>
					  </tbody>
					  
					</table>            
	
	<?=$footer;?>