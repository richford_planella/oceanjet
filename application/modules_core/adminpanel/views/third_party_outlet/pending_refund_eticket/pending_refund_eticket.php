<?=$header;?>
		<div class="row-fluid sortable">		
			<div class="box span12">
				<div class="box-header" data-original-title>
					<h2><i class="icon-reorder"></i><span class="break"></span><b>Pending Refund E-ticket Requests List</b></h2>
					<div class="box-icon">
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
					  <thead>
						  <tr>
								<th class="align-center">E-Ticket No.</th>
								<th>Date of Refund Request</th>
								<th>Refund Amount</th>
								<th class="align-center">Actions</th>
						  </tr>
					  </thead>   
					  <tbody>
					
								<?php if(!empty($refund_eticket)):?>
									<?php foreach($refund_eticket as $u):?>
										<tr>
											<td class="align-center"><?=$u->id_booking;?></td>
											<td class="center"><?php echo $u->firstname." ".$u->lastname;?></td>
											<td class="center"><?php echo $u->firstname." ".$u->lastname;?></td>
											<td class="align-center">
												<a class="btn" href="<?=admin_url('pending_refund_eticket', 'view', $u->id_booking);?>" title="View" data-rel="tooltip">
													<i class="halflings-icon white zoom-in"></i>  
												</a>
												
											</td>
									</tr>   
								<?php endforeach;?>
							<?php endif;?>
					  </tbody>
					  
					</table>            
				</div>
			</div><!--/span-->
		</div><!--/row-->
	</div><!--/.fluid-container-->
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>	
<?=$footer;?>