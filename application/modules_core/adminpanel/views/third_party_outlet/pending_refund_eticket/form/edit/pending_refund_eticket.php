<?=$header;?>
<?php //echo '<pre>';print_r($eticket);?>
	<form class="form-horizontal" action="<?=current_url();?>" method="POST">
			<div class="row-fluid ">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>E-Ticket Details</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="commission_code">Name</label>
								<div class="controls">
								  <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?php echo $eticket->firstname." ".$eticket->lastname; ?>" readonly>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission">Voyage No.</label>
								<div class="controls">
									 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->voyage;?>" readonly>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission_percentage">Origin</label>
								<div class="controls">
									 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->origin;?>" readonly>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission_percentage">Destination</label>
								<div class="controls">
									 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->destination;?>" readonly>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission_percentage">ETD</label>
								<div class="controls">
									 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->ETD;?>" readonly>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="commission_percentage">ETA</label>
								<div class="controls">
									 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->ETA;?>" readonly>
								</div>
							</div>
							<div class="control-group">
									<label class="control-label" for="commission_percentage">Vessel</label>
									<div class="controls">
										 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->vessel;?>" readonly>
									</div>
								</div>
							<div class="control-group">
									<label class="control-label" for="commission_percentage">Accommodation Type</label>
									<div class="controls">
										 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->accommodation_code;?>" readonly>
									</div>
								</div>
							<div class="control-group">
									<label class="control-label" for="commission_percentage">Voyage Status</label>
									<div class="controls">
										 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->accommodation_code;?>" readonly>
									</div>
								</div>
							<div class="control-group">
									<label class="control-label" for="commission_percentage">Ticket Amount</label>
									<div class="controls">
										 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->accommodation_code;?>" readonly>
									</div>
								</div>
							<div class="control-group">
									<label class="control-label" for="commission_percentage">Refund Details</label>
									
								</div>
							<div class="control-group">
									<label class="control-label" for="commission_percentage">Refund Code</label>
									<div class="controls">
										 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->rule_code;?>" readonly>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="commission_percentage">Refund Charge</label>
									<div class="controls">
										 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->accommodation_code;?>" readonly>
									</div>
								</div>
						   		<div class="control-group">
									<label class="control-label" for="commission_percentage">Refund Amount</label>
									<div class="controls">
										 <input readonly="readonly" class="input-xlarge number focused commission_code filter_val" id="debtor_code" name="debtor_code" type="text" value="<?=$eticket->rule_type_amount;?>" readonly>
									</div>
								</div>
							  <div class="form-actions">
								<button type="submit" class="btn btn-primary" >Approve Refund Request</button>
								<a href="<?=admin_url($this->classname);?>" class="btn">Cancel</a>
							</div>
															
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->    
                    
	</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>