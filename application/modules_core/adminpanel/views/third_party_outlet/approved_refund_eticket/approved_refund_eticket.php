<?=$header;?>
<div class="row table-header-custom nopadding">
    <div class="col-xs-4">
        <div class="input-group">
            <input type="text" class="form-control" id="column3_search" placeholder="">
        </div>
    </div>
    <div class="col-xs-8 text-right">
   
    </div>
    <!-- /.col-lg-6 -->
</div>
<?=$this->load->view(admin_dir('notification'));?>						
<table id="main_table" class="example-table table global-table nopadding">
  <thead>
	  <tr>
			<th class="align-center">E-Ticket No.</th>
			<th>Date Refunded</th>
			<th>Amount</th>
			<th>Approved By</th>
			<th class="align-center">Actions</th>
	  </tr>
  </thead>   
  <tbody>

		<?php if(!empty($refund_eticket)):?>
			<?php foreach($refund_eticket as $u):?>
				<tr>
					<td class="align-center"><?=$u->ticket_no;?></td>
					<td class="center"><?=$u->date_refunded;?></td>
					<td class="align-center"><?=$u->refund_amount;?></td>
					<td class="align-center"><?=$u->user_id;?></td>
					<td class="align-center">
						<a href="<?=admin_url('approved_refund_eticket', 'view', $u->id_booking);?>" title="View" data-rel="tooltip">
							View  
						</a>
						
					</td>
			</tr>   
		<?php endforeach;?>
	<?php endif;?>
</tbody>  
</table>            
<?=$footer;?>