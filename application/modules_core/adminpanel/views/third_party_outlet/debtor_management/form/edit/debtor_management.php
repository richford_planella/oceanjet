<?=$header;?>
	<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
		
        	<?=$this->load->view(admin_dir('notification'));?>
        	
            	<div class="form-group">
		        	<div class="col-xs-3">
		        		<label class="control-label" for="debtor_code">Debtor Code</label>					
						<input disabled="disabled" class="form-control focused debtor_code" id="debtor_code" name="debtor_code" type="text" value="<?=set_value('debtor_code', $debtor->debtor_code);?>">					
		        	</div>
		        	<div class="col-xs-3">
		        		<label class="control-label" for="debtor_name">Debtor Name</label>					
						<select class="selectpicker form-control" id="enabled" name="user_id" data-rel="chosen">
							<option value="">Please Select</option>
							<?php foreach ($user as $u) { ?>
								<option value="<?=$u->id_user_account;?>" <?=set_select('user_id',$u->id_user_account,($u->id_user_account == $debtor->user_id) ? TRUE : '')?>><?php echo $u->firstname." ".$u->lastname;?></option>
							<?php } ?>
						</select>
						<span class="input-notes-bottom"><?=form_error('user_id')?></span>	
		        	</div>
		        	<div class="col-xs-3">
		        		<label class="control-label" for="enabled">Status</label>					
						<select class="selectpicker form-control" id="enabled" name="enabled" data-rel="chosen">
							<option value="1" <?=set_select('enabled', '1',(($debtor->enabled) ? TRUE : ''));?>>Active</option>
							<option value="0" <?=set_select('enabled', '0',((!$debtor->enabled) ? TRUE : ''));?>>Inactive</option>
						</select>					
		        	</div>
		        </div>
		    
    	
        <div class="form-group">
        	<div class="col-xs-12">
        		<h4>Outlet/s</h4>				
        	</div>
    		<div class="col-xs-5">
        		<label class="control-label" for="outlet_id" ><b>Select Available Outlet</b></label>
        		<div <div class="input-group">					
					<select  id="outlet_id" class="selectpicker form-control">
						<option value="">Please Select</option>
						<?php foreach ($outlet as $o) { ?> 
							<option rel="<?=$o->outlet_commission;?>" value="<?=$o->id_outlet;?>" ><?=$o->outlet;?></option>
						<?php } ?>
					</select>
					<span class="input-notes-bottom"><?=form_error('outlet_id[]')?></span>
					<div class="input-group-btn">
						<a href="" class="btn oj-button add-outlet">Add</a>
					</div>
				</div>
			</div>
		</div>
		
	
		<div class="form-group">
        	<div class="col-xs-12">
        		<table class="table global-table nopadding">
					<thead>
						<tr>
							<th>Outlet</th>
							<th>Commission</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody id="outlet_tbody">
						<?php if (!empty($selected_outlets)) { ?>
							<?php foreach ($selected_outlets as $okey) {?>
								<tr>
									<td>
										<?=$okey->outlet;?>
										<input id="test" class="form-control" type="hidden" name="outlet_id[]" value="<?=$okey->id_outlet;?>"/>
									</td>
									<td>
										<?=$okey->outlet_commission;?>								
									</td>
									<td>
										<a href="" rel="<?=$okey->id_outlet;?>" class="remove-outlet">Delete</a>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
						
				</table>
				
        	</div>
    	</div>
    	<div class="form-group oj-form-footer">
	      		<div class="col-xs-12">
	        		<div class="btn-oj-group right">
	          			<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
	          			<button type="submit" name="submit" value="submit" id="update-debtor-outlet" class="btn oj-button">Update</button>
	        		</div>
	      		</div>
    
</form>		    
<?=$footer;?>