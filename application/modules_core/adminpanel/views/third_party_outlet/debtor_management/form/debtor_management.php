<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
 
	<div class="form-group">
    	<div class="col-xs-3">
    		<label class="control-label" for="debtor_code">Debtor Code</label>					
			<input disabled="disabled" class="form-control debtor_code" id="debtor_code" name="debtor_code" type="text" value="<?=set_value('debtor_code', $debtor_management->debtor_code);?>">				
    	</div>
    	<div class="col-xs-3">
    		<label class="control-label" for="debtor_name">Debtor Name</label>					
			<?php foreach ($user as $u) { ?>
				<?php if ($u->id_user_account == $debtor_management->user_id) {?>
					<?php $fullname = $u->firstname.' '.$u->lastname;?>
						<input disabled="disabled" class="form-control debtor_name" id="debtor_name" name="debtor_name" type="text" value="<?=set_value('debtor_name', $fullname);?>">
				<?php } ?>											
			<?php } ?>
				
    	</div>
    	<div class="col-xs-3">
    		<label class="control-label" for="debtor_code">Status</label>					
			<input disabled="disabled" class="form-control focused enabled" id="outlet_address" name="enabled" type="text" value="<?=set_value('enabled', $debtor_management->enabled == 1 ? 'Active' : 'Inactive');?>">					
    	</div>
    </div>
    <div class="form-group">
    	<div class="col-xs-6">
    		<label class="control-label" for="debtor_code">Wallet Balance</label>					
			<input placeholder="0.00" disabled="disabled" class="form-control focused enabled" id="outlet_address" name="enabled" type="text" value="<?=set_value('amount', $debtor_management->amount);?>">					
    	</div>
    </div>
	 
	<div class="form-group margin-bottom-0">
		<div class="col-xs-4">
		      <h4>Outlet/s</h4>				
		</div>
	</div>
	
	<div class="form-group">
        <div class="col-xs-12">
        	<table class="table global-table nopadding">
				<thead>
					<tr>
						<th>Outlet</th>
						<th>Comission</th>
					</tr>
				</thead>
				<tbody id="outlet_tbody">
					<?php foreach ($debtor_outlet as $key_do => $val_do) {?>
						<tr id="outlet_tr col-xs-4">
							<?php foreach ($outlet as $key_o => $val_o) { ?>
								<?php if ($val_do->outlet_id == $val_o->id_outlet) { ?>
									<td>
										<?=$val_o->outlet;?>
									</td>
									<td>
										<?=$val_o->outlet_commission;?>
									</td>
								<?php }?>
							<?php } ?>

						<tr>
					<?php } ?>
				</tbody>
			</table>
        </div>
    </div>
	    <div class="form-group oj-form-footer">
	      	<div class="col-xs-12">
	        	<div class="btn-oj-group right">
	          		<a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
	          		<a class="btn oj-button" href="<?=admin_url($this->classname, 'edit', $debtor_management->id_debtor);?>">Edit</a>
	          	</div>
	        </div>
	    </div>
   
</form>	

<?=$footer;?>