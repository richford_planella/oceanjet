<?=$header;?>
   <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
		  <thead>
			  <tr>
					<th class="align-center">Debtor Code</th>
					<th>Debtor Name</th>
					<th>Outlet/s</th>
					<th>Commission</th>
					<th>Wallet Balance</th>
					<th class="align-center">Status</th>
					<th class="align-center">Actions</th>
			  </tr>
		  </thead>   
		  <tbody>
		 
			<?php if(!empty($debtor_info)){?>
			<?php //echo '<pre>'; print_r($debtor_information);?>
				<?php foreach($debtor_info as $d){?>
					<tr>
						<td class="align-center"><?=$d->debtor_code;?></td>
						<?php foreach($user as $u){?>
							<?php if ($u->user_id == $d->user_id) { ?>
								<td class="center"><?=$u->firstname?> <?=$u->lastname?></td>
							<?php } ?>
							
						<?php } ?>
						<td>
						<?php foreach ($debtor_outlet as $outlets){?>
							<?php if ($outlets->debtor_id == $d->id_debtor) {?>
								<?php echo $outlets->outlet.'<br/>';?>
							<?php } ?>
						<?php }?>
						</td>
						<td>
						<?php foreach ($debtor_outlet as $o){?>
							<?php if ($o->debtor_id == $d->id_debtor) {?>
								<?php echo $o->outlet_commission.'<br/>';?>
							<?php } ?>
						<?php }?>
						</td>  

						<td class="center"><?=$d->amount;?></td>
						<td><?=($d->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
						 <td>
		                    <a href="<?=admin_url($this->classname, 'view', $d->id_debtor);?>">View</a>
		                    <a id="del" rel="<?=$u->firstname?> <?=$u->lastname?>"href="<?=admin_url($this->classname, 'edit', $d->id_debtor);?>">Edit</a>
		                    <a href="<?=admin_url($this->classname, 'delete', $d->id_debtor);?>" class="delete-debtor">Delete</a>
		                    
		                    <?php if($d->enabled):?>
		                        <a href="<?=admin_url($this->classname, 'toggle', $d->id_debtor, md5($token.$this->classname.$d->id_debtor));?>" class="de-activate-debtor">
		                                De-activate
		                        </a>
		                    <?php else: ?>
		                        <a href="<?=admin_url($this->classname, 'toggle', $d->id_debtor, md5($token.$this->classname.$d->id_debtor));?>" class="activate-debtor">
		                                Re-activate
		                        </a>
		                    <?php endif;?>
		                </td>
					</tr>   
				<?php } ?>
			<?php } else {?>
			<tr><td>No result found</td></tr>
			<?php } ?>
		  </tbody>
		  
		</table>            
		
	<?=$footer;?>