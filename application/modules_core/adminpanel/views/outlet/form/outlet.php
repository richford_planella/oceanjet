<?=$header;?>

    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
	<?=$this->load->view(admin_dir('notification'));?>
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Outlet Code</label>
          <input class="form-control textToUpper" id="outlet_code" type="text" placeholder="Input outlet code" name="outlet_code" value="<?=set_value('outlet_code', $outlet->outlet_code);?>" readonly="readonly" />
          <span class="input-notes-bottom"><?=strip_tags(form_error('outlet_code'))?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Outlet Name</label>
          <input class="form-control" id="outlet" type="text" placeholder="Input outlet name" name="outlet" value="<?=set_value('outlet', $outlet->outlet);?>" readonly="readonly" />
          <span class="input-notes-bottom"><?=strip_tags(form_error('outlet'))?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Outlet Type</label>
			<input class="form-control" id="outlet_type" type="text" name="outlet_type" value="<?=($outlet->outlet_type == 'Internal') ? 'Internal' : 'External' ?>" readonly />
          <span class="input-notes-bottom"><?=strip_tags(form_error('outlet_type'))?></span>
        </div>
      </div>

      <div class="form-group">
		<div class="col-xs-3" id="port">
          <label class="control-label">Port</label>
			<?php 	foreach ($port as $pkey => $pvalue): ?>
				<?php if($outlet->port_id == $pvalue->id_port): ?>
				<input class="form-control" id="port_id" type="text" name="port_id" value="<?=isset($pvalue->id_port) ? '['.$pvalue->port_code.'] '.$pvalue->port : '' ?>" readonly />
				<?php endif; ?>
			<?php endforeach; ?>	
          <span class="input-notes-bottom"><?=strip_tags(form_error('port_id'))?></span>
        </div>
		
		<div class="col-xs-3" id="commission" style="display:none;">
          <label class="control-label">Commission</label>
          <input class="form-control" type="text" id="outlet_commission" placeholder="Input commission (%)" name="outlet_commission" value="<?=set_value('outlet_commission', $outlet->outlet_commission);?>" onkeypress='numeric_validation(event)' readonly="readonly" />
		  <span class="input-notes-bottom"><?=strip_tags(form_error('outlet_commission'))?></span>
        </div>
		
        <div class="col-xs-6">
          <label class="control-label">Address</label>
          <input class="form-control" id="outlet_address" type="text" placeholder="Input address" name="outlet_address" value="<?=set_value('outlet_address', $outlet->outlet_address);?>" readonly="readonly" />
		  <span class="input-notes-bottom"><?=strip_tags(form_error('outlet_address'))?></span>
        </div>
      </div>
	  
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Contact No.</label>
          <input class="form-control" type="text" id="outlet_contact_no" placeholder="Input contact no" name="outlet_contact_no" value="<?=set_value('outlet_contact_no', $outlet->outlet_contact_no);?>" disabled />
        </div>
        <div class="col-xs-3">
          <label class="control-label">Status</label>
    			<input class="form-control" id="enabled" type="text" name="enabled" value="<?=($outlet->enabled == '1') ? 'Active' : 'Inactive' ?>" readonly />
    			<span class="input-notes-bottom"><?=strip_tags(form_error('enabled'))?></span>
        </div>
      </div>

      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Back</a>
			<a class="btn oj-button" href="<?=admin_url($this->classname, 'edit', $outlet->id_outlet);?>">Edit</a>
          </div>
        </div>
      </div>
    </form>

<?=$footer;?>