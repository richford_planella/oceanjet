<?=$header;?>

    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Outlet Code</label>
          <input class="form-control textToUpper" id="outlet_code" type="text" placeholder="Input outlet code" name="outlet_code" value="<?=set_value('outlet_code');?>"/>
          <span class="input-notes-bottom"><?=form_error('outlet_code')?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Outlet Name</label>
          <input class="form-control" id="outlet" type="text" placeholder="Input outlet name" name="outlet" value="<?=set_value('outlet');?>" />
          <span class="input-notes-bottom"><?=form_error('outlet')?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Outlet Type</label>
    			<select class="selectpicker form-control" id="outlet_type" name="outlet_type">
    				<option value="">Please Select</option>
    				<option value="Internal" <?=set_select('outlet_type', 'Internal');?>>Internal</option>
    				<option value="External" <?=set_select('outlet_type', 'External');?>>External</option>
    			</select>
          <span class="input-notes-bottom"><?=form_error('outlet_type')?></span>
        </div>
      </div>

      <div class="form-group">
    		<div class="col-xs-3" id="port">
          <label class="control-label">Port</label>
    			<select class="selectpicker form-control" id="port_id" name="port_id">
    				<option value="">Please Select</option>
    				<?php foreach($port as $pkey => $pvalue): ?>
    					<option value="<?=$pvalue->id_port ?>" <?=set_select('port_id', $pvalue->id_port) ?>> <?="[".$pvalue->port_code."] ".$pvalue->port ?></option>
    				<?php endforeach; ?>
    			</select>
          <span class="input-notes-bottom"><?=form_error('port_id')?></span>
        </div>
	
    		<div class="col-xs-3" id="commission" style="display:none;">
          <label class="control-label">Commission</label>
          <input class="form-control price" type="number" id="outlet_commission" placeholder="Input commission (%)" name="outlet_commission" value="<?=set_value('outlet_commission', 0.00);?>" />
    		  <span class="input-notes-bottom"><?=form_error('outlet_commission')?></span>
        </div>
    		
        <div class="col-xs-6">
          <label class="control-label">Address</label>
          <input class="form-control" id="outlet_address" type="text" placeholder="Input address" name="outlet_address" value="<?=set_value('outlet_address');?>" />
  		    <span class="input-notes-bottom"><?=form_error('outlet_address')?></span>
        </div>
      </div>
	  
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Contact No.</label>
          <input class="form-control" type="text" id="outlet_contact_no" placeholder="Input contact no" name="outlet_contact_no" value="<?=set_value('outlet_contact_no');?>" />
          <span class="input-notes-bottom"><?=form_error('outlet_contact_no')?></span>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Status</label>
    			<select class="selectpicker form-control" id="enabled" name="enabled" data-rel="chosen">
    				<option value="1" <?=set_select('enabled', '1');?>>Active</option>
    				<option value="0" <?=set_select('enabled', '0');?>>Inactive</option>
    			</select>
    			<span class="input-notes-bottom"><?=form_error('enabled')?></span>
        </div>
      </div>

      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
            <button type="submit" class="btn oj-button">Save</button>
          </div>
        </div>
      </div>
    </form>

<?=$footer;?>