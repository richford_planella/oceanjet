<?=$header;?>
	                      
    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Rule Code</label>
          <input class="form-control textToUpper" type="text" placeholder="Input rule code" name="rule_code" value="<?=set_value('rule_code', $rule_set->rule_code);?>" readonly />
		  <span class="input-notes-bottom"><?=form_error('rule_code');?></span>
        </div>
        <div class="col-xs-6">
          <label class="control-label">Rule Name</label>
          <input class="form-control" type="text" placeholder="Input rule name" name="rule_set" value="<?=set_value('rule_set', $rule_set->rule_set);?>" readonly />
		  <span class="input-notes-bottom"><?=form_error('rule_set');?></span>
        </div>
      </div>
      <h5 class="oj-form__title">Rule Set</h5>
      <div class="form-group">
        <div class="col-xs-12">

          <!--Condition List Table-->
          <div class="accommodation-table accommodation-table--no-border">
            <table class="table table-fixed ">
              <thead>
                <tr>
                  <th class="col-xs-6">Rule Type</th>
                  <th class="col-xs-3">Rule Type Rate</th>
                  <th class="col-xs-3">Rule Amount/Percentage</th>
                </tr>
              </thead>
              <tbody class="accommodation-table--height-auto">
                
				<?php foreach($rule_type_set as $k => $p):?>
					<tr class="checkbox-fields">
					  <td class="col-xs-6">
						<div class="checkbox">
						  <label>
							<input 
								type="checkbox" 
								class="checkbox-fields__check" 
								name="rule_type[<?=$p->id_rule_type_name; ?>][id]" 
								value="<?=$p->id_rule_type_name; ?>" 
								<?=isset($p->rule_type_name_id) ? 'checked' : set_checkbox('rule_type['.$p->id_rule_type_name.'][id]', $p->id_rule_type_name); ?>> <?=$p->rule_type; ?>
						  </label>
						</div>
					  </td>
					  
					<?php if($p->id_rule_type_name != $free_baggage): ?>
					  
					  <td class="col-xs-3">
						<label class="radio-inline">
						  <input class="checkbox-fields__radio rad-<?=$p->id_rule_type_name;?>" type="radio" name="rule_type[<?=$p->id_rule_type_name;?>][amount_type]" value="amount" <?=set_radio("rule_type[".$p->id_rule_type_name."][amount_type]", 'amount'); ?> <?=isset($p->rule_amount_type) ? ($p->rule_amount_type == "amount" ? "checked" : FALSE) : FALSE; ?>>Amount
						</label>
						<label class="radio-inline">
						  <input class="checkbox-fields__radio rad-<?=$p->id_rule_type_name;?>" type="radio" name="rule_type[<?=$p->id_rule_type_name;?>][amount_type]" value="percentage" <?=set_radio("rule_type[".$p->id_rule_type_name."][amount_type]", 'percentage'); ?> <?=isset($p->rule_amount_type) ? ($p->rule_amount_type == "percentage" ? "checked" : FALSE) : FALSE; ?>>Percentage
						</label>
						<span class="input-notes-bottom"><?=form_error('rule_type['.$p->id_rule_type_name.'][amount_type]');?></span>
					  </td>
					  <td class="col-xs-3 ">
						<input type="number" min="0" step="1" class="form-control price" id="val-<?=$p->id_rule_type_name;?>" placeholder="Input value" name="rule_type[<?=$p->id_rule_type_name;?>][amount]" value="<?=set_value('rule_type['.$p->id_rule_type_name.'][amount]', isset($p->rule_type_amount) ? $p->rule_type_amount : 0.00);?>" />
						<span class="input-notes-bottom"><?=form_error('rule_type['.$p->id_rule_type_name.'][amount]');?></span>
					  </td>
					
					<?php else: ?>
					  <td class="col-xs-3">
						<span>N/A</span>
					  </td>
					  <td class="col-xs-3">
						<input  name="rule_type[<?=$p->id_rule_type_name;?>][amount_type]" type="hidden" value="weight" />
						<input type="number" min="0" step="1" class="form-control price" id="val-<?=$p->id_rule_type_name;?>" placeholder="Input value" name="rule_type[<?=$p->id_rule_type_name;?>][amount]" type="text" value="<?=set_value('rule_type['.$p->id_rule_type_name.'][amount]',  isset($p->rule_type_amount) ? $p->rule_type_amount : 0.00);?>" />
						<span class="input-notes-bottom"><?=form_error('rule_type['.$p->id_rule_type_name.'][amount]');?></span>
					  </td>
					
					<?php endif; ?>
					
					</tr>
				<?php endforeach;?>
				
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Status</label>
			<select class="selectpicker form-control" id="enabled" name="enabled" data-rel="chosen">
				<option value="1" <?=set_select('enabled', '1',(($rule_set->enabled) ? TRUE : ''));?>>Active</option>
				<option value="0" <?=set_select('enabled', '0',((!$rule_set->enabled) ? TRUE : ''));?>>Inactive</option>
			</select>
			<span class="input-notes-bottom"><?=form_error('enabled')?></span>
        </div>
      </div>
	  
      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
            <button type="submit" class="btn oj-button">Update</button>
          </div>
        </div>
      </div>
    </form>
            
<?=$footer;?>