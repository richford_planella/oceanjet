<?=$header;?>

      <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
          <div class="input-group">
            <input type="text" class="form-control" id="column3_search" placeholder="Search">
          </div>
        </div>
        <div class="col-xs-8 text-right">
			<a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>

      </div>
	  <?=$this->load->view(admin_dir('notification'));?>
      <!-- /Table -->
      <table id="main_table" class="table global-table nopadding">
        <thead>
          <tr>
			<th>Rule Code</th>
			<th>Rule Name</th>
			<th>Status</th>
			<th>Actions</th>
          </tr>
        </thead>
        <tbody>
			<?php if(!empty($rule_set)):?>
				<?php foreach($rule_set as $u):?>
					<tr>
						<td class="align-center"><?=$u->rule_code; ?></td>
						<td class="center"><?=$u->rule_set; ?></td>
						<td><?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
						<td>
							<a href="<?=admin_url($this->classname, 'view', $u->id_rule_set);?>">View</a>
							<a href="<?=admin_url($this->classname, 'edit', $u->id_rule_set);?>">Edit</a>
							<a href="<?=admin_url($this->classname, 'delete', $u->id_rule_set);?>" class="remove">Delete</a>
							<?php if($u->enabled):?>
								<a href="<?=admin_url($this->classname, 'toggle', $u->id_rule_set, md5($token.$this->classname.$u->id_rule_set));?>" class="deactivate">
										De-activate
								</a>
							<?php else: ?>
								<a href="<?=admin_url($this->classname, 'toggle', $u->id_rule_set, md5($token.$this->classname.$u->id_rule_set));?>" class="activate">
										Re-activate
								</a>
							<?php endif;?>
						</td>
					</tr>   
				<?php endforeach;?>
			<?php endif;?>
        </tbody>
      </table>
	
<?=$footer;?>