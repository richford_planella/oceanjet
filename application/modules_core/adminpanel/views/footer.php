        </div><!--PDetails Container-->
    </div>
    </section>

    <footer>
      <div class="container-fluid">
        <p>OceanJet <?=date('Y');?> | Powered by Yondu</p>
      </div>
    </footer>
    <script src="<?=js_dir('app.js');?>"></script>
    <script src="<?=js_dir('custom.js');?>"></script>
    <script src="<?=js_dir('jquery','jquery.currency-format-tools.js');?>"></script>
    <script src="<?=js_dir('jquery','jquery.formatCurrency.js');?>"></script>
    <script src="<?=js_dir('jquery','jquery.numeric-tools.js');?>"></script>
    <script src="<?=js_dir('jquery','jquery.numeric-tools.js');?>"></script>
    <?php if (isset($js_files) && ! empty($js_files)):?>
            <?php foreach($js_files as $js):?>
                    <script type="text/javascript" src="<?=$js;?>"></script>
            <?php endforeach;?>
    <?php endif;?>
</body>
</html>