<table id="trip-details-table" class="table global-table nopadding example-table">
  <thead>
    <tr>
      <th>Reference No. </th>
      <th>Shipper's Name </th>
      <th>Shipper's Contact No. </th>
      <th>Driver's Name </th>
      <th>Driver's Contact No. </th>
      <th>Detailed Description of RORO</th>
      <th>Total Amount</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php 
  if(!empty($booking_roro)): ?>
      <?php foreach($booking_roro as $bkey => $bvalue): ?>
      <tr>
        <td><?=$bvalue->reference_no ?></td>
        <td><?=$bvalue->sender_first_name." ".$bvalue->sender_last_name ?></td>
        <td><?=$bvalue->sender_contact_no ?></td>
        <td><?=$bvalue->driver_first_name." ".$bvalue->driver_last_name ?></td>
        <td><?=$bvalue->driver_contact_no ?></td>
        <td><?=$bvalue->rolling_cargo_description ?></td>
        <td><?="100 PHP" ?></td>
        <td>Re-Print</td>
      </tr>
      <?php endforeach; ?>
  <?php else: ?>
     <tbody>
      <tr>
        <td colspan="8">No Checked-In RORO</td>
      </tr>
    </tbody>
  <?php endif; ?>
  </tbody>
</table>
