<?=$header;?>

    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-2">
          <label class="control-label">Origin</label>
            <select class="selectpicker form-control" name="origin_id" id="origin">
      				<option value="">Please Select</option>
      				 <?php if($port):?>
      					<?php foreach($port as $p):?>
      						<option value="<?=$p->id_port;?>" <?=set_select('origin_id', $p->id_port);?>> <?=$p->port;?> </option>
      				<?php endforeach;?>
      				<?php endif;?>
          </select>
          <span class="form-control-feedback" aria-hidden="true"></span>
        </div>
		
		    <div class="col-xs-2">
          <label class="control-label">Destination</label>
            <select class="selectpicker form-control" name="destination_id" id="destination">
      				<option value="">Please Select</option>
      				 <?php if($port):?>
      					<?php foreach($port as $p):?>
      						<option value="<?=$p->id_port;?>" <?=set_select('destination_id', $p->id_port);?>> <?=$p->port;?> </option>
      				<?php endforeach;?>
      				<?php endif;?>
          </select>
          <span class="form-control-feedback" aria-hidden="true"></span>
        </div>

        <div class="col-xs-6">
          <label class="control-label">Voyage</label>
          <select class="selectpicker form-control" name="subvoyage_management_id" id="voyage">
			       <option value="">Please Select</option>
          </select>
        </div>

<!--         <div class="col-xs-2">
          <button class="btn oj-button oj-button--margin-top-30" type="button">View</button>
        </div> -->
      </div>

      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">Trip Details</h3>
          </div>
          <div class="form-group">
            <div class="col-xs-3">
              <label class="control-label">Voyage Code</label>
              <input class="form-control trip_details" type="text" id="voyage_code" placeholder="Voyage code" readonly />
            </div>
            <div class="col-xs-6 col-xs-offset-3">

              <div class="btn-oj-group oj-button--margin-top-30 text-right">
                <button class="btn oj-button" type="button" data-toggle="modal" data-target="#trip-details-modal">Checked-In RORO List</button>
                <button class="btn oj-button" type="button">Print Manifest</button>
              </div>
            </div>

          </div>

          <div class="form-group">
            <div class="col-xs-3 has-feedback">
              <label class="control-label">Depature Date</label>
              <input class="form-control oj-date-value trip_details" type="text" id="departure_date" value="02/20/2015" placeholder="Departure date" disabled />
              <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Origin</label>
              <input class="form-control trip_details" type="text" id="origin_port" placeholder="Origin" disabled />
            </div>
            <div class="col-xs-3">
              <label class="control-label">Destination</label>
              <input class="form-control trip_details" type="text" id="destination_port" placeholder="Destination" disabled />
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-3 has-feedback">
              <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
              <input class="form-control oj-time-picker trip_details" type="text" id="etd" placeholder="ETD" readonly />
              <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="col-xs-3 has-feedback">
              <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
              <input class="form-control oj-time-picker trip_details" type="text" id="eta" placeholder="ETA" readonly />
              <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Vessel</label>
              <input class="form-control trip_details" type="text" id="vessel" placeholder="Vessel" disabled />
            </div>
          </div>
        </div>
      </div>
      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">Voyage Status</h3>
          </div>
          <div class="form-group">
            <div class="col-xs-3">
              <label class="control-label">Status</label>
              <input class="form-control" id="subvoyage_management_status" name="subvoyage_management_status" type="text"
                     value="<?=set_value('subvoyage_management_status', $subvoyage_management_status);?>" placeholder="Subvoyage status"
                     readonly />
              <input type="hidden" name="subvoyage_management_status_id" id="subvoyage_management_status_id" value="<?=set_value('subvoyage_management_status_id',$subvoyage_management_status_id);?>">
              <input type="hidden" name="subvoyage_update_status_id" id="subvoyage_update_status_id" value="<?=set_value('subvoyage_update_status_id',$subvoyage_update_status_id);?>">

            </div>
            <div class="col-xs-9">
              <label class="control-label">Action Button</label>
              <div class="btn-oj-group">
                <button class="btn oj-button svm-update-status" type="button" id="checkin" value="9">Open for Check-In</button>
                <button class="btn oj-button svm-update-status" type="button" id="depart" value="2">Depart</button>
                <button class="btn oj-button svm-update-status" type="button" id="undepart" value="4">Undepart</button>
                <button class="btn oj-button svm-update-status" type="button" id="delayed" value="3">Delayed</button>
                <button class="btn oj-button svm-update-status" type="button" id="cancel" value="6">Cancel</button>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">Shipper's Details</h3>
          </div>
		      <div class="form-group">
			     <div class="col-xs-6">
              <label class="control-label">Ticket Number</label>
              <input class="form-control ticket" name="ticket_number" id="ticket_number" type="number" placeholder="Input ticket number" value="" maxlength="12" size="12" />
              <input class="form-control ticket" name="booking_roro_id" id="booking_roro_id" type="hidden" placeholder="" value="" />
            </div>		  
		      </div>
          <div class="form-group">
            <div class="col-xs-3">
              <label class="control-label">First Name</label>
              <input class="form-control ticket" type="text" placeholder="First Name" id="sender_first_name" disabled />
            </div>
            <div class="col-xs-3">
              <label class="control-label">last Name</label>
              <input class="form-control ticket" type="text" placeholder="Last Name" id="sender_last_name" disabled />
            </div>
            <div class="col-xs-2">
              <label class="control-label">Middle Initial</label>
              <input class="form-control ticket" type="text" placeholder="Middle Initial" id="sender_middle_name" disabled />
            </div>
            <div class="col-xs-2">
              <label class="control-label">Contact Number</label>
              <input class="form-control ticket" type="text" placeholder="Contact Number" id="sender_contact_no" disabled />
            </div>
            <div class="col-xs-2">
              <label class="control-label">Birthday</label>
              <input class="form-control ticket" type="text" placeholder="Birthday" id="sender_birthdate" disabled />
            </div>
          </div>
        </div>
      </div>
      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">Driver's Details</h3>
          </div>
          <div class="form-group">

            <div class="col-xs-3">
              <label class="control-label">First Name</label>
              <input class="form-control ticket" type="text" placeholder="First Name" id="driver_first_name" disabled />
            </div>
            <div class="col-xs-3">
              <label class="control-label">Last Name</label>
              <input class="form-control ticket" type="text" placeholder="Last Name" id="driver_last_name" disabled />
            </div>
            <div class="col-xs-2">
              <label class="control-label">Middle Initial</label>
              <input class="form-control ticket" type="text" placeholder="Middle Initial" id="driver_middle_name" disabled />
            </div>
            <div class="col-xs-2">
              <label class="control-label">Contact Number</label>
              <input class="form-control ticket" type="text" placeholder="Contact Number" id="driver_contact_no" disabled />
            </div>
            <div class="col-xs-2">
              <label class="control-label">Birthday</label>
              <input class="form-control ticket" type="text" placeholder="Birthday" id="driver_birthdate" disabled />
            </div>
          </div>
        </div>
      </div>
      <div class="form-group form-group__custom">
        <div class="col-xs-12">
          <div class="inner-header">
            <h3 class="inner-header__title">RORO Details</h3>
          </div>
          <div class="form-group">

            <div class="col-xs-9">
              <label class="control-label">Detailed Description of Cargo</label>
              <input class="form-control ticket" type="text" placeholder="Detailed description of rolling cargo" id="rolling_cargo_description" disabled />
            </div>
            <div class="col-xs-3">
              <label class="control-label">RORO Rate</label>
              <input class="form-control ticket" name="roro_rate" id="roro_rate" type="text" placeholder="RORO Rate" disabled />
            </div>

          </div>
        </div>
      </div>
      <div class="form-group form-group__footer">
        <div class="col-xs-3">
          <label class="control-label">Reference Number</label>
          <input class="form-control ticket" name="reference_number" id="reference_number" type="text" placeholder="RN0001" readonly />
        </div>
        <div class="col-xs-3">
          <label class="control-label">Total Amount</label>
          <input class="form-control ticket" name="total_amount" id="total_amount" type="text" placeholder="P XX.XX" readonly />
        </div>
        <div class="col-xs-6">
          <div class="btn-oj-group right oj-button--margin-top-30">
            <button type="button" class="btn oj-button" id="roro-checkin">Check-In</button>
            <button type="button" class="btn oj-button" id="roro-print">Print BIll of Lading</button>
          </div>
        </div>
      </div>

      <div id="trip-details-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-table-v1">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
              <h4 class="modal-title" id="myModalLabel">Trip Details</h4>
            </div>
            <div class="modal-body">
              <!-- /Table -->
              <div class="modal-form">
                <div class="form-group">
                  <div class="col-xs-3">
                    <label class="control-label">Voyage Code</label>
                    <input class="form-control trip_details" type="text" value="" id="voyage_code1" placeholder="Voyage code" readonly />
                  </div>
                  <div class="col-xs-3 has-feedback">
                    <label class="control-label">Depature Date</label>
                    <input class="form-control oj-date-value trip_details" type="text" value="" placeholder="Departure date" id="departure_date1" disabled />
                    <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Origin</label>
                    <input class="form-control trip_details" type="text" value="" placeholder="Origin" id="origin_port1" disabled />
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Destination</label>
                    <input class="form-control trip_details" type="text" value="" placeholder="Destination" id="destination_port1" disabled />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-xs-3 has-feedback">
                    <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                    <input class="form-control oj-time-picker trip_details" type="text" value="" placeholder="ETD" id="etd1" readonly />
                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                  </div>
                  <div class="col-xs-3 has-feedback">
                    <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                    <input class="form-control oj-time-picker" type="text" value="" placeholder="ETA" id="eta1" readonly />
                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Vessel</label>
                    <input class="form-control" type="text" value="" placeholder="Vessel" id="vessel1" disabled />
                  </div>
                </div>
              </div>
              <div class="row table-header-custom nopadding">
                <div class="col-xs-4 roro-list">
                  <h4>Checked-In RORO List</h4>
                </div>
                <div class="col-xs-8 text-right">
                  <a href="#" class="btn oj-button">Print Manifest</a>
                </div>
              </div>
              <table id="trip-details-table" class="table global-table nopadding example-table">
                <thead>
                  <tr>
                    <th>Reference No. </th>
                    <th>Shipper's Name </th>
                    <th>Shipper's Contact No. </th>
                    <th>Driver's Name </th>
                    <th>Driver's Contact No. </th>
                    <th>Detailed Description of RORO</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                  </tr>
                </thead>
                 <tbody>
                  <tr>
                    <td colspan="8">No Checked-In RORO</td>
                  </tr>
                </tbody>
              </table>

            </div>

          </div>
        </div>
      </div>
      <!--End-->

    </form>
	
<?=$footer;?>