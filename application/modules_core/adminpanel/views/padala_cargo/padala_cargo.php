<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
			<tr>
				<th>Padala Cargo Code</th>
				<th>Description</th>
				<th>Origin</th>
				<th>Destination</th>
				<th>Amount</th>
				<th>Unit of Measurement</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
        <tbody>
            <?php if(!empty($padala_cargo)):?>
			<?php foreach($padala_cargo as $u):?>
			<tr>
				<td><?=$u->padala_cargo?></td>
				<td><?=$u->description?></td>
				<td><?=$u->origin?></td>
				<td><?=$u->destination?></td>
				<td><?=$u->amount?></td>
				<td><?=$u->unit_of_measurement?></td>
				<td><?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
				<td>
					<a href="<?=admin_url($this->classname, 'view', $u->id_padala_cargo);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $u->id_padala_cargo);?>">Edit</a>
                    <a href="<?=admin_url($this->classname, 'delete', $u->id_padala_cargo);?>" class="btn-delete-padala-cargo-rate">Delete</a>
                    <?php if($u->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_padala_cargo, md5($token.$this->classname.$u->id_padala_cargo));?>"  class="btn-deactivate-padala-cargo-rate">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_padala_cargo, md5($token.$this->classname.$u->id_padala_cargo));?>"  class="btn-activate-padala-cargo-rate">
                                Re-activate
                        </a>
                    <?php endif;?>
				</td>
			</tr>   
			<?php endforeach;?>
			<?php endif;?>
        </tbody>
    </table>
<?=$footer;?>