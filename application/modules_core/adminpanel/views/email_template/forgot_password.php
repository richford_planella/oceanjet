<p>Dear <strong><?=$employee_name;?></strong>,</p>

<p>Your password has been reset.</p>

<p>Please login using your temporary password: <strong><?=$password;?></strong></p>

<p><strong>Mother link:</strong> <?=admin_url('login');?></p>

<p><strong>If you did not request this new password or if you have any question please contact your System administrator.</strong></p>
