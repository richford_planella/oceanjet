<p>Dear&nbsp;<strong><?=$employee_name;?></strong>,</p>

<p>Your Mother account was successfully created. Please use the account information below.</p>

<p><strong>username:</strong> <?=$username;?></p>

<p><strong>password:</strong> <?=$password;?></p>

<p><strong>Mother link:</strong> <?=admin_url('login');?></p>

<p>Please try to access your account and change your password immediately.</p>