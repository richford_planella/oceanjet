<p>Dear&nbsp;<strong><?=$employee_name;?></strong>,</p>

<p>Your submitted time report has been disapproved.</p>

<p>Please read the reason below and make the appropriate changes.</p>

<p>Disapproved by: <?=$username;?></p>
<p>Reason: <?=$reason;?></p>