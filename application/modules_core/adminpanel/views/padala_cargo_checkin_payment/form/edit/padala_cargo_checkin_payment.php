<?=$header;?>
	<form class="form-horizontal" action="<?=current_url();?>" method="POST">
			<div class="row-fluid ">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>Modify Loose Cargo Rate</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="loose_cargo">Loose Cargo Rate Code</label>
								<div class="controls">
									<input class="input-xlarge number focused loose_cargo" id="loose_cargo" name="loose_cargo" type="text" value="<?=set_value('loose_cargo', $loose_cargo->loose_cargo);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="description">Description</label>
								<div class="controls">
									<input class="input-xlarge number focused description" id="description" name="description" type="text" value="<?=set_value('description', $loose_cargo->description);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="voyage_id">Voyage</label>
								<div class="controls">
									<select id="voyage_id" name="voyage_id" data-rel="chosen">
										 <option value="">Please Select</option>
										 <?php
										 	foreach ($voyage_list as $key => $value) {
										 		if ($value->id_voyage == $loose_cargo->voyage_id)
										 			echo "<option value=".$value->id_voyage." selected>".$value->voyage."</option>";
										 		else
										 			echo "<option value=".$value->id_voyage.">".$value->voyage."</option>";
										 	}
										 ?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="amount">Amount</label>
								<div class="controls">
									<input class="input-xlarge number focused amount" id="amount" name="amount" type="text" value="<?=set_value('amount', $loose_cargo->amount);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="unit_of_measurement">Unit of Measurement</label>
								<div class="controls">
									<input class="input-xlarge number focused unit_of_measurement" id="unit_of_measurement" name="unit_of_measurement" type="text" value="<?=set_value('unit_of_measurement', $loose_cargo->unit_of_measurement);?>">
								</div>
							</div>

							<div class="control-group">
								  <label class="control-label" for="enabled">Status</label>
								  <div class="controls">
									<select id="enabled" name="enabled" data-rel="chosen">
										<option value="">Please Select</option>
										<option value="1" <?=set_select('enabled', '1',(($loose_cargo->enabled) ? TRUE : ''));?>>Active</option>
										<option value="0" <?=set_select('enabled', '0',((!$loose_cargo->enabled) ? TRUE : ''));?>>Inactive</option>
									</select>
								  </div>
							</div>
							<div class="form-actions">
								<button type="submit" class="btn btn-primary" >Save changes</button>
								<a href="<?=admin_url($this->classname);?>" class="btn">Cancel</a>
							</div>
															
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->    
                    
	</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>