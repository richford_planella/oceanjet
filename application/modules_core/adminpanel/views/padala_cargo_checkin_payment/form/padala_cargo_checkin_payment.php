<?=$header;?>
	<form class="form-horizontal" action="<?=current_url().'/add';?>" method="POST">
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-plus"></i><span class="break"></span><b>Padala Cargo Check-In</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<p class="center">
							<a class="btn btn-large btn-success" href="<?=current_url().'/view'; ?>"><i class="halflings-icon white plus"></i>Checked-in Loose Cargo List</a>
						</p>
						<fieldset>
								<div class="control-group">
									<label class="control-label" for="origin_id">Origin</label>
									<div class="controls">
										<input class="input-xlarge focused" id="origin_id" name="origin_id" type="text" value="<?=set_value('origin_id');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="departure_date">Destination ID</label>
									<div class="controls">
										<input class="input-xlarge focused" id="departure_date" name="departure_date" type="text" value="<?=set_value('date');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="voyage_id">Voyage No</label>
									<div class="controls">
										<select class="input-xlarge" id="voyage_id" name="voyage_id" data-rel="chosen">
											 <option value="">Please Select</option>
											 <?php
											 	foreach ($voyage_list as $key => $value) {
											 		echo "<option value=".$value->id_voyage.">[".$value->voyage."][".$value->origin."-".$value->destination."][".date("h:iA", strtotime($value->ETD))."-".date("h:iA", strtotime($value->ETA))."][".$value->vessel_code."]</option>";
											 	}
											 ?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="reference_no">Reference No.</label>
									<div class="controls">
										<input class="input-xlarge focused description" id="reference_no" name="reference_no" type="text" value="<?=set_value('reference_no', uniqid());?>" readonly>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="loose_cargo">Sender's Details</label>
									<div class="controls">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="sender_first_name">First Name</label>
									<div class="controls">
										<input class="input-xlarge focused sender_first_name" id="sender_first_name" name="sender_first_name" type="text" value="<?=set_value('sender_first_name');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="sender_last_name">Last Name</label>
									<div class="controls">
										<input class="input-xlarge focused sender_last_name" id="sender_last_name" name="sender_last_name" type="text" value="<?=set_value('sender_last_name');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="sender_middle_initial">Middle Initial</label>
									<div class="controls">
										<input class="input-xlarge focused sender_middle_initial" id="sender_middle_initial" name="sender_middle_initial" type="text" value="<?=set_value('sender_middle_initial');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="sender_contact_no">Contact No.</label>
									<div class="controls">
										<input class="input-xlarge focused sender_contact_no" id="sender_contact_no" name="sender_contact_no" type="text" value="<?=set_value('sender_contact_no');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="description">Recipient's Details</label>
									<div class="controls">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="recipient_first_name">First Name</label>
									<div class="controls">
										<input class="input-xlarge focused recipient_first_name" id="recipient_first_name" name="recipient_first_name" type="text" value="<?=set_value('recipient_first_name');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="recipient_last_name">Last Name</label>
									<div class="controls">
										<input class="input-xlarge focused recipient_last_name" id="recipient_last_name" name="recipient_last_name" type="text" value="<?=set_value('recipient_last_name');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="recipient_middle_initial">Middle Initial</label>
									<div class="controls">
										<input class="input-xlarge focused recipient_middle_initial" id="recipient_middle_initial" name="recipient_middle_initial" type="text" value="<?=set_value('recipient_middle_initial');?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="recipient_contact_no">Contact No.</label>
									<div class="controls">
										<input class="input-xlarge focused recipient_contact_no" id="recipient_contact_no" name="recipient_contact_no" type="text" value="<?=set_value('recipient_contact_no');?>">
									</div>
								</div>					
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->    
			<div class="row-fluid sortable">
            	<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-plus"></i><span class="break"></span><b>Detailed Description of Cargo</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable">
							<thead>
								<tr>
									<th>Particulars</th>
									<th>Loose Cargo Rate</th>
									<th>Quantity</th>
									<th>UOM</th>
									<th>Amount</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody id="cargo_description_tbody">
								<tr id="cargo_description_tr">
									<td><input class="particular" id="particular" name="particular[]" type="text" value="<?=set_value('particular');?>"></td>
									<td>
										<select class="cargo_description" name="loose_cargo[]" data-rel="">
											<option value="">Please Select</option>
											<?php
											 	foreach ($loose_cargo as $key => $value) {
											 		echo "<option value=".$value->id_loose_cargo." uom=".$value->unit_of_measurement." amount=".$value->amount.">".$value->loose_cargo."</option>";
											 	}
											 ?>
										</select>
									</td>
									<td><input class="quantity" id="quantity" name="quantity[]" type="number" value="0" min="1"/></td>
									<td><input class="unit_of_measurement" id="unit_of_measurement" name="unit_of_measurement[]" type="text" value="<?=set_value('unit_of_measurement');?>" readonly></td>
									<td><input class="amount" id="amount" name="amount[]" type="number" value="<?=set_value('amount',0);?>" readonly></td>
									<td>
										<a class="btn add-cargo" title="Add" data-rel="tooltip">
											<i class="halflings-icon white plus"></i>  
										</a>
										<a class="btn btn-info remove-cargo" title="Delete" data-rel="tooltip">
											<i class="halflings-icon white minus"></i>  
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
            	</div>
            </div>
            <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-plus"></i><span class="break"></span><b>Total</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<fieldset>
								<div class="control-group">
									<label class="control-label" for="total_amount">Total Amount</label>
									<div class="controls">
										<input class="input-xlarge focused" id="total_amount" name="total_amount" type="text" value="<?=set_value('total_amount',0);?>" readonly>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="amount_tendered">Amount Tendered</label>
									<div class="controls">
										<input class="input-xlarge focused amount_tendered" id="amount_tendered" name="amount_tendered" type="number" value="<?=set_value('amount_tendered',0);?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="change">Change</label>
									<div class="controls">
										<input class="input-xlarge focused change" id="change" name="change" type="text" value="<?=set_value('change',0);?>" readonly>
									</div>
								</div>	
								<button type="submit">Check-In</button>
								<a class="btn" title="Add" data-rel="tooltip" id="print_bill">
									<i class="halflings-icon white plus"></i>  Print Bill of Lading
								</a>
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->  
	</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>
<?=$footer;?>