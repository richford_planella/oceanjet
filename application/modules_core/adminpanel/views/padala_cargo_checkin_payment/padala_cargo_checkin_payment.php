<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
	<?=$this->load->view(admin_dir('notification'));?>
	  <div class="form-group">
	  	<div class="col-xs-2">
			<label class="control-label">Origin</label>
			<select class="selectpicker form-control" name="origin_id">
				<option value="">Please select</option>
				 <?php 	foreach ($port_list as $k => $v): ?>
					<option value="<?=$v->id_port ?>" <?=set_select('origin_id', $v->id_port)?>><?=$v->port_code ?></option>
				 <?php endforeach; ?>
			</select>
			<span class="input-notes-bottom"><?=form_error('origin_id')?></span>
		</div>
		<div class="col-xs-2">
			<label class="control-label">Destination</label>
			<select class="selectpicker form-control" name="destination_id">
				<option value="">Please select</option>
				 <?php 	foreach ($port_list as $k => $v): ?>
				 	<option value="<?=$v->id_port ?>" <?=set_select('destination_id', $v->id_port)?>><?=$v->port_code ?></option>
				 <?php endforeach; ?>
			</select>
			<span class="input-notes-bottom"><?=form_error('destination_id')?></span>
		</div>
		<div class="col-xs-6">
			<label class="control-label">Voyage</label>
			<select class="selectpicker form-control" name="subvoyage_management_id">
				<option value="">Please select</option>
			</select>
			<input type="hidden" value="<?=$subvoyage_management_id?>" id="subvoyage_management_id"/>
			<span class="input-notes-bottom"><?=form_error('subvoyage_management_id')?></span>
		</div>

	    <div class="col-xs-2">
	      <!--<button class="btn oj-button oj-button--margin-top-30" type="button" id="btn-view">View</button>-->
	    </div>
	  </div>

	  <div class="form-group form-group__custom" id="div-trip-details">
	    <div class="col-xs-12">
	      <div class="inner-header">
	        <h3 class="inner-header__title">Trip Details</h3>
	      </div>
	      <div class="form-group">
	        <div class="col-xs-3">
	          <label class="control-label">Voyage Code</label>
	          <input class="form-control" type="text" placeholder="Voyage Code" id="trip-voyage-code" disabled/>
	        </div>
	        <div class="col-xs-3 col-xs-offset-6">
	          <!--<button class="btn oj-button oj-button--margin-top-30" type="button" id="btn-padala-cargo-list">View Baggage List</button>-->
	        </div>
	      </div>

	      <div class="form-group">
	        <div class="col-xs-3 has-feedback">
	          <label class="control-label">Depature Date</label>
	          <input class="form-control oj-date-value" type="text" placeholder="YYYY-MM-DD" value="Departure Date" id="trip-departure-date" disabled/>
	          <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
	        </div>
	        <div class="col-xs-3">
	          <label class="control-label">Origin</label>
	          <input class="form-control" type="text" placeholder="Origin" id="trip-origin" disabled/>
	        </div>
	        <div class="col-xs-3">
	          <label class="control-label">Destination</label>
	          <input class="form-control" type="text" placeholder="Destination" id="trip-destination" disabled/>
	        </div>
	      </div>
	      <div class="form-group">
	        <div class="col-xs-3 has-feedback">
	          <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
	          <input class="form-control oj-time-picker" type="text" placeholder="00:00:00" id="trip-ETD" readonly/>
	          <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
	        </div>
	        <div class="col-xs-3 has-feedback">
	          <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
	          <input class="form-control oj-time-picker" type="text" placeholder="00:00:00" id="trip-ETA" readonly/>
	          <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
	        </div>
	        <div class="col-xs-3">
	          <label class="control-label">Vessel</label>
	          <input class="form-control" type="text" id="trip-vessel" placeholder="Vessel" disabled/>
	        </div>
	      </div>
	    </div>
	  </div>


	<div class="form-group form-group__custom" id="div-cargo-details">
	  <div class="col-xs-12">
	    <div class="inner-header">
	      <h3 class="inner-header__title">Cargo Details</h3>
	    </div>
	    <div class="form-group">
	    <input type="hidden" name="checkin_id" id="checkin_id"/>
	      <div class="col-xs-4">
	        <label class="control-label">Reference Number</label>
	        <input class="form-control" type="text" placeholder="Input reference number" id="reference_no" value="<?=set_value('reference_no');?>" name="reference_no"/>
	        <span class="input-notes-bottom"><?=form_error('reference_no')?></span>
	      </div>
	      <div class="col-xs-3">
	        <button class="btn oj-button oj-button--margin-top-30" type="button" id="btn-search-ref-no">Go</button>
	      </div>
	    </div>
	    <div class="form-group">

	      <div class="col-xs-4">
	        <label class="control-label">Padala Cargo Rate</label>
	        <input class="form-control" type="text" placeholder="Padala cargo rate" id="padala-cargo-rate" readonly/>
	      </div>
	      <div class="col-xs-4">
	        <label class="control-label">Particulars</label>
	        <input class="form-control" type="text" placeholder="Particulars" id="particulars" readonly/>
	      </div>

	    </div>
	    <div class="form-group">

	      <div class="col-xs-4">
	        <label class="control-label">Sender</label>
	        <input class="form-control" type="text" placeholder="Sender" id="sender" readonly/>
	      </div>
	      <div class="col-xs-4">
	        <label class="control-label">Contact No. of Sender</label>
	        <input class="form-control" type="text" placeholder="Contact No. of Sender" id="sender-contact-no" readonly/>
	      </div>

	    </div>
	    <div class="form-group">

	      <div class="col-xs-4">
	        <label class="control-label">Recipient</label>
	        <input class="form-control" type="text" placeholder="Recipient" id="recipient" readonly/>
	      </div>
	      <div class="col-xs-4">
	        <label class="control-label">Contact No. of Recipient</label>
	        <input class="form-control" type="text" placeholder="Contact No. of Recipient" id="recipient-contact-no" readonly/>
	      </div>

	    </div>
	  </div>
	</div>



	<div class="form-group form-group__footer">

	  <div class="col-xs-3">
	    <label class="control-label">Total Amount</label>
	    <input class="form-control" type="text" placeholder="0.00" id="total-amount" name="total_amount" readonly/>
	    <span class="input-notes-bottom"><?=form_error('total_amount')?></span>
	  </div>
	  <div class="col-xs-3">
	    <label class="control-label">Amount Tendered</label>
	    <input class="form-control" type="number" placeholder="0.00" id="amount-tendered" name="amount_tendered" onkeypress="numeric_validation(event)" step="0.01" value="<?=set_value('amount_tendered');?>"/>
	    <span class="input-notes-bottom"><?=form_error('amount_tendered')?></span>
	  </div>
	  <div class="col-xs-3">
	    <label class="control-label">Change</label>
	    <input class="form-control" type="text" placeholder="0.00" id="change" readonly/>
	  </div>
	  <div class="col-xs-3">
	    <div class="btn-oj-group right oj-button--margin-top-30">
	      <button type="submit" class="btn oj-button" id="btn-pay">Pay</button>
	      <button type="submit" class="btn oj-button">Print Bill of Lading</button>
	    </div>
	  </div>
	</div>


	</form>
<?=$footer;?>