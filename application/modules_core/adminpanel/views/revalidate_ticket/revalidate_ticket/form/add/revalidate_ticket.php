<?=$header;?>
<form class="form-horizontal oj-form " action="<?=admin_url($this->classname,'add');?>" method="POST">
<?=$this->load->view(admin_dir('notification'));
	$data = $this->session->userdata('revalidate_session_data');
	$user_id = $this->session->get('admin','administrator_profile_id');
	
	var_dump($data);
?>

	<div class="form-group">
		<div class="col-xs-3">
			<label class="control-label">Origin</label>
			<select class="selectpicker form-control ticket-orig-dest" id="origin" name="origin" title="Choose">
				 <?php
					foreach ($port_options as $key => $value) {
						echo "<option value=". $value->id_port .">". $value->port ."</option>";
					}
				 ?>
			</select>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">Destination</label>
			<select class="selectpicker form-control ticket-orig-dest" id="destination" name="destination" title="Choose">
				 <?php
					foreach ($port_options as $key => $value) {
						echo "<option value=". $value->id_port .">". $value->port ."</option>";
					}
				 ?>
			</select>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">Voyage</label>
			<div class="select-holder">
				<select class="selectpicker form-control select-voyage" id="voyage" name="voyage" title="Choose">
				</select>
			</div>
			<span class="input-notes-bottom"><?=form_error('voyage')?></span>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">&nbsp;</label>
			<div class="btn-oj-group">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname, 'add');?>">Clear</a>
				<button type="submit" class="btn oj-button">Submit</button>
			</div>
		</div>
	</div>
	
	<!-- START TICKET DETAILS -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Ticket Details</span></div>
		
		<div class="oj-box2-content">
			<input type="hidden" name="booking_id" value="<?=$data['ticket_details'][0]->id_booking;?>" />
			<input type="hidden" name="user_id" value="<?=$user_id;?>" />
			<div class="form-group">
				<div class="col-xs-6">
					<label class="control-label">Name</label>
					<input type="text" id="name" name="name" class="form-control" value="<?=$data['ticket_details'][0]->fullname;?>" disabled />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Voyage Code</label>
					<input type="text" id="voyage_code" name="voyage_code" class="form-control" value="<?=$data['ticket_details'][0]->voyage_code;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Origin</label>
					<input type="text" id="origin" name="origin" class="form-control" value="<?=$data['ticket_details'][0]->origin;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Destination</label>
					<input type="text" id="destination" name="destination" class="form-control" value="<?=$data['ticket_details'][0]->destination;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Departure Date</label>
					<input type="text" id="departure_date" name="departure_date" class="form-control" value="<?=$data['ticket_details'][0]->departure_date;?>" disabled />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Departure Time (ETD)</label>
					<input type="text" id="etd" name="etd" class="form-control" value="<?=$data['ticket_details'][0]->ETD;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Arrival Time (ETA)</label>
					<input type="text" id="eta" name="eta" class="form-control" value="<?=$data['ticket_details'][0]->ETA;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Vessel</label>
					<input type="text" id="vessel" name="vessel" class="form-control" value="<?=$data['ticket_details'][0]->vessel;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Accommodation Type</label>
					<input type="text" id="accommodation" name="accommodation" class="form-control" value="<?=$data['ticket_details'][0]->accommodation;?>" disabled />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Voyage Status</label>
					<input type="text" id="voyage_status" name="voyage_status" class="form-control" value="<?=$data['ticket_details'][0]->voyage_status;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Ticket Amount</label>
					<input type="text" id="ticket_amount" name="ticket_amount" class="form-control" value="<?=$data['ticket_details'][0]->total_amount;?>" disabled />
				</div>
			</div>
			<br />
			<hr />
			<br />
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Rule Code</label>
					<input type="text" id="rule_code" name="rule_code" class="form-control" value="<?=$data['ticket_details'][0]->rule_code;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Refund Charge</label>
					<input type="text" id="refund_charge" name="refund_charge" class="form-control" value="<?=$data['ticket_details'][0]->rule_code;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Refund Amount</label>
					<input type="text" id="refund_amount" name="refund_amount" class="form-control" value="<?=$data['ticket_details'][0]->rule_code;?>" disabled />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Rule Code</label>
					<input type="text" id="rule_code" name="rule_code" class="form-control" value="<?=$data['ticket_details'][0]->rule_code;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Refund Charge</label>
					<input type="text" id="refund_charge" name="refund_charge" class="form-control" value="<?=$data['ticket_details'][0]->rule_code;?>" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Refund Amount</label>
					<input type="text" id="refund_amount" name="refund_amount" class="form-control" value="<?=$data['ticket_details'][0]->rule_code;?>" disabled />
				</div>
			</div>
		</div>
	</div>
	<br />
	<!-- END TICKET DETAILS -->
	
	<!-- START FORM BUTTONS FOOTER -->
	<div class="form-group oj-form-footer">
		<div class="col-xs-12">
			<div class="btn-oj-group left">
				<button type="submit" class="btn oj-button">Revalidate</button>
			</div>
		</div>
	</div>
	<!-- END FORM BUTTONS FOOTER -->
	
</form>
<?=$footer;?>