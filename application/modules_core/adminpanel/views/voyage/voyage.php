<?= $header; ?>
<!-- /Table Header -->
<div class="row table-header-custom nopadding">
    <div class="col-xs-4">
        <div class="input-group">
            <input type="text" class="form-control" id="column3_search" placeholder="Search">
        </div>
    </div>
    <div class="col-xs-8 text-right">
        <a href="<?= admin_url($this->classname, 'add'); ?>" class="btn oj-button">Create</a>
    </div>
    <!-- /.col-lg-6 -->
</div>
<?= $this->load->view(admin_dir('notification')); ?>
<!-- /Table -->
<table id="main_table" class="example-table table global-table nopadding">
    <thead>
        <tr>
            <th>Voyage Code</th>
            <th>No. of Legs</th>
            <th>Origin</th>
            <th>Destination</th>
            <th>ETD</th>
            <th>ETA</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($voyage)): ?>
            <?php foreach ($voyage as $u): ?>
                <tr>
                    <td><?= $u->voyage ?></td>
                    <td><?= $u->no_of_subvoyages ?></td>
                    <td><?= $u->origin ?></td>
                    <td><?= $u->destination ?></td>
                    <td><?= date_format(new DateTime($u->ETD), 'g:i a'); ?></td>
                    <td><?= date_format(new DateTime($u->ETA), 'g:i a'); ?></td>
                    <td><?= ($u->enabled) ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>'; ?></td>
                    <td>
                        <a href="<?= admin_url($this->classname, 'view', $u->id_voyage); ?>">View</a>
                        <a href="<?= admin_url($this->classname, 'edit', $u->id_voyage); ?>">Edit</a>
                        <a href="<?= admin_url($this->classname, 'delete', $u->id_voyage); ?>" class="btn-delete-voyage">Delete</a>
                        <?php if ($u->enabled): ?>
                            <a href="<?= admin_url($this->classname, 'toggle', $u->id_voyage, md5($token . $this->classname . $u->id_voyage)); ?>"  class="btn-deactivate-voyage">
                                De-activate
                            </a>
                        <?php else: ?>
                            <a href="<?= admin_url($this->classname, 'toggle', $u->id_voyage, md5($token . $this->classname . $u->id_voyage)); ?>"  class="btn-activate-voyage">
                                Re-activate
                            </a>
                        <?php endif; ?>
                    </td>
                </tr>   
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>
<?= $footer; ?>