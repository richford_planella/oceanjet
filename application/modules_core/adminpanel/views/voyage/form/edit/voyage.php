<?= $header; ?>
<form class="form-horizontal oj-form " action="<?= current_url(); ?>" method="POST">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">Voyage Code</label>
            <input class="form-control voyage textToUpper" type="text" placeholder="Input Number" name="voyage" value="<?= set_value('voyage', $voyage->voyage); ?>" autocomplete="off" readonly/>
            <span class="input-notes-bottom"><?= form_error('voyage') ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Status</label>
            <select class="selectpicker form-control" title="Choose" name="enabled">
                <option value="1" <?= set_select('enabled', '1', (($voyage->enabled) ? TRUE : '')); ?>>Active</option>
                <option value="0" <?= set_select('enabled', '0', ((!$voyage->enabled) ? TRUE : '')); ?>>Inactive</option>
            </select>
            <span class="input-notes-bottom"><?= form_error('enabled') ?></span>
        </div>
        <div class="col-xs-3 text-right">
            <!--<button id="add-leg-btn" class="btn oj-button oj-button--margin-top-30" type="button">Add Leg</button>-->
        </div>
    </div>
    <div id="oj-leg--voyage-cms">
        <?php $i = 1; ?>
        <?php foreach ($subvoyages as $key => $value) : ?>
            <input type="hidden" name="id_subvoyage[]" value="<?= $value->id_subvoyage; ?>"/>
            <div class="form-group oj-leg">
                <div class="col-lg-12">
                    <div class="input-group">
                        <div class="oj-leg__title oj-leg__title--gray">
                            Leg <?= $i++ ?>
                        </div>
                        <span class="input-group-btn">
                            <!--<button class="btn btn-default oj-leg__btn" type="button">Remove</button>-->
                        </span>
                    </div>
                </div>
                <div class="col-xs-12 oj-leg__body">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label class="control-label">Origin</label>
                            <?php foreach ($port_list as $k => $v): ?>
                                <?php if ($value->origin_id == $v->id_port): ?>
                                    <input class="form-control" type="text" value="<?= isset($v->id_port) ? $v->port : '' ?>" readonly />
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <span class="input-notes-bottom"><?= form_error('origin_id[]') ?></span>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Destination</label>
                            <?php foreach ($port_list as $k => $v): ?>
                                <?php if ($value->destination_id == $v->id_port): ?>
                                    <input class="form-control" type="text" value="<?= isset($v->id_port) ? $v->port : '' ?>" readonly />
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <span class="input-notes-bottom"><?= form_error('destination_id[]') ?></span>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Default Vessel</label>
                            <select class="selectpicker form-control" title="Choose" name="vessel_id[]">
                                <?php foreach ($vessel_list as $k => $v) : ?>
                                    <?php if ($value->vessel_id == $v->id_vessel): ?>
                                        <option value="<?= $v->id_vessel ?>" selected><?= $v->vessel ?></option>
                                    <?php else: ?>
                                        <option value="<?= $v->id_vessel ?>" <?= set_select('vessel_id[]', $v->id_vessel); ?>><?= $v->vessel ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                            <span class="input-notes-bottom"><?= form_error('vessel_id[]') ?></span>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Points Redemption</label>
                            <input class="form-control points_redemption price" type="number" placeholder="Input Number" name="points_redemption[]" step="0.01" value="<?= set_value('points_redemption[]', $value->points_redemption); ?>" onkeypress="numeric_validation(event)"/>
                            <span class="input-notes-bottom"><?= form_error('points_redemption[]') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-3 has-feedback">
                            <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                            <input class="form-control oj-time-picker oj-etd" type="text" placeholder="HH:MM" name="ETD[]" value="<?= set_value('ETD[]', $value->ETD); ?>"/>
                            <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                            <span class="input-notes-bottom"><?= form_error('ETD[' . $key . ']') ?></span>
                        </div>
                        <div class="col-xs-3 has-feedback">
                            <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                            <input class="form-control oj-time-picker oj-eta" type="text" placeholder="HH:MM" name="ETA[]" value="<?= set_value('ETA[]', $value->ETA); ?>"/>
                            <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                            <span class="input-notes-bottom"><?= form_error('ETA[' . $key . ']') ?></span>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Travel Time</label>
                            <input class="form-control oj-time-picker oj-travel-time" type="text" value="300" readonly/>
                            <span class="input-notes-bottom"></span>
                        </div>
                    </div>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a class="btn oj-button gray-button" href="<?= admin_url($this->classname); ?>">Cancel</a>
                <button type="submit" class="btn oj-button">Update</button>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="add-type-btn" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Accommodation Type</label>
                                <select class="selectpicker form-control" title="Choose">
                                    <option value="">Choose one</option>
                                    <option value="">Choose two</option>
                                    <option value="">Choose three</option>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Passenger Fare</label>
                                <select class="selectpicker form-control" title="Choose">
                                    <option value="">Choose one</option>
                                    <option value="">Choose two</option>
                                    <option value="">Choose three</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn oj-button">Add</button>
                </div>

            </div>
        </div>
    </div>
</form>
<?= $footer; ?>