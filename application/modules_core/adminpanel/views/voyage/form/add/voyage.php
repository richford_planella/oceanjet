<?= $header; ?>
<form class="form-horizontal oj-form " action="<?= current_url(); ?>" method="POST">
    <input type="hidden" id="leg_count" name="leg_count" value="<?= set_value('leg_count', $leg_count); ?>"/>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Voyage Code</label>
            <input class="form-control voyage textToUpper" type="text" placeholder="Input voyage code" name="voyage" value="<?= set_value('voyage'); ?>" autocomplete="off"/>
            <span class="input-notes-bottom"><?= form_error('voyage') ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Status</label>
            <select class="selectpicker form-control" name="enabled">
                <option value="1" <?= set_select('enabled', '1'); ?>>Active</option>
                <option value="0" <?= set_select('enabled', '0'); ?>>Inactive</option>
            </select>
            <span class="input-notes-bottom"><?= form_error('enabled') ?></span>
        </div>
        <div class="col-xs-3 text-left">
            <button id="add-leg-btn" class="btn oj-button oj-button--margin-top-30" type="button">Add Leg</button>
        </div>
    </div>
    <div id="oj-leg--voyage-cms">
        <?php $leg_no = 1; ?>
        <?php for ($i = 0; $i < $leg_count; $i++) : ?>
            <div class="form-group oj-leg">
                <div class="col-lg-12">
                    <div class="input-group">
                        <div class="oj-leg__title oj-leg__title--gray">
                            Leg <?= $leg_no++ ?>
                        </div>
                        <span class="input-group-btn">
                            <button class="btn btn-default oj-leg__btn" type="button">Remove</button>
                        </span>
                    </div>
                </div>
                <div class="col-xs-12 oj-leg__body">
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label class="control-label">Origin</label>
                            <select class="selectpicker form-control" name="origin_id[]">
                                <option value="">Please select</option>
                                <?php foreach ($port_list as $key => $value) : ?>
                                    <option value="<?= $value->id_port ?>" <?= set_select("origin_id[" . $i . "]", $value->id_port); ?>><?= $value->port ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="input-notes-bottom"><?= form_error('origin_id[' . $i . ']') ?></span>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Destination</label>
                            <select class="selectpicker form-control" name="destination_id[]">
                                <option value="">Please select</option>
                                <?php foreach ($port_list as $key => $value) : ?>
                                    <option value="<?= $value->id_port ?>" <?= set_select("destination_id[" . $i . "]", $value->id_port); ?>><?= $value->port ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="input-notes-bottom"><?= form_error('destination_id[' . $i . ']') ?></span>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Default Vessel</label>
                            <select class="selectpicker form-control" name="vessel_id[]">
                                <option value="">Please select</option>
                                <?php foreach ($vessel_list as $key => $value) : ?>
                                    <option value="<?= $value->id_vessel ?>" <?= set_select("vessel_id[" . $i . "]", $value->id_vessel) ?>><?= $value->vessel ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="input-notes-bottom"><?= form_error('vessel_id[' . $i . ']') ?></span>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Points Redemption</label>
                            <input class="form-control points_redemption price" type="number" placeholder="Input points redemption" name="points_redemption[]" value="<?= set_value('points_redemption[' . $i . ']'); ?>" step="0.01" onkeypress="numeric_validation(event)"/>
                            <span class="input-notes-bottom"><?= form_error('points_redemption[' . $i . ']') ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-3 has-feedback">
                            <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                            <input class="form-control oj-time-picker oj-etd" type="text" placeholder="HH:MM" name="ETD[]" value="<?= set_value('ETD[' . $i . ']') ?>" />
                            <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                            <span class="input-notes-bottom"><?= form_error('ETD[' . $i . ']') ?></span>
                        </div>
                        <div class="col-xs-3 has-feedback">
                            <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                            <input class="form-control oj-time-picker oj-eta" type="text" placeholder="HH:MM" name="ETA[]" value="<?= set_value('ETA[' . $i . ']') ?>" />
                            <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                            <span class="input-notes-bottom"><?= form_error('ETA[' . $i . ']') ?></span>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Travel Time</label>
                            <input class="form-control oj-time-picker oj-travel-time" type="text" value="300" readonly/>
                            <span class="input-notes-bottom"></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a class="btn oj-button gray-button" href="<?= admin_url($this->classname); ?>">Cancel</a>
                <button type="submit" class="btn oj-button" id="btn-submit">Save</button>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="add-type-btn" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Accommodation Type</label>
                                <select class="selectpicker form-control" title="Choose">
                                    <option value="">Choose one</option>
                                    <option value="">Choose two</option>
                                    <option value="">Choose three</option>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Passenger Fare</label>
                                <select class="selectpicker form-control" title="Choose">
                                    <option value="">Choose one</option>
                                    <option value="">Choose two</option>
                                    <option value="">Choose three</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn oj-button">Add</button>
                </div>

            </div>
        </div>
    </div>
</form>
<?= $footer; ?>