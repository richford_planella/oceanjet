<?=$header;?>
	<form class="form-horizontal" action="<?=current_url().'/edit/1';?>" method="POST">
			<div class="row-fluid ">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>Issue Passenger Ticket</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<div id="trip_details">
							<div class="control-group">
								<label class="control-label" for="voyage">Voyage</label>
								<div class="controls">
									<select id="voyage" name="voyage" data-rel="chosen">
										<option value="">Please Select</option>
									</select>
								</div>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="firstname">First Name</label>
							<div class="controls">
								<input class="input-xlarge number focused firstname filter_val" id="firstname" name="firstname" type="text" value="">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="lastname">Last Name</label>
							<div class="controls">
								<input class="input-xlarge number focused lastname filter_val" id="lastname" name="lastname" type="text" value="">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="birthday">Birthday</label>
							<div class="controls">
								<input type="date" class="form-control" placeholder="Birthday" name="birthday" id="birthday" required autocomplete="off" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="age">Age</label>
							<div class="controls">
								<input class="input-xlarge number focused age filter_val" id="age" name="age" type="text" value="">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="gender">Gender</label>
							<div class="controls">
								<select id="gender" name="gender" data-rel="chosen">
									<option value="">Please Select</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="contact_no">Contact Number</label>
							<div class="controls">
								<input class="input-xlarge number focused contact_no filter_val" id="contact_no" name="contact_no" type="text" value="">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="id_no">ID Number</label>
							<div class="controls">
								<input class="input-xlarge number focused id_no filter_val" id="id_no" name="id_no" type="text" value="">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="nationality">Nationality</label>
							<div class="controls">
								<input class="input-xlarge number focused nationality filter_val" id="nationality" name="nationality" type="text" value="">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="with_infant">With Infant</label>
							<div class="controls">
								<input type="checkbox" value="1" id="with_infant" name="with_infant" class="input-xlarge number focused with_infant filter_val" />
							</div>
						</div>
						<div class="control-group infant_name_container hide">
							<label class="control-label" for="infant_name">Infant's Name</label>
							<div class="controls">
								<input class="input-xlarge number focused infant_name filter_val" id="infant_name" name="infant_name" type="text" value="">
							</div>
						</div>
						
						<div class="form-actions">
							<button type="submit" class="btn btn-primary" >Save changes</button>
							<a href="<?=admin_url($this->classname);?>" class="btn">Cancel</a>
						</div>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->    
                    
	</form>
	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>