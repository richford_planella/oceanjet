<?=$header;?>
<form class="form-horizontal oj-form " action="<?=admin_url($this->classname,'issue');?>" method="POST">
<?=$this->load->view(admin_dir('notification'));?>
	<div class="form-group">
		<div class="col-xs-3">
			<label class="control-label">Origin</label>
			<select class="selectpicker form-control ticket-orig-dest" id="origin" name="origin" title="Choose">
				 <?php
					foreach ($port_options as $key => $value) {
						echo "<option value=". $value->id_port .">". $value->port ."</option>";
					}
				 ?>
			</select>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">Destination</label>
			<select class="selectpicker form-control ticket-orig-dest" id="destination" name="destination" title="Choose">
				 <?php
					foreach ($port_options as $key => $value) {
						echo "<option value=". $value->id_port .">". $value->port ."</option>";
					}
				 ?>
			</select>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">Voyage</label>
			<div class="select-holder">
				<select class="selectpicker form-control select-voyage" id="voyage" name="voyage" title="Choose">
				</select>
			</div>
			<span class="input-notes-bottom"><?=form_error('voyage')?></span>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">&nbsp;</label>
			<div class="btn-oj-group right">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Clear</a>
				<button type="submit" class="btn oj-button">Submit</button>
			</div>
		</div>
	</div>
  
	<!--
	<div class="form-group oj-form-footer">
		<div class="col-xs-12">
			<div class="btn-oj-group right">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
				<button type="submit" class="btn oj-button">Submit</button>
			</div>
		</div>
	</div>
	-->
</form>
<?=$footer;?>