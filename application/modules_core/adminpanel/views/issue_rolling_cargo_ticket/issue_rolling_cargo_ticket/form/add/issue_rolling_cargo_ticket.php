<?=$header;?>
<form class="form-horizontal oj-form " action="<?=admin_url($this->classname,'add');?>" method="POST">
<?=$this->load->view(admin_dir('notification'));
	$data = $this->session->userdata('rolling_cargo_session_data');
	
	var_dump($data);
?>
	<div class="form-group">
		<div class="col-xs-3">
			<label class="control-label">Origin</label>
			<select class="selectpicker form-control ticket-orig-dest" id="origin" name="origin" title="Choose">
				 <?php
					foreach ($port_options as $key => $value) {
						echo "<option value=". $value->id_port .">". $value->port ."</option>";
					}
				 ?>
			</select>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">Destination</label>
			<select class="selectpicker form-control ticket-orig-dest" id="destination" name="destination" title="Choose">
				 <?php
					foreach ($port_options as $key => $value) {
						echo "<option value=". $value->id_port .">". $value->port ."</option>";
					}
				 ?>
			</select>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">Voyage</label>
			<div class="select-holder">
				<select class="selectpicker form-control select-voyage" id="voyage" name="voyage" title="Choose">
				</select>
			</div>
			<span class="input-notes-bottom"><?=form_error('voyage')?></span>
		</div>
		
		<div class="col-xs-3">
			<label class="control-label">&nbsp;</label>
			<div class="btn-oj-group">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname, 'add');?>">Clear</a>
				<button type="submit" class="btn oj-button">Submit</button>
			</div>
		</div>
	</div>
	
	<!-- START TRIP DETAILS -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Trip Details</span></div>
		
		<?php
		// Voyage Details
		$voyage_id = "";
		$voyage_code = "";
		$voyage_origin = "";
		$voyage_destination = "";
		foreach($data['trip_details'] as $key=>$value) {
			$voyage_id .= $value->voyage_id;
			$voyage_code .= $value->voyage_code;
			$voyage_origin .= $value->port_origin;
			$voyage_destination .= $value->port_destination;
		}
		
		// Ticket Details
		$ticket_id = "";
		$ticket_no = "";
		foreach($data['ticket_details'] as $key=>$value) {
			$ticket_id .= $value->id_ticket_series_info;
			$ticket_no .= $value->ticket_no;
		}
		?>
		<div class="oj-box2-content">
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Voyage Code</label>
					<input type="text" id="voyage_code" class="form-control" value="<?php echo $voyage_code;?>" disabled />
					<input type="hidden" name="voyage_code" value="<?php echo $voyage_id; ?>" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Ticket Number</label>
					<input type="text" id="ticket_number" class="form-control" value="<?php echo $ticket_no; ?>" disabled />
					<input type="hidden" name="ticket_number" value="<?php echo $ticket_id; ?>" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Origin</label>
					<input type="text" id="trip_origin" class="form-control" value="<?php echo $voyage_origin; ?>" disabled />
					<input type="hidden" name="trip_origin" value="" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Destination</label>
					<input type="text" id="trip_destination" class="form-control" value="<?php echo $voyage_destination; ?>" disabled />
					<input type="hidden" name="trip_destination" value="" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Departure Time (ETD)</label>
					<input type="text" id="trip_etd" name="trip_etd" class="form-control" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Arrival Time (ETA)</label>
					<input type="text" id="trip_eta" name="trip_eta" class="form-control" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Vessel</label>
					<input type="text" id="trip_vessel" name="trip_vessel" class="form-control" disabled />
				</div>
			</div>
		</div>
	</div>
	<br />
	<!-- END TRIP DETAILS -->
	
	<!-- START SHIPPER/DRIVER INFORMATION -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Shipper/Driver Information</span></div>
		
		<div class="oj-box2-content">
			<label class="control-label">Shipper's Details</label>
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">First Name</label>
					<input type="text" id="shipper_firstname" name="shipper_firstname" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Last Name</label>
					<input type="text" id="shipper_lastname" name="shipper_lastname" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Middle Initial</label>
					<input type="text" id="shipper_middlename" name="shipper_middlename" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Contact Number</label>
					<input type="text" id="shipper_contactno" name="shipper_contactno" class="form-control" />
				</div>
			</div>
			
			<label class="control-label">Driver's Details</label>
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">First Name</label>
					<input type="text" id="driver_firstname" name="driver_firstname" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Last Name</label>
					<input type="text" id="driver_lastname" name="driver_lastname" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Middle Initial</label>
					<input type="text" id="driver_middlename" name="driver_middlename" class="form-control" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Contact Number</label>
					<input type="text" id="driver_contactno" name="driver_contactno" class="form-control" />
				</div>
			</div>
		</div>
	</div>
	<br />
	<!-- END SHIPPER/DRIVER INFORMATION -->
  
	<!-- START RATE DETAILS -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Rate Details</span></div>
		
		<div class="oj-box2-content">
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">RORO Rate Code</label>
					<select class="selectpicker form-control" id="roro_rate_code" name="roro_rate_code">
						<option value="0">Male</option>
						<option value="1">Female</option>
					</select>
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Discount Code</label>
					<select class="selectpicker form-control" id="discount_code" name="discount_code">
						<option value="0">Male</option>
						<option value="1">Female</option>
					</select>
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Mode of Payment</label>
					<select class="selectpicker form-control" id="mode_of_payment" name="mode_of_payment">
						<option value="0">Male</option>
						<option value="1">Female</option>
					</select>
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Reference Number</label>
					<input type="text" id="reference_no" name="reference_no" class="form-control" />
				</div>
			</div>
		</div>
	</div>
	<br />
	<!-- END RATE DETAILS -->
  
	<!-- START TOTAL -->
	<div class="oj-box2">
		<div class="oj-box2-header"><span class="ojb-title">Total</span></div>
		
		<div class="oj-box2-content">
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">RORO Rate</label>
					<input type="text" class="form-control" id="roro_rate" name="roro_rate" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Discount</label>
					<input type="text" class="form-control" id="discount" name="discount" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Terminal Fee</label>
					<input type="text" class="form-control" id="terminal_fee" name="terminal_fee" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Port Charge</label>
					<input type="text" class="form-control" id="port_charge" name="port_charge" disabled />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-3">
					<label class="control-label">Amount Tendered</label>
					<input type="text" class="form-control" id="amount_tendered" name="amount_tendered" />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Change</label>
					<input type="text" class="form-control" id="change" name="change" disabled />
				</div>
				
				<div class="col-xs-3">
					<label class="control-label">Total Due</label>
					<input type="text" class="form-control" id="total_due" name="total_due" disabled />
				</div>
			</div>
		</div>
	</div>
	<!-- END TOTAL -->
	
	<!-- START FORM BUTTONS FOOTER -->
	<div class="form-group oj-form-footer">
		<div class="col-xs-12">
			<div class="btn-oj-group left">
				<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Pay</a>
				<button type="submit" class="btn oj-button">Print Ticket</button>
			</div>
		</div>
	</div>
	<!-- END FORM BUTTONS FOOTER -->
	
</form>
<?=$footer;?>