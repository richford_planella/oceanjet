<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>Announcement Code</th>
                <th>Title</th>
                <th>Content</th>
                <th>Visibility</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($announcement)):?>
            <?php foreach($announcement as $u):?>
            <tr>
                <td><?=$u->announcement_code;?></td>
                <td><?=$u->announcement;?></td>
                <td><?=word_limiter($u->announcement_content,25);?></td>
                <td><?=implode(", ", $u->announcement_visibility);?></td>
                <td><?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
                <td>
                    <a href="<?=admin_url($this->classname, 'view', $u->id_announcement);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $u->id_announcement);?>">Edit</a>
                    <a href="<?=admin_url($this->classname, 'delete', $u->id_announcement);?>" class="remove">Delete</a>
                    <?php if($u->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_announcement, md5($token.$this->classname.$u->id_announcement));?>" class="deactivate">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_announcement, md5($token.$this->classname.$u->id_announcement));?>" class="activate">
                                Re-activate
                        </a>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
<?=$footer;?>