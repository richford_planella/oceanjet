<?=$header;?>
	
    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Announcement Code</label>
          <input name="announcement_code" class="form-control textToUpper" type="text" placeholder="Input announcement code" value="<?=set_value('announcement_code', $announcement->announcement_code);?>" readonly />
		  <span class="input-notes-bottom"><?=form_error('announcement_code')?></span>
        </div>
        <div class="col-xs-5">
          <label class="control-label">Title</label>
          <input name="announcement" class="form-control" type="text" placeholder="Input title" value="<?=set_value('announcement', $announcement->announcement);?>" readonly />
		  <span class="input-notes-bottom"><?=form_error('announcement')?></span>
        </div>

      </div>
      <div class="form-group">
        <div class="col-xs-12">
          <label class="control-label">Content</label>
          <div><?=$announcement->announcement_content;?></div>
		  <span class="input-notes-bottom"><?=form_error('announcement_content')?></span>
        </div>
      </div>
      <div class="form-group">
        <div class="col-xs-9">
          <label class="control-label">Visibility</label>
          <div class="checkbox-wrapper three-column user-visibility">
            <label class="checkbox-inline">
              <input type="checkbox" id="checkAll" value="option" disabled> All
            </label>
			<?php foreach($announcement->user_profile as $up):?>
            <label class="checkbox-inline">
				<input name="announcement_visibility[<?=$up->id_user_profile;?>]" class="chkrole" type="checkbox" value="<?=$up->id_user_profile;?>" <?=set_checkbox('announcement_visibility['.$up->id_user_profile.']', $up->id_user_profile);?> <?=isset($up->is_checked) ? "checked": FALSE; ?> disabled> <?=$up->user_profile?>
            </label>
			<?php endforeach;?>
			
          </div>
        </div>
		  <div class="form-group">
			<div class="col-xs-3">
			  <label class="control-label">Status</label>
				<input class="form-control" id="enabled" type="text" name="enabled" value="<?=($announcement->enabled == '1') ? 'Active' : 'Inactive' ?>" readonly />
				<span class="input-notes-bottom"><?=form_error('enabled')?></span>
			</div>
		  </div>
      </div>

     <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Back</a>
			<a class="btn oj-button" href="<?=admin_url($this->classname, 'edit', $announcement->id_announcement);?>">Edit</a>
          </div>
        </div>
      </div>  
    </form>
            
<?=$footer;?>