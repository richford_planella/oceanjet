<?=$header;?>
	
    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Announcement Code</label>
          <input name="announcement_code" class="form-control textToUpper" type="text" placeholder="Input announcement code" value="<?=set_value('announcement_code', $announcement->announcement_code);?>" readonly />
		  <span class="input-notes-bottom"><?=form_error('announcement_code')?></span>
        </div>
        <div class="col-xs-5">
          <label class="control-label">Title</label>
          <input name="announcement" class="form-control" type="text" placeholder="Input title" value="<?=set_value('announcement', $announcement->announcement);?>" />
		  <span class="input-notes-bottom"><?=form_error('announcement')?></span>
        </div>

      </div>
      <div class="form-group">
        <div class="col-xs-12">
          <label class="control-label">Content</label>
          <textarea name="announcement_content" class="form-control" rows="5" placeholder="No content available"><?=set_value('announcement_content', $announcement->announcement_content);?></textarea>
		  <span class="input-notes-bottom"><?=form_error('announcement_content')?></span>
        </div>
      </div>
      <div class="form-group">
        <div class="col-xs-9">
          <label class="control-label">Visibility</label>
          <div class="checkbox-wrapper three-column user-visibility">
            <label class="checkbox-inline">
              <input type="checkbox" id="checkAll" value="option"> All
            </label>
			<?php foreach($announcement->user_profile as $up):?>
            <label class="checkbox-inline">
				<input name="announcement_visibility[<?=$up->id_user_profile;?>]" class="chkrole" type="checkbox" value="<?=$up->id_user_profile;?>" <?=set_checkbox('announcement_visibility['.$up->id_user_profile.']', $up->id_user_profile);?> <?=isset($up->is_checked) ? "checked": FALSE; ?>> <?=$up->user_profile?>
            </label>
			<?php endforeach;?>
			
          </div>
        </div>
        <div class="col-xs-3">
          <label class="control-label">Status</label>
          <select class="selectpicker form-control" id="enabled" name="enabled" data-rel="chosen">
			<option value="1" <?=set_select('enabled', '1',(($announcement->enabled) ? TRUE : ''));?>>Active</option>
			<option value="0" <?=set_select('enabled', '0',((!$announcement->enabled) ? TRUE : ''));?>>Inactive</option>
		 </select>
		 <span class="input-notes-bottom"><?=form_error('enabled')?></span>
        </div>
      </div>

      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
		  <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
            <button type="submit" class="btn oj-button">Update</button>
          </div>
        </div>
      </div>
    </form>
            
<?=$footer;?>