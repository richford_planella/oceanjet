<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>Discount Code</th>
                <th>Discount Name</th>
                <th>Amount/Percentage</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($discount)):?>
            <?php foreach($discount as $u):?>
            <tr>
                <td><?=$u->discount_code;?></td>
                <td><?=$u->discount;?></td>
                <?php if($u->discount_type == "amount"): ?>
					<td class="center">P<?=$u->discount_amount;?>
				<?php else: ?>
					<td class="center"><?=$u->discount_amount;?>%
				<?php endif; ?>
                <td><?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
                <td>
                    <a href="<?=admin_url($this->classname, 'view', $u->id_discount);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $u->id_discount);?>">Edit</a>
                    <a href="<?=admin_url($this->classname, 'delete', $u->id_discount);?>" class="remove">Delete</a>
                    <?php if($u->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_discount, md5($token.$this->classname.$u->id_discount));?>" class="deactivate">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $u->id_discount, md5($token.$this->classname.$u->id_discount));?>" class="activate">
                                Re-activate
                        </a>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
<?=$footer;?>