<?=$header;?>

    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Discount Code</label>
          <input class="form-control textToUpper" id="discount_code" type="text" placeholder="Input Discount Code" name="discount_code" value="<?=set_value('discount_code', $discount->discount_code);?>" readonly />
          <span class="input-notes-bottom"><?=form_error('discount_code')?></span>
        </div>
        <div class="col-xs-6">
          <label class="control-label">Discount Name</label>
          <input class="form-control" id="discount" type="text" placeholder="Input Discount Name" name="discount" value="<?=set_value('discount', $discount->discount);?>" readonly />
          <span class="input-notes-bottom"><?=form_error('discount')?></span>
        </div>
      </div>

      <div class="form-group">
		<div class="col-xs-3">
          <label class="control-label">Discount Type</label>
			<input class="form-control" id="discount_type" type="text" placeholder="Input Discount Type" name="discount_type" value="<?=($discount->discount_type == 'percentage') ? 'Percentage' : 'Amount' ?>" readonly />
          <span class="input-notes-bottom"><?=form_error('discount_type')?></span>
        </div>
		
		<div class="col-xs-3">
		  <label class="control-label" id="lbldiscount"><?=($discount->discount_type =='amount' ) ? 'Amount (P)' : 'Percentage (%)';?></label>
          <input class="form-control price" id="discount_amount" type="number" placeholder="Input Amount/Percentage" name="discount_amount" value="<?=set_value('discount_amount',  $discount->discount_amount);?>" readonly />
          <span class="input-notes-bottom"><?=form_error('discount_amount')?></span>
        </div>

        <div class="col-xs-6">
          <label class="control-label">Conditions</label>
          <div class="checkbox-wrapper">
            <label class="checkbox-inline">
              <input type="checkbox" id="age-bracket" value="1" name="conditions[age_bracket]" <?=set_checkbox('conditions[age_bracket]', 1, !empty($discount->with_age) ? true : false);?> disabled />Age Bracket
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckbox2" value="1" name="conditions[vat_exempt]" <?=set_checkbox('conditions[vat_exempt]', 1, $discount->vat_exempt == 1 ? true : false);?> disabled />Vat-Exempt
            </label>
			<label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckbox3" value="1" name="conditions[require_id]" <?=set_checkbox('conditions[require_id]', 1, $discount->require_id == 1 ? true : false);?> disabled />Require ID
            </label>
            <div class="age-range-wrap">
              <div class="age-box">
                <label class="col-xs-3 control-label">From</label>
                <div class="col-xs-3">
                  <input type="number" class="form-control age" id="age-from" placeholder="0" name="age_from" value="<?=set_value('age_from', $discount->age_from);?>" readonly />
				</div>
              </div>
              <div class="age-box">
                <label  class="col-xs-2 control-label">To</label>
                <div class="col-xs-3">
                  <input type="number" class="form-control age" id="age-to" placeholder="0"  name="age_to" value="<?=set_value('age_to',  $discount->age_to);?>" readonly />
			    </div>
              </div>
            </div>
          </div>
         </div>
      </div>
	  
	  <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Status</label>
			<input class="form-control" id="enabled" type="text" name="enabled" value="<?=($discount->enabled == '1') ? 'Active' : 'Inactive' ?>" readonly />
			<span class="input-notes-bottom"><?=form_error('enabled')?></span>
        </div>
      </div>

     <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Back</a>
			<a class="btn oj-button" href="<?=admin_url($this->classname, 'edit', $discount->id_discount);?>">Edit</a>
          </div>
        </div>
      </div>            
    </form>

<?=$footer;?>