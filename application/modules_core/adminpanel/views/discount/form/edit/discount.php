<?=$header;?>

    <form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Discount Code</label>
          <input class="form-control textToUpper" id="discount_code" type="text" placeholder="Input discount code" name="discount_code" value="<?=set_value('discount_code', $discount->discount_code);?>" readonly />
          <span class="input-notes-bottom"><?=form_error('discount_code')?></span>
        </div>
        <div class="col-xs-6">
          <label class="control-label">Discount Name</label>
          <input class="form-control" id="discount" type="text" placeholder="Input discount name" name="discount" value="<?=set_value('discount', $discount->discount);?>" readonly />
          <span class="input-notes-bottom"><?=form_error('discount')?></span>
        </div>
      </div>

      <div class="form-group">
		<div class="col-xs-3">
          <label class="control-label">Discount Type</label>
			<select class="selectpicker form-control" id="discount_type" name="discount_type">
				<option value="">Please Select</option>
				<option value="amount" <?=set_select('discount_type', 'amount', (($discount->discount_type == 'amount') ? TRUE : ''));?>>Amount</option>
				<option value="percentage" <?=set_select('discount_type', 'percentage', (($discount->discount_type == 'percentage') ? TRUE : ''));?>>Percentage</option>
			</select>
          <span class="input-notes-bottom"><?=form_error('discount_type')?></span>
        </div>
		
		<div class="col-xs-3">
		  <label class="control-label" id="lbldiscount"><?=($discount->discount_type =='amount' ) ? 'Amount (P)' : 'Percentage (%)';?></label>
          <input class="form-control price" id="discount_amount" type="number" min="0" placeholder="Input amount/percentage" name="discount_amount" value="<?=set_value('discount_amount',  $discount->discount_amount);?>"/>
          <span class="input-notes-bottom"><?=form_error('discount_amount')?></span>
        </div>

        <div class="col-xs-6">
          <label class="control-label">Conditions</label>
		  <span class="input-notes-bottom"><?=form_error()?></span>
          <div class="checkbox-wrapper">
            <label class="checkbox-inline">
              <input type="checkbox" id="age-bracket" value="1" name="conditions[age_bracket]" <?=set_checkbox('conditions[age_bracket]', 1, !empty($discount->with_age) ? true : false);?> />Age Bracket
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckbox2" value="1" name="conditions[vat_exempt]" <?=set_checkbox('conditions[vat_exempt]', 1, $discount->vat_exempt == 1 ? true : false);?> />Vat-Exempt
            </label>
			<label class="checkbox-inline">
              <input type="checkbox" id="inlineCheckbox3" value="1" name="conditions[require_id]" <?=set_checkbox('conditions[require_id]', 1, $discount->require_id == 1 ? true : false);?> />Require ID
            </label>
            <div class="age-range-wrap">
              <div class="age-box">
                <label class="col-xs-3 control-label">From</label>
                <div class="col-xs-3">
                  <input type="number" class="form-control age" id="age-from" placeholder="0" name="age_from" min="0" value="<?=set_value('age_from', $discount->age_from);?>" />
				  <span class="input-notes-bottom"><?=form_error('age_from')?></span>
				</div>
              </div>
              <div class="age-box">
                <label  class="col-xs-2 control-label">To</label>
                <div class="col-xs-3">
                  <input type="number" class="form-control age" id="age-to" placeholder="0"  name="age_to" min="1" value="<?=set_value('age_to',  $discount->age_to);?>" />
				  <span class="input-notes-bottom"><?=form_error('age_to')?></span>
                </div>
              </div>
            </div>
          </div>
         </div>
      </div>
	  
	  <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Status</label>
			<select class="selectpicker form-control" id="enabled" name="enabled" data-rel="chosen">
				<option value="1" <?=set_select('enabled', '1', (($discount->enabled) ? TRUE : ''));?>>Active</option>
				<option value="0" <?=set_select('enabled', '0', ((!$discount->enabled) ? TRUE : ''));?>>Inactive</option>
			</select>
			<span class="input-notes-bottom"><?=form_error('enabled')?></span>
        </div>
      </div>

      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
            <button type="submit" class="btn oj-button">Update</button>
          </div>
        </div>
      </div>
    </form>

<?=$footer;?>