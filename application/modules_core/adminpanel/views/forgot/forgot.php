<?=$header;?>		
    <form action="" method="post">
            <?=$this->load->view(admin_dir('notification'));?>
            <div class="input-container row clearfix">
                <label for="username">Username</label>
                <span class="error-message"><?php echo form_error('username'); ?></span>
                <input type="text" class="login-input " id="username" name="username" value="<?=set_value('username');?>" />
            </div>
            <div class="input-container row clearfix">
                <label for="email_address">Email Address</label>
                <span class="error-message"><?php echo form_error('email_address'); ?></span>
                <input type="text" class="login-input error" id="email_address" name="email_address" />
            </div>
            <div class="input-container row clearfix login-btn-group">
                <a class="btn oj-button light-gray-button" href="<?=admin_url('login');?>" role="button">Back</a>
                <input class="btn oj-button orange-button " type="submit" value="Submit">
            </div>
    </form>          
<?=$footer;?>