<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <?php error_reporting(0); ?>
    <title>Boarding Pass</title>
    <style type="text/css">
        body {font-family: sans-serif;
            font-size: 10pt;
        }
        p {    margin: 0pt;
        }
        td { vertical-align: top; }
        .items td {
            border-left: 0.1mm solid #000000;
            border-right: 0.1mm solid #000000;
        }
        table thead td { background-color: #EEEEEE;
            text-align: center;
            border: 0.1mm solid #000000;
        }
    </style>

</head>

<body>

<table>
    <tr>
        <td width="460px">
            <table class="table" width="460px">
                <tbody>
                <tr><td colspan="2"> <b>Boarding Pass</b> <td></tr>
                <?php
                foreach ($passenger_details as $pd)
                {
                    echo "<tr><td>Reference No: </td><td> ".$pd->transaction_ref ."</td></tr>";
                    echo "<tr><td>Passenger’s Name:</td><td> ".$pd->lastname.", ".$pd->firstname." ".substr($pd->middlename,0,1).".</td></tr>";
                    echo "<tr><td>Voyage Details </td>";
                    foreach($voyage_details as $vd) {
                        echo "<td>
                                    Voyage:                " . $vd->voyage . "<br/>
                                    Origin - Destination:  " . $vd->port_origin ." - ". $vd->port_destination . "<br/>
                                    Departure Date:        " .  date("F j, Y - D", strtotime($vd->departure_date)) . "<br/>
                                    Time:                  " . date('g:i a',strtotime($vd->ETD)) ."-". date('g:i a',strtotime($vd->ETA)) ."<br/>
                                    Vessel:                " . $vd->vessel_code . "<br/>
                                  </td>
                              </tr>";
                    }
                    echo "<tr><td> Ticket No.: </td>";
                    echo "<td> ". $pd->ticket_no ."</td></tr>";
                    echo "<tr><td> Seat No.: </td>";
                    echo "<td> ". $pd->vessel_seats ."</td></tr>";
                }
                ?>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td width="460px">
            <table class="table" width="460px">
                <tbody>
                <tr><td colspan="2"> <b>Boarding Pass</b> <td></tr>
                <?php
                foreach ($passenger_details as $pd)
                {
                    echo "<tr><td>Reference No: </td><td> ".$pd->transaction_ref ."</td></tr>";
                    echo "<tr><td>Passenger’s Name:</td><td> ".$pd->lastname.", ".$pd->firstname." ".substr($pd->middlename,0,1).".</td></tr>";
                    echo "<tr><td>Voyage Details </td>";
                    foreach($voyage_details as $vd) {
                        echo "<td>
                                    Voyage:                " . $vd->voyage . "<br/>
                                    Origin - Destination:  " . $vd->port_origin ." - ". $vd->port_destination . "<br/>
                                    Departure Date:        " .  date("F j, Y - D", strtotime($vd->departure_date)) . "<br/>
                                    Time:                  " . date('g:i a',strtotime($vd->ETD)) ."-". date('g:i a',strtotime($vd->ETA)) ."<br/>
                                    Vessel:                " . $vd->vessel_code . "<br/>
                                  </td>
                              </tr>";
                    }
                    echo "<tr><td> Ticket No.: </td>";
                    echo "<td> ". $pd->ticket_no ."</td></tr>";
                    echo "<tr><td> Seat No.: </td>";
                    echo "<td> ". $pd->vessel_seats ."</td></tr>";
                }
                ?>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
</body></html>
