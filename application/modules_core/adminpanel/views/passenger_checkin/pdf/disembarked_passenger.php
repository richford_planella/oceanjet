<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <?php error_reporting(0); ?>
    <title>Passenger Manifest</title>
    <style type="text/css">
        body {
            font-family: "Times New Roman";
            font-size: 8pt;
            text-align: center;
        }
        p {    margin: 0pt;
        }
        td { vertical-align: top; }
        .items td {
            border-left: 0.1mm solid #000000;
            border-right: 0.1mm solid #000000;
        }
        table thead td {
            text-align: center;
            border: 0.1mm solid #000000;
        }
        .lower-text{
            text-align: justify;
        }

        .lf{
            text-align: right;
        }

        .rt{
            text-align: left;
        }

        .table{
            width: 100%;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 1px;
        }

        table, th, td {
            border: 1px solid black;
        }

        th{
            text-align: center;
        }

    </style>

</head>
<body>
<p style="font-size: 14px;">
    Lists of passengers that were not accommodated during swapping of vessels.
    </p>
<br/>

<table class="table" >
    <thead>
    <tr>
        <th> NO. </th>
        <th> LAST NAME </th>
        <th> FIRST NAME </th>
        <th> M.I. </th>
        <th> AGE </th>
        <th> SEX </th>
        <th> NATIONALITY </th>
    </tr>
    <thead>
    <tbody>
    <?php
    $x=1;
    if(count($passenger_details)==0)
        echo "<tr><td colspan='7'>No passenger records found.</td>td></tr>";

    foreach ($passenger_details as $pd)
    {
        echo "<tr><td>". $x++ ."</td>";
        echo "<td>".$pd->lastname."</td>";
        echo "<td>".$pd->firstname."</td>";
        echo "<td>".substr($pd->middlename,0,1).". </td>";
        echo "<td>".$pd->age."</td>";
        echo "<td>".$pd->gender."</td>";
        echo "<td>".$pd->nationality."</td>";
        echo "</tr>";
    }
    ?>
    </tbody>
</table>
<br>
<br>
<p> *** NOTHING FOLLOWS *** </p>

</body></html>
