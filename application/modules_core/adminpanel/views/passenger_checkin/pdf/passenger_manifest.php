<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <?php error_reporting(0); ?>
    <title>Passenger Manifest</title>
    <style type="text/css">
        body {
            font-family: "Times New Roman";
            font-size: 8pt;
            text-align: center;
        }
        p {    margin: 0pt;
        }
        td { vertical-align: top; }
        .items td {
            border-left: 0.1mm solid #000000;
            border-right: 0.1mm solid #000000;
        }
        table thead td {
            text-align: center;
            border: 0.1mm solid #000000;
        }
        .lower-text{
            text-align: justify;
        }

        .lf{
            text-align: right;
        }

        .rt{
            text-align: left;
        }

        .table{
            width: 100%;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 1px;
        }

        table, th, td {
            border: 1px solid black;
        }

        th{
            text-align: center;
        }

    </style>

</head>
<body>
<p style="font-size: 14px;">
    REPUBLIC OF THE PHILIPPINES<br>
    DEPARTMENT OF FINANCE
    <br>BUREAU OF CUSTOMS</p>

<h5>Philippine Coastwise Passenger Manifest</h5>

<?php foreach ($voyage_details as $vd): ?>
 <p>A complete list of all passengers on board <?= $vd->voyage ?> sailing from the Port of <?= $vd->port_origin ?>to <?= $vd->port_destination ?> <br/> on <?= date("F j, Y - D", strtotime($vd->departure_date)) ?> at <?= date('g:i a',strtotime($vd->ETD)) ."-". date('g:i a',strtotime($vd->ETA)) ?></p><br/>
<?php endforeach; ?>

<table class="table" >
    <thead>
    <tr>
        <th> NO. </th>
        <th> LAST NAME </th>
        <th> FIRST NAME </th>
        <th> M.I. </th>
        <th> AGE </th>
        <th> SEX </th>
        <th> NATIONALITY </th>
    </tr>
    <thead>
    <tbody>
    <?php
    $x=1;
    if(count($passenger_details)==0)
        echo "<tr><td colspan='7'>No passenger records found.</td>td></tr>";

    foreach ($passenger_details as $pd)
    {
        echo "<tr><td>". $x++ ."</td>";
        echo "<td>".$pd->lastname."</td>";
        echo "<td>".$pd->firstname."</td>";
        echo "<td>".substr($pd->middlename,0,1).". </td>";
        echo "<td>".$pd->age."</td>";
        echo "<td>".$pd->gender."</td>";
        echo "<td>".$pd->nationality."</td>";
        echo "</tr>";
    }
    ?>
    </tbody>
</table>
<br>
<br>
<p> *** NOTHING FOLLOWS *** </p>
<br>
<p class="lower-text">I, __________________ Master of the _____________________ do solemnly swear (or affirm) that the foregoin is a full and complete manifest <br/>
of all passengers taken on board on the said vessel on this present voyage, and that all</br>
statements contained therein are true and correct to the best of my knowledge and belief. </p>

<p class="lf">___________________________</p>
<p class="lf" style="margin-right: 50px;">Master</p>
<br>

<p class="rt">Subscribed and sworn to before me this _________ day of ___________, 20___.</p>

</body></html>
