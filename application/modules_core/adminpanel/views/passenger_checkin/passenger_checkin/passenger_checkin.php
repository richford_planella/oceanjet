<?=$header;?>
<?php extract($_POST); ?>
<form class="form-horizontal oj-form" method="POST" id="check-in-form">
    <?=$this->load->view(admin_dir('notification'));?>
        <!--    Voyage Selection-->
        <div class="form-group">
            <div class="col-xs-2">
                <label class="control-label">Origin</label>
                <select id="origin_id" name="origin_id" class="selectpicker form-control">
                    <option value="0">Please select</option>
                    <?php if($ports):?>
                        <?php foreach($ports as $s):?>
                            <option value="<?=$s->id_port;?>" <?=set_select('origin_id',$s->id_port);?>> <?=$s->port;?> </option>
                    <?php endforeach;?>
                    <?php endif;?>
                </select>
                <span class="input-notes-bottom"><?=form_error('origin_id');?></span>
            </div>
            <div class="col-xs-2">
                <label class="control-label">Destination</label>
                <select id="destination_id" name="destination_id" class="selectpicker form-control">
                    <option value="0">Please select</option>
                    <?php if($ports):?>
                        <?php foreach($ports as $s):?>
                            <option value="<?=$s->id_port;?>" <?=set_select('destination_id',$s->id_port);?>> <?=$s->port;?></option>
                        <?php endforeach;?>
                    <?php endif;?>
                </select>
                <span class="input-notes-bottom"><?=form_error('destination_id');?></span>
            </div>
            <div class="col-xs-6">
                <label class="control-label">Voyage</label>
                <select id="voyage_management_id" name="voyage_management_id" class="selectpicker form-control">
                    <option value="0">Please select</option>
                    <?php if($subvoyage):?>
                        <?php foreach($subvoyage as $value): ?>
                            <option value="<?=$value->id_subvoyage_management;?>" <?=set_select('voyage_management_id', $value->id_subvoyage_management);?>><?=$value->voyage." | ". $value->port_origin."-".$value->port_destination ." | ".  date("F j, Y - D", strtotime($value->departure_date))." | ". date('g:i a',strtotime($value->ETD)) ."-". date('g:i a',strtotime($value->ETA));?></option>
                        <?php endforeach;?>
                    <?php endif;?>
                </select>
                <span class="input-notes-bottom"><?=form_error('voyage_management_id');?></span>
            </div>
            <div class="col-xs-2">

            </div>
        </div>
        <!--    Trip Details-->
        <!--    Trip Details-->
        <div class="form-group form-group__custom">
            <div class="col-xs-12">
                <div class="inner-header">
                    <h3 class="inner-header__title">Trip Details</h3>
                </div>
                <div class="form-group">
                    <div class="col-xs-3">
                        <label class="control-label">Voyage Code</label>
                        <input name="voyage_text" class="form-control" value="<?=set_value('voyage_text',$voyage_text);?>" id="select_voyages" placeholder="Autofill voyage code" readonly>
                        <span class="input-notes-bottom"><?=form_error('voyage_text');?></span>
                    </div>
                    <div class="col-xs-3 col-xs-offset-6">

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-9">
                        <div class="seat-preview__title">
                            <h4>Voyage Details</h4>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-3 has-feedback">
                        <label class="control-label">Departure Date</label>
                        <input class="form-control" id="departure_date" name="departure_date" type="text"
                               value="<?=set_value('departure_date',$departure_date);?>" placeholder="Autofill departure date" readonly/>
                        <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                        <span class="input-notes-bottom"><?=form_error('departure_date');?></span>
                    </div>
                    <div class="col-xs-3">
                        <label class="control-label">Origin</label>
                        <input class="form-control" id="origin" name="origin" type="text"
                               value="<?=set_value('origin',$origin);?>" placeholder="Autofill origin"
                               readonly/>
                        <span class="input-notes-bottom"><?=form_error('origin');?></span>
                    </div>
                    <div class="col-xs-3">
                        <label class="control-label">Destination</label>
                        <input class="form-control" id="destination" name="destination" type="text"
                               value="<?=set_value('destination',$destination);?>" placeholder="Autofill destination"
                               readonly/>
                        <span class="input-notes-bottom"><?=form_error('destination');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-3 has-feedback">
                        <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                        <input class="form-control" id="etd" name="etd" type="text"
                               value="<?=set_value('etd',$etd);?>" placeholder="Autofill ETD"
                               readonly/>
                        <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                        <span class="input-notes-bottom"><?=form_error('etd');?></span>
                    </div>
                    <div class="col-xs-3 has-feedback">
                        <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                        <input class="form-control" id="eta" name="eta" type="text"
                               value="<?=set_value('eta',$eta);?>" placeholder="Autofill ETA"
                               readonly/>
                        <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                        <span class="input-notes-bottom"><?=form_error('eta');?></span>
                    </div>
                    <div class="col-xs-3">
                        <label class="control-label is_swap"><?=($is_swap)==1 ? 'New Vessel':'Vessel';?> </label>
                        <input type="hidden" name="vessel_id" id="vessel_id" value="<?=$vessel_id;?>">
                        <input class="form-control" id="vessel" name="vessel" type="text"
                               value="<?=set_value('vessel',$vessel);?>" placeholder="Autofill vessel"
                               readonly/>
                        <span class="input-notes-bottom"><?=form_error('vessel');?></span>
                    </div>
                </div>
            </div>
        </div>
        <!--    Trip Details-->
       <!--        Accommodation Status-->
        <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Accommodation Details</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-12" id="vessel-details">

                    <table class="table  table-striped table-form accommodation-table">
                        <tbody>
                        <tr>
                            <td colspan="4">Accommodation details table</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
       <!--        Accommodation Status-->
        <!--    Voyage Status-->
        <div class="form-group form-group__custom">
            <div class="col-xs-12">
                <div class="inner-header">
                    <h3 class="inner-header__title">Voyage Status</h3>
                </div>
                <div class="form-group">
                    <div class="col-xs-3">
                        <label class="control-label">Status</label>
                        <input class="form-control" id="subvoyage_status_text" name="subvoyage_status_text" type="text"
                               value="<?=set_value('subvoyage_status_text',$subvoyage_status_text);?>" placeholder="Autofill voyage status"
                               readonly/>
                        <input type="hidden" name="subvoyage_status_id" id="subvoyage_status_id" value="<?=set_value('subvoyage_status_id',$subvoyage_status_id);?>">
                        <span class="input-notes-bottom"><?=form_error('subvoyage_status_text');?></span>
                    </div>
                    <div class="col-xs-9">
                        <label class="control-label">Action Button</label>
                        <div class="btn-oj-group">
                            <button class="btn oj-button update-sub-voyage-status" data-status="9" type="button">Open for Check-In</button>
                            <button class="btn oj-button update-sub-voyage-status" data-status="2" type="button">Depart</button>
                            <button class="btn oj-button update-sub-voyage-status" data-status="4" type="button">Undepart</button>
                            <button class="btn oj-button update-sub-voyage-status" data-status="3" type="button">Delayed</button>
                            <button class="btn oj-button update-sub-voyage-status" data-status="6" type="button">Cancel</button>
                            <button class="btn oj-button update-sub-voyage-status" data-status="10" type="button">Open for Chance Passenger </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--    Voyage Status-->

    <!--        Seats-->
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Seat Plan Selection</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-12 seat-preview">
                    <div class="seat-preview__title">

                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <h4>Check-In Details</h4>
                        </div>
                        <div class="col-xs-2">
                            <label class="control-label">Seat Number</label>
                            <input name="vessel_seats" id="vessel_seats" class="form-control" type="text" placeholder="Autofill seat number" readonly>
                            <input name="accomodation_id" id="accomodation_id" type="hidden">
                            <input name="booking_id"   id="booking_id" type="hidden">
                            <input name="transaction_ref"   id="transaction_ref" type="hidden">
                            <input name="id_number_required"   id="id_number_required" type="hidden">
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Ticket Number</label>
                            <input class="form-control" id="ticket_no" name="ticket_no" type="text" value="" placeholder="Input ticket number" onkeypress="numeric_validation(event)"/>
                            <label id="check-in-messages" class="control-label"></label>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">ID Number</label>
                            <input class="form-control" id="id_number" name="id_number" type="text" value="" placeholder="Input ID number" onkeypress="numeric_validation(event)"/>
                        </div>
                        <div class="col-xs-4">
                            <div class="btn-oj-group oj-button--margin-top-30">
                                <button class="btn oj-button confirm-button" type="button">Confirm</button>
                                <button class="btn oj-button print-boarding-pass-button" type="button">Print Boarding Pass</button>
                            </div>

                        </div>

                    </div>
                    <div class="form-group no-margin-bottom">
                        <div class="col-xs-4">
                            <label class="control-label">Status</label>
                            <ul class="seat-preview__color-choice">
                                <li><span class="sm-box sm-box is-occupied"></span>Occupied</li>
                                <li><span class="sm-box is-vacant"></span>Vacant</li>
                            </ul>
                        </div>
                        <div class="col-xs-8">
                            <label class="control-label">Accommodation Type</label>
                            <ul class="seat-preview__color-choice">
                                <li class="oj-seats__list"><span class="sm-box" style="background-color: #000"></span>N/A</li>
                                <?php foreach($accommodation as $a):?>
                                    <li class="oj-seats__list"><span class="sm-box" style="background-color: <?=$a->palette;?>"></span><?=$a->accommodation_code;?></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <fieldset disabled="">
                                <div class="seat-preview__table">
                                    <table id="arrowkey-table" class="table table-bordered vessel-seats-table">
                                        <tbody>
                                        <tr>
                                            <td>Vessel seats</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="form-group oj-form-footer">
                        <div class="col-xs-12">
                            <div class="btn-oj-group right">
                                <button type="button" class="btn oj-button swap-vessel">Swap Vessels</button>
                                <button class="btn oj-button print-passenger-baggage-manifest" type="button">Print Passenger Manifest</button>
                                <?php if($is_swap==1) ?>
                                 <button class="btn oj-button disembark-button" type="button">Print Disembarked Passengers</button>
                                <button class="btn oj-button passenger-baggage-check-in" type="button">Passenger Baggage Check-In</button>
                            </div>
                        </div>
                    </div>
                    <br/>
                </div>
            </div>

        </div>
    </div>
    <!--        Seats-->
</form>


<!--Some generic modal coming right up start-->
<div id="confirmation-modal" class="modal bootstrap-dialog type-primary fade size-normal in" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h3 class="modal-title">Passenger Check-In</h3>
            </div>
            <div class="modal-body">
                <h4 id="myConfirmationModalLabel"></h4>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Dismiss </button>
            </div>
        </div>
    </div>
</div>
<!--Some generic modal coming right up end-->

<!--Modal Delayed Voyage Start-->
<div id="delayed-voyage" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h4 class="modal-title" id="myModalLabel">Tag voyage status as “delayed”, input no. of minutes delayed.</h4>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label">No. of minutes late.</label>
                            <div class="btn-group bootstrap-select form-control">
                                <input class="form-control" name="delayed_time" id="delayed_time" type="text" placeholder="Input number of minutes delayed" onkeypress="numeric_validation(event)">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Cancel </button>
                <button type="button" class="btn oj-button button-tag-voyage-delayed">Save </button>
            </div>

        </div>
    </div>
</div>
<!--Modal Delayed Voyage End-->

<!--Modal Canceled Voyage Start-->
<div id="canceled-voyage" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h4 class="modal-title" id="myModalLabel">Cancel voyage, input reason for cancelling.</h4>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label">Reason for cancelling.</label>
                            <div class="btn-group bootstrap-select form-control">
                                <input class="form-control" type="text" id="reason-for-cancelling" placeholder="Input input reason for cancelling">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Cancel </button>
                <button type="button" class="btn oj-button button-tag-voyage-canceled">Save </button>
            </div>

        </div>
    </div>
</div>
<!--Modal Canceled Voyage End-->


<!--Modal Get Reason for not having required ID Start-->
<div id="reason-for-not-having-id" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                <h4 class="modal-title" id="myModalLabel">Cannot proceed to check-in, ID was not supplied indicate reason. Thank you!</h4>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label">Reason</label>
                            <div class="btn-group bootstrap-select form-control">
                                <input class="form-control" name="reason_id" id="reason_id" type="text" placeholder="Input reason">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Cancel </button>
                <button type="button" class="btn oj-button button-tag-voyage-delayed">Save </button>
            </div>

        </div>
    </div>
</div>
<!--Modal Get Reason for not having required ID End-->
<script>
    var animation_logo = "<tr> <td colspan='4' style='background-color:#F9F9F9'><img src='<?=img_dir("ring.gif");?>'></td> </tr>";
    var is_swap = <?=($is_swap)==1 ? 1 :0;?>
</script>


<?=$footer;?>