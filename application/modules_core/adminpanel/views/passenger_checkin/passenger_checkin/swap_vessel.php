<?= $header; ?>
<?=$this->load->view(admin_dir('notification'));?>
<a class="btn oj-button" href="<?= admin_url($this->classname, 'add',$sv_details->id_subvoyage_management ); ?>" role="button">Back</a>

<form class="form-horizontal oj-form swap-form" method="POST" action="<?= admin_url($this->classname, 'setNewVessel'); ?>" id="check-in-form">
    <!--    Voyage Selection-->
    <div class="form-group">
        <div class="col-xs-2">
            <label class="control-label">Current Vessel</label>
            <input type="hidden" name="voyage_management_id" id="voyage_management_id"
                   value="<?= $sv_details->id_subvoyage_management; ?>">
            <input type="hidden" name="current_vessel_id" id="current_vessel_id" value="<?= $sv_details->vessel_id; ?>">
            <input name="voyage_text" class="form-control" value="<?= $sv_details->vessel_code; ?>" readonly>
        </div>
        <div class="col-xs-2">
            <label class="control-label">New Vessel</label>
            <select id="new_vessel" name="new_vessel" class="selectpicker form-control">
                <option value="0">Please select</option>
                <?php if ($vessels): ?>
                    <?php foreach ($vessels as $v): ?>
                        <option value="<?= $v->id_vessel; ?>"> <?= $v->vessel_code; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
            <span class="input-notes-bottom"><?= form_error('destination_id'); ?></span>
        </div>
        <div class="col-xs-6">
            <div class="btn-oj-group oj-button--margin-top-30">
                <button class="btn oj-button load-vessel" type="button">Load</button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-2">
            <label class="control-label">Current Vessel</label>
        </div>
        <div class="col-xs-6">
            <select id="select-current-seats" data-placeholder="Curren vessel" multiple="multiple" style="width: 100%;"></select>
        </div>
        <div class="col-xs-3">
            <button class="btn oj-button move-seats" type="button">Move</button>
            <button class="btn oj-button save-changes" type="button">Save</button>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-2">
            <label class="control-label">New Vessel</label>
        </div>
        <div class="col-xs-6">
            <select id="select-new-seats" data-placeholder="New vessel" multiple="multiple" style="width: 100%;"></select>
        </div>
    </div>




    <!--    Seat Plan Selection-->
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Seat Plan Selection</h3>
            </div>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Current Vessel</a></li>
                <li><a data-toggle="tab" href="#menu1">New Vessel</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <!--CURRENT VESSEL-->

                    <!--accommodation-->
                    <div class="form-group no-margin-bottom">
                        <div class="seat-preview__title">
                            <h4>Vessel Name: <?= $sv_details->vessel_code; ?></h4>
                        </div>
                        <div class="col-xs-4">
                            <label class="control-label">Status</label>
                            <ul class="seat-preview__color-choice">
                                <li><span class="sm-box sm-box is-occupied"></span>Occupied</li>
                                <li><span class="sm-box is-vacant"></span>Vacant</li>
                            </ul>
                        </div>
                        <div class="col-xs-8">
                            <label class="control-label">Accommodation Type</label>
                            <ul class="seat-preview__color-choice">
                                <li class="oj-seats__list"><span class="sm-box" style="background-color: #000"></span>N/A
                                </li>
                                <?php foreach ($accommodation as $a): ?>
                                    <li class="oj-seats__list"><span class="sm-box"
                                                                     style="background-color: <?= $a->palette; ?>"></span><?= $a->accommodation_code; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <!--accommodation-->
                    <!-- current vessel table-->
                    <div class="form-group">
                        <div class="col-xs-12">
                            <fieldset disabled="">
                                <div class="seat-preview__table">
                                    <table id="current_vessel_seats" class="table table-bordered vessel-seats-table current-seat-table">
                                        <tbody>
                                        <tr>
                                            <td>Vessel seats</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- current vessel table-->
                    <!--CURRENT VESSEL-->
                </div>
                <div id="menu1" class="tab-pane fade">
                    <!--NEW VESSEL-->
                    <!--accommodation-->
                    <div class="form-group no-margin-bottom">
                        <div class="seat-preview__title">
                            <h4 id="new_vessel_text">Vessel Name: -</h4>
                        </div>
                        <div class="col-xs-4">
                            <label class="control-label">Status</label>
                            <ul class="seat-preview__color-choice">
                                <li><span class="sm-box sm-box is-occupied"></span>Occupied</li>
                                <li><span class="sm-box is-vacant"></span>Vacant</li>
                            </ul>
                        </div>
                        <div class="col-xs-8">
                            <label class="control-label">Accommodation Type</label>
                            <ul class="seat-preview__color-choice">
                                <li class="oj-seats__list"><span class="sm-box" style="background-color: #000"></span>N/A
                                </li>
                                <?php foreach ($accommodation as $a): ?>
                                    <li class="oj-seats__list"><span class="sm-box"
                                                                     style="background-color: <?= $a->palette; ?>"></span><?= $a->accommodation_code; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <!--accommodation-->
                    <!-- new vessel table-->
                    <div class="form-group">
                        <div class="col-xs-12">
                            <fieldset disabled="">
                                <div class="seat-preview__table">
                                    <table id="new_vessel_seats" class="table table-bordered vessel-seats-table2 new-seat-table">
                                        <tbody>
                                        <tr>
                                            <td>Vessel seats</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- new vessel table-->
                    <!--NEW VESSEL-->
                </div>
            </div>


        </div>
    </div>
    <!--    Seat Plan Selection-->
</form>

<!--Some generic modal coming right up start-->
<div id="confirmation-modal" class="modal bootstrap-dialog type-primary fade size-normal in" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">× </span></button>
                <h3 class="modal-title">Swap Vessel</h3>
            </div>
            <div class="modal-body">
                <h4 id="myConfirmationModalLabel"></h4>
            </div>
            <div class="modal-footer btn-oj-group center">
                <button type="button" class="btn oj-button" data-dismiss="modal">Dismiss</button>
            </div>
        </div>
    </div>
</div>
<!--Some generic modal coming right up end-->

<script>
    var animation_logo = "<tr> <td colspan='4' style='background-color:#F9F9F9'><img src='<?=img_dir("ring.gif");?>'></td> </tr>";
</script>


<?= $footer; ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js"></script>
