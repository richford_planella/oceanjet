<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>Group Name</th>
                <th>Contact Person</th>
                <th>Contact No.</th>
                <th>PAX</th>
                <th>Issuing Outlet Code</th>
                <th>Expiry Date</th>
                <th>Booking Reservation Code</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($reservation)):?>
            <?php foreach($$reservation as $r):?>
            <tr>
                <td><?=$r->reservation;?></td>
                <td><?=$r->contact_firstname.' '.$r->contact_lastname;?></td>
                <td><?=$r->contact_number;?></td>
                <td><?=$r->outlet;?></td>
                <td><?=$r->expiration_date;?></td>
                <td><?=$r->reservation_code;?></td>
                <td>
                    <a href="<?=admin_url($this->classname, 'view', $r->id_reservation);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $r->id_reservation);?>">Edit</a>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
<?=$footer;?>