<?php if($voyage_count > 0):?>
<?php if($reservation_type == 'departure'):?>
<div class="col-xs-12">
    <table class="table  table-striped table-form">
        <thead>
            <tr>
                <th style="width: 20%;">DEPARTURE</th>
                <?php foreach($displayHeader as $key => $header):?>
                <th><?=$header->origin_port_code.'-'.$header->destination_port_code;?></th>
                <?php endforeach;?>
            </tr>            
        </thead>
        <tbody>
            <?php foreach($displayStats as $label => $value):?>
            <tr>
                <th scope="row"><?=$label;?></th>
                <?php foreach($displayHeader as $key => $header):?>
                <?php foreach($value as $vKey => $v):?>
                <?php if($key == $vKey):?>
                <td><?=$v;?></td>
                <?php endif;?>
                <?php endforeach;?>
                <?php endforeach;?>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<?php elseif($reservation_type == 'return'):?>
<div class="col-xs-12">
    <table class="table  table-striped table-form">
        <thead>
            <tr>
                <th style="width: 20%;">RETURN</th>
                <?php foreach($displayHeader as $key => $header):?>
                <th><?=$header->origin_port_code.'-'.$header->destination_port_code;?></th>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
            <?php foreach($displayStats as $label => $value):?>
            <tr>
                <th scope="row"><?=$label;?></th>
                <?php foreach($displayHeader as $key => $header):?>
                <?php foreach($value as $vKey => $v):?>
                <?php if($key == $vKey):?>
                <td><?=$v;?></td>
                <?php endif;?>
                <?php endforeach;?>
                <?php endforeach;?>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<?php endif; ?>
<?php else:?>
<div class="col-xs-12">
    <div class="form-box form-box--blank no-content">
        <span class="no-selected">No Input Data</span>
    </div>
</div>
<?php endif;?>


