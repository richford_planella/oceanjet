<div class="col-xs-3">
    <label class="control-label">Fare Details</label>
    <select class="selectpicker form-control" id="fare_select" name="fare_select">
        <option value="">Please select</option>
        <?php foreach($passenger_fare as $p):?>
        <option value="<?=$p->id_passenger_fare;?>" <?=set_select('fare_select',$p->id_passenger_fare);?>><?=$p->passenger_fare;?></option>
        <?php endforeach;?>
    </select>
    <span class="input-notes-bottom"><?php echo form_error('fare_select'); ?></span>
</div>