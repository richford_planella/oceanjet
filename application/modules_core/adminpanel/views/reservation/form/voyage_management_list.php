                                        <label class="control-label">Departure Date</label>
                                        <select class="selectpicker form-control" id="departure_date" name="departure_date">
                                            <option value="0" origin_subvoyage_id="0" destination_subvoyage_id="0" terminal_fee="0.00" port_charge="0.00">Please select</option>
                                            <?php foreach($voyage_management as $vm):?>
                                            <option value="<?=$vm->voyage_management_id;?>" origin_subvoyage_id="<?=$vm->origin_subvoyage_id;?>" destination_subvoyage_id="<?=$vm->destination_subvoyage_id;?>" terminal_fee="<?= $vm->terminal_fee; ?>" port_charge="<?= $vm->port_charge; ?>" <?=set_select('departure_date', $vm->voyage_management_id);?>>
                                                <?=$vm->voyage_code.' | '.$vm->port_origin.' - '.$vm->port_destination.' | '.date("F j, Y - D",strtotime($vm->dept_date)).' | '.date('g:i a',strtotime($vm->ETD)).'-'.date('g:i a',strtotime($vm->ETA));?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                        <input type="hidden" name="hidden_origin_subvoyage_id" id="hidden_origin_subvoyage_id" value="<?=set_value('hidden_origin_subvoyage_id');?>">
                                        <input type="hidden" name="hidden_destination_subvoyage_id" id="hidden_destination_subvoyage_id" value="<?=set_value('hidden_destination_subvoyage_id');?>">
                                        <input type="hidden" name="hidden_departure_date" id="hidden_departure_date" value="<?=set_value('hidden_departure_date');?>">
                                        <span class="input-notes-bottom"><?php echo form_error('departure_date'); ?></span>