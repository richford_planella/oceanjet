                    <div class="col-xs-12">
                        <div class="add-leg">
                            <div class="add-leg__header">
                                Return
                            </div>
                            <div class="add-leg__body">
                                <div class="form-group">
                                    <div class="col-xs-2">
                                        <label class="control-label">Origin</label>
                                        <select class="selectpicker form-control" id="ret_origin_id" name="ret_origin_id">
                                            <option value="">Please select origin</option>
                                            <?php foreach($port as $a):?>
                                                <option value="<?=$a->id_port;?>" <?=set_select('ret_origin_id', $a->id_port,($destination_id == $a->id_port) ? TRUE : '');?>><?=$a->port;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <input type="hidden" name="hidden_ret_origin_id" id="hidden_ret_origin_id" value="<?=$destination_id;?>">
                                        <span class="input-notes-bottom"><?php echo form_error('ret_origin_id'); ?></span>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="control-label">Destination</label>
                                        <select class="selectpicker form-control" id="ret_destination_id" name="ret_destination_id">
                                            <option value="">Please select destination</option>
                                            <?php foreach($port as $a):?>
                                                <option value="<?=$a->id_port;?>" <?=set_select('ret_destination_id', $a->id_port,($origin_id == $a->id_port) ? TRUE : '');?>><?=$a->port;?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <input type="hidden" name="hidden_ret_destination_id" id="hidden_ret_destination_id" value="<?=$origin_id;?>">
                                        <span class="input-notes-bottom"><?php echo form_error('ret_destination_id'); ?></span>
                                    </div>
                                    <div class="col-xs-6 ret_dept_date">
                                        <label class="control-label">Departure Date</label>
                                        <select class="selectpicker form-control" id="ret_departure_date" name="ret_departure_date">
                                            <option value="0" origin_subvoyage_id="0" destination_subvoyage_id="0">Please select departure date</option>
                                            <?php foreach($voyage_management as $vm):?>
                                            <option value="<?=$vm->voyage_management_id;?>" origin_subvoyage_id="<?=$vm->origin_subvoyage_id;?>" destination_subvoyage_id="<?=$vm->destination_subvoyage_id;?>" <?=set_select('departure_date', $vm->voyage_management_id);?>>
                                                <?=$vm->voyage_code.' | '.$vm->port_origin.' - '.$vm->port_destination.' | '.date("F j, Y - D",strtotime($vm->dept_date)).' | '.date('g:i a',strtotime($vm->ETD)).'-'.date('g:i a',strtotime($vm->ETA));?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                        <input type="hidden" name="hidden_ret_origin_subvoyage_id" id="hidden_ret_origin_subvoyage_id" value="<?=set_value('hidden_ret_origin_subvoyage_id');?>">
                                        <input type="hidden" name="hidden_ret_destination_subvoyage_id" id="hidden_ret_destination_subvoyage_id" value="<?=set_value('hidden_ret_destination_subvoyage_id');?>">
                                        <input type="hidden" name="hidden_ret_departure_date" id="hidden_ret_departure_date" value="<?=set_value('hidden_ret_departure_date');?>">
                                        <span class="input-notes-bottom"><?php echo form_error('ret_departure_date'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group form-group__footer ret-trip-details-div">
                                    <div class="col-xs-12">
                                        <div class="form-box form-box--blank no-content">
                                            <span class="no-selected">No Input Data</span>
                                            <input type="hidden" name="ret_voyage_count" id="ret_voyage_count" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>