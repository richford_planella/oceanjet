<?php if(!empty($voyage->id_voyage)):?>
<div class="col-xs-12">
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Voyage Code</label>
            <input class="form-control" type="text" value="<?=$voyage->voyage;?>" disabled />
            <input type="hidden" name="voyage_id" id="voyage_id" value="<?=$voyage->id_voyage;?>">
        </div>
    </div>
    <?php $i = 1;?>
    <?php foreach($subvoyage as $sv):?>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="seat-preview__title">
                <h4>Leg <?=($i > 9) ? $i : '0'.$i;?></h4>
                <input type="hidden" name="subvoyage_management_id[]" id="subvoyage_management_id_<?=$i;?>" value="<?=$sv->id_subvoyage_management;?>">
                <input type="hidden" name="subvoyage_id[]" id="subvoyage_id_<?=$i;?>" value="<?=$sv->id_subvoyage;?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3 has-feedback">
            <label class="control-label">Depature Date</label>
            <input class="form-control oj-date-value" type="text" value="<?=$sv->departure_date;?>" disabled/>
            <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Origin</label>
            <input class="form-control" type="text" value="<?=$sv->origin_port;?>" disabled/>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Destination</label>
            <input class="form-control" type="text" value="<?=$sv->destination_port;?>" disabled/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3 has-feedback">
            <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
            <input class="form-control oj-time-picker" type="text" value="<?=date('g:i a',strtotime($sv->ETD));?>" disabled/>
            <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="col-xs-3 has-feedback">
            <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
            <input class="form-control oj-time-picker" type="text" value="<?=date('g:i a',strtotime($sv->ETA));?>" disabled/>
            <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Vessel</label>
            <input class="form-control" type="text" value="<?=$sv->vessel_code.'-'.$sv->vessel;?>" disabled/>
        </div>
    </div>
    <?php $i++;?>
    <?php endforeach;?>
    <input type="hidden" name="voyage_count" id="voyage_count" value="<?=$voyage_count;?>">
</div>
<?php else:?>
<div class="col-xs-12">
    <div class="form-box form-box--blank no-content">
        <span class="no-selected">No Input Data</span>
        <input type="hidden" name="voyage_id" id="voyage_id" value="">
        <input type="hidden" name="voyage_count" id="voyage_count" value="<?=$voyage_count;?>">
    </div>
</div>
<?php endif;?>
