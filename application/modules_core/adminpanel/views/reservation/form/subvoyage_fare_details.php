<?php $i=1;?>
<?php foreach ($subvoyage_fare as $sf):?>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Origin</label>
            <input class="form-control" type="text" value="<?=$sf->origin_port;?>" disabled/>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Destination</label>
            <input class="form-control" type="text" value="<?=$sf->destination_port;?>" disabled/>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Fare</label>
            <input class="form-control " type="text"  value="<?=$sf->amount;?>" disabled/>
            <input class="control_sum" type="hidden" name="fare_amount[]" value="<?=$sf->amount;?>">
        </div>
    </div>
    <?php $i++; ?>
<?php endforeach;?>