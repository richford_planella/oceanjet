<?= $header; ?>
<form class="form-horizontal oj-form" id="myForm" action="<?= current_url(); ?>" method="POST" enctype="multipart/form-data">
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Reservation Details</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Group Name</label>
                    <input class="form-control" id="reservation" name="reservation" type="text" value="<?= set_value('reservation'); ?>" placeholder="Input group name" />
                    <span class="input-notes-bottom"><?php echo form_error('reservation'); ?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">PAX</label>
                    <input class="form-control" id="pax" name="pax" type="number" value="<?= set_value('pax'); ?>" placeholder="Input pax" />
                    <span class="input-notes-bottom"><?php echo form_error('pax'); ?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Sales Outlet Code</label>
                    <select class="selectpicker form-control" id="outlet_id" name="outlet_id">
                        <option value="">Please select</option>
                        <?php foreach ($outlet as $o): ?>
                            <option value="<?= $o->id_outlet; ?>" <?= set_select('outlet_id', $o->id_outlet); ?>><?= $o->outlet; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="input-notes-bottom"><?php echo form_error('outlet_id'); ?></span>
                </div>
                <div class="col-xs-3 has-feedback">
                    <label class="control-label">Expiry Date</label>
                    <input class="form-control" id="expiration_date" name="expiration_date" type="text" placeholder="YYYY-MM-DD" value="<?= set_value('expiration_date'); ?>" />
                    <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                    <span class="input-notes-bottom"><?php echo form_error('expiration_date'); ?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Firstname</label>
                    <input class="form-control" id="contact_firstname" name="contact_firstname" type="text" value="<?= set_value('contact_firstname'); ?>" placeholder="Input contact firstname" />
                    <span class="input-notes-bottom"><?php echo form_error('contact_firstname'); ?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Lastname</label>
                    <input class="form-control" id="contact_lastname" name="contact_lastname" type="text" value="<?= set_value('contact_lastname'); ?>" placeholder="Input contact lastname" />
                    <span class="input-notes-bottom"><?php echo form_error('contact_lastname'); ?></span>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Contact Number</label>
                    <input class="form-control" id="contact_number" name="contact_number" type="text" value="<?= set_value('contact_number'); ?>" placeholder="Input contact number" />
                    <span class="input-notes-bottom"><?php echo form_error('contact_number'); ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group  form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Trip Details</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="add-leg">
                        <div class="add-leg__header">
                            Departure
                        </div>
                        <div class="add-leg__body">
                            <div class="form-group">
                                <div class="col-xs-2">
                                    <label class="control-label">Origin</label>
                                    <select class="selectpicker form-control" id="origin_id" name="origin_id" <?= ($is_return == 1) ? "disabled='disabled'" : "" ?>>
                                        <option value="">Please select</option>
                                        <?php foreach ($port as $a): ?>
                                            <option value="<?= $a->id_port; ?>" <?= set_select('origin_id', $a->id_port); ?>><?= $a->port; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" name="hidden_origin_id" id="hidden_origin_id" value="<?= set_value('hidden_origin_id'); ?>">
                                    <span class="input-notes-bottom"><?php echo form_error('origin_id'); ?></span>
                                </div>
                                <div class="col-xs-2">
                                    <label class="control-label">Destination</label>
                                    <select class="selectpicker form-control" id="destination_id" name="destination_id" <?= ($is_return == 1) ? "disabled='disabled'" : "" ?>>
                                        <option value="">Please select</option>
                                        <?php foreach ($port as $a): ?>
                                            <option value="<?= $a->id_port; ?>" <?= set_select('destination_id', $a->id_port); ?>><?= $a->port; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" name="hidden_destination_id" id="hidden_destination_id" value="<?= set_value('hidden_destination_id'); ?>">
                                    <span class="input-notes-bottom"><?php echo form_error('destination_id'); ?></span>
                                </div>
                                <div class="col-xs-2 has-feedback">
                                    <label class="control-label">Departure Date</label>
                                    <input class="form-control" id="dep_date" name="dep_date" type="text" placeholder="YYYY-MM-DD" value="<?= set_value('dep_date'); ?>" />
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                                    <span class="input-notes-bottom"><?php echo form_error('dep_date'); ?></span>
                                </div>
                                <div class="col-xs-4 dept_date">
                                    <label class="control-label">Voyage</label>
                                    <select class="selectpicker form-control" id="departure_date" name="departure_date" <?= ($is_return == 1) ? "disabled='disabled'" : "" ?>>
                                        <option value="0" origin_subvoyage_id="0" destination_subvoyage_id="0" terminal_fee="0.00" port_charge="0.00">Please select</option>
                                        <?php foreach ($departure_date as $vm): ?>
                                            <option value="<?= $vm->voyage_management_id; ?>" origin_subvoyage_id="<?= $vm->origin_subvoyage_id; ?>" destination_subvoyage_id="<?= $vm->destination_subvoyage_id; ?>" terminal_fee="<?= $vm->terminal_fee; ?>" port_charge="<?= $vm->port_charge; ?>" <?= set_select('departure_date', $vm->voyage_management_id); ?>>
                                                <?= $vm->voyage_code . ' | ' . $vm->port_origin . ' - ' . $vm->port_destination . ' | ' . date("F j, Y - D", strtotime($vm->dept_date)) . ' | ' . date('g:i a', strtotime($vm->ETD)) . '-' . date('g:i a', strtotime($vm->ETA)); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" name="hidden_origin_subvoyage_id" id="hidden_origin_subvoyage_id" value="<?= set_value('hidden_origin_subvoyage_id'); ?>">
                                    <input type="hidden" name="hidden_destination_subvoyage_id" id="hidden_destination_subvoyage_id" value="<?= set_value('hidden_destination_subvoyage_id'); ?>">
                                    <input type="hidden" name="hidden_departure_date" id="hidden_departure_date" value="<?= set_value('hidden_departure_date'); ?>">
                                    <span class="input-notes-bottom"><?php echo form_error('departure_date'); ?></span>
                                </div>
                                <div class="col-xs-2 round-trip-div">
                                    <label class="control-label">Round Trip</label>
                                    <div class="checkbox ">
                                        <label class="checkbox-inline checkbox__label">
                                            <input class="checkbox__btn" type="checkbox" name="is_return" id="is_return" <?= set_checkbox("is_return", 1); ?> value="1"> Yes
                                        </label>
                                    </div>
                                    <span class="input-notes-bottom"><?php echo form_error('is_return'); ?></span>
                                </div>
                            </div>
                            <div class="form-group form-group__footer trip-details-div">
                                <?php if (!empty($voyage->id_voyage)): ?>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <div class="col-xs-3">
                                                <label class="control-label">Voyage Code</label>
                                                <input class="form-control" type="text" value="<?= $voyage->voyage; ?>" disabled />
                                                <input type="hidden" name="voyage_id" id="voyage_id" value="<?= $voyage->id_voyage; ?>">
                                            </div>
                                        </div>
                                        <?php $i = 1; ?>
                                        <?php foreach ($subvoyage as $sv): ?>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class="seat-preview__title">
                                                        <h4>Leg <?= ($i > 9) ? $i : '0' . $i; ?></h4>
                                                        <input type="hidden" name="subvoyage_management_id[]" id="subvoyage_management_id_<?= $i; ?>" value="<?= $sv->id_subvoyage_management; ?>">
                                                        <input type="hidden" name="subvoyage_id[]" id="subvoyage_id_<?= $i; ?>" value="<?= $sv->id_subvoyage; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-3 has-feedback">
                                                    <label class="control-label">Depature Date</label>
                                                    <input class="form-control oj-date-value" type="text" value="<?= $sv->departure_date; ?>" disabled/>
                                                    <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="control-label">Origin</label>
                                                    <input class="form-control" type="text" value="<?= $sv->origin_port; ?>" disabled/>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="control-label">Destination</label>
                                                    <input class="form-control" type="text" value="<?= $sv->destination_port; ?>" disabled/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-3 has-feedback">
                                                    <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                                                    <input class="form-control oj-time-picker" type="text" value="<?= date('g:i a', strtotime($sv->ETD)); ?>" disabled/>
                                                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-xs-3 has-feedback">
                                                    <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                                                    <input class="form-control oj-time-picker" type="text" value="<?= date('g:i a', strtotime($sv->ETA)); ?>" disabled/>
                                                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="control-label">Vessel</label>
                                                    <input class="form-control" type="text" value="<?= $sv->vessel_code . '-' . $sv->vessel; ?>" disabled/>
                                                </div>
                                            </div>
                                            <?php $i++; ?>
                                        <?php endforeach; ?>
                                        <input type="hidden" name="voyage_count" id="voyage_count" value="<?= $voyage_count; ?>">
                                    </div>
                                <?php else: ?>
                                    <div class="col-xs-12">
                                        <div class="form-box form-box--blank no-content">
                                            <span class="no-selected">No Input Data</span>
                                            <input type="hidden" name="voyage_id" id="voyage_id" value="">
                                            <input type="hidden" name="voyage_count" id="voyage_count" value="<?= $voyage_count; ?>">
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group return-container">
                <!-- RETURN START -->
                <div class="col-xs-12">
                    <div class="add-leg">
                        <div class="add-leg__header">
                            Return
                        </div>
                        <div class="add-leg__body">
                            <div class="form-group">
                                <div class="col-xs-2">
                                    <label class="control-label">Origin</label>
                                    <select class="selectpicker form-control" id="ret_origin_id" name="ret_origin_id" <?= ($is_return == 1) ? "disabled='disabled'" : "" ?> >
                                        <option value="">Please select</option>
                                        <?php foreach ($port as $a): ?>
                                            <option value="<?= $a->id_port; ?>" <?= set_select('ret_origin_id', $a->id_port); ?>><?= $a->port; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" name="hidden_ret_origin_id" id="hidden_ret_origin_id" value="<?= set_value('hidden_ret_origin_id'); ?>">
                                    <span class="input-notes-bottom"><?php echo form_error('ret_origin_id'); ?></span>
                                </div>
                                <div class="col-xs-2">
                                    <label class="control-label">Destination</label>
                                    <select class="selectpicker form-control" id="ret_destination_id" name="ret_destination_id" <?= ($is_return == 1) ? "disabled='disabled'" : "" ?>>
                                        <option value="">Please select</option>
                                        <?php foreach ($port as $a): ?>
                                            <option value="<?= $a->id_port; ?>" <?= set_select('ret_destination_id', $a->id_port); ?>><?= $a->port; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" name="hidden_ret_destination_id" id="hidden_ret_destination_id" value="<?= set_value('hidden_ret_destination_id'); ?>">
                                    <span class="input-notes-bottom"><?php echo form_error('ret_destination_id'); ?></span>
                                </div>
                                <div class="col-xs-6 ret_dept_date">
                                    <label class="control-label">Departure Date</label>
                                    <select class="selectpicker form-control" id="ret_departure_date" name="ret_departure_date" >
                                        <option value="0" origin_subvoyage_id="0" destination_subvoyage_id="0">Please select</option>
                                        <?php foreach ($ret_departure_date as $vm): ?>
                                            <option value="<?= $vm->voyage_management_id; ?>" origin_subvoyage_id="<?= $vm->origin_subvoyage_id; ?>" destination_subvoyage_id="<?= $vm->destination_subvoyage_id; ?>" <?= set_select('ret_departure_date', $vm->voyage_management_id); ?>>
                                                <?= $vm->voyage_code . ' | ' . $vm->port_origin . ' - ' . $vm->port_destination . ' | ' . date("F j, Y - D", strtotime($vm->dept_date)) . ' | ' . date('g:i a', strtotime($vm->ETD)) . '-' . date('g:i a', strtotime($vm->ETA)); ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" name="hidden_ret_origin_subvoyage_id" id="hidden_ret_origin_subvoyage_id" value="<?= set_value('hidden_ret_origin_subvoyage_id'); ?>">
                                    <input type="hidden" name="hidden_ret_destination_subvoyage_id" id="hidden_ret_destination_subvoyage_id" value="<?= set_value('hidden_ret_destination_subvoyage_id'); ?>">
                                    <input type="hidden" name="hidden_ret_departure_date" id="hidden_ret_departure_date" value="<?= set_value('hidden_ret_departure_date'); ?>">
                                    <span class="input-notes-bottom"><?php echo form_error('ret_departure_date'); ?></span>
                                </div>
                            </div>
                            <div class="form-group form-group__footer ret-trip-details-div">
                                <!-- RETURN VOYAGE DETAILS -->
                                <?php if (!empty($ret_voyage->id_voyage)): ?>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <div class="col-xs-3">
                                                <label class="control-label">Voyage Code</label>
                                                <input class="form-control" type="text" value="<?= $ret_voyage->voyage; ?>" disabled />
                                                <input type="hidden" name="return_voyage_id" id="return_voyage_id" value="<?= $ret_voyage->id_voyage; ?>">
                                            </div>
                                        </div>
                                        <?php $i = 1; ?>
                                        <?php foreach ($ret_subvoyage as $sv): ?>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class="seat-preview__title">
                                                        <h4>Leg <?= ($i > 9) ? $i : '0' . $i; ?></h4>
                                                        <input type="hidden" name="return_subvoyage_management_id[]" id="return_subvoyage_management_id_<?= $i; ?>" value="<?= $sv->id_subvoyage_management; ?>">
                                                        <input type="hidden" name="return_subvoyage_id[]" id="return_subvoyage_id_<?= $i; ?>" value="<?= $sv->id_subvoyage; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-3 has-feedback">
                                                    <label class="control-label">Depature Date</label>
                                                    <input class="form-control oj-date-value" type="text" value="<?= $sv->departure_date; ?>" disabled/>
                                                    <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="control-label">Origin</label>
                                                    <input class="form-control" type="text" value="<?= $sv->origin_port; ?>" disabled/>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="control-label">Destination</label>
                                                    <input class="form-control" type="text" value="<?= $sv->destination_port; ?>" disabled/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-3 has-feedback">
                                                    <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
                                                    <input class="form-control oj-time-picker" type="text" value="<?= date('g:i a', strtotime($sv->ETD)); ?>" disabled/>
                                                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-xs-3 has-feedback">
                                                    <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
                                                    <input class="form-control oj-time-picker" type="text" value="<?= date('g:i a', strtotime($sv->ETA)); ?>" disabled/>
                                                    <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="control-label">Vessel</label>
                                                    <input class="form-control" type="text" value="<?= $sv->vessel_code . '-' . $sv->vessel; ?>" disabled/>
                                                </div>
                                            </div>
                                            <?php $i++; ?>
                                        <?php endforeach; ?>
                                        <input type="hidden" name="ret_voyage_count" id="ret_voyage_count" value="<?= $ret_voyage_count; ?>">
                                    </div>
                                <?php else: ?>
                                    <div class="col-xs-12">
                                        <div class="form-box form-box--blank no-content">
                                            <span class="no-selected">No Input Data</span>
                                            <input type="hidden" name="return_voyage_id" id="return_voyage_id" value="">
                                            <input type="hidden" name="ret_voyage_count" id="ret_voyage_count" value="<?= $ret_voyage_count; ?>">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <!-- END RETURN VOYAGE DETAILS-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- RETURN END -->
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Accomodation Type</label>
                    <select class="selectpicker form-control" id="accommodation_id" name="accommodation_id">
                        <option value="">Please select</option>
                        <?php foreach ($accommodation as $a): ?>
                            <option value="<?= $a->id_accommodation; ?>" <?= set_select('accommodation_id', $a->id_accommodation); ?>><?= $a->accommodation; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="input-notes-bottom"><?php echo form_error('accommodation_id'); ?></span>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Trip Statistics Table</h3>
            </div>
            <div class="form-group departure-details">
                <?php if ($voyage_count > 0): ?>
                    <div class="col-xs-12">
                        <table class="table  table-striped table-form">
                            <thead>
                                <tr>
                                    <th style="width: 20%;">DEPARTURE</th>
                                    <?php foreach ($displayHeader as $key => $header): ?>
                                        <th><?= $header->origin_port_code . '-' . $header->destination_port_code; ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($displayStats as $label => $value): ?>
                                    <tr>
                                        <th scope="row"><?= $label; ?></th>
                                        <?php foreach ($displayHeader as $key => $header): ?>
                                            <?php foreach ($value as $vKey => $v): ?>
                                                <?php if ($key == $vKey): ?>
                                                    <td><?= $v; ?></td>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <div class="col-xs-12">
                        <div class="form-box form-box--blank no-content">
                            <span class="no-selected">No Input Data</span>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="form-group form-group__footer"></div>
            <div class="form-group return-details">
                <?php if ($ret_voyage_count > 0): ?>
                    <div class="col-xs-12">
                        <table class="table  table-striped table-form">
                            <thead>
                                <tr>
                                    <th style="width: 20%;">RETURN</th>
                                    <?php foreach ($displayRetHeader as $key => $header): ?>
                                        <th><?= $header->origin_port_code . '-' . $header->destination_port_code; ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($displayRetStats as $label => $value): ?>
                                    <tr>
                                        <th scope="row"><?= $label; ?></th>
                                        <?php foreach ($displayRetHeader as $key => $header): ?>
                                            <?php foreach ($value as $vKey => $v): ?>
                                                <?php if ($key == $vKey): ?>
                                                    <td><?= $v; ?></td>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <?php if ($is_return == 1): ?>
                        <div class="col-xs-12">
                            <div class="form-box form-box--blank no-content">
                                <span class="no-selected">No Input Data</span>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>            
        </div>
    </div>

    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Fare Details</h3>
            </div>
            <div class="form-group fare-select-div">
                <div class="col-xs-3">
                    <label class="control-label">Fare Details</label>
                    <select class="selectpicker form-control" id="fare_select" name="fare_select">
                        <option value="">Please select</option>
                        <?php foreach($departure_fare as $p):?>
                        <option value="<?=$p->id_passenger_fare;?>" <?=set_select('fare_select',$p->id_passenger_fare);?>><?=$p->passenger_fare;?></option>
                        <?php endforeach;?>
                    </select>
                    <span class="input-notes-bottom"><?php echo form_error('fare_select'); ?></span>
                </div>
            </div>
            <div class="form-group fare-details-div">
                <?php $i=1;?>
                <?php foreach ($departure_subvoyage_fare as $sf):?>
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Origin</label>
                            <input class="form-control" type="text" value="<?=$sf->origin_port;?>" disabled/>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Destination</label>
                            <input class="form-control" type="text" value="<?=$sf->destination_port;?>" disabled/>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Fare</label>
                            <input class="form-control" type="text"  value="<?=$sf->amount;?>" disabled/>
                            <input class="control_sum" type="hidden" name="fare_amount[]" value="<?=$sf->amount;?>">
                        </div>
                    </div>
                    <?php $i++; ?>
                <?php endforeach;?>
            </div>            
            <div class="form-group form-group__footer"></div><!-- separator -->
            <div class="form-group return-fare-select-div">
                <?php if ($is_return == 1): ?>
                    <div class="col-xs-3">
                        <label class="control-label">Fare Details</label>
                        <select class="selectpicker form-control" id="return_fare_select" name="return_fare_select">
                            <option value="">Please select</option>
                            <?php foreach($return_fare as $p):?>
                            <option value="<?=$p->id_passenger_fare;?>" <?=set_select('return_fare_select',$p->id_passenger_fare);?>><?=$p->passenger_fare;?></option>
                            <?php endforeach;?>
                        </select>
                        <span class="input-notes-bottom"><?php echo form_error('return_fare_select'); ?></span>
                    </div>
                <?php endif; ?>
            </div>
            <div class="form-group return-fare-details-div">
                <?php if ($is_return == 1): ?>
                <?php $i=1;?>
                <?php foreach ($return_subvoyage_fare as $sf):?>
                    <div class="form-group">
                        <div class="col-xs-3">
                            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Origin</label>
                            <input class="form-control" type="text" value="<?=$sf->origin_port;?>" disabled/>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Destination</label>
                            <input class="form-control" type="text" value="<?=$sf->destination_port;?>" disabled/>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Leg <?= ($i > 9) ? $i : '0' . $i; ?> Fare</label>
                            <input class="form-control" type="text"  value="<?=$sf->amount;?>" disabled/>
                            <input class="control_sum" type="hidden" name="fare_amount[]" value="<?=$sf->amount;?>">
                        </div>
                    </div>
                    <?php $i++; ?>
                <?php endforeach;?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Total</h3>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Fare Amount</label>
                    <input class="form-control price" type="text" id="total_fare" name="total_fare" value="<?=set_value('total_fare','0.00');?>" disabled/>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Terminal Fee</label>
                    <input class="form-control" type="text"  id="terminal_fee" name="terminal_fee" value="<?=set_value('terminal_fee','0.00');?>" disabled/>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Port Charge</label>
                    <input class="form-control" type="text" id="port_charge" name="port_charge" value="<?=set_value('port_charge','0.00');?>" disabled/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label class="control-label">Group's Total Amount</label>
                    <input class="form-control" type="text" id="group_total" name="group_total" value="<?=set_value('group_total','0.00');?>" disabled/>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Total Discount</label>
                    <input class="form-control" type="text" id="total_discount" name="total_discount" value="<?=set_value('total_discount','0.00');?>" disabled/>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Total Due</label>
                    <input class="form-control" type="text" id="grand_total" name="grand_total" value="<?=set_value('grand_total','0.00');?>" disabled/>
                </div>
                <div class="col-xs-3">
                    <label class="control-label">Balance</label>
                    <input class="form-control" type="text" id="balance" name="balance" value="<?=set_value('balance','0.00');?>" disabled/>
                </div>
            </div>
        </div>
    </div>
    
    <div class="form-group form-group__custom">
        <div class="col-xs-12">
            <div class="inner-header">
                <h3 class="inner-header__title">Passenger Information</h3>
            </div>
            <!-- /Table Header -->
            <div class="row table-header-custom nopadding">
                <div class="col-xs-12">
                    <button type="button" class="btn oj-button" data-toggle="modal" data-target="#modal-add-user">ADD PASSENGER</button>
                </div>
            </div>
            <!-- /Table -->
            <div class="form-group departure-details">
                <?php if ($voyage_count > 0): ?>
                    <div class="col-xs-12">
                        <table class="table  table-striped table-form">
                            <thead>
                                <tr>
                                    <th style="width: 20%;">DEPARTURE</th>
                                    <?php foreach ($displayHeader as $key => $header): ?>
                                        <th><?= $header->origin_port_code . '-' . $header->destination_port_code; ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($displayStats as $label => $value): ?>
                                    <tr>
                                        <th scope="row"><?= $label; ?></th>
                                        <?php foreach ($displayHeader as $key => $header): ?>
                                            <?php foreach ($value as $vKey => $v): ?>
                                                <?php if ($key == $vKey): ?>
                                                    <td><?= $v; ?></td>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <div class="col-xs-12">
                        <div class="form-box form-box--blank no-content">
                            <span class="no-selected">No Input Data</span>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-xs-12">
            <label class="control-label control-label--bold">Passenger Information</label>
            <div class="form-box">
                <!-- /Table Header -->
                <div class="row table-header-custom nopadding">
                    <div class="col-xs-12">
                        <button type="button" class="btn oj-button" data-toggle="modal" data-target="#modal-add-user">ADD PASSENGER</button>
                    </div>
                </div>
                <!-- /Table -->
                <table id="table-passenger-info" class="example-table table global-table nopadding">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Middle Initial</th>
                            <th>Contact No.</th>
                            <th>Total Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>John one</td>
                            <td>Doe</td>
                            <td>A</td>
                            <td>09991111222</td>
                            <td>P500.00</td>
                            <td><a href="voyage-details.html">View</a></td>
                        </tr>
                        <tr>
                            <td>John two</td>
                            <td>Doe</td>
                            <td>B</td>
                            <td>09991111222</td>
                            <td>P500.00</td>
                            <td><a href="voyage-details.html">View</a></td>
                        </tr>
                        <tr>
                            <td>John three</td>
                            <td>Doe</td>
                            <td>C</td>
                            <td>09991111222</td>
                            <td>P500.00</td>
                            <td><a href="voyage-details.html">View</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?= admin_url($this->classname); ?>" class="btn oj-button gray-button">Cancel</a>
                <button type="submit" class="btn oj-button">Reserve</button>
            </div>
        </div>
    </div>

    <!-- Modal Add Vessel-->
    <div id="modal-table-v1" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-table-v1">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title </h4>
                </div>
                <div class="modal-body">
                    <!-- /Table -->
                    <table id="add-voyage-number" class="table global-table nopadding">
                        <thead>
                            <tr>
                                <th>Voyage No. </th>
                                <th>Origin </th>
                                <th>Destination </th>
                                <th>ETD </th>
                                <th>ETA </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>0123 </td>
                                <td>MNL </td>
                                <td>CBU </td>
                                <td>09:30 AM </td>
                                <td>04:45 PM </td>
                                <td>
                                    <input class="btn custom-btn-test " data-dismiss="modal" type="button" value="Add">
                                </td>
                            </tr>
                            <tr>
                                <td>0123 </td>
                                <td>MNL </td>
                                <td>CBU </td>
                                <td>09:30 AM </td>
                                <td>04:45 PM </td>
                                <td>
                                    <input class="btn custom-btn-test " data-dismiss="modal" type="button" value="Add">
                                </td>
                            </tr>
                            <tr>
                                <td>0123 </td>
                                <td>MNL </td>
                                <td>CBU </td>
                                <td>09:30 AM </td>
                                <td>04:45 PM </td>
                                <td>
                                    <input class="btn custom-btn-test " data-dismiss="modal" type="button" value="Add">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
    <!-- Modal Add User -->
    <div id="modal-add-user" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modal-table-v1">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
                    <h4 class="modal-title" id="myModalLabel">View Passenger Details </h4>
                </div>
                <div class="modal-body text-left">
                    <!-- /Table -->
                    <div class="form-group no-margin">
                        <div class="col-xs-12">
                            <div class="form-box">
                                <div class="form-group">
                                    <div class="col-xs-3">
                                        <label class="control-label">First Name</label>
                                        <span class="input-notes-top ">*Autofill</span>
                                        <input class="form-control" type="text" placeholder="Input details">
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="control-label">Last Name</label>
                                        <span class="input-notes-top ">*Autofill</span>
                                        <input class="form-control" type="text" placeholder="Input details">
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group no-margin-bottom">
                                            <div class="col-xs-4">
                                                <label class="control-label">Middle Initial</label>
                                                <input class="form-control" type="text" placeholder="Input details">
                                            </div>
                                            <div class="col-xs-5 has-feedback">
                                                <label class="control-label">Depature Date</label>
                                                <input class="form-control custom-datepicker" type="text" placeholder="MM/DD/YYYY" />
                                                <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
                                                <span class="input-notes-bottom">*Note: your note</span>
                                            </div>
                                            <div class="col-xs-3">
                                                <label class="control-label">Age</label>
                                                <input class="form-control" type="number" placeholder="Age">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-3">
                                        <label class="control-label">Gender</label>
                                        <select class="selectpicker form-control" title="Choose">
                                            <option value="">Male</option>
                                            <option value="">Female</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="control-label">Contact Number</label>
                                        <input class="form-control" type="tel" placeholder="Input contact number">
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="control-label">ID Number</label>
                                        <input class="form-control" type="text" placeholder="Input detail">
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="control-label">Nationality</label>
                                        <input class="form-control" type="text" placeholder="Input detail">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-3">
                                        <label class="control-label">With Infant</label>
                                        <div class="radio radio--adj">
                                            <label class="radio-inline">
                                                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> No
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="control-label">Infat's Name</label>
                                        <input class="form-control" type="text" placeholder="Input detail">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-3">
                                        <label class="control-label">Discount</label>
                                        <select class="selectpicker form-control" title="Choose">
                                            <option value="">Discount 1</option>
                                            <option value="">Discount 2</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="control-label">Points</label>
                                        <span class="input-notes-top ">*Autofill</span>
                                        <input class="form-control" type="tel" readonly>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="control-label">Jetter's No.</label>
                                        <input class="form-control" type="text" placeholder="Input detail">
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="control-label">Total Amount</label>
                                        <span class="input-notes-top ">*Autofill</span>
                                        <input class="form-control" type="number" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer btn-oj-group text-push-right">
                    <button type="button" class="btn oj-button gray-button">Clear</button>
                    <button type="button" class="btn oj-button" data-dismiss="modal">Submit </button>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var voyage_count = '<?= $voyage_count; ?>';
    var is_return = '<?= $is_return; ?>';
</script>
<?= $footer; ?>