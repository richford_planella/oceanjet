<?=$header;?>
<form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
	<?=$this->load->view(admin_dir('notification'));?>
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Port Code</label>
      <input disabled="disabled" class="form-control focused port_code" id="port_code" name="port_code" type="text" value="<?=set_value('port_code', $port->port_code);?>">
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-5">
      <label class="control-label">Port Name</label>
      <input disabled="disabled" class="form-control focused port" id="port" name="port" type="text" value="<?=set_value('port', $port->port);?>">
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-5">
      <label class="control-label">Terminal Fee</label>
      <input disabled="disabled" class="form-control focused terminal_fee" id="terminal_fee" name="terminal_fee" type="text" value="<?=set_value('terminal_fee', $port->terminal_fee);?>">
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-xs-5">
      <label class="control-label">Port Charge</label>
      <input disabled="disabled" class="form-control focused port_charge" id="port_charge" name="port_charge" type="text" value="<?=set_value('port_charge', $port->port_charge);?>">
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label" for="outlet_code">Status</label>
      <select class="selectpicker form-control" title="Choose" name="enabled" disabled>
          <option value="1" <?=set_select('enabled', '1',(($port->enabled) ? TRUE : ''));?>>Active</option>
          <option value="0" <?=set_select('enabled', '0',((!$port->enabled) ? TRUE : ''));?>>Inactive</option>
      </select>
    </div>
  </div>
  
  <div class="form-group oj-form-footer">
    <div class="col-xs-12">
      <div class="btn-oj-group right">
        <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Back</a>
        <a class="btn oj-button" href="<?=admin_url($this->classname, 'edit', $port->id_port);?>">Edit</a>
      </div>
    </div>
  </div>
</form>
<?=$footer;?>