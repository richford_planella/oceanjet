<?=$header;?>
<form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
	<?=$this->load->view(admin_dir('notification'));?>
  <div class="form-group">
    <div class="col-xs-3">
		<label class="control-label">Port Code</label>
		<input disabled="disabled" class="form-control port_code filter_val" id="port_code" name="port_code" type="text" value="<?=set_value('port_code', $port->port_code);?>">
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-5">
		<label class="control-label">Port Name</label>
		<input disabled="disabled" class="form-control port filter_val" id="port" name="port" type="text" value="<?=set_value('port', $port->port);?>">
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-5">
		<label class="control-label">Terminal Fee</label>
		<input class="form-control terminal_fee price" id="terminal_fee" name="terminal_fee" type="number" value="<?=set_value('terminal_fee', $port->terminal_fee);?>">
		<span class="input-notes-bottom"><?=form_error('terminal_fee')?></span>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-xs-5">
		<label class="control-label">Port Charge</label>
		<input class="form-control port_charge price" id="port_charge" name="port_charge" type="number" value="<?=set_value('port_charge', $port->port_charge);?>">
		<span class="input-notes-bottom"><?=form_error('port_charge')?></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-3">
		<label class="control-label" for="enabled">Status</label>
		<select class="selectpicker form-control" title="Choose" name="enabled">
			<option value="1" <?=set_select('enabled', '1',(($port->enabled) ? TRUE : ''));?>>Active</option>
			<option value="0" <?=set_select('enabled', '0',((!$port->enabled) ? TRUE : ''));?>>Inactive</option>
			<span class="input-notes-bottom"><?=form_error('enabled')?></span>
		</select>
    </div>
  </div>
  
  <div class="form-group oj-form-footer">
    <div class="col-xs-12">
      <div class="btn-oj-group right">
        <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
        <button type="submit" class="btn oj-button">Update</button>
      </div>
    </div>
  </div>
</form>
<?=$footer;?>