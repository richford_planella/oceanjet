<?=$header;?>
<form class="form-horizontal oj-form " action="<?=current_url();?>" method="POST">
<?=$this->load->view(admin_dir('notification'));?>
  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Port Code</label>
      <input class="form-control port_code textToUpper" id="port_code" name="port_code" type="text" placeholder="Input Code" value="<?=set_value('port_code');?>" maxlength="5" />
      <span class="input-notes-bottom"><?=form_error('port_code')?></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-5">
      <label class="control-label">Port Name</label>
      <input class="form-control port" id="port" name="port" type="text" placeholder="Input Name" value="<?=set_value('port');?>" />
      <span class="input-notes-bottom"><?=form_error('port')?></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-5">
      <label class="control-label">Terminal Fee</label>
      <input class="form-control price terminal_fee" id="terminal_fee" name="terminal_fee" type="number" placeholder="Input Fee Amount" value="<?=set_value('terminal_fee');?>" />
      <span class="input-notes-bottom"><?=form_error('terminal_fee')?></span>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-xs-5">
      <label class="control-label">Port Charge</label>
      <input class="form-control price port_charge" id="port_charge" name="port_charge" type="number" placeholder="Input Charge Amount" value="<?=set_value('port_charge');?>" min="0" />
      <span class="input-notes-bottom"><?=form_error('port_charge')?></span>
    </div>
  </div>

  <div class="form-group">
    <div class="col-xs-3">
      <label class="control-label">Status</label>
      <select class="selectpicker form-control" title="" name="enabled">
          <option value="1" <?=set_select('enabled', '1');?>>Active</option>
          <option value="0" <?=set_select('enabled', '0');?>>Inactive</option>
      </select>
      <span class="input-notes-bottom"><?=form_error('enabled')?></span>
    </div>
  </div>
  
  <div class="form-group oj-form-footer">
    <div class="col-xs-12">
      <div class="btn-oj-group right">
        <a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Cancel</a>
        <button type="submit" class="btn oj-button">Save</button>
      </div>
    </div>
  </div>
</form>
<?=$footer;?>