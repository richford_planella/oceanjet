<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search" />
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>Port Code</th>
				<th>Port Name</th>
				<th>Terminal Fee</th>
				<th>Port Charge</th>
				<th>Status</th>
				<th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($port)):?>
			<?php foreach($port as $u):?>
				<tr>
					<td class="center"><?=$u->port_code?></td>
					<td class="center"><?=$u->port?></td>
					<td class="center"><?=$u->terminal_fee;?></td>
					<td class="center"><?=$u->port_charge;?></td>
					<td class="align-center">
						<?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?>
					</td>
					<td class="align-center">
						<a href="<?=admin_url($this->classname, 'view', $u->id_port);?>">View</a>
						<a href="<?=admin_url($this->classname, 'edit', $u->id_port);?>">Edit</a>
						<a href="<?=admin_url($this->classname, 'delete', $u->id_port);?>" class="btn-delete-port">Delete</a>
						<?php if($u->enabled):?>
							<a href="<?=admin_url($this->classname, 'toggle', $u->id_port, md5($token.$this->classname.$u->id_port));?>" class="btn-deactivate-port">
								De-activate
							</a>
						<?php else: ?>
							<a href="<?=admin_url($this->classname, 'toggle', $u->id_port, md5($token.$this->classname.$u->id_port));?>" class="btn-activate-port">
								Re-Activate
							</a>
						<?php endif;?>
					</td>
				</tr>   
			<?php endforeach;?>
			<?php endif;?>
        </tbody>
    </table>
<?=$footer;?>