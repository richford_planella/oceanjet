<?=$header;?>
	<form class="form-horizontal" action="<?=current_url();?>" method="POST">
			<div class="row-fluid ">
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-edit"></i><span class="break"></span><b>Modify Voyage</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?=$this->load->view(admin_dir('notification'));?>
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="voyage">Voyage No</label>
								<div class="controls">
									<input class="input-xlarge number focused voyage" id="voyage" name="voyage" type="text" value="<?=set_value('voyage', $voyage->voyage);?>">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="vessel_id">Default Vessel</label>
								<div class="controls">
									<select id="vessel_id" name="vessel_id" data-rel="chosen">
										 <option value="">Please Select</option>
										 <?php
										 	foreach ($vessel_list as $key => $value) {
										 		if ($voyage->vessel_id == $value->id_vessel)
										 			echo "<option value=".$value->id_vessel." selected>".$value->vessel_code."</option>";
										 		else
										 			echo "<option value=".$value->id_vessel.">".$value->vessel_code."</option>";
										 	}
										 ?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="origin">Origin</label>
								<div class="controls">
									<select id="origin" name="origin" data-rel="chosen">
										 <option value="">Please Select</option>
										 <?php
										 	foreach ($port_list as $key => $value) {
										 		if ($voyage->origin == $value->id_port)
										 			echo "<option value=".$value->id_port." selected>".$value->port_code."</option>";
										 		else
										 			echo "<option value=".$value->id_port.">".$value->port_code."</option>";
										 	}
										 ?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="destination">Destination</label>
								<div class="controls">
									<select id="destination" name="destination" data-rel="chosen">
										 <option value="">Please Select</option>
										 <?php
										 	foreach ($port_list as $key => $value) {
										 		if ($voyage->destination == $value->id_port)
										 			echo "<option value=".$value->id_port." selected>".$value->port_code."</option>";
										 		else
										 			echo "<option value=".$value->id_port.">".$value->port_code."</option>";
										 	}
										 ?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="ETD">ETD (Estimated Time of Departure)</label>
								<div class="controls">
									<input class="input-small" id="ETD_hr" type="number" value="<?=set_value('ETD_hr',$voyage->ETD_F[0]);?>" min=1 max=12 maxlength=2>
									:
									<input class="input-small" id="ETD_mi" type="number" value="<?=set_value('ETD_mi',$voyage->ETD_F[1]);?>" min=0 max=59 maxlength=2>
									<select class="input-small" id="ETD_f">
										<option value="am" <?php echo ("AM" == $voyage->ETD_F[2]) ? "selected" : ""?>>AM</option>
										<option value="pm" <?php echo ("PM" == $voyage->ETD_F[2]) ? "selected" : ""?>>PM</option>
									</select>
									<input type="hidden" id="ETD" name="ETD"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="ETA">ETA (Estimated Time of Arrival)</label>
								<div class="controls">
									<input class="input-small" id="ETA_hr" type="number" value="<?=set_value('ETA_hr',$voyage->ETA_F[0]);?>" min=1 max=12 maxlength=2>
									:
									<input class="input-small" id="ETA_mi" type="number" value="<?=set_value('ETA_mi',$voyage->ETA_F[1]);?>" min=0 max=59 maxlength=2>
									<select class="input-small" id="ETA_f">
										<option value="am" <?php echo ("AM" == $voyage->ETA_F[2]) ? "selected" : ""?>>AM</option>
										<option value="pm" <?php echo ("PM" == $voyage->ETA_F[2]) ? "selected" : ""?>>PM</option>
									</select>
									<input type="hidden" id="ETA" name="ETA"/>
								</div>
							</div>	
							<div class="control-group">
								<label class="control-label" for="enabled">Status</label>
								<div class="controls">
									<select id="enabled" name="enabled" data-rel="chosen">
										<option value="">Please Select</option>
										<option value="1" <?=set_select('enabled', '1',(($voyage->enabled) ? TRUE : ''));?>>Active</option>
										<option value="0" <?=set_select('enabled', '0',((!$voyage->enabled) ? TRUE : ''));?>>Inactive</option>
									</select>
								</div>
							</div>
							<div class="form-actions">
								<button type="submit" class="btn btn-primary" id="submit_btn">Save changes</button>
								<a href="<?=admin_url($this->classname);?>" class="btn">Cancel</a>
							</div>				
						</fieldset>
					</div>
				</div><!--/span-->
			
			</div><!--/row--> 
	</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>