<?=$header;?>

		<form class="form-horizontal" action="<?=current_url();?>" method="POST">
				<div class="row-fluid ">
					<div class="box span12">
						<div class="box-header" data-original-title>
							<h2><i class="icon-exclamation-sign"></i><span class="break"></span><b>View Accommodation</b></h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							</div>
						</div>
						<div class="box-content">
							<?=$this->load->view(admin_dir('notification'));?>
							<fieldset>
								<div class="control-group">
								<label class="control-label" for="voyage">Voyage No</label>
									<div class="controls">
										<input class="input-xlarge number focused voyage" id="voyage" name="voyage" type="text" value="<?=set_value('voyage', $voyage->voyage);?>" readonly>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="vessel_id">Default Vessel</label>
									<div class="controls">
										<select id="vessel_id" name="vessel_id" data-rel="chosen" readonly>
											 <option value="">Please Select</option>
											 <?php
											 	foreach ($vessel_list as $key => $value) {
											 		if ($voyage->vessel_id == $value->id_vessel)
											 			echo "<option value=".$value->id_vessel." selected>".$value->vessel_code."</option>";
											 		else
											 			echo "<option value=".$value->id_vessel.">".$value->vessel_code."</option>";
											 	}
											 ?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="origin">Origin</label>
									<div class="controls">
										<select id="origin" name="origin" data-rel="chosen" readonly>
											 <option value="">Please Select</option>
											 <?php
											 	foreach ($port_list as $key => $value) {
											 		if ($voyage->origin == $value->id_port)
											 			echo "<option value=".$value->id_port." selected>".$value->port_code."</option>";
											 		else
											 			echo "<option value=".$value->id_port.">".$value->port_code."</option>";
											 	}
											 ?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="destination">Destination</label>
									<div class="controls">
										<select id="destination" name="destination" data-rel="chosen" readonly>
											 <option value="">Please Select</option>
											 <?php
											 	foreach ($port_list as $key => $value) {
											 		if ($voyage->destination == $value->id_port)
											 			echo "<option value=".$value->id_port." selected>".$value->port_code."</option>";
											 		else
											 			echo "<option value=".$value->id_port.">".$value->port_code."</option>";
											 	}
											 ?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="ETD">ETD</label>
									<div class="controls">
										<input class="input-xlarge date focused ETD" id="ETD" name="ETD" type="text" value="<?=set_value('ETD',$voyage->ETD);?>" readonly>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="ETA">ETA</label>
									<div class="controls">
										<input class="input-xlarge date focused ETA" id="ETA" name="ETA" type="text" value="<?=set_value('ETA',$voyage->ETA);?>" readonly>
									</div>
								</div>
							</fieldset>
						</div>
					</div><!--/span-->
				
				</div><!--/row-->

				<div class="row-fluid sortable">
            	<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="icon-plus"></i><span class="break"></span><b>Default Fare Per Accommodation Type</b></h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable">
							<thead>
								<tr>
									<th class="align-center">Accommodation Type</th>
									<th>Passenger Fare</th>
								</tr>
							</thead>
							<tbody id="accommodation_type_tbody">
								<?php
									foreach ($default_fare as $key => $value) {
								?>
									<tr id="accommodation_type_tr">
										<td>
											<div class="controls">
												<select id="accommodation_id" name="accommodation_id[]" data-rel="" readonly>
													<option value="">Please Select</option>
													<?php
													 	foreach ($accommodation_list as $k => $v) {
													 		if ($value->accommodation_id == $v->id_accommodation)
													 			echo "<option value=".$v->id_accommodation." selected>".$v->accommodation_code."</option>";
													 		else
													 			echo "<option value=".$v->id_accommodation.">".$v->accommodation_code."</option>";
													 	}
													 ?>
												</select>
											</div>
										</td>
										<td>
											<div class="controls">
												<select id="fare_id" name="fare_id[]" data-rel="" readonly>
													<option value="">Please Select</option>
													<?php
													 	foreach ($fare_list as $k => $v) {
													 		if ($value->fare_id == $v->id_fare)
													 			echo "<option value=".$v->id_fare." selected>".$v->fare_code."</option>";
													 		else
													 			echo "<option value=".$v->id_fare.">".$v->fare_code."</option>";
													 	}
													 ?>
												</select>
											</div>
										</td>
									</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
	            </div>
	            <div class="row-fluid sortable">
	            	<div class="box span12">
						<div class="box-header" data-original-title>
							<h2><i class="icon-plus"></i><span class="break"></span><b>Default Fare Per Accommodation Type</b></h2>
							<div class="box-icon">
								<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							</div>
						</div>
						<div class="box-content">
							<div class="control-group">
							<label class="control-label" for="enabled">Status</label>
								<div class="controls">
									<select id="enabled" name="enabled" data-rel="chosen" readonly>
										<option value="">Please Select</option>
										<option value="1" <?=set_select('enabled', '1',(($voyage->enabled) ? TRUE : ''));?>>Active</option>
										<option value="0" <?=set_select('enabled', '0',((!$voyage->enabled) ? TRUE : ''));?>>Inactive</option>
									</select>
								</div>
							</div>
						</div>
	            </div> 
		</form>

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Warning!</h3>
		</div>
		<div class="modal-body">
			<p id="message_box"></p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
		</div>
	</div>

<?=$footer;?>

<script type="text/javascript">
$(document).ready(function(){

});

</script>