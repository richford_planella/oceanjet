<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
	<?=$this->load->view(admin_dir('notification'));?>
	  <div class="form-group">
	  	<div class="col-xs-2">
			<label class="control-label">Origin</label>
			<select class="selectpicker form-control" name="origin_id">
				<option value="">Please select</option>
				 <?php 	foreach ($port_list as $k => $v): ?>
					<option value="<?=$v->id_port ?>" <?=set_select('origin_id', $v->id_port)?>><?=$v->port_code ?></option>
				 <?php endforeach; ?>
			</select>
			<span class="input-notes-bottom"><?=form_error('origin_id')?></span>
		</div>
		<div class="col-xs-2">
			<label class="control-label">Destination</label>
			<select class="selectpicker form-control" name="destination_id">
				<option value="">Please select</option>
				 <?php 	foreach ($port_list as $k => $v): ?>
				 	<option value="<?=$v->id_port ?>" <?=set_select('destination_id', $v->id_port)?>><?=$v->port_code ?></option>
				 <?php endforeach; ?>
			</select>
			<span class="input-notes-bottom"><?=form_error('destination_id')?></span>
		</div>
		<div class="col-xs-6">
			<label class="control-label">Voyage</label>
			<select class="selectpicker form-control" name="subvoyage_management_id">
				<option value="">Please select</option>
			</select>
			<input type="hidden" value="<?=$subvoyage_management_id?>" id="subvoyage_management_id"/>
			<span class="input-notes-bottom"><?=form_error('subvoyage_management_id')?></span>
		</div>

	    <div class="col-xs-2">
	      <!--<button class="btn oj-button oj-button--margin-top-30" type="button" id="btn-view">View</button>-->
	    </div>
	  </div>

	  <div class="form-group form-group__custom" id="div-trip-details">
	    <div class="col-xs-12">
	      <div class="inner-header">
	        <h3 class="inner-header__title">Trip Details</h3>
	      </div>
	      <div class="form-group">
	        <div class="col-xs-3">
	          <label class="control-label">Voyage Code</label>
	          <input class="form-control" type="text" placeholder="Voyage Code" id="trip-voyage-code" disabled/>
	        </div>
	        <div class="col-xs-3 col-xs-offset-6">
	          <button class="btn oj-button oj-button--margin-top-30" type="button" id="btn-padala-cargo-list">View Baggage List</button>
	        </div>
	      </div>

	      <div class="form-group">
	        <div class="col-xs-3 has-feedback">
	          <label class="control-label">Depature Date</label>
	          <input class="form-control oj-date-value" type="text" placeholder="YYYY-MM-DD" value="Departure Date" id="trip-departure-date" disabled/>
	          <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
	        </div>
	        <div class="col-xs-3">
	          <label class="control-label">Origin</label>
	          <input class="form-control" type="text" placeholder="Origin" id="trip-origin" disabled/>
	        </div>
	        <div class="col-xs-3">
	          <label class="control-label">Destination</label>
	          <input class="form-control" type="text" placeholder="Destination" id="trip-destination" disabled/>
	        </div>
	      </div>
	      <div class="form-group">
	        <div class="col-xs-3 has-feedback">
	          <label class="control-label">ETD <small>(Estimated time of Departure)</small></label>
	          <input class="form-control oj-time-picker" type="text" placeholder="00:00:00" id="trip-ETD" readonly/>
	          <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
	        </div>
	        <div class="col-xs-3 has-feedback">
	          <label class="control-label">ETA <small>(Estimated Time of Arrival)</small></label>
	          <input class="form-control oj-time-picker" type="text" placeholder="00:00:00" id="trip-ETA" readonly/>
	          <span class="glyphicon glyphicon-time form-control-feedback" aria-hidden="true"></span>
	        </div>
	        <div class="col-xs-3">
	          <label class="control-label">Vessel</label>
	          <input class="form-control" type="text" id="trip-vessel" placeholder="Vessel" disabled/>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="form-group form-group__custom">
	    <div class="col-xs-12">
	      <div class="inner-header">
	        <h3 class="inner-header__title">Voyage Status</h3>
	      </div>
	      <div class="form-group">

	        <div class="col-xs-2">
	          	<label class="control-label">Status</label>
				<input class="form-control" type="text" placeholder="Status" id="trip-subvoyage-management-status" readonly/>
	        </div>
	        <div class="col-xs-2">
	          	<label class="control-label">Status Actions</label>
				<button class="btn oj-button form-control btn-voyage-status" type="button">Open for Check - In</button>
	        </div>
			<div class="col-xs-2">
	          	<label class="control-label">&nbsp;</label>
				<button class="btn oj-button form-control btn-voyage-status" type="button">Depart</button>
	        </div>
	        <div class="col-xs-2">
	          	<label class="control-label">&nbsp;</label>
				<button class="btn oj-button form-control btn-voyage-status" type="button">Undepart</button>
	        </div>
	        <div class="col-xs-2">
	          	<label class="control-label">&nbsp;</label>
				<button class="btn oj-button form-control btn-voyage-status" type="button">Delayed</button>
	        </div>
	        <div class="col-xs-2">
	          	<label class="control-label">&nbsp;</label>
				<button class="btn oj-button form-control btn-voyage-status" type="button">Cancel</button>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="form-group form-group__custom">
	    <div class="col-xs-12">
	      <div class="inner-header">
	        <h3 class="inner-header__title">Sender's Details</h3>
	      </div>
	      <div class="form-group">

	        <div class="col-xs-3">
	          	<label class="control-label">First Name</label>
				<input class="form-control voyage" type="text" placeholder="Input first name" name="sender_first_name" value="<?=set_value('sender_first_name');?>" autocomplete="off"/>
				<span class="input-notes-bottom"><?=form_error('sender_first_name')?></span>
	        </div>
	        <div class="col-xs-3">
	          	<label class="control-label">Last Name</label>
				<input class="form-control voyage" type="text" placeholder="Input last name" name="sender_last_name" value="<?=set_value('sender_last_name');?>" autocomplete="off"/>
				<span class="input-notes-bottom"><?=form_error('sender_last_name')?></span>
	        </div>
	        <div class="col-xs-2">
	          	<label class="control-label">Middle Initial</label>
				<input class="form-control voyage" type="text" placeholder="Input middle initial" name="sender_middle_initial" value="<?=set_value('sender_middle_initial');?>" autocomplete="off"/>
				<span class="input-notes-bottom"><?=form_error('sender_middle_initial')?></span>
	        </div>
	        <div class="col-xs-4">
	         	<label class="control-label">Contact Number</label>
				<input class="form-control voyage" type="text" placeholder="Input contact number" name="sender_contact_no" value="<?=set_value('sender_contact_no');?>" autocomplete="off"/>
				<span class="input-notes-bottom"><?=form_error('sender_contact_no')?></span>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="form-group form-group__custom">
	    <div class="col-xs-12">
	      <div class="inner-header">
	        <h3 class="inner-header__title">Recipient's Details</h3>
	      </div>
	      <div class="form-group">
	        <div class="col-xs-3">
	          	<label class="control-label">First Name</label>
				<input class="form-control voyage" type="text" placeholder="Input first name" name="recipient_first_name" value="<?=set_value('recipient_first_name');?>" autocomplete="off"/>
				<span class="input-notes-bottom"><?=form_error('recipient_first_name')?></span>
	        </div>
	        <div class="col-xs-3">
	          	<label class="control-label">Last Name</label>
				<input class="form-control voyage" type="text" placeholder="Input last name" name="recipient_last_name" value="<?=set_value('recipient_last_name');?>" autocomplete="off"/>
				<span class="input-notes-bottom"><?=form_error('recipient_last_name')?></span>
	        </div>
	        <div class="col-xs-2">
	          	<label class="control-label">Middle Initial</label>
				<input class="form-control voyage" type="text" placeholder="Input middle initial" name="recipient_middle_initial" value="<?=set_value('recipient_middle_initial');?>" autocomplete="off"/>
				<span class="input-notes-bottom"><?=form_error('recipient_middle_initial')?></span>
	        </div>
	        <div class="col-xs-4">
	         	<label class="control-label">Contact Number</label>
				<input class="form-control voyage" type="text" placeholder="Input contact number" name="recipient_contact_no" value="<?=set_value('recipient_contact_no');?>" autocomplete="off"/>
				<span class="input-notes-bottom"><?=form_error('recipient_contact_no')?></span>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="form-group form-group__custom">
	    <div class="col-xs-12">
	      <div class="inner-header">
	        <h3 class="inner-header__title">Padala Cargo Details</h3>
	      </div>

	      <div class="form-group">
	        <div class="col-xs-12">
	          <h4>Check-In Details</h4>
	        </div>
	        <div class="col-xs-2">
	          	<label class="control-label">Particulars</label>
				<input class="form-control" type="text" placeholder="Input particulars" id="input-particular"/>
	        </div>
	        <div class="col-xs-2">
	          <label class="control-label">Padala Rate</label>
				<select class="selectpicker form-control" id="padala-cargo-rates">
					<option value="">Please select</option>
				</select>
	        </div>
	        <div class="col-xs-2">
	          <label class="control-label">Quantity</label>
	          <input class="form-control" type="number" min="1" value="1" id="input-quantity" onkeypress="numeric_validation(event)" id="input-quantity"/>
	        </div>
	        <div class="col-xs-2">
	          <label class="control-label">UOM</label>
	          <input class="form-control" type="text" id="input-uom" readonly/>
	        </div>
	        <div class="col-xs-2">
	          <label class="control-label">Amount</label>
	          <input class="form-control" type="text" id="input-amount" readonly/>
	        </div>
	        <div class="col-xs-2">
	          <button class="btn oj-button oj-button--margin-top-30" type="button" id="btn-submit-add-cargo">Add</button>
	        </div>
	      </div>
	      <table id="main_table" class="table global-table nopadding">
	        <thead>
	          <tr>
	            <th>Particulars</th>
	            <th>Padala Rate</th>
	            <th>Quantity</th>
	            <th>UOM</th>
	            <th>Amount</th>
	            <th>Action</th>
	          </tr>
	        </thead>
	        <tbody>
	            <?php if(!empty($submitted_cargos)):?>
				<?php foreach($submitted_cargos as $u):?>
				<tr>
					<td><?=$u['particulars']?> <input type="hidden" name="particular[]" value="<?=$u['particulars']?>"/></td>
					<td><?=$u['padala_cargo']?> <input type="hidden" name="padala_cargo[]" value="<?=$u['padala_cargo_id']?>"/></td>
					<td><?=$u['quantity']?> <input type="hidden" name="quantity[]" value="<?=$u['quantity']?>"/></td>
					<td><?=$u['unit_of_measurement']?></td>
					<td><?=$u['amount']?></td>
					<td><a class="btn-delete-cargo" href="">Delete</a></td>
				</tr>   
				<?php endforeach;?>
				<?php endif;?>
	        </tbody>
	      </table>
	      <span class="input-notes-bottom"><?=form_error('cargo_description')?></span>
	    </div>
	  </div>

	  <div class="form-group form-group__footer">
	    <div class="col-xs-3">
	      	<label class="control-label">Reference No</label>
			<input class="form-control" type="text" placeholder="reference no" name="reference_no" value="<?=set_value('reference_no', uniqid());?>" autocomplete="off" readonly/>
	    	<span class="input-notes-bottom"><?=form_error('reference_no')?></span>
	    </div>
	    <div class="col-xs-3">
	      <label class="control-label">Total Amount</label>
			<input class="form-control" type="text" placeholder="XX.XX" name="total_amount" value="<?=set_value('total_amount');?>" autocomplete="off" readonly id="total-amount"/>
		    <span class="input-notes-bottom"><?=form_error('total_amount')?></span>
	    </div>
	    <div class="col-xs-6">
	      <div class="btn-oj-group right oj-button--margin-top-30">
	        <button type="submit" class="btn oj-button" id="btn-check-in">Check-In</button>
	        <button type="button" class="btn oj-button">Print BIll of Lading</button>
	      </div>
	    </div>
	  </div>

	  	<!-- Modal -->
    <div id="modal-table-v1" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg modal-table-v1">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">× </span> </button>
            <div class="col-lg-4">
            	<h4 class="modal-title" id="myModalLabel">Padala Cargo Masterlist</h4>
            </div>
            <div class="col-lg-2 col-lg-offset-6">
            	<button type="button" class="btn oj-button right">Print Manifest</button>
            </div>
          </div>
          <div class="modal-body">
            <!-- /Table -->
	        <table id="padala-cargo-list" class="table global-table nopadding">
	          <thead>
	            <tr>
	              <th>Reference No.</th>
	              <th>Name of Sender</th>
	              <th>Name of Recipient</th>
	              <th>Contact No. of Sender</th>
	              <th>Detailed Description of Cargo</th>
	              <th>Total Amount</th>
	              <th>Action</th>
	            </tr>
	          </thead>
	        </table>

          </div>

        </div>
      </div>
    </div>

</form>
<?=$footer;?>