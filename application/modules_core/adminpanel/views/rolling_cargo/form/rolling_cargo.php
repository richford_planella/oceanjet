<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">RORO Rate Code</label>
            <input readonly="readonly" class="form-control textToUpper" id="rolling_cargo_code" name="rolling_cargo_code" type="text" value="<?=set_value('rolling_cargo_code', $rolling_cargo->rolling_cargo_code);?>" placeholder="Input RORO Rate Code" />
        </div>
        <div class="col-xs-6">
            <label class="control-label">Voyage Code</label>
            <?php foreach($voyage as $v):?>
                <?php if($v->id_voyage == $rolling_cargo->voyage_id):?>
                      <input readonly="readonly" class="form-control" id="voyage_id" name="voyage_id" type="text" value="<?=set_value('voyage_id', $v->voyage);?>">
                <?php endif;?>
            <?php endforeach;?> 
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6">
            <label class="control-label">Description</label>
            <input readonly="readonly" class="form-control" id="rolling_cargo" name="rolling_cargo" type="text" value="<?=set_value('rolling_cargo', $rolling_cargo->rolling_cargo);?>" placeholder="Input Description" />
        </div>
        <div class="col-xs-2">
            <label class="control-label">Amount</label>
            <input readonly="readonly" class="form-control price" id="amount" name="amount" type="text" value="<?=set_value('amount', $rolling_cargo->amount);?>" placeholder="XX.XX" />
        </div>
        <div class="col-xs-3">
            <label class="control-label">Unit of Measurement</label>
            <input readonly="readonly" class="form-control" id="unit_measurement" name="unit_measurement" type="text" value="<?=set_value('unit_measurement', $rolling_cargo->unit_measurement);?>" placeholder="Input unit of measurement" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Rule Set</label>
            <?php foreach($rule_set as $v):?>
                <?php if($v->id_rule_set == $rolling_cargo->rule_set_id):?>
                      <input disabled="disabled" class="form-control" id="rule_set_id" name="rule_set_id" type="text" value="<?=set_value('rule_set_id', $v->rule_set);?>">
                <?php endif;?>
            <?php endforeach;?>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Status</label>
            <input disabled="disabled" class="form-control" id="enabled" name="enabled" type="text" value="<?=set_value('enabled', ($rolling_cargo->enabled) ? 'Active' : 'Inactive');?>">
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Back</a>
                <a href="<?=admin_url($this->classname,'edit',$rolling_cargo->id_rolling_cargo);?>" class="btn oj-button oj-button">Edit</a>
            </div>
        </div>
    </div>
</form>
<?=$footer;?>                        