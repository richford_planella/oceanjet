<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">RORO Rate Code</label>
            <input readonly="readonly" class="form-control textToUpper" id="rolling_cargo_code" name="rolling_cargo_code" type="text" value="<?=set_value('rolling_cargo_code', $rolling_cargo->rolling_cargo_code);?>" placeholder="Input RORO Rate Code" />
            <span class="input-notes-bottom"><?php echo form_error('rolling_cargo_code'); ?></span>
        </div>
        <div class="col-xs-6">
            <label class="control-label">Voyage Code</label>
            <select class="selectpicker form-control" id="voyage_id" name="voyage_id">
                <option value="">Please select</option>
                <?php foreach($voyage as $v):?>
                    <option value="<?=$v->id_voyage?>" <?=set_select('voyage_id', $v->id_voyage,(($rolling_cargo->voyage_id == $v->id_voyage) ? TRUE : ''));?>><?=$v->voyage;?></option>
                <?php endforeach;?>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('voyage_id'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6">
            <label class="control-label">Description</label>
            <input class="form-control" id="rolling_cargo" name="rolling_cargo" type="text" value="<?=set_value('rolling_cargo', $rolling_cargo->rolling_cargo);?>" placeholder="Input Description" />
            <span class="input-notes-bottom"><?php echo form_error('rolling_cargo'); ?></span>
        </div>
        <div class="col-xs-2">
            <label class="control-label">Amount</label>
            <input class="form-control price" id="amount" name="amount" type="text" value="<?=set_value('amount', $rolling_cargo->amount);?>" placeholder="XX.XX" />
            <span class="input-notes-bottom"><?php echo form_error('amount'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Unit of Measurement</label>
            <input class="form-control" id="unit_measurement" name="unit_measurement" type="text" value="<?=set_value('unit_measurement', $rolling_cargo->unit_measurement);?>" placeholder="Input unit of measurement" />
            <span class="input-notes-bottom"><?php echo form_error('unit_measurement'); ?></span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-3">
            <label class="control-label">Rule Set</label>
            <select class="selectpicker form-control" id="rule_set_id" name="rule_set_id">
                <option value="">Please select</option>
                <?php foreach($rule_set as $r):?>
                    <option value="<?=$r->id_rule_set?>" <?=set_select('rule_set_id', $r->id_rule_set,(($rolling_cargo->rule_set_id == $r->id_rule_set) ? TRUE : ''));?>><?=$r->rule_set;?></option>
                <?php endforeach;?>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('rule_set_id'); ?></span>
        </div>
        <div class="col-xs-3">
            <label class="control-label">Status</label>
            <select class="selectpicker form-control" id="enabled" name="enabled">
                <option value="1" <?=set_select('enabled', '1',(($rolling_cargo->enabled) ? TRUE : ''));?>>Active</option>
                <option value="0" <?=set_select('enabled', '0',((!$rolling_cargo->enabled) ? TRUE : ''));?>>Inactive</option>
            </select>
            <span class="input-notes-bottom"><?php echo form_error('enabled'); ?></span>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <a href="<?=admin_url($this->classname);?>" class="btn oj-button gray-button">Cancel</a>
                <button type="submit" name="submit" value="submit" class="btn oj-button">Update</button>
            </div>
        </div>
    </div>
</form>
<?=$footer;?>