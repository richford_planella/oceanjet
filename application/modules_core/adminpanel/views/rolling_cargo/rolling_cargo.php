<?=$header;?>
    <!-- /Table Header -->
    <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
            <div class="input-group">
                <input type="text" class="form-control" id="column3_search" placeholder="Search">
            </div>
        </div>
        <div class="col-xs-8 text-right">
            <a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <?=$this->load->view(admin_dir('notification'));?>
    <!-- /Table -->
    <table id="main_table" class="example-table table global-table nopadding">
        <thead>
            <tr>
                <th>RORO Code</th>
                <th>Description</th>
                <th>Voyage</th>
                <th>Amount</th>
                <th>Unit of Measurement</th>
                <th>Status </th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($rolling_cargo)):?>
            <?php foreach($rolling_cargo as $v):?>
            <tr>
                <td><?=$v->rolling_cargo_code;?></td>
                <td><?=$v->rolling_cargo;?></td>
                <td><?=$v->voyage;?></td>
                <td><?=number_format($v->amount,2,'.',',');?></td>
                <td><?=$v->unit_measurement;?></td>
                <td><?=($v->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
                <td>
                    <a href="<?=admin_url($this->classname, 'view', $v->id_rolling_cargo);?>">View</a>
                    <a href="<?=admin_url($this->classname, 'edit', $v->id_rolling_cargo);?>">Edit</a>
                    <a href="<?=admin_url($this->classname, 'delete', $v->id_rolling_cargo);?>" class="delete-roro">Delete</a>
                    <?php if($v->enabled):?>
                        <a href="<?=admin_url($this->classname, 'toggle', $v->id_rolling_cargo, md5($token.$this->classname.$v->id_rolling_cargo));?>" class="de-activate-roro">
                                De-activate
                        </a>
                    <?php else: ?>
                        <a href="<?=admin_url($this->classname, 'toggle', $v->id_rolling_cargo, md5($token.$this->classname.$v->id_rolling_cargo));?>" class="activate-roro">
                                Re-activate
                        </a>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
            <?php endif;?>
        </tbody>
    </table>
<?=$footer;?>