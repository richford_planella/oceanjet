<?=$header;?>

	<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST">
      <div class="form-group">
        <div class="col-xs-5">
          <label class="control-label">Payment Account Code</label>
          <input class="form-control textToUpper" type="text" placeholder="Input payment account code" name="payment_account" value="<?=set_value('payment_account', $payment->payment_account);?>" readonly />
          <span class="input-notes-bottom"><?=form_error('payment_account')?></span>
        </div>
        <div class="col-xs-5">
          <label class="control-label">Bank</label>
          <input class="form-control" type="text" placeholder="Input bank" name="bank" value="<?=set_value('bank', $payment->bank);?>" readonly />
		  <span class="input-notes-bottom"><?=form_error('bank')?></span>
        </div>
      </div>

      <div class="form-group">
        <div class="col-xs-5">
          <label class="control-label">Bank Account Name</label>
          <input class="form-control" type="text" placeholder="Input bank account name" name="bank_account_name" value="<?=set_value('bank_account_name', $payment->bank_account_name);?>" readonly />
		  <span class="input-notes-bottom"><?=form_error('bank_account_name')?></span>
		</div>
        <div class="col-xs-5">
          <label class="control-label">Bank Account Number</label>
          <input class="form-control" type="text" placeholder="Input bank account number" name="bank_account_no" value="<?=set_value('bank_account_no', $payment->bank_account_no);?>" readonly />
		  <span class="input-notes-bottom"><?=form_error('bank_account_no')?></span>
        </div>
      </div>

     <div class="form-group">
        <div class="col-xs-3">
          <label class="control-label">Status</label>
			<input class="form-control" id="enabled" type="text" name="enabled" value="<?=($payment->enabled == '1') ? 'Active' : 'Inactive' ?>" readonly />
			<span class="input-notes-bottom"><?=form_error('enabled')?></span>
        </div>
      </div>

      <div class="form-group oj-form-footer">
        <div class="col-xs-12">
          <div class="btn-oj-group right">
			<a class="btn oj-button gray-button" href="<?=admin_url($this->classname);?>">Back</a>
			<a class="btn oj-button" href="<?=admin_url($this->classname, 'edit', $payment->id_payment_account);?>">Edit</a>
          </div>
        </div>
      </div>
    </form>

<?=$footer;?>