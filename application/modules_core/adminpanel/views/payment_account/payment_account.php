<?=$header;?>

      <div class="row table-header-custom nopadding">
        <div class="col-xs-4">
          <div class="input-group">
            <input type="text" class="form-control" id="column3_search" placeholder="Search">
          </div>
        </div>
        <div class="col-xs-8 text-right">
			<a href="<?=admin_url($this->classname, 'add');?>" class="btn oj-button">Create</a>
        </div>

      </div>
	  <?=$this->load->view(admin_dir('notification'));?>
      <!-- /Table -->
      <table id="main_table" class="table global-table nopadding">
        <thead>
          <tr>
			<th>Payment Account Code</th>
			<th>Bank</th>
			<th>Bank Account Name</th>
			<th>Bank Account No.</th>
			<th>Status</th>
			<th>Actions</th>
          </tr>
        </thead>
        <tbody>
			<?php if(!empty($payment)):?>
				<?php foreach($payment as $u):?>
					<tr>
						<td class="align-center"><?=$u->payment_account; ?></td>
						<td class="center"><?=$u->bank; ?></td>
						<td class="center"><?=$u->bank_account_name; ?></td>
						<td class="center"><?=$u->bank_account_no;?></td>
						<td><?=($u->enabled)? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';?></td>
						<td>
							<a href="<?=admin_url($this->classname, 'view', $u->id_payment_account);?>">View</a>
							<a href="<?=admin_url($this->classname, 'edit', $u->id_payment_account);?>">Edit</a>
							<a href="<?=admin_url($this->classname, 'delete', $u->id_payment_account);?>" class="remove">Delete</a>
							<?php if($u->enabled):?>
								<a href="<?=admin_url($this->classname, 'toggle', $u->id_payment_account, md5($token.$this->classname.$u->id_payment_account));?>" class="deactivate">
										De-activate
								</a>
							<?php else: ?>
								<a href="<?=admin_url($this->classname, 'toggle', $u->id_payment_account, md5($token.$this->classname.$u->id_payment_account));?>" class="activate">
										Re-activate
								</a>
							<?php endif;?>
						</td>
					</tr>   
				<?php endforeach;?>
			<?php endif;?>
        </tbody>
      </table>
	
<?=$footer;?>