<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

<!--  <meta name="viewport" content="width=800"> -->
<!--  <meta name="viewport" content="min-width=800", initial-scale=1.0>-->
<!--  <meta name="viewport" content="width=device-width, initial-scale=1.0">-->

    <title><?=$this->configuration->getValue('value', array('configuration' => 'SITE_TITLE'));?></title>
    <link rel="stylesheet" href="<?=css_dir('admin/app.css');?>">

    <!--[if lt IE 9]>
        <script src="<?=js_dir('html5shiv.min.js');?>"></script>
        <script src="<?=js_dir('respond.min.js');?>"></script>
    <![endif]-->
    
    <?php if (isset($css_files) && ! empty($css_files)):?>
            <?php foreach($css_files as $css):?>
                    <link rel="stylesheet" type="text/css" href="<?=$css;?>"/>
            <?php endforeach;?>
    <?php endif;?>
                        
    <!-- start: Favicon -->
    <link rel="shortcut icon" href="<?=img_dir('favicon.png');?>">
    <!-- end: Favicon -->
        
    <script type="text/javascript">
            var baseURL = '<?=base_url();?>';
            var adminURL = '<?=admin_url();?>/';
            var siteURL = '<?=site_url();?>';
            var securedHash = '<?=$this->security->get_csrf_hash();?>';
            var classname = '<?=$this->classname;?>';
    </script>
        
</head>

<body>
    <section class="main-wrapper with-side-content clearfix  is-close">
        <header>
            <nav class="navbar navbar-default ">
                <div class="container-fluid nav-top">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?=admin_url();?>">
                            <img src="<?=img_dir('logo.png');?>" alt="Ocean Jet Logo" style="width: 40px;"/>
                        </a>
                    </div>
                    <h1 class="navbar-text"><?=$this->configuration->getValue('value', array('configuration' => 'SITE_TITLE'));?></h1>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <span class="username-log"><?=$this->session->get('admin', 'admin_name');?></span>
                        </li>
                        <li>
                            <img class="thumbnail-circle" src="<?=($this->session->get('admin', 'profile_pic'))? img_dir("users/".$this->session->get('admin', 'admin_id').'/'.$this->session->get('admin', 'profile_pic')) : img_dir("Dummy_image.png");?>" alt="image preview picture">
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-chevron-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><img class="img-circle" src="<?=($this->session->get('admin', 'profile_pic'))? img_dir("users/".$this->session->get('admin', 'admin_id').'/'.$this->session->get('admin', 'profile_pic')) : img_dir("Dummy_image.png");?>" alt=""></li>
                                <li>
                                  <h4><?=$this->session->get('admin', 'admin_name');?></h4></li>
                                <li><a href="<?=admin_url('profile');?>">my account</a></li>
                                <li><a href="<?=admin_url('logout');?>"><i class="fa fa-sign-out"></i>sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="container-fluid nav-bottom">
                    <ul class="nav navbar-nav">
                        <li  <?=($classname == 'adminpanel') ? 'class="active"' : "";?> >
                            <a href="<?=admin_url();?>"><i class="fa fa-home"></i>Dashboard</a>
                        </li>
                        <?php foreach($pages as $p):?>
                        <li <?=($classname == $p->classname OR $parent_page_id == $p->id_administrator_page) ? 'class="active"' : "";?> >
                            <a href="<?=admin_url($p->classname);?>"><i class="<?=$p->img;?>"></i><?=$p->administrator_page;?></a>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </nav>
            <!--Title Nav-->
<!--            <div class="container-fluid main-title">
                <h1> Dashboard </h1>
            </div>-->
            <!--Breadcrumbs-->
<!--            <ol class="breadcrumb custom-breadcrumb">
                <li><a href="<?=admin_url();?>">Home</a></li>
                <li class="active">Main Title</li>
            </ol>-->
        </header>

        <div class="container-fluid page-container">
            <!-- Content Header -->
            <div class="ojpage-header">
                <h1 class="pheader"><?=$title;?></h1>
            </div>
            <!--End  Content Header -->

            <!-- PDetails Container -->
            <div class="pdetails-container">