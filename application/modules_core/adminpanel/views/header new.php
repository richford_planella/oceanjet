<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!--  <meta name="viewport" content="width=800"> -->
	<!--  <meta name="viewport" content="min-width=800", initial-scale=1.0>-->
	<!--  <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

  <title><?=$this->configuration->getValue('value', array('configuration' => 'SITE_TITLE'));?></title>
  <link rel="stylesheet" href="<?=css_dir('admin/app.css');?>">

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="<?=css_dir('admin/ie.css');?>" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="<?=css_dir('admin/ie9.css');?>" rel="stylesheet">
	<![endif]-->
	
	<?php if (isset($css_files) && ! empty($css_files)):?>
		<?php foreach($css_files as $css):?>
				<link rel="stylesheet" type="text/css" href="<?=$css;?>"/>
		<?php endforeach;?>
	<?php endif;?>
	
	<script type="text/javascript">
		var baseURL = '<?=base_url();?>';
		var adminURL = '<?=admin_url();?>/';
		var siteURL = '<?=site_url();?>';
		var securedHash = '<?=$this->security->get_csrf_hash();?>';
		var classname = '<?=$this->classname;?>';
	</script>

</head>

<body>

<body>
  <section class="main-wrapper with-side-content clearfix">
    <header>
      <nav class="navbar navbar-default ">
        <div class="container-fluid nav-top">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">
              <img alt="Brand" src="...">
            </a>
          </div>
          <h1 class="navbar-text">Ocean Jet</h1>
          <ul class="nav navbar-nav navbar-right">
            <li>
              <span class="username-log">Username</span>
            </li>
            <li>
              <span class="thumbnail-circle"></span>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-chevron-down"></i></a>
              <ul class="dropdown-menu">
                <li><img class="img-circle" src="assets/images/lg-img-thumb.jpg" alt=""></li>
                <li>
                  <h4>Juan Dela Cruz</h4></li>
                <li><a href="#">my account</a></li>
                <li><a href="#"><i class="fa fa-sign-out"></i>sign out</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="container-fluid nav-bottom">
          <ul class="nav navbar-nav">
				<?php foreach($pages as $p):?>
					<?php if($p['has_submenu'] == 1):?>
						<?php if(isset($p['display']) AND $p['display'] == 1):?>
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="<?=admin_url($p['classname']);?>">
								<i class="<?=$p['img']?>"></i> <?=$p['module']?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
									<span><?=$p['module']?></span>
								</li>
								<?php foreach($p['submenu'] as $key =>$sm):?>
									<?php if(isset($sm['display']) AND $sm['display'] == 1):?>
										<li><a href="<?=admin_url($sm['classname']);?>"><i class="<?=$sm['img']?>"></i> <?=$sm['module']?></a></li>
									<?php endif;?>
								<?php endforeach;?>
							</ul>
						</li>
						<?php endif;?>
					<?php else:?>
								<?php if(isset($p['display']) AND $p['display'] == 1):?>
								<li class="dropdown">
									<a class="btn" href="<?=admin_url($p['classname']);?>">
										<i class="<?=$p['img']?>"></i> <?=$p['module']?>
									</a>
								</li>
						<?php endif;?>
					<?php endif;?>
				<?php endforeach;?>
          </ul>

          <div class="dropdown navbar-right create-quick-link">
            <button class="btn oj-button" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              create new
            </button>
            <ul class="dropdown-menu" aria-labelledby="dLabel">
              <li>
                <a href="#">Rule</a>
              </li>
              <li>
                <a href="#">Outlet</a>
              </li>
              <li>
                <a href="#">User</a>
              </li>
              <li>
                <a href="#">Voyage</a>
              </li>
              <li>
                <a href="#">Commission</a>
              </li>
              <li>
                <a href="#">Announcement</a>
              </li>
              <li>
                <a href="#">Port</a>
              </li>
              <li>
                <a href="#">Vessel</a>
              </li>
              <li>
                <a href="#">E-Voucher</a>
              </li>
              <li>
                <a href="#">Payment Account</a>
              </li>
              <li>
                <a href="#">Accomodation</a>
              </li>
              <li>
                <a href="#">Discount</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!--Title Nav-->
      <div class="container-fluid main-title">
        <h1> Main Title </h1>
      </div>
      <!--Breadcrumbs-->
      <ol class="breadcrumb custom-breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Content Management</a></li>
        <li class="active">Voyage</li>
      </ol>
    </header>
    <aside class="sidebar-wrap">
      <nav class="sub-navigation">
        <ul>
          <li class="active"><a href="#">Test</a></li>
          <li><a href="#">nav1</a></li>
          <li><a href="#">nav2</a></li>
          <li><a href="#">nav3</a></li>
        </ul>
      </nav>
    </aside>
    <div class="content-wrap">
      <div class="container-fluid page-container">
       
        <!-- Content Header -->
        <div class="ojpage-header">
          <h1 class="pheader">Your Title</h1>
        </div><!--End  Content Header -->