                        <?=$header;?>

			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
                                                <h2><i class="icon-reorder"></i><span class="break"></span><b>Audit Trail</b></h2>
						<div class="box-icon">
							<!--<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>-->
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<!--<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>-->
						</div>
					</div>
					<div class="box-content">
                                                <?=$this->load->view(admin_dir('notification'));?>
                                                <p class="center">
                                                        <a class="btn btn-primary prints" id="print_export" target="_blank" href="<?=admin_url($this->classname, 'prints', 'pdf');?>" >EXPORT TO PDF</a>
                                                        <a class="btn btn-primary prints" id="print_export_excel" target="_blank" href="<?=admin_url($this->classname, 'prints', 'excel');?>" >EXPORT TO EXCEL</a>
                                                </p>
                                                <table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Date</th>
								  <th>Time</th>
                                                                  <th>Username</th>
                                                                  <th>User</th>
                                                                  <th>Module</th>
								  <th>Action Performed</th>
							  </tr>
						  </thead>   
						  <tbody>
                                                        <?php if(!empty($logs)):?>
                                                        <?php foreach($logs as $l):?>
                                                        <tr>
								<td><?=date("Y-m-d",strtotime($l->date_added));?></td>
                                                                <td class="center"><?=date("H:i",strtotime($l->date_added));?></td>
                                                                <td class="center"><?=$l->username;?></td>
								<td class="center"><?=$l->lastname.', '.$l->firstname.' '.$l->middlename;?></td>
                                                                <td class="center"><?=ucwords(str_replace("_"," ",$l->classname));?></td>
                                                                <td class="center"><?=ucwords($l->action);?></td>
							</tr>   
                                                        <?php endforeach;?>
                                                        <?php endif;?>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<?=$footer;?>