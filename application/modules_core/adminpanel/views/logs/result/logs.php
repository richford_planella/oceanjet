<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <style>
            /*headings*/
            h1,h2,h3,h4,h5{margin:5px 0;}
            h1{font-size:24px;}
            h2{font-size:20px;}
            h3{font-size:16px;}
            h4,h5{font-size:14px;}

            body{background:#fdfdfd;font-family:Tahoma;font-size:12px;}
            .clear{clear:both;}
            .bold{font-weight: bold;}
            .text-left{width: 17%}
            
            .wrapper{width:100%;margin:0 auto;font-family:Tahoma;font-size:12px;height: 100%}
            .topBox{width:auto;margin-bottom: 5px;}
            
            .headerTable table{width: 100%;}
            
            .rowBox{width:auto;padding:5px;}
            .rowBox table{border:1px solid #333;width:100%;margin:0;font-family:Tahoma;table-layout: fixed;}
            .rowBox table td, th{word-wrap:break-word;overflow:hidden;width: 5px}
            .rowBox table td.top, table td.mid{text-align:left;padding:5px 4px;min-width:140px;font-size:12px;}
            .rowBox table td.top, td.mid, th.top, th.mid{border:1px solid #333;}
            .rowBox table td.bot{text-align:center;padding:5px 4px;font-size:12px;}
            .rowBox table td.bot span{font-size:14px;}
            .rowBox table td span{text-align:right;font-weight:bold;}
            .rowBox table td h4{font-size:14px;font-style:italic;}
            .rowBox table td h3{font-size:14px;}
            
            .vartical-align{vertical-align: top;}
            .mid-align{text-align: center;border:1px solid #333;}
            .left-align{text-align: left;border:1px solid #333;padding-left: 4px;}
            .right-align{text-align: right;border:1px solid #333;padding-right: 4px;}
            
            .width100px{width: 120px;}
            
            .vartical-no-align{vertical-align: top;}
            .mid-no-align{text-align: center;}
            .left-no-align{text-align: left;padding-left: 4px;}
            .right-no-align{text-align: right;padding-right: 4px;}
            
            .signatureBox{width:100%;border:1px solid #333;padding:5px 5px 20px 10px;}
            .sigBox{width: auto;margin: 5px 0px 10px 0px;}
            .sigBox table{border: none;width: 60%}
            @page{
                margin-top: 1mm;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <img src="<?=FCPATH;?>assets/img/logo_brown.jpg" style="height: 25px; width: 130px;padding-bottom: 10px;">
            <div class="topBox">
                <h1>AUDIT TRAIL</h1>
            </div>
            <div class="rowBox">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th class="mid-align">Date</th>
                        <th class="mid-align">Time</th>
                        <th class="mid-align">Username</th>
                        <th class="mid-align">User</th>
                        <th class="mid-align">Module</th>
                        <th class="mid-align">Action Performed</th>
                    </tr>
                    <?php foreach($logs as $l):?>
                    <tr>
                        <td class="mid-align"><?=date("Y-m-d",strtotime($l->date_added));?></td>
                        <td class="mid-align"><?=date("H:i",strtotime($l->date_added));?></td>
                        <td class="mid-align"><?=$l->username;?></td>
                        <td class="left-align"><?=$l->lastname.', '.$l->firstname.' '.$l->middlename;?></td>
                        <td class="left-align"><?=ucwords($l->classname);?></td>
                        <td class="mid-align"><?=ucwords($l->action);?></td>
                    </tr>
                    <?php endforeach;?>
                </table>
            </div>
        </div>
    </body>
</html>