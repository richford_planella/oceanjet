<?=$header;?>
<form class="form-horizontal oj-form" action="<?=current_url();?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <?=$this->load->view(admin_dir('notification'));?>
        <div class="col-xs-3">
            <label class="control-label">Site Title</label>
            <input class="form-control" type="text" name="value" placeholder="Input site title" value="<?=set_value('value',  $site_title);?>" />
            <span class="input-notes-bottom"><?php echo form_error('value'); ?></span>
        </div>
    </div>
    <div class="form-group oj-form-footer">
        <div class="col-xs-12">
            <div class="btn-oj-group right">
                <button type="submit" class="btn oj-button">Save</button>
            </div>
        </div>
    </div>
</form> 
<?=$footer;?>