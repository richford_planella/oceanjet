<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Discount Class
|--------------------------------------------------------------------------
|
| Discount Content Management
|
| @category	Controller
| @author		Philip Reamon
*/
class Discount extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		$this->load->model(admin_dir('discount/discount_model'));                
		$this->discount = new Discount_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Discount Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		// Get all discounts
		$discount = $this->discount->displaylist();
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Discounts')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.discount.js')))),
			'discount'		=> $discount,
		);
		
		parent::displayTemplate(admin_dir('discount/discount'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Discount Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// Get Discount
		$discount = new Discount_Model($this->id);
						
		// Check if a record exists
		$discount->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'View Discount')),
			'footer'        => parent::getTemplate(admin_dir('footer'),
									array("js_files" => array(
										js_dir('jquery', 'jquery.formatCurrency.js'), 
										js_dir('jquery', 'jquery.discount.js'), 
										js_dir('jquery', 'jquery.numeric-tools.js')))),
			'discount'	=> $discount,
		);
		
		parent::displayTemplate(admin_dir('discount/form/discount'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get discount
		$discount = new Discount_Model($this->id);
		
		// Check if a record exists
		$discount->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($discount->toggleStatus())
		{	
			// Check if status is in the query
			$toggle = isset($discount->enabled) ? $discount->enabled : FALSE;
			$discount_name = isset($discount->discount) ? $discount->discount : 'discount';
			
			// Set confirmation message
			if($toggle)
				$this->session->set_flashdata('confirm', ucwords($discount_name).' has been de-activated');
			else
				$this->session->set_flashdata('confirm', ucwords($discount_name).' has been re-activated');
			
			$this->session->set_flashdata('id', $discount->id);
		}
		else
		{
                        // Set confirmation message
                        $this->session->set_flashdata('note', 'Error in updating discount.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Discount
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Create Discount')),
			'footer'		=> parent::getTemplate(admin_dir('footer'),
									array("js_files" => array(
										js_dir('jquery', 'jquery.formatCurrency.js'), 
										js_dir('jquery', 'jquery.discount.js'), 
										js_dir('jquery', 'jquery.numeric-tools.js')))),
		);
			
		parent::displayTemplate(admin_dir('discount/form/add/discount'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Discount
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// Get discount
		$discount = new Discount_Model($this->id);
		
		// Check if a record exists
		$discount->redirectIfEmpty(admin_url($this->classname));
		
		// Form validation
		self::_validate();
		
		// get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Edit Discount')),
			'footer'		=> parent::getTemplate(admin_dir('footer'),
									array("js_files" => array(
										js_dir('jquery', 'jquery.formatCurrency.js'), 
										js_dir('jquery', 'jquery.discount.js'), 
										js_dir('jquery', 'jquery.numeric-tools.js')))),
			'discount'		=> $discount,
		);
		
		parent::displayTemplate(admin_dir('discount/form/edit/discount'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Delete discount
	 *
	 * @access	public
	 * @return		void
	 */
	public function delete()
	{
		// Get discounts
		$discount = new Discount_Model($this->id);
		
		// Check if a record exists
		$discount->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if($discount->countForeignKey() > 0)
		{	
			// Set confirmation message
			$this->session->set_flashdata('note', 'Cannot delete. '.ucwords($discount->discount).' was already used in the system');
		}
		else
		{	
			// Set confirmation message
			if($discount->delete())
				$this->session->set_flashdata('confirm', 'Successfully deleted '.ucwords($discount->discount));
			else
				$this->session->set_flashdata('note', "Error deleting discount");
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		if ($this->uri->rsegment(2) == 'add')
			$this->form_validation->set_rules('discount_code', 'discount code', 'required|trim|is_unique[discounts.discount_code]');
		
		$this->form_validation->set_rules('discount', 'discount name', 'required|trim');
		$this->form_validation->set_rules('discount_type', 'discount type', 'required|trim');
		$this->form_validation->set_rules('discount_amount', 'amount or percentage', 'required|trim|decimal');
		$this->form_validation->set_rules('enabled', 'status', 'required');
		
		// Get Conditions
		$conditions = $this->tools->getPost("conditions");

		if($conditions){
		
			// Conditions
			foreach($conditions as $ckey => $cvalue){
				// Format condition
				$desc = ucwords(str_replace("_", " ", $ckey));
				// Validate
				$this->form_validation->set_rules('conditions['.$ckey.']', 'Condition '.$desc, 'required');



				if($ckey == 'age_bracket'){
					$this->form_validation->set_rules("age_from", 'age from', 'required|numeric');
					$this->form_validation->set_rules("age_to", 'age to', 'required|numeric');
				}
			}			

		}
                             
		if ($this->uri->rsegment(2) == 'add')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add Discount
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
        $discount = new Discount_Model($this->id);
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$discount = new Discount_Model();
			
			parent::copyFromPost($discount, 'id_discount');
			
			// Check for successful insert
			if ($discount->add())
			{
				// Update
				$this->update_conditions($discount);
				
				parent::logThis($discount->id, 'Successfully added discount');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added discount');
				$this->session->set_flashdata('id', $discount->id);	
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving discount');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
    // --------------------------------------------------------------------
	
	/*
	 * Validate then edit Discount
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		$discount = new Discount_Model($this->id);

		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			parent::copyFromPost($discount, 'id_discount');
			
			// Check for successful update
			if ($discount->update())
			{
				// Update
				$this->update_conditions($discount);
				
				parent::logThis($discount->id, 'Successfully updated discount');
                                        
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated discount');
				$this->session->set_flashdata('id', $discount->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating discount');
			}
			
			redirect(admin_url($this->classname));
		}
	}
	
	 // --------------------------------------------------------------------
	
	/*
	 * Validate then Update Discount conditions
	 *
	 * @access	protected
	 * @return		void
	 */
	protected function update_conditions($discount)
	{
		// Get post data
		$conditions = $this->tools->getPost('conditions'); 
		
		if(isset($conditions["age_bracket"])){
			$conditions["with_age"] = !empty($this->tools->getPost("age_to")) ? 1 : 0;
			$conditions["age_from"] = $this->tools->getPost("age_from");
			$conditions["age_to"] = $this->tools->getPost("age_to");
			unset($conditions["age_bracket"]);
		}

		// Merge data
		$conditions = array_merge( array(
				"with_age" => !empty($this->tools->getPost("age_to")) ? 1 : 0,
				"age_from" => $this->tools->getPost("age_from"),
				"age_to" => $this->tools->getPost("age_to"),
				"vat_exempt" => $this->tools->getPost("vat_exempt"),
				"require_id" => $this->tools->getPost("require_id")
		), $conditions);
		
		// Update
		$discount->update(array("id_discount" => $discount->id), $conditions);
	}
	
	
   
}

/* End of file discount.php */
/* Location: ./application/modules_core/adminpanel/controllers/discount/discount.php */