<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Issue Rolling Cargo Ticket Class
|--------------------------------------------------------------------------
|
| Ticket Management
|
| @category	Controller
| @author		Richford Planella
*/
class Issue_rolling_cargo_ticket extends Admin_Core
{
    // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
		$this->load->model(admin_dir('booking/booking_model'));
		$this->load->model(admin_dir('shipper_driver_info/shipper_driver_info_model'));
		$this->load->model(admin_dir('port/port_model'));
		$this->load->model(admin_dir('ticket_series/ticket_series_ref_model'));
		$this->load->model(admin_dir('accommodation/accommodation_model'));
		$this->load->model(admin_dir('passenger_fare/passenger_fare_model'));
		
		$this->booking = new Booking_Model();
		$this->shipper_driver_info = new Shipper_Driver_Info_Model();
		$this->port = new Port_Model();
		$this->ticket_series = new Ticket_Series_Ref_Model();
		$this->accommodation = new Accommodation_Model();
		$this->passenger_fare = new Passenger_Fare_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Issue Rolling Cargo Ticket Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{
        // Session id of user
		$session_id = $this->session->get('admin','administrator_profile_id');
		
        // Query Options
		// $shipper = $this->shipper_driver_info->displayList();
		$port_options = $this->port->displayList();
		$ticket_series = $this->ticket_series->displayList(array('user_id' => $session_id, 'issued' => '0'),'','',1);

        
        // Form validation
        // self::_validate();
        
        // get data
        $data = array(
            'header'    					=> Modules::run(admin_dir('header/call_header'),array('title' => 'RORO Ticket Issuance')),
            'footer'        				=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
			'port_options'					=> $port_options,
			'ticket_series'					=> $ticket_series,
        );
        
        parent::displayTemplate(admin_dir('issue_rolling_cargo_ticket/issue_rolling_cargo_ticket/form/issue/issue_rolling_cargo_ticket'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Ticket Management Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// get user
		$ticket_management = new Ticket_management_Model($this->id);

		// Check if a record exists
		$ticket_management->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'ticket_management'		=> $ticket_management,
		);
		
		parent::displayTemplate(admin_dir('ticket_management/ticket_management/form/ticket_management'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get port
		$ticket_management = new Ticket_management_Model($this->id);
		
		// Check if a record exists
		$ticket_management->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($ticket_management->toggleStatus())
		{	
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successful in updating port.');
				$this->session->set_flashdata('id', $port->id);
		}
		else
		{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating port.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Port
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
		);
			
		parent::displayTemplate(admin_dir('issue_rolling_cargo_ticket/issue_rolling_cargo_ticket/form/add/issue_rolling_cargo_ticket'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Port
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// get user
        $issue_rolling_cargo_ticket = new Issue_Rolling_Cargo_Ticket_Model($this->id);
        
        // Check if a record exists
        $issue_rolling_cargo_ticket->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'issue_rolling_cargo_ticket'        => $issue_rolling_cargo_ticket,
        );
        
        parent::displayTemplate(admin_dir('issue_rolling_cargo_ticket/issue_rolling_cargo_ticket/form/edit/issue_rolling_cargo_ticket'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Initial Step Add Rolling Cargo Ticket
	 *
	 * @access	private
	 * @return		void
	 */
	public function issue()
	{
		// Session id of user
		$session_id = $this->session->get('admin','administrator_profile_id');
        
        // Form validation
        self::_validateInitialStep();
		
		// Query Options
		$ticket_series = $this->ticket_series->displayList(array('user_id' => $session_id, 'issued' => '0'),'','',1);
		$port_options = $this->port->displayList();
		$accommodation_options = $this->accommodation->displayList(array('enabled'=>1));
        
        // get data
        $data = array(
            'header'    					=> Modules::run(admin_dir('header/call_header'),array('title' => 'Passenger Ticket Issuance')),
            'footer'        				=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
			'ticket_series'					=> $ticket_series,
			'port_options'					=> $port_options,
			'accommodation_options'			=> $accommodation_options,
        );
        
        parent::displayTemplate(admin_dir('issue_rolling_cargo_ticket/issue_rolling_cargo_ticket/form/issue/issue_rolling_cargo_ticket'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		$this->form_validation->set_rules('trip_type', 'Trip Type', 'required|trim');
		$this->form_validation->set_rules('departure_date', 'Departure Date', 'required|trim');
		$this->form_validation->set_rules('voyage', 'Voyage', 'required|trim');
		$this->form_validation->set_rules('accommodation', 'Accommodation Type', 'required|trim');
		$this->form_validation->set_rules('firstname', 'Passenger First Name', 'required|trim');
		$this->form_validation->set_rules('lastname', 'Passenger Last Name', 'required|trim');
		$this->form_validation->set_rules('age', 'Passenger Last Name', 'required|trim');
		$this->form_validation->set_rules('gender', 'Passenger Gender', 'required|trim');
		$this->form_validation->set_rules('contact_no', 'Passenger Contact Number', 'required|trim');
		$this->form_validation->set_rules('id_no', 'Passenger ID Number', 'required|trim');
		$this->form_validation->set_rules('nationality', 'Passenger Nationality', 'required|trim');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();
            
        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
	}
	
	// --------------------------------------------------------------------
	
	private function _validateInitialStep()
	{
		// validate ticket initial step
		$this->form_validation->set_rules('voyage', 'Voyage', 'required|trim');
		// $this->form_validation->set_rules('accommodation', 'Accommodation', 'required|trim');
		
		if ($this->uri->rsegment(2) == 'issue')
			self::_issueInfo();
	}
        
    // --------------------------------------------------------------------
	
	/*
	 * Validate then add Port
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$port = new Port_Model();
			
			parent::copyFromPost($port, 'id_port');
                        
			// Check for successful insert
			if ($port->add())
			{
				parent::logThis($port->id, 'Successfully added Port');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added Port');
				$this->session->set_flashdata('id', $port->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving Port');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
    // --------------------------------------------------------------------
	
	/*
	 * Validate then edit Port
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
		$issue_rolling_cargo_ticket = new Issue_Rolling_Cargo_Ticket_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($issue_rolling_cargo_ticket, 'id_ticket');
            
            // Check for successful update
            if ($issue_rolling_cargo_ticket->update())
            {                                   
                 parent::logThis($issue_rolling_cargo_ticket->id, 'Successfully updated Ticket');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Ticket');
                $this->session->set_flashdata('id', $issue_rolling_cargo_ticket->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Eticket');
            }
            
            redirect(admin_url($this->classname));
        }
	}
	
	// --------------------------------------------------------------------
	/*
	 * Validate then move to next step of adding ticket
	 *
	 * @access		public
	 * @return		void
	 */
	private function _issueInfo()
	{
		// Session id of user
		$session_id = $this->session->get('admin','administrator_profile_id');
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$id_voyage = intval($this->tools->getPost('voyage'));
			
			$voyage_details = $this->booking->get_voyage_from_id($id_voyage);
			$passenger_fare_details = $this->passenger_fare->displayList(array('accommodation_id' => $id_accommodation));
			// $trip_details = $this->booking->get_trip_details($id_voyage);
			$ticket_details = $this->ticket_series->displayList(array('user_id' => $session_id, 'issued' => '0'),'','',1);
			$trip_statistics = '';
			
			$data = array(
				'voyage_details' => $voyage_details,
				'passenger_fare_details' => $passenger_fare_details,
				// 'trip_details' => $trip_details,
				'ticket_details' => $ticket_details,
				'trip_statistics' => $trip_statistics,
			);

			$input = $this->session->set_userdata('rolling_cargo_session_data', $data);
			
			redirect(admin_url($this->classname . '/add'),$input);
		}
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get voyage from selected origin/destination
	 *
	 * @access	public
	 * @return		void
	 */
	public function get_voyages()
	{
		$origin = intval($this->input->post('origin'));
		$destination = intval($this->input->post('destination'));
		
		$query = $this->booking->get_voyages(array('origin_id'=>$origin, 'destination_id'=>$destination));
		
		print json_encode($query);
	}
   
}

/* End of file port.php */
/* Location: ./application/modules_core/adminpanel/controllers/port/port.php */