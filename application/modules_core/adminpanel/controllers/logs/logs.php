<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Logs Class
|--------------------------------------------------------------------------
|
| Modify permissions of logs
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Logs extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
                $this->load->model(admin_dir('administrator_log/administrator_log_model'));
                
                $this->logs = new Administrator_Log_Model();
                
                // vat ID
		$this->id = $this->uri->rsegment(3);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display logs list
	 *
	 * @access		public
	 * @return		void
	 */
        public function index()
        {                
                // get data
                $data = array(
                                'header'    => Modules::run(admin_dir('header/call_header')),
                                'footer'    => parent::getTemplate(admin_dir('footer')),
                                'logs'     => $this->logs->displayList(array(),array('id_administrator_log' => 'DESC')),
                );
                
                parent::displayTemplate(admin_dir('logs/logs'),$data);
        }
        
        // --------------------------------------------------------------------
        
        
        /*
	 * Function to create pdf file
	 *
	 * @access		public
	 * @return		void
	 */
        public function prints()
        {
            // check if print to pdf
            if($this->uri->rsegment(3) == 'pdf')
            {
                $data = array(
                            'logs'     => $this->logs->displayList(),
                        );

                parent::displayTemplate(admin_dir('logs/result/logs'),$data);

                $this->load->library('pdf');

                $mpdf  = $this->pdf->load();
                $mpdf->AddPage('L');
                $mpdf->WriteHTML($this->output->get_output());
                if($this->uri->rsegment(4) == 'D')
                    $mpdf->Output("audit_trail.pdf", 'D');
                else
                    $mpdf->Output("audit_trail.pdf", 'I');
                exit;
            }
            elseif ($this->uri->rsegment(3) == 'excel')
            {
                // get logs
                $logs = $this->logs->displayList();
                
                //load our new PHPExcel library
                $this->load->library('excel');
                //activate worksheet number 1
                $this->excel->setActiveSheetIndex(0);
                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('Audit Trail');
                //set cell A1 content with some text
                $this->excel->getActiveSheet()->setCellValue('A1', 'Audit Trail');
                //change the font size
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
                //make the font become bold
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                //merge cell A1 until D1
                $this->excel->getActiveSheet()->mergeCells('A1:F1');
                //set aligment to center for that merged cell (A1 to D1)
                $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                //set column headers
                $this->excel->getActiveSheet()->setCellValue('A2', 'Date');
                $this->excel->getActiveSheet()->setCellValue('B2', 'Time');
                $this->excel->getActiveSheet()->setCellValue('C2', 'Username');
                $this->excel->getActiveSheet()->setCellValue('D2', 'User');
                $this->excel->getActiveSheet()->setCellValue('E2', 'Module');
                $this->excel->getActiveSheet()->setCellValue('F2', 'Action Performed');
                
                /** Borders for heading */
                $this->excel->getActiveSheet()->getStyle('A2:F2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        
                // loop logs
                $i = 3;
                foreach($logs as $l)
                {
                    //set columns
                    $this->excel->getActiveSheet()->setCellValue('A'.$i, date("Y-m-d",strtotime($l->date_added)));
                    $this->excel->getActiveSheet()->setCellValue('B'.$i, date("H:i",strtotime($l->date_added)));
                    $this->excel->getActiveSheet()->setCellValue('C'.$i, $l->username);
                    $this->excel->getActiveSheet()->setCellValue('D'.$i, $l->lastname.', '.$l->firstname.' '.$l->middlename);
                    $this->excel->getActiveSheet()->setCellValue('E'.$i, ucwords(str_replace("_"," ",$l->classname)));
                    $this->excel->getActiveSheet()->setCellValue('F'.$i, ucwords($l->action));
                    
                    $this->excel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                    $this->excel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                    $i++;
                }
                
                $filename='audit_trail_'.date("Y_m_d_H_i_s").'.xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache

                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
            }
        }
        
}