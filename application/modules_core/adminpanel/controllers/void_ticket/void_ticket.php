<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Void Ticket Class
|--------------------------------------------------------------------------
|
| Ticket Management
|
| @category	Controller
| @author		Richford Planella
*/
class Void_ticket extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
		$this->load->model(admin_dir('booking/Booking_Model'));
		$this->load->model(admin_dir('void_tickets/Void_Tickets_Model'));
		
		$this->booking = new Booking_Model();
		$this->void_ticket = new Void_Tickets_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Issue Rolling Cargo Ticket Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{
		// Session id of user
        $session_id = $this->session->get('admin','administrator_profile_id');
        
        // Initialize data
        $data = array(
            'header'    					=> Modules::run(admin_dir('header/call_header'),array('title' => 'Void Ticket')),
            'footer'        				=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
        );
        parent::displayTemplate(admin_dir('maintenance'),$data);
        // parent::displayTemplate(admin_dir('void_ticket/void_ticket/form/issue/void_ticket'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add to database void_ticket
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Void Ticket')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
		);
			
		parent::displayTemplate(admin_dir('void_ticket/void_ticket/form/add/void_ticket'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Initial Step Void Ticket
	 *
	 * @access	public
	 * @return		void
	 */
	public function issue()
	{   
        // Form validation
        self::_validateInitialStep();
		
		// Query Options
		
        
        // get data
        $data = array(
            'header'    					=> Modules::run(admin_dir('header/call_header'),array('title' => 'Void Ticket')),
            'footer'        				=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
        );
        
        parent::displayTemplate(admin_dir('void_ticket/void_ticket/form/issue/void_ticket'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		$this->form_validation->set_rules('void_reason', 'Reason to void', 'required|trim');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Validate then proceed to void ticket
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validateInitialStep()
	{
		// validate ticket initial step
		$this->form_validation->set_rules('ticket_number', 'Ticket Number', 'required|trim');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|trim');
		
		if ($this->uri->rsegment(2) == 'issue')
			self::_issueInfo();
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Validate then proceed to void ticket
	 *
	 * @access	private
	 * @return		void
	 */
	private function _issueInfo()
	{
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$ticket_number = $this->tools->getPost('ticket_number');
			$last_name = $this->tools->getPost('last_name');
			
			$ticket_details = $this->void_ticket->get_void_ticket_details(
				array(
					'ticket_number' => $ticket_number,
					'last_name' => $last_name
				)
			);
			
			$data = array(
				'ticket_details' => $ticket_details,
			);

			$input = $this->session->set_userdata('void_session_data', $data);
			
			redirect(admin_url($this->classname . '/add'),$input);
		}
	}
        
    // --------------------------------------------------------------------
	
	/*
	 * Validate then add Port
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$booking_id = $this->tools->getPost('booking_id');
			
			$void_ticket = new Void_Tickets_Model();
			$booking = new Booking_Model($booking_id);
			
			parent::copyFromPost($void_ticket, 'id_void_tickets');
			
			// Check for successful insert
			if ($void_ticket->add())
			{
				// Update booking ticket
				$booking->update(array(), array('booking_status_id'=>2));
				
				parent::logThis($void_ticket->id, 'Successfully Voided Ticket');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully Voided Ticket');
				$this->session->set_flashdata('id', $void_ticket->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in Voiding Ticket');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
   
}

/* End of file void_ticket.php */
/* Location: ./application/modules_core/adminpanel/controllers/void_ticket/void_ticket.php */