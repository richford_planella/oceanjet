<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Voyage Class
|--------------------------------------------------------------------------
|
| Voyage Content Management
|
| @category Controller
| @author       Philip Reamon
*/
class Roro_Eticket extends Admin_Core
{
    private $leg_count = 1;
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('booking/booking_model'));       
        $this->load->model(admin_dir('vessel/vessel_model'));
        $this->load->model(admin_dir('voyage/voyage_model'));
        $this->load->model(admin_dir('booking_roro/booking_roro_model')); 
        $this->load->model(admin_dir('rolling_cargo/rolling_cargo_model'));
        $this->load->model(admin_dir('subvoyage/subvoyage_model'));
        $this->load->model(admin_dir('subvoyage_management/subvoyage_management_model'));
        $this->load->model(admin_dir('discount/discount_model'));            
        $this->load->model(admin_dir('port/port_model'));
        $this->load->model(admin_dir('ticket_series/ticket_series_ref_model'));
         $this->load->model(admin_dir('user/user_account_model'));
        
        $this->booking = new Booking_Model();
        $this->vessel = new Vessel_Model();
        $this->voyage = new Voyage_Model();
        $this->booking_roro = new Booking_Roro_Model();
        $this->rolling_cargo = new Rolling_Cargo_Model();
        $this->subvoyage = new Subvoyage_Model();
        $this->subvoyage_management = new Subvoyage_Management_Model();
        $this->discount = new Discount_Model();
        $this->port = new Port_Model();
        $this->ticket_series_info = new Ticket_Series_Ref_Model();
       $this->user_account = new User_Account_Model(); 
                        
        // Voyage id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Voyage Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {
    // Form validation
        self::_validate();                
        // Get all Voyages
        $voyage = $this->voyage->displaylist();

        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'RORO E-Ticket Issuance')),
            'footer'    => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.roro-eticket.js')))),
            // 'footer'    => parent::getTemplate(admin_dir('footer')),
            'port'    => $this->port->displaylist(),
            'roro_rate' => $this->rolling_cargo->displaylist(),
            'discount' => $this->discount->displaylist(),
            'voyage' => $this->voyage->displaylist(),
        );
        
        parent::displayTemplate(admin_dir('third_party_outlet/roro_eticket/roro_eticket'), $data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Voyage Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get Voyage
        $voyage = new Voyage_Model($this->id);

        // get Subvoyages
        $subvoyage = new Subvoyage_Model();
                        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'View Voyage Code')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'voyage'        => $voyage,
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'subvoyages'    => $subvoyage->displayList(array("voyage_id" => $this->id))
        );
        
        parent::displayTemplate(admin_dir('voyage/form/voyage'),$data);
    }

            // --------------------------------------------------------------------
    
    /*
     * Delete voyage
     *
     * @access      public
     * @return      void
     */
    public function delete()
    {
        // get voyage
        $voyage = new Voyage_Model($this->id);
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Check if it is already in use
        if ($voyage->countFromVoyageManagement() > 0)
        {   
            // Set confirmation message
            $this->session->set_flashdata('note', 'Cannot delete. '.$voyage->voyage.' was already used in the system.');
        }
        else
        {
            //Delete its subvoyages
            $subvoyage = new Subvoyage_Model();
            $subvoyage->delete(array("voyage_id" => $this->id));

            //Delete voyage
            if ($voyage->delete())
                $this->session->set_flashdata('confirm', 'Successfully deleted '.$voyage->voyage);
            else
                $this->session->set_flashdata('note', 'Error deleting voyage');
        }
        
        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle Voyage status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get Voyage
        $voyage = new Voyage_Model($this->id);
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($voyage->toggleStatus())
        {   
            // Set confirmation message
            if($voyage->enabled)
                $this->session->set_flashdata('confirm', $voyage->voyage.' has been de-activated!');
            else
                $this->session->set_flashdata('confirm', $voyage->voyage.' has been re-activated!');
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating '.$voyage->voyage.'.');
        }

        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Voyage
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Create Voyage Schedule')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'leg_count'     => $this->leg_count
        );

        parent::displayTemplate(admin_dir('voyage/form/add/voyage'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Voyage
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get Voyage
        $voyage = new Voyage_Model($this->id);
        $subvoyage = new Subvoyage_Model();
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();

        // get data
        $data = array(
            'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit Voyage Schedule')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'voyage'        => $voyage,
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'subvoyages'    => $subvoyage->displayList(array("voyage_id" => $this->id))
        );

        parent::displayTemplate(admin_dir('voyage/form/edit/voyage'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        // $this->form_validation->set_rules('points_redemption', 'points redemption', 'required|numeric|decimal');
        // $this->form_validation->set_rules('enabled', 'status', 'required');
       
        if ($this->uri->rsegment(1) == 'roro_eticket') {
            $this->form_validation->set_rules('sender_first_name', 'sender firstname', 'required');
            $this->form_validation->set_rules('sender_last_name', 'sender lastname', 'required');
            $this->form_validation->set_rules('sender_middle_name', 'sender middlename', 'required');
            $this->form_validation->set_rules('sender_contact_no', 'sender contact number', 'required');

            $this->form_validation->set_rules('driver_first_name', 'driver firstname', 'required');
            $this->form_validation->set_rules('driver_last_name', 'driver lastname', 'required');
            $this->form_validation->set_rules('driver_middle_name', 'driver middlename', 'required');
            $this->form_validation->set_rules('driver_contact_no', 'driver contact number', 'required');

            $this->form_validation->set_rules('rolling_cargo_id', 'roro code', 'required');
            $this->form_validation->set_rules('discount_id', 'discount', 'required');

            self::_addInfo();
        }

    


    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Voyage
     *
     * @return      void
     */
    private function _addInfo()
    {
        $insert_data = array();
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            
            $ticket_series_info = new Ticket_Series_Ref_Model(); 
            // print '<pre>'.print_r($this->tools->getPost(),true).'</pre>';exit;
            // $digits = 5;
            // $series =str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
           
                    $insert_data = array(
                        "origin_id" => $this->tools->getPost("origin_id"),
                        "destination_id" => $this->tools->getPost("destination_id"),
                        "ticket_series_info_id" => $ticket_series_info->add(array("ticket_no" => $this->tools->getPost('eticket_no'))),
                        "discount_id" => $this->tools->getPost("discount_id"), 
                        "sender_first_name" => $this->tools->getPost('sender_first_name'),
                        "sender_middle_name" => $this->tools->getPost('sender_middle_name'),
                        "sender_last_name" => $this->tools->getPost('sender_last_name'),
                        "sender_contact_no" => $this->tools->getPost('sender_contact_no'),
                        "driver_first_name" => $this->tools->getPost('driver_first_name'),
                        "driver_middle_name" => $this->tools->getPost('driver_middle_name'),
                        "driver_last_name" => $this->tools->getPost('driver_last_name'),
                        "driver_contact_no" => $this->tools->getPost('driver_contact_no'),
                        "rolling_cargo_id" => $this->tools->getPost('roro_id'),
                        "booking_source_id" => 3,
                        "booking_status_id" => 1,
                    );
           
            parent::copyFromPost($this->booking_roro, 'id_booking_roro');
            
            
                // Check for successful insert
                if ($this->booking_roro->add($insert_data))
                {
                    parent::logThis($this->booking_roro->id, 'Successfully added roro ticket');
                    // Set confirmation message
                    $this->session->set_flashdata('confirm', 'Successfully added roro eticket');
                    $this->session->set_flashdata('id', $this->booking_roro->id);
                }
                else
                {
                    // Set confirmation message
                    $this->session->set_flashdata('error', 'Error in saving voyage code');
                }    
         

            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $voyage = new Voyage_Model($this->id);

        if (trim($this->tools->getPost("voyage")) == trim($voyage->voyage)) {
            $this->form_validation->set_rules('voyage', 'voyage No', 'required|trim');
        } else {
            $this->form_validation->set_rules('voyage', 'voyage No', 'required|trim|is_unique[voyage.voyage]');
        }

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            parent::copyFromPost($voyage, 'id_voyage');
            $voyage->voyage = strtoupper($voyage->voyage);

            //$voyage->origin_id = reset($this->tools->getPost("origin_id"));
            //$voyage->destination_id = end($this->tools->getPost("destination_id"));
            $voyage->ETD = DateTime::createFromFormat("H:i A", reset($this->tools->getPost("ETD")))->format("H:i:s");
            $voyage->ETA = DateTime::createFromFormat("H:i A", end($this->tools->getPost("ETA")))->format("H:i:s");
            
            // Check for successful update
            if ($voyage->update())
            {                                   
                parent::logThis($voyage->id, 'Successfully updated voyage');


                //origin_array = $this->tools->getPost("origin_id");
                //$destination_array = $this->tools->getPost("destination_id");
                $vessel_array = $this->tools->getPost("vessel_id");
                $ETD_array = $this->tools->getPost("ETD");
                $ETA_array = $this->tools->getPost("ETA");

                //Add subvoyages
                foreach ($this->tools->getPost("id_subvoyage") as $key => $value) {
                    //$origin_id = $value;
                    //$destination_id = $destination_array[$key];
                    $vessel_id = $vessel_array[$key];
                    $ETD = $ETD_array[$key];
                    $ETA = $ETA_array[$key];
                    $id_subvoyage = $value;

                    $subvoyage = new Subvoyage_Model($id_subvoyage);
                    $this->tools->setPost("voyage_id", $voyage->id);
                    $this->tools->setPost("vessel_id", $vessel_id);
                    $this->tools->setPost("ETD", DateTime::createFromFormat("H:i A", $ETD)->format("H:i:s"));
                    $this->tools->setPost("ETA", DateTime::createFromFormat("H:i A", $ETA)->format("H:i:s"));
                    //$this->tools->setPost("origin_id", $origin_id);
                    //$this->tools->setPost("destination_id", $destination_id);

                    parent::copyFromPost($subvoyage, 'id_subvoyage');
                    $subvoyage->update();
                }

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated voyage');
                $this->session->set_flashdata('id', $voyage->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating voyage');
            }
            
            redirect(admin_url($this->classname));
        }
    }
   public function get_voyage_details(){
        $arr_voyage = array();
        $voyage_id = $this->tools->getPost('voyage_id');
        $eticket_no  = "RORO-".str_pad(rand(0, pow(10, 5)-1), 5, '0', STR_PAD_LEFT);
        $arr_voyage = $this->voyage->displayList(array("id_voyage" => $voyage_id));
        $port_charge = $this->port->displaylist(array("id_port"=>$arr_voyage[0]->origin_id));
        $port_terminal_fee = $this->port->displaylist(array("id_port"=>$arr_voyage[0]->origin_id));
        $arr_voyage['eticket_no'] = $eticket_no;
        $arr_voyage['terminal_fee'] = $port_terminal_fee[0]->terminal_fee;
        $arr_voyage['port_charge'] = $port_charge[0]->port_charge;
      

        print json_encode($arr_voyage);    
    }

    public function get_roro_rate(){
        
        $roro_id = $this->tools->getPost('rid');
        $roro_rate = $this->rolling_cargo->displaylist(array("id_rolling_cargo"=>$roro_id));
       
        print json_encode($roro_rate);    
    }

    public function get_discount(){
        
        $discount_id = $this->tools->getPost('discount_id');
        $discount = $this->discount->displaylist(array("id_discount"=>$discount_id));
       
        print json_encode($discount);    
    }
   
}

/* End of file voyage.php */
/* Location: ./application/modules_core/adminpanel/controllers/voyage/voyage.php */