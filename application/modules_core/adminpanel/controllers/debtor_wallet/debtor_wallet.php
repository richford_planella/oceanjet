<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Commission Class
|--------------------------------------------------------------------------
|
| Commission Content Management
|
| @category	Controller
| @author		Philip Reamon
*/
class Debtor_Wallet extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		$this->load->model(admin_dir('third_party_outlet/debtor_management_model'));
		$this->load->model(admin_dir('third_party_outlet/debtor_wallet_model'));
		$this->load->model(admin_dir('outlet/outlet_model')); 
		$this->load->model(admin_dir('user/user_account_model'));  
		$this->load->model(admin_dir('payment_account/payment_account_model'));              
		$this->debtor_management = new Debtor_Management_Model();
		$this->debtor_wallet = new Debtor_Wallet_Model();
		$this->payment_account = new Payment_Account_Model();
		$this->outlet = new Outlet_Model();
		$this->user = new User_Account_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Commission Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		self::_validate();
		$arr_debtor=array();						
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Load Debtor Wallet')),
			'footer'	=> parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.debtor-wallet.js')))),
			'payment_account' => $this->payment_account->displaylist(),
			//'debtor_management'	=> $this->debtor_management->displaylist(),
			'user' => $this->user->displaylist(),
			'depdate' => $this->tools->getPost('deposit_date'),
			'debtor_management' => $this->debtor_management->displaylist()
		);
		
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_wallet/debtor_wallet'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Commission Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// Get commission
		$debtor_management = new Debtor_Wallet_Model($this->id);
		$deb = new Debtor_Management_Model();
		$payment_account = new Payment_Account_Model();
						
		// Check if a record exists
		$debtor_management->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Debtor Wallet')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'debtor'		=> $debtor_management,
			'debtor_management'=> $deb->displaylist(),
			'payment_account' => $payment_account->displaylist()
			
		);
		
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_wallet/form/debtor_wallet'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get commission
		$debtor_management = new Debtor_Management_Model($this->id);
		
		// Check if a record exists
		$debtor_management->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($debtor_management->toggleStatus())
		{	
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successful in updating Debtor.');
				$this->session->set_flashdata('id', $debtor_management->id);
		}
		else
		{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating Debtor.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Commission
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// echo '<pre>';
		// print_r($_POST);exit;
		// Form validation
		self::_validate();
		$arr_debtor=array();						
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Create Debtor Wallet')),
			'footer'	=> parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.debtor-wallet.js')))),
			'payment_account' => $this->payment_account->displaylist(),
			//'debtor_management'	=> $this->debtor_management->displaylist(),
			'user' => $this->user->displaylist(),
			'depdate' => $this->tools->getPost('deposit_date'),
			'debtor_management' => $this->debtor_management->displaylist()
		);

		
		//echo '<pre>';print_r($data['debtor_management']);
			
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_wallet/form/add/debtor_wallet'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Commission
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// get user
		$debtor_management = new Debtor_Wallet_Model($this->id);
		
		// Check if a record exists
		$debtor_management->redirectIfEmpty(admin_url($this->classname));
		
		// Form validation
		self::_validate();
		
		// get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Debtor Wallet')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'debtor'		=> $debtor_management,
			'commission' => $this->commission->displaylist(),
			'outlet' => $this->outlet->displaylist(),
			'user' => $this->user->displaylist()
		);
		
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_management/form/edit/debtor_wallet'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		$this->form_validation->set_rules('debtor_id', 'Debtor Code', 'required|trim');
		
		$this->form_validation->set_rules('payment_account_id', 'payment account code', 'required|trim');
		$this->form_validation->set_rules('deposit_date', 'date of deposit', 'required|trim|date');
		$this->form_validation->set_rules('reference', 'reference', 'required|trim');
		// $this->form_validation->set_rules('type', 'type', 'required|trim');
		$this->form_validation->set_rules('wallet_amount', 'amount', 'required|numeric|decimal');
		// $this->form_validation->set_rules('outlet_id', 'Outlet/s', 'required');
		// $this->form_validation->set_rules('enabled', 'Status', 'required');
                             
		// if ($this->uri->rsegment(2) == 'add')
		if ($this->uri->rsegment(1) == 'debtor_wallet')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add Commission
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{

		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$new_amount = 0.00;
			$debtor_wallet = new Debtor_Wallet_Model();
			$debtor_management = new Debtor_Management_Model($this->tools->getPost('debtor_id'));

			$debtor_wallet->deposit_date = date("Y-m-d",strtotime($this->tools->getPost('deposit_date')));
			
            parent::copyFromPost($debtor_wallet, 'id_debtor_wallet');
         
            // Add wallet
            if ($this->tools->getPost("add_wallet")) {
            	$debtor_wallet->type = $this->tools->getPost("add_wallet");
            	// Compute amount
				$new_amount = ($debtor_management->amount + $this->tools->getPost('wallet_amount'));
				// insert to debtor wallet
				$debtor_wallet->add();
				parent::logThis($debtor_wallet->id, 'Successfully added Debtor Wallet');
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added Debtor Wallet');
				$this->session->set_flashdata('id', $debtor_wallet->id);
				$debtor_management->update(array("id_debtor" => $this->tools->getPost('debtor_id')),array('amount' => $new_amount));
            }			
			
			// Deduct Amount
			if ($this->tools->getPost("deduct_wallet")) {
				// print_r($this->tools->getPost());exit;
				$debtor_wallet->type = $this->tools->getPost("deduct_wallet");
				// check if wallet amount is less than the current debtor wallet amount
				if ($this->tools->getPost('wallet_amount') <= $debtor_management->amount) {
					// Compute new debtor wallet amount
					$new_amount = ($debtor_management->amount - $this->tools->getPost('wallet_amount'));
					// Insert to debtor wallet
					$debtor_wallet->add();
					parent::logThis($debtor_wallet->id, 'Successfully added Debtor Wallet');
					// Set confirmation message
					$this->session->set_flashdata('confirm', 'Successfully added Debtor Wallet');
					$this->session->set_flashdata('id', $debtor_wallet->id);
					$debtor_management->update(array("id_debtor" => $this->tools->getPost('debtor_id')),array('amount' => $new_amount));
				} else {
				
					$this->session->set_flashdata('error', 'Error in Deducting Debtor Wallet');	
				}
			}	

			
			redirect(admin_url($this->classname));
						
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit Commission
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		$debtor_management = new Debtor_Management_Model($this->id);

		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                    
			parent::copyFromPost($debtor_management, 'id_debtor');
			
			// Check for successful update
			if ($debtor_management->update())
			{									
				 parent::logThis($debtor_management->id, 'Successfully updated commission');
                                        
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated commission');
				$this->session->set_flashdata('id', $debtor_management->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating commission');
			}
			
			redirect(admin_url($this->classname));
		}
	}
	public function generateWalletReport() {
		// print_r($this->tools->getPost());exit;
		$arr_debtor = array();
		$dateTo ="";
		$dateFrom="";
		if ($this->tools->getPost("generate_btn")) {
			if (!empty($this->tools->getPost('date_from')) || !empty($this->tools->getPost('date_to'))) {
				$dateFrom = date("Y-m-d",strtotime($this->tools->getPost('date_from')));
				$dateTo = date("Y-m-d",strtotime($this->tools->getPost('date_to')));
				$arr_debtor = $this->debtor_wallet->displaylist(array("DATE(deposit_date) >=" =>"$dateFrom", "DATE(deposit_date) <=" => "$dateTo" ));
			} else {
				$dateTo ="";
		$dateFrom="";
				$arr_debtor =  $this->debtor_wallet->displaylist();
			}	
		}
		

		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Wallet Loading Report')),
			'footer'	=> parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.debtor-wallet.js')))),
			'print_name' => $this->session->get('admin','admin_name'),
			'date_to' => $dateTo,
			'date_from' => $dateFrom,
			'debtor' => $arr_debtor 
			);
		

		
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_wallet/form/generate/debtor_wallet'),$data);
	}
	public function printDebtorWalletPDF() {
		
		$data = array(
			'print_name' => $this->session->get('admin','admin_name') 
			);
		//echo '<pre>';print_r($data['debtor_management']);
		if (!empty($this->tools->getGet('date_from')) && !empty($this->tools->getGet('date_to'))) {
			$dateFrom = date("Y-m-d",strtotime($this->tools->getGet('date_from')));
			$dateTo = date("Y-m-d",strtotime($this->tools->getGet('date_to')));
			$data['date_from'] = $dateFrom;
			$data['date_to'] = $dateTo;
			$data['debtor'] = $this->debtor_wallet->displaylist(array("DATE(deposit_date) >=" =>"$dateFrom", "DATE(deposit_date) <=" => "$dateTo" ));
		} else {
			$data['date_from'] = date("m-d-Y");
			$data['date_to'] = date("m-d-Y");
			$data['debtor'] = $this->debtor_wallet->displaylist();
		}
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_wallet/form/result/debtor_wallet'),$data);
		 $this->load->library('pdf');

            $mpdf  = $this->pdf->load();
            $mpdf->showImageErrors = true;
            $mpdf->AddPage('L');
            $mpdf->WriteHTML($this->output->get_output());
           
                $mpdf->Output("OceanJet_Wallet_Report".date("dmY").".pdf", 'I');
            exit;



	}
	public function downloadDebtorWalletPDF() {
		// Get all commission
		
		$data = array(
			'print_name' => $this->session->get('admin','admin_name') 
			);
		if (!empty($this->tools->getGet('date_from')) && !empty($this->tools->getGet('date_to'))) {
			$dateFrom = date("Y-m-d",strtotime($this->tools->getGet('date_from')));
			$dateTo = date("Y-m-d",strtotime($this->tools->getGet('date_to')));
			$data['date_from'] = $dateFrom;
			$data['date_to'] = $dateTo;
			$data['debtor'] = $this->debtor_wallet->displaylist(array("DATE(deposit_date) >=" =>"$dateFrom", "DATE(deposit_date) <=" => "$dateTo" ));
		} else {
			$data['date_from'] = date("m-d-Y");
			$data['date_to'] = date("m-d-Y");
			$data['debtor'] = $this->debtor_wallet->displaylist();
		}

		//echo '<pre>';print_r($data['debtor_management']);
		
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_wallet/form/result/debtor_wallet'),$data);
		 $this->load->library('pdf');

            $mpdf  = $this->pdf->load();
            $mpdf->showImageErrors = true;
            $mpdf->AddPage('L');
            $mpdf->WriteHTML($this->output->get_output());
            //if($this->uri->rsegment(4) == 'D')
                $mpdf->Output("OceanJet_Wallet_Report".date("dmY").".pdf", 'D');
            //else
            //    $mpdf->Output("test.pdf", 'I');
            exit;

	}

	public function downloadDebtorWalletExcel() {
		 //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Wallet Loading Report');
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Wallet Loading Report');
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        $this->excel->getActiveSheet()->mergeCells('A1:E1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'From Date:');
        $this->excel->getActiveSheet()->setCellValue('B2', date("m-d-Y",strtotime($this->tools->getGet('date_from'))));
        $this->excel->getActiveSheet()->setCellValue('C2', 'To Date:');
        $this->excel->getActiveSheet()->setCellValue('D2', date("m-d-Y",strtotime($this->tools->getGet('date_to'))));
        //set column headers
        $this->excel->getActiveSheet()->setCellValue('A3', 'Deposit No.');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Agent Code');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Agent Name');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Amount Loaded By');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Payment Details');
        /** Borders for heading */
        $this->excel->getActiveSheet()->getStyle('A3:E3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		if (!empty($this->tools->getGet('date_from')) && !empty($this->tools->getGet('date_to'))) {
			$dateFrom = date("Y-m-d",strtotime($this->tools->getGet('date_from')));
			$dateTo = date("Y-m-d",strtotime($this->tools->getGet('date_to')));
			$debtor = $this->debtor_wallet->displaylist(array("DATE(deposit_date) >=" =>"$dateFrom", "DATE(deposit_date) <=" => "$dateTo" ));
		} else {
			$debtor = $this->debtor_wallet->displaylist();
		}
		
	$test = 0;
        // loop logs
        $i = 4;
        // loop through debtor wallet
        foreach($debtor as $d){
        	if ($d->type == 1) {
        		$type = "Added";
        	} else {
        		$type = "Deducted";
        	}
            //set columns
            $this->excel->getActiveSheet()->setCellValue('A'.$i, $d->id_debtor_wallet);
            $this->excel->getActiveSheet()->setCellValue('B'.$i, $d->debtor_code);
            $this->excel->getActiveSheet()->setCellValue('C'.$i, $d->firstname." ".$d->lastname);
            $this->excel->getActiveSheet()->setCellValue('D'.$i ,'TBA');
            $this->excel->getActiveSheet()->setCellValue('E'.$i, $type." ".$d->wallet_amount);
           

            $i++;

            $test = $i;
        }
        $test = $test + 2;
        $this->excel->getActiveSheet()->setCellValue('A'.$test, 'Printed By:');
        $this->excel->getActiveSheet()->setCellValue('C'.$test, 'Checked By:');
        $this->excel->getActiveSheet()->setCellValue('E'.$test, 'Audited By:');

      	$this->excel->getActiveSheet()->setCellValue('A'.($test+1), preg_replace("/\s|&nbsp;/",'',$this->session->get('admin','admin_name')));
        $this->excel->getActiveSheet()->setCellValue('A'.($test+2), date("m-d-Y H:i:s"));

        
       

        $filename="OceanJet_Wallet_Report".date("dmY").".xls"; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');


	}
   
}

/* End of file commission.php */
/* Location: ./application/modules_core/adminpanel/controllers/commission/commission.php */