<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Refund Ticket Class
|--------------------------------------------------------------------------
|
| Ticket Management
|
| @category	Controller
| @author		Richford Planella
*/
class Revalidate_ticket extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
		$this->load->model(admin_dir('booking/Booking_Model'));
		
		$this->booking = new Booking_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Revalidate Ticket Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{
        // get data
        $data = array(
            'header'    					=> Modules::run(admin_dir('header/call_header'),array('title'=>'Revalidate Ticket')),
            'footer'        				=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
        );
        
        // parent::displayTemplate(admin_dir('revalidate_ticket/revalidate_ticket/form/issue/revalidate_ticket'),$data);
        parent::displayTemplate(admin_dir('maintenance'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add/update to database revalidate ticket
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title'=>'Revalidate Ticket')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
		);
			
		parent::displayTemplate(admin_dir('revalidate_ticket/revalidate_ticket/form/add/revalidate_ticket'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Initial Step Revalidate Ticket
	 *
	 * @access	public
	 * @return		void
	 */
	public function issue()
	{   
        // Form validation
        self::_validateInitialStep();
		
		// Query Options
		
        
        // get data
        $data = array(
            'header'    					=> Modules::run(admin_dir('header/call_header'),array('title' => 'Revalidate Ticket')),
            'footer'        				=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
        );
        
        parent::displayTemplate(admin_dir('revalidate_ticket/revalidate_ticket/form/issue/revalidate_ticket'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		$this->form_validation->set_rules('nationality', 'Passenger Nationality', 'required|trim');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();
	}
        
    // --------------------------------------------------------------------
	
	/*
	 * Validate then proceed to refund ticket
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validateInitialStep()
	{
		// validate ticket initial step
		$this->form_validation->set_rules('ticket_number', 'Ticket Number', 'required|trim');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|trim');
		
		if ($this->uri->rsegment(2) == 'issue')
			self::_issueInfo();
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Validate then add Port
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
        // Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$port = new Port_Model();
			
			parent::copyFromPost($port, 'id_port');
                        
			// Check for successful insert
			if ($port->add())
			{
				parent::logThis($port->id, 'Successfully added Port');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added Port');
				$this->session->set_flashdata('id', $port->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving Port');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
    // --------------------------------------------------------------------
	
	/*
	 * Validate then proceed to void ticket
	 *
	 * @access	private
	 * @return		void
	 */
	private function _issueInfo()
	{
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$ticket_number = $this->tools->getPost('ticket_number');
			$last_name = $this->tools->getPost('last_name');
			
			$ticket_details = $this->refund_ticket->get_refund_ticket_details(
				array(
					'ticket_number' => $ticket_number,
					'last_name' => $last_name
				)
			);
			
			$data = array(
				'ticket_details' => $ticket_details,
			);

			$input = $this->session->set_userdata('revalidate_session_data', $data);
			
			redirect(admin_url($this->classname . '/add'),$input);
		}
	}
        
    // --------------------------------------------------------------------
	
	
   
}

/* End of file void_ticket.php */
/* Location: ./application/modules_core/adminpanel/controllers/void_ticket/void_ticket.php */