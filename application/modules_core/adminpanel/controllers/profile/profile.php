<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| User Profile Class
|--------------------------------------------------------------------------
|
| Modify permissions of users
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Profile extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
                $this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('user/user_model'));
                $this->load->model(admin_dir('user/user_account_model'));
                $this->load->model(admin_dir('outlet/outlet_model'));
                $this->load->model(admin_dir('user/user_password_model'));
                $this->load->model(admin_dir('reset_password/reset_password_model'));
                $this->load->model(admin_dir('configuration/configuration_model'));
                
                $this->user_profile = new Profile_Model();
                $this->user = new User_Model();
                $this->user_account = new User_Account_Model();
                $this->outlet = new Outlet_Model();
                $this->user_password = new User_Password_Model();
                $this->configuration = new Configuration_Model();
                $this->reset_password = new Reset_Password_Model();
                
                // if session has value
                if( ! $this->session->get('admin', 'admin_login') OR $this->session->get('admin', 'encryption_key') != $this->config->item('encryption_key'))
                {
                        // redirect to dashboard
                        redirect(admin_url());
                }
                
                // user ID
		$this->id = $this->session->get('admin', 'admin_id');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display user profile
	 *
	 * @access		public
	 * @return		void
	 */
        public function index()
        {                
                // Get user detail
                $user = new User_Model($this->id);
                
                // Check if a record exists
		$user->redirectIfEmpty(admin_url());
                
                // get user account details
                $user->user_account = new User_Account_Model($user->id_user);
                
                // get user profile
                $user->user_profile = $this->user_profile->getValue('user_profile',array('id_user_profile' => $user->user_profile_id));
                
                $user_profile = 0;
                
                if($this->user_profile->getValue('is_outlet', array('id_user_profile' => $user->user_profile_id)) == 1)
                    $user_profile = 1;
                                
                if(isset($_FILES['photo']['name']) AND $_FILES['photo']['name'] <> "")
                {
                    // create folder for user
                    if(!is_dir(FCPATH.'assets/img/users/'.$user->id))
                    {
                        mkdir(FCPATH.'assets/img/users/'.$user->id);
                    }
                    
                    // update user account profile pic
                    $user_account = new User_Account_Model($user->id);

                    $filename = $_FILES['photo']['name'];
                    $fileExt = end(explode(".", $filename));
                    $newFileName = md5(time()).".".$fileExt;

                    $filepath = FCPATH.'assets/img/users/'.$user->id;

                    $file_info = parent::uploadImg($filepath,$newFileName);

                    // delete existing profile pic
                    if($user_account->profile_pic <> "")
                        unlink(FCPATH.'assets/img/users/'.$user->id.'/'.$user_account->profile_pic);

                    $user_account->update(array(),array('profile_pic' => $newFileName));
                    
                    // check if update self
                    if($this->session->get('admin', 'admin_id') == $this->id)
                    {
                        $new_user = new User_Model($this->id);

                        // get user account details
                        $new_user->user_account = new User_Account_Model($new_user->id_user);

                        // If all the conditions are true, initialize the session data
                        $newdata = array(
                                'admin_id'                  => $new_user->id_user,
                                'admin_email'               => $new_user->user_account->email_address,
                                'admin_username'            => $new_user->username,
                                'admin_name'                => substr($new_user->user_account->firstname, 0, 1).'.&nbsp;'.htmlentities(strtoupper($new_user->user_account->lastname), ENT_COMPAT, 'UTF-8'),
                                'admin_login'               => TRUE,
                                'encryption_key'            => $this->config->item('encryption_key'),
                                'administrator_profile_id'  => $new_user->user_profile_id,
                                'is_new'                    => $new_user->is_new,
                                'profile_pic'               => $new_user->user_account->profile_pic,
                                // Security token
                                'token'                     => substr(sha1(microtime()),4,30)
                        );

                        // Set the session data
                        $this->session->set_userdata('admin', $newdata);
                    }
                    
                    $this->session->set_flashdata('confirm', 'You have successfully updated your profile picture!');
                    redirect(admin_url($this->classname));
                }
                    
                
                // get data
                $data = array(
                                'header' => Modules::run(admin_dir('header/call_profile_header'),array('title' => 'Account Details', 'index' => 'profile')),
                                'footer'            => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.profile-tools.js')))),
                                'user_profile'      => $this->user_profile->displayList(array('enabled' => 1)),
                                'user'              => $user,
                                'outlet'            => $this->outlet->displayList(array('u.enabled' => 1)),
                                'user_type'         => $user_profile,
                );
                
                parent::displayTemplate(admin_dir('profile/form/profile'),$data);
        }  
        
        // --------------------------------------------------------------------
	
	/*
	 * User change password
	 *
	 * @access		public
	 * @return		void
	 */
        public function change_password()
        {                
                // Get user detail
                $user = new User_Model($this->id);
                
                // Check if a record exists
		$user->redirectIfEmpty(admin_url());
                
                // Form validation
		self::_validate();
                
                // get data
                $data = array(
                                'header' => Modules::run(admin_dir('header/call_profile_header'),array('title' => 'Change Password', 'index' => 'change_password')),
                                'footer'            => parent::getTemplate(admin_dir('footer')),
                );
                
                parent::displayTemplate(admin_dir('profile/form/change_password'),$data);
        }  
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{	
                $uppercase  = $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_UPPERCASE'));
                $lowercase  = $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_LOWERCASE'));
                $number     = $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_NUMBER'));
                $spl_char   = $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_SPL_CHAR'));
                $max        = $this->configuration->getValue('value', array('configuration' => 'MAX_PASSWORD_CHAR'));
                $min        = $this->configuration->getValue('value', array('configuration' => 'MIN_PASSWORD_CHAR'));
                
                $this->form_validation->set_rules('cur_password', 'current password', 'required|trim');
		$this->form_validation->set_rules('password', 'new password', 'required|min_length['.$min.']|max_length['.$max.']|password_check['.$uppercase.','.$lowercase.','.$number.','.$spl_char.']|matches[new_password]');
		$this->form_validation->set_rules('new_password', 'confirm new password', '');
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$user_id = $this->session->get('admin','admin_id');
			$user = new User_Model($user_id);
                        
                        // get user account details
                        $user->user_account = new User_Account_Model($user->id_user);
                
                        // check current password
                        if($user->password <> hash('sha256', $this->tools->getPost('cur_password').$user->salt))
                        {
                            // redirect to change password
                            $this->session->set_flashdata('note', 'Incorrect current password. Please try again!');
                            redirect(admin_url($this->classname,'change_password'));
                        }
                        
                        // check password history
                        $password_history = $this->user_password->displayList(array('user_id' => $user->id_user),array('id_user_password' => 'DESC'));
                        
                        $password_count = $this->configuration->getValue('value', array('configuration' => 'NUM_PASSWORD_CHANGE'));
                        
                        if(count($password_history) > $password_count)
                        {
                            $diff = count($password_history) - $password_count;
                            // cut array
                            array_splice($password_history, $diff);
                        }
                        
                        // loop password history
                        foreach($password_history as $p)
                        {
                            // check if user use current password
                            if($p->password == hash('sha256', $this->tools->getPost('password').$p->salt))
                            {
                                // redirect to change password
                                $this->session->set_flashdata('note', 'You cannot use your previous password as your new password');
                                redirect(admin_url($this->classname,'change_password'));
                            }
                        }
                        
			// handle brute force attacks
			sleep(1);
                        
                        //$email_data = array(
                        //                        'user'          => $user,
                        //                        'employee_name' => $user->user_account->lastname.', '.$user->user_account->firstname,
                        //                        'username'      => $user->employee_num,
                        //                    );
                        
                        // set email parameter
                        //$subject = 'Change Password';
                        //$body = parent::getTemplate(admin_dir('email_template/change_password'),$email_data);
                        
                        // send email
                        //parent::sendMail($body,$subject,$user->user_account->email_address);
                        
                        // update user information
                        $secure_token = substr(sha1(microtime()),4,30);
                        $salt = $this->misc->generate_code(8);
                        $password = hash('sha256', $this->tools->getPost('password').$salt);
                        
                        $user->update(array(),array(
                                                        'secure_token'      => $secure_token,
                                                        'salt'              => $salt,
                                                        'password'          => $password,
                                                        'is_new'            => 0,
                                                        'num_failed_login'  => 0,
                                                        'password_generated'=> date("Y-m-d"),
                                                        'last_login'        => date("Y-m-d H:i:s"),
                                                    ));
                        //insert user password
                        $this->user_password->add(array(
                                                            'user_id'   => $user->id,
                                                            'password'  => $password,
                                                            'salt'      => $salt,
                                                        ));
                        
                        // delete existing record
                        $this->reset_password->delete(array('user_id' => $user->id));
                            
                        $this->session->unset_userdata('admin');
		
                        $this->session->set_flashdata('confirm', 'Password successfully changed!');
                        redirect(admin_url('login'));
                
                        // Redirect to logout
			//redirect(admin_url('logout'));
		}
	}
}

/* End of file profile.php */
/* Location: ./application/modules_core/adminpanel/controllers/profile/profile.php */