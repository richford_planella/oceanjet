<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Refund Ticket Class
|--------------------------------------------------------------------------
|
| Ticket Management
|
| @category	Controller
| @author		Richford Planella
*/
class Upgrade_ticket extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
		$this->load->model(admin_dir('booking/Booking_Model'));
		
		$this->booking = new Booking_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Revalidate Ticket Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{
        
        // Check if a record exists
        // $issue_rolling_cargo_ticket->redirectIfEmpty(admin_url($this->classname));
        // $booking->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
		
		// Query Options
        
        // get data
        $data = array(
            'header'    					=> Modules::run(admin_dir('header/call_header')),
            'footer'        				=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
        );
        
        parent::displayTemplate(admin_dir('upgrade_ticket/upgrade_ticket/form/edit/upgrade_ticket'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Ticket Management Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// get user
		$ticket_management = new Ticket_management_Model($this->id);

		// Check if a record exists
		$ticket_management->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'ticket_management'		=> $ticket_management,
		);
		
		parent::displayTemplate(admin_dir('ticket_management/ticket_management/form/ticket_management'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get port
		$ticket_management = new Ticket_management_Model($this->id);
		
		// Check if a record exists
		$ticket_management->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($ticket_management->toggleStatus())
		{	
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successful in updating port.');
				$this->session->set_flashdata('id', $port->id);
		}
		else
		{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating port.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Port
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
		);
			
		parent::displayTemplate(admin_dir('port/port/form/add/port'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Port
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// get user
        // $issue_rolling_cargo_ticket = new Issue_Rolling_Cargo_Ticket_Model($this->id);
        
        // Check if a record exists
        // $issue_rolling_cargo_ticket->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            // 'issue_rolling_cargo_ticket'        => $issue_rolling_cargo_ticket,
        );
        
        parent::displayTemplate(admin_dir('upgrade_ticket/upgrade_ticket/form/edit/upgrade_ticket'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		$this->form_validation->set_rules('trip_type', 'Trip Type', 'required|trim');
		$this->form_validation->set_rules('departure_date', 'Departure Date', 'required|trim');
		$this->form_validation->set_rules('voyage', 'Voyage', 'required|trim');
		$this->form_validation->set_rules('accommodation', 'Accommodation Type', 'required|trim');
		$this->form_validation->set_rules('firstname', 'Passenger First Name', 'required|trim');
		$this->form_validation->set_rules('lastname', 'Passenger Last Name', 'required|trim');
		$this->form_validation->set_rules('age', 'Passenger Last Name', 'required|trim');
		$this->form_validation->set_rules('gender', 'Passenger Gender', 'required|trim');
		$this->form_validation->set_rules('contact_no', 'Passenger Contact Number', 'required|trim');
		$this->form_validation->set_rules('id_no', 'Passenger ID Number', 'required|trim');
		$this->form_validation->set_rules('nationality', 'Passenger Nationality', 'required|trim');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();
            
        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add Port
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$port = new Port_Model();
			
			parent::copyFromPost($port, 'id_port');
                        
			// Check for successful insert
			if ($port->add())
			{
				parent::logThis($port->id, 'Successfully added Port');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added Port');
				$this->session->set_flashdata('id', $port->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving Port');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit Port
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
		$issue_rolling_cargo_ticket = new Issue_Rolling_Cargo_Ticket_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($issue_rolling_cargo_ticket, 'id_ticket');
            
            // Check for successful update
            if ($issue_rolling_cargo_ticket->update())
            {                                   
                 parent::logThis($issue_rolling_cargo_ticket->id, 'Successfully updated Ticket');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Ticket');
                $this->session->set_flashdata('id', $issue_rolling_cargo_ticket->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Eticket');
            }
            
            redirect(admin_url($this->classname));
        }
	}
   
}

/* End of file void_ticket.php */
/* Location: ./application/modules_core/adminpanel/controllers/void_ticket/void_ticket.php */