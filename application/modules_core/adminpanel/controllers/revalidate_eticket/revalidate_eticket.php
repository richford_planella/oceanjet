<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Class
|--------------------------------------------------------------------------
|
| Outlet Content Management
|
| @category Controller
| @author       Ronald Meran
*/
class Revalidate_Eticket extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('third_party_outlet/revalidate_eticket_model'));   
        $this->load->model(admin_dir('trip_type/trip_type_model')); 
        $this->load->model(admin_dir('voyage/voyage_model'));   
        $this->load->model(admin_dir('accommodation/accommodation_model'));   
        $this->load->model(admin_dir('third_party_outlet/void_eticket_model'));       
        $this->revalidate_eticket = new Revalidate_Eticket_Model();
        $this->trip_type = new Trip_Type_Model();
        $this->voyage = new Voyage_Model();
        $this->accommodation = new Accommodation_Model();
        $this->void_eticket = new Void_Eticket_Model();
        // $this->load->model(admin_dir('/Eticket_Model'));
                        
        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Outlet Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        // $eticket = new Eticket_Model(1);
        
        // print_r($this->eticket_passenger->trip_type());exit;
        // Check if a record exists
        // $eticket_passenger->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.revalidate-eticket.js')))),
            'accommodation' => $this->accommodation->displayList()
            //'revalidate' => $this->revalidate_eticket->displayList(array("booking_source_id"=>3,"booking_status_id" => 6))
            
        );
        
        parent::displayTemplate(admin_dir('third_party_outlet/revalidate_eticket/revalidate_eticket'),$data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $void_eticket = new Revalidate_Eticket_Model($this->id);   
        // Check if a record exists
        $void_eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'revalidate_eticket'        => $void_eticket,
        );
        /*
        ticket status
        1 = issued
        2 = void
        3 = refund
        4 = upgrade
        */
        // if (isset($_POST['submit'])) {
           
        //  }
        
        parent::displayTemplate(admin_dir('third_party_outlet/revalidate_eticket/form/revalidate_eticket'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $accommodation = new Revalidate_Eticket_Model($this->id);
        
        // Check if a record exists
        $accommodation->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($accommodation->toggleStatus())
        {   
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating accommodation.');
            $this->session->set_flashdata('id', $accommodation->id);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating accommodation.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();
                            
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.eticket-passenger.js')))),
            'trip_type' => $this->trip_type->displayList(),
            'accommodation' => $this->accommodation->displayList(),
            'voyage' => $this->voyage->displayList()
        );
        

        parent::displayTemplate(admin_dir('third_party_outlet/revalidate_eticket/form/add/revalidate_eticket'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
    
        // // get user
         $revalidate_eticket = new Revalidate_Eticket_Model($this->id);
         $accommodation_type = new Accommodation_Model();
        // Check if a record exists
        $revalidate_eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'eticket'        => $revalidate_eticket->get_eticket_details($this->id),
            'accommodation' => $accommodation_type->displayList()
        );
        // echo '<pre>';
        // print_r($accommodation_type->displayList());
        parent::displayTemplate(admin_dir('third_party_outlet/revalidate_eticket/form/edit/revalidate_eticket'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('departure_date', 'Re-schedule Date', 'required|trim');
        $this->form_validation->set_rules('accommodation_id', 'Upgrade Accommodation To', 'required|trim');
        // $this->form_validation->set_rules('eticket_terms', 'Terms and Conditions', 'required|trim');
        // $this->form_validation->set_rules('eticket_reminders', 'Reminders', 'required|trim');

        // if ($this->uri->rsegment(2) == 'add')
        //     self::_addInfo();
            
        // if ($this->uri->rsegment(2) == 'edit')
        //     self::_editInfo();
        if ($this->uri->rsegment(1) == 'revalidate_eticket')
            self::_editInfo();
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Outlet
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $eticket_passenger = new Revalidate_Eticket_Model();
            
            parent::copyFromPost($eticket_passenger, 'id_ticket');
                        
            // Check for successful insert
            if ($eticket_passenger->add())
            {
                parent::logThis($accommodation->id, 'Successfully added Accommodation');
                
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added Accommodation');
                $this->session->set_flashdata('id', $outlet->id);   

                // passenger info
                $passenger_data = array(
                    'firstname' => $this->tools->getPost('firstname'),
                    'middlename' => $this->tools->getPost('lastname'),
                    'lastname' => $this->tools->getPost('lastname'),
                    'birthdate' => date("Y-m-d",strtotime($this->tools->getPost('birthdate'))),
                    'age' => $this->tools->getPost('age'),
                    'gender' => $this->tools->getPost('gender'),
                    'contactno' => $this->tools->getPost('contactno'),
                    'id_no' => $this->tools->getPost('id_no'),
                    'nationality' => $this->tools->getPost('nationality'),
                    'with_infant' => $this->tools->getPost('with_infant'),
                    'infant_name' => $this->tools->getPost('infant_name')
                    );
                // insert passenger info to passenger table
                $this->db->insert('passenger',$passenger_data);
                //
                $fare_data = array();
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving Accommodation');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $update_data = array();
        $insert_data = array();

        $booking = new Booking_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE) { 
            parent::copyFromPost($ebooking, 'id_booking'); 
            // parent::copyFromPost($void_reason, 'void_reason');    
            $update_data = array(
                    "booking_status_id"=>4,
                    );
            // is the date today less than or equal to departure date?
            // Check for successful update
            if ($booking->update(array(),$update_data))
            {                                   
                parent::logThis($booking->id, 'Successfully updated Eticket');
                $insert_data = array(
                    );                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Eticket');
                $this->session->set_flashdata('id', $booking->id);
            } else {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Eticket');
            }
            // print_r($this->db->last_query());exit;
            redirect(admin_url($this->classname));
        }
    }
   
    public function get_revalidate_eticket() {
        $data_revalidate = $this->revalidate_eticket->get_eticket_list($this->tools->getGet());
        echo json_encode($data_revalidate);
    }

   
   
}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */