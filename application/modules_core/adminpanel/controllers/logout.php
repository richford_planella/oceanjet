<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator logout Class
|--------------------------------------------------------------------------
|
| Handles the header part of the logout pages
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Logout extends MY_Controller
{
	function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
	}
	
	public function index()
	{
		// Log actions
		$this->logThis('administrator_log');
		
		$this->session->unset_userdata('admin');
		
		$this->session->set_flashdata('confirm', 'Logout successful!');
		redirect(admin_url('login'));
	}
}

/* End of file logout.php */
/* Location: ./application/modules_core/adminpanel/logout/logout.php */
