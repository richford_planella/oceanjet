<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| E-Voucher Class
|--------------------------------------------------------------------------
|
| Modify permissions of evoucher
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Evoucher extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
                
                parent::__construct();
		
                $this->load->model(admin_dir('evoucher/evoucher_model'));
                $this->load->model(admin_dir('evoucher/evoucher_series_model'));
                $this->load->model(admin_dir('port/port_model'));
                $this->load->model(admin_dir('outlet/outlet_model'));
                $this->load->model(admin_dir('accommodation/accommodation_model'));
                
                $this->evoucher = new Evoucher_Model();
                $this->evoucher_series = new Evoucher_Series_Model();
                $this->port = new Port_Model();
                $this->outlet = new Outlet_Model();
                $this->accommodation = new Accommodation_Model();
                                
                // vessel ID
		$this->id = $this->uri->rsegment(3);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display evoucher list
	 *
	 * @access		public
	 * @return		void
	 */
        public function index()
        {                
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'E-voucher')),
                                'footer'        => parent::getTemplate(admin_dir('footer')),
                                'evoucher'        => $this->evoucher->displayList(),
                );
                
                parent::displayTemplate(admin_dir('evoucher/evoucher'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * View e-voucher info
	 *
	 * @access		public
	 * @return		void
	 */
        public function view()
        {
                // get evoucher
                $evoucher = new Evoucher_Model($this->id);
                
                // Check if a record exists
		$evoucher->redirectIfEmpty(admin_url($this->classname));
                                
               if($this->tools->getPost("check"))
                {
                    // do cancel function here
                    self::_cancelBatch();
                }
                                
                // set select amount
                $set_amount = $evoucher->evoucher_type;
                
                if($this->tools->getPost('evoucher_type'))
                    $set_amount = $this->tools->getPost('evoucher_type');
                
                // get data
                $data = array(
                                'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'View E-Voucher')),
                                'footer'            => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.evoucher-tools.js')))),
                                'evoucher'          => $evoucher,
                                'port'              => $this->port->displayList(array('enabled' => 1)),
                                'accommodation'     => $this->accommodation->displayList(array('enabled' => 1)),
                                'set_amount'        => $set_amount,
                                'outlet'            => $this->outlet->displayList(array('u.enabled' => 1)),
                                'evoucher_series'   => $this->evoucher_series->displayList(array('evoucher_id' => $evoucher->id)),
                );
                
                parent::displayTemplate(admin_dir('evoucher/form/evoucher'),$data);
        }    
        
        // --------------------------------------------------------------------
	
	/*
	 * Edit vessel
	 *
	 * @access		public
	 * @return		void
	 */
        public function generate()
        {
                // get evoucher
                $evoucher = new Evoucher_Model($this->id);
                
                // Check if a record exists
		$evoucher->redirectIfEmpty(admin_url($this->classname));
                                
                // Form validation
		self::_validateSeries();
                                
                // set select amount
                $set_amount = $evoucher->evoucher_type;
                
                if($this->tools->getPost('evoucher_type'))
                    $set_amount = $this->tools->getPost('evoucher_type');
                
                // get data
                $data = array(
                                'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Generate E-voucher')),
                                'footer'            => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.evoucher-tools.js')))),
                                'evoucher'          => $evoucher,
                                'port'              => $this->port->displayList(array('enabled' => 1)),
                                'accommodation'     => $this->accommodation->displayList(array('enabled' => 1)),
                                'set_amount'        => $set_amount,
                                'evoucher_series'   => $this->evoucher_series->displayList(array('evoucher_id' => $evoucher->id)),
                                'outlet'            => $this->outlet->displayList(array('u.enabled' => 1)),
                );
                
                parent::displayTemplate(admin_dir('evoucher/form/generate'),$data);
                
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Add evoucher
	 *
	 * @access		public
	 * @return		void
	 */
        public function add()
        {
                // Form validation
		self::_validate();
                
                // set select amount
                $set_amount = 1;
                
                if($this->tools->getPost('evoucher_type'))
                    $set_amount = $this->tools->getPost('evoucher_type');
                
                // get data
                $data = array(
                                'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Create E-voucher')),
                                'footer'            => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.evoucher-tools.js')))),
                                'port'              => $this->port->displayList(array('enabled' => 1)),
                                'accommodation'     => $this->accommodation->displayList(array('enabled' => 1)),
                                'set_amount'        => $set_amount,
                                'outlet'            => $this->outlet->displayList(array('u.enabled' => 1)),
                );
                
                parent::displayTemplate(admin_dir('evoucher/form/add/evoucher'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Edit vessel
	 *
	 * @access		public
	 * @return		void
	 */
        public function edit()
        {
                // get evoucher
                $evoucher = new Evoucher_Model($this->id);
                
                // Check if a record exists
		$evoucher->redirectIfEmpty(admin_url($this->classname));
                                
                // Check if all series unused
                if($this->evoucher_series->displayList(array('evoucher_id' => $evoucher->id,'estatus' => 'Used'),array(),TRUE) > 0)
                {
                    // Set confirmation message
                    $this->session->set_flashdata('error', 'You cannot modify this e-voucher set.');
                    redirect(admin_url($this->classname));
                }
                
                // Form validation
		self::_validate();
                                
                // set select amount
                $set_amount = $evoucher->evoucher_type;
                
                if($this->tools->getPost('evoucher_type'))
                    $set_amount = $this->tools->getPost('evoucher_type');
                
                // get data
                $data = array(
                                'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit E-Voucher')),
                                'footer'            => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.evoucher-tools.js')))),
                                'evoucher'          => $evoucher,
                                'port'              => $this->port->displayList(array('enabled' => 1)),
                                'accommodation'     => $this->accommodation->displayList(array('enabled' => 1)),
                                'set_amount'        => $set_amount,
                                'outlet'            => $this->outlet->displayList(array('u.enabled' => 1)),
                );
                
                parent::displayTemplate(admin_dir('evoucher/form/edit/evoucher'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the generate form
	 *
	 * @access		private
	 * @return		void
	 */
        private function _validateSeries()
	{
                $this->form_validation->set_rules('gen_quantity', 'Generate Series', 'required|integer');
             
		if ($this->uri->rsegment(2) == 'generate')
                        self::_addSeriesInfo();
	}
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add e-voucher series
	 *
	 * @access		private
	 * @return		void
	 */
	private function _addSeriesInfo()
	{
                // Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                        // get evoucher
                        $evoucher = new Evoucher_Model($this->id);
                            
                        // check if generate series greater than total quantity
                        $total_available = ($evoucher->quantity  - $evoucher->alloted_evoucher) - (($evoucher->generated_evoucher  - $evoucher->alloted_evoucher) - $evoucher->cancelled_evoucher);
                        
                        $generate_voucher = $evoucher->generated_evoucher + $this->tools->getPost('gen_quantity');
                        
                        if($this->tools->getPost('gen_quantity') > $total_available)
                        {
                            // Set confirmation message
                            $this->session->set_flashdata('error', 'You can only generate less than or equal to '.$total_available.' voucher');
                            redirect(admin_url($this->classname,'generate',  $this->id));
                        }
                        
                        // loop generate series value
                        for($i=1; $i <= $this->tools->getPost('gen_quantity');$i++)
                        {
                            $evoucher_series = new Evoucher_Series_Model();

                            $this->tools->setPost('evoucher_id', $evoucher->id);
                            $this->tools->setPost('evoucher_code', $this->misc->generate_code(7));
                            $this->tools->setPost('amount', $evoucher->evoucher_amount);
                            $this->tools->setPost('estatus', 'Unused');
                            $this->tools->setPost('date_generated', date("Y-m-d"));
                            $this->tools->setPost('expiration_date', date("Y-m-d",strtotime("+".$evoucher->validity." day", strtotime(date("Y-m-d")))));
                            $this->tools->setPost('date_used', '0000-00-00');

                            parent::copyFromPost($evoucher_series, 'id_evoucher_series');

                            // insert 
                            $evoucher_series->add();
                            
                            parent::logThis($evoucher_series->id,'Successfully added e-voucher series.');
                                
                            // Set confirmation message
                            $this->session->set_flashdata('confirm', 'Successfully generate e-voucher series.');
                        }
                        
                        // update evoucher
                        $evoucher->update(array(),array('generated_evoucher' => $generate_voucher));
                           
                        redirect(admin_url($this->classname,'view',  $this->id));
                }
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{
                $this->form_validation->set_rules('origin_id', 'origin', 'required|integer');
		$this->form_validation->set_rules('destination_id', 'destination', 'required|integer');
                $this->form_validation->set_rules('accommodation_id', 'accommodation', 'required|integer');
                $this->form_validation->set_rules('quantity', 'quantity', 'required|integer');
                $this->form_validation->set_rules('evoucher_type', 'override rate', 'required|integer');
                $this->form_validation->set_rules('evoucher_amount', 'amount/percent', 'required|decimal');
                $this->form_validation->set_rules('outlet_id', 'issuing outlet', 'required|integer');
                $this->form_validation->set_rules('validity', 'validity', 'required|integer');
             
		if ($this->uri->rsegment(2) == 'add')
                        self::_addInfo();
			
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add e-voucher
	 *
	 * @access		private
	 * @return		void
	 */
	private function _addInfo()
	{
                // validate for unique value
                $this->form_validation->set_rules('evoucher', 'e-voucher type', 'required|trim|max_length[128]|is_unique[evoucher.evoucher]');
                
                // Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                        $evoucher = new Evoucher_Model();
                        
                        $this->tools->setPost('status', 'New');
                        $this->tools->setPost('alloted_evoucher', 0);
                        $this->tools->setPost('generated_evoucher', 0);
                        $this->tools->setPost('cancelled_evoucher', 0);
                        
                        parent::copyFromPost($evoucher, 'id_evoucher');
                        
                        // Check for successful insert
			if ($evoucher->add())
			{
                                
                                parent::logThis($evoucher->id,'Successfully added e-voucher.');
                                
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added e-voucher.');
				$this->session->set_flashdata('id', $evoucher->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving e-voucher');
			}
                        redirect(admin_url($this->classname));
                }
        }
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit e-voucher
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
                $evoucher = new Evoucher_Model($this->id);
                
                // check if evoucher code doesn't change
                if($this->evoucher->getValue('id_evoucher', array('evoucher' => $this->tools->getPost('evoucher'))) == $this->id)
                        $this->form_validation->set_rules('evoucher', 'e-voucher type', 'required|trim|max_length[128]');
                else
                        $this->form_validation->set_rules('evoucher', 'e-voucher type', 'required|trim|max_length[128]|is_unique[evoucher.evoucher]');
                
                // Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                        $evoucher = new Evoucher_Model($this->id);
                        
                        parent::copyFromPost($evoucher, 'id_evoucher');
			
			// Check for successful update
			if ($evoucher->update())
			{	
                                $evoucher_series = new Evoucher_Series_Model();
                                
                                // update all series
                                $this->evoucher_series->update(array('evoucher_id' => $this->id), array('amount' => $this->tools->getPost('evoucher_amount')));
                                                                
                                parent::logThis($evoucher->id,'E-voucher successfully updated.');
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successful in updating e-voucher.');
				$this->session->set_flashdata('id', $evoucher->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating e-voucher');
			}
			
			redirect(admin_url($this->classname));
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Cancel selected series 
	 *
	 * @access		public
	 * @return		void
	 */
        public function cancel()
        {
            $evoucher = new Evoucher_Model($this->id);
            
            // Check if a record exists
            $evoucher->redirectIfEmpty(admin_url($this->classname));
                
            $evoucher_series = new Evoucher_Series_Model($this->uri->rsegment(4));
            
            // Check if a record exists
            $evoucher_series->redirectIfEmpty(admin_url($this->classname));
            
            // update 
            $evoucher_series->update(array(),array('estatus' => 'Cancelled'));
                    
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'E-voucher series cancelled.');
                
            // update evoucher
            $evoucher->update(array(),array('cancelled_evoucher' => 1 + $evoucher->cancelled_evoucher));
            redirect(admin_url($this->classname,'view',  $this->id));
                
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Cancel all selected 
	 *
	 * @access		public
	 * @return		void
	 */
        private function _cancelBatch()
        {
            $evoucher = new Evoucher_Model($this->id);
            
            $count = 0;
            // loop MyCheckBox $_POST
            foreach($this->tools->getPost("check") as $key => $value)
            {
                if($value > 0)
                {
                    $count++;
                    $evoucher_series = new Evoucher_Series_Model($key);
                    
                    // update 
                    $evoucher_series->update(array(),array('estatus' => 'Cancelled'));
                }
            }
            
            // check if need to redirect
            if($count > 0)
            {
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'E-voucher series cancelled.');
                // update evoucher
                $evoucher->update(array(),array('cancelled_evoucher' => $count + $evoucher->cancelled_evoucher));
                redirect(admin_url($this->classname,'view',  $this->id));
            }
        }
}

/* End of file evoucher.php */
/* Location: ./application/modules_core/adminpanel/controllers/evoucher/evoucher.php */