<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Profile Class
|--------------------------------------------------------------------------
|
| Modify permissions of administrator profile
|
| @category		Controller
| @author		Baladeva Juganas
*/
class User_Profile extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
                $this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('user/user_profile_level_model'));
                $this->load->model(admin_dir('user/user_level_model'));
                $this->load->model(admin_dir('administrator_access/administrator_access_model'));
                $this->load->model(admin_dir('administrator_page/administrator_page_model'));
                
                $this->user_profile = new Profile_Model();
                $this->user_level = new User_Level_Model();
                $this->user_profile_level = new User_Profile_Level_Model();
                $this->admin_access = new Administrator_Access_Model();
                $this->admin_page = new Administrator_Page_Model();
                
                // administrator ID
		$this->id = $this->uri->rsegment(3);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display user profile list
	 *
	 * @access		public
	 * @return		void
	 */
        public function index()
        {               
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'User Matrix')),
                                'footer'        => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.user-profile-tools.js')))),
                                'user_profile'  => $this->user_profile->displayList(array(),array('id_user_profile' => 'ASC')),
                );
                
                parent::displayTemplate(admin_dir('user/profile/profile'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * View user profile info
	 *
	 * @access		public
	 * @return		void
	 */
        public function view()
        {
                // get user profile
                $user_profile = new Profile_Model($this->id);
                
                // Check if a record exists
		$user_profile->redirectIfEmpty(admin_url($this->classname));
                
                // Get all main page
                $pages = $this->admin_page->displayList(array('parent_administrator_page_id' => 0));
                
                foreach($pages as $p)
                    $p->subpage = $this->admin_page->displayList(array('parent_administrator_page_id' => $p->id_administrator_page));
                
                $admin_access = $this->admin_access->getPermissions(array('user_profile_id' => $user_profile->id));
                
                // Get user profile access
                foreach($pages as $p)
                    foreach($admin_access as $ac)
                    {
                        if($p->id_administrator_page == $ac->administrator_page_id)
                        {
                            $p->access = $ac->access;
                            $p->view = $ac->view;
                            $p->add = $ac->add;
                            $p->edit = $ac->edit;
                            $p->delete = $ac->delete;
                            $p->export = $ac->export;
                            $p->print = $ac->print;
                        }
                        
                        // assign modules to sub pages
                        foreach($p->subpage as $sp)
                        {
                            if($sp->id_administrator_page == $ac->administrator_page_id)
                            {
                                $sp->access = $ac->access;
                                $sp->view = $ac->view;
                                $sp->add = $ac->add;
                                $sp->edit = $ac->edit;
                                $sp->delete = $ac->delete;
                                $sp->export = $ac->export;
                                $sp->print = $ac->print;
                            }
                        }
                    }
                
                // Get user level    
                $level = $this->user_level->displayList();
                    
                // Get user level
                $user_profile_level = $this->user_profile_level->displayList(array('user_profile_id' => $user_profile->id));
                
                // Loop user level
                foreach($level as $l)
                    foreach($user_profile_level as $upl)
                        if($l->id_user_level == $upl->user_level_id)
                            $l->checked = 1;
                    
                // get data
                $data = array(
                                'header'                => Modules::run(admin_dir('header/call_header'),array('title' => 'View User Matrix')),
                                'footer'                => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.user-profile-action-tools.js')))),
                                'user_profile'          => $user_profile,
                                'level'                 => $level,
                                'pages'                 => $pages,
                                'admin_access'          => $admin_access,
                                'user_profile_level'    => $user_profile_level,
                                'sub_page' => $this->admin_page->displayList(array('parent_administrator_page_id !=' => 0)),
                );
                
                parent::displayTemplate(admin_dir('user/profile/form/profile'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Delete user profile status
	 *
	 * @access		public
	 * @return		void
	 */
        public function delete()
        {
                // get user profile
                $user_profile = new Profile_Model($this->id);
                
                // Check if a record exists
		$user_profile->redirectIfEmpty(admin_url($this->classname));
                
                // Check for successful toggle
                if ($user_profile->countFromUser() > 0)
                {	
                        // Set confirmation message
                        $this->session->set_flashdata('note', 'Cannot delete '.$user_profile->user_profile.' was already used in the system..');
                }
                else
                {
                        // Delete user role access
                        $administrator_access = new Administrator_Access_Model();
                        
                        $administrator_access->delete(array('user_profile_id' => $this->id));
                        
                        // Delete user role level
                        $user_role_level = new User_Profile_Level_Model();
                        
                        $user_role_level->delete(array('user_profile_id' => $this->id));
                        
                        // Set confirmation message
                        if($user_profile->delete())
                            $this->session->set_flashdata('confirm', "Successfully deleted ".$user_profile->user_profile.'!');
                        else
                            $this->session->set_flashdata('note', "Error deleting user role!");
                }
                
                redirect(admin_url($this->classname));
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Toggle user profile status
	 *
	 * @access		public
	 * @return		void
	 */
        public function toggle()
        {
                // get user profile
                $user_profile = new Profile_Model($this->id);
                
                // Check if a record exists
		$user_profile->redirectIfEmpty(admin_url($this->classname));
                
                // Check for successful toggle
                if ($user_profile->toggleStatus())
                {	
                        // Set confirmation message
                        if($user_profile->enabled)
                            $this->session->set_flashdata('confirm', $user_profile->user_profile.' has been de-activated!');
                        else
                            $this->session->set_flashdata('confirm', $user_profile->user_profile.' has been re-activated!');
                        $this->session->set_flashdata('id', $user_profile->id);
                }
                else
                {
                        // Set confirmation message
                        $this->session->set_flashdata('note', 'Error in updating user role');
                }
                
                redirect(admin_url($this->classname));
        }
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Add user profile
	 *
	 * @access		public
	 * @return		void
	 */
        public function add()
        {
                // Form validation
		self::_validate();
                
                // display default page
                $default_page = 0;
                
                // Get all main page
                $pages = $this->admin_page->displayList(array('parent_administrator_page_id' => 0));
                
                foreach($pages as $p)
                    $p->subpage = $this->admin_page->displayList(array('parent_administrator_page_id' => $p->id_administrator_page));
                
                if($this->tools->getPost('has_menu'))
                    $default_page = 0;
                else
                    $default_page = 1;
                                
                // get data
                $data = array(
                                'header' => Modules::run(admin_dir('header/call_header'),array('title' => 'Create User Matrix')),
                                'footer' => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.user-profile-action-tools.js')))),
                                'level'  => $this->user_level->displayList(),
                                'pages'  => $pages,
                                'default_page' => $default_page,
                                'sub_page' => $this->admin_page->displayList(array('parent_administrator_page_id !=' => 0)),
                );
                
                parent::displayTemplate(admin_dir('user/profile/form/add/profile'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Edit user profile
	 *
	 * @access		public
	 * @return		void
	 */
        public function edit()
        {
                // get user profile
                $user_profile = new Profile_Model($this->id);
                
                // Check if a record exists
		$user_profile->redirectIfEmpty(admin_url($this->classname));
                
                // Get all main page
                $pages = $this->admin_page->displayList(array('parent_administrator_page_id' => 0));
                
                foreach($pages as $p)
                    $p->subpage = $this->admin_page->displayList(array('parent_administrator_page_id' => $p->id_administrator_page));
                
                $admin_access = $this->admin_access->getPermissions(array('user_profile_id' => $user_profile->id));
                
                // Get user profile access
                foreach($pages as $p)
                    foreach($admin_access as $ac)
                    {
                        if($p->id_administrator_page == $ac->administrator_page_id)
                        {
                            $p->access = $ac->access;
                            $p->view = $ac->view;
                            $p->add = $ac->add;
                            $p->edit = $ac->edit;
                            $p->delete = $ac->delete;
                            $p->export = $ac->export;
                            $p->print = $ac->print;
                        }
                        
                        // assign modules to sub pages
                        foreach($p->subpage as $sp)
                        {
                            if($sp->id_administrator_page == $ac->administrator_page_id)
                            {
                                $sp->access = $ac->access;
                                $sp->view = $ac->view;
                                $sp->add = $ac->add;
                                $sp->edit = $ac->edit;
                                $sp->delete = $ac->delete;
                                $sp->export = $ac->export;
                                $sp->print = $ac->print;
                            }
                        }
                    }
                
                // Get user level    
                $level = $this->user_level->displayList();
                    
                // Get user level
                $user_profile_level = $this->user_profile_level->displayList(array('user_profile_id' => $user_profile->id));
                
                // Loop user level
                foreach($level as $l)
                    foreach($user_profile_level as $upl)
                        if($l->id_user_level == $upl->user_level_id)
                            $l->checked = 1;
                
                // Form validation
		self::_validate();
                
                // get data
                $data = array(
                                'header'                => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit User Matrix')),
                                'footer'                => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.user-profile-action-tools.js')))),
                                'user_profile'          => $user_profile,
                                'level'                 => $level,
                                'pages'                 => $pages,
                                'admin_access'          => $admin_access,
                                'user_profile_level'    => $user_profile_level,
                                'sub_page' => $this->admin_page->displayList(array('parent_administrator_page_id !=' => 0)),
                );
                
                parent::displayTemplate(admin_dir('user/profile/form/edit/profile'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{
		$this->form_validation->set_rules('user_profile', 'user role', 'required|trim|max_length[128]');
		$this->form_validation->set_rules('user_level_id', 'user level', 'required|integer');
                $this->form_validation->set_rules('is_outlet', 'outlet enabled', 'required|integer');
                $this->form_validation->set_rules('has_mobile_access', 'mobile access', '');
                $this->form_validation->set_rules('session_timeout', 'session expiry', 'required|integer');
                $this->form_validation->set_rules('has_transaction', 'transaction option', '');
                $this->form_validation->set_rules('enabled', 'status', 'required');
                
                if($this->tools->getPost('user_profile_level'))
                    foreach($this->tools->getPost('user_profile_level') as $key => $value)
                        $this->form_validation->set_rules("user_profile_level[$key]", 'Accessible Levels', '');
                
                $this->form_validation->set_rules('has_menu', 'menu option', '');
                
                if($this->tools->getPost('has_menu'))
                    $this->form_validation->set_rules('administrator_page_id', 'landing page', '');
                else
                    $this->form_validation->set_rules('administrator_page_id', 'landing page', 'required');
                
                if($this->tools->getPost('page'))
                    foreach($this->tools->getPost('page') as $key => $value)
                    {
                        $this->form_validation->set_rules("page[$key][access]", 'Access Page', '');
                        $this->form_validation->set_rules("page[$key][view]", 'View Page', '');
                        $this->form_validation->set_rules("page[$key][add]", 'Add Page', '');
                        $this->form_validation->set_rules("page[$key][edit]", 'Edit Page', '');
                        $this->form_validation->set_rules("page[$key][delete]", 'Delete Page', '');
                        $this->form_validation->set_rules("page[$key][export]", 'Export Page', '');
                        $this->form_validation->set_rules("page[$key][print]", 'Print Page', '');
                    }
                        
                
		if ($this->uri->rsegment(2) == 'add')
                        self::_addInfo();
			
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add user profile
	 *
	 * @access		private
	 * @return		void
	 */
	private function _addInfo()
	{
                // Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                        $user_profile = new Profile_Model();
                        
                        // check for transaction option
                        if(!$this->tools->getPost('has_transaction'))
                            $this->tools->setPost('has_transaction',0);
                        
                        // check for mobile access
                        if(!$this->tools->getPost('has_mobile_access'))
                            $this->tools->setPost('has_mobile_access',0);

                        // check for menu option
                        if(!$this->tools->getPost('has_menu'))
                            $this->tools->setPost('has_menu',0);
                        
                        parent::copyFromPost($user_profile, 'id_user_profile');

                        // Check for successful insert
			if ($user_profile->add())
			{
                                // save user profile level
                                if($this->tools->getPost('user_profile_level'))
                                    foreach($this->tools->getPost('user_profile_level') as $key => $value)
                                    {
                                        $user_profile_level = new User_Profile_Level_Model();
                                        
                                        $user_profile_level->add(array(
                                                                        'user_profile_id'   => $user_profile->id,
                                                                        'user_level_id'     => $value
                                        ));
                                    }
                                    
                                // save access
                                if($this->tools->getPost('page'))
                                    foreach($this->tools->getPost('page') as $p)
                                    {
                                        $access = new Administrator_Access_Model();
                                        
                                        $data = array(
                                                        "user_profile_id"           => $user_profile->id,
                                                        "administrator_page_id"     => $p['page_id'],
                                                        "access"                    => (isset($p['access'])) ? 1 : 0,
                                                        "view"                      => (isset($p['view'])) ? 1 : 0,
                                                        "add"                       => (isset($p['add'])) ? 1 : 0,
                                                        "edit"                      => (isset($p['edit'])) ? 1 : 0,
                                                        "delete"                    => (isset($p['delete'])) ? 1 : 0,
                                                        "approve"                   => (isset($p['approve'])) ? 1 : 0,
                                                        "verify"                    => (isset($p['verify'])) ? 1 : 0,
                                                        "print"                     => (isset($p['print'])) ? 1 : 0,
                                                        "request"                   => (isset($p['request'])) ? 1 : 0,
                                                        "close"                     => (isset($p['close'])) ? 1 : 0,
                                                        "received"                  => (isset($p['received'])) ? 1 : 0,
                                                        "allocate"                  => (isset($p['allocate'])) ? 1 : 0,
                                                        "activate"                  => (isset($p['activate'])) ? 1 : 0,
                                                        "reactivate"                => (isset($p['reactivate'])) ? 1 : 0,
                                                        "export"                    => (isset($p['export'])) ? 1 : 0,
                                                    );
                                        
                                        $access->add($data);
                                    }
                                    
                                parent::logThis($user_profile->id,'Successfully added user role!');
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added user role!');
				$this->session->set_flashdata('id', $user_profile->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving user profile');
			}
                        redirect(admin_url($this->classname));
                }
        }
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit user profile
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
		$user_profile = new Profile_Model($this->id);
                
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{       
                        // check for transaction option
                        if(!$this->tools->getPost('has_transaction'))
                            $this->tools->setPost('has_transaction',0);
                        
                        // check for mobile access
                        if(!$this->tools->getPost('has_mobile_access'))
                            $this->tools->setPost('has_mobile_access',0);
                        
                        // check for menu option
                        if(!$this->tools->getPost('has_menu'))
                            $this->tools->setPost('has_menu',0);
                        
                        if($this->tools->getPost('has_menu'))
                            $this->tools->setPost('administrator_page_id', 0);
                                                
			parent::copyFromPost($user_profile, 'id_user_profile');
			
			// Check for successful update
			if ($user_profile->update())
			{	                        
                                // delete all access
                                $this->user_profile_level->delete(array('user_profile_id' => $user_profile->id));
                                
                                // save user profile level
                                if($this->tools->getPost('user_profile_level'))
                                    foreach($this->tools->getPost('user_profile_level') as $key => $value)
                                    {
                                        $user_profile_level = new User_Profile_Level_Model();
                                        
                                        $user_profile_level->add(array(
                                                                        'user_profile_id'   => $user_profile->id,
                                                                        'user_level_id'     => $value
                                        ));
                                    }
                                    
                                // delete all access
                                $this->admin_access->delete(array('user_profile_id' => $user_profile->id));
                                
                                // save access
                                if($this->tools->getPost('page'))
                                    foreach($this->tools->getPost('page') as $p)
                                    {
                                        $access = new Administrator_Access_Model();
                                        
                                        $data = array(
                                                        "user_profile_id"           => $user_profile->id,
                                                        "administrator_page_id"     => $p['page_id'],
                                                        "access"                    => (isset($p['access'])) ? 1 : 0,
                                                        "view"                      => (isset($p['view'])) ? 1 : 0,
                                                        "add"                       => (isset($p['add'])) ? 1 : 0,
                                                        "edit"                      => (isset($p['edit'])) ? 1 : 0,
                                                        "delete"                    => (isset($p['delete'])) ? 1 : 0,
                                                        "approve"                   => (isset($p['approve'])) ? 1 : 0,
                                                        "verify"                    => (isset($p['verify'])) ? 1 : 0,
                                                        "print"                     => (isset($p['print'])) ? 1 : 0,
                                                        "request"                   => (isset($p['request'])) ? 1 : 0,
                                                        "close"                     => (isset($p['close'])) ? 1 : 0,
                                                        "received"                  => (isset($p['received'])) ? 1 : 0,
                                                        "allocate"                  => (isset($p['allocate'])) ? 1 : 0,
                                                        "activate"                  => (isset($p['activate'])) ? 1 : 0,
                                                        "reactivate"                => (isset($p['reactivate'])) ? 1 : 0,
                                                        "export"                    => (isset($p['export'])) ? 1 : 0,
                                                    );
                                        
                                        $access->add($data);
                                    }
                                    
                                parent::logThis($user_profile->id,'Successfully updated user role!');
                                
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated user role!');
				$this->session->set_flashdata('id', $user_profile->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating user role');
			}
			
			redirect(admin_url($this->classname));
		}
	}
        
}

/* End of file profile.php */
/* Location: ./application/modules_core/adminpanel/controllers/user/profile.php */