<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Header Class
|--------------------------------------------------------------------------
|
| Handles the header part of the admin pages
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Header extends Admin_Core
{
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
                $this->classname = strtolower(get_class());
		parent::__construct();
                
                $this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('administrator_access/administrator_access_model'));
                $this->load->model(admin_dir('administrator_page/administrator_page_model'));
                
                $this->user_profile = new Profile_Model();
                $this->admin_access = new Administrator_Access_Model();
                $this->admin_page = new Administrator_Page_Model();
	}

	// --------------------------------------------------------------------
	
	/*
	 * Display admin header
	 *
	 * @access		public
	 * @param		array
	 * @return		string
	 */
	public function call_header($header_data = array())
	{
                // Get user profile
                $user_profile = new Profile_Model($this->session->get('admin','administrator_profile_id'));
                
                // admin page classname base on segment
		$classname = $this->uri->rsegment(1);
                
                // Get the current page id
		$page_id = $this->admin_page->getPageID($classname);
                
                // Get the parent page id
		$parent_page_id = $this->admin_page->getParentPageID($classname);
                
                // Get parent page details
                $parent_page = new Administrator_Page_Model($parent_page_id);
                
                // Get classname of the first sub page
                $parent_page->classname = $this->admin_page->getSubClass($parent_page->id_administrator_page, $user_profile->id); 
                
                // Get current page details
                $current_page = new Administrator_Page_Model($page_id);
                
                // Get all main page
                $mainPages = $this->admin_page->getHeaderTabs($user_profile->id);
                
                // Get classname of the first sub page
                foreach($mainPages as $mp)
                    $mp->classname = $this->admin_page->getSubClass($mp->id_administrator_page, $user_profile->id); 
                
                // Get all sub page
                $subPage = $this->admin_page->getSubPage($parent_page_id, $user_profile->id);
                
                $data = array(
                                'pages'             => $mainPages,
                                'classname'         => $classname,
                                'parent_page_id'    => $parent_page_id,
                                'parent_page'       => $parent_page,
                                'current_page'      => $current_page,
                                'sub_page'          => $subPage,
                                'title'             => $header_data['title'],
                );
                
                return parent::getTemplate(admin_dir('header'), array_merge($header_data, $data));
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Display admin header
	 *
	 * @access		public
	 * @param		array
	 * @return		string
	 */
	public function call_dashboard_header($header_data = array())
	{
                // admin page classname base on segment
		$classname = $this->uri->rsegment(1);
                
                // Get the parent page id
		$parent_page_id = $this->admin_page->getParentPageID($classname);
                
                // Get user profile
                $user_profile = new Profile_Model($this->session->get('admin','administrator_profile_id'));
                
                // Get all main page
                $mainPages = $this->admin_page->getHeaderTabs($user_profile->id);
                
                // Get classname of the first sub page
                foreach($mainPages as $mp)
                    $mp->classname = $this->admin_page->getSubClass($mp->id_administrator_page, $user_profile->id);
                
                $data = array(
                                'pages'             => $mainPages,
                                'classname'         => $classname,
                                'parent_page_id'    => $parent_page_id,
                                'title'             => $header_data['title'],
                );
                
                return parent::getTemplate(admin_dir('adminpanel/header'), array_merge($header_data, $data));
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Display profile header
	 *
	 * @access		public
	 * @param		array
	 * @return		string
	 */
	public function call_profile_header($header_data = array())
	{
                // admin page classname base on segment
		$classname = $header_data['index'];
                
                // Get the parent page id
		$parent_page_id = $this->admin_page->getParentPageID($classname);
                
                // Get user profile
                $user_profile = new Profile_Model($this->session->get('admin','administrator_profile_id'));
                
                // Get all main page
                $mainPages = $this->admin_page->getHeaderTabs($user_profile->id);
                
                // Get classname of the first sub page
                foreach($mainPages as $mp)
                    $mp->classname = $this->admin_page->getSubClass($mp->id_administrator_page, $user_profile->id);
                
                $data = array(
                                'pages'             => $mainPages,
                                'classname'         => $classname,
                                'parent_page_id'    => $parent_page_id,
                                'title'             => $header_data['title'],
                );
                
                return parent::getTemplate(admin_dir('profile/header'), array_merge($header_data, $data));
        }
}

/* End of file header.php */
/* Location: ./application/modules_core/adminpanel/header/header.php */
