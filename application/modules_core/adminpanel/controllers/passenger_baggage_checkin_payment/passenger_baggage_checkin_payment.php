<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Passengers Baggage Check Payment In Class
|--------------------------------------------------------------------------
|
| Passengers Baggage Check In Content Management
|
| @category	Controller
| @author		Gian Asuncion
*/

class Passenger_Baggage_Checkin_Payment extends Admin_Core
{

    // ------------------------------------------------------------------------

    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());

        parent::__construct();

        // Passenger Baggage Check In Model
        $this->load->model(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin_model'));
        $this->passenger_baggage_checkin = new Passenger_Baggage_Checkin_Model();

        // Rolling Cargo Check In Model
        $this->load->model(admin_dir('rolling_cargo/rolling_cargo_model'));
        $this->roro = new Rolling_Cargo_Model();

        // Rolling Cargo Check In Model
        $this->load->model(admin_dir('rolling_cargo_checkin/rolling_cargo_checkin_model'));
        $this->rolling_cargo = new Rolling_Cargo_Checkin_Model();

        // Voyage
        $this->load->model(admin_dir('voyage/voyage_model'));
        $this->voyage = new Voyage_Model();

        // Voyage Management
        $this->load->model(admin_dir('voyage_management/voyage_management_model'));
        $this->voyage_management = new Voyage_Management_Model();

        // SuB Voyage Management
        $this->load->model(admin_dir('subvoyage_management/subvoyage_management_model'));
        $this->subvoyage_management = new Subvoyage_Management_Model();

        // Transaction
        $this->load->model(admin_dir('transaction/transaction_model'));
        $this->transaction = new Transaction_Model();

        // Voyage Management Status
        $this->load->model(admin_dir('voyage_management_status/voyage_management_status_model'));
        $this->voyage_management_status = new Voyage_Management_Status_Model();

        //Passenger check-in Model
        $this->load->model(admin_dir('passenger_checkin/Passenger_Checkin_Model'));
        $this->passenger_checkin = new Passenger_Checkin_Model();

        //SubVoyage
        $this->load->model(admin_dir('subvoyage/subvoyage_model'));
        $this->subvoyage = new Subvoyage_Model();

        //Attended baggage
        $this->load->model(admin_dir('attended_baggage/Attended_Baggage_Model'));
        $this->attended_baggage = new Attended_Baggage_Model();

        //Port
        $this->load->model(admin_dir('port/Port_Model'));
        $this->port = new Port_Model();

        // Rolling Cargo Check In id
        $this->id = $this->uri->rsegment(3);
    }

    public function index()
    {
        //Get sub voyage lists
        //Get Attended baggage
        $subvoyage = array();
        $baggage_rates = array();
        if ($this->tools->getPost('origin_id') AND $this->tools->getPost('destination_id')) {
            $subvoyage = $this->passenger_checkin->getSubVoyages($this->tools->getPost("origin_id"),
                $this->tools->getPost("destination_id"));

            $baggage_rates = $this->attended_baggage->displayList(array('u.origin_id' => $this->tools->getPost("origin_id"),
                'u.destination_id' => $this->tools->getPost("destination_id")));
        }


        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'Passenger Baggage Check-In Payment')),
            'footer' => parent::getTemplate(admin_dir('footer')),
            'voyage' => $this->voyage->displaylist(),
            'post_data' => $this->tools->getPost(),
            'subvoyage' => $subvoyage,
            'baggage_rates' => $baggage_rates,
            'ports' => $this->port->displayList(array('u.enabled' => 1), array('u.id_port' => 'ASC'))
        );

        parent::displayTemplate(admin_dir('passenger_baggage_checkin_payment/passenger_baggage_checkin_payment/form/add/passenger_baggage_checkin_payment'), $data);
    }

    // --------------------------------------------------------------------
    /*
     * Display Passenger Baggage Check In Filter List
     *
     * @access	public
     * @return		void
     */
    public function filter()
    {
        // Get all rolling cargo
        $departure_date = $this->uri->rsegment(3);
        $voyage_id = $this->uri->rsegment(4);
        //List of baggages under particular voyage
        $passenger_baggage_checkin = $this->passenger_baggage_checkin->displayList(array('vm.departure_date' => $departure_date, 'voyage_management_id' => $voyage_id));

        // Initialize data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header')),
            'footer' => parent::getTemplate(admin_dir('footer')),
            'voyage_details' => $this->voyage_management->displayList(array('u.departure_date' => $departure_date, 'u.id_voyage_management' => $voyage_id)),
            'passenger_baggage_checkins' => $passenger_baggage_checkin,
            'departure_date' => $departure_date,
            'voyage_id' => $voyage_id
        );

        parent::displayTemplate(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin/passenger_baggage_checkin'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * View Rolling Cargo Check In
     *
     * @access	public
     * @return		void
     */
    public function view()
    {
        // Get baggage lists based on sub voyage id
        $subvoyage_management_id = $this->uri->rsegment(3);
        $baggages = $this->passenger_baggage_checkin->displayList(array('u.subvoyage_management_id'=>$subvoyage_management_id));

        //Get Voyage Details
        $subvoyage = $this->subvoyage_management->displayList(array("u.id_subvoyage_management"=> $subvoyage_management_id));

        // Initialize data
        $data = array(
            'header'	    => Modules::run(admin_dir('header/call_header'),array('title'=>'View Baggage List')),
            'footer'		=> parent::getTemplate(admin_dir('footer')),
            'baggages'	    => $baggages,
            'subvoyage_details'     => $subvoyage
        );

        parent::displayTemplate(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin/passenger_baggage_checkin'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Toggle user status
     *
     * @access	public
     * @return		void
     */
    public function toggle()
    {
        // Get rolling cargo checkin
        $rolling_cargo_checkin = new Rolling_Cargo_Checkin_Model($this->id);

        // Check if a record exists
        $rolling_cargo_checkin->redirectIfEmpty(admin_url($this->classname));

        // Check for successful toggle
        if ($rolling_cargo_checkin->toggleStatus()) {
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating Rolling Cargo Check In');
            $this->session->set_flashdata('id', $rolling_cargo_checkin->id);
        } else {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating Rolling Cargo Check In');
        }

        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------

    /*
     * Add Passenger Baggage Check In
     *
     * @access	public
     * @return		void
     */
    public function add()
    {

    }

    // --------------------------------------------------------------------

    /*
     * Edit Rolling Cargo Check In
     *
     * @access	public
     * @return		void
     */
    public function edit()
    {
        // Get Passenger Baggage checkin info
        $passenger_baggage_checkin = $this->passenger_baggage_checkin->displayList(array('u.id_passenger_baggage_checkin' => $this->id))[0];

        self::_validate();

        // get data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header')),
            'footer' => parent::getTemplate(admin_dir('footer')),
            'passenger_baggage_checkin' => $passenger_baggage_checkin,
            'voyage_management_id' => $passenger_baggage_checkin->voyage_management_id,
            'voyage_management' => $this->voyage_management->displaylist(array('u.departure_date' => $passenger_baggage_checkin->departure_date)), //List of voyage based on departure date
            'voyage_details' => $this->voyage_management->displaylist(array('u.id_voyage_management' => $passenger_baggage_checkin->voyage_management_id))[0],
        );

        parent::displayTemplate(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin/form/edit/passenger_baggage_checkin'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Validate the form
     *
     * @access	private
     * @return		void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('origin_id', 'origin', 'required|trim');
        $this->form_validation->set_rules('destination_id', 'destination', 'required|trim');
        $this->form_validation->set_rules('voyage_text', 'voyage', 'required|trim');
        $this->form_validation->set_rules('voyage_management_id', 'voyage', 'required|trim');
        $this->form_validation->set_rules('subvoyage_status_text', 'leg status', 'required|trim');
        $this->form_validation->set_rules('subvoyage_status_id', 'leg status ID', 'required|trim');
        $this->form_validation->set_rules('origin', 'origin', 'required|trim');
        $this->form_validation->set_rules('destination', 'destination', 'required|trim');
        $this->form_validation->set_rules('etd', 'ETD', 'required|trim');
        $this->form_validation->set_rules('eta', 'ETA', 'required|trim');
        $this->form_validation->set_rules('vessel', 'vessel', 'required|trim');
        $this->form_validation->set_rules('id_passenger_baggage_checkin', 'Id passenger baggage checkin', 'required|trim');
        $this->form_validation->set_rules('transaction_ref', 'Transaction Reference no', 'required|trim');
        $this->form_validation->set_rules('reference_no', 'reference no', 'required|trim');
        $this->form_validation->set_rules('ticket_no', 'ticket no', 'required|trim');
        $this->form_validation->set_rules('passenger_name', 'passenger name', 'required|trim');
        $this->form_validation->set_rules('attended_baggage', 'attended baggage', 'required|trim');
        $this->form_validation->set_rules('weight', 'weight', 'required|trim|decimal');
        $this->form_validation->set_rules('quantity', 'quantity', 'required|trim|numeric');
        $this->form_validation->set_rules('uom', 'UOM', 'required|trim');
        $this->form_validation->set_rules('free_baggage', 'free baggage', 'required|trim');
        $this->form_validation->set_rules('attended_baggage', 'attended baggage', 'required|trim');
        $this->form_validation->set_rules('total_amount', 'total amount', 'required|trim');
        $this->form_validation->set_rules('amount_tendered', 'amount tendered', 'required|trim|decimal|greater_than[0]');
        $this->form_validation->set_rules('change', 'change', 'required|trim');

        self::_addInfo();
    }

    // --------------------------------------------------------------------

    /*
     * Validate then add Rolling Cargo Check In
     *
     * @access	private
     * @return		void
     */
    private function _addInfo()
    {
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE) {
            //set default values
            $this->tools->setPost('cash', 0.00);
            $this->tools->setPost('paid_status', 0);

            $passenger_baggage_checkin = new Passenger_Baggage_Checkin_Model();

            //Condition before updating
            $condition = array('transaction_ref' => $this->tools->getPost('transaction_ref'));
            //Set of data to be updated
            $data = array('paid_status' => 1,
                          'amount_tendered' => $this->tools->getPost('amount_tendered'));

            if ($passenger_baggage_checkin->update($condition, $data)) {
                parent::logThis($this->tools->getPost('reference_no'), 'Successfully added passenger baggage check-in payment');

                //Insert data to transaction table
                $transaction_data = array('total_amount'    => $this->tools->getPost('total_amount'),
                                          'amount_tendered' => $this->tools->getPost('amount_tendered'),
                                          'checkin_type_id' => 2,
                                          'checkin_id'      => $this->tools->getPost('id_passenger_baggage_checkin'),
                                          'transaction_ref' => $this->tools->getPost('reference_no'));
                $passenger_baggage_checkin->insertTransaction($this->tools->getPost('reference_no'),$transaction_data);

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added passenger baggage check-in payment');
                $this->session->set_flashdata('id', $this->tools->getPost('reference_no'));

                //Redirect to print page when successfully saving payment
                redirect(admin_url('passenger_baggage_checkin', 'printBaggageReceipt', $this->tools->getPost('id_passenger_baggage_checkin'), $this->tools->getPost('voyage_management_id')));
            } else {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving passenger baggage check-in payment');
            }

            redirect(admin_url($this->classname, 'view', $this->tools->getPost('voyage_management_id')));
        }
    }

    // --------------------------------------------------------------------


    /*
	 * Validate then edit passenger baggage checkin
	 *
	 * @access	private
	 * @return		void
	 */
    private function _editInfo()
    {
        $passenger_baggage_checkin = new Passenger_Baggage_Checkin_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE) {

            parent::copyFromPost($passenger_baggage_checkin, 'id_passenger_baggage_checkin');

            // Check for successful update
            if ($passenger_baggage_checkin->update()) {
                parent::logThis($passenger_baggage_checkin->id, 'Successfully updated passenger baggage check-in');

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Successfully updated passenger baggage check-in');
                $this->session->set_flashdata('id', $passenger_baggage_checkin->id);
            } else {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating passenger baggage check-in');
            }

            $departure_date = $this->tools->getPost('departure_date');
            $voyage_management_id = $this->tools->getPost('voyage_management_id');
            redirect(admin_url($this->classname, 'filter', $departure_date, $voyage_management_id));
        }
    }

    // ===================================================================
    //    AJAX FUNCTION
    // ===================================================================
    protected function ajax()
    {
        // Initialize
        $return = array();

        // Get module
        $module = $this->tools->getPost("module");

        // Return option
        switch ($module) {

            case 'passenger_details':
                $passenger_Details = $this->passenger_baggage_checkin->displayList(array('tsi.ticket_no' => $this->tools->getPost("ticket_no")));

                foreach ($passenger_Details as $rkey => $rvalue)
                    $return["details"] = $rvalue;

                break;
        }


        $this->encode($return);
    }

    // --------------------------------------------------------------------

    /*
     * Validate then edit Rolling Cargo Check In
     *
     * @access	protected
     * @return		json
     */
    protected function encode($data)
    {
        echo json_encode($data);
    }

}

/* End of file rolling_cargo_checkin.php */
/* Location: ./application/modules_core/adminpanel/conttrollers/passenger_baggage_checkin/passenger_baggage_checkin.php */