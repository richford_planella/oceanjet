<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Passengers Baggage Check In Class
|--------------------------------------------------------------------------
|
| Passengers Baggage Check In Content Management
|
| @category	Controller
| @author		Gian Asuncion
*/

class Passenger_Baggage_Checkin extends Admin_Core
{

    // ------------------------------------------------------------------------

    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());

        parent::__construct();

        // Passenger Baggage Check In Model
        $this->load->model(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin_model'));
        $this->passenger_baggage_checkin = new Passenger_Baggage_Checkin_Model();

        // Rolling Cargo Check In Model
        $this->load->model(admin_dir('rolling_cargo/rolling_cargo_model'));
        $this->roro = new Rolling_Cargo_Model();

        // Rolling Cargo Check In Model
        $this->load->model(admin_dir('rolling_cargo_checkin/rolling_cargo_checkin_model'));
        $this->rolling_cargo = new Rolling_Cargo_Checkin_Model();

        // Voyage
        $this->load->model(admin_dir('voyage/voyage_model'));
        $this->voyage = new Voyage_Model();

        // Voyage Management
        $this->load->model(admin_dir('voyage_management/voyage_management_model'));
        $this->voyage_management = new Voyage_Management_Model();

        // SuB Voyage Management
        $this->load->model(admin_dir('subvoyage_management/subvoyage_management_model'));
        $this->subvoyage_management = new Subvoyage_Management_Model();

        // Transaction
        $this->load->model(admin_dir('transaction/transaction_model'));
        $this->transaction = new Transaction_Model();

        // Voyage Management Status
        $this->load->model(admin_dir('voyage_management_status/voyage_management_status_model'));
        $this->voyage_management_status = new Voyage_Management_Status_Model();

        //Passenger check-in Model
        $this->load->model(admin_dir('passenger_checkin/Passenger_Checkin_Model'));
        $this->passenger_checkin = new Passenger_Checkin_Model();

        //SubVoyage
        $this->load->model(admin_dir('subvoyage/subvoyage_model'));
        $this->subvoyage = new Subvoyage_Model();

        //Attended baggage
        $this->load->model(admin_dir('attended_baggage/Attended_Baggage_Model'));
        $this->attended_baggage = new Attended_Baggage_Model();

        //Port
        $this->load->model(admin_dir('port/Port_Model'));
        $this->port = new Port_Model();

        // Rolling Cargo Check In id
        $this->id = $this->uri->rsegment(3);
    }

    public function index()
    {
        redirect(admin_url($this->classname, 'add'));
    }

    // --------------------------------------------------------------------
    /*
     * Display Passenger Baggage Check In Filter List
     *
     * @access	public
     * @return		void
     */
    public function filter()
    {
        // Get all rolling cargo
        $departure_date = $this->uri->rsegment(3);
        $voyage_id = $this->uri->rsegment(4);
        //List of baggages under particular voyage
        $passenger_baggage_checkin = $this->passenger_baggage_checkin->displayList(array('vm.departure_date' => $departure_date, 'voyage_management_id' => $voyage_id));

        // Initialize data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header')),
            'footer' => parent::getTemplate(admin_dir('footer')),
            'voyage_details' => $this->voyage_management->displayList(array('u.departure_date' => $departure_date, 'u.id_voyage_management' => $voyage_id)),
            'passenger_baggage_checkins' => $passenger_baggage_checkin,
            'departure_date' => $departure_date,
            'voyage_id' => $voyage_id
        );

        parent::displayTemplate(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin/passenger_baggage_checkin'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * View Passenger Check In
     *
     * @access	public
     * @return		void
     */
    public function view()
    {
        // Get baggage lists based on sub voyage id
        $subvoyage_management_id = $this->uri->rsegment(3);
        $baggages = $this->passenger_baggage_checkin->displayList(array('u.subvoyage_management_id'=>$subvoyage_management_id));

        //Get Voyage Details
        $subvoyage = $this->subvoyage_management->displayList(array("u.id_subvoyage_management"=> $subvoyage_management_id));

        // Initialize data
        $data = array(
            'header'	    => Modules::run(admin_dir('header/call_header'),array('title'=>'View Baggage List')),
            'footer'		=> parent::getTemplate(admin_dir('footer')),
            'baggages'	    => $baggages,
            'subvoyage_details'     => $subvoyage
        );

        parent::displayTemplate(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin/passenger_baggage_checkin'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Toggle user status
     *
     * @access	public
     * @return		void
     */
    public function toggle()
    {
        // Get rolling cargo checkin
        $rolling_cargo_checkin = new Rolling_Cargo_Checkin_Model($this->id);

        // Check if a record exists
        $rolling_cargo_checkin->redirectIfEmpty(admin_url($this->classname));

        // Check for successful toggle
        if ($rolling_cargo_checkin->toggleStatus()) {
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating Rolling Cargo Check In');
            $this->session->set_flashdata('id', $rolling_cargo_checkin->id);
        } else {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating Rolling Cargo Check In');
        }

        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------

    /*
     * Add Passenger Baggage Check In
     *
     * @access	public
     * @return		void
     */
    public function add()
    {
        //Set default variables
        $voyage_management_id = $this->uri->rsegment(3);

        //Set default voyage details once voyage management Id is being set
        //Else set to empty details
        $voyage_details = '';
        if ($voyage_management_id!=null)
            $voyage_details = $this->subvoyage_management->displaylist(array('u.id_subvoyage_management' => $voyage_management_id))[0];

        //Set origin & destination IDs
        if(isset($voyage_details->origin_id) AND  isset($voyage_details->destination_id)) {
            $this->tools->setPost('origin_id', $voyage_details->origin_id);
            $this->tools->setPost('destination_id', $voyage_details->destination_id);
            $this->tools->setPost('departure_date', $voyage_details->departure_date);
            $this->tools->setPost('voyage_management_id', $voyage_details->id_subvoyage_management);
            $this->tools->setPost('voyage_text', $voyage_details->voyage);
            $this->tools->setPost('subvoyage_status_text', $voyage_details->description);
            $this->tools->setPost('subvoyage_status_id', $voyage_details->subvoyage_management_status_id);
            $this->tools->setPost('origin', $voyage_details->port_origin);
            $this->tools->setPost('destination', $voyage_details->port_destination);
            $this->tools->setPost('vessel', $voyage_details->vessel_code);
            $this->tools->setPost('eta', $voyage_details->ETA);
            $this->tools->setPost('etd', $voyage_details->ETD);
        }

        //Get sub voyage lists
        //Get Attended baggage
        $subvoyage = array();
        $baggage_rates = array();
        if(($this->tools->getPost('origin_id') AND $this->tools->getPost('destination_id'))) {
            $subvoyage = $this->passenger_checkin->getSubVoyages($this->tools->getPost("origin_id"),
                                                                 $this->tools->getPost("destination_id"));

            $baggage_rates = $this->attended_baggage->displayList(array('u.origin_id'=>$this->tools->getPost("origin_id"),
                                                                        'u.destination_id'=> $this->tools->getPost("destination_id")));
        }


        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'),array('title'=>'Passenger Baggage Check-In')),
            'footer' => parent::getTemplate(admin_dir('footer')),
            'voyage_management_id' => $voyage_management_id,
            'voyage_management_details' => $voyage_details,
            'voyage' => $this->voyage->displaylist(),
            'post_data' => $this->tools->getPost(),
            'subvoyage' => $subvoyage,
            'baggage_rates' => $baggage_rates,
            'ports'         => $this->port->displayList(array('u.enabled'=>1),array('u.id_port'=>'ASC'))
        );

        parent::displayTemplate(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin/form/add/passenger_baggage_checkin'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Edit Rolling Cargo Check In
     *
     * @access	public
     * @return		void
     */
    public function edit()
    {
        // Get Passenger Baggage checkin info
        $passenger_baggage_checkin = $this->passenger_baggage_checkin->displayList(array('u.id_passenger_baggage_checkin'=>$this->id))[0];

        self::_validate();

        // get data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header')),
            'footer' => parent::getTemplate(admin_dir('footer')),
            'passenger_baggage_checkin' => $passenger_baggage_checkin,
            'voyage_management_id' => $passenger_baggage_checkin->voyage_management_id,
            'voyage_management'    => $this->voyage_management->displaylist(array('u.departure_date' => $passenger_baggage_checkin->departure_date)), //List of voyage based on departure date
            'voyage_details'       => $this->voyage_management->displaylist(array('u.id_voyage_management' => $passenger_baggage_checkin->voyage_management_id))[0],
        );

        parent::displayTemplate(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin/form/edit/passenger_baggage_checkin'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Validate the form
     *
     * @access	private
     * @return		void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('origin_id', 'origin', 'required|trim');
        $this->form_validation->set_rules('destination_id', 'destination', 'required|trim');
        $this->form_validation->set_rules('voyage_text', 'voyage', 'required|trim');
        $this->form_validation->set_rules('voyage_management_id', 'voyage', 'required|trim');
        $this->form_validation->set_rules('subvoyage_status_text', 'leg status', 'required|trim');
        $this->form_validation->set_rules('subvoyage_status_id', 'leg status ID', 'required|trim');
        $this->form_validation->set_rules('departure_date', 'Departure date', 'required|trim');
        $this->form_validation->set_rules('origin', 'origin', 'required|trim');
        $this->form_validation->set_rules('destination', 'destination', 'required|trim');
        $this->form_validation->set_rules('etd', 'ETD', 'required|trim');
        $this->form_validation->set_rules('eta', 'ETA', 'required|trim');
        $this->form_validation->set_rules('vessel', 'vessel', 'required|trim');
        $this->form_validation->set_rules('reference_no', 'reference no', 'required|trim');
        $this->form_validation->set_rules('ticket_no', 'ticket no', 'required|trim');
        $this->form_validation->set_rules('booking_status_id', 'Booking Stat ID', 'required|trim');
        $this->form_validation->set_rules('passenger_id', 'passenger id', 'required|trim');
        $this->form_validation->set_rules('passenger_name', 'passenger name', 'required|trim');
        $this->form_validation->set_rules('id_attended_baggage', 'attended baggage', 'required|trim');
        $this->form_validation->set_rules('reference_no', 'Reference number', 'required|trim');
        $this->form_validation->set_rules('weight', 'weight', 'required|trim|decimal');
        $this->form_validation->set_rules('quantity', 'quantity', 'required|trim|numeric');
        $this->form_validation->set_rules('uom', 'UOM', 'required|trim');
        $this->form_validation->set_rules('free_baggage', 'free baggage', 'required|trim');
        $this->form_validation->set_rules('attended_baggage', 'attended baggage', 'required|trim');
        $this->form_validation->set_rules('excess_baggage_amount', 'attended baggage', 'required|trim');
        $this->form_validation->set_rules('total_amount', 'total amount', 'required|trim');

        self::_addInfo();
    }

    // --------------------------------------------------------------------

    /*
     * Validate then add Rolling Cargo Check In
     *
     * @access	private
     * @return		void
     */
    private function _addInfo()
    {
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE) {
            //set default values
            $this->tools->setPost('cash', 0.00);
            $this->tools->setPost('paid_status', 0);

            $passenger_baggage_checkin = new Passenger_Baggage_Checkin_Model();

            //Condition before updating
            $condition = array('transaction_ref'=>$this->tools->getPost('transaction_ref'));
            //Set of data to be updated
            $data = array('attended_baggage_id'   => $this->tools->getPost('id_attended_baggage'),
                          'quantity'     => $this->tools->getPost('quantity'),
                          'weight'       => $this->tools->getPost('weight'),
                          'reference_no' => $this->tools->getPost('reference_no'),
                          'paid_status'  => 0,
                          'free_baggage' => $this->tools->getPost('free_baggage'),
                          'total_amount' => $this->tools->getPost('total_amount'));

            if ($passenger_baggage_checkin->update($condition, $data)) {
                parent::logThis($this->tools->getPost('reference_no'), 'Successfully added passenger baggage check-in');

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added passenger baggage check-in');
                $this->session->set_flashdata('id', $this->tools->getPost('reference_no'));

            } else {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving passenger baggage check-in');
            }

            redirect(admin_url($this->classname, 'view', $this->tools->getPost('voyage_management_id')));
        }
    }

    // --------------------------------------------------------------------


    /*
	 * Validate then edit passenger baggage checkin
	 *
	 * @access	private
	 * @return		void
	 */
    private function _editInfo()
    {
        $passenger_baggage_checkin = new Passenger_Baggage_Checkin_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {

            parent::copyFromPost($passenger_baggage_checkin, 'id_passenger_baggage_checkin');

            // Check for successful update
            if ($passenger_baggage_checkin->update())
            {
                parent::logThis($passenger_baggage_checkin->id, 'Successfully updated passenger baggage check-in');

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Successfully updated passenger baggage check-in');
                $this->session->set_flashdata('id', $passenger_baggage_checkin->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating passenger baggage check-in');
            }

            $departure_date = $this->tools->getPost('departure_date');
            $voyage_management_id = $this->tools->getPost('voyage_management_id');
            redirect(admin_url($this->classname, 'filter', $departure_date, $voyage_management_id));
        }
    }

    // ===================================================================
    //    AJAX FUNCTION
    // ===================================================================
    protected function ajax()
    {
        // Initialize
        $return = array();

        // Get module
        $module = $this->tools->getPost("module");

        // Return option
        switch ($module) {

            case 'lists_of_bagges':
                $voyages = $this->passenger_baggage_checkin->displayList(array('u.subvoyage_management_id'=>$this->tools->getPost("subvoyage_management_id")));

                //Prepare voyages table for modal table
                $return["lists_baggages"] = '';
                foreach ($voyages as $rkey => $rvalue){
                    $button = "<input class='btn custom-btn-test select-voyage'
                                                       data-id-passenger-baggage-checkin='".$rvalue->id_passenger_baggage_checkin."' type='button' value='Re-print baggage claim tag'>";
                    $return["lists_baggages"] .= "<tr>
                                              <td>". $rvalue->transaction_ref ." </td>
                                              <td>". $rvalue->ticket_no ." </td>
                                              <td>". $rvalue->lastname  .", ".$rvalue->firstname ." ".$rvalue->middlename." </td>
                                              <td>". $rvalue->total_amount." </td>
                                              <td>". (($rvalue->paid_status) == 1 ? 'Paid' : 'Not yet paid' )." </td>
                                              <td>
                                              ". (($rvalue->paid_status) == 1 ? $button  : '-' ) ."
                                              </td>
                                            </tr>";
                }

                //if no voyage found
                if(count($voyages)==0)
                    $return["lists_baggages"] = "<tr><td colspan='6'>No baggages found. Please try again.</td></tr>";
                break;

            case 'subvoyage':
                $return["option"] = array();

                //Get sub voyage lists
                $subvoyage = $this->passenger_checkin->getSubVoyages($this->tools->getPost("origin_id"),
                                                                     $this->tools->getPost("destination_id"));

                foreach($subvoyage as $key => $value)
                    $return["option"][$key] = "<option value='" . $value->id_subvoyage_management . "'>".$value->voyage." | ". $value->port_origin."-".$value->port_destination ." | ".  date("F j, Y - D", strtotime($value->departure_date))." | ". date('g:i a',strtotime($value->ETD)) ."-". date('g:i a',strtotime($value->ETA)) ." </option>";

                array_unshift($return["option"], "<option value=''>Please select</option>");
                break;

            case 'details':
                //Get Voyage Details
                $subvoyage = $this->subvoyage_management->displayList(array("u.id_subvoyage_management"=> $this->tools->getPost("voyage_management_id")));

                foreach($subvoyage as $rkey => $rvalue)
                    $return["subvoyage"]["details"] = $rvalue;

                //Get Excess Baggage Rate
                $return["attended_baggage_option"] = array();
                $baggage_rates = $this->attended_baggage->displayList(array('u.origin_id' => $this->tools->getPost("origin_id"),'u.destination_id'=>$this->tools->getPost("destination_id")));
                foreach ($baggage_rates as $rkey => $rvalue)
                    $return["attended_baggage_option"][$rkey] = "<option value='" . $rvalue->id_attended_baggage . "'>[" . $rvalue->attended_baggage . "] " . $rvalue->description . "</option>";

                array_unshift($return["attended_baggage_option"], "<option value=''>Please select</option>");

            break;

            case 'passenger_details':
                foreach ($this->passenger_baggage_checkin->passengerDetails($this->tools->getPost("ticket_no")) as $rvalue) {
                    $return["details"]["passenger_id"]             = $rvalue->passenger_id;
                    $return["details"]["id_ticket_series_info"]    = $rvalue->id_ticket_series_info;
                    $return["details"]["passenger_name"]           = $rvalue->passenger_name;
                    $return["details"]["id_rule_type"]             = $rvalue->id_rule_type;
                    $return["details"]["rule_type_amount"]         = $rvalue->rule_type_amount;
                    $return["details"]["subvoyage_management_id"]  = $rvalue->subvoyage_management_id;
                    $return["details"]["transaction_ref"]          = $rvalue->transaction_ref;
                    $return["details"]["booking_status_id"]        = $rvalue->booking_status_id;
                    $return["details"]["booking_id"]                = $rvalue->id_booking;
                    $return["details"]["id_number"]                 = $rvalue->passenger_valid_id_number;
                    $return["details"]["accommodation_id"]          = $rvalue->accommodation_id;
                }
                break;

            case 'update_subvoyage_status':
                $status                  = $this->tools->getPost("status");
                $sub_voyage_management_id = $this->tools->getPost("voyage_management_id");
                $departure_delay_time = $this->tools->getPost("delayed_time");
                $canceled_reason      = $this->tools->getPost("canceled_reason");

                $this->subvoyage_management->update(array("id_subvoyage_management"=>$sub_voyage_management_id),array('subvoyage_management_status_id'=>$status));

                //Departed but delayed
                if($status == 3) {
                    $this->subvoyage_management->update(array("id_subvoyage_management"=>$sub_voyage_management_id),array('subvoyage_management_status_id'=>$status, 'departure_delay_time'=>$departure_delay_time));
                }elseif($status == 6){
                //Cancelled
                    $this->subvoyage_management->update(array("id_subvoyage_management"=>$sub_voyage_management_id),array('subvoyage_management_status_id'=>$status, 'remarks'=>$canceled_reason));
                }

                //Get Sub voyage status
                $subvoyage = $this->subvoyage_management->displayList(array("id_subvoyage_management"=>$sub_voyage_management_id));

                foreach($subvoyage as $rkey => $rvalue)
                    $return["subvoyage"]["details"] = $rvalue;
                break;

            case 'excess_baggage':
                foreach ($this->attended_baggage->displaylist(array("u.id_attended_baggage" => $this->tools->getPost("id_attended_baggage"))) as $rkey => $rvalue) {
                    $return["details"] = $rvalue;
                }
                break;
        }


        $this->encode($return);
    }

    // --------------------------------------------------------------------

    /*
     * Validate then edit Rolling Cargo Check In
     *
     * @access	protected
     * @return		json
     */
    protected function encode($data)
    {
        echo json_encode($data);
    }

    /*
     * Print baggage claim tag
     * @return pdf file
     * */
    public function printBaggageReceipt()
    {
        $ticket_no = $this->uri->rsegment(3);
        $subvoyage_management_id = $this->uri->rsegment(4);
        $data = array('passenger_details'=> $this->passenger_baggage_checkin->displayList(array('tsi.ticket_no' => $ticket_no)),
                      'voyage_details'   => $this->subvoyage_management->displayList(array("u.id_subvoyage_management"=> $subvoyage_management_id)));

        parent::displayTemplate(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin/form/pdf/baggage_receipt'),$data);
        $this->load->library('pdf');

        $mpdf  = $this->pdf->load();
        $mpdf->showImageErrors = true;
        $mpdf->AddPage('L');
        $mpdf->WriteHTML($this->output->get_output());

        $mpdf->Output("passenger_receipt_".date("d-m-Y").".pdf", 'I');
        exit;
    }

}

/* End of file rolling_cargo_checkin.php */
/* Location: ./application/modules_core/adminpanel/conttrollers/passenger_baggage_checkin/passenger_baggage_checkin.php */