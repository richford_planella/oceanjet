<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Port Class
|--------------------------------------------------------------------------
|
| Port Content Management
|
| @category	Controller
| @author		Richford Planella
*/
class Port extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		$this->load->model(admin_dir('port/port_model'));                
		$this->port = new Port_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Port Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		// Get all outlets
		$port = $this->port->displaylist();
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title'=>'Port')),
			'footer'    => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.port.js')))),
			'port'		=> $port,
		);
		
		parent::displayTemplate(admin_dir('port/port/port'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Port Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// get user
		$port = new Port_Model($this->id);
						
		// Check if a record exists
		$port->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'View Port')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'port'		=> $port,
		);
		
		parent::displayTemplate(admin_dir('port/port/form/port'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get port
		$port = new Port_Model($this->id);
		
		// Check if a record exists
		$port->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($port->toggleStatus())
		{	
				// Set confirmation message
				if($accommodation->enabled)
					$this->session->set_flashdata('confirm', $port->port .' has been de-activated!');
				else
					$this->session->set_flashdata('confirm', $port->port .' has been re-activated!');
		}
		else
		{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating port.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Port
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Create Port')),
			'footer'    => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.port.js')))),
		);
			
		parent::displayTemplate(admin_dir('port/port/form/add/port'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Port
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// get user
		$port = new Port_Model($this->id);
		
		// Check if a record exists
		$port->redirectIfEmpty(admin_url($this->classname));
		
		// Form validation
		self::_validate();
		
		// get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Edit Port')),
			'footer'    => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.port.js')))),
			'port'		=> $port,
		);
		
		parent::displayTemplate(admin_dir('port/port/form/edit/port'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
                             
		if ($this->uri->rsegment(2) == 'add') {
			$this->form_validation->set_rules('port_code', 'port code', 'required|trim|is_unique[port.port_code]');
			$this->form_validation->set_rules('terminal_fee', 'terminal fee', 'required|trim|decimal');
			$this->form_validation->set_rules('port_charge', 'port charge', 'required|trim|decimal');
			$this->form_validation->set_rules('port', 'port name', 'required');
			$this->form_validation->set_rules('enabled', 'status', 'required');
			self::_addInfo();
		}
			
		if ($this->uri->rsegment(2) == 'edit') {
			$this->form_validation->set_rules('terminal_fee', 'Terminal fee', 'required|trim|decimal');
			$this->form_validation->set_rules('port_charge', 'Port charge', 'required|trim|decimal');
			$this->form_validation->set_rules('enabled', 'Status', 'required');
			self::_editInfo();
		}
	}
        
    // --------------------------------------------------------------------
	
	/*
     * Delete voyage
     *
     * @access      public
     * @return      void
     */
    public function delete()
    {
        // get user profile
		$port = new Port_Model($this->id);
		
		// Check if a record exists
		$port->redirectIfEmpty(admin_url($this->classname));
		
		if ($port->countFromUser() > 0) {	
			// Set confirmation message
			$this->session->set_flashdata('note', 'Cannot delete. '.$port->port .' was already used in the system.');
		} else {
			// Delete port
			$port->delete(array('id_port' => $this->id));
			
			// Set confirmation message
			if($port->delete())
				$this->session->set_flashdata('confirm', 'Successfully deleted '.$port->port);
			else
				$this->session->set_flashdata('note', "Error deleting port!");
		}
		
		redirect(admin_url($this->classname));
    }
	
	/*
	 * Validate then add Port
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$port = new Port_Model();
			
			parent::copyFromPost($port, 'id_port');
			
            
			$port->port_code = strtoupper($port->port_code);
			// Check for successful insert
			if ($port->add())
			{
				parent::logThis($port->id, 'Successfully added port');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added port');
				$this->session->set_flashdata('id', $port->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving port');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit Port
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
		$port = new Port_Model($this->id);
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                    
			parent::copyFromPost($port, 'id_port');
			
			// Check for successful update
			if ($port->update())
			{									
				parent::logThis($port->id, 'Successfully updated port '. $port->port);
                                        
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated port '. $port->port);
				$this->session->set_flashdata('id', $port->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating port');
			}
			
			redirect(admin_url($this->classname));
		}
	}
   
}

/* End of file port.php */
/* Location: ./application/modules_core/adminpanel/controllers/port/port.php */