<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Class
|--------------------------------------------------------------------------
|
| Outlet Content Management
|
| @category Controller
| @author       Ronald Meran
*/
class Void_Eticket extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('third_party_outlet/void_eticket_model'));
        $this->load->model(admin_dir('port/port_model'));
        $this->load->model(admin_dir('vessel/vessel_model'));   
        $this->load->model(admin_dir('trip_type/trip_type_model')); 
        $this->load->model(admin_dir('voyage/voyage_model'));   
        $this->load->model(admin_dir('accommodation/accommodation_model'));   
        $this->load->model(admin_dir('third_party_outlet/void_eticket_model'));       
        $this->void_eticket = new Void_Eticket_Model();
        $this->trip_type = new Trip_Type_Model();
        $this->voyage = new Voyage_Model();
        $this->accommodation = new Accommodation_Model();
        $this->void_eticket = new Void_Eticket_Model();
        $this->port = new Port_Model();
        $this->vessel = new Vessel_Model();
        // $this->load->model(admin_dir('/Eticket_Model'));
                        
        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Outlet Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        // $eticket = new Eticket_Model(1);
        
        // print_r($this->eticket_passenger->trip_type());exit;
        // Check if a record exists
        // $eticket_passenger->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Void E-Ticket')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.void-eticket.js')))),
           // 'void_eticket' => $this->void_eticket->displayList(array("b.booking_status_id" => 1))
            
        );
        parent::displayTemplate(admin_dir('maintenance'),$data);
        // parent::displayTemplate(admin_dir('third_party_outlet/void_eticket/void_eticket'),$data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $void_eticket = new Void_Eticket_Model($this->id);   
        // Check if a record exists
        $void_eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'void_eticket'        =>$void_eticket->get_eticket_details($this->id),
            'port' => $this->port->displayList(),
            'vessel' => $this->vessel->displayList(),
            'accommodation' => $this->accommodation->displayList()

        );
      
        
        parent::displayTemplate(admin_dir('third_party_outlet/void_eticket/form/void_eticket'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $accommodation = new Void_Eticket_Model($this->id);
        
        // Check if a record exists
        $accommodation->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($accommodation->toggleStatus())
        {   
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating accommodation.');
            $this->session->set_flashdata('id', $accommodation->id);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating accommodation.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();
                            
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Void E-Ticket')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.void-eticket.js')))),
            'trip_type' => $this->trip_type->displayList(),
            'accommodation' => $this->accommodation->displayList(),
            'voyage' => $this->voyage->displayList(),
        );
        

        parent::displayTemplate(admin_dir('third_party_outlet/void_eticket/form/add/void_eticket'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
    
        // // get user
        // $void_eticket = new Void_Eticket_Model($this->id);
         
        // Check if a record exists
       // $void_eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        // $data = array(
        //     'header'    => Modules::run(admin_dir('header/call_header')),
        //     'footer'        => parent::getTemplate(admin_dir('footer')),
        //     'void_eticket'        => $void_eticket->get_eticket_details($this->id),
        //     'port' => $this->port->displayList(),
        //     'vessel' => $this->vessel->displayList(),
        //     'accommodation' => $this->accommodation->displayList()

        // );
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Void E-Ticket')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.void-eticket.js')))),
            'trip_type' => $this->trip_type->displayList(),
            'accommodation' => $this->accommodation->displayList(),
            'voyage' => $this->voyage->displayList(),
        );
        parent::displayTemplate(admin_dir('third_party_outlet/void_eticket/form/edit/void_eticket'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('void_reason', 'Reason to Void', 'required|trim');
        // $this->form_validation->set_rules('eticket_terms', 'Terms and Conditions', 'required|trim');
        // $this->form_validation->set_rules('eticket_reminders', 'Reminders', 'required|trim');

        // if ($this->uri->rsegment(2) == 'add')
        //     self::_addInfo();
            
        // if ($this->uri->rsegment(2) == 'edit')
        //     self::_editInfo();
        //echo $this->uri->rsegment(1);exit;
        if ($this->uri->rsegment(1) == 'void_eticket') {
            self::_editInfo();
        }
            
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Outlet
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $eticket_passenger = new Eticket_Passenger_Model();
            
            parent::copyFromPost($eticket_passenger, 'id_ticket');
                        
            // Check for successful insert
            if ($eticket_passenger->add())
            {
                parent::logThis($accommodation->id, 'Successfully added Accommodation');
                
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added Accommodation');
                $this->session->set_flashdata('id', $outlet->id);   

            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving Accommodation');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {


        $eticket = new Void_Eticket_Model($this->tools->getPost('booking_id'));

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE) { 
           
            parent::copyFromPost($eticket, 'id_booking'); 
            // parent::copyFromPost($void_reason, 'void_reason');    
            // Check for successful update
            if ($eticket->update(array(),array("booking_status_id"=>2)))
            { 
                // insert to void tickets table
                $this->db->insert('void_tickets',array("booking_id"=>$this->id,"void_reason" => $this->tools->getPost('void_reason')));                                 
                parent::logThis($eticket->id, 'Successfully updated Eticket');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Eticket');
                $this->session->set_flashdata('id', $eticket->id);
            } else {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Eticket');
            }
            // print_r($this->db->last_query());exit;
            redirect(admin_url($this->classname));
        }
    }
    public function void() {
        // $eticket = new Eticket_Model(1);
        
        // print_r($this->eticket_passenger->trip_type());exit;
        // Check if a record exists
        // $eticket_passenger->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Void E-Ticket')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.void-eticket.js')))),
           // 'void_eticket' => $this->void_eticket->displayList(array("b.booking_status_id" => 1))
            
        );
        
        parent::displayTemplate(admin_dir('third_party_outlet/void_eticket/void_eticket'),$data);
    }
    public function get_details() {
        $data_discount = $this->void_eticket->get_eticket_list($this->tools->getGet());
        echo json_encode($data_discount);
    }
   
}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */