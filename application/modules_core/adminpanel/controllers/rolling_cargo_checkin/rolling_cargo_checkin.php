<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Rolling Cargo Check In Class
|--------------------------------------------------------------------------
|
| Rolling Cargo Check In Content Management
|
| @category	Controller
| @author	Ronald Meran
*/
class Rolling_Cargo_Checkin extends Admin_Core
{

	// ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		parent::__construct();

		// Rolling Cargo Check In Model
		$this->load->model(admin_dir('port/port_model'));                
		$this->load->model(admin_dir('voyage/voyage_model'));                
		$this->load->model(admin_dir('subvoyage/subvoyage_model'));                
		$this->load->model(admin_dir('transaction/transaction_model'));                
		$this->load->model(admin_dir('booking_roro/booking_roro_model'));                
		$this->load->model(admin_dir('rolling_cargo/rolling_cargo_model'));                
		$this->load->model(admin_dir('ticket_series/ticket_series_ref_model'));                
		$this->load->model(admin_dir('passenger_checkin/passenger_checkin_model'));                
		$this->load->model(admin_dir('rolling_cargo_checkin/rolling_cargo_checkin_model'));                
		$this->load->model(admin_dir('subvoyage_management/subvoyage_management_model'));                
		$this->load->model(admin_dir('voyage_management_status/voyage_management_status_model'));                
		
		// Initialize
		$this->port = new Port_Model();
		$this->voyage = new Voyage_Model();
		$this->roro = new Rolling_Cargo_Model();
		$this->subvoyage = new Subvoyage_Model();
		$this->transaction = new Transaction_Model();
		$this->booking_roro = new Booking_Roro_Model();
		$this->ticket_series = new Ticket_Series_Ref_Model();
		$this->rolling_cargo = new Rolling_Cargo_Checkin_Model();
		$this->passenger_checkin = new Passenger_Checkin_Model();
		$this->subvoyage_management = new Subvoyage_Management_Model();
		$this->voyage_management_status = new Voyage_Management_Status_Model();
		
		// Rolling Cargo Check In id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Rolling Cargo Check In Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{
		// Get all rolling cargo
		$booking_roro = array();
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array("title" => "RORO Check-In")),
			'footer'	=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.rolling_cargo_checkin.js')))),
			'port'	=> $this->port->displayList(array("u.enabled" => 1)),
			'booking_roro'	=> $booking_roro,
			'subvoyage_management_status' => $this->tools->getPost('subvoyage_status'),
			'subvoyage_management_status_id' => $this->tools->getPost('subvoyage_status_id'),
			'subvoyage_update_status_id' => $this->tools->getPost('subvoyage_update_status_id')
		);
		
		parent::displayTemplate(admin_dir('rolling_cargo_checkin/rolling_cargo_checkin/rolling_cargo_checkin'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then add Rolling Cargo Check In
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
		// Check if form validation is TRUE

		if ($this->form_validation->run() == FALSE)
		{
			// Initialize
			$rolling_cargo_checkin = new Rolling_Cargo_Checkin_Model();
             
			// Post data
			$post = array("booking_roro_id" => $this->tools->getPost("booking_roro_id"),
						  "subvoyage_management_id" => $this->tools->getPost("subvoyage_management_id"),
						  "enabled" => 1, "date_added" => date("Y-m-d H:i:s"), "date_update" => date("Y-m-d H:i:s"));

			// Check for successful insert
			if ($rolling_cargo_checkin->add($post))
			{			
				parent::logThis($rolling_cargo_checkin->id, 'Successfully added Rolling Cargo Check In');
				
				// Set confirmation message
				$success = TRUE;
				$this->session->set_flashdata('confirm', 'Successfully added Rolling Cargo Check In');
				$this->session->set_flashdata('id', $rolling_cargo_checkin->id);	
			}
			else
			{
				// Set confirmation message
				$success = FALSE;
				$this->session->set_flashdata('error', 'Error in saving Rolling Cargo Check In');
			}
			
			// Redirect
			$this->encode(array("success" => $success));
		}
	}
        	
	// ===================================================================
	//    AJAX FUNCTION
	// ===================================================================
	
	protected function ajax()
	{
		// Initialize
		$return = array();
		
		// Initialize
		$obj = $this->_cast_to_obj($this->tools->getPost());

		// Return option
		switch($obj->module){
			
			// Print voyages in options
			case 'voyage':
				$this->printSubvoyages($obj);
				break;

			// Print the Subvoyage details
			case 'dtvoyage':
				$this->printSubVoyageDetails($obj);
				break;
			
			// Details of Ticket
			case 'dtTicket':
				$this->getTicketDetails($obj);
				break;

			// Check in RORO
			case 'checkin':
				$this->checkinRORO($obj);
				break;

			// Change the voyage status
			case 'changeVoyageStatus':
				$this->changeVoyageStatus($obj);
				break;

			// Get the list of checked in RORO
			case 'checkedInROROList':
				$this->getCheckedInRORO($obj);
				break;
		}
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get Subvoyages
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function getSubVoyages($obj)
	{	
		// Initialize
		$subvoyages = array();
		
		// Get subvoyages
		$subvoyage = $this->passenger_checkin->getSubVoyages($this->tools->getPost("origin"), $this->tools->getPost("destination"));

		// Subvoyages
		if(!empty($subvoyage))
			foreach($subvoyage as $skey => $svalue)
				$subvoyages[$svalue->id_subvoyage_management] = $svalue;
				
		return $subvoyages;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Print Subvoyages in select option
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function printSubvoyages($obj)
	{
		// Initialize
 		$return["option"] = array();

		// Get Subvoyages
		$subvoyage = $this->getSubVoyages($obj);

		// Initialize
		$return["opSubvoyage"] = array();
		
		// Subvoyage origin
		if(!empty($subvoyage))
			foreach($subvoyage as $key => $value)
				$return["opSubvoyage"][$value->id_subvoyage_management] = "<option value='".$value->id_subvoyage_management."'>".$value->voyage." | ".date("M d, Y \- D", strtotime($value->departure_date))." | ".date("g:i a", strtotime($value->ETD))." - ".date("g:i a", strtotime($value->ETA))."</option>";
		
		// Add Please Select
		array_unshift($return["opSubvoyage"], "<option value=''>Please Select</option>");
		
		// Print
		print_r($return["opSubvoyage"]);

	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get Subvoyage Details
	 *
	 * @access	protected
	 * @return	array
	 */
	protected function getSubVoyageDetails($obj)
	{
		// Initialize
		$subvoyages = array();
		
		// Get subvoyages
		$subvoyage = $this->subvoyage_management->displayList(array("id_subvoyage_management" => $obj->voyage));
		
		// Get only the subvoyage selected
		foreach($subvoyage as $skey => $svalue)
			if($svalue->id_subvoyage_management == $obj->voyage)
				$subvoyages = $svalue;
		
		return $subvoyages;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Print Subvoyage Details
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function printSubVoyageDetails($obj)
	{
		// Get Subvoyage details
		$return = $this->getSubVoyageDetails($obj);

		// Print
		$this->encode($return);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Get the Checked In RORO
	 *
	 * @access	protected
	 * @return	html
	 */
	protected function getCheckedInRORO($obj)
	{
		// Get Checked In RORO using subvoyage management id
		$return = $this->rolling_cargo->displayList(array("id_subvoyage_management" => $obj->voyage));

		// Initialize
		$data = array("booking_roro" => $return);

		// Get the template
		parent::displayTemplate(admin_dir('rolling_cargo_checkin/rolling_cargo_checkin/rolling_cargo_list'), $data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Get Ticket Number Details
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function getTicketDetails($obj)
	{
		// Get the ticket series
		$ticket_series = $this->ticket_series->displayList(array("ticket_no" => $obj->ticket_number),"","",1);

		// Ticket Series Ref Id
		$ticket_series_id = isset($ticket_series[0]->id_ticket_series_info) ? $ticket_series[0]->id_ticket_series_info : FALSE;

		// Get the details in Booking Roro		
		$booking_roro = $this->booking_roro->displayList(array("ticket_series_info_id" => $ticket_series_id));

		// Encode result
		if($ticket_series_id)
			$booking_roro = $booking_roro[0];
		else
			$booking_roro = array("success" => FALSE);

		// Return
		$this->encode($booking_roro);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Checkin the RORO Vehicle
	 *
	 * @access	protected
	 * @return	void
	 */
	protected function checkinRORO($obj)
	{
		// Checkin the vehicle
		self::_addInfo();
	}

	/*
	 * Change the status of Subvoyage Management
	 *
	 * @access	protected
	 * @return	void
	 */
	protected function changeVoyageStatus($obj)
	{
		// Initialize
		$data = array();

		// Get rolling cargo
		$subvoyage_management = new Subvoyage_Management_Model($obj->subvoyage_management_id);

		// Check if a record exists
		$subvoyage_management->redirectIfEmpty(admin_url($this->classname));

		// Check if valdata is empty
		$check_data = !empty($obj->valdata) ? $obj->valdata : FALSE; 

		// Update data
		if(!empty($check_data)){
			if($obj->cstatus == 3)
				$data["departure_delay_time"] = $obj->valdata;
			elseif($obj->cstatus == 6)
				$data["remarks"] = $obj->valdata;
			
			// Set Status
			$data["subvoyage_management_status_id"] = $obj->cstatus;
		}

		// Check for successful update
		if($subvoyage_management->update(array("id_subvoyage_management" => $subvoyage_management->id), $data)){									
			// Success
			$success = !empty($check_data) ? TRUE : FALSE;
			$message = !empty($check_data) ? 'Successfully updated voyage status' : "Please fill up required field";

			// Get the new subvoyage management
			$subvoyage_management = new Subvoyage_Management_Model($obj->subvoyage_management_id);
			parent::logThis($subvoyage_management->id, $message);
                                    
			// Set confirmation message
			$this->session->set_flashdata('confirm', $message);
			$this->session->set_flashdata('id', $subvoyage_management->id);
		}
		else
		{
			// Set confirmation message
			$success = FALSE;
			$message = 'Error in updating voyage status';
			$this->session->set_flashdata('note', $message);
		}

		// Redirect
		$this->encode(array("success" => $success, "message" => $message, "subvoyage" => $subvoyage_management));

	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Cast array to object
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function _cast_to_obj($arr)
	{
		// Cast to object
		return (object) $arr;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Print json
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function encode($data)
	{
		echo json_encode($data);
	}
   
}

/* End of file rolling_cargo_checkin.php */
/* Location: ./application/modules_core/adminpanel/conttrollers/rolling_cargo_checkin/rolling_cargo_checkin.php */