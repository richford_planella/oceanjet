<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Login Class
|--------------------------------------------------------------------------
|
| Delegates functions for adminsitration login records
|
| @category		Controller
| @author		Baladeva Juganas
*/

class Login extends MY_Controller
{
        public function __construct()
        {
            parent::__construct();
        }
        
        public function index()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('login/header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('login/footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('login/login'),$data);
        }
        
        public function dashboard()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/dashboard'),$data);
        }
        
        public function messages()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/messages'),$data);
        }
        
        public function tasks()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/tasks'),$data);
        }
        
        public function ui()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/ui'),$data);
        }
        
        public function widgets()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/widgets'),$data);
        }
        
        public function submenu()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/submenu'),$data);
        }
        
        public function submenu2()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/submenu2'),$data);
        }
        
        public function submenu3()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/submenu3'),$data);
        }
        
        public function form()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/form'),$data);
        }
        
        public function chart()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/chart'),$data);
        }
        
        public function typography()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/typography'),$data);
        }
        
        public function gallery()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/gallery'),$data);
        }
        
        public function table()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/table'),$data);
        }
        
        public function calendar()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/calendar'),$data);
        }
        
        public function file_manager()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/file_manager'),$data);
        }
        
        public function icon()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('templates/icon'),$data);
        }
}