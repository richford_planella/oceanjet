<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Class
|--------------------------------------------------------------------------
|
| Outlet Content Management
|
| @category Controller
| @author       Ronald Meran
*/
class Approved_Refund_Eticket extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {


        // Classname
        $this->classname = strtolower(get_class());
       
        parent::__construct();

        $this->load->model(admin_dir('third_party_outlet/approved_refund_eticket_model'));   
        $this->load->model(admin_dir('trip_type/trip_type_model')); 
        $this->load->model(admin_dir('voyage/voyage_model'));   
        $this->load->model(admin_dir('accommodation/accommodation_model'));  
        $this->load->model(admin_dir('port/port_model'));
        $this->load->model(admin_dir('rule_set/rule_set_model'));  
        $this->load->model(admin_dir('rule_set/rule_type_model'));   
         $this->load->model(admin_dir('rule_set/rule_type_name_model'));     
        $this->refund_eticket_approved = new Approved_Refund_Eticket_Model();
        $this->trip_type = new Trip_Type_Model();
        $this->voyage = new Voyage_Model();
        $this->port = new Port_Model();
        $this->accommodation = new Accommodation_Model();
        $this->rule_set = new Rule_Set_Model();
        $this->rule_type = new Rule_Type_Model();
        $this->rule_type_name = new Rule_Type_Name_Model();
        // $this->load->model(admin_dir('/Eticket_Model'));
                        
        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Outlet Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {   
       
        
        // $eticket = new Eticket_Model(1);
        
        // print_r($this->eticket_passenger->trip_type());exit;
        // Check if a record exists
        // $eticket_passenger->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array("title"=>"Approved E-ticket Refund Requests")),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'refund_eticket' => $this->refund_eticket_approved->displayList(array("booking_source_id"=>3,"booking_status_id"=>3,"refund_ticket_status_id"=>2))
            
        );
        
        parent::displayTemplate(admin_dir('third_party_outlet/approved_refund_eticket/approved_refund_eticket'),$data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $refund_eticket_approved = new Approved_Refund_Eticket_Model($this->id);
                        
        // Check if a record exists
        $refund_eticket_approved->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array("title"=>"Approved E-ticket Refund Requests")),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'eticket'        => $refund_eticket_approved->get_eticket_details($this->id),
            'port' => $this->port->displayList(),
            'rule_set' => $this->rule_set->displayList(),
            'rule_type' => $this->rule_type->displayList(),
            'accommodation' => $this->accommodation->displayList()
        );
       
        parent::displayTemplate(admin_dir('third_party_outlet/approved_refund_eticket/form/approved_refund_eticket'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $accommodation = new Accommodation_Model($this->id);
        
        // Check if a record exists
        $accommodation->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($accommodation->toggleStatus())
        {   
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating accommodation.');
            $this->session->set_flashdata('id', $accommodation->id);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating accommodation.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();
                            
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.eticket-passenger.js')))),
            'trip_type' => $this->trip_type->displayList(),
            'accommodation' => $this->accommodation->displayList(),
            'voyage' => $this->voyage->displayList()
        );
        

        parent::displayTemplate(admin_dir('third_party_outlet/approved_refund_eticket/form/add/approved_refund_eticket'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
         // // get user
         $refund_eticket_approval = new Approved_Refund_Eticket_Model($this->id);
         
        // Check if a record exists
        $refund_eticket_approval->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'eticket'        => $refund_eticket_approval->get_eticket_details($this->id)
        );
        
        parent::displayTemplate(admin_dir('third_party_outlet/approved_refund_eticket/form/edit/approved_refund_eticket'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('eticket_details', 'E-ticket/itinerary Details', 'required|trim');
        $this->form_validation->set_rules('eticket_terms', 'Terms and Conditions', 'required|trim');
        $this->form_validation->set_rules('eticket_reminders', 'Reminders', 'required|trim');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();
            
        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Outlet
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $eticket_passenger = new Eticket_Passenger_Model();
            
            parent::copyFromPost($eticket_passenger, 'id_ticket');
                        
            // Check for successful insert
            if ($eticket_passenger->add())
            {
                parent::logThis($accommodation->id, 'Successfully added Accommodation');
                
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added Accommodation');
                $this->session->set_flashdata('id', $outlet->id);   

                // passenger info
                $passenger_data = array(
                    'firstname' => $this->tools->getPost('firstname'),
                    'middlename' => $this->tools->getPost('lastname'),
                    'lastname' => $this->tools->getPost('lastname'),
                    'birthdate' => date("Y-m-d",strtotime($this->tools->getPost('birthdate'))),
                    'age' => $this->tools->getPost('age'),
                    'gender' => $this->tools->getPost('gender'),
                    'contactno' => $this->tools->getPost('contactno'),
                    'id_no' => $this->tools->getPost('id_no'),
                    'nationality' => $this->tools->getPost('nationality'),
                    'with_infant' => $this->tools->getPost('with_infant'),
                    'infant_name' => $this->tools->getPost('infant_name')
                    );
                // insert passenger info to passenger table
                $this->db->insert('passenger',$passenger_data);
                //
                $fare_data = array();
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving Accommodation');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $eticket = new Refund_Eticket_Approval_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($eticket, 'id_booking');
            
            // Check for successful update
            if ($eticket->update(array(),array("ticket_status" => 3)))
            {                                   
                 parent::logThis($eticket->id, 'Successfully updated Eticket');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Eticket');
                $this->session->set_flashdata('id', $eticket->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Eticket');
            }
            
            redirect(admin_url($this->classname));
        }
    }
}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */