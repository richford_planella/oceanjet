<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Voyage Class
|--------------------------------------------------------------------------
|
| Voyage Content Management
|
| @category Controller
| @author       Kenneth Bahia
*/
class Voyage extends Admin_Core
{
    private $leg_count = 1;
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('voyage/Voyage_Model'));       
        $this->load->model(admin_dir('vessel/Vessel_Model'));
        $this->load->model(admin_dir('port/Port_Model'));
        $this->load->model(admin_dir('accommodation/Accommodation_Model')); 
        $this->load->model(admin_dir('subvoyage/Subvoyage_Model'));            

        $this->voyage = new Voyage_Model();
        $this->vessel = new Vessel_Model();
        $this->port = new Port_Model();
        $this->accommodation = new Accommodation_Model();
                        
        // Voyage id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Voyage Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        // Get all Voyages
        $voyage = $this->voyage->displaylist();

        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Voyage Code')),
            'footer'    => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'voyage'    => $voyage,
        );
        
        parent::displayTemplate(admin_dir('voyage/voyage'), $data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Voyage Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get Voyage
        $voyage = new Voyage_Model($this->id);

        // get Subvoyages
        $subvoyage = new Subvoyage_Model();
                        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'View Voyage Code')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'voyage'        => $voyage,
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'subvoyages'    => $subvoyage->displayList(array("voyage_id" => $this->id))
        );
        
        parent::displayTemplate(admin_dir('voyage/form/voyage'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Delete voyage
     *
     * @access      public
     * @return      void
     */
    public function delete()
    {
        // get voyage
        $voyage = new Voyage_Model($this->id);
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Check if it is already in use
        if ($voyage->countFromVoyageManagement() > 0)
        {   
            // Set confirmation message
            $this->session->set_flashdata('note', 'Cannot delete. '.$voyage->voyage.' was already used in the system.');
        }
        else
        {
            //Delete its subvoyages
            $subvoyage = new Subvoyage_Model();
            $subvoyage->delete(array("voyage_id" => $this->id));

            //Delete voyage
            if ($voyage->delete())
                $this->session->set_flashdata('confirm', 'Successfully deleted '.$voyage->voyage);
            else
                $this->session->set_flashdata('note', 'Error deleting voyage');
        }
        
        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle Voyage status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get Voyage
        $voyage = new Voyage_Model($this->id);
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($voyage->toggleStatus())
        {   
            // Set confirmation message
            if($voyage->enabled)
                $this->session->set_flashdata('confirm', $voyage->voyage.' has been de-activated!');
            else
                $this->session->set_flashdata('confirm', $voyage->voyage.' has been re-activated!');
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating '.$voyage->voyage.'.');
        }

        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Voyage
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Create Voyage Code')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'leg_count'     => $this->leg_count
        );

        parent::displayTemplate(admin_dir('voyage/form/add/voyage'),$data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Edit Voyage
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get Voyage
        $voyage = new Voyage_Model($this->id);
        $subvoyage = new Subvoyage_Model();
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();

        // get data
        $data = array(
            'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit Voyage Code')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'voyage'        => $voyage,
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'subvoyages'    => $subvoyage->displayList(array("voyage_id" => $this->id))
        );

        parent::displayTemplate(admin_dir('voyage/form/edit/voyage'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('enabled', 'status', 'required');
       
        if ($this->uri->rsegment(2) == 'add') {
            $this->form_validation->set_rules('voyage', 'voyage code', 'required|trim|is_unique[voyage.voyage]');
            $ETD_array = $this->tools->getPost("ETD");
            $ETA_array = $this->tools->getPost("ETA");
            $origin_array = $this->tools->getPost("origin_id");
            $destination_array = $this->tools->getPost("destination_id");
            $vessel_array = $this->tools->getPost("vessel_id");
            $leg_count = $this->tools->getPost("leg_count");

            if (!empty($ETD_array)) {
                //set validations for dynamic elements
                foreach ($ETD_array as $key => $value) {
                    $ETD = $ETD_array[$key];
                    $ETA = $ETA_array[$key];
                    $origin_id = $origin_array[$key];
                    $destination_id = $destination_array[$key];
                    $vessel_id = $vessel_array[$key];

                    $this->form_validation->set_rules('points_redemption['.$key.']', 'points redemption', 'required|numeric|decimal');
                    $this->form_validation->set_rules('ETD['.$key.']', 'ETD', 'required');
                    $this->form_validation->set_rules('ETA['.$key.']', 'ETA', 'required');
                    $this->form_validation->set_rules('origin_id['.$key.']', 'origin', 'required');
                    $this->form_validation->set_rules('destination_id['.$key.']', 'destination', 'required');
                    $this->form_validation->set_rules('vessel_id['.$key.']', 'default vessel', 'required');
                    
                }

                //leg count for view
                $this->leg_count = intval($leg_count);
            }
            self::_addInfo();
        }

        if ($this->uri->rsegment(2) == 'edit') {
            $ETD_array = $this->tools->getPost("ETD");
            $ETA_array = $this->tools->getPost("ETA");

            if (!empty($ETD_array)) {
                foreach ($ETD_array as $key => $value) {
                    $ETD = $ETD_array[$key];
                    $ETA = $ETA_array[$key];
                    
                    $this->form_validation->set_rules('points_redemption['.$key.']', 'points redemption', 'required|numeric|decimal');
                    $this->form_validation->set_rules('ETD['.$key.']', 'ETD', 'required');
                    $this->form_validation->set_rules('ETA['.$key.']', 'ETA', 'required');
                    
                }
            }
            self::_editInfo();
        }


    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Voyage
     *
     * @return      void
     */
    private function _addInfo()
    {
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $voyage = new Voyage_Model();
            
            parent::copyFromPost($voyage, 'id_voyage');
            $voyage->voyage = strtoupper($voyage->voyage);

            $voyage->no_of_subvoyages = intval(count($this->tools->getPost("origin_id")));

            $voyage->trip_type_id = 1;
            if (intval(count($this->tools->getPost("origin_id")) > 1)) {
                $voyage->trip_type_id = 3;
            }

            $voyage->origin_id = reset($this->tools->getPost("origin_id"));
            $voyage->destination_id = end($this->tools->getPost("destination_id"));
            $voyage->ETD = DateTime::createFromFormat("H:i A", reset($this->tools->getPost("ETD")))->format("H:i:s");
            $voyage->ETA = DateTime::createFromFormat("H:i A", end($this->tools->getPost("ETA")))->format("H:i:s");

            // Check for successful insert
            if ($voyage->add())
            {
                parent::logThis($voyage->id, 'Successfully added voyage code');

                $destination_array = $this->tools->getPost("destination_id");
                $vessel_array = $this->tools->getPost("vessel_id");
                $ETD_array = $this->tools->getPost("ETD");
                $ETA_array = $this->tools->getPost("ETA");

                foreach ($this->tools->getPost("origin_id") as $key => $value) {
                    $origin_id = $value;
                    $destination_id = $destination_array[$key];
                    $vessel_id = $vessel_array[$key];
                    $ETD = $ETD_array[$key];
                    $ETA = $ETA_array[$key];

                    $subvoyage = new Subvoyage_Model();
                    $this->tools->setPost("voyage_id", $voyage->id);
                    $this->tools->setPost("vessel_id", $vessel_id);
                    $this->tools->setPost("ETD", DateTime::createFromFormat("H:i A", $ETD)->format("H:i:s"));
                    $this->tools->setPost("ETA", DateTime::createFromFormat("H:i A", $ETA)->format("H:i:s"));
                    $this->tools->setPost("origin_id", $origin_id);
                    $this->tools->setPost("destination_id", $destination_id);
                    $this->tools->setPost("points_redemption", $this->tools->getPost('points_redemption', $key));

                    parent::copyFromPost($subvoyage, 'id_subvoyage');
                    $subvoyage->add();
                }

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added voyage code');
                $this->session->set_flashdata('id', $voyage->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving voyage code');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $voyage = new Voyage_Model($this->id);

        if (trim($this->tools->getPost("voyage")) == trim($voyage->voyage)) {
            $this->form_validation->set_rules('voyage', 'voyage No', 'required|trim');
        } else {
            $this->form_validation->set_rules('voyage', 'voyage No', 'required|trim|is_unique[voyage.voyage]');
        }

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            parent::copyFromPost($voyage, 'id_voyage');
            $voyage->voyage = strtoupper($voyage->voyage);

            //$voyage->origin_id = reset($this->tools->getPost("origin_id"));
            //$voyage->destination_id = end($this->tools->getPost("destination_id"));
            $voyage->ETD = DateTime::createFromFormat("H:i A", reset($this->tools->getPost("ETD")))->format("H:i:s");
            $voyage->ETA = DateTime::createFromFormat("H:i A", end($this->tools->getPost("ETA")))->format("H:i:s");
            
            // Check for successful update
            if ($voyage->update())
            {                                   
                parent::logThis($voyage->id, 'Successfully updated voyage');


                //origin_array = $this->tools->getPost("origin_id");
                //$destination_array = $this->tools->getPost("destination_id");
                $vessel_array = $this->tools->getPost("vessel_id");
                $ETD_array = $this->tools->getPost("ETD");
                $ETA_array = $this->tools->getPost("ETA");
                $points_array = $this->tools->getPost("points_redemption");
                
                //Add subvoyages
                foreach ($this->tools->getPost("id_subvoyage") as $key => $value) {
                    //$origin_id = $value;
                    //$destination_id = $destination_array[$key];
                    $vessel_id = $vessel_array[$key];
                    $ETD = $ETD_array[$key];
                    $ETA = $ETA_array[$key];
                    $id_subvoyage = $value;
                    $points = $points_array[$key];

                    $subvoyage = new Subvoyage_Model($id_subvoyage);
                    $this->tools->setPost("voyage_id", $voyage->id);
                    $this->tools->setPost("vessel_id", $vessel_id);
                    $this->tools->setPost("ETD", DateTime::createFromFormat("H:i A", $ETD)->format("H:i:s"));
                    $this->tools->setPost("ETA", DateTime::createFromFormat("H:i A", $ETA)->format("H:i:s"));
                    $this->tools->setPost("points_redemption", $points);
                    //$this->tools->setPost("origin_id", $origin_id);
                    //$this->tools->setPost("destination_id", $destination_id);

                    parent::copyFromPost($subvoyage, 'id_subvoyage');
                    $subvoyage->update();
                }

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated voyage');
                $this->session->set_flashdata('id', $voyage->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating voyage');
            }
            
            redirect(admin_url($this->classname));
        }
    }
   
}

/* End of file voyage.php */
/* Location: ./application/modules_core/adminpanel/controllers/voyage/voyage.php */