<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Payment Account Class
|--------------------------------------------------------------------------
|
| Payment Account Content Management
|
| @category	Controller
| @author		Ronald Meran
*/
class Payment_Account extends Admin_Core
{
	// ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		$this->load->model(admin_dir('payment_account/payment_account_model'));                
		$this->payment = new Payment_Account_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Payment Account Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		// Get all payment account
		$payment = $this->payment->displaylist();
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Payment Accounts')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.payment_account.js')))),
			'payment'		=> $payment,
		);
		
		parent::displayTemplate(admin_dir('payment_account/payment_account'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Payment Account Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// Get payment account
		$payment = new Payment_Account_Model($this->id);
						
		// Check if a record exists
		$payment->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'View Payment Account')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'payment'		=> $payment,
		);
		
		parent::displayTemplate(admin_dir('payment_account/form/payment_account'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get payment account
		$payment = new Payment_Account_Model($this->id);
		
		// Check if a record exists
		$payment->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($payment->toggleStatus())
		{	
			// Check if status is in the query
			$toggle = isset($payment->enabled) ? $payment->enabled : FALSE;
			$payment_account = isset($payment->payment_account) ? $payment->payment_account : 'payment account';
			
			// Set confirmation message
			if($toggle)
				$this->session->set_flashdata('confirm', ucwords($payment_account).' has been de-activated');
			else
				$this->session->set_flashdata('confirm', ucwords($payment_account).' has been re-activated');
			
			$this->session->set_flashdata('id', $payment->id);
		}
		else
		{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating payment account.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Payment Account
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Create Payment Account')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
		);
			
		parent::displayTemplate(admin_dir('payment_account/form/add/payment_account'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Payment Account
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// Get payment account
		$payment = new Payment_Account_Model($this->id);
		
		// Check if a record exists
		$payment->redirectIfEmpty(admin_url($this->classname));
		
		// Form validation
		self::_validate();
		
		// get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Edit Payment Account')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'payment'		=> $payment,
		);
		
		parent::displayTemplate(admin_dir('payment_account/form/edit/payment_account'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Delete payment account
	 *
	 * @access	public
	 * @return		void
	 */
	public function delete()
	{
		// Get payment account
		$payment = new Payment_Account_Model($this->id);
		
		// Check if a record exists
		$payment->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if($payment->countForeignKey() > 0)
		{	
			// Set confirmation message
			$this->session->set_flashdata('note', 'Cannot delete. '.ucwords($payment->payment_account).' was already used in the system');
		}
		else
		{	
			// Set confirmation message
			if($payment->delete())
				$this->session->set_flashdata('confirm', 'Successfully deleted '.ucwords($payment->payment_account));
			else
				$this->session->set_flashdata('note', "Error deleting payment account");
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		if ($this->uri->rsegment(2) == 'add')
			$this->form_validation->set_rules('payment_account', 'payment account code', 'required|trim|is_unique[payment_account.payment_account]');
		
		$this->form_validation->set_rules('bank', 'bank name', 'required|trim');
		$this->form_validation->set_rules('bank_account_name', 'bank account name', 'required|trim');
		$this->form_validation->set_rules('bank_account_no', 'bank account number', 'required');
		$this->form_validation->set_rules('enabled', 'status', 'required');
                             
		if ($this->uri->rsegment(2) == 'add')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then add Payment Account
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$payment = new Payment_Account_Model();
			
			parent::copyFromPost($payment, 'id_payment_account');
                        
			// Check for successful insert
			if ($payment->add())
			{
				parent::logThis($payment->id, 'Successfully added payment account');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added payment account');
				$this->session->set_flashdata('id', $payment->id);	
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving payment account');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then edit Payment Account
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		$payment = new Payment_Account_Model($this->id);

		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                    
			parent::copyFromPost($payment, 'id_payment_account');
			
			// Check for successful update
			if ($payment->update())
			{									
				 parent::logThis($payment->id, 'Successfully updated payment account');
                                        
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated payment account');
				$this->session->set_flashdata('id', $payment->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating payment account');
			}
			
			redirect(admin_url($this->classname));
		}
	}
   
}

/* End of file payment_account.php */
/* Location: ./application/modules_core/adminpanel/conttrollers/payment_account/payment_account.php */