<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Passenger Fare Class
|--------------------------------------------------------------------------
|
| Passenger Fare Content Management
|
| @category	Controller
| @author	Ronald Meran
*/
class Passenger_Fare extends Admin_Core
{
	// ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		// Load Models
		$this->load->model(admin_dir('port/port_model'));
		$this->load->model(admin_dir('outlet/outlet_model'));                
		$this->load->model(admin_dir('voyage/voyage_model'));
		$this->load->model(admin_dir('rule_set/rule_set_model'));                             
		$this->load->model(admin_dir('trip_type/trip_type_model'));                
		$this->load->model(admin_dir('subvoyage/subvoyage_model'));
		$this->load->model(admin_dir('passenger_fare/conditions_model'));                
		$this->load->model(admin_dir('passenger_fare/passenger_fare_model'));                            
		$this->load->model(admin_dir('accommodation/accommodation_model'));                
		$this->load->model(admin_dir('passenger_fare/subvoyage_fare_model'));                
		$this->load->model(admin_dir('passenger_fare/passenger_fare_conditions_model'));                
		
		// Initialize
		$this->port = new Port_Model();
		$this->outlet = new Outlet_Model();
		$this->voyage = new Voyage_Model();
		$this->rule_set = new Rule_Set_Model();
		$this->trip_type = new Trip_Type_Model();
		$this->condition = new Conditions_Model();
		$this->subvoyage = new Subvoyage_Model();
		$this->subvoyage_fare = new Subvoyage_Fare_Model();
		$this->accommodation = new Accommodation_Model();
		$this->passenger_fare = new Passenger_Fare_Model();
		$this->passenger_fare_conditions = new Passenger_Fare_Conditions_Model();
						
		// Passenger Fare id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Passenger Fare Master List
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{                
		// Get all passenger fare
		$passenger_fare = $this->passenger_fare->displaylist();
		
		// Initialize data
		$data = array(
			'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'Passenger Fare')),
			'footer' => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.passenger_fare.js')))),
			'passenger_fare' => $passenger_fare,
		);
		
		parent::displayTemplate(admin_dir('passenger_fare/passenger_fare'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Passenger Fare Information
	 *
	 * @access	public
	 * @return	void
	 */
	public function view()
	{
		// Get passenger fare
		$passenger_fare = new Passenger_Fare_Model($this->id);
						
		// Check if a record exists
		$passenger_fare->redirectIfEmpty(admin_url($this->classname));

		// Fare conditions
		self::join_fare_conditions($passenger_fare);

		// Initialize data
		$data = array(
			'header'		=> Modules::run(admin_dir('header/call_header'), array('title' => 'View Passenger Fare')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array())),
			'conditions'	=> $this->condition->displaylist(),
			'passenger_fare'=> $passenger_fare,
			'subvoyage' 	=> $this->subvoyage->displaylist(array("voyage_id" => $passenger_fare->voyage_id)),
			
			// Return value for posted data
			'subvoyages' 	=> !empty(self::getPostedSubvoyage()) ? self::getPostedSubvoyage() : array(),
		);
		
		parent::displayTemplate(admin_dir('passenger_fare/form/passenger_fare'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle passenger fare status
	 *
	 * @access	public
	 * @return	void
	 */
	public function toggle()
	{
		// Get passenger fare
		$passenger_fare = new Passenger_Fare_Model($this->id);
		
		// Check if a record exists
		$passenger_fare->redirectIfEmpty(admin_url($this->classname));
				
		// Check for successful toggle
		if ($passenger_fare->toggleStatus())
		{	
			// Check if status is in the query
			$toggle = isset($passenger_fare->enabled) ? $passenger_fare->enabled : FALSE;
			$passenger_fare = isset($passenger_fare->passenger_fare) ? $passenger_fare->passenger_fare : 'passenger fare';
			
			// Set confirmation message
			if($toggle)
				$this->session->set_flashdata('confirm', ucwords($passenger_fare).' has been de-activated');
			else
				$this->session->set_flashdata('confirm', ucwords($passenger_fare).' has been re-activated');
			
			$this->session->set_flashdata('id', $discount->id);
		}
		else
		{
			// Set confirmation message
			$this->session->set_flashdata('note', 'Error in updating passenger fare.');
		}		
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Passenger Fare
	 *
	 * @access	public
	 * @return	void
	 */
	public function add()
	{
		// Form validation
		self::_validate();	
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Create Passenger Fare')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), 
									array("js_files" => array(
										js_dir('jquery', 'jquery.formatCurrency.js'), 
										js_dir('jquery', 'jquery.passenger_fare.js'), 
										js_dir('jquery', 'jquery.numeric-tools.js')))),
			'accommodation' => $this->accommodation->displaylist(array("enabled" => 1)),
			'rule_set' 		=> $this->rule_set->displaylist(array("enabled" => 1)),
			'outlet' 		=> $this->outlet->displaylist(array("u.enabled" => 1)),
			'voyage'		=> $this->voyage->displaylist(array("u.enabled" => 1)),
			'conditions'	=> $this->condition->displaylist(),
			'subvoyage' 	=> $this->subvoyage->displaylist(array("voyage_id" => $this->tools->getPost("voyage_id"))),
			
			// Return value for posted data
			'subvoyages' 	=> !empty(self::getPostedSubvoyage()) ? self::getPostedSubvoyage() : array(),
			'return_voyage' => array(),
		);

		parent::displayTemplate(admin_dir('passenger_fare/form/add/passenger_fare'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Passenger Fare
	 *
	 * @access	public
	 * @return	void
	 */
	public function edit()
	{
		// Get Passenger Fare
		$passenger_fare = new Passenger_Fare_Model($this->id);
		
		// Check if a record exists
		$passenger_fare->redirectIfEmpty(admin_url($this->classname));
		
		// Fare conditions
		self::join_fare_conditions($passenger_fare);
		
		// Form validation
		self::_validate();

		// Get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Edit Passenger Fare')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), 
									array("js_files" => array(
										js_dir('jquery', 'jquery.formatCurrency.js'), 
										js_dir('jquery', 'jquery.passenger_fare.js'), 
										js_dir('jquery', 'jquery.numeric-tools.js')))),
			'conditions'	=> $this->condition->displaylist(),
			'passenger_fare'=> $passenger_fare,
			'subvoyage' 	=> $this->subvoyage->displaylist(array("voyage_id" => $passenger_fare->voyage_id)),
			
			// Return value for posted data
			'subvoyages' 	=> !empty(self::getPostedSubvoyage()) ? self::getPostedSubvoyage() : array(),
		);		
		
		parent::displayTemplate(admin_dir('passenger_fare/form/edit/passenger_fare'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Delete passenger fare
	 *
	 * @access	public
	 * @return	void
	 */
	public function delete()
	{
		// Get outlet
		$passenger_fare = new Passenger_Fare_Model($this->id);
		
		// Check if a record exists
		$passenger_fare->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if($passenger_fare->countForeignKey() > 0)
		{	
			// Set confirmation message
			$this->session->set_flashdata('note', 'Cannot delete. Passenger Fare was already used in the system');
		}
		else
		{	
			  // Delete Passenger Fare child
            $subvoyage_fare = new Subvoyage_Fare_Model();
            $subvoyage_fare->delete(array("passenger_fare_id" => $this->id));
			
			// Delete all passenger fare conditions
			$passenger_fare_conditions = new Passenger_Fare_Conditions_Model();
            $passenger_fare_conditions->delete(array("passenger_fare_id" => $this->id));
			
			// Set confirmation message
			if($passenger_fare->delete())
				$this->session->set_flashdata('confirm', 'Successfully deleted '.ucwords($passenger_fare->passenger_fare));
			else
				$this->session->set_flashdata('note', "Error deleting passenger fare");
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return	void
	 */
	private function _validate()
	{
		if ($this->uri->rsegment(2) == 'add'){
			$this->form_validation->set_rules('passenger_fare', 'passenger fare code', 'required|trim|is_unique[passenger_fare.passenger_fare]');
			$this->form_validation->set_rules('subvoyage', 'subvoyage', 'required');
			$this->form_validation->set_rules('accommodation_id', 'accommodation type', 'required|trim');
			$this->form_validation->set_rules('voyage_id', 'voyage', 'required|trim');
		}
			
		$this->form_validation->set_rules('seat_limit', 'seat limit', 'required|trim|numeric');
		$this->form_validation->set_rules('regular_fare', 'regular fare', 'required|trim');
		$this->form_validation->set_rules('rule_set_id', 'rule set', 'required|trim');
		$this->form_validation->set_rules('default_fare', 'default fare', 'trim');
		$this->form_validation->set_rules('return_fare', 'return fare', 'trim');
		$this->form_validation->set_rules('require_id', 'require id', 'trim');
		$this->form_validation->set_rules('origin_id', 'origin_id id', 'trim');
		$this->form_validation->set_rules('destination_id', 'destination_id', 'trim');
		$this->form_validation->set_rules('enabled', 'status', 'required');
		$this->form_validation->set_rules('points_earning', 'points earning', 'trim');
		$this->form_validation->set_rules('enabled', 'status', 'required');

		if($this->tools->getPost('conditions')){
			// Conditions
			foreach($this->tools->getPost('conditions') as $ckey => $cvalue){
				$this->form_validation->set_rules('conditions['.$ckey."]", 'Condition', 'required');				

				foreach($this->for_validate_conditions() as $dkey => $dvalue){
					if($ckey == $dkey){
						// Type name
						$fcname = ucwords(str_replace("_", " ", $dvalue));
						// First Validation Rule
						$this->form_validation->set_rules('fare_conditions['.$dvalue."]", $fcname, 'required');
						
						if($ckey == 1 || $ckey == 3 || $ckey == 4 || $ckey == 5){
							switch($ckey){
								case 1:
									$dvalue = 'age_bracket_to';
									break;
								case 3:
									$dvalue = 'max_advance_booking';
									break;
								case 4:
									$dvalue = 'booking_period_to';
									break;
								case 5:
									$dvalue = 'travel_period_to';
									break;							
							}
							
							// Second Validation Rule
							$this->form_validation->set_rules('fare_conditions['.$dvalue."]", $fcname, 'required');
						} 	
					}	
				}
			}			
		}
                             
		if ($this->uri->rsegment(2) == 'add')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then add Passenger Fare
	 *
	 * @access	private
	 * @return	void
	 */
	private function _addInfo()
	{
		// Initialize
		$subvoyage_fare = new Subvoyage_Fare_Model();
		$passenger_fare_conditions = new Passenger_Fare_Conditions_Model();
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$passenger_fare = new Passenger_Fare_Model();
			
			parent::copyFromPost($passenger_fare, 'id_passenger_fare');
                        
			// Check for successful insert
			if ($passenger_fare->add())
			{				
				// Add data
				self::add_update($passenger_fare);
				
				// Log
				parent::logThis($passenger_fare->id, 'Successfully added Passenger Fare');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added Passenger Fare');
				$this->session->set_flashdata('id', $passenger_fare->id);	
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving Passenger Fare');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then edit Passenger Fare
	 *
	 * @access	private
	 * @return	void
	 */
	private function _editInfo()
	{
		// Initialize
		$subvoyage_fare = new Subvoyage_Fare_Model();
		$passenger_fare = new Passenger_Fare_Model($this->id);
		$passenger_fare_conditions = new Passenger_Fare_Conditions_Model();
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			parent::copyFromPost($passenger_fare, 'id_passenger_fare');
				
			// Check for successful update
			if ($passenger_fare->update())
			{
				// Default Fare
				$default_fare = $this->tools->getPost('default_fare') ? 1 : 0;
			
				// Re-update
				$passenger_fare->update(array("id_passenger_fare" => $passenger_fare->id), array("default_fare" => $default_fare));
				
				// Delete data
				$data = array("passenger_fare_id" => $passenger_fare->id);
				$subvoyage_fare->delete($data);	
				$passenger_fare_conditions->delete($data);
			
				// Add data
				self::add_update($passenger_fare);
				
				parent::logThis($passenger_fare->id, 'Successfully updated Passenger Fare');
                                        
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated Passenger Fare');
				$this->session->set_flashdata('id', $passenger_fare->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating Passenger Fare');
			}
			
			redirect(admin_url($this->classname));
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * Get the posted subvoyages with/without amount
	 *
	 * @access	public
	 * @return	array
	 */
	protected function getPostedSubvoyage()
	{
		// Initialize
		$objSubvoyage = array();

		// Get Posted data for ajax returns
		if($this->tools->getPost('subvoyages'))
			foreach($this->tools->getPost('subvoyages') as $key => $value)
				$objSubvoyage[$key] = (object) $value;

		// Return
		return $objSubvoyage;
	}	
	
	// --------------------------------------------------------------------
	
	/*
	 * Add or Update Passenger Fare and conditions
	 *
	 * @access	public
	 * @return	void
	 */
	protected function add_update($passenger_fare)
	{
		// Initialize
		$subvoyage_fare = new Subvoyage_Fare_Model();
		$passenger_fare_conditions = new Passenger_Fare_Conditions_Model();
		
		// Posted fare conditions
		$fconditions = $this->tools->getPost('fare_conditions');
		
		// Passenger Fare Conditions 
		foreach($this->tools->getPost('conditions') as $ckey => $cvalue){
		
			// Initialize and add data
			$pfare = array("passenger_fare_id" => $passenger_fare->id, "conditions_id" => $cvalue);
			
			// Passenger Fare
			switch($cvalue){
				case 1:
					$fare_conditions["age_bracket_from"] = $fconditions["age_bracket_from"];
					$fare_conditions["age_bracket_to"] = $fconditions["age_bracket_to"];
					break;
				case 2:
					$fare_conditions["outlet_id"] = $fconditions["outlet_id"];
					break;
				case 3:
					$fare_conditions["min_advance_booking"] = $fconditions["min_advance_booking"];
					$fare_conditions["max_advance_booking"] = $fconditions["max_advance_booking"];
					break;
				case 4:
					$fare_conditions["booking_period_from"] = $fconditions["booking_period_from"];
					$fare_conditions["booking_period_to"] = $fconditions["booking_period_to"];
					break;
				case 5:
					$fare_conditions["travel_period_from"] = $fconditions["travel_period_from"];
					$fare_conditions["travel_period_to"] = $fconditions["travel_period_to"];
					break;
				case 6:
					$fare_conditions["travel_days"] = $fconditions["travel_days"];
					break;
				case 7:
					$fare_conditions["max_leg_interval"] = $fconditions["max_leg_interval"];
					break;
			}
			
			// Passenger Fare
			$passenger_fare_conditions->add(array_merge($pfare, $fare_conditions));
			
			// Unset
			unset($fare_conditions);
		}

		// Subvoyage
		foreach($this->tools->getPost('subvoyages') as $vkey => $vvalue){
			$legs = $subvoyage_fare->add(array("passenger_fare_id" => $passenger_fare->id, "subvoyage_id" => $vvalue["id_subvoyage"], "amount" => $vvalue["amount"]));
		}
		
	}
	
	// --------------------------------------------------------------------

	/*
	 * Passenger Fare and conditions
	 *
	 * @access	public
	 * @return	void
	 */	 
	protected function join_fare_conditions($passenger_fare)
	{
		// Define and Initialize
		$voyage						= $this->voyage->displaylist();
		$rule_set 					= $this->rule_set->displaylist(array("enabled" => 1));
		$accommodation				= $this->accommodation->displaylist(array("enabled" => 1));
		$passenger_fare->voyage		= self::re_order($passenger_fare->voyage_id, 'id_voyage', $voyage);
		$passenger_fare->rule_set 	= self::re_order($passenger_fare->rule_set_id, 'id_rule_set', $rule_set);
		$passenger_fare->outlet		= $this->outlet->displaylist(array("u.enabled" => 1));
		$passenger_fare->voyage_id	= $passenger_fare->voyage_id;
		$passenger_fare->conditions	= self::_join_conditions();
		$passenger_fare->rule_set_id = $passenger_fare->rule_set_id;
		$passenger_fare->subvoyage_fare = $this->subvoyage_fare->displaylist(array("passenger_fare_id" => $passenger_fare->id));
		$passenger_fare->subvoyage_id 	= isset($passenger_fare->subvoyage_fare[0]) ? $passenger_fare->subvoyage_fare[0]->subvoyage_id : FALSE;
		$passenger_fare->accommodation	= self::re_order($passenger_fare->accommodation_id, 'id_accommodation', $accommodation);	
		$passenger_fare->accommodation_id = $passenger_fare->accommodation_id;
		
		// Get outlet
		$outlet_id =  $this->passenger_fare_conditions->displayList(array("passenger_fare_id" => $passenger_fare->id, "conditions_id" => 2));
		$passenger_fare->outlet_id = isset($outlet_id[0]->outlet_id) ? $outlet_id[0]->outlet_id : FALSE;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Re-oder Object Array
	 *
	 * @access	public
	 * @return		object
	 */
	protected function re_order($id, $index, $obj)
	{
		$object = new stdClass();
		
		foreach($obj as $key => $value){
			if($id == $value->$index){
				$object = $value;
				unset($obj[$key]);
			}
		}
		
		// Re-assign and initialize
		$objArray = $obj;
		array_unshift($objArray, $object);
		
		return $objArray;
		
	}
	
	 // --------------------------------------------------------------------
	
	/*
	 * Join Fare and conditions
	 *
	 * @access	protected
	 * @return	array
	 */
	private function _join_conditions()
	{
		// Initialize
		$fare_conditions = new Passenger_Fare_Conditions_Model();
	
		// Search
		$fare_conditions = $this->passenger_fare_conditions->displaylist(array("passenger_fare_id" => $this->id));
		
		// Fare and conditions
		foreach($this->condition->displayList() as $rkey => $rvalue){
			
			$fconditions[$rkey] = $rvalue;
			
			foreach($fare_conditions as $tkey => $tvalue){
				if($rvalue->id_conditions == $tvalue->conditions_id)
					$fconditions[$rkey] = $tvalue;
			}
			
			$fconditions[$rkey]->conditions = $rvalue->conditions;
		}
		
		return $fconditions;
	}
	
	 // --------------------------------------------------------------------
	
	/*
	 * Get Fare conditions fields
	 *
	 * @access	protected
	 * @return	array
	 */
	protected function fare_conditions_fields()
	{		
		// Initialize
		return $this->passenger_fare_conditions->_fields(NULL);
	}
	
	
	 // --------------------------------------------------------------------
	
	/*
	 * Get Fare conditions fields for validations
	 *
	 * @access	protected
	 * @return	array
	 */
	protected function for_validate_conditions()
	{		
		// Initialize
		$fare_conditions = self::fare_conditions_fields();
		
		// Unset PK, FK, Age Bracket To, Max Advance Booking, Booking Period To, Travel Period To
		unset(	$fare_conditions->{'1'}, 
					$fare_conditions->{'2'}, 
					$fare_conditions->{'4'}, 
					$fare_conditions->{'7'}, 
					$fare_conditions->{'9'}, 
					$fare_conditions->{'11'});	

		// Return
		return array_combine(range(1, count((array) $fare_conditions)), array_values((array) $fare_conditions));	

	}
	
	// ===================================================================
	//    AJAX FUNCTION
	// ===================================================================
	
	protected function ajax()
	{
		// Initialize
		$obj = $this->_cast_to_obj($this->tools->getPost());
		
		// Generate return
		switch($obj->module){
		
			// Return the options (Subvoyage Legs)
			case 'voyage':
				self::printSubvoyages($obj);
				break;	
			
			// Print the subvoyages details
			case 'subvoyage':
				self::printSubvoyageDetails($obj);
				break;

			// Return the data of selected subvoyage
			case 'dtSubvoyage':
				self::returnSubvoyageDetails($obj);
				break;
			
			// Adds Legs
			case 'addLegs':
				self::printAddLegs($obj);
				break;

			// Retuns the data of added Legs
			case 'dtAddLegs':
				self::returnAddLegs($obj);
				break;
				
			// Removes the legs
			case 'removeLegs':
				self::removeSubvoyage($obj);
				break;

			// Returns the details of the end legs
			case 'dtRemoveLegs':
				self::returnEndLegs($obj);
				break;
				
			case 'registerRemove':
				self::registerRemove($obj);
				break;

			case 'returnVoyages':
				self::getReturnVoyages($obj);
				break;
		}
				
	}

	// --------------------------------------------------------------------
	
	/*
	 * Set Voyage
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function voyage_id($obj)
	{
		return array("voyage_id" => $obj->voyage_id);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get Subvoyages
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function getSubVoyages($arr)
	{
		// Get subvoyages
		$subvoyage = $this->subvoyage->displaylist($arr);
		
		// Subvoyages
		foreach($subvoyage as $skey => $svalue)
			$subvoyages[$svalue->id_subvoyage] = $svalue;
			
		return $subvoyages;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Print Subvoyages in select option
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function printSubvoyages($obj)
	{
		// Get Subvoyages
		$subvoyage = $this->getSubVoyages($this->voyage_id($obj));
		
		// Subvoyage origin
		foreach($subvoyage as $key => $value)
			$return["opSubvoyage"][$value->id_subvoyage] = "<option value='".$value->id_subvoyage."'>".$value->origin."</option>";

		if(!$obj->voyage_id)
			$return["opSubvoyage"] = array();
		
		// Add Please Select
		array_unshift($return["opSubvoyage"], "<option value=''>Please Select</option>");
		
		// Print
		print_r($return["opSubvoyage"]);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get Subvoyage Details
	 *
	 * @access	protected
	 * @return	void
	 */
	protected function getSubVoyageDetails($obj)
	{
		// Initialize
		$data = array();
		
		// Get subvoyages
		$subvoyages = $this->getSubVoyages($this->voyage_id($obj));
		
		// Get only the subvoyage selected
		foreach($subvoyages as $skey => $svalue)
			if($svalue->id_subvoyage == $obj->subvoyage_id)
				$subvoyage[$svalue->id_subvoyage] = $svalue;
		
		if($obj->subvoyage_id){
		
			// Get the order of selected subvoyage_id
			$selected_leg_order = array_search($obj->subvoyage_id, array_keys($subvoyages));
			
			// Validate
			$subvoyage = !empty($subvoyage) ? $subvoyage : FALSE;
			
			// Initialize array (leg order + 1 to bypass -Please Select-)
			$data = array("subvoyages" => $subvoyage, "key" => $obj->subvoyage_id, "leg_count" => count($subvoyages), "leg_order" => ($selected_leg_order + 1));
			
		}

		return $data;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Return only the Subvoyage Details
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function returnSubvoyageDetails($obj)
	{
		// Initialize
		$data = array();
		
		// Get subvoyage details
		if($obj->subvoyage_id)
			$data = $this->getSubVoyageDetails($obj);

		// Print
		$this->encode($data);
		
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Print Subvoyage Details
	 *
	 * @access	protected
	 * @return	html parse
	 */
	protected function printSubvoyageDetails($obj)
	{
		// Get Subvoyage Details
		$data = $this->getSubVoyageDetails($obj);
		
		// Display template
		parent::displayTemplate(admin_dir('passenger_fare/form/add_subvoyages'), $data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Add Subvoyage/s
	 *
	 * @access	protected
	 * @return	array
	 */
	protected function addSubvoyage($obj)
	{
		// Initialize
		$data = array();

		if($obj->last_subvoyage_id != 'false' && !empty($obj->voyage_id)){

			// Get Subvoyages
			$subvoyages = $this->getSubVoyages($this->voyage_id($obj));

			// Get the next item
			$next_subvoyage = $this->getNextItem($subvoyages, $obj->last_subvoyage_id);
			
			// Get the key
			$key = !empty($next_subvoyage) ? key($next_subvoyage) : FALSE;

			// Get the details of the subvoyage
			$sub_id = isset($obj->subvoyage_id) ? $obj->subvoyage_id : FALSE;

			// Get the corresponding subvoyage
			$subvoyage = $this->getSubVoyages(array("id_subvoyage" => $sub_id));

			// Get the origin
			$origin = isset($subvoyage[$sub_id]->origin_id) ? $subvoyage[$sub_id]->origin_id : FALSE;

			// Get the temp destination
			$destination = isset($next_subvoyage[$key]->destination_id) ? $next_subvoyage[$key]->destination_id : FALSE;

			// Initialize array
			$data = array("subvoyages" => $next_subvoyage, "key" => $key, "origin_id" => $origin, "destination_id" => $destination);	
		}

		return $data;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Return only the details of Legs
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function returnAddLegs($obj)
	{
		// Initialize
		$data = array();
		
		if($obj->last_subvoyage_id)
			$data = $this->addSubvoyage($obj);

		// Print
		$this->encode($data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Print Subvoyage/s
	 *
	 * @access	protected
	 * @return	html parse
	 */
	protected function printAddLegs($obj)
	{
		// Add Subvoyage
		$data = $this->addSubvoyage($obj);

		// Display template
		if(isset($data["key"]))		
			parent::displayTemplate(admin_dir('passenger_fare/form/add_subvoyages'), $data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Return only the details of Legs
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function returnEndLegs($obj)
	{
		// Initialize
		$data = array();

		if($obj->voyage_id){

			// Subvoyage id
			$sub_id = $obj->last_subvoyage_id;

			// Get the corresponding subvoyage
			$subvoyage = $this->getSubVoyages(array("id_subvoyage" => $sub_id));

			// Get the temp destination
			$destination = isset($subvoyage[$sub_id]->destination_id) ? $subvoyage[$sub_id]->destination_id : FALSE;
		
			// Initialize array (No updating of origin, since the last leg is the only one removed)
			$data = array("subvoyages" => $subvoyage, "origin_id" => FALSE, "destination_id" => $destination);	
		}

		// Print
		$this->encode($data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Print Subvoyage Details
	 *
	 * @access	protected
	 * @return	html parse
	 */
	protected function getReturnVoyages($obj)
	{
		// Initialize
		$origin = $destination = NULL;
		
		// Get Subvoyage Details
		$subvoyage = $this->subvoyage->displayList(array("id_subvoyage" => $obj->subvoyage_id));

		// Get origin and destination for the voyages
		foreach($subvoyage as $skey => $svalue){
			$origin = $svalue->destination_id;
			$destination = $svalue->origin_id;
		}

		// Get the voyages
		$voyages = $this->subvoyage->displayList(array("u.origin_id" => $origin, "u.destination_id" => $destination));

		// Display template
		parent::displayTemplate(admin_dir('passenger_fare/form/return_voyage'), array("return_voyage" => $voyages));
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Remove Subvoyage/s
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function removeSubvoyage($obj)
	{		
		// Unset session
		$sub_unset = $this->session->unset_userdata('last_subvoyage_id');
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get the next value based on known key
	 *
	 * @access	protected
	 * @return	array
	 */
	public function registerRemove($obj)
	{

		
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get the next value based on known key
	 *
	 * @access	protected
	 * @return	array
	 */
	public function getNextItem($array, $key)
	{
		// Get the array keys
		$keys = array_keys($array);

		// Validate and search
		if ( (false !== ($p = array_search($key, $keys))) && ($p < count($keys) - 1) )
		{
			return array($keys[++$p] => $array[$keys[$p]]);
		}
		else
		{
			return false;
		}
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Cast array to object
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function _cast_to_obj($arr)
	{
		// Cast to object
		return (object) $arr;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Encode object
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function encode($data)
	{
		echo json_encode($data);
	}
   
}

/* End of file passenger_fare.php */
/* Location: ./application/modules_core/adminpanel/controllers/passenger_fare/passenger_fare.php */