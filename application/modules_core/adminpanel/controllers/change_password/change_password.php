<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Change Password Class
|--------------------------------------------------------------------------
|
| Delegates functions for adminsitration change password records
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Change_Password extends Admin_Core
{
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
               	$this->classname = strtolower(get_class());

		parent::__construct();
                
		// Check if admin already change password
		if (!$this->session->get('admin','is_new'))
			redirect(admin_url('profile'));

		// Load the models directly involve with the class 
		$this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('user/user_model'));
                $this->load->model(admin_dir('user/user_account_model'));
                $this->load->model(admin_dir('user/user_password_model'));
                $this->load->model(admin_dir('configuration/configuration_model'));
                $this->load->model(admin_dir('reset_password/reset_password_model'));
                
                $this->user_profile = new Profile_Model();
                $this->user = new User_Model();
                $this->user_account = new User_Account_Model();
                $this->user_password = new User_Password_Model();
                $this->configuration = new Configuration_Model();
                $this->reset_password = new Reset_Password_Model();
	}
	
	// ------------------------------------------------------------------------
	
	/*
	 * Shows the change password form for admins
	 *
	 * @access		public
	 * @return		void
	 */
	public function index()
	{
		// form validation
		self::_validate();
                                
		$data = array(	'header' => parent::getTemplate(admin_dir('change_password/header')),
                                'footer' => parent::getTemplate(admin_dir('change_password/footer')),
                        );
                
		$this->load->view(admin_dir('change_password/change_password'), $data);
	}

        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{	
                $uppercase  = $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_UPPERCASE'));
                $lowercase  = $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_LOWERCASE'));
                $number     = $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_NUMBER'));
                $spl_char   = $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_SPL_CHAR'));
                $max        = $this->configuration->getValue('value', array('configuration' => 'MAX_PASSWORD_CHAR'));
                $min        = $this->configuration->getValue('value', array('configuration' => 'MIN_PASSWORD_CHAR'));
                
                $this->form_validation->set_rules('cur_password', 'Current Password', 'required|trim');
		$this->form_validation->set_rules('password', 'New Password', 'required|min_length['.$min.']|max_length['.$max.']|password_check['.$uppercase.','.$lowercase.','.$number.','.$spl_char.']|matches[re_password]');
		$this->form_validation->set_rules('re_password', 'Confirm New Password', '');
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$user_id = $this->session->get('admin','admin_id');
			$user = new User_Model($user_id);
                        
                        // get user account details
                        $user->user_account = new User_Account_Model($user->id_user);
                
                        // check current password
                        if($user->password <> hash('sha256', $this->tools->getPost('cur_password').$user->salt))
                        {
                            // redirect to change password
                            $this->session->set_flashdata('note', 'Incorrect current password. Please try again!');
                            redirect(admin_url($this->classname));
                        }
                        
                        // check password history
                        $password_history = $this->user_password->displayList(array('user_id' => $user->id_user),array('id_user_password' => 'DESC'));
                        
                        $password_count = $this->configuration->getValue('value', array('configuration' => 'NUM_PASSWORD_CHANGE'));
                        
                        if(count($password_history) > $password_count)
                        {
                            $diff = count($password_history) - $password_count;
                            // cut array
                            array_splice($password_history, $diff);
                        }
                        
                        // loop password history
                        foreach($password_history as $p)
                        {
                            // check if user use current password
                            if($p->password == hash('sha256', $this->tools->getPost('password').$p->salt))
                            {
                                // redirect to change password
                                $this->session->set_flashdata('note', 'You cannot use your previous password as your new password');
                                redirect(admin_url($this->classname));
                            }
                        }
                        
			// handle brute force attacks
			sleep(1);
                        
                        //$email_data = array(
                        //                        'user'          => $user,
                        //                        'employee_name' => $user->user_account->lastname.', '.$user->user_account->firstname,
                        //                        'username'      => $user->employee_num,
                        //                    );
                        
                        // set email parameter
                        //$subject = 'Change Password';
                        //$body = parent::getTemplate(admin_dir('email_template/change_password'),$email_data);
                        
                        // send email
                        //parent::sendMail($body,$subject,$user->user_account->email_address);
                        
                        // update user information
                        $secure_token = substr(sha1(microtime()),4,30);
                        $salt = $this->misc->generate_code(8);
                        $password = hash('sha256', $this->tools->getPost('password').$salt);
                        
                        $user->update(array(),array(
                                                        'secure_token'      => $secure_token,
                                                        'salt'              => $salt,
                                                        'password'          => $password,
                                                        'is_new'            => 0,
                                                        'num_failed_login'  => 0,
                                                        'password_generated'=> date("Y-m-d"),
                                                        'last_login'        => date("Y-m-d H:i:s"),
                                                    ));
                        //insert user password
                        $this->user_password->add(array(
                                                            'user_id'   => $user->id,
                                                            'password'  => $password,
                                                            'salt'      => $salt,
                                                        ));
                        
                        // delete existing record
                        $this->reset_password->delete(array('user_id' => $user->id));
                            
                        $this->session->unset_userdata('admin');
		
                        $this->session->set_flashdata('confirm', 'Password successfully changed');
                        redirect(admin_url('login'));
                
                        // Redirect to logout
			//redirect(admin_url('logout'));
		}
	}
}

/* End of file login.php */
/* Location: ./application/modules_core/adminpanel/login/login.php */