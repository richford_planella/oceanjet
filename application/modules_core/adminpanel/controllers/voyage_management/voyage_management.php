<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Outlet Class
  |--------------------------------------------------------------------------
  |
  | Outlet Content Management
  |
  | @category Controller
  | @author       Kenneth Bahia
 */

class Voyage_Management extends Admin_Core
{
    // ------------------------------------------------------------------------

    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());

        parent::__construct();

        $this->load->model(admin_dir('voyage/Voyage_Model'));
        $this->load->model(admin_dir('voyage_management/Voyage_Management_Model'));
        $this->load->model(admin_dir('subvoyage_management/Subvoyage_Management_Model'));
        $this->load->model(admin_dir('vessel/Vessel_Model'));
        $this->load->model(admin_dir('subvoyage_management_status/Subvoyage_Management_Status_Model'));
        $this->load->model(admin_dir('subvoyage/Subvoyage_Model'));

        $this->voyage = new Voyage_Model();
        $this->voyage_management = new Voyage_Management_Model();
        $this->subvoyage_management = new Subvoyage_Management_Model();
        $this->vessel = new Vessel_Model();
        $this->subvoyage_management_status = new Subvoyage_Management_Status_Model();
        $this->subvoyage = new Subvoyage_Model();

        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }

    // --------------------------------------------------------------------

    /*
     * Display Outlet Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {
        // Get all outlets
        $voyage_management = $this->voyage_management->displaylist();

        $voyage_schedule = array();

        foreach ($voyage_management as $key => $value)
        {
            $id_voyage_management = $value->id_voyage_management;

            $voyage_schedule[$key][$id_voyage_management] = $id_voyage_management;

            $subvoyage_management = $this->subvoyage_management->displayList(array("voyage_management_id" => $id_voyage_management));

            $total_travel_time = new DateTime("00:00");
            $total_travel_time_clone = clone $total_travel_time;
            $total_departure_delay_time = 0;

            if (count($subvoyage_management) > 0)
            {
                $voyage_management[$key]->departure_date = $subvoyage_management[0]->departure_date;
            }

            foreach ($subvoyage_management as $k => $v)
            {
                $ETD = new DateTime($v->departure_date . " " . $v->ETD);
                $ETA = new DateTime($v->departure_date . " " . $v->ETA);

                if ($ETD > $ETA)
                {
                    $ETA->modify("+1 day");
                }

                $total_travel_time->add($ETD->diff($ETA));
                $total_departure_delay_time += $v->departure_delay_time;
            }

            $td_format = $total_travel_time->format("H:i:s");

            $voyage_management[$key]->travel_time = $td_format;
            $voyage_management[$key]->departure_delay_time = $total_departure_delay_time;
        }


        // Initialize data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'Voyage Schedule Masterlist')),
            'footer' => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage-management.js'), js_dir('dataTables.filter.range.js')))),
            'voyage_management' => $voyage_management,
            'subvoyage_management_status' => $this->subvoyage_management_status->displayList(),
            'voyage_list' => $this->voyage->displayList()
        );

        parent::displayTemplate(admin_dir('voyage_management/voyage_management'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Delete voyage management
     *
     * @access      public
     * @return      void
     */
    public function delete()
    {
        // get voyage management
        $voyage_management = new Voyage_Management_Model($this->id);

        // Check if a record exists
        $voyage_management->redirectIfEmpty(admin_url($this->classname));

        $voyage_id = $voyage_management->voyage_id;

        $subvoyages = new Subvoyage_Model();
        $subvoyage = $subvoyages->displayList(array("voyage_id" => $voyage_id));

        $subvoyage_array = array();
        foreach ($subvoyage as $key => $value)
        {
            $subvoyage_array[] = $value->id_subvoyage;
        }

        // Check if it is already in use
        if ($voyage_management->countFromBooking($subvoyage_array) > 0)
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Cannot delete. Voyage management was already used in the system.');
        } else
        {
            //Delete voyage
            if ($voyage_management->delete())
                $this->session->set_flashdata('confirm', 'Voyage management has been deleted!');
            else
                $this->session->set_flashdata('note', 'Error deleting voyage management');
        }

        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------

    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $voyage_management = new Voyage_Management_Model($this->id);

        // Check if a record exists
        $voyage_management->redirectIfEmpty(admin_url($this->classname));

        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'View Voyage Schedule')),
            'footer' => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage-management.js')))),
            "subvoyages" => $this->subvoyage_management->displayList(array("u.voyage_management_id" => $this->id)),
            'vessel_list' => $this->vessel->displayList(),
            'subvoyage_management_status_list' => $this->subvoyage_management_status->displayList(),
            'voyage' => $this->voyage_management->displayList(array("id_voyage_management" => $this->id))[0]
        );

        parent::displayTemplate(admin_dir('voyage_management/form/voyage_management'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Toggle voyage management status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $voyage_management = new Voyage_Management_Model($this->id);

        // Check if a record exists
        $voyage_management->redirectIfEmpty(admin_url($this->classname));

        // Check for successful toggle
        if ($voyage_management->toggleStatus())
        {
            // Set confirmation message
            if ($voyage_management->enabled)
                $this->session->set_flashdata('confirm', $voyage_management->voyage_management . ' has been de-activated!');
            else
                $this->session->set_flashdata('confirm', $voyage_management->voyage_management . ' has been re-activated!');
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating ' . $voyage_management->voyage_management . '.');
        }

        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------

    /*
     * Update Sched status
     *
     * @access  public
     * @return      void
     */
    public function updateSchedStatus()
    {
        if (!empty($this->tools->getPost()))
        {
            $subvoyage_management_status_id = $this->tools->getPost("status");
            foreach ($this->tools->getPost("voyage_management_array") as $key => $value)
            {
                $subvoyage_management = new Subvoyage_Management_Model();

                if ($subvoyage_management_status_id == 7)
                { //if status is unblock
                    $result = $subvoyage_management->displayList(array("voyage_management_id" => $value, "subvoyage_management_status_id" => 5)); //select only voyages with block status
                } else
                {
                    $result = $subvoyage_management->displayList(array("voyage_management_id" => $value));
                }

                foreach ($result as $k => $v)
                {
                    $subvoyage_management_n = new Subvoyage_Management_Model($v->id_subvoyage_management);
                    $subvoyage_management_n->subvoyage_management_status_id = $subvoyage_management_status_id;
                    $subvoyage_management_n->update();
                }
            }

            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating voyage management.');

            $data = array
                (
                "url" => admin_url($this->classname)
            );

            echo json_encode($data);
        }
    }

    // --------------------------------------------------------------------

    /*
     * Select default vessel of voyage
     *
     * @access  public
     * @return      void
     */
    public function subvoyages()
    {
        if (!empty($this->tools->getPost()))
        {
            $voyage_id = $this->tools->getPost("voyage_id");

            $vessel = new Vessel_Model();
            $subvoyage = new Subvoyage_Model();

            $data = array
                (
                "vessel_list" => $vessel->displayList(),
                "subvoyages" => $subvoyage->displayList(array("voyage_id" => $voyage_id))
            );

            echo json_encode($data);
        }
    }

    // --------------------------------------------------------------------

    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();
        
        $subvoyage = array();
        
        // check voyage post data
        if($this->tools->getPost('voyage_id'))
            $subvoyage = $this->subvoyage->displayList(array("voyage_id" => $this->tools->getPost('voyage_id')));
            
        
        // Initialize data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'Create Voyage Schedule')),
            'footer' => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage-management.js')))),
            'voyage_list' => $this->voyage->displayList(),
            'vessel_list' => $this->vessel->displayList(),
            'subvoyage' => $subvoyage,
        );

        parent::displayTemplate(admin_dir('voyage_management/form/add/voyage_management'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // Form validation
        self::_validate();

        // get user
        $voyage_management = new Voyage_Management_Model($this->id);

        // Check if a record exists
        $voyage_management->redirectIfEmpty(admin_url($this->classname));

        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'Edit Voyage Schedule')),
            'footer' => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage-management.js')))),
            "subvoyages" => $this->subvoyage_management->displayList(array("u.voyage_management_id" => $this->id)),
            'vessel_list' => $this->vessel->displayList(),
            'subvoyage_management_status_list' => $this->subvoyage_management_status->displayList(),
            'voyage' => $this->voyage_management->displayList(array("id_voyage_management" => $this->id))[0]
        );

        parent::displayTemplate(admin_dir('voyage_management/form/edit/voyage_management'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('voyage_id', 'voyage code', 'required');
        $this->form_validation->set_rules('date_from', 'date from', 'required');
        $this->form_validation->set_rules('date_to', 'date to', 'required');
        
        if($this->tools->getPost('vessel_id'))
            foreach($this->tools->getPost('vessel_id') as $key => $value)
                $this->form_validation->set_rules("vessel_id[$key]", 'vessel', 'required');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();

        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }

    // --------------------------------------------------------------------

    /*
     * Validate then add voyage schedule
     *
     * @return      void
     */
    private function _addInfo()
    {
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $dates = $this->misc->date_range($this->tools->getPost("date_from"),$this->tools->getPost("date_to"));
            
            echo '<pre>';
            print_r($dates);
            echo '</pre>';
            die;
            $voyage_management = new Voyage_Management_Model();

            parent::copyFromPost($voyage_management, 'id_voyage');
            $voyage_management->enabled = 1;

            // Check for successful insert
            if ($voyage_management->add())
            {
                parent::logThis($voyage_management->id, 'Successfully added voyage management');

                $subvoyage_array = $this->tools->getPost("subvoyage_id");
                $departure_date_array = $this->tools->getPost("departure_date");
                $vessel_array = $this->tools->getPost("vessel_id");
                $subvoyage_management_status_id = 1; //on dock by default

                foreach ($subvoyage_array as $key => $value)
                {
                    $subvoyage_id = $value;
                    $departure_date = $departure_date_array[$key];
                    $vessel_id = $vessel_array[$key];

                    $subvoyage_management = new Subvoyage_Management_Model();
                    $this->tools->setPost("voyage_management_id", $voyage_management->id);
                    $this->tools->setPost("subvoyage_id", $subvoyage_id);
                    $this->tools->setPost("departure_date", $departure_date);
                    $this->tools->setPost("vessel_id", $vessel_id);
                    $this->tools->setPost("subvoyage_management_status_id", $subvoyage_management_status_id);
                    $this->tools->setPost("departure_delay_time", 0);
                    $this->tools->setPost("remarks", "");

                    parent::copyFromPost($subvoyage_management, 'id_subvoyage_management');
                    $subvoyage_management->add();

                    parent::logThis($subvoyage_management->id, 'Successfully added subvoyage management');
                }

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added voyage management');
                $this->session->set_flashdata('id', $voyage_management->id);
            } else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving voyage management');
            }

            redirect(admin_url($this->classname));
        }
    }

    // --------------------------------------------------------------------

    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $voyage_management = new Voyage_Management_Model($this->id);

        // Check if a record exists
        $voyage_management->redirectIfEmpty(admin_url($this->classname));

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $id_subvoyage_management_array = $this->tools->getPost("id_subvoyage_management");
            $departure_date_array = $this->tools->getPost("departure_date");
            $vessel_array = $this->tools->getPost("vessel_id");

            $error_count = 0;

            foreach ($id_subvoyage_management_array as $key => $value)
            {
                $subvoyage_management = new Subvoyage_Management_Model($value);

                $this->tools->setPost("departure_date", $departure_date_array[$key]);
                $this->tools->setPost("vessel_id", $vessel_array[$key]);

                parent::copyFromPost($subvoyage_management, "id_subvoyage_management");

                if ($subvoyage_management->update())
                {
                    parent::logThis($subvoyage_management->id, "Successfully updated subvoyage management");
                } else
                {
                    $error_count++;
                }
            }

            if ($error_count == 0)
            {
                $this->session->set_flashdata('confirm', 'Successfully updated voyage management');
                $this->session->set_flashdata('id', $subvoyage_management->id);
            } else
            {
                $this->session->set_flashdata('note', 'Error in updating voyage management');
            }

            redirect(admin_url($this->classname));
        }
    }

}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */