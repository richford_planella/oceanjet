<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Class
|--------------------------------------------------------------------------
|
| Outlet Content Management
|
| @category Controller
| @author       Kenneth Bahia
*/
class Whitelist extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('whitelist/Whitelist_Model'));
        
        $this->whitelist = new Whitelist_Model();
                        
        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Outlet Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {               
        var_dump($this->tools->getPost());
        var_dump($this->tools->getGet());

        return;

        // Get all outlets
        $whitelist = $this->whitelist->displaylist();

        // Initialize data
        $data = array(
            'header'                   => Modules::run(admin_dir('header/call_header')),
            'footer'            => parent::getTemplate(admin_dir('footer')),
            'whitelist'        => $this->whitelist->displayList()
        );
        
        parent::displayTemplate(admin_dir('whitelist/whitelist/whitelist'), $data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $voyage_management = new Voyage_Management_Model($this->id);
                        
        // Check if a record exists
        $voyage_management->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'voyage_list'   => $this->voyage->displayList(),
            'voyage_management'        => $voyage_management,
            'voyage_management_status'  => $this->voyage_management_status->displayList(),
            'vessel_list'   => $this->vessel->displayList()
        );
        
        parent::displayTemplate(admin_dir('voyage_management/voyage_management/form/voyage_management'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $voyage = new Voyage_Model($this->id);
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($voyage->toggleStatus())
        {   
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating voyage.');
            $this->session->set_flashdata('id', $voyage->id_voyage);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating voyage.');
        }
        
        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------
    
    /*
     * Update Sched status
     *
     * @access  public
     * @return      void
     */
    public function updateSchedStatus()
    {
        foreach ($this->tools->getPost("ids") as $key => $value) {
            $voyage_management = new Voyage_Management_Model($value);

            $this->tools->setPost("status", $this->tools->getPost("status"));

            parent::copyFromPost($voyage_management, "");

            $voyage_management->update();
        }
    }

    // --------------------------------------------------------------------
    
    /*
     * Select default vessel of voyage
     *
     * @access  public
     * @return      void
     */
    public function findDefaultVessel()
    {
        if (!empty($this->tools->getPost())) {
            $voyage_id = $this->tools->getPost("voyage_id");

            $voyage = new Voyage_Model();

            echo json_encode(intval($voyage->getValue("vessel_id", array("id_voyage" => $voyage_id))));
        }
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header'                    => Modules::run(admin_dir('header/call_header')),
            'footer'                    => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage-management.js')))),
            'voyage_list'               => $this->voyage->displayList(),
            'vessel_list'               => $this->vessel->displayList(),
            'voyage_management_status'  => $this->voyage_management_status->displayList()
        );
            
        parent::displayTemplate(admin_dir('voyage_management/voyage_management/form/add/voyage_management'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get user
        $voyage_management = new Voyage_Management_Model();
        
        // Form validation
        self::_validate();

        $where_in = $this->tools->getGet();

        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage-management.js')))),
            'voyage_management'        => $voyage_management->displayList(array(),array('id_voyage_management' => 'ASC'),FALSE,$where_in),
            'voyage_list'   => $this->voyage->displayList(),
            'vessel_list'   => $this->vessel->displayList(),
            'voyage_management_status'  => $this->voyage_management_status->displayList()
        );

        parent::displayTemplate(admin_dir('voyage_management/voyage_management/form/edit/voyage_management'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('voyage_id', 'Voyage No', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('vessel_id', 'Vessel', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();
            
        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }

        // --------------------------------------------------------------------
    
    /*
     * Validate then add Outlet
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $voyage_management = new Voyage_Management_Model();
            
            parent::copyFromPost($voyage_management, 'id_voyage_management_status');
                        
            // Check for successful insert
            if ($voyage_management->add())
            {
                parent::logThis($voyage_management->id, 'Successfully added Voyage Management');

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added Voyage Management');
                $this->session->set_flashdata('id', $voyage_management->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving Voyage Management');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $voyage_management = new Voyage_Management_Model();

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($voyage_management, 'id_voyage_management');

            $id_voyage_management_array = $this->tools->getGet("id_voyage_management");
            $voyage_array = $this->tools->getPost("voyage_id");
            $date_array = $this->tools->getPost("date");
            $vessel_array = $this->tools->getPost("vessel_id");
            $status_array = $this->tools->getPost("status");

            $error_count = 0;

            foreach ($id_voyage_management_array as $key => $value) {
                $voyage_management = new Voyage_Management_Model($value);

                $this->tools->setPost("voyage_id", $voyage_array[$key]);
                $this->tools->setPost("date", $date_array[$key]);
                $this->tools->setPost("vessel_id", $vessel_array[$key]);
                $this->tools->setPost("status", $status_array[$key]);

                parent::copyFromPost($voyage_management, "");

                if ($voyage_management->update()) {
                    parent::logThis($voyage_management->id, "Successfully updated Voyage Management");
                } else {
                    $error_count++;
                }
            }

            if ($error_count == 0) {
                $this->session->set_flashdata('confirm', 'Successfully updated Voyage Management');
                $this->session->set_flashdata('id', $voyage_management->id);
            } else {
                $this->session->set_flashdata('note', 'Error in updating Voyage Management');
            }

            redirect(admin_url($this->classname));
        }
    }
   
}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */