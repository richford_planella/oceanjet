<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Attended baggage rate Class
|--------------------------------------------------------------------------
|
| Attended baggage rate Content Management
|
| @category Controller
| @author       Kenneth Bahia
*/
class Attended_Baggage extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('attended_baggage/Attended_Baggage_Model'));
        $this->load->model(admin_dir('port/Port_Model'));

        $this->attended_baggage = new Attended_Baggage_Model();
        $this->port = new Port_Model();
                        
        // Attended baggage rate id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Attended baggage rate Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        // Get all Attended baggage rates
        $attended_baggage = $this->attended_baggage->displaylist();
        
        // Initialize data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Attended Baggage Rate')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.attended_baggage.js')))),
            'attended_baggage'  => $attended_baggage,
        );
        
        parent::displayTemplate(admin_dir('attended_baggage/attended_baggage'), $data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Attended baggage rate Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get Attended baggage rate
        $attended_baggage = new Attended_Baggage_Model($this->id);
                        
        // Check if a record exists
        $attended_baggage->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'View Attended Baggage Rate')),
            'footer'            => parent::getTemplate(admin_dir('footer')),
            'attended_baggage'  => $attended_baggage,
            'port_list'         => $this->port->displayList()
        );
        
        parent::displayTemplate(admin_dir('attended_baggage/form/attended_baggage'),$data);
    }

        // --------------------------------------------------------------------
    
    /*
     * Delete Attended baggage rate
     *
     * @access      public
     * @return      void
     */
    public function delete()
    {
        // get Attended baggage rate
        $attended_baggage = new Attended_Baggage_Model($this->id);
        
        // Check if a record exists
        $attended_baggage->redirectIfEmpty(admin_url($this->classname));
        
        // Check if it is already in use
        if ($attended_baggage->countFromPassengerBaggageCheckin() > 0)
        {   
            // Set confirmation message
            $this->session->set_flashdata('note', 'Cannot delete. '.$attended_baggage->attended_baggage.' was already used in the system.');
        }
        else
        {
            //Delete Attended baggage rate
            if ($attended_baggage->delete())
                $this->session->set_flashdata('confirm', 'Successfully deleted '.$attended_baggage->attended_baggage);
            else
                $this->session->set_flashdata('note', 'Error deleting Attended baggage rate');
        }
        
        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle Attended baggage rate status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get Attended baggage rate
        $attended_baggage = new Attended_Baggage_Model($this->id);
        
        // Check if a record exists
        $attended_baggage->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($attended_baggage->toggleStatus())
        {   
            // Set confirmation message
            if($attended_baggage->enabled)
                $this->session->set_flashdata('confirm', $attended_baggage->attended_baggage.' has been de-activated!');
            else
                $this->session->set_flashdata('confirm', $attended_baggage->attended_baggage.' has been re-activated!');
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating '.$attended_baggage->attended_baggage.'.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Attended baggage rate
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Create Attended Baggage Rate')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.attended_baggage.js')))),
            'port_list'     => $this->port->displayList()
        );
            
        parent::displayTemplate(admin_dir('attended_baggage/form/add/attended_baggage'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Attended baggage rate
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get user
        $attended_baggage = new Attended_Baggage_Model($this->id);
        
        // Check if a record exists
        $attended_baggage->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit Attended Baggage Rate')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.attended_baggage.js')))),
            'attended_baggage'  => $attended_baggage,
            'port_list'         => $this->port->displayList()
        );
        
        parent::displayTemplate(admin_dir('attended_baggage/form/edit/attended_baggage'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('description', 'description', 'required|trim');
        $this->form_validation->set_rules('origin_id', 'origin', 'required|trim');
        $this->form_validation->set_rules('destination_id', 'destination', 'required|trim');
        $this->form_validation->set_rules('amount', 'amount', 'required|numeric|decimal');
        $this->form_validation->set_rules('unit_of_measurement', 'unit of measurement', 'required|trim');
        $this->form_validation->set_rules('enabled', 'status', 'required');
                             
        if ($this->uri->rsegment(2) == 'add') {
            $this->form_validation->set_rules('attended_baggage', 'attended baggage rate code', 'required|trim|is_unique[attended_baggage.attended_baggage]');
            self::_addInfo();
        }

        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Attended baggage rate
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $attended_baggage = new Attended_Baggage_Model();
            
            parent::copyFromPost($attended_baggage, 'id_attended_baggage');
            $attended_baggage->attended_baggage = strtoupper($attended_baggage->attended_baggage);
                        
            // Check for successful insert
            if ($attended_baggage->add())
            {
                parent::logThis($attended_baggage->id, 'Successfully added attended baggage rate');
                
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added attended baggage rate');
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving attended baggage rate');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit Attended baggage rate
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $attended_baggage = new Attended_Baggage_Model($this->id);

        if (trim($this->tools->getPost("attended_baggage")) == trim($attended_baggage->attended_baggage)) {
            $this->form_validation->set_rules('attended_baggage', 'attended baggage rate code', 'required|trim');
        } else {
            $this->form_validation->set_rules('attended_baggage', 'attended baggage rate code', 'required|trim|is_unique[attended_baggage.attended_baggage]');
        }

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($attended_baggage, 'id_attended_baggage');
            $attended_baggage->attended_baggage = strtoupper($attended_baggage->attended_baggage);
            
            // Check for successful update
            if ($attended_baggage->update())
            {                                   
                 parent::logThis($attended_baggage->id, 'Successfully updated attended baggage rate');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated attended baggage rate');
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating attended baggage rate');
            }
            
            redirect(admin_url($this->classname));
        }
    }
   
}

/* End of file attended_baggage.php */
/* Location: ./application/modules_core/adminpanel/controllers/attended_baggage/attended_baggage.php */