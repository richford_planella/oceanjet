<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Reservation Class
|--------------------------------------------------------------------------
|
| Modify permissions of reservation
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Reservation extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
                
                parent::__construct();
		
                $this->load->model(admin_dir('reservation/reservation_model'));
                $this->load->model(admin_dir('reservation/reservation_status_model'));
                $this->load->model(admin_dir('outlet/outlet_model'));
                $this->load->model(admin_dir('accommodation/accommodation_model'));
                $this->load->model(admin_dir('vessel/vessel_model'));
                $this->load->model(admin_dir('vessel/vessel_seats_model'));
                $this->load->model(admin_dir('voyage/voyage_model'));
                $this->load->model(admin_dir('subvoyage/subvoyage_model'));
                $this->load->model(admin_dir('voyage_management/voyage_management_model'));
                $this->load->model(admin_dir('subvoyage_management/subvoyage_management_model'));
                $this->load->model(admin_dir('voyage_management_status/voyage_management_status_model'));
                $this->load->model(admin_dir('passenger_fare/passenger_fare_model'));
                $this->load->model(admin_dir('port/port_model'));
                $this->load->model(admin_dir('trip_type/trip_type_model'));
                $this->load->model(admin_dir('booking/booking_model'));
                
                $this->reservation = new Reservation_Model();
                $this->reservation_status = new Reservation_Status_Model();
                $this->outlet = new Outlet_Model();
                $this->accommodation = new Accommodation_Model();
                $this->vessel = new Vessel_Model();
                $this->vessel_seats = new Vessel_Seats_Model();
                $this->voyage = new Voyage_Model();
                $this->subvoyage = new Subvoyage_Model();
                $this->voyage_management = new Voyage_Management_Model();
                 $this->subvoyage_management = new Subvoyage_Management_Model();
                $this->voyage_management_status = new Voyage_Management_Status_Model();
                $this->passenger_fare = new Passenger_Fare_Model();
                $this->trip_type = new Trip_Type_Model();
                $this->port = new Port_Model();
                $this->booking = new Booking_Model();
                                
                // reservation ID
		$this->id = $this->uri->rsegment(3);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display reservation list
	 *
	 * @access		public
	 * @return		void
	 */
        public function index()
        {                
                // get data
                $data = array(
                                'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Reservation Masterlist')),
                                'footer'            => parent::getTemplate(admin_dir('footer')),
                                'reservation'       => $this->reservation->displayList(),
                );
                
                parent::displayTemplate(admin_dir('reservation/reservation'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Add reservation
	 *
	 * @access		public
	 * @return		void
	 */
        public function add()
        {
                // Form validation
		self::_validate();
                                
                // get data
                $data = array(
                                'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Create Reservation')),
                                'footer'            => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.reservation-tools.js')))),
                                'outlet'            => $this->outlet->displayList(array('u.enabled' => 1)),
                                'port'              => $this->port->displayList(array('enabled' => 1)),
                                'trip_type'         => $this->trip_type->displayList(),
                                'accommodation'     => $this->accommodation->displayList(array('enabled' => 1)),
                );
                
                parent::displayTemplate(admin_dir('reservation/form/add/reservation'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{
//                $this->form_validation->set_rules('reservation', 'group name', 'required|trim|max_length[128]');
//                $this->form_validation->set_rules('origin_id', 'Origin', 'required|integer');
//		$this->form_validation->set_rules('destination_id', 'Destination', 'required|integer');
//                $this->form_validation->set_rules('accommodation_id', 'Accommodation', 'required|integer');
//                $this->form_validation->set_rules('quantity', 'Quantity', 'required|integer');
//                $this->form_validation->set_rules('evoucher_type', 'E-voucher Type', 'required|integer');
//                $this->form_validation->set_rules('evoucher_amount', 'Amount/Percent', 'required|decimal');
//             
//		if ($this->uri->rsegment(2) == 'add')
//                        self::_addInfo();
//			
//			
//		if ($this->uri->rsegment(2) == 'edit')
//			self::_editInfo();
	}
        
        // ===================================================================
        //    AJAX FUNCTION
        // ===================================================================
        
        // --------------------------------------------------------------------
	
	/*
	 * Function to get all voyage with departure date base on origin/destination
	 *
	 * @access		private
	 * @return		void
	 */
        public function displayDeparture()
        {
                if ( ! IS_AJAX)
                {
                        // Set confirmation message
                        $this->session->set_flashdata('error', 'Direct access forbidden');
                        redirect(admin_url($this->classname));
                }
                
                $voyage_management = $this->booking->get_voyages(array(
                                                            'origin_id' => $this->tools->getPost('origin_id'),
                                                            'destination_id' => $this->tools->getPost('destination_id'),
                                                            ));
                
                

                $data = array('voyage_management' => $voyage_management);

                parent::displayTemplate(admin_dir('reservation/form/voyage_management_list'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Function to get all voyage with departure date base on  return origin/destination
	 *
	 * @access		private
	 * @return		void
	 */
        public function displayReturn()
        {
                if ( ! IS_AJAX)
                {
                        // Set confirmation message
                        $this->session->set_flashdata('error', 'Direct access forbidden');
                        redirect(admin_url($this->classname));
                }
                
                $voyage_management = $this->booking->get_voyages(array(
                                                            'origin_id' => $this->tools->getPost('destination_id'),
                                                            'destination_id' => $this->tools->getPost('origin_id'),
                                                            ));
                
                

                $data = array(
                                'voyage_management' => $voyage_management,
                                'port'              => $this->port->displayList(array('enabled' => 1)),
                                'origin_id'         => $this->tools->getPost('origin_id'),
                                'destination_id'    => $this->tools->getPost('destination_id'),
                                
                    );

                parent::displayTemplate(admin_dir('reservation/form/return_voyage_management_list'),$data);
        }
}

/* End of file reservation.php */
/* Location: ./application/modules_core/adminpanel/controllers/reservation/reservation.php */