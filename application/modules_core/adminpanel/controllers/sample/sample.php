<?php

class Sample extends MY_Controller
{
    public function __construct()
    {
        // Load the models directly involve with the class 
        $this->load->model(admin_dir('user/profile_model'));
        $this->load->model(admin_dir('user/user_model'));
        $this->load->model(admin_dir('user/user_account_model'));

        $this->user_profile = new Profile_Model();
        $this->user = new User_Model();
        $this->user_account = new User_Account_Model();
        
        parent::__construct();
    }
    
    public function index()
    {
        $this->load->library('csvreader');
        $result = $this->csvreader->parse_file($this->configuration->getValue('value', array('configuration' => 'UPLOAD_DIR')).'\masterlist.csv');
        //$data['csvData'] = $result;
        //$this->load->view('view_csv', $data);
        print_r($result);
    }
}