<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Class
|--------------------------------------------------------------------------
|
| Outlet Content Management
|
| @category Controller
| @author       Ronald Meran
*/
class Eticket_Passenger extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();
        $this->load->model(admin_dir('eticket_passenger/eticket_passenger_model'));
        $this->load->model(admin_dir('booking/booking_model')); 
        $this->load->model(admin_dir('passenger/passenger_model'));
        $this->load->model(admin_dir('ticket_series/ticket_series_ref_model'));
        $this->load->model(admin_dir('passenger_fare/passenger_fare_model'));   
        $this->load->model(admin_dir('voyage/voyage_model'));
        $this->load->model(admin_dir('voyage_management/voyage_management_model'));   
        $this->load->model(admin_dir('accommodation/accommodation_model')); 
        $this->load->model(admin_dir('discount/discount_model'));
        $this->load->model(admin_dir('port/port_model'));  
        $this->load->model(admin_dir('subvoyage/subvoyage_model'));
        $this->load->model(admin_dir('subvoyage_management/subvoyage_management_model'));
        $this->load->model(admin_dir('passenger_fare/subvoyage_fare_model'));
        $this->load->model(admin_dir('accommodation/accommodation_model'));
        $this->load->model(admin_dir('user/user_account_model'));
        $this->load->model(admin_dir('third_party_outlet/debtor_outlet_model'));
        $this->load->model(admin_dir('third_party_outlet/debtor_transaction_history_model'));
        $this->load->model(admin_dir('third_party_outlet/debtor_management_model'));
        $this->load->model(admin_dir('vessel/vessel_model'));
        $this->load->model(admin_dir('discount/discount_model'));
        $this->load->model(admin_dir('payment_account/payment_account_model'));
        $this->load->model(admin_dir('payment_method/payment_method_model'));

        $this->accommodation = new Accommodation_Model();
        $this->booking = new Booking_Model();
        $this->voyage = new Voyage_Model();
        $this->voyage_management = new Voyage_Management_Model();
        $this->discount = new Discount_Model();
        $this->passenger_fare = new Passenger_Fare_Model();
        $this->port = new Port_Model();
        $this->subvoyage = new Subvoyage_Model();
        $this->subvoyage_management = new Subvoyage_Management_Model();
        $this->user_account = new User_Account_Model();
        $this->debtor_outlet = new Debtor_Outlet_Model();
        $this->debtor = new Debtor_Management_Model();
        $this->eticket = new Eticket_Passenger_Model();
        $this->vessel = new Vessel_Model();
        $this->discount = new Discount_Model();
        $this->passenger = new Passenger_Model();
        $this->payment_account = new Payment_Account_Model();
        $this->subvoyage_fare = new Subvoyage_Fare_Model();
        $this->payment_method = new Payment_Method_Model();
        $this->debtor_transaction_history = new Debtor_Transaction_History_Model();
        // $this->load->model(admin_dir('/Eticket_Model'));
                        
        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Outlet Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {   

        // initialise voyage information
        $voyage_info = array();
        // initialise per leg array
        $leg_array = array();
        $passenger_fare = array();
       
        if (count($this->tools->getPost()) > 0) {
            $origin_id = $this->tools->getPost('origin');
            $destination_id = $this->tools->getPost('destination');
            $voyage_management = $this->tools->getPost('voyage_management');
            $passenger_fare_id = $this->tools->getPost('passenger_fare_id');
            $accommodation_id = $this->tools->getPost('accommodation_type_id');
            $voyage_id = $this->tools->getPost('voy_id');
            //
            if (!empty($accommodation_id) && !empty($voyage_id)) {
                $passenger_fare = $this->passenger_fare->displayList(array("voyage_id"=>$voyage_id,"accommodation_id"=> $accommodation_id));    
            }
            // get voyage information
            $voyage_info = $this->booking->get_voyage_from_id($origin_id,$destination_id,$voyage_management);
            // get passenger fare
            
            //get subvoyage id
            foreach ($voyage_info as $sid) {
                $leg_array[] = $this->subvoyage_fare->displayList(array("subvoyage_id"=>$sid->id_subvoyage,"passenger_fare_id"=>$passenger_fare_id));
            }
        } 
        // echo '<pre>';
        // print_r($this->tools->getPost());
        // print_r($voyage_info);
        // echo '</pre>';
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Passenger E-Ticket Issuance')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.eticket-passenger.js')))),
            'port' => $this->port->displayList(),
            'accommodation' => $this->accommodation->displayList(),
            'discount' => $this->discount->displayList(),
            'payment_method' => $this->payment_method->displayList(),
            'voyage_details' => $voyage_info,
            'voyage_fare_details' => $leg_array,
            'voyage_passenger_fare' => $passenger_fare
        );
        
    // print_r($arTripDetails); 
        parent::displayTemplate(admin_dir('third_party_outlet/eticket_passenger/eticket_passenger'),$data);
    }
    

    // --------------------------------------------------------------------
    
    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $eticket_passenger = new Eticket_Passenger_Model($this->id);
        $ticket_series_info = new Ticket_Series_Ref_Model();                
        // Check if a record exists
        $eticket_passenger->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'eticket_passenger'        => $eticket_passenger->get_ticket_details($this->id),
            'port' => $this->port->displayList(),
            'ticket_series_info' => $ticket_series_info->displayList(),
            'accommodation' => $this->accommodation->displayList(),
            'payment_account' => $this->payment_account->displayList(),
            
            
        );
        
        parent::displayTemplate(admin_dir('third_party_outlet/eticket_passenger/form/eticket_passenger'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $eticket = new Eticket_Model($this->id);
        
        // Check if a record exists
        $eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($eticket->toggleStatus())
        {   
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating accommodation.');
            $this->session->set_flashdata('id', $accommodation->id);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating accommodation.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();    
       
        
                            
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Passenger E-Ticket Issuance')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.eticket-passenger.js')))),
            'port' => $this->port->displayList(),
            'accommodation' => $this->accommodation->displayList(),
            'discount' => $this->discount->displayList(),
            'voyage_details' => $this->tools->getPost()
            
          
            
        );
        

        parent::displayTemplate(admin_dir('third_party_outlet/eticket_passenger/form/add/eticket_passenger'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get user
        $eticket = new Eticket_Model($this->id);
        
        // Check if a record exists
        $eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'eticket'        => $eticket,
        );
        
        parent::displayTemplate(admin_dir('eticket/eticket/form/edit/eticket'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        // passenger details
        $this->form_validation->set_rules('firstname', 'First Name', 'required|trim');
        $this->form_validation->set_rules('middlename', 'Middle Name', 'required|trim');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim');
        $this->form_validation->set_rules('birthdate', 'Birthdate', 'required|trim');
        $this->form_validation->set_rules('contactno', 'Contact Number', 'required|trim');
        $this->form_validation->set_rules('id_no', 'ID Number', 'required|trim');
        $this->form_validation->set_rules('nationality', 'Nationality', 'required|trim');
        $this->form_validation->set_rules('accommodation_type_id', 'accommodation type', 'required|trim'); 

        if ($this->uri->rsegment(1) == 'eticket_passenger')
            self::_addInfo();
        
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Outlet
     *
     * @return      void
     */
    private function _addInfo()
    {
        // print '<pre>'.print_r($this->tools->getPost(),true).'</pre>';exit;
        // Check if form validation is TRUE
       if ($this->form_validation->run() == TRUE)
        {

            $passenger_id = 0;
            $passenger_data = array();
            
            $ticket_series_info = new Ticket_Series_Ref_Model(); 
            // get sales agent information
            $user = $this->user_account->displayList(array('id_user_account'=>$this->session->get('admin','admin_id')));
            // get outlet 
            $debtor_outlet = $this->debtor_outlet->displayList(array('outlet_id' => $user->outlet_id));
            // get debtor
            $debtor_amount = $this->debtor->displayList(array('id_debtor' => $debtor_outlet->debtor_id));
            $debtor = new Debtor_Management_Model($debtor_amount->id_debtor);
            // outlet commission
            $outlet_commission = $this->outlet->displayList(array('id_outlet' => $user->outlet_id));
            // passenger info
            $passenger_data = array(
                "firstname" => $this->tools->getPost('firstname'),
                "middlename" => $this->tools->getPost('lastname'),
                "lastname" => $this->tools->getPost('lastname'),
                "birthdate" => date("Y-m-d",strtotime($this->tools->getPost('birthdate'))),
                "age" => $this->tools->getPost('age'),
                "gender" => $this->tools->getPost('gender'),
                "contactno" => $this->tools->getPost('contactno'),
                "id_no" => $this->tools->getPost('id_no'),
                "nationality" => $this->tools->getPost('nationality'),
                "with_infant" => $this->tools->getPost('with_infant'),
                "infant_name" => $this->tools->getPost('infant_name')
            );
            // print '<pre>'.print_r($passenger_data,true).'</pre>';exit;
            $passenger_id = $this->passenger->add($passenger_data);    
            //
            $ref_series =str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
            $count = 0;
            if ($this->tools->getPost('subvoyage_management_id')) {
                //
                foreach($this->tools->getPost('subvoyage_management_id') as $key => $val) {
                   
                    $series =$this->tools->getPost('ticket_series_info');
                   // insert to booking
                    $insert_data[] = array(
                            "user_id" => $this->session->get('admin','admin_id'),
                            "ticket_series_info_id" => $ticket_series_info->add(array("ticket_no" => $series[$count])),
                            "passenger_id"=>$passenger_id,
                            "passenger_fare_id" => $this->tools->getPost('passenger_fare_id'),
                            "booking_source_id" => 3,
                            "booking_status_id" => 1,
                            "discount_id" => $this->tools->getPost('discount_id'),
                            "subvoyage_management_id" => $val,
                            "transaction_ref" => $ref_series,
                            "points"=>"",
                            "jetter_no" => $this->tools->getPost('jetter_no'),
                            "payment"=> $this->tools->getPost('payment'),
                            "fare_price" => $this->tools->getPost('fare_price'),
                            "return_fare_price" => $this->tools->getPost('return_fare_price'),
                            "total_discount" => $this->tools->getPost('total_discount'),
                            "terminal_fee" => $this->tools->getPost('terminal_fee'),
                            "port_charge" => $this->tools->getPost('port_charge'),
                            "payment_method_id" => $this->tools->get('payment_method_id'),
                            "total_amount" => $this->tools->getPost('total_amount'),
                            "reference_id" => $ref_series,
                            "date_added" => date("Y-m-d H:i:s"),

                    );

                    $counter++;   
                }

            }
            // do we have a return trip?
            if ($this->tools->getPost('return_subvoyage_management_id')) {
                $insert_data[] = array(
                            "user_id" => $this->session->get('admin','admin_id'),
                            "ticket_series_info_id" => $ticket_series_info->add(array("ticket_no" => $this->tools->getPost('return_ticket_no'))),
                            "passenger_id"=>$passenger_id,
                            "passenger_fare_id" => $this->tools->getPost('return_passenger_fare_id'),
                            "booking_source_id" => 3,
                            "booking_status_id" => 1,
                            "discount_id" => $this->tools->getPost('return_discount_id'),
                            "subvoyage_management_id" => $this->tools->getPost('return_subvoyage_management_id'),
                            "transaction_ref" => $ref_series,
                            "points"=>"",
                            "jetter_no" => $this->tools->getPost('return_jetter_no'),
                            "payment"=> $this->tools->getPost('payment'),
                            "fare_price" => $this->tools->getPost('fare_price'),
                             "return_fare_price" => $this->tools->getPost('return_fare_price'),
                             "total_discount" => $this->tools->getPost('total_discount'),
                            "terminal_fee" => $this->tools->getPost('terminal_fee'),
                            "is_return" => 1,
                            "port_charge" => $this->tools->getPost('port_charge'),
                            "payment_method_id" => $this->tools->get('payment_method_id'),
                            "total_amount" => $this->tools->getPost('total_amount'),
                            "reference_id" => $ref_series,
                            "date_added" => date("Y-m-d H:i:s"),

                    );

            }
            //
            foreach ($insert_data as $bookingData) {
                if ($this->booking->add($bookingData)) {
                    parent::copyFromPost($this->booking, 'id_booking');
                    parent::logThis($this->booking->id, 'Successfully added E-ticket');
                    // Set confirmation message
                    $this->session->set_flashdata('confirm', 'Successfully added E-ticket');
                    $this->session->set_flashdata('id', $this->booking->id);   
          
                } else {
                    // Set confirmation message
                    $this->session->set_flashdata('error', 'Error in saving E-Ticket');
                }    
            } 
            // update debtor wallet
            $new_debtor_amount = $debtor_amount->amount - $this->tools->getPost('total_amount') - $outlet_commission->outlet_commission;
            //
            $debtor->update(array('amount' => $new_debtor_amount));
            $transactionData = array(
                'debtor_id' => $debtor_amount->id_debtor,
                'transaction_date' => date("Y-m-d"),
                'transaction_details' => "outlet name issue ticket",
                'transaction_debit' => $this->tools->getPost('total_amount') - $outlet_commission->outlet_commission
                );
            // 
            $this->debtor_transaction_history->add($transactionData);
            redirect(admin_url($this->classname));
                        
       }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $eticket = new Eticket_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($eticket, 'id_eticket');
            
            // Check for successful update
            if ($eticket->update())
            {                                   
                 parent::logThis($eticket->id, 'Successfully updated Eticket');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Eticket');
                $this->session->set_flashdata('id', $eticket->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Eticket');
            }
            
            redirect(admin_url($this->classname));
        }
    }
    
    public function get_discount() {
        $discount_id = $this->tools->getPost('discount_id');

        $data_discount = $this->discount->displayList(array("id_discount"=>$discount_id));
        print json_encode($data_discount);
    }  

    public function get_voyage() {
        $voyage_info = array();
        $departure_date = $this->tools->getPost('departure_date');

        $voyage = $this->booking->get_voyages(array("origin_id"=>$this->tools->getPost('origin'),"destination_id"=>$this->tools->getPost('destination')));
        // foreach ($voyage as $v) {
        //     //
        //     if ($departure_date == $v->dept_date) {
        //         $voyage_info[] = $v;
        //     }
        // }
        // print json_encode($voyage_info);
        print json_encode($voyage);       
    }

    public function get_voyage_info() {

        $origin_id = $this->tools->getPost("origin");
        $destination_id =$this->tools->getPost("destination");
        $voyage_management = $this->tools->getPost("voyage_management");
        $voyage_id = $this->tools->getPost("voyage_id");        
        $voyage_info = $this->booking->get_voyage_from_id($origin_id,$destination_id,$voyage_management);

        $data = array(
            'voyage_details' => $voyage_info,
            'payment_method' => $this->payment_method->displayList(),
            'port' => $this->port->displayList()
            // 'passenger_fare_details' => $this->passenger_fare->displayList(array('u.accommodation_id'=>$this->tools->getPost("accommodation_type_id"), 'u.voyage_id'=>$this->tools->getPost('departure_date'))),

        );
        parent::displayTemplate(admin_dir('third_party_outlet/eticket_passenger/eticket_details'),$data);
        
    }
    // get passenger fare
    public function get_passenger_fare() {
        // get passenger fare
        $passenger_fare = $this->passenger_fare->displayList(array("voyage_id"=>$this->tools->getGet('voyage_id'),"accommodation_id"=> $this->tools->getGet('accommodation_id')));
        echo json_encode($passenger_fare);
    }

    public function get_fare_details() {
        $leg_array = array();
        $origin_id = $this->tools->getPost("origin");
        $destination_id =$this->tools->getPost("destination");
        $voyage_management = $this->tools->getPost("voyage_management");
        $voyage_id = $this->tools->getPost("voyage_id"); 
        $passenger_fare_id = $this->tools->getPost("passenger_fare_id");
        $subvoyage_id = $this->booking->get_voyage_from_id($origin_id,$destination_id,$voyage_management);

        //get subvoyage id
        foreach ($subvoyage_id as $sid) {
            $leg_array[] = $this->subvoyage_fare->displayList(array("subvoyage_id"=>$sid->id_subvoyage,"passenger_fare_id"=>$passenger_fare_id));
        }

        $data = array(
            "voyage_fare_details" => $leg_array,
            "discount" => $this->discount->displayList()
            );

        parent::displayTemplate(admin_dir('third_party_outlet/eticket_passenger/eticket_fare_details'),$data);
    }
    // get passenger fare
    public function get_return_passenger_fare() {
        // get passenger fare
        $passenger_fare = $this->passenger_fare->displayList(array("voyage_id"=>$this->tools->getGet('voyage_id'),"accommodation_id"=> $this->tools->getGet('accommodation_id')));
        $data = array(
            'return_passenger_fare' => $passenger_id,
            "discount" => $this->discount->displayList()
            );
        parent::displayTemplate(admin_dir('third_party_outlet/eticket_passenger/eticket_return_fare'),$data);
        // echo json_encode($passenger_fare);
    }

    

    public function get_passenger_fare_details(){
        $passenger_details = $this->passenger_fare->displayList(array("id_passenger_fare"=>$this->tools->getGet('passenger_fare_id')));
        echo json_encode($passenger_details);
    }

    
    

    public function get_evoucher()
    {
        $evoucher_code = $this->tools->getPost('evoucher_code');
        
        $query = $this->evoucher_series->displayList(array('evoucher_code'=>$evoucher_code));
        
        print json_encode($query);
    }
  


   
}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */