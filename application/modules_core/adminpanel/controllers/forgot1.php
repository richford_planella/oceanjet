<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Forgot Password Class
|--------------------------------------------------------------------------
|
| Delegates functions for adminsitration forgot password records
|
| @category		Controller
| @author		Baladeva Juganas
*/

class Forgot extends MY_Controller
{
        public function __construct()
        {
            parent::__construct();
        }
        
        public function index()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('login/header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('login/footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('login/forgot'),$data);
        }
        
        public function password()
        {
            $data = array (
                            'header' => $this->load->view(admin_dir('login/header'),array(),TRUE),
                            'footer' => $this->load->view(admin_dir('login/footer'),array(),TRUE),
            );
            $this->load->view(admin_dir('login/forgot'),$data);
        }
}