<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Login Class
|--------------------------------------------------------------------------
|
| Delegates functions for adminsitration login records
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Login extends Admin_Core
{
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
               	$this->classname = strtolower(get_class());

		parent::__construct();
                                                
		// Check if admin is already logged in
		if ($this->session->get('admin','admin_login'))
			redirect(admin_url());

		// Load the models directly involve with the class 
		$this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('user/user_model'));
                $this->load->model(admin_dir('user/user_account_model'));
                $this->load->model(admin_dir('reset_password/reset_password_model'));
                
                $this->user_profile = new Profile_Model();
                $this->user = new User_Model();
                $this->user_account = new User_Account_Model();
                $this->reset_password = new Reset_Password_Model();
	}
	
	// ------------------------------------------------------------------------
	
	/*
	 * Shows the login form for admins
	 *
	 * @access		public
	 * @return		void
	 */
	public function index()
	{
		// form validation
		self::_validate();
		
		$data = array(	'header' => parent::getTemplate(admin_dir('login/header')),
                                'footer' => parent::getTemplate(admin_dir('login/footer')),
                        );
                
		$this->load->view(admin_dir('login/login'), $data);
	}

        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{		
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$user_id = $this->user->getUserID(array('username' => $this->tools->getPost('username')));
			$user = new User_Model($user_id);
                        
                        // get user account details
                        $user->user_account = new User_Account_Model($user->id_user);
                        
			// handle brute force attacks
			sleep(1);
                        
                        $login_attempt    = $this->configuration->getValue('value', array('configuration' => 'LOGIN_ATTEMPT'));
                        
			// check for record
			if (empty($user_id))
			{
				// Redirect with error if the condition above == FALSE
				$this->session->set_flashdata('note', 'Failed to log in. Invalid username and/or password. Please try again.');
				redirect(admin_url('login'));
			}
                        
			// check for password in encryption
			$pw = $user->password;
                        
                        $time_lock = $this->configuration->getValue('value', array('configuration' => 'ENABLE_LOCKED_ACCT'));
                        // check last failed login attempt
                        $current_time = strtotime(date("Y-m-d H:i:s"));
                        $datetime_from = strtotime("+".$time_lock." minutes",strtotime($user->last_failed_login));
                                
			if (hash('sha256', $this->tools->getPost('password').$user->salt) != $pw)
			{
                                if($current_time > $datetime_from)
                                {
                                    // reset failed log in attempt
                                    $current_attempt = 0;
                                    
                                    // update user log in attempt
                                    $user->update(array(),array('num_failed_login' => 1,'is_locked' => 0, 'last_failed_login' => date("Y-m-d H:i:s")));
                                }
                                else
                                {
                                    // check log in attempts
                                    if($user->num_failed_login < $login_attempt)
                                        $current_attempt = $user->num_failed_login + 1;
                                    elseif($user->num_failed_login == $login_attempt)
                                        $current_attempt = $user->num_failed_login;
                                    
                                    // update user log in attempt
                                    $user->update(array(),array('num_failed_login' => $current_attempt, 'last_failed_login' => date("Y-m-d H:i:s")));
                                }
                                
                                
                                if($current_attempt == $login_attempt)
                                {
                                    // set locked status to 1
                                    $user->update(array(),array('is_locked' => 1));
                                    
                                    // send email for account locked
                                    $email_data = array(
                                                            'user'          => $user,
                                                            'employee_name' => $user->user_account->lastname.', '.$user->user_account->firstname,
                                                            'username'      => $user->username,
                                                        );
                        
                                    // set email parameter
                                    $subject = 'Blocked Account';
                                    $body = parent::getTemplate(admin_dir('email_template/block_email'),$email_data);

                                    // send email
                                    parent::sendMail($body,$subject,$user->user_account->email_address);
                                    
                                    // Redirect with error if the condition above == TRUE
                                    $this->session->set_flashdata('note', 'Your account has been blocked. Contact your System Admin to retrieve account.');
                                    redirect(admin_url('login'));
                                }
                                
				// Redirect with error if the condition above == FALSE
				$this->session->set_flashdata('note', 'Failed to log in. Invalid username and/or password. Please try again.');
				redirect(admin_url('login'));
			}
                        else
                        {
                                // check if password is new
                                if($user->is_new)
                                {
                                    // check if user exist on reset password
                                    if($this->reset_password->displayList(array('user_id' => $user->id),array(),TRUE) > 0)
                                    {
                                        $reset_pass = new Reset_Password_Model($this->reset_password->getValue('id_reset_password', array('user_id' => $user_id)));

                                        if($pw == $reset_pass->value)
                                        {
                                            
                                            $date_now = strtotime(date('Y-m-d h:i'));
                                            // check time
                                            if($date_now > $reset_pass->exp_date)
                                            {
                                                // Redirect with error if the condition above == TRUE
                                                $this->session->set_flashdata('note', 'Your pass is already expired. Contact your System Admin to retrieve account.');
                                                redirect(admin_url('login'));
                                            }
                                        }
                                    }
                                }
                                
                                if($current_time > $datetime_from)
                                {                                    
                                    // update user log in attempt
                                    $user->update(array(),array('num_failed_login' => 1,'is_locked' => 0, 'last_failed_login' => date("Y-m-d H:i:s")));
                                }
                                
                                // filter last password change
                                $password_change = $this->configuration->getValue('value', array('configuration' => 'REQ_PASSWORD_CHANGE'));
                                // check last failed login attempt
                                $curr_time = strtotime(date("Y-m-d"));
                                $date_from = strtotime("+".$password_change." days",strtotime($user->password_generated));
                                
                                if($curr_time >= $date_from)
                                {                                    
                                    // update user log in attempt
                                    $user->update(array(),array('is_new' => 1));
                                }
                                
                                // filter inactive account
                                $inactive_account = $this->configuration->getValue('value', array('configuration' => 'INACTIVE_ACCT'));
                                // check last failed login attempt
                                $curr_time = strtotime(date("Y-m-d"));
                                $date_from = strtotime("+".$inactive_account." days",strtotime(date("Y-m-d",strtotime($user->last_login))));
                                
                                if($curr_time >= $date_from)
                                {                  
                                    // update user log in attempt
                                    $user->update(array(),array('enabled' => 0));
                                }
                                
                                $user = new User_Model($user_id);

                                // get user account details
                                $user->user_account = new User_Account_Model($user->id_user);
                        }
                        
                        // Check to see if the administrator is blocked
			if ( $user->is_locked )
			{
				// Redirect with error if the condition above == FALSE
				$this->session->set_flashdata('note', 'Your account has been blocked. Contact your System Admin to retrieve account.');
				redirect(admin_url('login'));
			}
                        
                        // get user account details
                        //$user->user_account = new User_Account_Model($user->id_user);
                
			// Check to see if the administrator is allowed to login
			if ( ! $user->enabled OR $user->is_deleted )
			{
				// Redirect with error if the condition above == FALSE
				$this->session->set_flashdata('note', 'Your account is inactive. Contact your System Admin to retrieve account.');
				redirect(admin_url('login'));
			}

			// If all the conditions are true, initialize the session data
			$newdata = array(
				'admin_id'                  => $user->id_user,
				'admin_email'               => $user->user_account->email_address,
                                'admin_username'            => $user->username,
				'admin_name'                => substr($user->user_account->firstname, 0, 1).'.&nbsp;'.htmlentities(strtoupper($user->user_account->lastname), ENT_COMPAT, 'UTF-8'),
                                'admin_login'               => TRUE,
				'encryption_key'            => $this->config->item('encryption_key'),
				'administrator_profile_id'  => $user->user_profile_id,
                                'is_new'                    => $user->is_new,
                                'profile_pic'               => $user->user_account->profile_pic,
				// Security token
				'token'                     => substr(sha1(microtime()),4,30)
			);
                        
                        // update latest log in
                        $user->update(array(),array("last_login" => date("Y-m-d H:i:s"), 'num_failed_login' => 0));
                        
                        $session_time = $this->configuration->getValue('value', array('configuration' => 'RE_AUTHENTICATE_ACCT'));
                        
                        //set session expire time, after that user should login again
                        $this->config->set_item('sess_expiration', $session_time * 60);
                        
			// Set the session data
			$this->session->set_userdata('admin', $newdata);

			// Log actions
			$this->logThis('administrator_log');
			
			// Check if redirect URL is set
			if ($this->session->userdata('redirect'))
			{
				// Get the redirect session and remove it
				$redirect = $this->session->userdata('redirect');
				$this->session->unset_userdata('redirect');
				
				// Redirect
				redirect($redirect);
			}
                        
			// Redirect to admin home page if redirect is not set
			redirect(admin_url());
		}
	}
}

/* End of file login.php */
/* Location: ./application/modules_core/adminpanel/login/login.php */