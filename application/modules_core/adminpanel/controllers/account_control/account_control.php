<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Account Control Class
|--------------------------------------------------------------------------
|
| Modify permissions of account control
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Account_Control extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
                $this->load->model(admin_dir('configuration/configuration_model'));
                $this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('user/user_model'));
                $this->load->model(admin_dir('user/user_account_model'));
                
                $this->configuration = new Configuration_Model();
                $this->user_profile = new Profile_Model();
                $this->user = new User_Model();
                $this->user_account = new User_Account_Model();
                
                // if session has value
                if( ! $this->session->get('admin', 'admin_login') OR $this->session->get('admin', 'encryption_key') != $this->config->item('encryption_key'))
                {
                        // redirect to dashboard
                        redirect(admin_url());
                }
                
                // user ID
		$this->id = $this->session->get('admin', 'admin_id');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display group billing list
	 *
	 * @access		public       
	 * @return		void
	 */
        public function index()
        {               
                // validate form
                self::_validate();
                                
                // get data
                $data = array(
                                'header'                => Modules::run(admin_dir('header/call_header'),array('title' => 'Account Control')),
                                'footer'                => parent::getTemplate(admin_dir('footer')),
                                'NUM_PASSWORD_CHANGE'   => $this->configuration->getValue('value', array('configuration' => 'NUM_PASSWORD_CHANGE')),
                                'REQ_PASSWORD_CHANGE'   => $this->configuration->getValue('value', array('configuration' => 'REQ_PASSWORD_CHANGE')),
                                'MIN_PASSWORD_CHAR'     => $this->configuration->getValue('value', array('configuration' => 'MIN_PASSWORD_CHAR')),
                                'MAX_PASSWORD_CHAR'     => $this->configuration->getValue('value', array('configuration' => 'MAX_PASSWORD_CHAR')),
                                'PASSWORD_REQ_UPPERCASE'=> $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_UPPERCASE')),
                                'PASSWORD_REQ_LOWERCASE'=> $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_LOWERCASE')),
                                'PASSWORD_REQ_NUMBER'   => $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_NUMBER')),
                                'PASSWORD_REQ_SPL_CHAR' => $this->configuration->getValue('value', array('configuration' => 'PASSWORD_REQ_SPL_CHAR')),
                                'LOGIN_ATTEMPT'         => $this->configuration->getValue('value', array('configuration' => 'LOGIN_ATTEMPT')),
                                'ENABLE_LOCKED_ACCT'    => $this->configuration->getValue('value', array('configuration' => 'ENABLE_LOCKED_ACCT')),
                                'RE_AUTHENTICATE_ACCT'  => $this->configuration->getValue('value', array('configuration' => 'RE_AUTHENTICATE_ACCT')),
                                'INACTIVE_ACCT'         => $this->configuration->getValue('value', array('configuration' => 'INACTIVE_ACCT')),
                );

                parent::displayTemplate(admin_dir('account_control/form/account_control'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{
            $this->form_validation->set_rules('NUM_PASSWORD_CHANGE', 'password history count', 'required|trim|xss_clean');
            $this->form_validation->set_rules('REQ_PASSWORD_CHANGE', 'no. of days password need to change', 'required|trim|xss_clean');
            $this->form_validation->set_rules('MIN_PASSWORD_CHAR', 'minimum password character', 'required|trim|xss_clean');
            $this->form_validation->set_rules('MAX_PASSWORD_CHAR', 'maximum password character', 'required|trim|xss_clean');
            $this->form_validation->set_rules('PASSWORD_REQ_UPPERCASE', 'password required uppercase character occurrence', 'required|trim|xss_clean');
            $this->form_validation->set_rules('PASSWORD_REQ_LOWERCASE', 'password required lowercase character occurrence', 'required|trim|xss_clean');
            $this->form_validation->set_rules('PASSWORD_REQ_NUMBER', 'password required number occurrence', 'required|trim|xss_clean');
            $this->form_validation->set_rules('PASSWORD_REQ_SPL_CHAR', 'password required special characters occurrence', 'required|trim|xss_clean');
            $this->form_validation->set_rules('LOGIN_ATTEMPT', 'no. log in attempt', 'required|trim|xss_clean');
            $this->form_validation->set_rules('ENABLE_LOCKED_ACCT', 'minutes to re-activate locked account', 'required|trim|xss_clean');
            $this->form_validation->set_rules('RE_AUTHENTICATE_ACCT', 'minutes to re-authenticate after idle', 'required|trim|xss_clean');
            $this->form_validation->set_rules('INACTIVE_ACCT', 'no. of days to auto deactivate account', 'required|trim|xss_clean');
            
            self::_editInfo();
        }
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit upload directory
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
            // Check if form validation is TRUE
            if ($this->form_validation->run() == TRUE)
            {
                // update upload directory configuration
                $this->configuration->update(array('configuration' => 'NUM_PASSWORD_CHANGE'),array('value' => $this->tools->getPost('NUM_PASSWORD_CHANGE')));
                $this->configuration->update(array('configuration' => 'REQ_PASSWORD_CHANGE'),array('value' => $this->tools->getPost('REQ_PASSWORD_CHANGE')));
                $this->configuration->update(array('configuration' => 'MIN_PASSWORD_CHAR'),array('value' => $this->tools->getPost('MIN_PASSWORD_CHAR')));
                $this->configuration->update(array('configuration' => 'MAX_PASSWORD_CHAR'),array('value' => $this->tools->getPost('MAX_PASSWORD_CHAR')));
                $this->configuration->update(array('configuration' => 'PASSWORD_REQ_UPPERCASE'),array('value' => $this->tools->getPost('PASSWORD_REQ_UPPERCASE')));
                $this->configuration->update(array('configuration' => 'PASSWORD_REQ_LOWERCASE'),array('value' => $this->tools->getPost('PASSWORD_REQ_LOWERCASE')));
                $this->configuration->update(array('configuration' => 'PASSWORD_REQ_NUMBER'),array('value' => $this->tools->getPost('PASSWORD_REQ_NUMBER')));
                $this->configuration->update(array('configuration' => 'PASSWORD_REQ_SPL_CHAR'),array('value' => $this->tools->getPost('PASSWORD_REQ_SPL_CHAR')));
                $this->configuration->update(array('configuration' => 'LOGIN_ATTEMPT'),array('value' => $this->tools->getPost('LOGIN_ATTEMPT')));
                $this->configuration->update(array('configuration' => 'ENABLE_LOCKED_ACCT'),array('value' => $this->tools->getPost('ENABLE_LOCKED_ACCT')));
                $this->configuration->update(array('configuration' => 'RE_AUTHENTICATE_ACCT'),array('value' => $this->tools->getPost('RE_AUTHENTICATE_ACCT')));
                $this->configuration->update(array('configuration' => 'INACTIVE_ACCT'),array('value' => $this->tools->getPost('INACTIVE_ACCT')));
                
                parent::logThis(0,'Successfully updated system configuration!');
                
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated system configuration!');
                redirect(admin_url($this->classname));
            }
        }
        
}

/* End of file account_control.php */
/* Location: ./application/modules_core/adminpanel/controllers/account_control/account_control.php */