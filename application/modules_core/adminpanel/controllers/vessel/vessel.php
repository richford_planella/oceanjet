<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Vessel Class
|--------------------------------------------------------------------------
|
| Modify permissions of vessel
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Vessel extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
                
                parent::__construct();
		
                $this->load->model(admin_dir('accommodation/accommodation_model'));
                $this->load->model(admin_dir('vessel/vessel_model'));
                $this->load->model(admin_dir('vessel/vessel_seats_model'));
                
                $this->accommodation = new Accommodation_Model();
                $this->vessel = new Vessel_Model();
                $this->vessel_seats = new Vessel_Seats_Model();
                
                // load library
                $this->load->library('csvreader');
                
                $this->csvreader = new csvreader();
                
                // vessel ID
		$this->id = $this->uri->rsegment(3);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display vessel list
	 *
	 * @access		public
	 * @return		void
	 */
        public function index()
        {                
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Vessel ')),
                                'footer'        => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.vessel-tools.js')))),
                                'vessel'        => $this->vessel->displayList(),
                );
                
                parent::displayTemplate(admin_dir('vessel/vessel'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * View vessel info
	 *
	 * @access		public
	 * @return		void
	 */
        public function view()
        {
                // get vessel
                $vessel = new Vessel_Model($this->id);
                
                // Check if a record exists
		$vessel->redirectIfEmpty(admin_url($this->classname));
                
                // array seat plan
                $seat_plan = array();
                
                // get seat plan
                for($i = 1; $i <= $vessel->total_row; $i++)
                    for($x = 1; $x <= $vessel->total_column; $x++)
                    {
                        $seat_plan[$i][$x]['value'] = $this->vessel_seats->getValue('vessel_seats', array(
                                                                                                            'vessel_id' => $vessel->id,
                                                                                                            'column'    => $x,
                                                                                                            'row'       => $i,
                                                                                                            ));
                        // get accommodation id
                        $accommodation_id = $this->vessel_seats->getValue('accommodation_id', array(
                                                                                                            'vessel_id' => $vessel->id,
                                                                                                            'column'    => $x,
                                                                                                            'row'       => $i,
                                                                                                            ));
                        
                        if($accommodation_id > 0)
                            $seat_plan[$i][$x]['color'] = $this->accommodation->getValue('palette', array(
                                                                                                            'id_accommodation' => $accommodation_id,
                                                                                                            ));
                        else
                            $seat_plan[$i][$x]['color'] = "#fff";
                        
                        $seat_plan[$i][$x]['accommodation'] = $accommodation_id;

                                
                    }
                
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'View Vessel')),
                                'footer'        => parent::getTemplate(admin_dir('footer')),
                                'vessel'        => $vessel,
                                'seat_plan'     => $seat_plan,
                                'accommodation' => $this->accommodation->displayList(array('enabled' => 1)),
                );
                
                parent::displayTemplate(admin_dir('vessel/form/vessel'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Togglwe vessel status
	 *
	 * @access		public
	 * @return		void
	 */
        public function toggle()
        {
                // get vessel
                $vessel = new Vessel_Model($this->id);
                
                // Check if a record exists
		$vessel->redirectIfEmpty(admin_url($this->classname));
                
                // Check for successful toggle
                if ($vessel->toggleStatus())
                {	
                        // Set confirmation message
                        if($vessel->enabled)
                            $this->session->set_flashdata('confirm', $vessel->vessel.' has been de-activated!');
                        else
                            $this->session->set_flashdata('confirm', $vessel->vessel.' has been re-activated!');
                        $this->session->set_flashdata('id', $vessel->id);
                }
                else
                {
                        // Set confirmation message
                        $this->session->set_flashdata('note', 'Error in updating vessel');
                }
                
                redirect(admin_url($this->classname));
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Delete vessel
	 *
	 * @access		public
	 * @return		void
	 */
        public function delete()
        {
                // get vessel
                $vessel = new Vessel_Model($this->id);
                
                // Check if a record exists
		$vessel->redirectIfEmpty(admin_url($this->classname));
                
                // Check for successful toggle
                if ($vessel->countFromSubvoyageManagement() > 0)
                {	
                        // Set confirmation message
                        $this->session->set_flashdata('note', 'Cannot delete '.$vessel->vessel_code.' was already used in the system.');
                }
                else
                {
                        // Delete vessel seats
                        $vessel_seats = new Vessel_Seats_Model();
                        
                        $vessel_seats->delete(array('vessel_id' => $this->id));
                                                
                        // Set confirmation message
                        if($vessel->delete())
                            $this->session->set_flashdata('confirm', "Successfully deleted ".$vessel->vessel_code.'!');
                        else
                            $this->session->set_flashdata('note', "Error deleting vessel!");
                }
                
                redirect(admin_url($this->classname));
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Add vessel
	 *
	 * @access		public
	 * @return		void
	 */
        public function add()
        {
                // initialize seat plan
                $seat_plan = array();
                // initialize counter
                $total_seats = '';
                $total_column = 0;
                $total_row = 0;
                
                if($this->tools->getPost('vessel_seats'))
                {
                    // initialize seat plan
                    $seat_plan = array();
                    // reset counter    
                    $total_seats = '';
                    $total_column = 0;
                    $total_row = count($this->tools->getPost('vessel_seats'));
                    
                    foreach($this->tools->getPost('vessel_seats') as $key => $value)
                    {
                        $column = 1;
                        $total_column = count($value);
                        foreach($value as $keyValue => $val)
                        {
                            // count seats
                            if($val == 'N/A' OR $val == "")
                            {    
                                $val = 'N/A';
                                $accommodation_id = $this->tools->getPost('accommodation',$key,$column);
                            }
                            else
                            {    
                                $total_seats++;
                                $arrVal = explode('-',$val);
                                $accommodation_id = $this->tools->getPost('accommodation',$key,$column);
                            }

                            $seat_plan[$key][$column]['value'] = $val;
                            $seat_plan[$key][$column]['accommodation'] = $accommodation_id;

                            if($accommodation_id > 0)
                                $seat_plan[$key][$column]['color'] = $this->accommodation->getValue('palette', array(
                                                                                                                        'id_accommodation' => $accommodation_id,
                                                                                                                    ));
                            else
                                $seat_plan[$key][$column]['color'] = "#fff";

                            // increment column count
                            $column++;
                        }
                    }
                }
                
                if($this->tools->getPost("submit"))
                {
                    if($this->tools->getPost("submit") == 'upload')
                    {
                        // initialize seat plan
                        $seat_plan = array();
                        // reset counter
                        $total_seats = '';
                        $total_column = 0;
                        $total_row = 0;
                        
                        // check csv file content
                        if(!empty($_FILES["fileInput"]["tmp_name"]))
                        {
                            // get file path
                            $file_path = $_FILES["fileInput"]["tmp_name"];

                            // read csv
                            $result = $this->csvreader->parse_file($file_path);

                            // assign total seats, row, column
                            $total_seats = '';
                            $total_column = 0;
                            $total_row = count($result);
                            
                            // loop $result
                            foreach($result as $key => $value)
                            {
                                $column = 1;
                                $total_column = count($value);
                                foreach($value as $keyValue => $val)
                                {
                                    // count seats
                                    if($val == 'N/A' OR $val == "")
                                    {    
                                        $val = 'N/A';
                                        $accommodation_id = 0;
                                    }
                                    else
                                    {    
                                        $total_seats++;
                                        $arrVal = explode('-',$val);
                                        $accommodation_id = $this->accommodation->getValue('id_accommodation', array('accommodation_code' => $arrVal[0]));
                                    }
                                    
                                    $seat_plan[$key][$column]['value'] = $val;
                                    $seat_plan[$key][$column]['accommodation'] = $accommodation_id;
                                    
                                    if($accommodation_id > 0)
                                        $seat_plan[$key][$column]['color'] = $this->accommodation->getValue('palette', array(
                                                                                                                                'id_accommodation' => $accommodation_id,
                                                                                                                            ));
                                    else
                                        $seat_plan[$key][$column]['color'] = "#fff";
                                    
                                    // increment column count
                                    $column++;
                                }
                            }
                        }
                    }
                }
                
                // Form validation
                self::_validate();
                        
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Create Vessel')),
                                'footer'        => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.vessel-action-tools.js')))),
                                'seat_plan'     => $seat_plan,
                                'total_column'  => $total_column,
                                'total_row'     => $total_row,
                                'total_seats'   => $total_seats,
                                'accommodation' => $this->accommodation->displayList(array('enabled' => 1)),
                );
                
                parent::displayTemplate(admin_dir('vessel/form/add/vessel'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Edit vessel
	 *
	 * @access		public
	 * @return		void
	 */
        public function edit()
        {
                // get vessel
                $vessel = new Vessel_Model($this->id);
                
                // Check if a record exists
		$vessel->redirectIfEmpty(admin_url($this->classname));
                
                // initialize seat plan
                $seat_plan = array();
                // initialize counter
                $total_seats = '';
                $total_column = 0;
                $total_row = 0;
                                
                if($this->tools->getPost('vessel_seats'))
                {
                    // initialize seat plan
                    $seat_plan = array();
                    // reset counter    
                    $total_seats = '';
                    $total_column = 0;
                    $total_row = count($this->tools->getPost('vessel_seats'));
                    
                    foreach($this->tools->getPost('vessel_seats') as $key => $value)
                    {
                        $column = 1;
                        $total_column = count($value);
                        foreach($value as $keyValue => $val)
                        {
                            // count seats
                            if($val == 'N/A' OR $val == "")
                            {    
                                $val = 'N/A';
                                $accommodation_id = $this->tools->getPost('accommodation',$key,$column);
                            }
                            else
                            {    
                                $total_seats++;
                                $arrVal = explode('-',$val);
                                $accommodation_id = $this->tools->getPost('accommodation',$key,$column);
                            }

                            $seat_plan[$key][$column]['value'] = $val;
                            $seat_plan[$key][$column]['accommodation'] = $accommodation_id;

                            if($accommodation_id > 0)
                                $seat_plan[$key][$column]['color'] = $this->accommodation->getValue('palette', array(
                                                                                                                        'id_accommodation' => $accommodation_id,
                                                                                                                    ));
                            else
                                $seat_plan[$key][$column]['color'] = "#fff";

                            // increment column count
                            $column++;
                        }
                    }
                }
                else
                {
                    // initialize seat plan
                    $seat_plan = array();
                    // reset counter    
                    $total_seats = $vessel->total_seats;
                    $total_column = $vessel->total_column;
                    $total_row = $vessel->total_row;
                    
                    // get seat plan
                    for($i = 1; $i <= $vessel->total_row; $i++)
                        for($x = 1; $x <= $vessel->total_column; $x++)
                        {
                            $seat_plan[$i][$x]['value'] = $this->vessel_seats->getValue('vessel_seats', array(
                                                                                                                'vessel_id' => $vessel->id,
                                                                                                                'column'    => $x,
                                                                                                                'row'       => $i,
                                                                                                                ));
                            // get accommodation id
                            $accommodation_id = $this->vessel_seats->getValue('accommodation_id', array(
                                                                                                                'vessel_id' => $vessel->id,
                                                                                                                'column'    => $x,
                                                                                                                'row'       => $i,
                                                                                                                ));
                            
                            if($accommodation_id > 0)
                                $seat_plan[$i][$x]['color'] = $this->accommodation->getValue('palette', array(
                                                                                                                'id_accommodation' => $accommodation_id,
                                                                                                                ));
                            else
                                $seat_plan[$i][$x]['color'] = "#fff";

                            $seat_plan[$i][$x]['accommodation'] = $accommodation_id;
                        }
                }
                
                if($this->tools->getPost("submit"))
                {
                    if($this->tools->getPost("submit") == 'upload')
                    {
                        // initialize seat plan
                        $seat_plan = array();
                        // reset counter
                        $total_seats = '';
                        $total_column = 0;
                        $total_row = 0;
                        
                        // check csv file content
                        if(!empty($_FILES["fileInput"]["tmp_name"]))
                        {
                            // get file path
                            $file_path = $_FILES["fileInput"]["tmp_name"];

                            // read csv
                            $result = $this->csvreader->parse_file($file_path);

                            // assign total seats, row, column
                            $total_seats = '';
                            $total_column = 0;
                            $total_row = count($result);
                            
                            // loop $result
                            foreach($result as $key => $value)
                            {
                                $column = 1;
                                $total_column = count($value);
                                foreach($value as $keyValue => $val)
                                {
                                    // count seats
                                    if($val == 'N/A' OR $val == "")
                                    {    
                                        $val = 'N/A';
                                        $accommodation_id = 0;
                                    }
                                    else
                                    {    
                                        $total_seats++;
                                        $arrVal = explode('-',$val);
                                        $accommodation_id = $this->accommodation->getValue('id_accommodation', array('accommodation_code' => $arrVal[0]));
                                    }
                                    
                                    $seat_plan[$key][$column]['value'] = $val;
                                    $seat_plan[$key][$column]['accommodation'] = $accommodation_id;
                                    
                                    if($accommodation_id > 0)
                                        $seat_plan[$key][$column]['color'] = $this->accommodation->getValue('palette', array(
                                                                                                                                'id_accommodation' => $accommodation_id,
                                                                                                                            ));
                                    else
                                        $seat_plan[$key][$column]['color'] = "#fff";
                                    
                                    // increment column count
                                    $column++;
                                }
                            }
                        }
                    }
                }
                
                // Form validation
                self::_validate();
                
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit Vessel')),
                                'footer'        => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.vessel-action-tools.js')))),
                                'vessel'        => $vessel,
                                'seat_plan'     => $seat_plan,
                                'total_column'  => $total_column,
                                'total_row'     => $total_row,
                                'total_seats'   => $total_seats,
                                'accommodation' => $this->accommodation->displayList(array('enabled' => 1)),
                );
                
                parent::displayTemplate(admin_dir('vessel/form/edit/vessel'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{
                $this->form_validation->set_rules('vessel', 'vessel name', 'required|trim|max_length[128]');
                $this->form_validation->set_rules('total_seats', 'seat plan', 'required|trim|xss_clean|integer');
		$this->form_validation->set_rules('enabled', 'status', 'required');                    
             
		if ($this->uri->rsegment(2) == 'add')
                        self::_addInfo();
			
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add department
	 *
	 * @access		private
	 * @return		void
	 */
	private function _addInfo()
	{
                // check for unique vessel code
                $this->form_validation->set_rules('vessel_code', 'vessel code', 'required|trim|max_length[32]|is_unique[vessel.vessel_code]');
                
                // Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                        if($this->tools->getPost("submit") == 'submit')
                        {
                            $vessel = new Vessel_Model();
                        
                            $this->tools->setPost('total_seats', 0);
                            $this->tools->setPost('total_row', 0);
                            $this->tools->setPost('total_column', 0);
                            $this->tools->setPost('vessel_code', strtoupper($this->tools->getPost('vessel_code')));

                            parent::copyFromPost($vessel, 'id_vessel');

                            // Check for successful insert
                            if ($vessel->add())
                            {
                                    if($this->tools->getPost('vessel_seats'))
                                    {
                                        // reset counter    
                                        $total_seats = '';
                                        $total_column = 0;
                                        $total_row = count($this->tools->getPost('vessel_seats'));

                                        foreach($this->tools->getPost('vessel_seats') as $key => $value)
                                        {
                                            $column = 1;
                                            $total_column = count($value);
                                            foreach($value as $keyValue => $val)
                                            {
                                                $vessel_seats = new Vessel_Seats_Model();

                                                // count seats
                                                if($val == 'N/A' OR $val == "")
                                                {    
                                                    $val = 'N/A';
                                                    $accommodation_id = $this->tools->getPost('accommodation',$key,$column);
                                                }
                                                else
                                                {    
                                                    $total_seats++;
                                                    $accommodation_id = $this->tools->getPost('accommodation',$key,$column);
                                                }

                                                $add_data = array(
                                                                    'vessel_id'         => $vessel->id,
                                                                    'row'               => $key,
                                                                    'column'            => $column,
                                                                    'vessel_seats'      => $val,
                                                                    'accommodation_id'  => $accommodation_id,
                                                            );

                                                // insert vessel seats
                                                $vessel_seats->add($add_data);

                                                // increment column count
                                                $column++;
                                            }
                                        }
                                        // update vessel
                                        $this->vessel->update(
                                                                array(
                                                                        'id_vessel' => $vessel->id
                                                                ),
                                                                array(
                                                                        'total_seats'   => $total_seats,
                                                                        'total_row'     => $total_row,
                                                                        'total_column'  => $total_column)
                                                            );
                                    }

                                    parent::logThis($vessel->id,'Successfully added vessel.');

                                    // Set confirmation message
                                    $this->session->set_flashdata('confirm', 'Successfully added vessel.');
                                    $this->session->set_flashdata('id', $vessel->id);
                            }
                            else
                            {
                                    // Set confirmation message
                                    $this->session->set_flashdata('error', 'Error in saving vessel');
                            }
                            redirect(admin_url($this->classname));
                        }
                }
        }
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit vessel
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
		$vessel = new Vessel_Model($this->id);
                
                // check if vessel code doesn't change
                if($this->vessel->getValue('id_vessel', array('vessel_code' => $this->tools->getPost('vessel_code'))) == $this->id)
                        $this->form_validation->set_rules('vessel_code', 'vessel code', 'required|trim|max_length[32]');
                else
                        $this->form_validation->set_rules('vessel_code', 'vessel code', 'required|trim|max_length[32]|is_unique[vessel.vessel_code]');
                
                // Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                        if($this->tools->getPost("submit") == 'submit')
                        {
                            parent::copyFromPost($vessel, 'id_vessel');
			
                            // Check for successful update
                            if ($vessel->update())
                            {	
                                    if($this->tools->getPost('vessel_seats'))
                                    {
                                        // reset counter    
                                        $total_seats = '';
                                        $total_column = 0;
                                        $total_row = count($this->tools->getPost('vessel_seats'));

                                        // delete all existing vessel seats
                                        $this->vessel_seats->delete(array('vessel_id' => $vessel->id));

                                        foreach($this->tools->getPost('vessel_seats') as $key => $value)
                                        {
                                            $column = 1;
                                            $total_column = count($value);
                                            foreach($value as $keyValue => $val)
                                            {
                                                $vessel_seats = new Vessel_Seats_Model();

                                                // count seats
                                                if($val == 'N/A' OR $val == "")
                                                {    
                                                    $val = 'N/A';
                                                    $accommodation_id = $this->tools->getPost('accommodation',$key,$column);
                                                }
                                                else
                                                {    
                                                    $total_seats++;
                                                    $accommodation_id = $this->tools->getPost('accommodation',$key,$column);
                                                }

                                                $add_data = array(
                                                                    'vessel_id'         => $vessel->id,
                                                                    'row'               => $key,
                                                                    'column'            => $column,
                                                                    'vessel_seats'      => $val,
                                                                    'accommodation_id'  => $accommodation_id,
                                                            );

                                                // insert vessel seats
                                                $vessel_seats->add($add_data);

                                                // increment column count
                                                $column++;
                                            }
                                        }
                                        // update vessel
                                        $this->vessel->update(
                                                                array(
                                                                        'id_vessel' => $vessel->id
                                                                ),
                                                                array(
                                                                        'total_seats'   => $total_seats,
                                                                        'total_row'     => $total_row,
                                                                        'total_column'  => $total_column)
                                                            );
                                    }

                                    parent::logThis($vessel->id,'Successfully updated vessel.');
                                    // Set confirmation message
                                    $this->session->set_flashdata('confirm', 'Successfully updated vessel.');
                                    $this->session->set_flashdata('id', $vessel->id);
                            }
                            else
                            {
                                    // Set confirmation message
                                    $this->session->set_flashdata('note', 'Error in updating vessel');
                            }
                            
                            redirect(admin_url($this->classname));
                        }
		}
	}
        
}

/* End of file vessel.php */
/* Location: ./application/modules_core/adminpanel/controllers/vessel/vessel.php */