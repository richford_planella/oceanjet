<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Administrator Forgot Password Class
|--------------------------------------------------------------------------
|
| Delegates functions for adminsitration forgot password records
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Forgot extends Admin_Core
{
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
               	$this->classname = strtolower(get_class());

		parent::__construct();
                
		// Check if admin is already logged in
		if ($this->session->get('admin','admin_login'))
			redirect(admin_url());

		// Load the models directly involve with the class 
		$this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('user/user_model'));
                $this->load->model(admin_dir('user/user_account_model'));
                $this->load->model(admin_dir('user/user_password_model'));
                $this->load->model(admin_dir('reset_password/reset_password_model'));
                
                $this->user_profile = new Profile_Model();
                $this->user = new User_Model();
                $this->user_account = new User_Account_Model();
                $this->user_password = new User_Password_Model();
                $this->reset_password = new Reset_Password_Model();
	}
	
	// ------------------------------------------------------------------------
	
	/*
	 * Shows the forgot password form for admins
	 *
	 * @access		public
	 * @return		void
	 */
	public function index()
	{
		// form validation
		self::_validate();
		
		$data = array(	'header' => parent::getTemplate(admin_dir('forgot/header')),
                                'footer' => parent::getTemplate(admin_dir('forgot/footer'), array('js_files' => array(js_dir('jquery','jquery.valid-employee-number-tools.js')))),
                        );
                
		$this->load->view(admin_dir('forgot/forgot'), $data);
	}

        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{		
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('email_address', 'Email Address', 'required|trim|email');
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$user_id = $this->user->getUserID(array('username' => $this->tools->getPost('username')));
			$user = new User_Model($user_id);
                        
			// handle brute force attacks
			sleep(1);
                        
			// check for record
			if (empty($user_id))
			{
				// Redirect with error if the condition above == FALSE
				$this->session->set_flashdata('note', ' Invalid username and/or e-mail address.');
				redirect(admin_url('forgot'));
			}
                        
                        // get user account details
                        $user->user_account = new User_Account_Model($user->id_user);
                        
			// check for email address
			$email = $user->user_account->email_address;

			if ($this->tools->getPost('email_address') != $email)
			{
				// Redirect with error if the condition above == FALSE
				$this->session->set_flashdata('note', ' Invalid username and/or e-mail address.');
				redirect(admin_url('forgot'));
			}
                        
                        $real_password = $this->misc->generate_code(5);
                        
                        $email_data = array(
                                                'user'          => $user,
                                                'employee_name' => $user->user_account->lastname.', '.$user->user_account->firstname,
                                                'username'      => $user->username,
                                                'password'      => $real_password,
                                            );
                        
                        // set email parameter
                        $subject = 'Forgot Password';
                        $body = parent::getTemplate(admin_dir('email_template/forgot_password'),$email_data);
                        
                        // send email
                        parent::sendMail($body,$subject,$user->user_account->email_address);
                        
                        // update user information
                        $secure_token = substr(sha1(microtime()),4,30);
                        $salt = $this->misc->generate_code(8);
                        $password = hash('sha256', $real_password.$salt);                       
                        
                        $user->update(array(),array(
                                                        'secure_token'      => $secure_token,
                                                        'salt'              => $salt,
                                                        'password'          => $password,
                                                        'is_new'            => 1,
                                                        'num_failed_login'  => 0,
                                                        'last_login'        => date("Y-m-d H:i:s"),
                                                    ));
                        
                        //insert user password
                        $this->user_password->add(array(
                                                            'user_id'   => $user->id,
                                                            'password'  => $password,
                                                            'salt'      => $salt,
                                                        ));
                        
                        // check if user exist on reset password
                        if($this->reset_password->displayList(array('user_id' => $user->id),array(),TRUE) > 0)
                        {
                            // delete existing record
                            $this->reset_password->delete(array('user_id' => $user->id));
                        }
                        
                        $date_now = strtotime(date('Y-m-d h:i'));
                        
                        //insert reset password
                        $this->reset_password->add(array(
                                                            'user_id'       => $user->id,
                                                            'value'         => $password,
                                                            'date_added'    => $date_now,
                                                            'exp_date'      => strtotime("+24 hours",$date_now),
                                                        ));
                        
                        
                        $this->session->set_flashdata('confirm', 'Password has been reset. Please check your email address.');
                        redirect(admin_url($this->classname));
		}
	}
}

/* End of file login.php */
/* Location: ./application/modules_core/adminpanel/login/login.php */