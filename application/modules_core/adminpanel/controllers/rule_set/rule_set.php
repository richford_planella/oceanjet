<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Rule Set Class
|--------------------------------------------------------------------------
|
| Rule Set Content Management
|
| @category	Rule Set Controller
| @author	Ronald Meran
*/
class Rule_Set extends Admin_Core
{
	/* const int free baggage */
	protected $free_baggage = 51;
	
	// ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
		// Rule Set Model
		$this->load->model(admin_dir('rule_set/rule_set_model'));
		$this->load->model(admin_dir('rule_set/rule_type_model'));
		$this->load->model(admin_dir('rule_set/rule_type_name_model'));
		
		// Initialize
		$this->rule_set = new Rule_Set_Model();
		$this->rule_type = new Rule_Type_Model();
		$this->rule_type_name = new Rule_Type_Name_Model();
						
		// Rule Set id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Rule Set Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		// Get all rule set
		$rule_set = $this->rule_set->displaylist();	
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Rule Set')),
			'footer'	=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.rule_set.js')))),
			'rule_set'	=> $rule_set,
		);
		
		parent::displayTemplate(admin_dir('rule_set/rule_set'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Rule Type Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// Initialize
		$rule_set 	= new Rule_Set_Model($this->id);
		
		// Check if a record exists
		$rule_set->redirectIfEmpty(admin_url($this->classname));
		
		// Join rule type
		$rule_type_set = $this->_join_rule_type_set();

		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'View Rule Set')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'rule_type_name' => $this->rule_type_name->displayList(),
			'rule_set'		=> $rule_set,
			'rule_type_set'	=> $rule_type_set,
			'free_baggage' => $this->free_baggage
		);
		
		parent::displayTemplate(admin_dir('rule_set/form/rule_set'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get announcement
		$rule_set = new Rule_Set_Model($this->id);
		
		// Check if a record exists
		$rule_set->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($rule_set->toggleStatus())
		{
			// Check if status is in the query
			$toggle = isset($rule_set->enabled) ? $rule_set->enabled : FALSE;
			$rule_set_name = isset($rule_set->rule_set) ? $rule_set->rule_set : 'rule set';
			
			// Set confirmation message
			if($toggle)
				$this->session->set_flashdata('confirm', ucwords($rule_set_name).' has been de-activated');
			else
				$this->session->set_flashdata('confirm', ucwords($rule_set_name).' has been re-activated');
				
			$this->session->set_flashdata('id', $rule_set->id);
		}
		else
		{
			// Set confirmation message
			$this->session->set_flashdata('note', 'Error in updating rule set.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Rule Set
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
		
		// Initialize data
		$data = array(
			'header'		=> Modules::run(admin_dir('header/call_header'), array('title' => 'Create Rule Set')),
			'footer'			=> parent::getTemplate(admin_dir('footer'), 
										array("js_files" => array(
											js_dir('jquery', 'jquery.formatCurrency.js'), 
											js_dir('jquery', 'jquery.rule_set.js'), 
											js_dir('jquery', 'jquery.numeric-tools.js')))),
			'rule_type_name'	=> $this->rule_type_name->displayList(),
			'free_baggage' => $this->free_baggage
		);
			
		parent::displayTemplate(admin_dir('rule_set/form/add/rule_set'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Rule Set
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// Initialize
		$rule_set 	= new Rule_Set_Model($this->id);
		
		// Check if a record exists
		$rule_set->redirectIfEmpty(admin_url($this->classname));
		
		// Join rule type
		$rule_type_set = $this->_join_rule_type_set();		
		
		// Form validation
		self::_validate();
		
		// Get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Edit Rule Set')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), 
									array("js_files" => array(
										js_dir('jquery', 'jquery.formatCurrency.js'), 
										js_dir('jquery', 'jquery.rule_set.js'), 
										js_dir('jquery', 'jquery.numeric-tools.js')))),
			'rule_type_name'	=> $this->rule_type_name->displayList(),
			'rule_set'			=> $rule_set,
			'rule_type_set'	=> $rule_type_set,
			'free_baggage' => $this->free_baggage
		);
		
		parent::displayTemplate(admin_dir('rule_set/form/edit/rule_set'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Delete rule set
	 *
	 * @access	public
	 * @return		void
	 */
	public function delete()
	{
		// Get rule set
		$rule_set = new Rule_Set_Model($this->id);
		
		// Check if a record exists
		$rule_set->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if($rule_set->countForeignKey() > 0)
		{	
			// Set confirmation message
			$this->session->set_flashdata('note', 'Cannot delete. '.ucwords($rule_set->rule_set).' was already used in the system');
		}
		else
		{	
			  // Delete Rule Set child
            $rule_type = new Rule_Type_Model();
            $rule_type->delete(array("rule_set_id" => $this->id));
			
			// Set confirmation message
			if($rule_set->delete())
				$this->session->set_flashdata('confirm', 'Successfully deleted '.ucwords($rule_set->rule_set));
			else
				$this->session->set_flashdata('note', "Error deleting outlet");
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		if ($this->uri->rsegment(2) == 'add'){
			$this->form_validation->set_rules('rule_code', 'rule code', 'required|trim|is_unique[rule_set.rule_code]');
			$this->form_validation->set_rules('rule_set', 'rule name', 'required|trim');
		}

		$this->form_validation->set_rules('enabled', 'status', 'required');			
		
		if($this->tools->getPost('rule_type'))
			foreach($this->tools->getPost('rule_type') as $key => $value){
				if(isset($value["id"])){
					// Checkbox
					$this->form_validation->set_rules("rule_type[".$key."][id]", 'rule type', 'required');
					
					if($key == $this->free_baggage){
						$this->form_validation->set_rules("rule_type[".$key."][amount]", 'weight', 'required');
					}
					else{					
						$this->form_validation->set_rules("rule_type[".$key."][amount]", 'amount', 'required');
						$this->form_validation->set_rules("rule_type[".$key."][amount_type]", 'rule amount type', 'required');					
					}
				}
			}
                             
		if ($this->uri->rsegment(2) == 'add')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then add Rule Set
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{    
		// Initialize
		$rule_set 			= new Rule_Set_Model();
		$rule_type 		= new Rule_Type_Model();
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			// Add rule set data
			$data = array("rule_code" 	=> strtoupper($this->tools->getPost("rule_code")), 
						  "rule_set" 		=> $this->tools->getPost("rule_set"),
						  "enabled" 		=> $this->tools->getPost("enabled"),
						  "date_added" 	=> date("Y-m-d H:i:s"),
						  "date_update" 	=> date("Y-m-d H:i:s"));
		
			// Check for successful insert			
			if($_rule_set = $rule_set->add($data))
			{
				// Join Rule Type Name and Rules
				$rtype = $this->join_post_rules($_rule_set);
			
				// Add Rule Types
				foreach($rtype as $rules)
					$rule_type->add($rules);
			
				// Log announcement
				parent::logThis($rule_set->id, 'Successfully added rule set');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added rule set');
				$this->session->set_flashdata('id', $rule_set->id);	
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving rule set');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
   // --------------------------------------------------------------------
	
	/*
	 * Validate then update Rule Set
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		// Initialize
		$rule_set 		= new Rule_Set_Model($this->id);
		$rule_type 		= new Rule_Type_Model();
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{			
			// Join Rule Type Name and Rules
			$rtype = $this->join_post_rules(FALSE);
				
			// Delete data
			$data = array("rule_set_id" => $rule_set->id);
			$rule_type->delete($data);	
				
			// Add Rule Set
			foreach($rtype as $rules)
				$rule_type->add($rules);
					
			// Check for successful update
			if ($rule_set->update(array("date_update" => date("Y-m-d H:i:s"))))
			{
				parent::logThis($rule_set->id, 'Successfully updated rule set');
									
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated rule set');
				$this->session->set_flashdata('id', $rule_set->id);
				
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating rule set');
			}
			
			redirect(admin_url($this->classname));
		}
	}
	
	 // --------------------------------------------------------------------
	
	/*
	 * Join Rule Type and Set
	 *
	 * @access	protected
	 * @return		array
	 */
	protected function _join_rule_type_set()
	{
		// Rule Type
		$rule_type = new Rule_Type_Model();
	
		// Display Rule Type
		$rule_type = $rule_type->displayList(array("rule_set_id" => $this->id));
		
		// Rule Types
		foreach($this->rule_type_name->displayList() as $rkey => $rvalue){
			
			$rtype[$rvalue->id_rule_type_name] = $rvalue;
			
			foreach($rule_type as $tkey => $tvalue){
				if($rvalue->id_rule_type_name == $tvalue->rule_type_name_id)
					$rtype[$rvalue->id_rule_type_name] = $tvalue;
			}
		}
		
		return $rtype;
	}
	
	 // --------------------------------------------------------------------
	
	/*
	 * Join Rule Type Name and Rules
	 *
	 * @access	protected
	 * @return		array
	 */
	protected function join_post_rules($id_rule_set = '')
	{
		 foreach($this->tools->getPost("rule_type") as $rkey => $rvalue){
			if(isset($rvalue["id"])){
				$rtype[$rkey]["rule_set_id"] 			= !empty($id_rule_set) ? $id_rule_set : $this->id;
				$rtype[$rkey]["rule_type_name_id"] 		= $rkey;
				$rtype[$rkey]["rule_type_amount"] 		= isset($rvalue["amount"]) ? $rvalue["amount"] : 0;
				$rtype[$rkey]["rule_amount_type"] 		= isset($rvalue["amount_type"]) ? $rvalue["amount_type"] : 0;
			}
		 }

		 return $rtype;
	}
   
}

/* End of file rule_set.php */
/* Location: ./application/modules_core/adminpanel/controllers/rule_set/rule_set.php */