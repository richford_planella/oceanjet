<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Class
|--------------------------------------------------------------------------
|
| Outlet Content Management
|
| @category Controller
| @author       Ronald Meran
*/
class Refund_Eticket extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {


        // Classname
        $this->classname = strtolower(get_class());
       
        parent::__construct();

        $this->load->model(admin_dir('third_party_outlet/refund_eticket_model'));
        $this->load->model(admin_dir('booking/booking_model'));   
        $this->load->model(admin_dir('trip_type/trip_type_model'));
        $this->load->model(admin_dir('port/port_model')); 
        $this->load->model(admin_dir('voyage/voyage_model'));   
        $this->load->model(admin_dir('accommodation/accommodation_model')); 
        $this->load->model(admin_dir('refund_tickets/refund_tickets_model'));
        $this->load->model(admin_dir('rule_set/rule_type_model'));         
        $this->refund_eticket = new Refund_Eticket_Model();
        $this->trip_type = new Trip_Type_Model();
        $this->voyage = new Voyage_Model();
        $this->accommodation = new Accommodation_Model();
        $this->refund_tickets = new Refund_Tickets_Model();
        $this->port = new Port_Model();
        $this->booking = new Booking_Model();
        $this->rule_type = new Rule_Type_Model();
        // $this->load->model(admin_dir('/Eticket_Model'));
                        
        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Outlet Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
 
        // $eticket = new Eticket_Model(1);
        // echo '<pre>';
        // print_r($this->rule_type->displayList());exit;
        // Check if a record exists
        // $eticket_passenger->redirectIfEmpty(admin_url($this->classname));
          self::_validate();
        
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Refund E-Ticket')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.refund-eticket.js')))),
            'refund_eticket' => $this->refund_eticket->displayList(array("booking_source_id"=>3,"booking_status_id"=>3))
            
        );
        
        parent::displayTemplate(admin_dir('third_party_outlet/refund_eticket/refund_eticket'),$data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $eticket = new Refund_Eticket_Model($this->id);
                        
        // Check if a record exists
        $eticket->redirectIfEmpty(admin_url($this->classname));
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Refund E-Ticket')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'eticket'        => $eticket->get_eticket_details($this->id),
            'port' => $this->port->displayList()
        );
        
        
        parent::displayTemplate(admin_dir('third_party_outlet/refund_eticket/form/refund_eticket'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $accommodation = new Accommodation_Model($this->id);
        
        // Check if a record exists
        $accommodation->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($accommodation->toggleStatus())
        {   
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating accommodation.');
            $this->session->set_flashdata('id', $accommodation->id);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating accommodation.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();
                            
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Refund E-Ticket')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.eticket-passenger.js')))),
            'trip_type' => $this->trip_type->displayList(),
            'accommodation' => $this->accommodation->displayList(),
            'voyage' => $this->voyage->displayList()
        );
        

        parent::displayTemplate(admin_dir('third_party_outlet/eticket_passenger/form/add/eticket_passenger'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get user
       // $eticket = new Refund_Eticket_Model($this->id);
        
        // Check if a record exists
        //$eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.refund-eticket.js')))),
            // 'eticket'        => $eticket->get_eticket_details($this->id),
            'port' => $this->port->displayList()
        );
        
        
        parent::displayTemplate(admin_dir('third_party_outlet/refund_eticket/form/edit/refund_eticket'),$data);
    }

    public function edit_info() {
       
         // if (isset($_POST)) {
            // update booking
            $this->booking->update(array("id_booking"=>$this->id),array("booking_status_id" => 3));
            // insert to request refund
            $this->refund_tickets->add(array("booking_id"=>$this->id,"date_refunded"=>date("Y-m-d H:i:s"),"refund_ticket_status_id"=>$this->id,));
            // redirec to home
            redirect(admin_url($this->classname));


          
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        //$this->form_validation->set_rules('last_name', 'lastname', 'required|trim');
        // $this->form_validation->set_rules('eticket_terms', 'Terms and Conditions', 'required|trim');
        // $this->form_validation->set_rules('eticket_reminders', 'Reminders', 'required|trim');
        // if ($this->uri->rsegment(1) == 'refund_eticket')
        //     self::_addInfo();
            
        if ($this->uri->rsegment(1) == 'refund_eticket')

            self::_editInfo();
    }

        
// --------------------------------------------------------------------
    
    /*
     * Validate then add Outlet
     *
     * @return      void
     */
    private function _addInfo() 
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $eticket_passenger = new Eticket_Passenger_Model();
            
            parent::copyFromPost($eticket_passenger, 'id_ticket');
                        
            // Check for successful insert
            if ($eticket_passenger->add())
            {
                parent::logThis($accommodation->id, 'Successfully added Accommodation');
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added Accommodation');
                $this->session->set_flashdata('id', $outlet->id);   
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving Accommodation');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        // print "test";
        // print '<pre>'.print_r($this->tools->getPost(),true).'</pre>';exit;
        
        $eticket = new Booking_Model($this->tools->getPost("booking_id"));

        // Check if form validation is TRUE
         if ($this->form_validation->run() == TRUE)
         {
    
             parent::copyFromPost($eticket, 'id_booking');
            
            // Check for successful update
            if ($eticket->update(array(),array("booking_status_id" => 3)))
            {                                   
                 parent::logThis($eticket->id, 'Successfully updated Eticket');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Eticket');
                $this->session->set_flashdata('id', $eticket->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Eticket');
            }
            $insert_data = array("booking_id"=>$this->tools->getPost("booking_id"),"refund_ticket_status_id" => 1,"date_refunded"=>date("Y-m-d H:i:s"),"refund_amount" => $this->tools->getPost("refund_amount"));
            // insert to refund table
            $this->refund_tickets->add($insert_data);
            redirect(admin_url($this->classname));
         }
    }

    public function get_issued_eticket_list() {

        $data_rules = array();
        $test = array();

        
        $data_discount = $this->refund_eticket->get_eticket_list($this->tools->getGet());
        foreach($data_discount as $dt) {
            $data_rules['fullname'] = $dt->firstname." ".$dt->lastname;
            $data_rules['voyage'] = $dt->voyage;
            $data_rules['departure_date'] = $dt->departure_date;
            $data_rules['origin'] = $dt->origin;
            $data_rules['destination'] = $dt->destination;
            $data_rules['ETD'] = $dt->ETD;
            $data_rules['ETA'] = $dt->ETA;
            $data_rules['vessel'] = $dt->vessel;
            $data_rules['id_booking'] = $dt->id_booking;
            $data_rules['accommodation'] = $dt->accommodation;
            $data_rules['description'] = $dt->description;
            $data_rules['total_amount'] = $dt->total_amount;
            $data_rule_type = $this->rule_type->displayList(array("rule_set_id" => $dt->rule_set_id));
        }
        foreach ($data_rule_type as $drt) {
            $data_rules['rule_code'] = $drt->rule_code;
            $data_rules['rule_type_amount'][] = $drt->rule_type_amount; 
        }
        $test[] = $data_rules;

        echo json_encode($test);
    }

}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */