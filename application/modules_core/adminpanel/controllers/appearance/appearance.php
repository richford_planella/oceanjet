<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Appearance Class
|--------------------------------------------------------------------------
|
| Modify permissions of appearance
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Appearance extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();
		
                $this->load->model(admin_dir('configuration/configuration_model'));
                $this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('user/user_model'));
                $this->load->model(admin_dir('user/user_account_model'));
                
                $this->configuration = new Configuration_Model();
                $this->user_profile = new Profile_Model();
                $this->user = new User_Model();
                $this->user_account = new User_Account_Model();
                
                // if session has value
                if( ! $this->session->get('admin', 'admin_login') OR $this->session->get('admin', 'encryption_key') != $this->config->item('encryption_key'))
                {
                        // redirect to dashboard
                        redirect(admin_url());
                }
                
                // user ID
		$this->id = $this->session->get('admin', 'admin_id');
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display group billing list
	 *
	 * @access		public       
	 * @return		void
	 */
        public function index()
        {               
                // validate form
                self::_validate();
                                
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Appearance')),
                                'footer'        => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.employee-data-tools.js')))),
                                'site_title'   => $this->configuration->getValue('value', array('configuration' => 'SITE_TITLE')),
                );

                parent::displayTemplate(admin_dir('appearance/form/appearance'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{
            $this->form_validation->set_rules('value', 'Site Title', 'required|trim|xss_clean');
            
            self::_editInfo();
        }
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit upload directory
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
            // Check if form validation is TRUE
            if ($this->form_validation->run() == TRUE)
            {
                // update upload directory configuration
                $this->configuration->update(array('configuration' => 'SITE_TITLE'),array('value' => $this->tools->getPost('value')));
                
                parent::logThis(0,'Change Site Title');
                
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Site title updated.');
                redirect(admin_url($this->classname));
            }
        }
        
}

/* End of file upload_csv.php */
/* Location: ./application/modules_core/adminpanel/controllers/upload_csv/upload_csv.php */