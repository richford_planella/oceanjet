<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Padala Cargo Rate Class
|--------------------------------------------------------------------------
|
| Padala Cargo Rate Content Management
|
| @category Controller
| @author       Kenneth Bahia
*/
class Padala_Cargo extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('padala_cargo/Padala_Cargo_Model'));
        $this->load->model(admin_dir('port/Port_Model'));

        $this->padala_cargo = new Padala_Cargo_Model();
        $this->port = new Port_Model();
                        
        // Padala Cargo Rate id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Padala Cargo Rate Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        // Get all padala cargo rate
        $padala_cargo = $this->padala_cargo->displaylist();
        
        // Initialize data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Padala Cargo Rate')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.padala-cargo.js')))),
            'padala_cargo'  => $padala_cargo,
        );
        
        parent::displayTemplate(admin_dir('padala_cargo/padala_cargo'), $data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Padala Cargo Rate Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $padala_cargo = new Padala_Cargo_Model($this->id);
                        
        // Check if a record exists
        $padala_cargo->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'View Padala Cargo Rate')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'padala_cargo'  => $padala_cargo,
            'port_list'     => $this->port->displayList()
        );
        
        parent::displayTemplate(admin_dir('padala_cargo/form/padala_cargo'),$data);
    }

        // --------------------------------------------------------------------
    
    /*
     * Delete padala cargo rate
     *
     * @access      public
     * @return      void
     */
    public function delete()
    {
        // get padala cargo rate
        $padala_cargo = new Padala_Cargo_Model($this->id);
        
        // Check if a record exists
        $padala_cargo->redirectIfEmpty(admin_url($this->classname));
        
        // Check if it is already in use
        if ($padala_cargo->countFromCargoDescription() > 0)
        {   
            // Set confirmation message
            $this->session->set_flashdata('note', 'Cannot delete. '.$padala_cargo->padala_cargo.' was already used in the system.');
        }
        else
        {
            //Delete padala cargo rate
            if ($padala_cargo->delete())
                $this->session->set_flashdata('confirm', 'Successfully deleted '.$padala_cargo->padala_cargo);
            else
                $this->session->set_flashdata('note', 'Error deleting padala cargo rate');
        }
        
        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $padala_cargo = new Padala_Cargo_Model($this->id);
        
        // Check if a record exists
        $padala_cargo->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($padala_cargo->toggleStatus())
        {   
            // Set confirmation message
            if($padala_cargo->enabled)
                $this->session->set_flashdata('confirm', $padala_cargo->padala_cargo.' has been de-activated!');
            else
                $this->session->set_flashdata('confirm', $padala_cargo->padala_cargo.' has been re-activated!');
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating '.$padala_cargo->padala_cargo.'.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Padala Cargo Rate
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Create Padala Cargo Rate')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.padala-cargo.js')))),
            'port_list'     => $this->port->displayList()
        );
            
        parent::displayTemplate(admin_dir('padala_cargo/form/add/padala_cargo'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Padala Cargo Rate
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get user
        $padala_cargo = new Padala_Cargo_Model($this->id);
        
        // Check if a record exists
        $padala_cargo->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit Padala Cargo Rate')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.padala-cargo.js')))),
            'padala_cargo'  => $padala_cargo,
            'port_list'     => $this->port->displayList()
        );
        
        parent::displayTemplate(admin_dir('padala_cargo/form/edit/padala_cargo'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('description', 'Description', 'required|trim');
        $this->form_validation->set_rules('origin_id', 'Origin', 'required|trim');
        $this->form_validation->set_rules('destination_id', 'Destination', 'required|trim');
        $this->form_validation->set_rules('amount', 'Amount', 'required|numeric|decimal');
        $this->form_validation->set_rules('unit_of_measurement', 'Unit of Measurement', 'required|trim');
        $this->form_validation->set_rules('enabled', 'Status', 'required');
                             
        if ($this->uri->rsegment(2) == 'add') {
            $this->form_validation->set_rules('padala_cargo', 'Padala Cargo Rate Code', 'required|trim|is_unique[padala_cargo.padala_cargo]');
            self::_addInfo();
        }
            
        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Padala Cargo Rate
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $padala_cargo = new Padala_Cargo_Model();
            
            parent::copyFromPost($padala_cargo, 'id_padala_cargo');
            $padala_cargo->padala_cargo = strtoupper($padala_cargo->padala_cargo);
                        
            // Check for successful insert
            if ($padala_cargo->add())
            {
                parent::logThis($padala_cargo->id, 'Successfully added padala cargo rate');
                
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added padala cargo rate');
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving padala cargo rate');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit Padala Cargo Rate
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $padala_cargo = new Padala_Cargo_Model($this->id);

        if (trim($this->tools->getPost("padala_cargo")) == trim($padala_cargo->padala_cargo)) {
            $this->form_validation->set_rules('padala_cargo', 'Padala Cargo Rate Code', 'required|trim');
        } else {
            $this->form_validation->set_rules('padala_cargo', 'Padala Cargo Rate Code', 'required|trim|is_unique[padala_cargo.padala_cargo]');
        }

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($padala_cargo, 'id_padala_cargo');
            $padala_cargo->padala_cargo = strtoupper($padala_cargo->padala_cargo);
            
            // Check for successful update
            if ($padala_cargo->update())
            {                                   
                 parent::logThis($padala_cargo->id, 'Successfully updated padala cargo rate');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated padala cargo rate');
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating padala cargo rate');
            }
            
            redirect(admin_url($this->classname));
        }
    }
   
}

/* End of file padala_cargo.php */
/* Location: ./application/modules_core/adminpanel/controllers/padala_cargo/padala_cargo.php */