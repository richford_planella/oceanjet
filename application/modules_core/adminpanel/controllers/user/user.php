<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Users Class
  |--------------------------------------------------------------------------
  |
  | Modify permissions of users
  |
  | @category		Controller
  | @author		Baladeva Juganas
 */

class User extends Admin_Core
{
    // ------------------------------------------------------------------------

    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());

        parent::__construct();

        $this->load->model(admin_dir('user/profile_model'));
        $this->load->model(admin_dir('user/user_model'));
        $this->load->model(admin_dir('user/user_account_model'));
        $this->load->model(admin_dir('user/user_level_model'));
        $this->load->model(admin_dir('user/user_profile_level_model'));
        $this->load->model(admin_dir('user/user_password_model'));
        $this->load->model(admin_dir('reset_password/reset_password_model'));
        $this->load->model(admin_dir('outlet/outlet_model'));

        $this->user_profile = new Profile_Model();
        $this->user = new User_Model();
        $this->user_account = new User_Account_Model();
        $this->user_level = new User_Level_Model();
        $this->user_profile_level = new User_Profile_Level_Model();
        $this->user_password = new User_Password_Model();
        $this->reset_password = new Reset_Password_Model();
        $this->outlet = new Outlet_Model();

        // user ID
        $this->id = $this->uri->rsegment(3);
    }

    // --------------------------------------------------------------------

    /*
     * Display user list
     *
     * @access		public
     * @return		void
     */
    public function index()
    {
        // Get all user
        $user = $this->user->displaylist();

        // get user account details
        foreach ($user as $u)
            $u->user_account = new User_Account_Model($u->id_user);

        // get outlet details
        foreach ($user as $u)
            $u->user_account->outlet = $this->outlet->getValue('outlet', array('id_outlet' => $u->user_account->outlet_id));

        // get data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'User Accounts')),
            'footer' => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.users-tools.js')))),
            'user' => $user,
        );

        parent::displayTemplate(admin_dir('user/user/user'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * View user info
     *
     * @access		public
     * @return		void
     */
    public function view()
    {
        // get user
        $user = new User_Model($this->id);

        // Check if a record exists
        $user->redirectIfEmpty(admin_url($this->classname));

        // get user account details
        $user->user_account = new User_Account_Model($user->id_user);

        // get user profile
        $user->user_profile = $this->user_profile->getValue('user_profile', array('id_user_profile' => $user->user_profile_id));

        $user_profile = 0;

        if ($this->user_profile->getValue('is_outlet', array('id_user_profile' => $user->user_profile_id)) == 1)
            $user_profile = 1;


        // get data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'User Accounts')),
            'footer' => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.user-tools.js')))),
            'user_profile' => $this->user_profile->displayList(array('enabled' => 1)),
            'user' => $user,
            'outlet' => $this->outlet->displayList(array('u.enabled' => 1)),
            'user_type' => $user_profile,
        );

        parent::displayTemplate(admin_dir('user/user/form/user'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Reset user password
     *
     * @access		public
     * @return		void
     */
    public function reset()
    {
        // get user
        $user = new User_Model($this->id);

        // Check if a record exists
        $user->redirectIfEmpty(admin_url($this->classname));

        // user account
        $user->user_account = new User_Account_Model($user->id_user);

        $real_password = $this->misc->generate_code(5);

        $email_data = array(
            'user' => $user,
            'employee_name' => $user->user_account->lastname . ', ' . $user->user_account->firstname,
            'username' => $user->username,
            'password' => $real_password,
        );

        // set email parameter
        $subject = 'Forgot Password';
        $body = parent::getTemplate(admin_dir('email_template/forgot_password'), $email_data);

        // send email
        parent::sendMail($body, $subject, $user->user_account->email_address);

        // update user information
        $secure_token = substr(sha1(microtime()), 4, 30);
        $salt = $this->misc->generate_code(8);
        $password = hash('sha256', $real_password . $salt);

        $user->update(array(), array(
            'secure_token' => $secure_token,
            'salt' => $salt,
            'password' => $password,
            'is_new' => 1,
            'is_locked' => 0,
            'num_failed_login' => 0,
            'password_generated' => date("Y-m-d"),
            'last_login' => date("Y-m-d H:i:s"),
        ));

        //insert user password
        $this->user_password->add(array(
            'user_id' => $user->id,
            'password' => $password,
            'salt' => $salt,
        ));

        // check if user exist on reset password
        if ($this->reset_password->displayList(array('user_id' => $user->id), array(), TRUE) > 0)
        {
            // delete existing record
            $this->reset_password->delete(array('user_id' => $user->id));
        }

        $date_now = strtotime(date('Y-m-d h:i'));

        //insert reset password
        $this->reset_password->add(array(
            'user_id' => $user->id,
            'value' => $password,
            'date_added' => $date_now,
            'exp_date' => strtotime("+24 hours", $date_now),
        ));

        $this->session->set_flashdata('confirm', 'Password has been reset.');
        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------

    /*
     * Toggle user status
     *
     * @access		public
     * @return		void
     */
    public function toggle()
    {
        // get user
        $user = new User_Model($this->id);

        // Check if a record exists
        $user->redirectIfEmpty(admin_url($this->classname));

        // Check for successful toggle
        if ($user->toggleStatus())
        {
            // Set confirmation message
            if ($user->enabled)
                $this->session->set_flashdata('confirm', $user->username . ' has been de-activated!');
            else
                $this->session->set_flashdata('confirm', $user->username . ' has been re-activated!');
            $this->session->set_flashdata('id', $user->id);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating user.');
        }

        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------

    /*
     * Toggle user status
     *
     * @access		public
     * @return		void
     */
    public function toggleLocked()
    {
        // get user
        $user = new User_Model($this->id);

        // Check if a record exists
        $user->redirectIfEmpty(admin_url($this->classname));

        // Check for successful toggle
        if ($user->toggleLocked())
        {
            // Set confirmation message
            if ($user->is_locked)
                $this->session->set_flashdata('confirm', $user->username . ' has been unlocked!');
            else
                $this->session->set_flashdata('confirm', $user->username . ' has been locked!');
            $this->session->set_flashdata('id', $user->id);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating user.');
        }

        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------

    /*
     * Add user
     *
     * @access		public
     * @return		void
     */
    public function add()
    {
        $user_profile = 0;

        if ($this->tools->getPost('user_profile_id'))
            if ($this->user_profile->getValue('is_outlet', array('id_user_profile' => $this->tools->getPost('user_profile_id'))) == 1)
                $user_profile = 1;

        // Form validation
        self::_validate();

        // get data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'User Accounts')),
            'footer' => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.user-tools.js')))),
            'user_profile' => $this->user_profile_level->getUserProfileLevel($this->session->get('admin', 'administrator_profile_id')),
            'outlet' => $this->outlet->displayList(array('u.enabled' => 1)),
            'user_type' => $user_profile,
        );

        parent::displayTemplate(admin_dir('user/user/form/add/user'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Edit user
     *
     * @access		public
     * @return		void
     */
    public function edit()
    {
        // get user
        $user = new User_Model($this->id);

        // Check if a record exists
        $user->redirectIfEmpty(admin_url($this->classname));

        // get user account details
        $user->user_account = new User_Account_Model($user->id_user);

        // get user profile
        $user->user_profile = $this->user_profile->getValue('user_profile', array('id_user_profile' => $user->user_profile_id));

        // get user level
        $user_level_id = $this->user_profile->getValue('user_level_id', array('id_user_profile' => $user->user_profile_id));

        if ($this->user_profile_level->checkUserLevel($this->session->get('admin', 'administrator_profile_id'), $user_level_id) < 1)
        {
            $this->session->set_flashdata('note', 'Access denied, you cannot modify user account.');
            redirect(admin_url($this->classname));
        }

        $user_profile = 0;

        if ($this->tools->getPost('user_profile_id'))
        {
            if ($this->user_profile->getValue('is_outlet', array('id_user_profile' => $this->tools->getPost('user_profile_id'))) == 1)
                $user_profile = 1;
        }
        else
        if ($this->user_profile->getValue('is_outlet', array('id_user_profile' => $user->user_profile_id)) == 1)
            $user_profile = 1;


        // Form validation
        self::_validate();

        // get data
        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'), array('title' => 'User Accounts')),
            'footer' => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.user-tools.js'), js_dir('jquery', 'jquery.valid-employee-number-tools.js')))),
            'user_profile' => $this->user_profile_level->getUserProfileLevel($this->session->get('admin', 'administrator_profile_id')),
            'outlet' => $this->outlet->displayList(array('u.enabled' => 1)),
            'user' => $user,
            'user_type' => $user_profile,
        );

        parent::displayTemplate(admin_dir('user/user/form/edit/user'), $data);
    }

    // --------------------------------------------------------------------

    /*
     * Validate the form
     *
     * @access		private
     * @return		void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('user_profile_id', 'user role', 'required|trim|integer');
        $this->form_validation->set_rules('lastname', 'last name', 'required|trim|max_length[64]');
        $this->form_validation->set_rules('firstname', 'first name', 'required|trim|max_length[64]');
        $this->form_validation->set_rules('middlename', 'middle name', 'trim|max_length[64]');

        //echo $this->tools->getPost('user_type_id');
        if ($this->tools->getPost('user_profile_id'))
            if ($this->user_profile->getValue('is_outlet', array('id_user_profile' => $this->tools->getPost('user_profile_id'))))
                $this->form_validation->set_rules('outlet_id', 'outlet', 'required|integer|trim');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();


        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }

    // --------------------------------------------------------------------

    /*
     * Validate then add user profile
     *
     * @access		private
     * @return		void
     */
    private function _addInfo()
    {
        $this->form_validation->set_rules('username', 'username', 'required|trim|min_length[4]|max_length[64]|is_unique[user.username]');
        $this->form_validation->set_rules('email_address', 'email address', 'trim|valid_email|is_unique[user_account.email_address]');

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            //$employee_num = $this->tools->getPost('employee_num');
            $user = new User_Model();
            $user_account = new User_Account_Model();

            $real_password = $this->misc->generate_code(7);

            $secure_token = substr(sha1(microtime()), 4, 30);
            $salt = $this->misc->generate_code(8);
            $this->tools->setPost('secure_token', $secure_token);
            $this->tools->setPost('salt', $salt);
            $this->tools->setPost('enabled', 1);
            $this->tools->setPost('is_deleted', 0);
            $this->tools->setPost('is_locked', 0);
            $this->tools->setPost('is_new', 1);
            $this->tools->setPost('num_failed_login', 0);
            $this->tools->setPost('password', hash('sha256', $real_password . $salt));
            $this->tools->setPost('password_generated', date("Y-m-d"));
            $this->tools->setPost('last_login', date("Y-m-d H:i:s"));

            parent::copyFromPost($user, 'id_user');

            // Check for successful insert
            if ($user->add())
            {
                // add user account
                $this->tools->setPost('user_id', $user->id);
                $this->tools->setPost('profile_pic', '');

                parent::copyFromPost($user_account, 'id_user_account');

                //insert user password
                $this->user_password->add(array(
                    'user_id' => $user->id,
                    'password' => hash('sha256', $real_password . $salt),
                    'salt' => $salt,
                ));

                $date_now = strtotime(date('Y-m-d h:i'));

                //insert reset password
                $this->reset_password->add(array(
                    'user_id' => $user->id,
                    'value' => hash('sha256', $real_password . $salt),
                    'date_added' => $date_now,
                    'exp_date' => strtotime("+24 hours", $date_now),
                ));

                if ($user_account->add())
                {
                    // create folder for user
                    if (!is_dir(FCPATH . 'assets/img/users/' . $user->id))
                    {
                        mkdir(FCPATH . 'assets/img/users/' . $user->id);
                    }

                    // check if images exist
                    if ($_FILES['photo']['name'])
                    {
                        $filename = $_FILES['photo']['name'];
                        $fileExt = end(explode(".", $filename));
                        $newFileName = md5(time()) . "." . $fileExt;

                        $filepath = FCPATH . 'assets/img/users/' . $user->id;

                        $file_info = parent::uploadImg($filepath, $newFileName);

                        // update user account profile pic
                        $user_account = new User_Account_Model($user_account->id);

                        $user_account->update(array(), array('profile_pic' => $newFileName));
                    }

                    // get user
                    $users = new User_Model($user->id);

                    // get user account details
                    $users->user_account = new User_Account_Model($users->id_user);

                    $email_data = array(
                        'user' => $users,
                        'employee_name' => $users->user_account->lastname . ', ' . $users->user_account->firstname,
                        'username' => $users->username,
                        'password' => $real_password,
                    );

                    // set email parameter
                    $subject = 'New User Account';
                    $body = parent::getTemplate(admin_dir('email_template/create_account'), $email_data);

                    // send email
                    parent::sendMail($body, $subject, $users->user_account->email_address);

                    parent::logThis($users->id_user, 'Successfully added user account!');
                    // Set confirmation message
                    $this->session->set_flashdata('confirm', 'Successfully added user account!');
                    $this->session->set_flashdata('id', $user->id);
                } else
                {
                    // Delete record of User Account
                    $user->delete();
                    // Set confirmation message
                    $this->session->set_flashdata('error', 'Error in saving user account');
                }
            } else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving user account');
            }
            redirect(admin_url($this->classname));
        }
    }

    // --------------------------------------------------------------------

    /*
     * Validate then edit user
     *
     * @access		private
     * @return		void
     */
    private function _editInfo()
    {
        $user = new User_Model($this->id);

        // get user account details
        $user->user_account = new User_Account_Model($user->id_user);

        // check if employee number doesn't change
        if ($this->user->getValue('id_user', array('username' => $this->tools->getPost('username'))) == $this->id)
            $this->form_validation->set_rules('username', 'username', 'required|trim|min_length[4]|max_length[64]');
        else
            $this->form_validation->set_rules('username', 'username', 'required|trim|min_length[4]|max_length[64]|is_unique[user.username]');

        // check if email address doesn't change
        if ($this->user_account->getValue('user_id', array('email_address' => $this->tools->getPost('email_address'))) == $user->user_account->email_address)
            $this->form_validation->set_rules('email_address', 'email address', 'trim|valid_email|is_unique[user_account.email_address]');
        else
            $this->form_validation->set_rules('email_address', 'email address', 'trim|valid_email');

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            parent::copyFromPost($user, 'id_user');

            // Check for successful update
            if ($user->update())
            {
                $user_account = new User_Account_Model($this->user_account->getValue('id_user_account', array('user_id' => $user->id)));
                // update user account
                // check if user_profile_id <> 3
                if ($this->user_profile->getValue('is_outlet', array('id_user_profile' => $this->tools->getPost('user_profile_id'))) == 0)
                    $this->tools->setPost('outlet_id', 0);

                parent::copyFromPost($user_account, "id_user_account");

                if ($user_account->update())
                {
                    // create folder for user
                    if (!is_dir(FCPATH . 'assets/img/users/' . $user->id))
                    {
                        mkdir(FCPATH . 'assets/img/users/' . $user->id);
                    }

                    // check if images exist
                    if ($_FILES['photo']['name'])
                    {
                        // update user account profile pic
                        $user_account = new User_Account_Model($user_account->id);

                        $filename = $_FILES['photo']['name'];
                        $fileExt = end(explode(".", $filename));
                        $newFileName = md5(time()) . "." . $fileExt;

                        $filepath = FCPATH . 'assets/img/users/' . $user->id;

                        $file_info = parent::uploadImg($filepath, $newFileName);

                        // delete existing profile pic
                        if ($user_account->profile_pic <> "")
                            unlink(FCPATH . 'assets/img/users/' . $user->id . '/' . $user_account->profile_pic);

                        $user_account->update(array(), array('profile_pic' => $newFileName));
                    }

                    // check if update self
                    if ($this->session->get('admin', 'admin_id') == $this->id)
                    {
                        $new_user = new User_Model($this->id);

                        // get user account details
                        $new_user->user_account = new User_Account_Model($new_user->id_user);

                        // If all the conditions are true, initialize the session data
                        $newdata = array(
                            'admin_id' => $new_user->id_user,
                            'admin_email' => $new_user->user_account->email_address,
                            'admin_username' => $new_user->username,
                            'admin_name' => substr($new_user->user_account->firstname, 0, 1) . '.&nbsp;' . htmlentities(strtoupper($new_user->user_account->lastname), ENT_COMPAT, 'UTF-8'),
                            'admin_login' => TRUE,
                            'encryption_key' => $this->config->item('encryption_key'),
                            'administrator_profile_id' => $new_user->user_profile_id,
                            'is_new' => $new_user->is_new,
                            'profile_pic' => $new_user->user_account->profile_pic,
                            // Security token
                            'token' => substr(sha1(microtime()), 4, 30)
                        );

                        // Set the session data
                        $this->session->set_userdata('admin', $newdata);
                    }

                    parent::logThis($user->id_user, 'Successfully updated user account!');

                    // Set confirmation message
                    $this->session->set_flashdata('confirm', 'Successfully updated user account!');
                    $this->session->set_flashdata('id', $user->id);
                } else
                {
                    // Set confirmation message
                    $this->session->set_flashdata('note', 'Error in updating user account');
                }
            } else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating user account');
            }

            redirect(admin_url($this->classname));
        }
    }

    // ===================================================================
    //    AJAX FUNCTION
    // ===================================================================
}

/* End of file user.php */
/* Location: ./application/modules_core/adminpanel/controllers/user/user.php */