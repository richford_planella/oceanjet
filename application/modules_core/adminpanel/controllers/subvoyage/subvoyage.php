<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Class
|--------------------------------------------------------------------------
|
| Outlet Content Management
|
| @category Controller
| @author       Kenneth Bahia
*/
class Subvoyage extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('voyage/Voyage_Model'));       
        $this->load->model(admin_dir('vessel/Vessel_Model'));
        $this->load->model(admin_dir('port/Port_Model'));
        $this->load->model(admin_dir('accommodation/Accommodation_Model'));            

        $this->voyage = new Voyage_Model();
        $this->vessel = new Vessel_Model();
        $this->port = new Port_Model();
        $this->accommodation = new Accommodation_Model();
                        
        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Outlet Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        // Get all outlets
        $voyage = $this->voyage->displaylist();

        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'voyage'        => $voyage,
        );
        
        parent::displayTemplate(admin_dir('voyage/voyage/voyage'), $data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get user
        $voyage = new Voyage_Model($this->id);
                        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'voyage'        => $voyage,
            'default_fare'  => array(),
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'accommodation_list'     => $this->accommodation->displayList(),
            'fare_list'     => array()
        );
        
        parent::displayTemplate(admin_dir('voyage/voyage/form/voyage'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $voyage = new Voyage_Model($this->id);
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($voyage->toggleStatus())
        {   
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating voyage.');
            $this->session->set_flashdata('id', $voyage->id_voyage);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating voyage.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();
                            
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'accommodation_list'     => $this->accommodation->displayList(),
            'fare_list'     => array()
        );
            
        parent::displayTemplate(admin_dir('voyage/voyage/form/add/voyage'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get user
        $voyage = new Voyage_Model($this->id);
        
        // Check if a record exists
        $voyage->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();

        $pattern = "/[: ]/";
        $ETD_format = date("h:i A", strtotime($voyage->ETD));
        $ETA_format = date("h:i A", strtotime($voyage->ETA));

        $voyage->ETD_F = preg_split($pattern, $ETD_format);
        $voyage->ETA_F = preg_split($pattern, $ETA_format);
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.voyage.js')))),
            'voyage'        => $voyage,
            'default_fare'  => array(),
            'vessel_list'   => $this->vessel->displayList(),
            'port_list'     => $this->port->displayList(),
            'accommodation_list'     => $this->accommodation->displayList(),
            'fare_list'     => array()
        );

        parent::displayTemplate(admin_dir('voyage/voyage/form/edit/voyage'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('vessel_id', 'Vessel', 'required|trim');
        $this->form_validation->set_rules('origin', 'Origin', 'required');
        $this->form_validation->set_rules('destination', 'Destination', 'required');
        $this->form_validation->set_rules('ETD', 'ETD', 'required');
        $this->form_validation->set_rules('ETA', 'ETA', 'required');
        //$this->form_validation->set_rules('accommodation_id', 'Accommodation Type', 'required');
        //$this->form_validation->set_rules('fare_id', 'Fare', 'required');
        $this->form_validation->set_rules('enabled', 'Status', 'required');

        if ($this->uri->rsegment(2) == 'add') {
            $this->form_validation->set_rules('voyage', 'Voyage No', 'required|trim|is_unique[voyage.voyage]');
            self::_addInfo();
        }

        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Outlet
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $voyage = new Voyage_Model();
            
            parent::copyFromPost($voyage, 'id_voyage');

            $ETD = DateTime::createFromFormat("H:i A", $voyage->ETD)->format("H:i:s");
            $ETA = DateTime::createFromFormat("H:i A", $voyage->ETA)->format("H:i:s");

            $voyage->ETD = $ETD;
            $voyage->ETA = $ETA;
                        
            // Check for successful insert
            if ($voyage->add())
            {
                parent::logThis($voyage->id, 'Successfully added Voyage');

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added Voyage');
                $this->session->set_flashdata('id', $voyage->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving Voyage');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $voyage = new Voyage_Model($this->id);

        if (trim($this->tools->getPost("voyage")) == trim($voyage->voyage)) {
            $this->form_validation->set_rules('voyage', 'Voyage No', 'required|trim');
        } else {
            $this->form_validation->set_rules('voyage', 'Voyage No', 'required|trim|is_unique[voyage.voyage]');
        }

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($voyage, 'id_voyage');

            $ETD = DateTime::createFromFormat("H:i A", $voyage->ETD)->format("H:i:s");
            $ETA = DateTime::createFromFormat("H:i A", $voyage->ETA)->format("H:i:s");

            $voyage->ETD = $ETD;
            $voyage->ETA = $ETA;
            
            // Check for successful update
            if ($voyage->update())
            {                                   
                 parent::logThis($voyage->id, 'Successfully updated voyage');

                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Voyage');
                $this->session->set_flashdata('id', $voyage->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating voyage');
            }
            
            redirect(admin_url($this->classname));
        }
    }
   
}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */