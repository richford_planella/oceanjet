<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Passenger Checkin Class
|--------------------------------------------------------------------------
|
| Passenger Checkin Content Management
|
| @category Controller
| @author       Gian Asuncion
*/

class Passenger_Checkin extends Admin_Core
{
    public function __construct()
    {
        $this->classname = strtolower(get_class());
        parent::__construct();

        $this->load->model(admin_dir('passenger_checkin/Passenger_Checkin_Model'));
        $this->load->model(admin_dir('booking/booking_model'));
        $this->load->model(admin_dir('booking/booking_status_model'));
        $this->load->model(admin_dir('reservation/reservation_model'));
        $this->load->model(admin_dir('reservation/reservation_status_model'));
        $this->load->model(admin_dir('reservation/reservation_booking_model'));
        $this->load->model(admin_dir('outlet/outlet_model'));
        $this->load->model(admin_dir('accommodation/accommodation_model'));
        $this->load->model(admin_dir('subvoyage/subvoyage_model'));
        $this->load->model(admin_dir('ticket_series/ticket_series_ref_model'));
        $this->load->model(admin_dir('vessel/vessel_model'));
        $this->load->model(admin_dir('vessel/vessel_seats_model'));
        $this->load->model(admin_dir('voyage/voyage_model'));
        $this->load->model(admin_dir('voyage_management/voyage_management_model'));
        $this->load->model(admin_dir('voyage_management_status/voyage_management_status_model'));
        $this->load->model(admin_dir('port/Port_Model'));
        $this->load->model(admin_dir('passenger_baggage_checkin/passenger_baggage_checkin_model'));
        $this->load->model(admin_dir('subvoyage_management/subvoyage_management_model'));
        $this->load->model(admin_dir('accommodation/accommodation_model'));

        $this->accommodation = new Accommodation_Model();
        $this->subvoyage_management = new Subvoyage_Management_Model();
        $this->passenger_baggage_checkin = new Passenger_Baggage_Checkin_Model();
        $this->passenger_checkin = new Passenger_Checkin_Model();
        $this->booking = new Booking_Model();
        $this->booking_status = new Booking_Status_Model();
        $this->reservation = new Reservation_Model();
        $this->reservation_status = new Reservation_Status_Model();
        $this->reservation_booking = new Reservation_Booking_Model();
        $this->outlet = new Outlet_Model();
        $this->accommodation = new Accommodation_Model();
        $this->ticket_series_ref = new Ticket_Series_Ref_Model();
        $this->vessel = new Vessel_Model();
        $this->vessel_seats = new Vessel_Seats_Model();
        $this->subvoyage = new Subvoyage_Model();
        $this->voyage = new Voyage_Model();
        $this->voyage_management = new Voyage_Management_Model();
        $this->voyage_management_status = new Voyage_Management_Status_Model();
        $this->port = new Port_Model();

        $this->id = $this->uri->rsegment(3);
    }

    public function index()
    {
        redirect(admin_url($this->classname, 'add'));
    }

    public function add()
    {
        //Set default variables
        $voyage_management_id = $this->uri->rsegment(3);

        //Set default voyage details once voyage management Id is being set
        //Else set to empty details
        $voyage_details = '';
        if ($voyage_management_id!=null)
            $voyage_details = $this->subvoyage_management->displaylist(array('u.id_subvoyage_management' => $voyage_management_id))[0];

        //Initialize set post
        $this->setPOSTdata();

        //Set origin & destination IDs
        if(isset($voyage_details->origin_id) AND  isset($voyage_details->destination_id)) {
            $this->tools->setPost('origin_id', $voyage_details->origin_id);
            $this->tools->setPost('destination_id', $voyage_details->destination_id);
            $this->tools->setPost('departure_date', $voyage_details->departure_date);
            $this->tools->setPost('voyage_management_id', $voyage_details->id_subvoyage_management);
            $this->tools->setPost('voyage_text', $voyage_details->voyage);
            $this->tools->setPost('subvoyage_status_text', $voyage_details->description);
            $this->tools->setPost('subvoyage_status_id', $voyage_details->subvoyage_management_status_id);
            $this->tools->setPost('origin', $voyage_details->port_origin);
            $this->tools->setPost('destination', $voyage_details->port_destination);
            $this->tools->setPost('vessel', $voyage_details->vessel_code);
            $this->tools->setPost('vessel_id', $voyage_details->vessel_id);
            $this->tools->setPost('eta', $voyage_details->ETA);
            $this->tools->setPost('etd', $voyage_details->ETD);
            $this->tools->setPost('is_swap', 1);
            $this->form_validation->set_rules('origin_id', 'origin', 'required|trim');
            $this->form_validation->set_rules('destination_id', 'destination', 'required|trim');
            $this->form_validation->set_rules('voyage_management_id', 'voyage', 'required|trim');
            $this->form_validation->run();
        }

        //Get sub voyage lists
        //Get Attended baggage
        $subvoyage = array();
        if(($this->tools->getPost('origin_id') AND $this->tools->getPost('destination_id'))) {
            $subvoyage = $this->passenger_checkin->getSubVoyages($this->tools->getPost("origin_id"),
                $this->tools->getPost("destination_id"));
        }

        $data = array(
            'header' => Modules::run(admin_dir('header/call_header'),array('title'=>'Passenger Check-In')),
            'footer' => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.passenger.checkin.js'), js_dir('', 'keynavigator-min.js')))),
            'ports'         => $this->port->displayList(array('u.enabled'=>1),array('u.id_port'=>'ASC')),
            'accommodation' => $this->accommodation->displayList(array('enabled' => 1)),
            'voyage_management_id' => $voyage_management_id,
            'voyage_management_details' => $voyage_details,
            'subvoyage' => $subvoyage,
        );

        parent::displayTemplate(admin_dir('passenger_checkin/passenger_checkin/passenger_checkin'), $data);
    }

    public function setPOSTData()
    {
        $this->tools->setPost('origin_id','');
        $this->tools->setPost('destination_id', '');
        $this->tools->setPost('departure_date', '');
        $this->tools->setPost('voyage_management_id', '');
        $this->tools->setPost('voyage_text', '');
        $this->tools->setPost('subvoyage_status_text', '');
        $this->tools->setPost('subvoyage_status_id', '');
        $this->tools->setPost('origin', '');
        $this->tools->setPost('destination', '');
        $this->tools->setPost('vessel', '');
        $this->tools->setPost('vessel_id', '');
        $this->tools->setPost('eta', '');
        $this->tools->setPost('etd', '');
        $this->tools->setPost('is_swap', '');
    }

    public function swapVessel()
    {
        //Get Subvoyage Vessel
        $subvoyage_management_id = $this->uri->rsegment(3);
        $sv_details = $this->subvoyage_management->displayList(array("u.id_subvoyage_management"=> $subvoyage_management_id))[0];
        $vessels = $this->vessel->displayList(array('v.enabled'=>1, 'v.id_vessel <>'=>$sv_details->vessel_id));

        $data = array(
            'header'      => Modules::run(admin_dir('header/call_header'),array('title'=>'Swap Vessel')),
            'footer'      => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery','jquery.swap.vessel.js')))),
            'sv_details'  => $sv_details,
            'vessels'     => $vessels,
            'accommodation' => $this->accommodation->displayList(array('enabled' => 1)),
        );

        parent::displayTemplate(admin_dir('passenger_checkin/passenger_checkin/swap_vessel'), $data);
    }

    //Set new vessel
    public function setNewVessel()
    {
        $subvoyage_management_id = $this->tools->getPost("voyage_management_id");
        $new_vessel              = $this->tools->getPost("new_vessel");
        $current_vessel_id       = $this->tools->getPost("current_vessel_id");

        //Update passenger seat
        $this->subvoyage_management->update(array('id_subvoyage_management'=>$subvoyage_management_id),
                                             array("vessel_id" => $new_vessel, 'old_vessel_id'=>$current_vessel_id, 'is_swap'=>1));

        $this->session->set_flashdata('confirm', 'Successfully swapped vessels');
        redirect(admin_url($this->classname, 'add', $subvoyage_management_id));
    }

    public function ajax()
    {
        $return = array();
        $module = $this->tools->getPost("module");

        switch ($module) {
            case 'voyage' :
                //Select voyage based on departure date
                $voyage = $this->passenger_checkin->getVoyages($this->tools->getPost("departure_date"));

                foreach ($voyage as $rkey => $rvalue)
                    $return["option"][$rkey] = "<option value='" . $rvalue->id_voyage_management . "'>" . $rvalue->voyage . "</option>";

                //Select Option of Voyages
                array_unshift($return["option"], "<option value=''>Please Select</option>");

                break;

            case 'subvoyage':
                $return["option"] = array();

                $subvoyage = $this->passenger_checkin->getSubVoyages($this->tools->getPost("departure_date"),
                    $this->tools->getPost("id_voyage_management"));

                foreach ($subvoyage as $key => $value)
                    $return["option"][$key] = "<option value='" . $value->id_subvoyage . "'>[" . $value->origin . "-" . $value->destination . "][" . $value->ETD . "-" . $value->ETA . "]</option>";

                array_unshift($return["option"], "<option value=''>Please Select</option>");

                //voyages lists based on departure date
                //use to set default values on voyage details
                $voyage = $this->passenger_checkin->getVoyages($this->tools->getPost("departure_date"));
                $return['voyages_lists'] = $voyage;
                break;

            //Voyage details
            case 'details':
                $return["subvoyage"] = '';
                $subvoyage = $this->subvoyage->displayList(
                    array("id_subvoyage" => $this->tools->getPost("id_subvoyage"))
                );

                foreach ($subvoyage as $rkey => $rvalue)
                    $return["subvoyage"]["details"] = $rvalue;

                break;

            case 'table':

                //Change status Incoming Passengers to No show PAX
                $subvoyage_status_id = $this->tools->getPost("subvoyage_status_id");
                if($subvoyage_status_id==1 || $subvoyage_status_id==2 || $subvoyage_status_id==3)
                    $subvoyage_status_id = "NO SHOW PAX";
                else
                    $subvoyage_status_id = "Incoming Passengers";

                $subvoyage_management_id = $this->tools->getPost("subvoyage_management_id");
                $vessel_id               = $this->tools->getPost("vessel_id");
                $vessel_stats            = ($this->passenger_checkin->vesselStatusTable($subvoyage_management_id,$vessel_id));

                //Check if we have a swap vessel
                $swap = $this->subvoyage_management->displayList(array('u.id_subvoyage_management'=>$subvoyage_management_id))[0];

                $new_vessel_status = array();
                foreach ($vessel_stats as $vs):
                    $new_vessel_status['Total Capacity'][$vs['aCode']][]         = $vs['total_capacity'];

                    //Swap Fields
                    if($swap->is_swap==1){
                        $new_vessel_status['Occupied'][$vs['aCode']][]                        = $vs['occupied_seats'];
                        $new_vessel_status['Vacant'][$vs['aCode']][]                          = $vs['vacant_seats'];
                        $new_vessel_status['Tickets Not Yet Transferred'][$vs['aCode']][]     = $vs['ticket_not_transfered'];
                    }else{
                        $new_vessel_status['Ticketed PAX'][$vs['aCode']][]           = $vs['ticketed_pax'];
                        $new_vessel_status['Checked-in PAX'][$vs['aCode']][]         = $vs['checkin_pax'];
                        $new_vessel_status[$subvoyage_status_id][$vs['aCode']][]     = $vs['no_show_pax'];
                    }

                    $new_vessel_status['Available Seats'][$vs['aCode']][]        = $vs['available_seats'];
                endforeach;

                $return["details"] = "<table class='table  table-striped table-form'>
                                                    <thead><tr><th>Items</th>";
                foreach ($vessel_stats as $vs):
                    $return["details"] .= "<th>" . $vs['aCode'] . "</th>";
                endforeach;
                    $return["details"] .= "</thead></tr><tbody>";

                foreach ($new_vessel_status as $key => $value):
                    $return["details"] .= "<tr>
                                                <th scope='row'>".$key."</th>";
                foreach ($value as $k => $v):
                        $return["details"] .= "<td>".$v[0]."</td>";
                    endforeach;
                    $return["details"] .= "</tr>";
                  endforeach;
                    $return["details"] .=  "</tbody></table>";
                    break;

            case 'seatplan':
                $vessel_id = $this->tools->getPost("vessel_id");
                $voyage_management_id = $this->tools->getPost("voyage_management_id");
                $return["seat_plan"] = $this->drawVesselSeat($vessel_id,$voyage_management_id);
            break;

            //Get passenger details based on passenger_id
            case 'passenger_details':

                  $return["details"] = '';
                  $passenger_details = $this->passenger_checkin->displayList(array("u.booking_id" => $this->tools->getPost("booking_id")));

                    foreach ($passenger_details as $rkey => $rvalue)
                        $return["details"] = $rvalue;

                break;

            case 'depart_status':
                $depart_status = $this->tools->getPost("depart_status");

                $id = $this->tools->getPost("voyage");
                $voyage = new Voyage_Model($id);
                $voyage_management = $this->voyage_management->displayList(
                    array("voyage_id" => $voyage->id_voyage)
                );

                $this->voyage_management->update(
                    array('voyage_id' => $voyage_management[0]->id_voyage_management),
                    array(
                        'status' => $depart_status
                    )
                );

                if ($depart_status == 3) {
                    $delayed_time = $this->tools->getPost("delayed_time");
                    $this->voyage_management->update(
                        array('voyage_id' => $voyage_management[0]->id_voyage_management),
                        array(
                            'actual_departure_time' => $delayed_time
                        )
                    );
                }

                break;

            case 'vacant_available_seats':
                //Get all Passengers that had been check-in
               $current_vessels = $this->passenger_checkin->displayList(array('u.subvoyage_management_id' => $this->tools->getPost("voyage_management_id"), 'u.vessel_id'=>$this->tools->getPost("current_vessel")));
                $return["current_seats"] = array();
                foreach($current_vessels as $cv)
                    array_push($return["current_seats"],array('vessel_seats' => $cv->vessel_seats,
                                                      'name'         => $cv->lastname.', '.$cv->firstname.' '.substr($cv->middlename,0,1).'.',
                                                      'ticket_no'    => $cv->ticket_no,
                                                      'valid_id'     => $cv->passenger_valid_id_number));


               //Get all available seats from new vessel
               $new_vessel_available_seats = $this->passenger_checkin->getAvailableSeatsNewVessel($this->tools->getPost("voyage_management_id"),$this->tools->getPost("new_vessel_id"));

                $return["new_seats"] = array();
                foreach($new_vessel_available_seats as $cv)
                    array_push($return["new_seats"],$cv->vessel_seats);



            break;

            case 'save_checkin':
                //Save Data to passenger check-in
                $passenger_checkin = new Passenger_Checkin_Model();
                $this->tools->setPost('subvoyage_management_id', $this->tools->getPost('voyage_management_id'));
                parent::copyFromPost($passenger_checkin, 'id_passenger_checkin');

                $passenger_checkin->add();

                //Update booking status to from 1 (Issued) to 5 (Check-in)
                $this->booking->update(array('id_booking'=>$this->tools->getPost('booking_id')),
                                       array("booking_status_id" => 5));

                //BAGGAGE CHECK-IN Start
                //Insert booking Id is found update on baggage check-in INSERT
                $passenger_baggage_checkin = new Passenger_Baggage_Checkin_Model();

                //Allow only 1 booking Id on check-in
                $bd = $passenger_baggage_checkin->displayList(array('u.transaction_ref'=>$this->tools->getPost('transaction_ref'),
                                                                    'u.booking_id'       =>$this->tools->getPost('booking_id')));
                if(count($bd)==0) {
                    $passenger_baggage_checkin->add(array(
                        'subvoyage_management_id' => $this->tools->getPost('voyage_management_id'),
                        'booking_id'              => $this->tools->getPost('booking_id'),
                        'transaction_ref'         => $this->tools->getPost('transaction_ref'),
                        'date_added'              => date("Y-m-d H:i:s")
                    ));
                }

                //Then Update baggage check IN fields
                //By getting the Transaction reference ID :)
                $bd = $passenger_baggage_checkin->displayList(array('u.transaction_ref'=>$this->tools->getPost('transaction_ref')))[0];
                $existing_data_from_baggage = array('attended_baggage_id'=>$bd->attended_baggage_id,
                                                    'quantity'           =>$bd->quantity,
                                                    'weight'             =>$bd->weight,
                                                    'free_baggage'       =>$bd->free_baggage,
                                                    'amount_tendered'    =>$bd->amount_tendered,
                                                    'total_amount'       =>$bd->total_amount,
                                                    'paid_status'        =>$bd->paid_status,
                                                    'date_update'        =>date("Y-m-d H:i:s")
                );

                $passenger_baggage_checkin->update(array('transaction_ref'=>$this->tools->getPost('transaction_ref')),$existing_data_from_baggage);
                //BAGGAGE CHECK-IN END

                $return['msg'] = 'ok';
            break;

            case 'swap_seats':
                $subvoyage_management_id = $this->tools->getPost("voyage_management_id");
                $current_vessel_id    = $this->tools->getPost("current_vessel_id");
                $new_vessel           = $this->tools->getPost("new_vessel");
                $current_seats        = explode(',',$this->tools->getPost("current_seats"));
                $new_seats            = explode(',',$this->tools->getPost("new_seats"));

                //swap vessels
                $x=0;
                foreach($current_seats as $cs)
                {
                    //Get current passenger
                    $passenger = $this->passenger_checkin->displayList(array('u.subvoyage_management_id'=>$subvoyage_management_id,
                                                                             'u.vessel_seats'=>$cs,
                                                                             'u.vessel_id'=>$current_vessel_id))[0];
                    //Update passenger seat
                    $this->passenger_checkin->update(array('id_passenger_checkin'=>$passenger->id_passenger_checkin),
                                                     array("vessel_id" => $new_vessel, 'vessel_seats'=>$new_seats[$x]));
                    $x++;
                }

                $return['msg'] = 'ok';
            break;
        }

        $this->encode($return);
    }

    public function drawVesselSeat($vessel_id,$voyage_management_id)
    {
        // get vessel
        $vessel = new Vessel_Model($vessel_id);

        // array seat plan
        $seat_plan = array();

        // get seat plan
        for($i = 1; $i <= $vessel->total_row; $i++) {
            for ($x = 1; $x <= $vessel->total_column; $x++) {
                $seat_plan[$i][$x]['value'] = $this->vessel_seats->getValue('vessel_seats', array(
                    'vessel_id' => $vessel->id,
                    'column' => $x,
                    'row' => $i,
                ));
                // get accommodation id
                $accommodation_id = $this->vessel_seats->getValue('accommodation_id', array(
                    'vessel_id' => $vessel->id,
                    'column' => $x,
                    'row' => $i,
                ));
                //get color code
                if ($accommodation_id > 0)
                    $seat_plan[$i][$x]['color'] = $this->accommodation->getValue('palette', array(
                                'id_accommodation' => $accommodation_id,
                    ));
                else
                    $seat_plan[$i][$x]['color'] = "#000";

                //Set accommodation id
                $seat_plan[$i][$x]['accommodation'] = $accommodation_id;

                //Occupied || Vacant
                $passenger_checkin_id = $this->db->select('booking_id')->from('passenger_checkin')
                                                 ->where(array('subvoyage_management_id'=>$voyage_management_id,
                                                               'vessel_seats'=> $seat_plan[$i][$x]['value'],
                                                               'vessel_id'=>$vessel_id))->get();
                $booking_id = 0;
                if ($passenger_checkin_id->num_rows() > 0) {
                    $row = $passenger_checkin_id->row();
                    $booking_id = $row->booking_id;
                }
                $seat_plan[$i][$x]['booking_id'] = $booking_id;
            }
        }

        return $seat_plan;
    }


    protected function encode($data)
    {
        echo json_encode($data);
    }

    /*
 * Print boarding pass
 * @return pdf file
 * */
    public function printBoardingPass()
    {
        $ticket_no = $this->uri->rsegment(3);
        $subvoyage_management_id = $this->uri->rsegment(4);
        $data = array('passenger_details'=> $this->passenger_baggage_checkin->displayList(array('tsi.ticket_no' => $ticket_no)),
                      'voyage_details'   => $this->subvoyage_management->displayList(array("u.id_subvoyage_management"=> $subvoyage_management_id)));

        parent::displayTemplate(admin_dir('passenger_checkin/pdf/print_baggage_claim'),$data);
        $this->load->library('pdf');

        $mpdf  = $this->pdf->load();
        $mpdf->showImageErrors = true;
        $mpdf->AddPage('L');
        $mpdf->WriteHTML($this->output->get_output());

        $mpdf->Output("print_boarding_pass_".date("d-m-Y").".pdf", 'I');
        exit;
    }

    /*
     * Print manifest
     * @return pdf file
     * */
    public function printPassengerManifest()
    {
        $subvoyage_management_id = $this->uri->rsegment(3);
        $vessel_id = $this->uri->rsegment(4);
        $data = array('passenger_details'=> $this->passenger_checkin->displayList(array('u.subvoyage_management_id' => $subvoyage_management_id,'u.vessel_id'=>$vessel_id)),
                      'voyage_details'   => $this->subvoyage_management->displayList(array("u.id_subvoyage_management"=> $subvoyage_management_id)));

        parent::displayTemplate(admin_dir('passenger_checkin/pdf/passenger_manifest'),$data);
        $this->load->library('pdf');

        $mpdf  = $this->pdf->load();
        $mpdf->showImageErrors = true;
        $mpdf->AddPage('P');
        $mpdf->WriteHTML($this->output->get_output());

        $mpdf->Output("passenger_manifest_".date("d-m-Y").".pdf", 'I');
        exit;
    }


    /*
     * Print manifest
     * @return pdf file
     * */
    public function printDisembarkedPassenger()
    {
        $subvoyage_management_id = $this->uri->rsegment(3);
        $swap = $this->subvoyage_management->displayList(array('u.id_subvoyage_management'=>$subvoyage_management_id))[0];

        $data = array('passenger_details'=> $this->passenger_checkin->displayList(array('u.subvoyage_management_id' => $subvoyage_management_id,'u.vessel_id'=>$swap->old_vessel_id)),
                      'voyage_details'   => $this->subvoyage_management->displayList(array("u.id_subvoyage_management" => $subvoyage_management_id)));

        parent::displayTemplate(admin_dir('passenger_checkin/pdf/disembarked_passenger'),$data);
        $this->load->library('pdf');

        $mpdf  = $this->pdf->load();
        $mpdf->showImageErrors = true;
        $mpdf->AddPage('P');
        $mpdf->WriteHTML($this->output->get_output());

        $mpdf->Output("passenger_manifest_".date("d-m-Y").".pdf", 'I');
        exit;
    }

}
/* End of file passenger_checkin.php */
/* Location: ./application/modules_core/adminpanel/controllers/passenger_checkin/passenger_checkin.php */