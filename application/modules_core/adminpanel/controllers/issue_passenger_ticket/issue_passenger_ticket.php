<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Issue Passenger Ticket Class
|--------------------------------------------------------------------------
|
| Ticket Management
|
| @category	Controller
| @author	Richford Planella
| @updates  Ronald Meran March 23, 2016
*/
class Issue_passenger_ticket extends Admin_Core
{
    // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		$this->load->model(admin_dir('booking/Booking_Model'));
		$this->load->model(admin_dir('passenger/Passenger_Model'));
		$this->load->model(admin_dir('voyage/Voyage_Model'));
		$this->load->model(admin_dir('ticket_series/ticket_series_ref_model'));
		$this->load->model(admin_dir('port/port_model'));
		$this->load->model(admin_dir('subvoyage_management/subvoyage_management_model'));
		$this->load->model(admin_dir('subvoyage/subvoyage_model'));
		$this->load->model(admin_dir('passenger_fare/subvoyage_fare_model'));
		$this->load->model(admin_dir('voyage_management/voyage_management_model'));
		$this->load->model(admin_dir('accommodation/accommodation_model'));
		$this->load->model(admin_dir('passenger_fare/passenger_fare_model'));
		$this->load->model(admin_dir('discount/discount_model'));
		$this->load->model(admin_dir('evoucher/evoucher_series_model'));
		
		$this->port = new Port_Model();
		$this->voyage = new Voyage_Model();
		$this->booking = new Booking_Model();
		$this->discount = new Discount_Model();
		$this->passenger = new Passenger_Model();
		$this->subvoyage = new Subvoyage_Model();
		$this->accommodation = new Accommodation_Model();
		$this->subvoyage_fare = new Subvoyage_Fare_Model();
		$this->passenger_fare = new Passenger_Fare_Model();
		$this->evoucher_series = new Evoucher_Series_Model();
		$this->ticket_series = new Ticket_Series_Ref_Model();
		$this->voyage_management = new Voyage_Management_Model();
		$this->subvoyage_management = new Subvoyage_Management_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Issue Passenger Ticket Master List
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		// Session id of user
		$session_id = $this->session->get('admin','administrator_profile_id');

		// Form validation on initial details
		self::_validateInitialStep();

       	// Form validation on issuing ticket
		$validate = self::_issueInfo();

        // Initialize data
        $data = array(
            'header'    	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Passenger Ticket Issuance')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
			'ticket_series'	=> $this->ticket_series->displayList(array('user_id' => $session_id, 'issued' => '0'),'','',1),
			'accommodation'	=> $this->accommodation->displayList(array('enabled'=>1)),
			'port'			=> $this->port->displayList(),
			'voyage'		=> self::getVoyages('main'),
			'return_voyage'	=> self::getVoyages('return'),
			'ticket_issuance_details' => $validate
        );
        
        parent::displayTemplate(admin_dir('issue_passenger_ticket/form/issue/issue_passenger_ticket'),$data);
	}

        
	// --------------------------------------------------------------------
	
	/*
	 * Add Ticket Last Step
	 *
	 * @access	public
	 * @return	void
	 */
	// public function add($session_data = array())
	// {
	// 	// Session id of user
	// 	$session_id = $this->session->get('admin','administrator_profile_id');
		
	// 	// Query Options
	// 	$port_options = $this->port->displayList();
	// 	$discount_code_options = $this->discount->displayList();
	// 	$passenger_fare_code_options = $this->passenger_fare->displayList();
	// 	$accommodation_options = $this->accommodation->displayList(array('enabled'=>1));
	// 	$ticket_series = $this->ticket_series->displayList(array('user_id' => $session_id, 'issued' => '0'),'','',1);
		
	// 	// Form validation
	// 	self::_validate();
							
	// 	// Initialize data
	// 	$data = array(
 //            'header'    					=> Modules::run(admin_dir('header/call_header'),array('title' => 'Passenger Ticket Issuance')),
 //            'footer'        				=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.ticket_management.js')))),
	// 		'ticket_series'					=> $ticket_series,
	// 		'port_options'					=> $port_options,
	// 		'accommodation_options'			=> $accommodation_options,
	// 		'passenger_fare_code_options'	=> $passenger_fare_code_options,
	// 		'discount_code_options'			=> $discount_code_options,
	// 		'session_data'					=> $session_data
 	//       );
			
	// 	// parent::displayTemplate(admin_dir('issue_passenger_ticket/form/add/issue_passenger_ticket'),$data);
	// }
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form for initial information
	 *
	 * @access	private
	 * @return	void
	 */
	private function _validateInitialStep()
	{
		// Validate ticket initial step
		$this->form_validation->set_rules('voyage', 'voyage', 'required|trim');
		$this->form_validation->set_rules('origin', 'origin', 'required|trim');
		$this->form_validation->set_rules('destination', 'destination', 'required|trim');
		$this->form_validation->set_rules('accommodation', 'accommodation', 'required|trim');
		$this->form_validation->set_rules('departure_date', 'departure date', 'required|trim');
		$this->form_validation->set_rules('round_trip', 'roundtrip', 'trim');

		if($this->tools->getPost('round_trip')){
			$this->form_validation->set_rules('return_date', 'return date', 'required|trim');
			$this->form_validation->set_rules('return_voyage', 'return voyage', 'required|trim');
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return	void
	 */
	private function _validate()
	{
		// validate ticket last step
		$this->form_validation->set_rules('firstname', 'passenger first name', 'required|trim');
		$this->form_validation->set_rules('lastname', 'passenger last name', 'required|trim');
		$this->form_validation->set_rules('age', 'passenger age', 'required|trim');
		$this->form_validation->set_rules('gender', 'passenger gender', 'required|trim');
		$this->form_validation->set_rules('contact_number', 'passenger contact number', 'required|trim');
		$this->form_validation->set_rules('id_number', 'passenger ID number', 'required|trim');
		$this->form_validation->set_rules('nationality', 'passenger nationality', 'required|trim');

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Ticket Issuance validation
	 *
	 * @access	private
	 * @return	void
	 */
	private function _issueInfo()
	{	
		// Initialize get subvoyage management
		$origin = $this->tools->getPost("origin") ? $this->tools->getPost("origin") : 0;
		$destination = $this->tools->getPost("destination") ? $this->tools->getPost("destination") : 0;
		$subvoyage_management = $this->tools->getPost() ? self::getSubvoyageManagement($this->tools->getPost("voyage"), $origin, $destination) : array();
		$return_subvoyage_management = $this->tools->getPost() ? self::getSubvoyageManagement($this->tools->getPost("return_voyage"), $destination, $origin) : array();

		// Set the data
		$data = array('subvoyage_management' => $subvoyage_management,
					  'return_subvoyage_management' => $return_subvoyage_management,
					  'passenger_fare' => $this->passenger_fare->displayList(array('u.enabled' => 1, 'accommodation_id' => $this->tools->getPost('accommodation'))),
					  'discount' => $this->discount->displayList(array('u.enabled' => 1)));
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
			return parent::getTemplate(admin_dir('issue_passenger_ticket/form/issue/ticket_issuance_details'), $data);

		return FALSE; 
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then add Port
	 *
	 * @access	private
	 * @return	void
	 */
	private function _addInfo()
	{
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$booking = new Booking_Model();
			$passenger = new Passenger_Model();
			
			parent::copyFromPost($booking, 'id_booking');
			parent::copyFromPost($passenger, 'id_passenger');
			
			// Get data for passenger table insertion
			$firstname = $this->tools->getPost('firstname');
			$lastname = $this->tools->getPost('lastname');
			$middle_initial = $this->tools->getPost('middle_initial');
			$gender = $this->tools->getPost('gender');
			$birthdate = $this->tools->getPost('birthdate');
			$age = $this->tools->getPost('age');
			$contact_number = $this->tools->getPost('contact_number');
			$id_number = $this->tools->getPost('id_number');
			$nationality = $this->tools->getPost('nationality');
			$with_infant = $this->tools->getPost('with_infant');
			$infant_name = $this->tools->getPost('infant_name');
			
			// Get data for booking table insertion
			$user_id = $this->tools->getPost('user_id');
			$ticket_series_no = $this->tools->getPost('ticket_number');
			$voyage_id = $this->tools->getPost('voyage');
			$accommodation = $this->tools->getPost('accommodation');
			$transaction_ref = $this->misc->generate_code(8);
                        
			// Check for successful insert
			// if ($passenger->add())
			if ($passenger->add())
			{
				$data_passenger = array(
					"firstname" => $firstname,
					"lastname" => $lastname,
					"middlename" => $middle_initial,
					"gender" => $gender,
					"birthdate" => $birthdate,
					"age" => $age,
					"contactno" => $contact_number,
					"id_no" => $id_number,
					"nationality" => $nationality,
					"with_infant" => 1,
					"infant_name" => $infant_name
				);
				
				$passenger->add($data_passenger);
				
				/*
				*user_id
				*ticket_series_info_id
				*passenger_id
				passenger_fare_id
				booking_source_id
				booking_status_id
				discount_id
				subvoyage_management_id
				*transaction_ref
				points
				jetter_no
				payment
				fare_price
				return_fare_price
				total_discount
				terminal_fee
				port_charge
				total_amount
				reference_id
				passenger_valid_id_number
				date_added
				date_update
				*/
				$data_booking = array(
					"user_id" => intval(1),
					"ticket_series_info_id" => intval(1),
					"passenger_id" => intval(1),
					"passenger_fare_id" => intval(1),
					"booking_source_id" => intval(1),
					"booking_status_id" => intval(1),
					"discount_id" => intval(1),
					"subvoyage_management_id" => intval(1),
					"transaction_ref" => $transaction_ref,
					"points" => floatval(1.1),
					"jetter_no" => 'jetter',
					"payment" => 'payment',
					"fare_price" => floatval(1.1),
					"return_fare_price" => floatval(1.1),
					"total_discount" => floatval(1.1),
					"terminal_fee" => floatval(1.1),
					"port_charge" => floatval(1.1),
					"total_amount" => floatval(1.1),
					"reference_id" => intval(1),
					"passenger_valid_id_number" => intval(1)
				);
				$booking->add($data_booking);
				
				parent::logThis($booking->id, 'Successfully added Ticket');
				parent::logThis($passenger->id, 'Successfully added Ticket');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added Ticket');
				$this->session->set_flashdata('id', $booking->id);
				$this->session->set_flashdata('id', $passenger->id);
				// $this->session->set_flashdata('firstname', $passenger->firstname);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving Port');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}

	// ===================================================================
	//    AJAX FUNCTION
	// ===================================================================
	
	protected function ajax()
	{
		// Initialize
		$obj = $this->_cast_to_obj($this->tools->getPost());
		
		// Generate return
		switch($obj->module){
		
			// Return the options (Subvoyage Legs)
			case 'getvoyage':
				self::printVoyages($obj);
				break;	

			// Get the passenger fare
			case 'passenger_fare':
				self::getPassengerFare($obj);
				break;	

			// Get the passenger fare
			case 'subvoyage_fare':
				self::getSubvoyageFare($obj);
				break;

			// Get count subvoyage management
			case 'count_subvoyages':
				self::getSubvoyageCount($obj);
				break;		
		}
				
	}

	// --------------------------------------------------------------------
	
	/*
	 * Get voyages on submit
	 *
	 * @access	public
	 * @return	array
	 */
	public function getVoyages($mode)
	{
		// Initialize
		$voyages = array(); 

		// Validate if origin and destination are not empty
		$validate = !empty($this->tools->getPost("origin")) && 
					!empty($this->tools->getPost("destination")) ? TRUE : FALSE;

		if($validate) 
			$data = $this->tools->getPost();
		else
			$data = array("origin" => '0', "destination" => '0');

		// Cast to object
		$post = self::_cast_to_obj($data);

		// Voyage
		$voyage = $validate ? self::getSubvoyages($post, $mode) : array();

		// Departure date
		$departure_date = !empty($post->departure_date) ? $post->departure_date : NULL;
		
		// Return date
		$return_date = !empty($post->return_date) ? $post->return_date : NULL;

		// Validate
		$post_date = ($mode == 'main') ? $departure_date : $return_date;

		// Posted data
		if($post)
			foreach($voyage as $key => $value)
				if(isset($post->departure_date))
					if($value->dept_date == $post_date)
						$voyages[$value->id_subvoyage_management] = $value;

		// Return 
		return $voyages;
	}


	// --------------------------------------------------------------------
	
	/*
	 * Get voyage from selected origin/destination
	 *
	 * @access	public
	 * @return	void
	 */
	public function getSubvoyages($obj, $mode)
	{
		// Get the voyages
		$subvoyage = $this->booking->get_voyages(array('origin_id'=>$obj->origin, 'destination_id'=>$obj->destination));
		
		if($mode == 'return')
			$subvoyage = $this->booking->get_voyages(array('origin_id'=>$obj->destination, 'destination_id'=>$obj->origin));
		
		// Return data
		return $subvoyage;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Get the count of subvoyages
	 *
	 * @access	public
	 * @return	int
	 */
	public function getSubvoyageCount($obj)
	{
		// Get subvoyages
		$subvoyages = self::getSubvoyageManagement($obj->subvoyage_management_id, $obj->origin, $obj->destination);

		// Return count
		$this->encode(array("subvoyage_count" =>count($subvoyages)));
	}

	// --------------------------------------------------------------------

	/*
	 * Get subvoyage management
	 *
	 * @access	public
	 * @return	array
	 */
	public function getSubvoyageManagement($id, $origin, $destination)
	{
		// Initialize
		$sv = array();
		$okey = $dkey = 0;

		// Session id of user
		$session_id = $this->session->get('admin','admin_id');

		// Get the voyages
		$sv_management = new Subvoyage_Management_Model($id);

		// Get the voyage management id
		$voyage_mgt_id = isset($sv_management->voyage_management_id) ? $sv_management->voyage_management_id : 0;

		// Subvoyage
		$sv = $this->subvoyage_management->displayList(array("voyage_management_id" => $voyage_mgt_id));

		// Search for the selected subvoyages
		foreach($sv as $skey => $svalue){
			
			if($svalue->origin_id == $origin)
				$okey = $svalue->subvoyage_id;

			if($svalue->destination_id == $destination)
				$dkey = $svalue->subvoyage_id;
		}

		// Get the subvoyage management
		$sv = $this->booking->get_voyage_from_id($okey, $dkey, $voyage_mgt_id);

		// Get all ticket number
		$ticket_series = $this->ticket_series->displayList(array('user_id' => $session_id, 'issued' => '0'),'','',count($sv));

		// Assign ticket number
		foreach($sv as $skey => $svalue){
			
			if(!empty($ticket_series)){
				// Assign
				foreach($ticket_series as $tkey => $tvalue){
					
					// Initialize new property
					$sv[$skey]->ticket_number = new stdClass();
				
					// Initialize
					$sv[$skey]->ticket_number = $ticket_series[$skey]->ticket_no;
				}
			}
		}

		// Return data
		return $sv;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Get voyage for the ajax dropdown
	 *
	 * @access	public
	 * @return	array
	 */
	public function get_voyages($obj)
	{
		// Initialize
		$return["voyage"] = array();

		$subvoyage = self::getSubvoyages($obj);

		// Subvoyage origin
		foreach($subvoyage as $key => $value)
			if($value->dept_date == $obj->departure_date)
				$return["voyage"][$value->id_subvoyage_management] = "<option value='".$value->id_subvoyage_management."'>".$value->voyage_code." | ".$value->vessel." | ".$value->ETD." - ".$value->ETA."</option>";
		
		// Return
		return $return;
	}

	// --------------------------------------------------------------------

	/*
	 * Print selected voyage from origin/destination
	 *
	 * @access	public
	 * @return	json
	 */
	public function printVoyages($obj)
	{
		// Get the voyages
		$return = self::get_voyages($obj);
		
		// Add Please Select
		array_unshift($return["voyage"], "<option value=''>Please Select</option>");

		// Print
		print_r($return["voyage"]);

	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Validate selected voyage if multi-leg or return trip
	 *
	 * @access	public
	 * @return		void
	 */
	public function validate_voyage()
	{
		$voyage_id = $this->input->post('voyage_id');
		
		$query = $this->booking->get_voyage_from_id($voyage_id);
		
		print json_encode($query);
	}

	// --------------------------------------------------------------------

	/*
	 * Get passenger fare
	 *
	 * @access	public
	 * @return	json
	 */
	public function getPassengerFare($obj)
	{
		// Initialize
		$fare = new stdClass();

		// Get the passenger fare
		$passenger_fare = $this->passenger_fare->displayList(array('id_passenger_fare'=>$obj->passenger_fare_id));

		// Validate passenger fare
		$passenger_fare = isset($passenger_fare[0]) ? $passenger_fare[0] : array();

		// Get the passenger fare per subvoyage
		$subvoyage_fare = $this->subvoyage_fare->displayList(array('passenger_fare_id'=>$obj->passenger_fare_id));

		// Get the subvoyage_fare
		$fare = $passenger_fare;
		$fare->subvoyage_fare = $subvoyage_fare;

		// Return data
		$this->encode($fare);
	}

	// --------------------------------------------------------------------

	/*
	 * Get subvoyage fare
	 *
	 * @access	public
	 * @return	json
	 */
	public function getSubvoyageFare($obj)
	{
		// Validate passenger fare
		$passenger_fare = isset($passenger_fare[0]) ? $passenger_fare[0] : array();

		// Return data
		$this->encode($passenger_fare);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Validate selected voyage if multi-leg or return trip
	 *
	 * @access	public
	 * @return	void
	 */
	public function get_total_fare()
	{
		$id_fare_code = $this->tools->getPost('id_fare_code');
		
		$query = '';
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get details for selected evoucher
	 *
	 * @access	public
	 * @return		void
	 */
	public function get_evoucher()
	{
		$evoucher_code = $this->tools->getPost('evoucher_code');
		
		$query = $this->evoucher_series->displayList(array('evoucher_code'=>$evoucher_code));
		
		print json_encode($query);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Get details for selected discount code
	 *
	 * @access	public
	 * @return		void
	 */
	public function get_discount()
	{
		$discount_id = $this->tools->getPost('discount_id');
		
		$query = $this->discount->displayList(array('id_discount'=>$discount_id));
		
		print json_encode($query);
	}

	/*
	 * Cast array to object
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function _cast_to_obj($arr)
	{
		// Cast to object
		return (object) $arr;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Encode object
	 *
	 * @access	protected
	 * @return	json
	 */
	protected function encode($data)
	{
		echo json_encode($data);
	}
   
}

/* End of file port.php */
/* Location: ./application/modules_core/adminpanel/controllers/port/port.php */