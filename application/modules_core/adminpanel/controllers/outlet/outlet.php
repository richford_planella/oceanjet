<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Outlet Class
|--------------------------------------------------------------------------
|
| Outlet Content Management
|
| @category	Controller
| @author		Ronald Meran
*/
class Outlet extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		// Outlet Models
		$this->load->model(admin_dir('outlet/outlet_model'));                
		$this->load->model(admin_dir('port/port_model'));           		
		
		// Initialize
		$this->port = new Port_Model();
		$this->outlet = new Outlet_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Outlet Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		// Get all outlets
		$outlet = $this->outlet->displaylist();
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Outlet')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.outlet.js')))),
			'outlet'		=> $outlet,
		);
		
		parent::displayTemplate(admin_dir('outlet/outlet'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Outet Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// get user
		$outlet = new Outlet_Model($this->id);
						
		// Check if a record exists
		$outlet->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'View Outlet')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.outlet.js')))),
			'outlet'		=> $outlet,
			'port'			=> $this->port->displaylist()
		);
		
		parent::displayTemplate(admin_dir('outlet/form/outlet'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get outlet
		$outlet = new Outlet_Model($this->id);
		
		// Check if a record exists
		$outlet->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($outlet->toggleStatus())
		{	
			// Check if status is in the query
			$toggle = isset($outlet->enabled) ? $outlet->enabled : FALSE;
			$outlet_name = isset($outlet->outlet) ? $outlet->outlet : 'outlet';
			
			// Set confirmation message
			if($toggle)
				$this->session->set_flashdata('confirm', ucwords($outlet_name).' has been de-activated');
			else
				$this->session->set_flashdata('confirm', ucwords($outlet_name).' has been re-activated');
			
			$this->session->set_flashdata('id', $outlet->id);
		}
		else
		{
			// Set confirmation message
			$this->session->set_flashdata('note', 'Error in updating outlet.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Outlet
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Create Outlet')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), 
									array("js_files" => array(
											js_dir('jquery', 'jquery.outlet.js'), 
											js_dir('jquery', 'jquery.formatCurrency.js'), 
											js_dir('jquery', 'jquery.numeric-tools.js')))),
			'port'			=> $this->port->displaylist(array("enabled" => 1)),
		);
			
		parent::displayTemplate(admin_dir('outlet/form/add/outlet'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Outlet
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// get user
		$outlet = new Outlet_Model($this->id);

		// Check if a record exists
		$outlet->redirectIfEmpty(admin_url($this->classname));
		
		// Form validation
		self::_validate();
		
		// get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array('title' => 'Edit Outlet')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), 
									array("js_files" => array(
										js_dir('jquery', 'jquery.outlet.js'), 
										js_dir('jquery', 'jquery.formatCurrency.js'), 
										js_dir('jquery', 'jquery.numeric-tools.js')))),
			'outlet'		=> $outlet,
			'port'			=> $this->port->displaylist(array("enabled" => 1)),
		);
		
		parent::displayTemplate(admin_dir('outlet/form/edit/outlet'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Delete outlet
	 *
	 * @access	public
	 * @return		void
	 */
	public function delete()
	{
		// Get outlet
		$outlet = new Outlet_Model($this->id);
		
		// Check if a record exists
		$outlet->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if($outlet->countForeignKey() > 0)
		{	
			// Set confirmation message
			$this->session->set_flashdata('note', 'Cannot delete. '.ucwords($outlet->outlet).' was already used in the system');
		}
		else
		{	
			// Set confirmation message
			if($outlet->delete())
				$this->session->set_flashdata('confirm', "Successfully deleted ".ucwords($outlet->outlet));
			else
				$this->session->set_flashdata('note', "Error deleting outlet");
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		if ($this->uri->rsegment(2) == 'add'){
			$this->form_validation->set_rules('outlet_code', 'outlet code', 'required|trim|is_unique[outlet.outlet_code]');
			$this->form_validation->set_rules('outlet', 'outlet name', 'required|trim');
		}
		
		if(strtolower($this->tools->getPost("outlet_type")) == "internal" || 
			empty($this->tools->getPost("outlet_type")))	
			$this->form_validation->set_rules('port_id', 'port', 'required|trim');
		
		$this->form_validation->set_rules('outlet_type', 'outlet type', 'required|trim');
		$this->form_validation->set_rules('outlet_commission', 'commission', 'required|trim|numeric');
		$this->form_validation->set_rules('outlet_address', 'address', 'required|trim');
		$this->form_validation->set_rules('outlet_contact_no', 'contact no', 'required|trim');
		$this->form_validation->set_rules('enabled', 'status', 'required');
                             
		if ($this->uri->rsegment(2) == 'add')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then add Outlet
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$outlet = new Outlet_Model();
			
			parent::copyFromPost($outlet, 'id_outlet');
                        
			// Check for successful insert
			if ($outlet->add())
			{
				parent::logThis($outlet->id, 'Successfully added outlet');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added outlet');
				$this->session->set_flashdata('id', $outlet->id);	
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving outlet');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then edit outlet
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		$outlet = new Outlet_Model($this->id);

		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			parent::copyFromPost($outlet, 'id_outlet');
			
			// Check for successful update
			if ($outlet->update())
			{									
				 parent::logThis($outlet->id, 'Successfully updated outlet');
                                        
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated outlet');
				$this->session->set_flashdata('id', $outlet->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating outlet');
			}
			
			redirect(admin_url($this->classname));
		}
	}
   
}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/outlet/outlet.php */