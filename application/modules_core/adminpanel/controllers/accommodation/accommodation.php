<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Accommodation Type Class
|--------------------------------------------------------------------------
|
| Accommodation Type Content Management
|
| @category Controller
| @author       Kenneth Bahia
*/
class Accommodation extends Admin_Core
{
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('accommodation/Accommodation_Model'));                
        $this->accommodation = new Accommodation_Model();
                        
        // Accommodation id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Accommodation Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        // Get all accommodation type
        $accommodation = $this->accommodation->displaylist();
        
        // Initialize data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Accommodation Types')),
            'footer'            => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.accommodation.js')))),
            'accommodation'     => $accommodation,
        );
        
        parent::displayTemplate(admin_dir('accommodation/accommodation'), $data);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * View Accommodation Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        // get Accommodation
        $accommodation = new Accommodation_Model($this->id);
                        
        // Check if a record exists
        $accommodation->redirectIfEmpty(admin_url($this->classname));
        
        // Initialize data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'View Accommodation Type')),
            'footer'            => parent::getTemplate(admin_dir('footer')),
            'accommodation'     => $accommodation,
        );
        
        parent::displayTemplate(admin_dir('accommodation/form/accommodation'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Delete Accommodation Type
     *
     * @access  public
     * @return      void
     */
    public function delete()
    {
        // get Accommodation
        $accommodation = new Accommodation_Model($this->id);
                        
        // Check if a record exists
        $accommodation->redirectIfEmpty(admin_url($this->classname));
        
        // Check if it is already in use
        if ($accommodation->countFromVesselSeats() > 0)
        {   
            // Set confirmation message
            $this->session->set_flashdata('note', 'Cannot delete. '.$accommodation->accommodation.' was already used in the system.');
        }
        else
        {
            //Delete accommodation type
            if ($accommodation->delete())
                $this->session->set_flashdata('confirm', 'Successfully deleted '.$accommodation->accommodation);
            else
                $this->session->set_flashdata('note', 'Error deleting accommodation type');
        }
        
        redirect(admin_url($this->classname));
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle accommodation type status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get accommodation type
        $accommodation = new Accommodation_Model($this->id);
        
        // Check if a record exists
        $accommodation->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($accommodation->toggleStatus())
        {   
            // Set confirmation message
            if($accommodation->enabled)
                $this->session->set_flashdata('confirm', $accommodation->accommodation.' has been de-activated!');
            else
                $this->session->set_flashdata('confirm', $accommodation->accommodation.' has been re-activated!');
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating '.$accommodation->accommodation.".");
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Accommodation Type
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();
                            
        // Initialize data
        $data = array(
            'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Create Accommodation Type')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
        );
            
        parent::displayTemplate(admin_dir('accommodation/form/add/accommodation'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Accommodation Type
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get accommodation type
        $accommodation = new Accommodation_Model($this->id);
        
        // Check if a record exists
        $accommodation->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'            => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit Accommodation Type')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'accommodation'        => $accommodation,
        );
        
        parent::displayTemplate(admin_dir('accommodation/form/edit/accommodation'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('accommodation', 'accommodation name', 'required|trim');
        $this->form_validation->set_rules('enabled', 'status', 'required');
                             
        if ($this->uri->rsegment(2) == 'add') {
            $this->form_validation->set_rules('accommodation_code', 'accommodation code', 'required|trim|is_unique[accommodation.accommodation_code]');
            $this->form_validation->set_rules('palette', 'colour code', 'required|trim');
            self::_addInfo();
        }
            
        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Accommodation Type
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $accommodation = new Accommodation_Model();
            
            parent::copyFromPost($accommodation, 'id_accommodation');
            $accommodation->accommodation_code = strtoupper($accommodation->accommodation_code);
                        
            // Check for successful insert
            if ($accommodation->add())
            {
                parent::logThis($accommodation->id, 'Successfully added accommodation type');
                
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully added accommodation type');
                $this->session->set_flashdata('id', $accommodation->id);   
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving accommodation type');
            }
            
            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit Accommodation Type
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $accommodation = new Accommodation_Model($this->id);

        if (trim($this->tools->getPost("accommodation_code")) == trim($accommodation->accommodation_code)) {
            $this->form_validation->set_rules('accommodation_code', 'accommodation code', 'required|trim');
            //$this->form_validation->set_rules('palette', 'colour code', 'required|trim');
        } else {
            $this->form_validation->set_rules('accommodation_code', 'accommodation code', 'required|trim|is_unique[accommodation.accommodation_code]');
            //$this->form_validation->set_rules('palette', 'colour code', 'required|trim');
        }

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($accommodation, 'id_accommodation');
            $accommodation->accommodation_code = strtoupper($accommodation->accommodation_code);
            
            // Check for successful update
            if ($accommodation->update())
            {                                   
                 parent::logThis($accommodation->id, 'Successfully updated accommodation type');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated accommodation type');
                $this->session->set_flashdata('id', $accommodation->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating accommodation type');
            }
            
            redirect(admin_url($this->classname));
        }
    }
   
}

/* End of file accommodation.php */
/* Location: ./application/modules_core/adminpanel/controllers/accommodation/accommodation.php */