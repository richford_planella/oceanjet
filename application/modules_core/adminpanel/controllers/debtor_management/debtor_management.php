<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Commission Class
|--------------------------------------------------------------------------
|
| Commission Content Management
|
| @category	Controller
| @author		Philip Reamon
*/
class Debtor_Management extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		$this->load->model(admin_dir('third_party_outlet/debtor_management_model'));
		$this->load->model(admin_dir('third_party_outlet/debtor_wallet_model'));
		$this->load->model(admin_dir('third_party_outlet/debtor_outlet_model')); 
		$this->load->model(admin_dir('outlet/outlet_model')); 
		$this->load->model(admin_dir('user/user_account_model'));                
		$this->debtor_management = new Debtor_Management_Model();
		$this->debtor_outlet = new Debtor_Outlet_Model();
		$this->outlet = new Outlet_Model();
		$this->user = new User_Account_Model();
		$this->debtor_wallet = new Debtor_Wallet_Model();
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Commission Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		

		$data = array(
			'header' => Modules::run(admin_dir('header/call_header'),array('title' => 'Debtor Masterlist')),
			'footer' => parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.add-debtor.js')))),
			'debtor_info' => $this->debtor_management->displaylist(),
			'debtor_outlet' => $this->debtor_outlet->displaylist(),
			'user' => $this->user->displaylist(array("user_profile_id"=>11))

			);
		
		
		// $debtor_info = $this->debtor_management->displaylist();
		 //print '<pre>'.print_r($debtor_info,true).'</pre>';exit;
		// foreach ($debtor_info as $debtor) {
		// 	$arr_debtor['debtor_id'] = $debtor->id_debtor;
		// 	$arr_debtor['debtor_code'] = $debtor->debtor_code;
		// 	$arr_debtor['debtor_name'] = $debtor->firstname.' '.$debtor->lastname;
		// 	$arr_debtor['outlets'] = $this->debtor_outlet->displaylist(array("debtor_id"=>$debtor->id_debtor));
		// 	$arr_debtor['outlet_commission'] = $this->debtor_outlet->displaylist(array("debtor_id"=>$debtor->id_debtor));
		// 	$arr_debtor['wallet_amount'] = $debtor->amount;
		// 	$arr_debtor['debtor_enabled'] = $debtor->enabled;

		// }
		// if (count($arr_debtor) > 0) {
		// 	$data['debtor_information'][] = $arr_debtor;	
		// } else {
		// 	$data['debtor_information']= array();
		// }
		
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_management/debtor_management'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Commission Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// Get commission
		$debtor_management = new Debtor_Management_Model($this->id);
		// Get commission
		$debtor_outlet = new Debtor_Outlet_Model();					
		// Check if a record exists
		$debtor_management->redirectIfEmpty(admin_url($this->classname));
		
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Debtor Details')),
			'footer'		=> parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.add-debtor.js')))),
			'debtor_management'		=> $debtor_management,
			'outlet' => $this->outlet->displaylist(array("outlet_type"=>"External")),
			'user' => $this->user->displaylist(),
			'debtor_outlet'=> $debtor_outlet->displaylist(array("debtor_id"=>$this->id)),

		);
		
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_management/form/debtor_management'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		$debtor_management = new Debtor_Management_Model($this->id);
		$debtor_user_id = $debtor_management->displaylist(array("id_debtor" => $this->id));
		// get user id
		foreach ($debtor_user_id as $du) {
			$debtor_user_id = $du->user_id;
		}
		// get debtor fullname
		$debtor_name = $this->user->displaylist(array("user_id"=>$debtor_user_id));
		//
		foreach ($debtor_name as $dn) {
			$debtor_name = $dn->firstname." ".$dn->lastname;
		}
		
		// Check for successful toggle
		if ($debtor_management->toggleStatus())
		{	
				// Set confirmation message
			if($debtor_management->enabled)
				$this->session->set_flashdata('confirm', $debtor_name. ' has been deactivated');
			else
				$this->session->set_flashdata('confirm', $debtor_name. ' has been activated');

				$this->session->set_flashdata('id', $debtor_management->id);
		}
		else
		{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating Debtor.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Commission
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// echo '<pre>';
		// print_r($this->tools->getPost());
		// exit;
		$do_array=array();
		// echo '<pre>';
		// print_r($_POST);exit;
		// Form validation
		self::_validate();					
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Create Debtor')),
			'footer'	=> parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.add-debtor.js')))),
			'outlet' => $this->outlet->displaylist(array("outlet_type"=>"External","u.enabled"=>1)),
			'user' => $this->user->displaylist(array("user_profile_id"=>11)),
			'debtor_outlet' => $this->debtor_outlet->displaylist(),
			'debtor_code' => $this->tools->getPost('debtor_code'),
			'debtor_name' => $this->tools->getPost('user_id'),
			'status' => $this->tools->getPost('enabled'),
			'selected_outlets' => $this->tools->getPost('outlet_id')
		);

		// extract outlet id
		foreach ($this->debtor_outlet->displaylist() as $do) {
			$do_array[] = $do->outlet_id;
		}

		$data['deb_outlet'] = $do_array;
			
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_management/form/add/debtor_management'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Commission
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		$arr_outlet = array();
		// get debtor
		$debtor_management = new Debtor_Management_Model($this->id);
		$debtor_outlet = new Debtor_Outlet_Model();
		if($this->tools->getPost('submit'))
		{
			if ($this->tools->getPost('outlet_id')) {
				foreach ($this->tools->getPost('outlet_id') as $key => $val) {
					$arr_outlet[] = new Outlet_Model($val);
				}
			} 	
			
		}else {
			$arr_outlet = $debtor_outlet->displaylist(array("debtor_id"=>$this->id));
		}

		// Check if a record exists
		$debtor_management->redirectIfEmpty(admin_url($this->classname));
		// Form validation
		self::_validate();
		// get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'),array('title' => 'Edit Debtor')),
			'footer'		=> parent::getTemplate(admin_dir('footer'),array('js_files'=>array(js_dir('jquery/jquery.edit-debtor.js')))),
			'debtor'		=> $debtor_management,
			'outlet' => $this->outlet->displaylist(array("outlet_type"=>"External","u.enabled"=>1)),
			'user' => $this->user->displaylist(array("user_profile_id"=>11)),
			'selected_outlets' => $arr_outlet
		);
		
		parent::displayTemplate(admin_dir('third_party_outlet/debtor_management/form/edit/debtor_management'),$data);
	}
  
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		$arr_outlet = array();
		
		$this->form_validation->set_rules('user_id', 'debtor name', 'required|trim');
		
           
		if ($this->uri->rsegment(2) == 'add') {
			$this->form_validation->set_rules('debtor_code', 'debtor code', 'required|trim|is_unique[debtor.debtor_code]');
			
			self::_addInfo();
		}

			// if (!array_key_exists("outlet_id",$this->tools->getPost()) || count($this->tools->getPost("outlet_id")) == 0) {
			// 	$this->form_validation->set_rules('outlet_id[]', 'outlet', 'required|trim');
			// }
		
		if ($this->uri->rsegment(2) == 'edit') {
		
			$this->form_validation->set_rules('outlet_id[]', 'outlet', 'required|trim');
			self::_editInfo();
		}
			
			
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add Commission
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
        

           // print '<pre>'.print_r($_POST,true).'</pre>';exit;
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$debtor_management = new Debtor_Management_Model();
			// print '<pre>'.print_r($this->tools->getPost('outlet_id'),true).'</pre>';exit;
			parent::copyFromPost($debtor_management, 'id_debtor');
                        
			// Check for successful insert
			if ($debtor_management->add())
			{
				parent::logThis($debtor_management->id, 'Successfully added Debtor');
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added Debtor');
				$this->session->set_flashdata('id', $debtor_management->id);
				// extract outlet id
				foreach ($this->tools->getPost('outlet_id') as $key => $val) {
					// insert to table
					$this->db->insert('debtor_outlet', array("debtor_id"=>$debtor_management->id,"outlet_id"=>$val));	
				}	
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving Debtor');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit Commission
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		
		$outlet_array = array();
		//$debtor_outlet = $this->debtor_outlet->displaylist(array("debtor_id"=>$this->id));
		
		$debtor_management = new Debtor_Management_Model($this->id);
		
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
              // print '<pre>'.print_r($debtor_management->updateDebtorOutlet($this->tools->getPost('outlet_id'),$this->id),true).'</pre>';exit;      
			parent::copyFromPost($debtor_management, 'id_debtor');

			
			// Check for successful update
			if ($debtor_management->update())
			{									
				 parent::logThis($debtor_management->id, 'Successfully updated Debtor');
                                        
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated Debtor');
				$this->session->set_flashdata('id', $debtor_management->id);
				
				// foreach ($debtor_outlet as $t) {
				// 	$outlet_array[] = $t->outlet_id;
				// }
				
				$debtor_outlet = new Debtor_Outlet_Model();

				$debtor_outlet->delete(array('debtor_id' => $debtor_management->id));

				if($this->tools->getPost('outlet_id'))
					foreach($this->tools->getPost('outlet_id') as $key => $val)
						$debtor_outlet->add(array("debtor_id"=>$debtor_management->id,"outlet_id"=>$val));

				// loop through
				// foreach ($this->tools->getPost('outlet_id') as $key => $val) {
				// 	if (in_array($val,$outlet_array)) {
				// 		$debtor_outlet->delete(array("debtor_id"=>$this->id,"outlet_id"=>$val));	
				// 	}
				// 	// for new values	
				// 	if(!in_array($val,$outlet_array)) {
				// 		$debtor_outlet->add(array("debtor_id"=>$this->id,"outlet_id"=>$val));	
				// 	}
				// // insert to table
				// }
				
				
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating commission');
			}
			
			redirect(admin_url($this->classname));
		}
	}

	public function delete() {
		$debtor_management = new Debtor_Management_Model($this->id);
		$debtor_user_id = $debtor_management->displaylist(array("id_debtor" => $this->id));
		// get user id
		foreach ($debtor_user_id as $du) {
			$debtor_user_id = $du->user_id;
		}
		// get debtor fullname
		$debtor_name = $this->user->displaylist(array("user_id"=>$debtor_user_id));
		//
		foreach ($debtor_name as $dn) {
			$debtor_name = $dn->firstname." ".$dn->lastname;
		}
		// check if we have a debtor id in debtor wallet
		if ($debtor_management->countFromUser()) {
			$this->session->set_flashdata('note', 'Cannot delete '.$debtor_name.' was already used in the system.');
			redirect(admin_url($this->classname));	
		} else {
			$this->session->set_flashdata('confirm', 'Successful in deleting Debtor '.$debtor_name);
			$debtor_management->delete();
			$this->debtor_outlet->delete(array("debtor_id" => $this->id));
			redirect(admin_url($this->classname));
		}
	}

	public function delete_outlet() {
		// $delete = $this->db->delete('debtor_outlet', array('id_debtor_outlet' => $this->tools->getGet('debtor_outlet_id')));
		$debtor_outlet = new Debtor_Outlet_Model($this->tools->getGet('debtor_outlet_id'));
		$debtor_outlet->delete();
			
		 
	}

	
   
}

/* End of file commission.php */
/* Location: ./application/modules_core/adminpanel/controllers/commission/commission.php */