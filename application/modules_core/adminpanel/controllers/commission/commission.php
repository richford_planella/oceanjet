<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Commission Class
|--------------------------------------------------------------------------
|
| Commission Content Management
|
| @category	Controller
| @author		Ronald Meran
*/
class Commission extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		$this->load->model(admin_dir('commission/commission_model'));                
		$this->commission = new Commission_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Commission Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		// Get all commission
		$commission = $this->commission->displaylist();
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'commission'		=> $commission,
		);
		
		parent::displayTemplate(admin_dir('commission/commission/commission'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Commission Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// Get commission
		$commission = new Commission_Model($this->id);
						
		// Check if a record exists
		$commission->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'commission'		=> $commission,
		);
		
		parent::displayTemplate(admin_dir('commission/commission/form/commission'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get commission
		$commission = new Commission_Model($this->id);
		
		// Check if a record exists
		$commission->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($commission->toggleStatus())
		{	
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successful in updating commission.');
				$this->session->set_flashdata('id', $commission->id);
		}
		else
		{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating commission.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Commission
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header')),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.numeric-tools.js')))),
		);
			
		parent::displayTemplate(admin_dir('commission/commission/form/add/commission'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Commission
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// get user
		$commission = new Commission_Model($this->id);
		
		// Check if a record exists
		$commission->redirectIfEmpty(admin_url($this->classname));
		
		// Form validation
		self::_validate();
		
		// get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'commission'		=> $commission,
		);
		
		parent::displayTemplate(admin_dir('commission/commission/form/edit/commission'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		$this->form_validation->set_rules('commission_code', 'Commission Code', 'required|trim|is_unique[commission.commission_code]');
		$this->form_validation->set_rules('commission_percentage', 'Percentage', 'required|trim|decimal');
		$this->form_validation->set_rules('commission', 'Description', 'required');
		$this->form_validation->set_rules('enabled', 'Status', 'required');
                             
		if ($this->uri->rsegment(2) == 'add')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add Commission
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$commission = new Commission_Model();
			
			parent::copyFromPost($commission, 'id_commission');
                        
			// Check for successful insert
			if ($commission->add())
			{
				parent::logThis($commission->id, 'Successfully added Commission');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added Commission');
				$this->session->set_flashdata('id', $commission->id);	
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving Commission');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit Commission
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		$commission = new Commission_Model($this->id);

		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                    
			parent::copyFromPost($commission, 'id_commission');
			
			// Check for successful update
			if ($commission->update())
			{									
				 parent::logThis($commission->id, 'Successfully updated commission');
                                        
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated commission');
				$this->session->set_flashdata('id', $commission->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating commission');
			}
			
			redirect(admin_url($this->classname));
		}
	}
   
}

/* End of file commission.php */
/* Location: ./application/modules_core/adminpanel/controllers/commission/commission.php */