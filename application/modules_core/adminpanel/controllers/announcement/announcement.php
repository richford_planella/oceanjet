<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Announcement Class
|--------------------------------------------------------------------------
|
| Announcement Content Management
|
| @category	Controller
| @author		Ronald Meran
*/
class Announcement extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		// Load helper
		$this->load->helper('text');
		
		// Announcement Model
		$this->load->model(admin_dir('user/profile_model'));		
		$this->load->model(admin_dir('announcement/announcement_model'));
		$this->load->model(admin_dir('announcement/announcement_visibility_model'));		
		
		// Initialize
		$this->user_profile = new Profile_Model();
		$this->announcement = new Announcement_Model();
		$this->visibility = new Announcement_Visibility_Model();
						
		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Announcement Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		// Get all announcement
		$announcement = $this->announcement->displaylist();
		
		// Join user role
		$announcement = $this->_join_role($announcement, TRUE);
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array("title" => "Announcement")),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array( 
									js_dir('tinymce/js/tinymce', 'tinymce.min.js'), 
									js_dir('jquery', 'jquery.announcement.js')
								))),
			'announcement'		=> $announcement,
		);
		
		parent::displayTemplate(admin_dir('announcement/announcement'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Announcement Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		// Get Announcement
		$announcement = new Announcement_Model($this->id);
		
		// Check if a record exists
		$announcement->redirectIfEmpty(admin_url($this->classname));
		
		// Join user role
		$announcement = $this->_join_role($announcement, FALSE);
		
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array("title" => "View ".$announcement->announcement)),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array( 
									js_dir('tinymce/js/tinymce', 'tinymce.min.js'), 
									js_dir('jquery', 'jquery.announcement.js')
								))),
			'announcement'		=> $announcement,
		);
		
		parent::displayTemplate(admin_dir('announcement/form/announcement'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle user status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get announcement
		$announcement = new Announcement_Model($this->id);
		
		// Check if a record exists
		$announcement->redirectIfEmpty(admin_url($this->classname));	
		
		// Check for successful toggle
		if ($announcement->toggleStatus())
		{	
			// Check if status is in the query
			$toggle = isset($announcement->enabled) ? $announcement->enabled : FALSE;
			$announcement_name = isset($announcement->announcement) ? $announcement->announcement : 'announcement';
			
			// Set confirmation message
			if($toggle)
				$this->session->set_flashdata('confirm', ucwords($announcement_name).' has been de-activated');
			else
				$this->session->set_flashdata('confirm', ucwords($announcement_name).' has been re-activated');
			
			$this->session->set_flashdata('id', $announcement->id);
		}
		else
		{
			// Set confirmation message
			$this->session->set_flashdata('note', 'Error in updating announcement.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Announcement
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();
							
		// Initialize data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array("title" => "Create Announcement")),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array( 
									js_dir('tinymce/js/tinymce', 'tinymce.min.js'), 
									js_dir('jquery', 'jquery.announcement.js')
								))),
			'user_profile'	=> $this->user_profile->displayList(array('enabled' => 1)),
		);
			
		parent::displayTemplate(admin_dir('announcement/form/add/announcement'),$data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Edit Announcement
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// Get announcement
		$announcement = new Announcement_Model($this->id);
		
		// Check if a record exists
		$announcement->redirectIfEmpty(admin_url($this->classname));
		
		// Join user role
		$announcement = $this->_join_role($announcement, FALSE);		

		// Form validation
		self::_validate();
		
		// Get data
		$data = array(
			'header'	=> Modules::run(admin_dir('header/call_header'), array("title" => "Edit Announcement")),
			'footer'		=> parent::getTemplate(admin_dir('footer'), array("js_files" => array( 
									js_dir('tinymce/js/tinymce', 'tinymce.min.js'), 
									js_dir('jquery', 'jquery.announcement.js')
								))),
			'announcement'		=> $announcement,
		);
		
		parent::displayTemplate(admin_dir('announcement/form/edit/announcement'),$data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Delete announcement
	 *
	 * @access	public
	 * @return		void
	 */
	public function delete()
	{
		// Get announcement
		$announcement = new Announcement_Model($this->id);
		
		// Check if a record exists
		$announcement->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if($announcement->countForeignKey() > 0)
		{	
			// Set confirmation message
			$this->session->set_flashdata('note', 'Cannot delete. '.ucwords($announcement->announcement).' was already used in the system');
		}
		else
		{	
			// Delete Announcement child
            $announcement_visibility = new Announcement_Visibility_Model();
            $announcement_visibility->delete(array("announcement_id" => $this->id));
			
			// Set confirmation message
			if($announcement->delete())
				$this->session->set_flashdata('confirm', 'Successfully deleted '.ucwords($announcement->announcement));
			else
				$this->session->set_flashdata('note', "Error deleting announcement");
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{

		if ($this->uri->rsegment(2) == 'add')
			$this->form_validation->set_rules('announcement_code', 'announcement code', 'required|trim|is_unique[announcement.announcement_code]');
			
		$this->form_validation->set_rules('announcement', 'title', 'required|trim');
		$this->form_validation->set_rules('announcement_content', 'content', 'required|trim');
		
		// Visibility required
		if($this->tools->getPost('announcement_visibility'))
			foreach($this->tools->getPost('announcement_visibility') as $key => $value){
				$this->form_validation->set_rules("announcement_visibility[".$value."]", 'visibility', 'trim');
			}
		
		$this->form_validation->set_rules('enabled', 'Status', 'required');
                             
		if ($this->uri->rsegment(2) == 'add')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add Announcement
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$announcement = new Announcement_Model();
			$visibility 			= new Announcement_Visibility_Model();
			
			parent::copyFromPost($announcement, 'id_announcement');						
						
			// Check for successful insert
			if ($announcement->add())
			{
				// Copy post data from visibility
				parent::copyFromPost($visibility, 'id_announcement_visibility');

				// Add Visibility
				foreach($this->tools->getPost("announcement_visibility") as $key => $value)
				{
					// Get the data
					$data = array("announcement_id" => $announcement->id, "user_profile_id" => $value);
					$visibility->add($data);
				}
				
				// Log announcement
				parent::logThis($announcement->id, 'Successfully added announcement');
				
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added announcement');
				$this->session->set_flashdata('id', $announcement->id);	
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving announcement');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
   // --------------------------------------------------------------------
	
	/*
	 * Validate then edit Announcement
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		$announcement = new Announcement_Model($this->id);
		$visibility 			= new Announcement_Visibility_Model($this->id);

		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                    
			parent::copyFromPost($announcement, 'id_announcement');
			
			// Check for successful update
			if ($announcement->update())
			{
				// Copy post data from visibility
				parent::copyFromPost($visibility, 'id_announcement_visibility');

				// Add Visibility
				foreach($this->tools->getPost("announcement_visibility") as $key => $value)
				{
					// Delete data
					$data = array("announcement_id" => $visibility->id);
					$visibility->delete($data);
					
					// Add data
					$data = array("announcement_id" => $announcement->id, "user_profile_id" => $value);
					$visibility->add($data);
				}
						
				parent::logThis($announcement->id, 'Successfully updated announcement');
									
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated announcement');
				$this->session->set_flashdata('id', $announcement->id);
				
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating announcement');
			}
			
			redirect(admin_url($this->classname));
		}
	}
	
	 // --------------------------------------------------------------------
	
	/*
	 * Join user role in Announcement_Model
	 *
	 * @access	public
	 * @return		object
	 */
	private function _join_role($announcement, $index)
	{	
		// Initialize
		$arr = array();
		
		// Get the visibility
		if(isset($announcement->id))
			$arr = array("announcement_id" => $announcement->id_announcement);

		// Visibility
		$visibility = $this->visibility->displaylist($arr);
		
		// Get user profile
		$user_profile	= $this->user_profile->displayList(array('enabled' => 1));

		// Combine announcement and visibility
		if($index){
			foreach($announcement as $akey => $afield){
				foreach($visibility as $vkey => $vfield){
					if($vfield->announcement_id == $afield->id_announcement){
						foreach($user_profile as $pkey => $pvalue){
							if($pvalue->id_user_profile ==$vfield->user_profile_id)
								$announcement[$akey]->announcement_visibility[$vfield->user_profile_id] = $pvalue->user_profile;
						}
					}
				}
			}			
		}
		else{
		
			// Initialize
			$_announcement = new stdClass();
			
			// Combine announcement and visibility
			foreach($user_profile as $pkey => $pvalue){
				foreach($visibility as $vkey => $vvalue){
					// Select user profile/s
					$_announcement->user_profile[$pvalue->id_user_profile] = $pvalue;
					
					if($pvalue->id_user_profile == $vvalue->id_user_profile)
						$_announcement->user_profile[$pvalue->id_user_profile]->is_checked = 1;

				}
			}		

			// Re-instantiate
			$announcement = (object) array_merge((array) $announcement, (array) $_announcement);
			
		}

		return $announcement;

	}
   
}

/* End of file announcement.php */
/* Location: ./application/modules_core/adminpanel/controllers/announcement/announcement.php */