<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Adminpanel Class
|--------------------------------------------------------------------------
|
| Delegates functions for account label records
|
| @category		Controller
| @author		Baladeva Juganas
*/

class Adminpanel extends Admin_Core {
    
        public function __construct()
        {
            $this->classname = strtolower(get_class());
            parent::__construct();
        }

        public function index()
        {
            $data = array(	'header' => Modules::run(admin_dir('header/call_dashboard_header'),array('title' => 'Dashboard')),
                                'footer' => parent::getTemplate(admin_dir('adminpanel/footer')),
                        );
                
            $this->load->view(admin_dir('adminpanel/adminpanel'), $data);
        }
    
    
}