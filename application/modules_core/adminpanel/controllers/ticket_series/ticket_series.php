<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Ticket Series Class
|--------------------------------------------------------------------------
|
| Ticket Series Management
|
| @category	Controller
| @author   Gian Asuncion
*/
class Ticket_Series extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
		
		parent::__construct();

		$this->load->model(admin_dir('ticket_series/ticket_series_model'));
		$this->ticket_series = new Ticket_Series_Model();

		$this->load->model(admin_dir('ticket_series/ticket_series_ref_model'));
		$this->ticket_series_ref = new Ticket_Series_Ref_Model();

		$this->load->model(admin_dir('user/user_account_model'));
        $this->load->model(admin_dir('user/user_model'));


		// Outlet id
		$this->id = $this->uri->rsegment(3);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Display Outlet Master List
	 *
	 * @access	public
	 * @return		void
	 */
	public function index()
	{                
		// Get all ticket series
		$ticket_series = $this->ticket_series->getGeneratedTickets();
		
		// Initialize data
		$data = array(
			'header'	    => Modules::run(admin_dir('header/call_header'),array('title'=>'Ticket Series Assignment')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'ticket_series'	=> $ticket_series,
		);
		
		parent::displayTemplate(admin_dir('ticket_series/ticket_series/ticket_series'), $data);
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * View Ticket Series Information
	 *
	 * @access	public
	 * @return		void
	 */
	public function view()
	{
		$ticket_series = new Ticket_Series_Model($this->id);
        $ticket_series_ref = new Ticket_Series_Ref_Model();
        $assigned_tickets_per_agents = $ticket_series_ref->getAssignedTickets($ticket_series->id); //Get all assigned tickets
						
		// Check if a record exists
		$ticket_series->redirectIfEmpty(admin_url($this->classname));
		
		// Initialize data
		$data = array(
			'header'	    => Modules::run(admin_dir('header/call_header'),array('title'=>'View Ticket Series Assignment')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'ticket_series'	=> $ticket_series,
            'ticket_series_id'  => $ticket_series->id,
            'assigned_tickets_per_agents'  => $assigned_tickets_per_agents,
		);
		
		parent::displayTemplate(admin_dir('ticket_series/ticket_series/form/ticket_series'),$data);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle status
	 *
	 * @access	public
	 * @return		void
	 */
	public function toggle()
	{
		// Get outlet
		$ticket_series = new Ticket_Series_Model($this->id);
		
		// Check if a record exists
		$ticket_series->redirectIfEmpty(admin_url($this->classname));
		
		// Check for successful toggle
		if ($ticket_series->toggleStatus())
		{
			//disable or enable reference tickets
			$ticket_series_ref = new Ticket_Series_Ref_Model();
			$ticket_series_ref->toggleRefStatus($ticket_series->id,$ticket_series->enabled,true);

			// Set confirmation message
			$this->session->set_flashdata('confirm', 'Successful in updating ticket series.');
			$this->session->set_flashdata('id', $ticket_series->id);
		}
		else
		{
			// Set confirmation message
			$this->session->set_flashdata('note', 'Error in updating ticket series.');
		}
		
		redirect(admin_url($this->classname));
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Add Ticket
	 *
	 * @access	public
	 * @return		void
	 */
	public function add()
	{
		// Form validation
		self::_validate();

		//Get prev generated ticket
		$prev_generated_ticket = $this->ticket_series->sumGeneratedTicket();

		// Initialize data
		$data = array(
			'header'	    => Modules::run(admin_dir('header/call_header'),array('title'=>'Create Ticket Set')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'prev_generated_ticket' => $prev_generated_ticket
		);
			
		parent::displayTemplate(admin_dir('ticket_series/ticket_series/form/add/ticket_series'),$data);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Edit Ticket
	 *
	 * @access	public
	 * @return		void
	 */
	public function edit()
	{
		// get user
		$ticket_series = new Ticket_Series_Model($this->id);
		$ticket_series_ref = new Ticket_Series_Ref_Model();
		$user_model = new User_Account_Model();

		// Check if a record exists
		$ticket_series->redirectIfEmpty(admin_url($this->classname));

        // Form validation
        if($this->tools->getPost('submit') AND $this->tools->getPost('submit') == 'Assign Tickets')
            self::_assign();
        elseif($this->tools->getPost('submit') AND $this->tools->getPost('submit') == 'Save changes')
		    self::_validate();

		$available_tickets = $ticket_series_ref->countBy(array('ticket_series_id'=>$ticket_series->id,'issued'=>0,'enabled'=>1,'user_id'=>0)); //Get available tickets
        $sales_agents      = $user_model->displayList(array('user_profile_id' => 4, 'u.enabled'=>1),array('firstname'=>'ASC'));
        $assigned_tickets_per_agents = $ticket_series_ref->getAssignedTickets($ticket_series->id); //Get all assigned tickets

		// get data
		$data = array(
			'header'		=> Modules::run(admin_dir('header/call_header'),array('title'=>'Edit Ticket Series Assignment')),
			'footer'		=> parent::getTemplate(admin_dir('footer')),
			'ticket_series'	=> $ticket_series,
			'available_tickets' => $available_tickets,
			'sales_agents'      => $sales_agents,
			'ticket_series_id'  => $ticket_series->id,
			'assigned_tickets_per_agents'  => $assigned_tickets_per_agents,
            'footer'        => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.ticket.series.assignment.js')))),
		);
		
		parent::displayTemplate(admin_dir('ticket_series/ticket_series/form/edit/ticket_series'),$data);
	}

	/*
	 * Assign Ticket
	 *
	 * @access	public
	 * @return		void
	 */
	private function _assign()
	{
        $this->form_validation->set_rules('limit_ticket', 'available tickets', 'required|trim|integer|greater_than[0]');
        $this->form_validation->set_rules('sales_agent_id', 'sales agent', 'required');

        $ticket_series_id = $this->id;
        //$ticket_series = new Ticket_Series_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE) {
            $ticket_series_ref = new Ticket_Series_Ref_Model();

            // Form validation
            $limit = $this->tools->getPost('limit_ticket');
            $sales_agent_id = $this->tools->getPost('sales_agent_id');

            //If assigned user want to change the ownership of the ticket
            if ($this->tools->getPost('assigned_sales_agent')) {
                $ticket_lists = $ticket_series_ref->displayList(array('ticket_series_id' => $ticket_series_id, 'issued' => 0, 'enabled' => 1, 'user_id' => $this->tools->getPost('assigned_sales_agent')), false, false, $limit, 1); //Get available tickets
                $this->session->set_flashdata('confirm', 'Successfully transferred ticket series');
            } else {
                $ticket_lists = $ticket_series_ref->displayList(array('ticket_series_id' => $ticket_series_id, 'issued' => 0, 'enabled' => 1, 'user_id' => 0), false, false, $limit, 1); //Get available tickets
                $this->session->set_flashdata('confirm', 'Successfully assigned ticket series');
            }

            //update selected tickets
            foreach ($ticket_lists as $ticket_list) {
                $ticket_series_ref = new Ticket_Series_Ref_Model($ticket_list->id_ticket_series_info);
                //update each ticket
                $ticket_series_ref->update(array(), array('user_id' => $sales_agent_id));
            }

            redirect(admin_url($this->classname, 'edit', $this->id)); //Redirect to edit form
        }


	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access	private
	 * @return		void
	 */
	private function _validate()
	{
		$this->form_validation->set_rules('no_tickets', 'number of tickets to generate', 'required|trim|integer|greater_than[0]');
		$this->form_validation->set_rules('enabled', 'status', 'required');

		if ($this->uri->rsegment(2) == 'add')
			self::_addInfo();
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add Ticket Series
	 *
	 * @access	private
	 * @return		void
	 */
	private function _addInfo()
	{
           
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
			$ticket_series = new Ticket_Series_Model();

			//Set ticket_ends_to
			$this->tools->setPost('ticket_ends_to', ($this->tools->getPost('ticket_starts_from') + $this->tools->getPost('no_tickets')) - 1);

			parent::copyFromPost($ticket_series, 'id_ticket_outlet');

			// Check for successful insert
			if ($ticket_series->add())
			{
				parent::logThis($ticket_series->id, 'Successfully added ticket series ');

				//Insert into reference table
				for($x=$this->tools->getPost('ticket_starts_from')-1;$x<$this->tools->getPost('ticket_ends_to');$x++){

					//Call reference table
					$ticket_series_ref = new Ticket_Series_Ref_Model();

					$this->tools->setPost('ticket_series_id', $ticket_series->id);
					$this->tools->setPost('ticket_no', $x+1);
					$this->tools->setPost('issued', 0);
					$this->tools->setPost('enabled', 1);

					parent::copyFromPost($ticket_series_ref, 'id_ticket_series_info');

					$ticket_series_ref->add();
				}

				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added ticket series ');
				$this->session->set_flashdata('id', $ticket_series->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving Ticket series');
			}
			
			redirect(admin_url($this->classname));
						
		}
	}
        
	// --------------------------------------------------------------------
	
	/*
	 * Validate then edit ticket series
	 *
	 * @access	private
	 * @return		void
	 */
	private function _editInfo()
	{
		$ticket_series = new Ticket_Series_Model($this->id);

		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                    
			parent::copyFromPost($ticket_series, 'id_outlet');
			
			// Check for successful update
			if ($ticket_series->update())
			{									
				 parent::logThis($ticket_series->id, 'Successfully updated ticket series');

				//disable or enable reference tickets
				$ticket_series_ref = new Ticket_Series_Ref_Model();
				$ticket_series_ref->toggleRefStatus($ticket_series->id,$ticket_series->enabled,false);

				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated ticket series');
				$this->session->set_flashdata('id', $ticket_series->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating ticket series');
			}
			
			redirect(admin_url($this->classname));
		}
	}
   
}

/* End of file outlet.php */
/* Location: ./application/modules_core/adminpanel/controllers/ticket_series/ticket_series.php */