<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| E-Ticket Class
|--------------------------------------------------------------------------
|
| E-Ticket Content Management
|
| @category Controller
| @author   Kenneth Bahia
*/
class Eticket extends Admin_Core
{
    private $img_path = NULL;
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('eticket/Eticket_Model'));                
        $this->eticket = new Eticket_Model();
                        
        // Outlet id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display E-ticket Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        $eticket = new Eticket_Model(1);
        
        // Check if a record exists
        $eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();

        // Eticket details
        $eticket->eticket_details = parent::getTemplate(admin_dir('eticket/form/itinerary'));

        // Get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'E-Ticket/Itinerary Details')),
            'footer'    => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('tinymce/js/tinymce', 'tinymce.min.js'), js_dir('jquery', 'jquery.eticket.js')))),
            'eticket'   => $eticket,
        );
        
        parent::displayTemplate(admin_dir('eticket/form/eticket'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Edit Eticket
     *
     * @access  public
     * @return  void
     */
    public function edit()
    {
        // get user
        $eticket = new Eticket_Model($this->id);
        
        // Check if a record exists
        $eticket->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();

        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'E-Ticket/Itinerary Details')),
            'footer'    => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('tinymce/js/tinymce', 'tinymce.min.js'), js_dir('jquery', 'jquery.eticket.js')))),
            'eticket'   => $eticket,
        );
        
        parent::displayTemplate(admin_dir('eticket/form/edit/eticket'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return  void
     */
    private function _validate()
    {
        $this->form_validation->set_rules('eticket_terms', 'Terms and Conditions', 'required|trim');
        $this->form_validation->set_rules('eticket_reminders', 'Reminders', 'required|trim');
        $this->form_validation->set_rules('eticket_details', 'Details', 'required|trim');
        $this->form_validation->set_rules('eticket_full', 'Full E-Ticket', 'required|trim');

        $config['upload_path'] = './assets/img/eticket';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '100';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';

        $this->upload->initialize($config);

        if ($this->uri->rsegment(2) == 'add')
            self::_addInfo();
            
        if ($this->uri->rsegment(2) == 'index')
            self::_editInfo();
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Validate then edit eticket
     *
     * @access  private
     * @return  void
     */
    private function _editInfo()
    {
        $eticket = new Eticket_Model(1);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            // Copy from post
            parent::copyFromPost($eticket, 'id_eticket');

            if($img = self::_uploadImage()) {
                // Eticket image path
                $eticket->img_path = img_dir('eticket').'/'.$img['file_name'];
            }
            else {
                // Set flash data
                $this->session->set_flashdata('note', $data["error"] = $this->upload->display_errors());
                redirect(admin_url($this->classname));
            }

            // Check for successful update
            if ($eticket->update())
            {                                   
                 parent::logThis($eticket->id, 'Successfully updated Eticket');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Eticket');
                $this->session->set_flashdata('id', $eticket->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Eticket');
            }   
            
            redirect(admin_url($this->classname));
        }
    }

    // --------------------------------------------------------------------
    
    /*
     * Upload image
     *
     * @access  private
     * @return  void
     */
    private function _uploadImage()
    {         
        // Do Upload Image
        if (!$this->upload->do_upload("img_path")) {
            // Return false
            return false;

        } else {
            // Return the data of file uploaded
            return $this->upload->data();
        }

        return true;
    }
   
}

/* End of file eticket.php */
/* Location: ./application/modules_core/adminpanel/controllers/eticket/eticket.php */