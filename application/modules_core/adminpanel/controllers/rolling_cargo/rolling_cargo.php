<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Rolling Cargo Class
|--------------------------------------------------------------------------
|
| Modify permissions of rolling cargo
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Rolling_Cargo extends Admin_Core
{
        // ------------------------------------------------------------------------
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
		// Classname
		$this->classname = strtolower(get_class());
                
                parent::__construct();
		
                $this->load->model(admin_dir('rule_set/rule_set_model'));
                $this->load->model(admin_dir('rolling_cargo/rolling_cargo_model'));
                $this->load->model(admin_dir('voyage/voyage_model'));
                
                $this->voyage = new Voyage_Model();
                $this->rule_set = new Rule_Set_Model();
                $this->rolling_cargo = new Rolling_Cargo_Model();
                                
                // vessel ID
		$this->id = $this->uri->rsegment(3);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display rolling cargo list
	 *
	 * @access		public
	 * @return		void
	 */
        public function index()
        {                
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'RORO Rate')),
                                'footer'        => parent::getTemplate(admin_dir('footer'), array('js_files' => array(js_dir('jquery', 'jquery.roro-tools.js')))),
                                'rolling_cargo' => $this->rolling_cargo->displayList(),
                );
                
                parent::displayTemplate(admin_dir('rolling_cargo/rolling_cargo'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * View rolling cargo info
	 *
	 * @access		public
	 * @return		void
	 */
        public function view()
        {
                // get rolling cargo
                $rolling_cargo = new Rolling_Cargo_Model($this->id);
                
                // Check if a record exists
		$rolling_cargo->redirectIfEmpty(admin_url($this->classname));
                                
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'View RORO Rate')),
                                'footer'        => parent::getTemplate(admin_dir('footer')),
                                'voyage'        => $this->voyage->displayList(array('u.enabled' => 1)),
                                'rule_set'      => $this->rule_set->displayList(array('u.enabled' => 1)),
                                'rolling_cargo' => $rolling_cargo,
                );
                
                parent::displayTemplate(admin_dir('rolling_cargo/form/rolling_cargo'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Togglwe rolling cargo status
	 *
	 * @access		public
	 * @return		void
	 */
        public function toggle()
        {
                // get rolling cargo
                $rolling_cargo = new Rolling_Cargo_Model($this->id);
                
                // Check if a record exists
		$rolling_cargo->redirectIfEmpty(admin_url($this->classname));
                
                // Check for successful toggle
                if ($rolling_cargo->toggleStatus())
                {	
                        // Set confirmation message
                        if($rolling_cargo->enabled)
                            $this->session->set_flashdata('confirm', $rolling_cargo->rolling_cargo_code.' has been de-activated!');
                        else
                            $this->session->set_flashdata('confirm', $rolling_cargo->rolling_cargo_code.' has been re-activated!');
                        $this->session->set_flashdata('id', $rolling_cargo->id);
                }
                else
                {
                        // Set confirmation message
                        $this->session->set_flashdata('note', 'Error in updating vessel');
                }
                
                redirect(admin_url($this->classname));
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Delete RORO Rate
	 *
	 * @access		public
	 * @return		void
	 */
        public function delete()
        {
                // get rolling cargo
                $roro = new Rolling_Cargo_Model($this->id);
                
                // Check if a record exists
		$roro->redirectIfEmpty(admin_url($this->classname));
                
                // Check for successful toggle
                if ($roro->countFromROROCheckin() > 0)
                {	
                        // Set confirmation message
                        $this->session->set_flashdata('note', 'Cannot delete '.$roro->rolling_cargo_code.' was already used in the system.');
                }
                else
                {                                                
                        // Set confirmation message
                        if($roro->delete())
                            $this->session->set_flashdata('confirm', "Successfully deleted ".$roro->rolling_cargo_code.'!');
                        else
                            $this->session->set_flashdata('note', "Error deleting RORO rate!");
                }
                
                redirect(admin_url($this->classname));
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Add rolling cargo
	 *
	 * @access		public
	 * @return		void
	 */
        public function add()
        {
                // Form validation
		self::_validate();
                
                // get data
                $data = array(
                                'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Create RORO Rate')),
                                'footer'        => parent::getTemplate(admin_dir('footer')),
                                'voyage'    => $this->voyage->displayList(array('u.enabled' => 1)),
                                'rule_set'  => $this->rule_set->displayList(array('u.enabled' => 1)),
                );
                
                parent::displayTemplate(admin_dir('rolling_cargo/form/add/rolling_cargo'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Edit rolling cargo
	 *
	 * @access		public
	 * @return		void
	 */
        public function edit()
        {
                // get rolling cargo
                $rolling_cargo = new Rolling_Cargo_Model($this->id);
                
                // Check if a record exists
		$rolling_cargo->redirectIfEmpty(admin_url($this->classname));
                
                // Form validation
		self::_validate();
                
                // get data
                $data = array(
                                'header'        => Modules::run(admin_dir('header/call_header'),array('title' => 'Edit RORO Rate')),
                                'footer'        => parent::getTemplate(admin_dir('footer')),
                                'voyage'        => $this->voyage->displayList(array('u.enabled' => 1)),
                                'rule_set'      => $this->rule_set->displayList(array('u.enabled' => 1)),
                                'rolling_cargo' => $rolling_cargo,
                );
                
                parent::displayTemplate(admin_dir('rolling_cargo/form/edit/rolling_cargo'),$data);
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate the form
	 *
	 * @access		private
	 * @return		void
	 */
	private function _validate()
	{
                $this->form_validation->set_rules('rolling_cargo', 'description', 'required|trim|max_length[128]');
                $this->form_validation->set_rules('voyage_id', 'voyage', 'required|trim|intiger');
                $this->form_validation->set_rules('amount', 'amount', 'required|trim|decimal');
                $this->form_validation->set_rules('unit_measurement', 'unit of measurement', 'required|trim|intiger');
                $this->form_validation->set_rules('rule_set_id', 'rule set', 'required|trim|max_length[128]');
		$this->form_validation->set_rules('enabled', 'status', 'required');                    
             
		if ($this->uri->rsegment(2) == 'add')
                        self::_addInfo();
			
			
		if ($this->uri->rsegment(2) == 'edit')
			self::_editInfo();
	}
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then add rolling cargo
	 *
	 * @access		private
	 * @return		void
	 */
	private function _addInfo()
	{
                // check for unique rolling cargo code
                $this->form_validation->set_rules('rolling_cargo_code', 'RORO code', 'required|trim|max_length[32]|is_unique[rolling_cargo.rolling_cargo_code]');
                
                // Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                        $rolling_cargo = new Rolling_Cargo_Model();
                                                
                        $this->tools->setPost('rolling_cargo_code', strtoupper($this->tools->getPost('rolling_cargo_code')));
                        
                        parent::copyFromPost($rolling_cargo, 'id_rolling_cargo');
                        
                        // Check for successful insert
			if ($rolling_cargo->add())
			{

                                parent::logThis($rolling_cargo->id,'Successfully added RORO rate.');
                                
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully added RORO rate.');
				$this->session->set_flashdata('id', $rolling_cargo->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('error', 'Error in saving RORO rate');
			}
                        redirect(admin_url($this->classname));
                }
        }
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Validate then edit rolling cargo
	 *
	 * @access		private
	 * @return		void
	 */
	private function _editInfo()
	{
                $rolling_cargo = new Rolling_Cargo_Model($this->id);
                
                // check if rolling cargo code doesn't change
                if($this->rolling_cargo->getValue('id_rolling_cargo', array('rolling_cargo_code' => $this->tools->getPost('rolling_cargo_code'))) == $this->id)
                        $this->form_validation->set_rules('rolling_cargo_code', 'RORO code', 'required|trim|max_length[32]');
                else
                        $this->form_validation->set_rules('rolling_cargo_code', 'RORO code', 'required|trim|max_length[32]|is_unique[rolling_cargo.rolling_cargo_code]');
                
		// Check if form validation is TRUE
		if ($this->form_validation->run() == TRUE)
		{
                        parent::copyFromPost($rolling_cargo, 'id_rolling_cargo');
			
			// Check for successful update
			if ($rolling_cargo->update())
			{	
                                parent::logThis($rolling_cargo->id,'Successfully updated RORO rate.');
				// Set confirmation message
				$this->session->set_flashdata('confirm', 'Successfully updated RORO rate.');
				$this->session->set_flashdata('id', $rolling_cargo->id);
			}
			else
			{
				// Set confirmation message
				$this->session->set_flashdata('note', 'Error in updating RORO rate');
			}
			
			redirect(admin_url($this->classname));
		}
	}
        
}

/* End of file rolling_cargo.php */
/* Location: ./application/modules_core/adminpanel/controllers/rolling_cargo/rolling_cargo.php */