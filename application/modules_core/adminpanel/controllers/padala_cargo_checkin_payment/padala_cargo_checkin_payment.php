<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Padala Cargo Check In Payment Class
|--------------------------------------------------------------------------
|
| Padala Cargo Check In Payment
|
| @category Controller
| @author       Kenneth Bahia
*/
class Padala_Cargo_Checkin_Payment extends Admin_Core
{
    var $submitted_cargos = array();
        // ------------------------------------------------------------------------
    
    /**
     * Constructor
     *
     * Called automatically
     * Inherits method from the parent class
     */
    public function __construct()
    {
        // Classname
        $this->classname = strtolower(get_class());
        
        parent::__construct();

        $this->load->model(admin_dir('padala_cargo/Padala_Cargo_Model'));
        $this->load->model(admin_dir('padala_cargo_checkin/Padala_Cargo_Checkin_Model'));
        $this->load->model(admin_dir('voyage_management/Voyage_Management_Model'));
        $this->load->model(admin_dir('subvoyage_management/Subvoyage_Management_Model'));
        $this->load->model(admin_dir('port/Port_Model'));
        $this->load->model(admin_dir('booking/Booking_Model'));
        
        $this->load->model(admin_dir('cargo_description/Cargo_Description_Model'));
        $this->load->model(admin_dir('transaction/Transaction_Model'));

        $this->padala_cargo = new Padala_Cargo_Model();
        $this->padala_cargo_checkin = new Padala_Cargo_Checkin_Model();
        $this->voyage_management = new Voyage_Management_Model();
        $this->subvoyage_management = new Subvoyage_Management_Model();
        $this->port = new Port_Model();
        $this->booking = new Booking_Model();
        $this->transaction = new Transaction_Model();
                        
        // Padala Cargo Check In id
        $this->id = $this->uri->rsegment(3);
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Display Padala Cargo Check In Master List
     *
     * @access  public
     * @return      void
     */
    public function index()
    {                
        // Get all padala cargo checkins
        $padala_cargo_checkin = $this->padala_cargo_checkin->displaylist();
        
        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header'),array('title' => 'Padala Cargo Check-In Payment')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.padala-cargo-checkin-payment.js')))),
            'padala_cargo'   => $this->padala_cargo->displayList(),
            'port_list'   => $this->port->displayList(),
            'submitted_cargos' => $this->submitted_cargos,
            'subvoyage_management_id' => $this->tools->getPost("subvoyage_management_id")
        );

        parent::displayTemplate(admin_dir('padala_cargo_checkin_payment/padala_cargo_checkin_payment'), $data);
        //parent::displayTemplate(admin_dir('maintenance'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * View Padala Cargo Check in Information
     *
     * @access  public
     * @return      void
     */
    public function view()
    {
        $padala_cargo_checkin = new Padala_Cargo_Checkin_Model();
        
        $list = array();
        $departure_date = "";
        $voyage_id = 0;

        if (!empty($this->tools->getPost())) {
            $departure_date = $this->tools->getPost("departure_date");
            $voyage_id = $this->tools->getPost("voyage_id");
            
            $where = array(
                            "departure_date" => $departure_date, 
                            "voyage_id" => $voyage_id, 
                            "t.checkin_type_id" => 3
                        );

            $list = $loose_cargo_checkin->displayList($where);
        }
        //'header'    => Modules::run(admin_dir('header/call_header', array("css_files" => array(css_dir('admin', 'print.css'))))),
        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer'), array("js_files" => array(js_dir('jquery', 'jquery.loose-cargo-checkin.js')))),
            'loose_cargo_checkin'        => $list,
            'voyage_list'   => array(),
            'voyage_id' => $voyage_id,
            'departure_date' => $departure_date
        );

        //var_dump(css_dir("admin","print.css"));

        parent::displayTemplate(admin_dir('padala_cargo_checkin/form/padala_cargo_checkin'),$data);
    }

    // --------------------------------------------------------------------
    
    /*
     * Toggle user status
     *
     * @access  public
     * @return      void
     */
    public function toggle()
    {
        // Get outlet
        $loose_cargo_checkin = new loose_Cargo_Checkin_Model($this->id);
        
        // Check if a record exists
        $loose_cargo_checkin->redirectIfEmpty(admin_url($this->classname));
        
        // Check for successful toggle
        if ($loose_cargo_checkin->toggleStatus())
        {   
            // Set confirmation message
            $this->session->set_flashdata('confirm', 'Successful in updating loose_cargo_checkin.');
            $this->session->set_flashdata('id', $loose_cargo_checkin->id);
        }
        else
        {
            // Set confirmation message
            $this->session->set_flashdata('note', 'Error in updating loose_cargo_checkin.');
        }
        
        redirect(admin_url($this->classname));
    }
        
    // --------------------------------------------------------------------
    
    /*
     * Add Outlet
     *
     * @access  public
     * @return      void
     */
    public function add()
    {
        // Form validation
        self::_validate();

        // Initialize data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'voyage_list'   => $this->voyage_management->displayList()
        );
            
        parent::displayTemplate(admin_dir('loose_cargo_checkin/form/add/loose_cargo_checkin'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Edit Outlet
     *
     * @access  public
     * @return      void
     */
    public function edit()
    {
        // get user
        $loose_cargo_checkin = new loose_Cargo_Checkin_Model($this->id);
        
        // Check if a record exists
        $loose_cargo_checkin->redirectIfEmpty(admin_url($this->classname));
        
        // Form validation
        self::_validate();
        
        // get data
        $data = array(
            'header'    => Modules::run(admin_dir('header/call_header')),
            'footer'        => parent::getTemplate(admin_dir('footer')),
            'loose_cargo_checkin'        => $loose_cargo_checkin,
            'voyage_list'   => $this->voyage->displayList()
        );
        
        parent::displayTemplate(admin_dir('loose_cargo_checkin/form/edit/loose_cargo_checkin'),$data);
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate the form
     *
     * @access  private
     * @return      void
     */
    private function _validate()
    {   
        $this->form_validation->set_rules('origin_id', 'origin', 'required|numeric');
        $this->form_validation->set_rules('destination_id', 'destination', 'required|numeric');
        $this->form_validation->set_rules('subvoyage_management_id', 'voyage', 'required|numeric');
        $this->form_validation->set_rules('reference_no', 'reference number', 'required');
        $this->form_validation->set_rules('total_amount', 'total amount', 'required');

        $total_amount = (float)$this->tools->getPost("total_amount");
        $amount_tendered = (float)$this->tools->getPost("amount_tendered");

        $this->form_validation->set_rules('amount_tendered', 'amount tendered', 'required|decimal|check_amount['.$total_amount.']');

        if ($this->uri->rsegment(2) == 'index')
            self::_addInfo();
            
        if ($this->uri->rsegment(2) == 'edit')
            self::_editInfo();
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then add Padala Cargo Checkin
     *
     * @return      void
     */
    private function _addInfo()
    {
           
        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
            $transaction = new Transaction_Model();
            $this->tools->setPost("transaction_ref", $this->tools->getPost("reference_no"));
            parent::copyFromPost($transaction, "id_transcation");
            $transaction->checkin_type_id = 3; //padala cargo checkin

            if ($transaction->add()){
                parent::logThis($transaction->id, 'Successfully saved padala cargo payment');

                 // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully saved padala cargo payment');
            } else {
                // Set confirmation message
                $this->session->set_flashdata('error', 'Error in saving padala cargo payment');
            }

            redirect(admin_url($this->classname));
                        
        }
    }
        
        // --------------------------------------------------------------------
    
    /*
     * Validate then edit outlet
     *
     * @access  private
     * @return      void
     */
    private function _editInfo()
    {
        $loose_cargo_checkin = new loose_Cargo_Checkin_Model($this->id);

        // Check if form validation is TRUE
        if ($this->form_validation->run() == TRUE)
        {
                    
            parent::copyFromPost($loose_cargo_checkin, 'id_loose_cargo');
            
            // Check for successful update
            if ($loose_cargo_checkin->update())
            {                                   
                 parent::logThis($loose_cargo_checkin->id, 'Successfully updated Loose Cargo');
                                        
                // Set confirmation message
                $this->session->set_flashdata('confirm', 'Successfully updated Loose Cargo');
                $this->session->set_flashdata('id', $loose_cargo_checkin->id);
            }
            else
            {
                // Set confirmation message
                $this->session->set_flashdata('note', 'Error in updating Loose Cargo');
            }
            
            redirect(admin_url($this->classname));
        }
    }

        // --------------------------------------------------------------------
    
    /*
     * View Outet Information
     *
     * @access  public
     * @return      void
     */
    public function searchReferenceNo()
    {
        if (!empty($this->tools->getPost())) {
            $subvoyage_management_id = intval($this->tools->getPost('subvoyage_management_id'));
            $reference_no = $this->tools->getPost('reference_no');

            $post_data = array(
                                "subvoyage_management_id" => $subvoyage_management_id,
                                "reference_no"            => $reference_no
                            );

            $padala_cargo_checkin = $this->padala_cargo_checkin->displayList($post_data);

            $result = FALSE;
            $checkin_details = "No records found";
            
            if (!empty($padala_cargo_checkin)) {
                $result = TRUE;
                $checkin_details = $padala_cargo_checkin;
            }

            $transaction = $this->transaction->displayList(array("transaction_ref" => $reference_no));

            if (!empty($transaction)) {
                $result = FALSE;
                $checkin_details = "Check-In with Reference no. <b>". $reference_no ."</b> is paid already.";
            }

            $response = array("result" => $result, "checkin_details" => $checkin_details);

            print json_encode($response);
        }
    }
}

/* End of file padala_cargo_checkin.php */
/* Location: ./application/modules_core/adminpanel/controllers/padala_cargo_checkin/padala_cargo_checkin.php */