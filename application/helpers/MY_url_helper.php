<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/*
 * CodeIgniter URL Helpers Extension
 *
 * @category	Helpers
 * @author	Baladeva Juganas	
 */

define('_INCL_DIR_', config_item('base_url').'assets/');
define('_THEME_DIR_', _INCL_DIR_.'themes');
define('_MDL_DIR_', _INCL_DIR_.'module');
define('_IMG_DIR_', _INCL_DIR_.'img');
define('_CSS_DIR_', _INCL_DIR_.'css');
define('_JS_DIR_', _INCL_DIR_.'js');
define('_FONT_DIR_', _INCL_DIR_.'font');
define('_FL_DIR_', _INCL_DIR_.'flash/');
define('_UPLD_DIR_', config_item('base_url').'uploads/');
define('_DB_DIR_', config_item('base_url').'database');
define('_UP_IMG_DIR_', _UPLD_DIR_.'images/');
define('_UP_DOCS_DIR_', _UPLD_DIR_.'documents/');

// ------------------------------------------------------------------------

/*
 * Includes directory
 *
 * Get URL of the external files needed for the site
 * (CSS, Javascript, Flash, etc.)
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('includes'))
{
	function includes($subdirectory = '', $file = '')
	{
		$link = _INCL_DIR_;
		
		if( ! empty($subdirectory))
		{
			$link .= $subdirectory;
			
			if( ! empty($file))
				$link .= '/' . $file;
		}
			
		return $link;
	}
}

// ------------------------------------------------------------------------

/*
 * Themes URL
 *
 * Get URL of the external files needed for the site
 * (CSS, Javascript, Flash, etc.)
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('theme_url'))
{
	function theme_url($theme = 'default', $subdirectory = '', $file = '')
	{
		$link = _THEME_DIR_;
		
		if( ! empty($theme))
		{
			$link .= '/' . $theme;
			
			if( ! empty($subdirectory))
				$link .= '/' . $subdirectory;
				
			if( ! empty($file))
				$link .= '/' . $file;
		}
			
		return $link;
	}
}

// ------------------------------------------------------------------------

/*
 * Themes directory
 *
 * Get URL of the external files needed for the site
 * (CSS, Javascript, Flash, etc.)
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('theme_dir'))
{
	function theme_dir($theme = 'default', $subdirectory = '', $file = '')
	{
		$link = 'themes';
		
		if( ! empty($theme))
		{
			$link .= '/' . $theme;
			
			if( ! empty($subdirectory))
				$link .= '/' . $subdirectory;
				
			if( ! empty($file))
				$link .= '/' . $file;
		}
			
		return $link;
	}
}
// ------------------------------------------------------------------------

/*
 * Images directory
 *
 * Get the images from under the includes directory
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('img_dir'))
{
	function img_dir($subdirectory = '', $image = '')
	{
		$link = _IMG_DIR_;
		
		if( ! empty($subdirectory))
			$link .= '/' . $subdirectory;
		
		if( ! empty($image))
			$link .= '/' . $image;
		
		return $link;
	}
}

// ------------------------------------------------------------------------

/*
 * CSS directory
 *
 * Get the CSS from under the includes directory
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('css_dir'))
{
	function css_dir($subdirectory = '', $css = '')
	{
		$link = _CSS_DIR_;
		
		if( ! empty($subdirectory))
			$link .= '/' . $subdirectory;
		
		if( ! empty($css))
			$link .= '/' . $css;
		
		return $link;
	}
}

// ------------------------------------------------------------------------

/*
 * JS directory
 *
 * Get the CSS from under the includes directory
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('js_dir'))
{
	function js_dir($subdirectory = '', $js = '')
	{
		$link = _JS_DIR_;
		
		if( ! empty($subdirectory))
			$link .= '/' . $subdirectory;
		
		if( ! empty($js))
			$link .= '/' . $js;
		
		return $link;
	}
}

// ------------------------------------------------------------------------

/*
 * Font directory
 *
 * Get the font from under the assets directory
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('font_dir'))
{
	function font_dir($subdirectory = '', $font = '')
	{
		$link = _FONT_DIR_;
		
		if( ! empty($subdirectory))
			$link .= '/' . $subdirectory;
		
		if( ! empty($font))
			$link .= '/' . $font;
		
		return $link;
	}
}

// ------------------------------------------------------------------------

/*
 * Database directory
 *
 * Get the font from under the assets directory
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('database_dir'))
{
	function database_dir($subdirectory = '', $font = '')
	{
		$link = _DB_DIR_;
		
		if( ! empty($subdirectory))
			$link .= '/' . $subdirectory;
		
		if( ! empty($font))
			$link .= '/' . $font;
		
		return $link;
	}
}

// ------------------------------------------------------------------------

/*
 * Module Directory
 *
 * Get the module directory
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('module_dir'))
{
	function module_dir($module = '', $file = '')
	{
		$link = _MDL_DIR_;
		
		if( ! empty($module))
			$link .= '/' . $module;
		
		if( ! empty($file))
			$link .= '/' . $file;
			
		return $link; 
	}
}

// ------------------------------------------------------------------------

/*
 * Module URL
 *
 * Get the module url
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('module_url'))
{
	function module_url($subdirectory = '')
	{
		$CI =& get_instance();
		$link = base_url() . 'modules';
		
		if( ! empty($subdirectory))
			$link .= '/' . $subdirectory;
			
		return $link; 
	}
}

// ------------------------------------------------------------------------

/*
 * Admin Directory
 *
 * Get the admin directory defined on your config file.
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('admin_dir'))
{
	function admin_dir($subdirectory = '')
	{
		$CI =& get_instance();
		
		$link = $CI->config->item('admin_dir');
		
		if( ! empty($subdirectory))
			$link .= '/' . $subdirectory;
			
		return $link; 
	}
}

// ------------------------------------------------------------------------

/*
 * Admin URL
 *
 * Generate link for admin pages
 *
 * @access	public
 * @param	string
 * @param	string
 * @param	mixed
 * @param 	mixed
 * @return	string
 */
if ( ! function_exists('admin_url'))
{
	function admin_url($classname = '', $method = '', $identifier = '', $segment = '')
	{
		$CI =& get_instance();
		$link = base_url().$CI->config->item('admin_dir');
		
		if ( ! empty($classname))
		{
			$link .= '/'.$classname;
			
			if ( ! empty($method))
			{
				$link .= '/'.$method;
				
				if ( ! empty($identifier))
				{
					$link .= '/'.$identifier;
					
					if ( ! empty($segment))
					{
						$link .= '/'.$segment;
					}
				}
			}
		}
		
		return $link;
	}
}

/* End of file MY_url_helper.php */
/* Location: ./application/helpers/MY_url_helper.php */
