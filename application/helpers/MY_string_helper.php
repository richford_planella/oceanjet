<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extension of the String Helper
 *
 * Features:
 * - Extends some base PHP functions
 *
 *
 * @package	String Helper
 * @subpackage	Helpers
 * @category	Helpers
 * @author	Baladeva Juganas
 * @version 	0.1
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
if ( ! function_exists('str_lowercase'))
{
    function str_lowercase($string = '', $remove_space = FALSE)
    {
        if($remove_space)
			return preg_replace("/\s+/", "_", strtolower($string));
			
		return strtolower($string);
    }
}

if ( ! function_exists('str_uppercase'))
{
    function str_uppercase($string = '', $remove_space = FALSE)
    {
        if($remove_space)
			return preg_replace("/\s+/", "_", strtoupper($string));
			
		return strtoupper($string);
    }
}

if ( ! function_exists('convert_to_time'))
{
    function convert_to_time($string = '', $delimeter = ':')
    {        
        // reverse string
        $revString = strrev($string);
        
        // convert string value to array
        $arrString = str_split($revString);
        
        // return default
        if($string == '0' OR $string == '')
            return '00:00';
        
        $retString = '';
        
        // loop string
        foreach($arrString as $key =>$value)
        {
            if($key == 2)
                $retString = $retString.$delimeter.$value;
            else
                $retString = $retString.$value;
        }
        
        if(count($arrString) == 3)
            $retString = $retString.'0';
        elseif(count($arrString) == 2)
            $retString = $retString.$delimeter.'00';
        elseif(count($arrString) == 1)
            $retString = $retString.'0'.$delimeter.'00';
        
        
        return strrev($retString);
    }
}


/* End of file MY_string_helper.php */
/* Location: ./application/helpers/MY_string_helper.php */
