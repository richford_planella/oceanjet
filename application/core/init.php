<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$currentDir = dirname(__FILE__);

define('__ROOT_DIR__',  realpath($currentDir.'/../..'));
define('_MODULE_DIR_', APPPATH.'modules_core/modules/controllers/');
define('_ALL_THEME_DIR_', APPPATH.'modules_core/customer/views/themes/');

$my_session = new MY_Session();

$this->load->vars(
	array(
		'token' => $my_session->get('admin', 'token'),
		)
	);

/* End of file init.php */
/* Location: ./application/core/init.php */