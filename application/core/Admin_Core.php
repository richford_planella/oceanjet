<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Admininstrator Core Class
|--------------------------------------------------------------------------
|
| Handles the administrator panel
|
| @category		Controller
| @author		Baladeva Juganas
*/
class Admin_Core extends MY_Controller
{
        // string classname that extends this class
	public $classname = '';
	        
        public $breadcrumb = array();
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct()
	{
                parent::__construct();
                
                $this->load->library('session');
                
                // Check for admin login URL
		if ($this->uri->rsegment(1) != 'login' AND $this->uri->rsegment(1) != 'forgot')
		{
			// if page is not the admin login URL
			if( ! $this->session->get('admin', 'admin_login') OR $this->session->get('admin', 'encryption_key') != $this->config->item('encryption_key'))
			{
				// Set redirect session
				$this->session->set_userdata('redirect', current_url());
				redirect(admin_url('login'));
			}
		}
                
                // check if password newly created
                if($this->uri->rsegment(1) != 'change_password')
                {
                        // check sessiecho on is_new
                        if($this->session->get('admin', 'is_new'))
                        {
                                // force user to change password
                                redirect(admin_url('change_password'));
                        }        
                }
                
                $this->load->model(admin_dir('user/profile_model'));
                $this->load->model(admin_dir('administrator_access/administrator_access_model'));
                $this->load->model(admin_dir('administrator_page/administrator_page_model'));
                
                $this->user_profile = new Profile_Model();
                $this->admin_access = new Administrator_Access_Model();
                $this->admin_page = new Administrator_Page_Model();
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Determine administrator permissions on pages
	 *
	 * All admin pages goes to this function first
	 * Overides all function call
	 *
	 * @access		public
	 * @return		void
	 */
	public function _remap()
	{
		// Get the page id of the current page
		$page_id = $this->admin_page->getPageID($this->classname);
		
		// Get the profile id of the logged in administrator
		$this->profile_id = $this->session->get('admin','administrator_profile_id');
		
		// Identify method by segment
		$method = $this->uri->rsegment(2);
                         
		switch ($method)
		{
                        // determine view permission
                        case "index":
                                $access = $this->admin_access->getAccess('access', $page_id, $this->profile_id);
				break;
			// determine view permission
			case "generate":
			case "view":
			case "search":
			case "result":
				$access = $this->admin_access->getAccess('view', $page_id, $this->profile_id);
				break;
			
			// determine add permission
			case "add":
				$access = $this->admin_access->getAccess('add', $page_id, $this->profile_id);
				break;
			
			// determine edit permission
			case "edit":
                        case "cancel":
			case "position":
			case "enable":
			case "install":
			case "uninstall":
				$access = $this->admin_access->getAccess('edit', $page_id, $this->profile_id);
				break;
			
			// determine delete permission
                        case "toggle":
                        case "activate":
                        case "reactivate":
			case "delete":
				$access = $this->admin_access->getAccess('delete', $page_id, $this->profile_id);
				break;
                            
                        // determine approve permission
			case "approve":
				$access = $this->admin_access->getAccess('approve', $page_id, $this->profile_id);
				break;
                            
                        // determine verify permission
                        case "revert":
			case "verify":
				$access = $this->admin_access->getAccess('verify', $page_id, $this->profile_id);
				break;
                        
                        // determine print permission
                        case "prints":
			case "print":
				$access = $this->admin_access->getAccess('print', $page_id, $this->profile_id);
				break;
                        
                        // determine request permission
                        case "request":
				$access = $this->admin_access->getAccess('request', $page_id, $this->profile_id);
				break;
                        
                        // determine close permission
			case "close":
				$access = $this->admin_access->getAccess('close', $page_id, $this->profile_id);
				break;
                            
                        // determine received permission
			case "returns":
                        case "received":
				$access = $this->admin_access->getAccess('received', $page_id, $this->profile_id);
				break;
                            
                        // determine allocate permission
			case "allocate":
				$access = $this->admin_access->getAccess('allocate', $page_id, $this->profile_id);
				break;
                            
			// allow access by default
			default:
				$access = 1;
		}
		
		// Grant permission on the admin homepage for all administrator
		if($this->classname == admin_dir() OR $this->uri->rsegment(1) == 'login' OR $this->uri->rsegment(1) == 'forgot' OR $this->uri->rsegment(1) == 'profile' OR $this->uri->rsegment(1) == 'change_password' )
			$access = 1;

		if (isset($access) && $access)
		{
			if (is_callable($this->classname, $method) && method_exists($this->classname, $method))
				$this->$method();
			else
				self::page_error();
		}
		else
		{
			if (is_callable($this->classname, $method) && method_exists($this->classname, $method))
				self::deny($method);
			else
				self::page_error();
		}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Deny access to the page
	 *
	 * @access		private
	 * @return		void
	 */
	private function deny($method_name = '')
	{
		switch ($method_name)
		{
                        // determine view permission
                        case "index":
                                $function = 'access';
				break;
			// determine view permission
			case "view":
			case "search":
			case "result":
				$function = 'view';
				break;
			
			// determine add permission
			case "add":
				$function = 'add';
				break;
			
			// determine edit permission
			case "edit":
                        case "cancel":
			case "position":
			case "enable":
			case "install":
			case "uninstall":
				$function = 'edit';
				break;
			
			// determine delete permission
                        case "toggle":
                        case "activate":
                        case "reactivate":
			case "delete":
				$function = 'delete';
				break;
                        // determine approve permission
			case "approve":
				$function = 'approve';
				break;
                            
                        // determine verify permission
                        case "revert":
			case "verify":
				$function = 'verify';
				break;
                        
                        // determine print permission
			case "prints":
                        case "export":
                        case "print":
				$function = 'print';
				break;
                        
                        // determine request permission
                        case "request":
				$function = 'request';
				break;
                        
                        // determine close permission
			case "close":
				$function = 'close';
				break;
                            
                        // determine received permission
			case "received":
				$function = 'receive';
				break;
                            
                        // determine allocate permission
			case "allocate":
				$function = 'allocate';
				break;
		}
		
		$data = array(	'header' => Modules::run(admin_dir('header/call_header')),
                                'footer' => parent::getTemplate(admin_dir('footer')),
                                'message' => "You don't have permission to $function here."
                        );
					
		$this->load->view(admin_dir('deny'), $data);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Catch the error if method does not exists
	 *
	 * @access		private
	 * @return		void
	 */
	private function page_error()
	{
		$data = array(	'header' => Modules::run(admin_dir('header/call_header')),
                                'footer' => parent::getTemplate(admin_dir('footer')),
                                'message' => "Page does not exists."
                        );
					
		$this->load->view(admin_dir('deny'), $data);
	}
}