<?php if ( !  defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Model Class
|--------------------------------------------------------------------------
|
| Extended core for Model classes
|
| @category		Model
| @author		Baladeva Juganas
*/
class MY_Model extends CI_Model 
{
        /* int object id */
	public $id;

	/* string table name */
	protected $table = '';
        
        // ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
	public function __construct($id = NULL)
	{
		parent::__construct();

		if ( ! empty($id))
		{
			// WHERE
			$this->db->where(array($this->identifier => $id));

			$query = $this->db->get($this->table);
			$result = $query->row();
		
			$this->id = $id;
		}
		
		if( ! empty($result))
		{
			foreach ($result AS $key => $value)
				if (key_exists($key, $this))
					$this->{$key} = $value;
		}
	}
        
        // --------------------------------------------------------------------
	/*
	 * Executes query
	 *
	 * @access		public
	 * @param 		string
	 * @return		object
	 */
	public function get($table = '')
	{
		if ( ! empty($table))
		{
			$query = $this->db->get($table);
			$result = $query->result();
			
			return $result;
		}
	}

	// --------------------------------------------------------------------
	/*
	 * Executes query
	 *
	 * @access		public
	 * @param 		string
	 * @return		object
	 */
	public function getRow($table = '')
	{
		if ( ! empty($table))
		{
			$query = $this->db->get($table);
			$result = $query->row();
			
			return $result;
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * Executes INSERT
	 * Add records
	 *
	 * @access		public
	 * @param 		array
	 * @return		int
	 */
	public function add($fields = array())
	{
		// auto fill-in date added
		if (key_exists('date_added', $this))
			$this->date_added = date('Y-m-d H:i:s');

		// auto fill-in date updated
		if (key_exists('date_update', $this))
			$this->date_update = date('Y-m-d H:i:s');

		// get fields
		if ( ! empty($fields))
			$this->db->insert($this->table, $fields);
		else
			$this->db->insert($this->table, $this->getFields());

		$this->id = $this->db->insert_id();

		return $this->id;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Executes UPDATE
	 * Updates records
	 *
	 * @access		public
	 * @param 		array
	 * @return		bool
	 */
	public function update($where = array(), $fields = array())
	{
		// auto fill-in date updated
		if (key_exists('date_update', $this))
			$this->date_update = date('Y-m-d H:i:s');
		
		// WHERE
		if ( ! empty($where))
			$this->db->where($where);
		else
			$this->db->where(array($this->identifier => $this->id));

		// update fields
		if ( ! empty($fields))
			$this->db->update($this->table, $fields);
		else
			$this->db->update($this->table, $this->getFields());

		return TRUE;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Executes DELETE
	 * Delete records
	 *
	 * @access		public
	 * @param 		array
	 * @return		bool
	 */
	public function delete($where = array())
	{
		// WHERE
		if ( ! empty($where))
			$this->db->where($where);
		else
			$this->db->where(array($this->identifier => $this->id));
		
		// DELETE
		$this->db->delete($this->table);
		
		return TRUE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Executes TRUNCATE
	 * Delete records
	 *
	 * @access		public
	 * @param 		array
	 * @return		bool
	 */
	public function truncate()
	{
		// TRUNCATE
		$this->db->truncate($this->table); 
		
		return TRUE;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Redirect if object is empty
	 *
	 * @access		public
	 * @param		string
	 * @return		void
	 */
	public function redirectIfEmpty($redirect = '')
	{
		if ( ! $this->{$this->identifier} || empty($this->{$this->identifier}) || (isset($this->is_deleted) && $this->is_deleted == 1))
		{
			$this->session->set_flashdata('note', 'There is no value attached to that ID.');
			redirect($redirect);
		}
	}

	// --------------------------------------------------------------------
	
	/*
	 * Get value of a specific column
	 *
	 * @access		public
	 * @param		string
	 * @param		mixed
	 * @return		void
	 */
	public function getValue($fieldname = '', $where = array())
	{
		// SELECT
		$this->db->select($fieldname);

		// WHERE
		$this->db->where($where);

		$query = $this->db->get($this->table);
		$row = $query->row();
		
		if ($row)
			return $row->{$fieldname};

		return FALSE;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Toggle record status
	 *
	 * @access		public
	 * @return		void
	 */
	public function toggleStatus()
	{
		// set WHERE based on table indentifier
		$this->db->where(array($this->identifier => $this->id));

		// SET values
		$this->db->set('enabled', '! enabled', FALSE);

		// UPDATE fields
		$this->db->update($this->table);
                
                return TRUE;
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Toggle record status
	 *
	 * @access		public
	 * @return		void
	 */
	public function toggleLocked()
	{
		// set WHERE based on table indentifier
		$this->db->where(array($this->identifier => $this->id));

		// SET values
		$this->db->set('is_locked', '! is_locked', FALSE);

		// UPDATE fields
		$this->db->update($this->table);
                
                return TRUE;
	}

	// --------------------------------------------------------------------
	
	/*
	 * Change status to locked
	 *
	 * @access		public
	 * @return		void
	 */
	public function changeToDeleted()
	{
		// set WHERE based on table indentifier
		$this->db->where(array($this->identifier => $this->id));

		// SET values
		$this->db->set('is_deleted', 1);

		// UPDATE fields
		$this->db->update($this->table);
	}

	// --------------------------------------------------------------------
	
	/*
	 * Check if column is existing
	 *
	 * @access		public
	 * @return		void
	 */
	public function checkColumn($column = '', $table = '')
	{
		// WHERE
		$this->db->where(array('TABLE_SCHEMA' => $this->db->database));
		$this->db->where(array('TABLE_NAME' => $table));
		$this->db->where(array('COLUMN_NAME' => $column));

		$query = $this->db->get('information_schema.COLUMNS');
		$result = $query->result();

		return $result;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Function execute SELECT with LIKE in SQL
	 * @access		public
	 * @param		compare: string
	 *				where: array
	 *				order_by: array
	 *				limit: int
	 *				offset: int
	 * @redirect	questionnaire main page
	 * @return		NULL
	 * 
	 */
	function searchBy($compare = 'AND', $where = array(), $order_by = array(), $wildcard = 'after', $limit = '', $offset = '')
	{
		/* check for WHERE statement */
		if ( ! empty($where))
		{
			if($compare == 'AND')
				$this->db->like($where, '', $wildcard);
				
			if($compare == 'OR')
				$this->db->or_like($where);
		}
			
		/* check for ORDER ($field) BY ($direction) statement */
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
		
		/* for limit and offset (useful for pagination) */
		if ($limit)
			$this->db->limit($limit, $offset);
		
		$query = $this->db->get($this->table);
		$result = $query->result();
		
		return $result;
	}
	
	function findMin($field = '', $compare = 'AND', $where = array())
	{
		$this->db->select_min($field);
		
		// WHERE
		if($compare == 'AND')
			self::andWhere($where);
			
		if($compare == 'OR')
			self::orWhere($where);
		
		$query = $this->db->get($this->table);
		$row = $query->row();
		
		if ( ! $row)
			return FALSE;
		
		return $row->$field;
	}
	
	function findMax($field = '', $compare = 'AND', $where = array())
	{
		$this->db->select_max($field);
		
		// WHERE
		if($compare == 'AND')
			self::andWhere($where);
			
		if($compare == 'OR')
			self::orWhere($where);
		
		$query = $this->db->get($this->table);
		$row = $query->row();
		
		if ( ! $row)
			return FALSE;
		
		return $row->$field;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Function to return the value of a single fieldname
	 *
	 * @access		public
	 * @param		string
	 *				string
	 *				array / string
	 * @return		int / string / bool
	 */
	 
	function findFieldValue($fieldname = '', $compare = 'AND', $where = array(), $order_by = array(), $limit = '', $offset = '')
	{
		// WHERE
		if($compare == 'AND')
			self::andWhere($where);
			
		if($compare == 'OR')
			self::orWhere($where);
		
		$this->db->select($fieldname);
		
		// ORDER BY
		self::orderBy($order_by);
		
		// LIMIT OFFSET
		self::limitBy($limit, $offset);
		
		$query = $this->db->get($this->table);
		$row = $query->row();
		if ($row)
			return $row->$fieldname;
			
		return NULL;
	}
	
	// --------------------------------------------------------------------

	/*
	 * Count records based on parameters
	 *
	 * @access		public
	 * @param 		string
	 * @param 		mixed
	 * @return		int
	 */
	public function countBy($where = array())
	{
		// WHERE
		$this->db->where($where);
		
		return $this->db->count_all_results($this->table);
	}
	
	function getSum($fieldname = '', $alias = '', $compare = 'AND', $where = array())
	{
		// WHERE
		if($compare == 'AND')
			self::andWhere($where);
			
		if($compare == 'OR')
			self::orWhere($where);
		
		$this->db->select_sum($fieldname);
		$query = $this->db->get($this->table);
		
		$row = $query->row();
		
		if ($row)
			return $row->$fieldname;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Executes SELECT
	 *
	 * @access		public
	 * @param		string
	 * @return		void
	 */
	public function selectBy($select = '')
	{
		if ( ! empty($select))
			$this->db->select($select);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Executes JOIN
	 *
	 * @access		public
	 * @param		string
	 * @return		void
	 */
	public function joinBy($table = '', $condition = '', $direction = '')
	{
		if ( ! empty($table) && ! empty($condition) && ! empty($direction))
			$this->db->join($table, $condition, $direction);
	}
	 
	// --------------------------------------------------------------------
	
	/*
	 * Executes WHERE with AND
	 *
	 * @access		public
	 * @param		mixed
	 * @return		void
	 */
	public function andWhere($where = '')
	{
		if ( ! empty($where))
			$this->db->where($where);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Executes WHERE with OR
	 *
	 * @access		public
	 * @param		mixed
	 * @return		void
	 */
	public function orWhere($where = '')
	{
		if ( ! empty($where))
			$this->db->or_where($where);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Order (field) By (direction)
	 *
	 * @access		public
	 * @param		array
	 * @return		object
	 */
	public function orderBy($order_by = array())
	{
		if ( ! empty($order_by))
		{
			foreach($order_by as $field => $direction)
				$this->db->order_by($field, $direction);
		}
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Limit results by offset
	 *
	 * @access		public
	 * @param		int
	 * @param		int
	 * @return		object
	 */
	public function limitBy($limit = '', $offset = '')
	{
		if ($limit)
			$this->db->limit($limit, $offset);
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Executes query
	 *
	 * @access		public
	 * @param		array
	 * @return		object
	 */
	public function getObject()
	{
		$query = $this->db->get($this->table);
		$result = $query->result();
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Executes query with one result
	 *
	 * @access		public
	 * @param		array
	 * @return		object
	 */
	public function getSingleObject()
	{
		$query = $this->db->get($this->table);
		$result = $query->row();
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Executes query
	 *
	 * @access		public
	 * @param		array
	 * @return		array
	 */
	public function getArray()
	{
		$query = $this->db->get($this->table);

		$result = $query->result_array();
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	/*
	 * Executes query, returns with the value of the field
	 *
	 * @access		public
	 * @param		string
	 * @return		mixed
	 */
	public function getFieldValue($fieldname = '')
	{
		$query = $this->db->get($this->table);
		$row = $query->row();
		
		if ($row)
			return $row->$fieldname;
			
		return NULL;
	}
}