<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Controller extension class
|--------------------------------------------------------------------------
|
| Extends the CI Controller class
|
| @category		Controller
| @author		Baladeva Juganas
*/
class MY_Controller extends MX_Controller
{
        public $_CI;
	// ------------------------------------------------------------------------
	
	/*
	 * Constructor
	 *
	 * Called automatically
	 * Inherits method from the parent class
	 */
        public function __construct()
        {
                parent::__construct();
                
                $this->load->model(admin_dir('configuration/configuration_model'));
                
                $this->configuration = new Configuration_Model();
                
                // set library Tools
                $this->tools = new Tools();
                $this->misc = new Misc();
                $this->session = new MY_Session();
                
                date_default_timezone_set($this->configuration->getValue('value', array('configuration' => 'DEFAULT_TIME_ZONE')));
                
                // Include initial variables
		include_once(dirname(__FILE__).'/init.php');
                
                // Initialize the form validation
		$this->load->library('form_validation');
                $this->load->library('email');
		$this->form_validation->_CI =& $this;
                
                //$this->output->enable_profiler(TRUE);
        }
    
        // --------------------------------------------------------------------
	
	/*
	 * Get template file
	 *
	 * @access		public
	 * @param		string
	 * @param		array
	 * @return		string
	 */
	public function getTemplate($dir = '', $data = array())
	{
		return $this->load->view($dir, $data, TRUE);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Display template file
	 *
	 * @access		public
	 * @param		string
	 * @param		array
	 * @return		string
	 */
	public function displayTemplate($dir = '', $data = array())
	{
		return $this->load->view($dir, $data);
	}
        
        
        // --------------------------------------------------------------------
	
	/*
	 * Function to change the security token
	 *
	 * @access		public
	 * @return		string
	 */
	public function changeToken()
	{
		$this->session->unset_userdata('token');
		
		return $this->session->set_userdata('token', substr(sha1(microtime()),4,30));
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Copy postdata to object
	 *
	 * @access		public
	 * @param 		object
	 * @param 		string
	 * @return		void
	 */
	public function copyFromPost(&$object, $except = '')
	{
                $tools = new Tools();
                
		foreach ($tools->getPost() as $key => $value)
			if (key_exists($key, $object) AND $key != $except)
			{
				// Does not change password if field is empty
				if ($key == 'password' AND empty($value))
					continue;

				$object->{$key} = $value;
			}
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Log the actions
	 *
	 * @access		public
	 * @return		void
	 */
	public function logThis($reference_id = '', $description = '')
	{
		$this->load->model(admin_dir('administrator_log/administrator_log_model'));
                
                $log = new Administrator_Log_Model();
                
                $fields = array('administrator_id' => $this->session->get('admin', 'admin_id'),
                                'administrator_profile_id' => $this->session->get('admin', 'administrator_profile_id'),
                                'classname' => $this->classname,
                                'action' => $this->router->method,
                                'reference_id' => $reference_id,
                                'description' => $description,
                                'date_added' => date('Y-m-d G:i:s')
                                );

                $log->add($fields);
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Send E-mail
	 *
	 * @access		public
	 * @param		directory
	 * @param		string
	 * @param		string
	 * @param		string
	 * @param		string
	 * @param		string
	 * @return		void
	 */
	public function sendEmail($template = '', $subject = '', $to = '', $toName = '', $from = 'admin@email.com', $fromName = '', $cc = '', $bcc = '' )
	{
		$config['protocol'] = $this->configuration->getValue('value', array('configuration' => 'EMAIL_PROTOCOL'));
		$config['mailtype'] = $this->configuration->getValue('value', array('configuration' => 'EMAIL_TYPE'));
		$config['newline'] = "\r\n";
		$config['crlf'] = "\r\n";
		
		if ($config['protocol'] == 'smtp')
		{
			$config['smtp_host'] = $this->configuration->getValue('value', array('configuration' => 'SMTP_HOST'));
			$config['smtp_user'] = $this->configuration->getValue('value', array('configuration' => 'SMTP_USER'));
			$config['smtp_pass'] = $this->configuration->getValue('value', array('configuration' => 'SMTP_PASS'));
			$config['smtp_port'] = $this->configuration->getValue('value', array('configuration' => 'SMTP_PORT'));
		}
		
		// Load E-mail Library
		$this->load->library('email');
		
		$this->email->initialize($config);
		
		$this->email->from($from, $fromName);
		$this->email->to($to);
                $this->email->cc($cc);
                $this->email->bcc($bcc);
		$this->email->subject($subject);
		$this->email->set_newline("\r\n");
		$this->email->message($template);
	
                echo $this->email->print_debugger();
	
		$this->email->clear();
                
                
                
	}
        
        // --------------------------------------------------------------------
	
	/*
	 * Send E-mail PHPMailer
	 *
	 * @access		public
	 * @param		directory
	 * @param		string
	 * @param		string
	 * @param		string
	 * @param		string
	 * @return		void
	 */
        public function sendMail($template = '', $subject = '', $to = '', $from = 'yondutesting@gmail.com', $fromName = 'YONDU')
        {
            $result = $this->email
                ->from($from)
                ->reply_to($fromName)    // Optional, an account where a human being reads.
                ->to($to)
                ->subject($subject)
                ->message($template)
                ->send();

            var_dump($result);
            echo '<br />';
            echo $this->email->print_debugger();

            //exit;
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Function to backup table
	 *
	 * @access		public
	 * @return		void
	 */
        public function backupTable($tables = array(), $name = 'table', $download = FALSE)
        {
            // Load the DB utility class
            $this->load->dbutil();
            
            // set filename
            $filename = $name.'_'.date("Y_m_d_H_i_s").'.sql';
            
            $prefs = array(
                'tables'      => $tables,  // Array of tables to backup.
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'txt',             // gzip, zip, txt
                'filename'    => $filename,    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );
            
            // execute backup
            $backup = $this->dbutil->backup($prefs);
            
            // Load the file helper and write the file to your server
            $this->load->helper('file');
            write_file(FCPATH.'database/backupsql/tables/'.$filename, $backup); 
            
            // Load the download helper and send the file to your desktop
            $this->load->helper('download');
            
            if($download)
                force_download($filename, $backup);
            
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Function to backup table
	 *
	 * @access		public
	 * @return		void
	 */
        public function backupDB($name = 'database', $download = FALSE)
        {
            // Load the DB utility class
            $this->load->dbutil();
            
            // Backup your entire database and assign it to a variable
            $backup =& $this->dbutil->backup(); 
            
            // set filename
            $filename = $name.'_'.date("Y_m_d_H_i_s").'.sql';
            
            // Load the file helper and write the file to your server
            $this->load->helper('file');
            write_file(FCPATH.'database/backupsql/database/'.$filename, $backup); 

            // Load the download helper and send the file to your desktop
            $this->load->helper('download');
            
            if($download)
                force_download($filename, $backup);
            
        }
        
        // --------------------------------------------------------------------
	
	/*
	 * Upload images
	 *
	 * @access		private
	 * @return		void
	 */
        public function uploadImg($filepath, $filename)
        {
            $config['upload_path'] = $filepath;
            $config['allowed_types'] = 'gif|jpg|png';
            //$config['max_size'] = '100';
            //$config['max_width']  = '1024';
            //$config['max_height']  = '768';
            $config['overwrite'] = TRUE;
            //$config['encrypt_name'] = FALSE;
            //$config['remove_spaces'] = TRUE;
            $config['file_name'] = $filename;
            
            $this->load->library('upload', $config);
            
            $this->upload->initialize($config);
            
            if ( ! $this->upload->do_upload('photo'))
            {
                echo $this->upload->display_errors();die;
            }
            else
            {

                return $this->upload->data();
            }
        }
}
