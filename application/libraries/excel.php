<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');  
 // --------------------------------------------------------------------

/**
 * Excel Class
 *
 * Extends phpExcel third party application
 *
 * @author		Baladeva Juganas
 */

require_once APPPATH."third_party/PHPExcel/Classes/PHPExcel.php";

class Excel extends PHPExcel {
    
    public function __construct() {
        parent::__construct();
    }
    
}
