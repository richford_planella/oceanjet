<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// --------------------------------------------------------------------

/**
 * Cookie Class
 *
 * Extends basic PHP functions
 *
 * @author		Baladeva Juganas
 */
class Cookie
{
	public $CI;


	// ------------------------------------------------------------------------
	
	/*
	 * Set cookie data
	 *
	 * @access 		public
	 * @param 		string
	 * @param 		mixed
	 * @param 		time
	 * @return 		mixed
	 */
	public function set($cookie = '', $value = '')
	{
		setcookie($cookie, $value, time() + 1728000);
	}

	// ------------------------------------------------------------------------
	
	/*
	 * Get cookie data
	 *
	 * @access 		public
	 * @param 		string
	 * @return 		mixed
	 */
	public function get($cookie = '')
	{
		if ($cookie != '')
			if (isset($_COOKIE[$cookie]))
				return ($_COOKIE[$cookie]);
			else
				return FALSE;

		return $_COOKIE;
	}

}
