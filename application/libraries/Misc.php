<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// --------------------------------------------------------------------

/**
 * Misc Class
 *
 * Extends basic PHP functions
 *
 * @author		Baladeva Juganas
 */

class Misc {
    
    // ------------------------------------------------------------------------
	
    /*
     * Generate String from random characters
     *
     */
    function generate_code($length , $valid_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1023456789')
    {
        $random_string = "";
        $num_valid_chars = strlen($valid_chars);
        
        for ($i = 0; $i < $length; $i++)
        {
            $random_pick = mt_rand(1, $num_valid_chars);
            $random_char = $valid_chars[$random_pick-1];
            $random_string .= $random_char;
        }
        
        return $random_string;
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Convert interget to string
     *
     */
    function convert_number_to_words($number) {
    
        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Generate String from random characters
     *
     */
    function my_htmlspecialchars_decode($string,$style=ENT_COMPAT)
    {
        $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS,$style));
        if($style === ENT_QUOTES){ $translation['&#039;'] = '\''; }
        return strtr($string,$translation);
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Generate String from random characters
     *
     */
    function safe_html($string)
    {
        return htmlspecialchars($string,ENT_QUOTES);
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Generate String from random characters
     *
     */
    function decode_safe_html($string,$style=ENT_COMPAT)
    {
        $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS,$style));
        if($style === ENT_QUOTES){ $translation['&#039;'] = '\''; }
        return strtr($string,$translation);
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Generate a safe Array for SQL QUERY from $_POST
     *
     */
    function make_array_safe_for_SQL($array)
    {
        foreach($array as $key => $value)
        $array[$key] = $this->safe_html($value);

        return $array;
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Clean URL_ENCODE
     *
     */
    function clean_url($URL){
        $entities = array('%20','%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
        $replacements = array(' ','!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
        
        return str_replace($entities, $replacements, $URL);
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Display a calendar view 
     *
     */
    function generate_calendar($date_now){
        //variable to hold the calendar
        $calendar = "";
        //This gets today's date 
        $now = strtotime($date_now);
        $date = $now ;
        
        //This puts the day, month, and year in seperate variables
        $day = date('d', $date) ; 
        $month = date('m', $date) ; 
        $year = date('Y', $date) ;
        
        //Here we generate the first day of the month 
        $first_day = mktime(0,0,0,$month, 1, $year) ;
        
        //This gets us the month name 
        $title = date('F', $first_day) ;

        //Here we find out what day of the week the first day of the month falls on 
        $day_of_week = date('D', $first_day) ;
        
         //Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero

        switch($day_of_week){ 

            case "Sun": $blank = 0; break; 

            case "Mon": $blank = 1; break; 

            case "Tue": $blank = 2; break; 

            case "Wed": $blank = 3; break; 

            case "Thu": $blank = 4; break; 

            case "Fri": $blank = 5; break; 

            case "Sat": $blank = 6; break; 

        }
        
         //We then determine how many days are in the current month

        $days_in_month = date('t',mktime(0,0,0,$month, 1, $year)); //cal_days_in_month(0, $month, $year) ; 

        //Here we start building the table heads 

        $calendar = "<table border=1 width=294>";

        $calendar = $calendar."<tr><th colspan=7> $title $year </th></tr>";

        $calendar = $calendar."<tr><td width=42>S</td><td width=42>M</td><td 
        width=42>T</td><td width=42>W</td><td width=42>T</td><td 
        width=42>F</td><td width=42>S</td></tr>";



        //This counts the days in the week, up to 7

        $day_count = 1;



        $calendar = $calendar."<tr>";

        //first we take care of those blank days

        while ( $blank > 0 ) 

        { 

        $calendar = $calendar."<td></td>"; 

        $blank = $blank-1; 

        $day_count++;

        } 

        //sets the first day of the month to 1 

        $day_num = 1;



        //count up the days, untill we've done all of them in the month

        while ( $day_num <= $days_in_month ) 

        { 

        $calendar = $calendar."<td> $day_num </td>"; 

        $day_num++; 

        $day_count++;



        //Make sure we start a new row every week

        if ($day_count > 7)

        {

        $calendar = $calendar."</tr><tr>";

        $day_count = 1;

        }

        } 

        //Finaly we finish out the table with some blank details if needed

        while ( $day_count >1 && $day_count <=7 ) 

        { 

        $calendar = $calendar."<td> </td>"; 

        $day_count++; 

        } 


        $calendar = $calendar."</tr></table>";
        
        return $calendar;
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Return all value from DOCTYPE
     *
     */
    public function get_file_extension(){
        $ext = "csv|xls|ppt|zip|mp3|mp2|mid|mpga|wav|gif|jpeg|jpg|jpe|png|tiff|tif|txt|text|mpeg|mpg|mpe|qt|mov|avi|movie|doc|docx|word|pdf";
        return $ext;
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Return number of working days from date range
     *
     */
    public function getWorkingDays($startDate, $endDate)
    {
        $begin = strtotime($startDate);
        $end   = strtotime($endDate);
        if ($begin > $end) {
            echo "startdate is in the future! <br />";

            return 0;
        } else {
            $no_days  = 0;
            $weekends = 0;
            while ($begin <= $end) {
                $no_days++; // no of days in the given interval
                $what_day = date("N", $begin);
                if ($what_day > 5) { // 6 and 7 are weekend days
                    $weekends++;
                };
                $begin += 86400; // +1 day
            };
            $working_days = $no_days - $weekends;

            return $working_days;
        }
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Return number of days between two dates
     *
     */
    public function getDateDiff($startDate, $endDate)
    {
        $datetime1 = new DateTime("Y-m-d", strtotime($startDate));

        $datetime2 = new DateTime("Y-m-d", strtotime($endDate));

        $difference = $datetime1->diff($datetime2);
        
        return $difference;
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Return if date is weekends
     *
     */
    public function isWeekend($date) 
    {
        $weekDay = date('w', strtotime($date));
        return ($weekDay == 0 || $weekDay == 6);
    }
    
    // ------------------------------------------------------------------------
	
    /* Subtract time
     * Return string for hour and minutes
     *
     */
    function subtract_time($start_hour, $start_min, $end_hour, $end_minute)
    {
        // check if end time is valid
//        if($end_hour > $start_hour)
//        {
//            return 0;
//        }
        
        // check if end minute is greater than start minute
        if($end_minute > $start_min)
        {
            // subract 1 on hour
            $start_hour = $start_hour - 1;
            
            // add 60 minutes
            $start_min = $start_min + 60;
        }
        
        // create time
        $hours = $start_hour - $end_hour;
        $mins  = $start_min - $end_minute;
        
        return $this->generateReadableTime($hours,$mins,'real_time');
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Return string for hour and minutes
     *
     */
    function generateReadableTime($hours,$mins, $type = 'real_time')
    {
        if($mins > 59)
        {
            $total_min2 = floor($mins/60);
            $mins = $mins%60;
            $hours = $hours + $total_min2;

            if($mins < 10)
            {
                $mins = "0".$mins;
            }
        }
        else if($mins < 10)
        {
            $mins = "0".$mins;
        }

        if($hours < 10)
        {
            $dis_total_hour = "0"+$hours;
        }
        else
        {
            $dis_total_hour = $hours;
        }
        
        if($type == 'real_time')
            return $hours.$mins;
        elseif($type == 'number')
            return number_format($hours+($mins/60),2,'.','');
        elseif($type == 'time')
            return $hours.':'.$mins;
        elseif($type == 'display_time')
            return $dis_total_hour.':'.$mins;
        
    }
    
    // ------------------------------------------------------------------------
	
    /*
     * Return if date is in given range
     *
     */
    function check_in_range($start_date, $end_date, $date_start_from_user, $data_end_from_user)
    {
      // Convert to timestamp
      $start_ts = strtotime($start_date);
      $end_ts = strtotime($end_date);
      $user_start_ts = strtotime($date_start_from_user);
      $user_end_ts = strtotime($data_end_from_user);

      // Check that user date is between start & end
      return (
              ((($user_start_ts >= $start_ts) && ($user_start_ts <= $end_ts)) 
              OR 
              (($user_end_ts >= $start_ts) && ($user_end_ts <= $end_ts)))
              OR
              ((($start_ts >= $user_start_ts) && ($start_ts <= $user_end_ts)) 
              OR 
              (($end_ts >= $user_start_ts) && ($end_ts <= $user_end_ts)))
             );
    }
    
    /**
    * Creating date collection between two dates
    *
    * <code>
    * <?php
    * # Example 1
    * date_range("2014-01-01", "2014-01-20", "+1 day", "m/d/Y");
    *
    * # Example 2. you can use even time
    * date_range("01:00:00", "23:00:00", "+1 hour", "H:i:s");
    * </code>
    *
    * @param string since any date, time or datetime format
    * @param string until any date, time or datetime format
    * @param string step
    * @param string date of output format
    * @return array
    */
    function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) 
    {

       $dates = array();
       $start = strtotime($first);
       $end = strtotime($last);

       while( $start <= $end ) {

           $dates[] = date($output_format, $start);
           $start = strtotime($step, $start);
       }

       return $dates;
    }
    
}
// END Misc Class

/* End of file Misc.php */
/* Location: ./application/libraries/Misc.php */
