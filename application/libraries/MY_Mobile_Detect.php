<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 // --------------------------------------------------------------------

/**
 * PDF Class
 *
 * Extends mobile detect third party application
 *
 * @author		Kenneth Bahia
 */

class MY_Mobile_Detect {
    
    function mobile_detect()
    {
        $CI = & get_instance();
        log_message('Debug', 'Mobile Detect class is loaded.');
    }
 
    function load()
    {
        include_once APPPATH.'/third_party/Mobile-Detect-2.8.19/Mobile_Detect.php';
         
        return new Mobile_Detect;
    }
}